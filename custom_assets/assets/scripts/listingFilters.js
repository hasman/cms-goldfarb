(function (goldfarb, $) {
	goldfarb.listingFilters = function () {
		var $container = $('.listing-group');
		if (!$container.length) {
			return;
		}

		// init Isotope
		$container.each(function() {
			$(this).isotope({
				itemSelector: '.listing-tile',
				layoutMode: 'fitRows',
				sortBy: 'number',
				sortAscending: true,
				fitRows: {
					gutter: 0
				}
			});
		});

		// filter with selects and checkboxes

		let $filterGroup, $dataFilter, $dataSort, inclusives, unitSort, filterValue;

		$filterGroup = $('.data-filter-group');
		$dataFilter = $('[data-filter]');
		$dataSort = $('[data-sort-order]');
		unitSort = 'desc';

		$filterGroup.on('click', '[data-filter]', function(e) {
			//e.preventDefault();
			// map input values to an array
			inclusives = [];
			// inclusive filters from checkboxes
			$dataFilter.each( function( i, elem ) {
				// if checkbox, use value if checked
				let sortValue, elemValue;
				elemValue = elem.dataset.filter;

				if ( elem.checked) {
					inclusives.push( elemValue );
				}
			});
			filterValue = inclusives.length ? inclusives.join(', ') : '*';
			$container.isotope({ filter: filterValue });
			console.log(filterValue);
		});

		$dataSort.on('change', function(e) {
			let sortValue = this.options[this.selectedIndex].value;

			if (sortValue === unitSort) {
				$container.isotope({sortAscending: true});
			} else {
				$container.isotope({sortAscending: false});
			}
		});

	};
}(window.goldfarb = window.goldfarb || {}, jQuery));
