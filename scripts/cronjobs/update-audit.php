<?php
require_once __DIR__."/../zf.php";

$_db = Zend_Db_Table::getDefaultAdapter();
$options = Zend_Registry::get('configuration')->toArray();

$listings = Custom_Service_Audit::goldfarb();

foreach($listings as $listing) {
	$data = $listing;
	$data['created'] = $data['modified'] = new Zend_Db_Expr('NOW()');
	$last_insert_id = $_db->insert('gf_audits', $data);
	if ($last_insert_id) {
		echo ".";
	} else {
		echo "\nErr: " . json_encode($listing) . "\n";
	}
}
echo "\nDone\n";
