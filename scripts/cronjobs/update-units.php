<?php
require_once __DIR__."/../zf.php";

$_db = Zend_Db_Table::getDefaultAdapter();
$options = Zend_Registry::get('configuration')->toArray();

$log = [];
$availableUnits = [];
//for circut breaker since yardi likes to randomly retur zero results
$zeroUnitBuildings = 0;

$yardi = new Custom_Model_Yardi();

$mapper = new Custom_Model_Mapper_GfBuilding();
$buildings = $mapper->fetchYardiBuildings();

$mapper = new Custom_Model_Mapper_GfUnit();

foreach ($buildings as $building) {
	$properties = explode(',', $building->yardi_code);

	//loop through each property code
	foreach ($properties as $propertyCode) {
		$data = $yardi->getAvailability($propertyCode);
		if ($data !== false && is_array($data) ) {
			//update units
			foreach ($data as $unit) {
				if (!isset($unit->Error) && isset($unit->ApartmentId) && $unit->ApartmentId) {
					$mapper->updateUnit($unit, $building->id);
					$availableUnits[] = $unit->ApartmentId;
				}else {
					$log[] = "Error response for building: {$building->name} property: code {$propertyCode}. Error: {$unit->Error} <br/>\n";
				}
			}
		} else {
			$zeroUnitBuildings++;
			$log[] = "Yardi API did not have any availability for building: {$building->name} property: code {$propertyCode} <br/>\n";
		}
	}
}
if($zeroUnitBuildings <= 5){
	$mapper->setRentedUnitsToOccupied($availableUnits);
}else {//circuit breaker fired
	$mail = new Zend_Mail();
	$mail->setBodyHtml('<p>Issue with the Yardi API. Contact Yardi because the Rent Cafe is returning buildings without units.</p>');
	$mail->setFrom('noreply@goldprop.com', 'Goldfarb Website');

	if (APPLICATION_ENV == 'production') {
		$mail->addTo('marketing@goldprop.com');
		$mail->addTo('helpdesk@goldprop.com');
	}else {
		$mail->addTo('craig@cyber-ny.com');
		$mail->addTo('ahmadou@cyber-ny.com');
	}

	$mail->setSubject('Goldfarb Website: Yardi API Error');
	$mail->send();

	$log[] = "Yardi API returned too many buildings without units, no units where set to occupied <br/>\n";
}

foreach ($log as $line) {
	echo $line;
}
