<?php
require_once __DIR__."/../zf.php";

$_db = Zend_Db_Table::getDefaultAdapter();
$options = Zend_Registry::get('configuration')->toArray();

$domain = (APPLICATION_ENV == 'production')?"https://goldfarbproperties.com":"https://staging-goldfarb.cnyawscloud2.com";


$sql = "SELECT u.*, b.name AS building, b.code AS building_code, r.code AS region_code
	FROM gf_audits AS a
	LEFT JOIN gf_units AS u ON a.unit_id = u.id
	LEFT JOIN gf_buildings AS b ON u.building_id = b.id
	LEFT JOIN gf_neighborhoods AS n ON b.neighborhood_id = n.id
	LEFT JOIN gf_regions AS r ON n.region_id = r.id
	WHERE (
	REPLACE(a.price,'$','') <> u.price OR 
	(u.status = 'Available' AND a.status IN ('unavailable','occupied')) 
	)
	#AND u.status = 'Available'
	AND a.created >= DATE_SUB(NOW(), INTERVAL 1 DAY) 
	GROUP BY u.id";
$units = $_db->fetchAll($sql);

if ($units) {
	$body = [];

	$unit_mapper = new Custom_Model_Mapper_GfUnit();
	$unit_image_mapper = new Custom_Model_Mapper_GfUnitImage();

	foreach ($units as $unit) {
		$unit_image = $unit_image_mapper->fetchPrimaryByUnitId($unit['id']);
		$effective_price = $unit_mapper->getUnitNetEffectivePrice($unit['id']);

		$bedrooms = ($unit['bedrooms'])?"{$unit['bedrooms']} Bed":"Studio";
		$bathrooms = ($unit['bathrooms'])?"{$unit['bathrooms']} Bath":"";

		$price = "$".number_format($effective_price,0);
		if ($effective_price < $unit['price']) {
			$price .= "<sup>*</sup>";
		}

		$unit_url = "$domain/rentals/".$unit['region_code']."/".$unit['building_code']."/apartment-".$unit['code'];

		$body[] = "<tr>
                    <td class=\"location-container__image-cell\">
                       <img src=\"".$domain."/".$unit_image['image']."\">
                    </td>
                </tr>
                <tr class=\"location-container__specific-detail-container\">
                    <td>
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"location-container__specific-detail-table\">
                            <tr class=\"location-container--border\">
                                <td  class=\"location-container__location-name\">
                                    ".$unit['building']." <br>#".$unit['number']."
                                </td>
                                <td class=\"location-container__apartment-details\">
                                    ".$bedrooms."<br>".$bathrooms."
                                </td>
                                <td class=\"location-container__price\">
                                    ".$price."
                                </td>
                            </tr>
                            <tr>
                                <td class=\"location-container__view-unit-container\" colspan=\"3\">
                                    <a class=\"location-container__view-unit-link\" href=\"".$unit_url."\" ><span style=\"color:#d8b256;\">View Unit</span></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>";
	}

	$mailbody = "
	<style type=\"text/css\">
        table{
        border-collapse:collapse;
        }
        .body-container{
        background-color: #e6e6e6;
        font-family: Helvetica, sans-serif;
        margin: 0 auto !important;
        padding: 0;
        width: 100%;
        -webkit-text-size-adjust:100%;
        -ms-text-size-adjust:100%;
        }
        .container-table{
        max-width:600px;
        }
        .container-table__cell{
        padding: 10px 0 30px 0;
        max-width:600px;
        }
        .email-body-container{
        margin:0 auto;
        }
        .email-body-container__logo{
        padding: 40px 0;
        }
        .email-content-container__listing-container{
        padding-top:20px;
        }
        .email-content-container__listing-header{
        background-color: #ffffff;
        border-bottom: 1px solid #ddd8d3;
        font-size: 15px;
        font-weight: 100;
        letter-spacing: 7px;
        padding: 20px;
        text-align: center;
        text-transform: uppercase;
        line-height: 1.7;
        }
        .email-content-container__location-content{
        background-color: #f9f9f9;
        padding: 15px;
        }
        .location-container__borough-cell{
        background-color: #ffffff;
        font-size: 13px;
        padding: 10px 0px 10px 10px;
        text-transform: uppercase;
        text-align:left;
        }
        .location-container__spacer{
        height: 25px;
        }
        .location-container__specific-detail-table td{
        border: 1px solid #ddd8d3;
        }
        .location-container__creation-cell{
        background-color: #ffffff;
        color: #aaaaaa;
        font-size: small;
        font-weight: 100;
        padding: 10px 8px 10px 1px;
        text-align: right;
        }
        .location-container__image{
        width: 100%;
        height: auto;
        display: block;
        }
        .location-container__location-name{
        font-size: 15px;
        font-weight: 100;
        padding:10px;
        padding-left: 5px;
        text-align: center;
        }
        .location-container__apartment-details{
        font-size: 15px;
        font-weight: 100;
        padding: 15px;
        text-align: center;
        }
        .location-container__price{
        font-family: Helvetica;
        font-size: 15px;
        font-weight: 100;
        padding: 15px;
        text-align: center;
        }
        .location-container__address-information{
        padding: 15px 10px 10px 10px;
        /*border: 0;*/
        font-weight: 200;
        font-size: 14px;
        }
        .location-container__specific-detail-container{
        margin-bottom: 15px;
        }
        .location-container__apartment-description{
        padding: 15px 10px 15px 10px;
        font-weight: 100;
        font-size: 15px;
        }
        .location-container__view-unit-container{
        letter-spacing: 5px;
        padding-bottom: 20px;
        padding-top: 20px;
        text-align: center;
        }
        .location-container__view-unit-link{
        border: 2px solid #d8b256;
        color: #d8b256;
        padding: 10px 20% 10px 20%;
        text-decoration: none;
        text-transform: uppercase;
        width: 100%;
        }
        .footer-container__social-media-icons-container{
        padding: 20px;
        text-align: center;
        }
        .footer-container__social-media-icons-container a{
        padding: 0  20px;
        }
        .footer-container__dashboard-link-container{
        padding: 0 0 15px 0;
        text-align: center;
        }
        .footer-container__visit-dashboard-link{
        color: #aaaaaa;
        font-size: 15px;
        font-weight: 100;
        text-decoration: none;
        }
        .footer-container__view-online-container{
        text-align: center;
        padding :5px;
        }
        .footer-container__view-online-link{
        color: #aaaaaa;
        font-size: 15px;
        font-weight: 100;
        text-decoration: none;
        }
        .footer-container__copyright-container{
        padding-bottom: 20px;
        width: 100%;
        }
        .footer-container__copyright{
        color: #aaaaaa;
        font-size: 15px;
        font-weight: 100;
        text-align: center;
        }
        @media screen and (min-device-width: 482px) and (max-device-width: 600px) {
        table[class=\"container-table\"]{
        width:100% !important;
        }
        body{
        width:100% !important;
        }
        }
        @media screen and (max-device-width: 481px) {
        body{
        width:100% !important;
        }
        .email-content-container__listing-header{
        letter-spacing: 1px !important;
        max-width:600px !important;
        }
        .location-container__price, .location-container__apartment-details, .location-container__location-name {
        border: 0 !important;
        margin: auto !important;
        padding-bottom: 5px !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
        padding-top: 5px !important;
        text-align: center !important;
        width: 100% !important;
        }
        .location-container__address-information{
        border-top: 0;
        }
        .location-container__specific-detail-table{
        width: 100% !important;
        }
        .location-container__borough-cell{
        text-align: center !important;
        padding: 5px !important;
        }
        .location-container__creation-cell{
        text-align: center !important;
        padding: 5px !important;
        }
        .location-container__view-unit-link{
        padding: 10px 10% 10px 10% !important;
        font-size: small !important;
        font-weight: bold !important;
        }
        .footer-container__dashboard-link-container{
        padding-right: 5px !important;
        }
        .footer-container__view-online-link, .footer-container__visit-dashboard-link{
        font-size:13px !important;
        }
        </style>
        <!--[if gte mso 9]>
        <style type=\"text/css\">
        .location-container__address-information,
        .email-content-container__listing-header,
        .location-container__borough-cell,
        .location-container__creation-cell,
        .location-container__location-name,
        .location-container__price,
        .location-container__apartment-details,
        .location-container__apartment-description,
        .location-container__view-unit-link,
        .footer-container__visit-dashboard-link,
        .footer-container__view-online-link,
        .footer-container__unsubscribe-link,
        .footer-container__copyright{
        font-family: Helvetica, sans-serif;
        }
        </style>
         <![endif]-->
	";

	$mailbody .= " <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"  width=\"100%\">
                    <tr class=\"email-content-container__listing-container\">
                        <td class=\"email-content-container__listing-header\">
                            Yesterday's Updates <br> ".date('l, F j, Y', strtotime("yesterday"))."
                        </td>
                    </tr>";
	$mailbody .= implode("", $body);
	$mailbody .= "</table>";

	if (APPLICATION_ENV == 'production') {
		$subject = 'Website Updated Units';

		$email_address = 'marketing@goldprop.com';
	} else {
		$subject = 'STAGING :: Website Updated Units';
		$email_address = 'ahmadou@cyber-ny.com';
		//$email_address = 'craig@cyber-ny.com';
		//$email_address = 'marketing@goldprop.com';

		exit;
	}

	$emails = new Application_Model_Emails();
	$emails->generic($mailbody, $email_address, $subject);
}
