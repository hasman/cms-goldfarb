<?php
class Admin_Model_AdminUser extends Application_Model_Abstract
{
	public $id;
	//public $username;
	public $first_name;
	public $last_name;
	public $password;
	public $email;
	public $role;
	public $last_login;
	public $created;
	public $modified;
	public $status;
	public $avatar;
}
