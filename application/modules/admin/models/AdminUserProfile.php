<?php
class Admin_Model_AdminUserProfile extends Application_Model_Abstract
{
	public $admin_user_id;
	public $company;
	public $date_of_birth;
	public $phone;
	public $address;
	public $address2;
	public $city;
	public $region;
	public $postcode;
	public $country;
	public $website;
	public $facebook_url;
	public $twitter_url;
	public $instagram_url;
	public $youtube_url;
	public $linkedin_url;
}
