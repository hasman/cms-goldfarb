<?php
class Admin_Model_Mapper_Collection extends Application_Model_Mapper_Collection
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select();

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('name '.$match.' OR
				                            code '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('name '.$match);
							break;
						case 'status':
							$select->where('status = ?', $val);
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchIdPairs($ignore_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('c'=>$this->_table_prefix.'collections'),'*')
			->order('name')
		;

		if ($ignore_id) {
			$select->where('id <> ?', $ignore_id);
		}

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->id] = $row->name;
		}

		return $pairs;
	}

	public function fetchCodePairs($ignore_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('c'=>$this->_table_prefix.'collections'),'*')
			->order('name')
		;

		if ($ignore_id) {
			$select->where('id <> ?', $ignore_id);
		}

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->code] = $row->name;
		}

		return $pairs;
	}

	public function fetchStringList($product_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('c'=>$this->_table_prefix.'collections'),'*')
			->order(array('c.name ASC'));
		if ($product_id) {
			$select->joinLeft(array('pc'=>$this->_table_prefix.'product_collections'),'pc.collection_id = c.id','')
				->joinLeft(array('p'=>$this->_table_prefix.'products'),'pc.product_id = p.id', '')
				->where("pc.product_id = ?",$product_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.$item->name.'"';
		}

		return implode(",",$all);
	}

	public function fetchJSON($product_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('c'=>$this->_table_prefix.'collections'),'*')
			->order(array('c.name ASC'));
		if ($product_id) {
			$select->joinLeft(array('pc'=>$this->_table_prefix.'product_collections'),'pc.collection_id = c.id','')
				->joinLeft(array('p'=>$this->_table_prefix.'products'),'pc.product_id = p.id', '')
				->where("pc.product_id = ?",$product_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = array("id"=>$item->id,"text"=>$item->name);
		}

		return json_encode($all);
	}

}
