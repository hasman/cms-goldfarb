<?php
class Admin_Model_Mapper_VideoPlatformVideoAnalytic extends Application_Model_Mapper_VideoPlatformVideoAnalytic
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('v'=>$this->_table_prefix.'video_platform_videos'),'*')
			->joinLeft(array('va'=>$this->_table_prefix.'video_platform_video_analytics'), 'va.video_platform_video_id = v.id', '')
			->columns(array(
				'total_views'=>new Zend_Db_Expr("COUNT(*)"),
				'total_plays'=>new Zend_Db_Expr("(SELECT COUNT(*) FROM {$this->_table_prefix}video_platform_video_analytics AS va2 WHERE v.id = va2.video_platform_video_id AND va2.played = 'Yes')"),
				'total_seconds'=>new Zend_Db_Expr("SUM(va.seconds_played)"),
			))
			->group('v.id')
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('v.filename '.$match.' OR
											v.title '.$match.' OR
				                            v.code '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('v.title '.$match);
							break;
						case 'status':
							$select->where('v.status = ?', $val);
							break;
						case 'date_from':
							$select->where('va.created >= ?',$val);
							break;
						case 'date_to':
							$select->where('va.created <= ?',$val);
							break;
						default:
							$select->where('v.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchByCustomerId($customer_id, $limit = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->where("customer_id = ?", $customer_id)
			->order('created DESC')
			->group('video_platform_video_id')
		;

		if ($limit) {
			$select->limit($limit);
		}

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			return $result;
		}

		return null;
	}
}
