<?php
class Admin_Model_Mapper_FaqSection extends Application_Model_Mapper_FaqSection
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('s'=>$this->_table_prefix.'faq_sections'),'*')
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('s.code '.$match.' OR
										s.title '.$match);
							break;
						case 'title':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('s.title '.$match);
							break;
						case 'code':
							$select->where('s.code '.$match);
							break;
						default:
							$select->where('s.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchOrdered()
	{
		$select = $this->getDbTable()->select()
			->order('sort_order ASC');

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}

	public function fetchCodePairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->code] = $row->title;
		}

		return $array;
	}
}
