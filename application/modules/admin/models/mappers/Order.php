<?php
class Admin_Model_Mapper_Order extends Application_Model_Mapper_Order
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('o'=>$this->_table_prefix.'orders'),'*')
			->joinLeft(array('oab'=>$this->_table_prefix.'order_addresses'),'o.id = oab.order_id AND oab.type="billing"',array('first_name','last_name','company','address','address2','city','region','postcode','country','phone'))
			->joinLeft(array('oas'=>$this->_table_prefix.'order_addresses'),'o.id = oas.order_id AND oas.type="shipping"',array('shipping_first_name'=>'first_name','shipping_last_name'=>'last_name','shipping_company'=>'company','shipping_address'=>'address','shipping_address2'=>'address2','shipping_city'=>'city','shipping_region'=>'region','shipping_postcode'=>'postcode','shipping_country'=>'country','shipping_phone'=>'phone'))
			->joinLeft(array('opay'=>$this->_table_prefix.'order_payments'),'o.id = opay.order_id',array('type','total_amount','discount_amount','shipping_amount','tax_amount'))
			->joinLeft(array('op'=>$this->_table_prefix.'order_profiles'),'o.id = op.order_id',array('promo_code','affiliate_code','site'))
			->joinLeft(array('oship'=>$this->_table_prefix.'order_shipping'),'o.id = oship.order_id',array('shipping_service'=>'service','shipping_class'=>'class'))
			->group('o.id')
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('oab.first_name '.$match.' OR
				                            oab.last_name '.$match.' OR
				                            oab.company '.$match.' OR
				                            op.promo_code '.$match.' OR
				                            op.affiliate_code '.$match.' OR
				                            o.email '.$match.' OR
				                            o.id = "'.$val.'"');
							break;
						case 'type':
							$select->where('opay.type '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('oab.last_name '.$match);
							break;
						case 'customer_id':
							$select->where('customer_id =?', $val);
							break;
						case 'status':
							$select->where('o.status =?', $val);
							break;
						case 'date_from':
							$select->where('o.created >= ?',$val);
							break;
						case 'date_to':
							$select->where('o.created <= ?',$val);
							break;
						case 'address':
							$select->where('oab.address '.$match.' OR
				                            oas.address '.$match);
							break;
						case 'city':
							$select->where('oab.city '.$match.' OR
				                            oas.city '.$match);
							break;
						case 'zip':
							$select->where('oab.postcode '.$match.' OR
				                            oas.postcode '.$match);
							break;
						case 'shipping_region':
							$select->where('oas.region =?', $val);
							break;
						case 'auth_code':
							$select->where('opay.response '.$match.'');
							break;
						case 'site':
							$select->where('op.site =?', $val);
							break;
						case 'item_status':
							$select->joinLeft(array('oi'=>'order_items'),'o.id = oi.order_id',null)
								->where('oi.status = ?', $val);
							break;
						default:
							$select->where('o.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'o.status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function findNextPrev($search = '', $current_id, $dir = 'next') {
		if ($dir == 'next') {
			$dir = "DESC";
		}elseif ($dir == 'prev') {
			$dir = "ASC";
		}

		$select = $this->listSearch('o.id', $dir, $search);

		if ($dir == 'next') {
			$select->where('o.id < ?', $current_id);
		}elseif ($dir == 'prev') {
			$select->where('o.id > ?', $current_id);
		}

		$select->limit(1);

		$row = $this->getDbTable()->fetchRow($select);

		if (!$row) return null;

		return $row->id;
	}

	public function fetchSales($start_date = '', $end_date = '')
	{
		//sales per day - month to date
		//echo "<br><br>orderdate_range: " . $date_range;

		$sql = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('o'=>$this->_table_prefix.'orders'),'')
			->joinLeft(array('opay'=>$this->_table_prefix.'order_payments'),'o.id = opay.order_id','')
			->where('o.status <> "Canceled"')
			->columns('DATE_FORMAT(o.created,"%Y-%m-%d") AS period','o')
			->columns('(SUM(opay.total_amount)) AS amount','opay')
			->columns('(COUNT(*)) AS orders', 'o')
			->group('DATE_FORMAT(o.created,"%Y%m%d")')
		;

		if ($start_date) {
			$sql->where('o.created >= ?', $start_date);
		}

		if ($end_date) {
			$sql->where('o.created <= ?', $end_date);
		}

		$result = $this->getDbTable()->fetchAll($sql);

		return $result;
	}

	public function fetchSalesYear()
	{
		//fetchSalesYear
		$sql = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('o'=>$this->_table_prefix.'orders'),'')
			->joinLeft(array('opay'=>$this->_table_prefix.'order_payments'),'o.id = opay.order_id','')
			->where('o.created >= DATE_SUB(DATE(NOW()), INTERVAL 12 MONTH) ')
			->where('o.status <> "Canceled"')
			->columns('DATE_FORMAT(o.created,"%Y-%m") AS period','o')
			->columns('(SUM(opay.total_amount)) AS amount','opay')
			->columns('(COUNT(*)) AS orders', 'o')
			->group('DATE_FORMAT(o.created,"%Y%m")')
		;
		$result = $this->getDbTable()->fetchAll($sql);

		return $result;
	}

	public function fetchProductOrder()
	{
		//fetchProductOrder - month to date
		$sql = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('o'=>$this->_table_prefix.'orders'),'')
			->joinLeft(array('oi'=>$this->_table_prefix.'order_items'),'o.id = oi.order_id',array('warehouse_sku','title'))
			->where('o.created >= DATE_SUB(DATE(NOW()), INTERVAL 1 MONTH) ')
			->where('sku IS NOT NULL')
			->where('oi.line_total > 0')
			->where('o.status <> "Canceled"')
			->columns('(SUM(oi.line_total)) AS amount','oi')
			->columns('(SUM(oi.qty)) AS orders', 'o')
			->group('oi.warehouse_sku')
			->order('orders DESC')
			->limit(20)
		;
		$result = $this->getDbTable()->fetchAll($sql);

		return $result;
	}

	public function fetchProductOrderAllTime()
	{
		//fetchProductOrderAllTime
		$sql = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('o'=>$this->_table_prefix.'orders'),'')
			->joinLeft(array('oi'=>$this->_table_prefix.'order_items'),'o.id = oi.order_id',array('warehouse_sku','title'))
			->where('sku IS NOT NULL')
			->where('oi.line_total > 0')
			->where('o.status <> "Canceled"')
			->columns('(SUM(oi.line_total)) AS amount','oi')
			->columns('(SUM(oi.qty)) AS orders', 'o')
			->group('oi.warehouse_sku')
			->order('orders DESC')
			->limit(20)
		;
		$result = $this->getDbTable()->fetchAll($sql);

		return $result;
	}

	public function recentOrder()
	{
		//Recent order
		$sql = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('o'=>$this->_table_prefix.'orders'),'*')
			->joinLeft(array('oab'=>$this->_table_prefix.'order_addresses'),'o.id = oab.order_id AND oab.type="billing"',array('first_name','last_name','company'))
			->joinLeft(array('opay'=>$this->_table_prefix.'order_payments'),'o.id = opay.order_id',array('type','total_amount'))
			->columns(new Zend_Db_Expr ('"Order" AS order_type'))
			->order('o.created DESC')
			->limit(10)
		;
		$result = $this->getDbTable()->fetchAll($sql);

		return $result;
	}

}
