<?php
class Admin_Model_Mapper_Template extends Application_Model_Mapper_Template
{
	public function listAll(){
		$select = $this->getDbTable()->select()->order('title');
		return $this->fetchAll($select);
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}

	public function fetchCodePairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->code] = $row->title;
		}

		return $array;
	}

	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('t'=>$this->_table_prefix.'templates'),'*')
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('t.code '.$match.' OR
										t.title '.$match);
							break;
						case 'title':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('t.title '.$match);
							break;
						case 'code':
							$select->where('t.code '.$match);
							break;
						default:
							$select->where('t.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function checkDuplicate($code, $id=null)
	{
		if (!$code) return true;

		$select = $this->getDbTable()->select()->where("code = ?",$code);
		if ($id) {
			$select->where("id <> ?",$id);
		}

		if ($this->fetchAll($select)) {
			return true;
		}
		return false;
	}
}
