<?php
class Admin_Model_Mapper_Page extends Application_Model_Mapper_Page
{
	protected $events;

	public function events(Zend_EventManager_EventCollection $events = null)
	{
		if (null !== $events) {
			$this->events = $events;
		} elseif (null === $this->events) {
			$this->events = new Zend_EventManager_EventManager(__CLASS__);
		}
		return $this->events;
	}

	public function listAll(){
		$select = $this->getDbTable()->select()->order('title');
		return $this->fetchAll($select);
	}

	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select();

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (!is_array($val)) {
						if (strpos($val, '"') !== false) {
							$match = '= "' . str_replace('"', '', $val) . '"';
						} else {
							$match = 'LIKE "%' . addslashes($val) . '%"';
						}
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('title '.$match.' OR
				                            code '.$match);
							break;
						case 'code':
							$select->where('code '.$match.'');
							break;
						case 'status':
							$select->where('status =?', $val);
							break;
						case 'editable':
							if (is_array($val)) {
								$ar = '';
								foreach ($val as $k=>$v) {
									$ar .= "'$v',";
								}
								$select->where('editable IN ('.substr($ar,0,-1).')');
							}else {
								$select->where('editable =?', $val);
							}
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}

		return $select;
	}

	public function fetchPagePairs()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('block_type = ?', 'page')
			->order('title')
		;
		$results = $this->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->code] = $row->title;
		}

		return $pairs;
	}

	public function fetchBlockPairs()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('block_type = ?', 'block')
			->order('title')
		;
		$results = $this->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->id] = $row->title;
		}

		return $pairs;
	}

	public function save($entity)
	{
		$params = compact('entity');
		$this->events()->trigger(__FUNCTION__.'.pre', $this, $params);

		$output = parent::save($entity);

		$params = compact('entity', 'output');
		$this->events()->trigger(__FUNCTION__.'.post', $this, $params);

		return $output;
	}

}
