<?php
class Admin_Model_Mapper_ProductCategory extends Application_Model_Mapper_ProductCategory
{
	public function deleteByProductId($product_id)
	{
		return $this->getDbTable()->delete(array('product_id = ?' => $product_id));
	}

	public function fetchProductsByCategoryId($category_id, $sort_type = null, $limit = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pc'=>$this->_table_prefix."product_categories"),array('product_category_id'=>'id'))
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'pc.product_id = p.id', '*')
			->where("pc.category_id = ?", $category_id)
			->order('p.title_sort ASC');
		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}

	public function fetchCategoryNamesByProductId($product_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pc'=>$this->_table_prefix.'product_categories'))
			->where("pc.product_id = ?", $product_id)
			->joinLeft(array('c'=>$this->_table_prefix.'categories'),'pc.category_id = c.id','name')
			->columns(array('parent_name'=>"(SELECT NAME FROM {$this->_table_prefix}categories WHERE c.parent_id = categories.id)"))
			->order(array('pc.primary_category DESC','c.name ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$name = '';
				if ($row->parent_name) {
					$name .= $row->parent_name." -> ";
				}
				$name .= $row->name;

				$collection[] = $name;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function save($entity)
	{
		$select = $this->getDbTable()->select()
			->where("category_id = ?", $entity->category_id)
			->where("product_id = ?", $entity->product_id)
		;
		$row = $this->getDbTable()->fetchRow($select);

		if ($row) {
			$output = $row;
		} else {
			$output = parent::save($entity);
		}

		return $output;
	}

	public function deleteByCategoryIdTagId($category_id, $tag_id)
	{
		$mapper = new Application_Model_Mapper_ProductTag();
		$products = $mapper->fetchProductsByTagId($tag_id);

		foreach ($products as $product) {
			$this->getDbTable()->delete(array("category_id = '$category_id' AND product_id = ?" => $product->id));
		}

		return true;
	}
}
