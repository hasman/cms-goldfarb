<?php
class Admin_Model_Mapper_VideoPlatformOrder extends Application_Model_Mapper_VideoPlatformOrder
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){

		//Setting setIntegrityCheck(true) would result to: Select query cannot join with another table
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pi'=>$this->_table_prefix.'video_platform_order_items'), array('video_platform_order_id','video_platform_video_id','title', 'price','purchase_type'))
			->joinLeft(array('pv'=>$this->_table_prefix.'video_platform_videos'),'pi.video_platform_video_id = pv.id', array('code','filename','transcode_status','thumbnail_src','poster_src','notify_email'))
			->joinLeft(array('po'=>$this->_table_prefix.'video_platform_orders'),'pi.video_platform_order_id = po.id', array('id','customer_id','bill_method','total','created'))
			->joinLeft(array('cu'=>$this->_table_prefix.'customers'),'po.customer_id = cu.id', array('first_name','last_name','email','status'))
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('pi.title '.$match.' OR											
				                            cu.email '.$match.' OR 
				                            cu.last_name '.$match. ' OR
				                            cu.first_name '.$match);
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('pi.title '.$match);
							break;
						case 'purchase_type':
							$select->where('pi.purchase_type = ?', $val);
							break;
						case 'date_from':
							$select->where('po.created >= ?',$val);
							break;
						case 'date_to':
							$select->where('po.created <= ?',$val);
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchTransaction($trans_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pi'=>$this->_table_prefix.'video_platform_order_items'), array('title','price','discount','tax','total','purchase_type','created'))
			->joinLeft(array('po'=>$this->_table_prefix.'video_platform_orders'),'pi.video_platform_order_id = po.id', array('id','customer_id','bill_method'))
			->joinLeft(array('cu'=>$this->_table_prefix.'customers'),'po.customer_id = cu.id', array('email','status'))
			->joinLeft(array('poa'=>$this->_table_prefix.'video_platform_order_addresses'),'pi.video_platform_order_id = poa.video_platform_order_id', array('type','address','address2','city','county','region','postcode','country','phone','first_name','last_name'))
			->where('po.id = ?', $trans_id)
		;
		$result = $this->getDbTable()->fetchRow($select);

		return $result;
	}


	public function videoSales($start_date = '', $end_date = '')
	{
		//echo "<br><br>videodate_range: " . $date_range;
		//video sales - month to date
		$sql = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('vpoi'=>$this->_table_prefix.'video_platform_order_items'),'*')
			->joinLeft(array('vpopay'=>$this->_table_prefix.'video_platform_order_payments'),'vpoi.video_platform_order_id = vpopay.video_platform_order_id','*')
			->columns('DATE_FORMAT(vpoi.created,"%Y-%m-%d") AS period','vpoi')
			->columns('(SUM(vpopay.total_amount)) AS amount','vpopay')
			->columns('(COUNT(*)) AS orders', 'vpoi')
			->group('DATE_FORMAT(vpoi.created,"%Y%m%d")')
			->order('vpoi.created DESC')
			->limit(10)
		;

		if ($start_date) {
			$sql->where('vpoi.created >= ?', $start_date);
		}

		if ($end_date) {
			$sql->where('vpoi.created <= ?', $end_date);
		}

		$result = $this->getDbTable()->fetchAll($sql);

		return $result;
	}

	public function videoSalesTop10()
	{
		//video sales - month to date
		$sql = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('vpoi'=>$this->_table_prefix.'video_platform_order_items'),'*')
			->joinLeft(array('vpopay'=>$this->_table_prefix.'video_platform_order_payments'),'vpoi.video_platform_order_id = vpopay.video_platform_order_id','*')
			->joinLeft(array('vpoa'=>$this->_table_prefix.'video_platform_order_addresses'),'vpoi.video_platform_order_id = vpoa.video_platform_order_id AND vpoa.type="billing"',array('first_name','last_name','company'))
			->columns(new Zend_Db_Expr ('"VideoOrder" AS order_type'))
			->order('vpoi.created DESC')
			->limit(10)
		;
		$result = $this->getDbTable()->fetchAll($sql);

		return $result;
	}

}
