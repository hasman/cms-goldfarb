<?php
class Admin_Model_Mapper_Video extends Application_Model_Mapper_Video
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('v'=>$this->_table_prefix.'videos'),'*')
			->joinLeft(array('vc'=>$this->_table_prefix.'video_categories'), 'v.video_category_id = vc.id', array('category_title'=>'name'))
			;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('v.title '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('title '.$match);
							break;
						case 'video_category_id':
							$select->where('v.video_category_id =?', $val);
							break;
						case 'status':
							$select->where('v.status =?', $val);
							break;
						default:
							$select->where('v.id =?', $val);
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchByCategoryId($category_id = 0)
	{
		$select = $this->getDbTable()->select()->where("video_category_id = ?", $category_id)
			->order('sort_order ASC');

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}
}
