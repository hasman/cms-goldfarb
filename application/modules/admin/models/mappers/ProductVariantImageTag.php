<?php
class Admin_Model_Mapper_ProductVariantImageTag extends Application_Model_Mapper_ProductVariantImageTag
{
	public function deleteByProductVariantImageId($image_id)
	{
		return $this->getDbTable()->delete(array('product_variant_image_id = ?' => $image_id));
	}
}
