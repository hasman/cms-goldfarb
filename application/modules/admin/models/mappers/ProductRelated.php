<?php
class Admin_Model_Mapper_ProductRelated extends Application_Model_Mapper_ProductRelated
{
	public function fetchRelatedProductIdsByProductId($product_id)
	{
		$select = $this->getDbTable()->select()->where("product_id = ?",$product_id);
		$results = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($results as $row) {
			$array[$row->product_related_id] = $row->product_related_id;
		}

		return $array;
	}

	public function fetchRelatedProductsByProductId($product_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pr'=>$this->_table_prefix.'product_related'),'')
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'pr.product_related_id = p.id',array('product_title'=>'title','product_id'=>'id', 'product_sku'=>'sku'))
			->where("product_id = ?",$product_id);
		$results = $this->getDbTable()->fetchAll($select);

		return $results;
	}

	public function deleteByProductId($product_id)
	{
		$this->getDbTable()->delete("product_id = '$product_id'");

		return;
	}
}
