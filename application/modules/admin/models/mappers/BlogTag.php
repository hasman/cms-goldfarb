<?php
class Admin_Model_Mapper_BlogTag extends Application_Model_Mapper_BlogTag
{
	public function fetchOrdered()
	{
		$select = $this->getDbTable()->select()->order(array('title ASC'));
		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}

	public function fetchByBlogId($blog_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('bpt'=>$this->_table_prefix.'blog_post_tag'),'*')
			->where("bpt.blog_post_id = ?", $blog_id)
			->joinLeft(array('bt'=>$this->_table_prefix.'blog_tags'),'bpt.blog_tag_id = bt.id','')
			->order(array('bt.title ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function doesTagExist($tag_title)
	{
		$select = $this->getDbTable()->select()
			->where("title = ?", $tag_title)
		;

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function fetchTagNamesByBlogId($blog_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('bpt'=>$this->_table_prefix.'blog_post_tag'))
			->where("bpt.blog_post_id = ?", $blog_id)
			->joinLeft(array('bt'=>$this->_table_prefix.'blog_tags'),'bpt.blog_tag_id = bt.id','title')
			->order(array('bt.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function deleteByBlogId($blog_id)
	{
		return $this->getDbTable()->delete(array('blog_post_id = ?' => $blog_id));
	}

	public function fetchStringList($blog_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('bt'=>$this->_table_prefix.'blog_tags'),'*')
			->order(array('bt.title ASC'));
		if ($blog_id) {
			$select->joinLeft(array('bpt'=>$this->_table_prefix.'blog_post_tag'),'bpt.blog_tag_id = bt.id','')
				->where("bpt.blog_post_id =?",$blog_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.$item->title.'"';
		}

		return implode(",",$all);
	}

	public function fetchJSON($blog_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('bt'=>$this->_table_prefix.'blog_tags'),'*')
			->order(array('bt.title ASC'));
		if ($blog_id) {
			$select->joinLeft(array('bpt'=>$this->_table_prefix.'blog_post_tag'),'bpt.blog_tag_id = bt.id','')
				->where("bpt.blog_post_id =?",$blog_id);
		}
		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = array("id"=>$item->id,"text"=>$item->title);
		}

		return json_encode($all);
	}
}
