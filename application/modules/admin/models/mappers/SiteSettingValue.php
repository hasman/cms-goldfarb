<?php
class Admin_Model_Mapper_SiteSettingValue extends Application_Model_Mapper_SiteSettingValue
{
	public function findByCode($code)
	{
		$select = $this->getDbTable()->select()
			->where('code = ?', $code);
		$result = $this->fetchAll($select);

		if (count($result) == 0) return new Application_Model_SiteSettingValue();

		$row = current($result);
		return new $this->_entity($row->toArray());
	}
}
