<?php
class Admin_Model_Mapper_Customer extends Application_Model_Mapper_Customer
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('c'=>$this->_table_prefix.'customers'),'*')
			->joinLeft(array('cab'=>$this->_table_prefix.'customer_addresses'),'cab.customer_id = c.id AND cab.address_type="billing" AND cab.default_address = "Yes" ',array('billing_first_name'=>'first_name','billing_last_name'=>'last_name','billing_company'=>'company','billing_address1'=>'address1','billing_address2'=>'address2','billing_city'=>'city','billing_region'=>'region','billing_postcode'=>'postcode','billing_country'=>'country','billing_phone'=>'phone'))
			->joinLeft(array('cas'=>$this->_table_prefix.'customer_addresses'),'cas.customer_id = c.id AND cas.address_type="shipping" AND cas.default_address = "Yes" ',array('shipping_first_name'=>'first_name','shipping_last_name'=>'last_name','shipping_company'=>'company','shipping_address1'=>'address1','shipping_address2'=>'address2','shipping_city'=>'city','shipping_region'=>'region','shipping_postcode'=>'postcode','shipping_country'=>'country','shipping_phone'=>'phone'))
			;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('c.first_name '.$match.' OR
				                            c.last_name '.$match.' OR
				                            c.email '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('c.lastname '.$match);
							break;
						case 'status':
							$select->where('c.status =?', $val);
							break;
						default:
							$select->where('c.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function customerSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('o'=>$this->_table_prefix.'orders'),array('customer_id','email'))
			->joinLeft(array('ob'=>$this->_table_prefix.'order_addresses'),'o.id = ob.order_id AND ob.type = "order"',array(
				'billing_firstname' => 'first_name',
				'billing_lastname' => 'last_name',
				'billing_company' => 'company',
				'billing_address1' => 'address1',
				'billing_address2' => 'address2',
				'billing_city' => 'city',
				'billing_region' => 'region',
				'billing_postcode' => 'postcode',
				'billing_country' => 'country',
				'billing_phone' => 'phone_day',
			))
			->joinLeft(array('os'=>$this->_table_prefix.'order_addresses'),'o.id = os.order_id AND os.type = "shipping"',array(
				'shipping_firstname' => 'first_name',
				'shipping_lastname' => 'last_name',
				'shipping_company' => 'company',
				'shipping_address1' => 'address1',
				'shipping_address2' => 'address2',
				'shipping_city' => 'city',
				'shipping_region' => 'region',
				'shipping_postcode' => 'postcode',
				'shipping_country' => 'country',
				'shipping_phone' => 'phone_day',
			))
			->distinct()
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('ob.first_name '.$match.' OR
				                            ob.last_name '.$match.' OR
				                            ob.company '.$match.' OR
				                            os.firstname '.$match.' OR
				                            os.lastname '.$match.' OR
				                            os.company '.$match.' OR
				                            o.email '.$match.'');
							break;
						default:
							$select->where('o.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}
}
