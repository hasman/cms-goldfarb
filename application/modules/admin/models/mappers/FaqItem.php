<?php
class Admin_Model_Mapper_FaqItem extends Application_Model_Mapper_FaqItem
{
	public function fetchBySectionId($section_id = 0)
	{
		$select = $this->getDbTable()->select()->where("faq_section_id = ?", $section_id)
			->order('sort_order ASC');

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('i'=>$this->_table_prefix.'faq_items'),'*')
			->joinLeft(array('s'=>$this->_table_prefix.'faq_sections'), 'i.faq_section_id = s.id', array('section_title'=>'title'))
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('i.question'.$match);
							break;
						case 'section_id':
							$select->where('i.faq_section_id = ?',$val);
							break;
						default:
							$select->where('i.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}
}
