<?php
class Admin_Model_Mapper_BlogPostTag extends Application_Model_Mapper_BlogPostTag
{
	public function deleteByBlogId($blog)
	{
		return $this->getDbTable()->delete(array('blog_post_id = ?' => $blog));
	}

	public function fetchAllByBlogId($blog_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('bpt'=>$this->_table_prefix.'blog_post_tag'),'*')
			->joinLeft(array('bt'=>$this->_table_prefix.'blog_tags'),'bpt.blog_tag_id = bt.id',array('title'=>'title','code'=>'code','status'=>'status'))
			->where("bpt.blog_post_id = ?",$blog_id);

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}
}
