<?php
class Admin_Model_Mapper_VideoPlatformVideoMonetizationLink extends Application_Model_Mapper_VideoPlatformVideoMonetizationLink
{
	public function fetchByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()
			->where("video_platform_video_id = ?", $video_id)
			->order("sort_order")
		;

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}
}
