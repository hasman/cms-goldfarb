<?php
class Admin_Model_Mapper_VideoPlatformVideoCategory extends Application_Model_Mapper_VideoPlatformVideoCategory
{
	public function deleteByVideoId($video_id)
	{
		return $this->getDbTable()->delete(array('video_platform_video_id = ?' => $video_id));
	}

	public function fetchVideosByCategoryId($category_id, $sort_type = null, $limit = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pc'=>$this->_table_prefix."video_platform_video_categories"),array('video_platform_category_id'=>'id'))
			->joinLeft(array('p'=>$this->_table_prefix.'video_platform_videos'),'pc.video_platform_video_id = p.id', '*')
			->where("pc.video_platform_category_id = ?", $category_id)
			->order('p.title ASC');
		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}

	public function fetchCategoryNamesByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pc'=>$this->_table_prefix.'video_platform_video_categories'))
			->where("pc.video_platform_video_id = ?", $video_id)
			->joinLeft(array('c'=>$this->_table_prefix.'video_platform_categories'),'pc.video_platform_category_id = c.id','name')
			->order(array('c.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function save($entity)
	{
		$select = $this->getDbTable()->select()
			->where("video_platform_category_id = ?", $entity->video_platform_category_id)
			->where("video_platform_video_id = ?", $entity->video_platform_video_id)
		;
		$row = $this->getDbTable()->fetchRow($select);

		if ($row) {
			$output = $row;
		} else {
			$output = parent::save($entity);
		}

		return $output;
	}

}
