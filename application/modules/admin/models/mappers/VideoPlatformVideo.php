<?php
class Admin_Model_Mapper_VideoPlatformVideo extends Application_Model_Mapper_VideoPlatformVideo
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('v'=>$this->_table_prefix.'video_platform_videos'),'*')
			->joinLeft(array('vs'=>$this->_table_prefix.'video_platform_video_streams'), 'vs.video_platform_video_id = v.id', array('stream_transcode_status'=>'transcode_status'))
			->columns(array('has_video'=>new Zend_Db_Expr("IF ((v.embed_code IS NULL || v.embed_code = '') AND (v.embed_url IS NULL || v.embed_url = '') AND (v.filename IS NULL or v.filename = ''), 'No', 'Yes')")))
			->group('v.id')
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('v.filename '.$match.' OR
											v.title '.$match.' OR
				                            v.code '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('v.title '.$match);
							break;
						case 'status':
							$select->where('v.status = ?', $val);
							break;
						case 'has_video':
							$select->having('has_video = ?', $val);
							break;
						case 'transcode_status':
							$select->where('v.transcode_status = ?', $val);
							break;
						case 'stream_transcode_status':
							$select->where('vs.stream_transcode_status = ?', $val);
							break;
						case 'admin_user_id':
							$select->where('v.admin_user_id = ?', $val);
							break;
						default:
							$select->where('v.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}

	public function createVideoHash()
	{
		$valid = false;
		while ($valid === false) {
			$code = sha1(uniqid(rand()));
			if ($this->doesExists(array('video_hash'=>$code)) === false) {
				$valid = true;
			}else {
				$valid = false;
			}
		}

		return $code;
	}
}
