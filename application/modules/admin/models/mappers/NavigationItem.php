<?php
class Admin_Model_Mapper_NavigationItem extends Application_Model_Mapper_NavigationItem
{
	public function mainNavArray()
	{
		$select = $this->getDbTable()->select()
			//->where('status = ?','Enabled')
			->where('main_nav_status = ?','Enabled')
			//->where('main_nav_parent_page_id = ?',0)
			->order('main_nav_sort_order')
		;
		$results = $this->fetchAll($select);

		$rows = [];
		foreach ($results as $result) {
			$rows[] = $result->toArray();
		}

		return parent::buildTree($rows);
	}

	public function mainNavPagePairs($whole_row = false)
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('main_nav_status = ?','Enabled')
			->where('main_nav_parent_id IS NULL OR main_nav_parent_id = 0')
			->order('title')
		;
		$results = $this->fetchAll($select);

		if ($whole_row) {
			return $results;
		}

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->id] = $row->title;
		}

		return $pairs;
	}

	public function setMainNavSortOrder($id, $sort = null)
	{
		$data = array(
			'main_nav_sort_order' => ($sort)?$sort:99999,
		);

		if ($id) {
			$data['modified'] = date("Y-m-d H:i:s");
			$this->getDbTable()->update($data, array('id = ?' => $id));
		}

		return $id;
	}

	public function findByPageId($page_id)
	{
		$select = $this->getDbTable()->select()->where("page_id = ?", $page_id);
		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			return new Application_Model_NavigationItem($result->toArray());
		}else {
			return null;
		}
	}

	public function findByCategoryId($category_id)
	{
		$select = $this->getDbTable()->select()->where("category_id = ?", $category_id);
		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			return new Application_Model_NavigationItem($result->toArray());
		}else {
			return null;
		}
	}
}
