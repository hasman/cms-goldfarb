<?php
class Admin_Model_Mapper_ProductVariant extends Application_Model_Mapper_ProductVariant
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pv'=>$this->_table_prefix.'product_variants'),'*')
			->joinLeft(array('pvt'=>$this->_table_prefix.'product_variant_types'), 'pv.product_variant_type_id = pvt.id', array('type'=>'title'))
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('pv.title '.$match.' OR
				                            pv.sku '.$match.' OR
				                            pv.barcode '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('pv.title_sort '.$match);
							break;
						case 'status':
							$select->where('pv.status =?', $val);
							break;
						default:
							$select->where('pv.id = ?', $val);
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function getVariantsByProductId($product_id, $product_variant_type_id = null, $status = null, $mask = null)
	{
		$select = $this->getDbTable()->select()->where("parent_product_id = ?", $product_id)
			->order('sort_order');

		if ($product_variant_type_id) {
			$select->where('product_variant_type_id = ?', $product_variant_type_id);
		}

		if ($status) {
			$select->where('status = ?', $status);
		}

		if ($mask) {
			if (strpos($mask,'"') !== false) {
				$match = '= "'.str_replace('"','',$mask).'"';
			}else {
				$match = 'LIKE "%'.addslashes($mask).'%"';
			}

			$select->where('title '.$match.' OR
								sku '.$match.' OR
								code '.$match);
		}

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}

	}
}
