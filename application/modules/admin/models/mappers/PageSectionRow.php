<?php
class Admin_Model_Mapper_PageSectionRow extends Application_Model_Mapper_PageSectionRow
{
	public function isEditable($row_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('p'=>$this->_table_prefix.'pages'),'editable')
			->from(array('ps'=>$this->_table_prefix.'page_sections'),null)
			->from(array('psr'=>$this->_table_prefix.'page_section_rows'),null)
			->where('ps.page_id = p.id')
			->where('psr.page_section_id = ps.id')
			->where('psr.id = ?', $row_id)
		;
		$row = $this->getDbTable()->fetchRow($select);
		if ($row['editable'] == 'Yes' || $row['editable'] == 'Section' || $row['editable'] == 'Row') return true;

		return false;
	}

	public function addRow($section_id, $prev_id = 0)
	{
		$row = parent::find($prev_id);
		$prev_sort = ($row)?$row->sort_order:0;

		$this->getDbTable()->update(array('sort_order'=>new Zend_Db_Expr('sort_order +1')),"page_section_id = '$section_id' AND sort_order > '$prev_sort'");

		return parent::save(new Application_Model_PageSectionRow(array('page_section_id'=>$section_id,'sort_order'=>($row)?$prev_sort+1:0)));
	}

	public function deleteRow($row_id)
	{
		$this->getDbTable()->delete(array('id = ?' => $row_id));

		//delete row columns
		$mapper = new Admin_Model_Mapper_PageSectionRowColumn();
		$mapper->deleteRowColumns($row_id);
	}

	public function getRowFromColumnId($column_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('psr'=>$this->_table_prefix.'page_section_rows'),'id')
			->joinLeft(array('psrc'=>$this->_table_prefix.'page_section_row_columns'),'psrc.page_section_row_id = psr.id',null)
			->where('psrc.id = ?',$column_id)
		;
		$row = $this->getDbTable()->fetchRow($select);

		return $row->id;
	}

	public function cloneRow($clone_id)
	{
		$row = parent::find($clone_id);
		$row = $row->toArray();
		$row_clone_id = $row['id'];
		unset($row['id']);
		$row['sort_order'] = $row['sort_order'] + 1;
		$row['created'] = $row['modified'] = new Zend_Db_Expr("NOW()");

		$row_id = parent::save(new Application_Model_PageSectionRow($row));

		//clone columns
		$mapper = new Admin_Model_Mapper_PageSectionRowColumn();
		$mapper->cloneRowColumns($row_id, $row_clone_id);

		return $row_id;
	}

	public function cloneRows($section_id, $clone_id)
	{
		$rows = parent::getPageSectionRows($clone_id);
		if (!empty($rows)) {
			foreach ($rows as $row) {
				$row = $row->toArray();
				$row_clone_id = $row['id'];
				unset($row['id']);
				$row['page_section_id'] = $section_id;
				$row['created'] = $row['modified'] = new Zend_Db_Expr("NOW()");

				$row_id = parent::save(new Application_Model_PageSectionRow($row));

				//clone columns
				$mapper = new Admin_Model_Mapper_PageSectionRowColumn();
				$mapper->cloneRowColumns($row_id, $row_clone_id);
			}
		}
	}
}
