<?php
class Admin_Model_Mapper_BlogPost extends Application_Model_Mapper_BlogPost
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('ba'=>$this->_table_prefix.'blog_posts'),'*')
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('ba.code '.$match.' OR
										ba.title '.$match);
							break;
						case 'title':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('ba.title '.$match);
							break;
						case 'code':
							$select->where('ba.code '.$match);
							break;
						case 'status':
							$select->where('ba.status =?', $val);
							break;
						default:
							$select->where('ba.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function checkDuplicate($code, $id=null)
	{
		if (!$code) return true;

		$select = $this->getDbTable()->select()->where("code = ?",$code);
		if ($id) {
			$select->where("id <> ?",$id);
		}

		if ($this->fetchAll($select)) {
			return true;
		}
		return false;
	}
}
