<?php
class Admin_Model_Mapper_PageSectionRowColumn extends Application_Model_Mapper_PageSectionRowColumn
{
	public function deleteRowColumns($row_id)
	{
		$columns = parent::getPageSectionRowColumns($row_id);
		$this->getDbTable()->delete(array('page_section_row_id = ?' => $row_id));

		//delete column modules
		$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
		foreach ($columns as $column) {
			$mapper->deleteColumnModules($column->id);
		}
	}

	public function cloneRowColumns($row_id, $clone_id)
	{
		$columns = parent::getPageSectionRowColumns($clone_id);
		if (!empty($columns)) {
			foreach ($columns as $column) {
				$column = $column->toArray();
				$column_clone_id = $column['id'];
				unset($column['id']);
				$column['page_section_row_id'] = $row_id;
				$column['created'] = $row['modified'] = new Zend_Db_Expr("NOW()");

				$column_id = parent::save(new Application_Model_PageSectionRowColumn($column));

				//clone modules
				$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
				$mapper->cloneRowColumnModules($column_id, $column_clone_id);
			}
		}
	}
}
