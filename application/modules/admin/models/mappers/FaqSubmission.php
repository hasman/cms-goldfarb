<?php
class Admin_Model_Mapper_FaqSubmission extends Application_Model_Mapper_FaqSubmission
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('i'=>$this->_table_prefix.'faq_submissions'),'*')
			->joinLeft(array('s'=>$this->_table_prefix.'faq_sections'), 'i.faq_section_id = s.id', array('section_title'=>'title'))
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('i.question '.$match.' OR
										i.name '.$match.' OR
										i.email '.$match);
							break;
						case 'section_id':
							$select->where('i.faq_section_id = ?',$val);
							break;
						case 'is_spam':
							$select->where('i.is_spam = ?',$val);
							break;
						default:
							$select->where('i.id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}
}
