<?php
class Admin_Model_Mapper_VideoPlatformImageTag extends Application_Model_Mapper_VideoPlatformImageTag
{
	public function fetchOrdered()
	{
		$select = $this->getDbTable()->select()->order(array('title ASC'));
		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}

	public function fetchByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('vt'=>$this->_table_prefix.'video_platform_video_image_tag'),'*')
			->where("vt.video_platform_video_id = ?", $video_id)
			->joinLeft(array('vtags'=>$this->_table_prefix.'video_platform_image_tags'),'vt.video_platform_image_tag_id = vtags.id','')
			->order(array('vtags.title ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function doesTagExist($tag_title)
	{
		$select = $this->getDbTable()->select()
			->where("title = ?", $tag_title)
		;

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function fetchTagNamesByVideoImageId($image_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('vt'=>$this->_table_prefix.'video_platform_video_image_tag'),'')
			->where("vt.video_platform_video_image_id = ?", $image_id)
			->joinLeft(array('vtags'=>$this->_table_prefix.'video_platform_image_tags'),'vt.video_platform_image_tag_id = vtags.id','title')
			->order(array('vtags.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function deleteByVideoId($video_id)
	{
		return $this->getDbTable()->delete(array('video_platform_video_id = ?' => $video_id));
	}

	public function fetchStringList($video_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('vtags'=>$this->_table_prefix.'video_platform_image_tags'),'*')
			->order(array('vtags.title ASC'));
		if ($video_id) {
			$select->joinLeft(array('vt'=>$this->_table_prefix.'video_platform_video_image_tag'),'vt.video_platform_image_tag_id = vtags.id','')
				->where("vt.video_platform_video_id =?",$video_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.$item->title.'"';
		}

		return implode(",",$all);
	}

	public function fetchJSON($video_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('vtags'=>$this->_table_prefix.'video_platform_image_tags'),'*')
			->order(array('tags.title ASC'));
		if ($video_id) {
			$select->joinLeft(array('vt'=>$this->_table_prefix.'video_platform_video_image_tag'),'vt.video_platform_image_tag_id = vtags.id','')
				->where("vt.video_platform_video_id =?",$video_id);
		}
		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = array("id"=>$item->id,"text"=>$item->title);
		}

		return json_encode($all);
	}

}
