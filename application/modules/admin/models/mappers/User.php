<?php
class Admin_Model_Mapper_User extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Admin_Model_DbTable_User');
		$this->setEntity('Admin_Model_User');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function checkDuplicate($email, $id=null)
	{
		$select = $this->getDbTable()->select()->where("email = ?",$email);
		if ($id) {
			$select->where("id <> ?",$id);
		}
		if ($this->fetchAll($select)) {
			return true;
		}
		return false;
	}

	public function listAll(){
		$select = $this->getDbTable()->select()->order('name');
		return $this->fetchAll($select);
	}

	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from($this->_table_prefix.'admin_users','*');

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'role':
							$select->where('role =?',$val);
							break;
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('username '.$match.' OR
				                            email '.$match.' OR
				                            first_name '.$match.' OR
				                            last_name '.$match);
							break;
						case 'username':
							$select->where('username '.$match.'');
							break;
						case 'email':
							$select->where('email '.$match.'');
							break;
						case 'first_name':
							$select->where('first_name '.$match.'');
							break;
						case 'last_name':
							$select->where('last_name '.$match.'');
							break;
						case 'status':
							$select->where('status =?', $val);
							break;
						default:
							$select->where('email '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function findAvatarByEmail($email)
	{
		$select = $this->getDbTable()->select()->where("email = ?",$email);
		if ($row = $this->fetchAll($select)) {
			return $row[0]->avatar;
		}
		return false;
	}
}
