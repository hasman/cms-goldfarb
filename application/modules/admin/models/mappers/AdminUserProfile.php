<?php
class Admin_Model_Mapper_AdminUserProfile extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Admin_Model_DbTable_AdminUserProfile');
		$this->setEntity('Admin_Model_AdminUserProfile');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByUserId($user_id)
	{
		$select = $this->getDbTable()->select()->where("admin_user_id = ?", $user_id);
		$result = $this->getDbTable()->fetchAll($select);

		if ($result->count() > 0) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}
}
