<?php
class Admin_Model_Mapper_ImageTag extends Application_Model_Mapper_ImageTag
{
	public function fetchOrdered()
	{
		$select = $this->getDbTable()->select()->order(array('title ASC'));
		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}

	public function fetchTagNamesByProductImageId($image_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pit'=>$this->_table_prefix.'product_image_tag'))
			->where("product_image_id = ?", $image_id)
			->joinLeft(array('it'=>$this->_table_prefix.'image_tags'),'pit.image_tag_id = it.id','title')
			->order(array('it.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchTagNamesByProductVariantImageId($image_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pvit'=>$this->_table_prefix.'product_variant_image_tag'))
			->where("pvit.product_variant_image_id = ?", $image_id)
			->joinLeft(array('it'=>$this->_table_prefix.'image_tags'),'pvit.image_tag_id = it.id','title')
			->order(array('it.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function doesTagExist($tag_title)
	{
		$select = $this->getDbTable()->select()
			->where("title = ?", $tag_title)
		;

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function fetchStringList($type = 'product', $image_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('it'=>$this->_table_prefix.'image_tags'),'*')
			->order(array('it.title ASC'));
		if ($image_id && $type == 'product') {
			$select->joinLeft(array('pit'=>$this->_table_prefix.'product_image_tag'),'pit.image_tag_id = it.id','')
				->where("pit.product_image_id = ?",$image_id);
		}elseif ($image_id && $type == 'variant') {
			$select->joinLeft(array('pvit'=>$this->_table_prefix.'product_variant_image_tag'),'pvit.image_tag_id = it.id','')
				->where("pvit.product_variant_image_id = ?",$image_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.addslashes($item->title).'"';
		}

		return implode(",",$all);
	}

	public function fetchJSON($type = 'product', $image_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('it'=>$this->_table_prefix.'image_tags'),'*')
			->order(array('it.title ASC'));
		if ($image_id && $type == 'product') {
			$select->joinLeft(array('pit'=>$this->_table_prefix.'product_image_tag'),'pit.image_tag_id = it.id','')
				->where("pit.product_image_id = ?",$image_id);
		}elseif ($image_id && $type == 'variant') {
			$select->joinLeft(array('pvit'=>$this->_table_prefix.'product_variant_image_tag'),'pvit.image_tag_id = it.id','')
				->where("pvit.product_variant_image_id = ?",$image_id);
		}
		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = array("id"=>$item->id,"text"=>$item->title);
		}

		return json_encode($all);
	}
}
