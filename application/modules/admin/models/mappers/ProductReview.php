<?php
class Admin_Model_Mapper_ProductReview extends Application_Model_Mapper_ProductReview
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pr'=>$this->_table_prefix.'product_reviews'),'*')
			->joinLeft(array('p'=>'products'),'pr.product_id = p.id', array('product_code'=>'code'))
			->joinLeft(array('pv'=>'product_variants'),'pr.product_variant_id = pv.id', array('variant_code'=>'code'))
			->columns(array('product_title' => new Zend_Db_Expr("IF (pr.product_variant_id, pv.title, p.title)"), 'product_sku' => new Zend_Db_Expr("IF (pr.product_variant_id, pv.sku, p.sku)")) )
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('name '.$match);
							break;
						case 'status':
							$select->where('status =?', $val);
							break;
						default:
							$select->where('id =?', $val);
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}
}
