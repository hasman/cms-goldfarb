<?php
class Admin_Model_Mapper_ProductTag extends Application_Model_Mapper_ProductTag
{
	public function deleteByProductId($product_id)
	{
		return $this->getDbTable()->delete(array('product_id = ?' => $product_id));
	}
}
