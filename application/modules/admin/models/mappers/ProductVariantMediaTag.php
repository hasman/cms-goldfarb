<?php
class Admin_Model_Mapper_ProductVariantMediaTag extends Application_Model_Mapper_ProductVariantMediaTag
{
	public function deleteByProductVariantMediaId($media_id)
	{
		return $this->getDbTable()->delete(array('product_variant_media_id = ?' => $media_id));
	}
}
