<?php
class Admin_Model_Mapper_ProductTags extends Application_Model_Mapper_ProductTags
{
	public function fetchOrdered()
	{
		$select = $this->getDbTable()->select()->order(array('title ASC'));
		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}

	public function fetchByProductId($product_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pt'=>$this->_table_prefix.'product_tag'),'*')
			->where("pt.product_id = ?", $product_id)
			->joinLeft(array('ptags'=>$this->_table_prefix.'product_tags'),'pt.product_tag_id = ptags.id','')
			->order(array('ptags.title ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function doesTagExist($tag_title)
	{
		$select = $this->getDbTable()->select()
			->where("title = ?", $tag_title)
		;

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function fetchTagNamesByProductId($product_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pt'=>$this->_table_prefix.'product_tag'), '')
			->where("pt.product_id = ?", $product_id)
			->joinLeft(array('ptags'=>$this->_table_prefix.'product_tags'),'pt.product_tag_id = ptags.id','title')
			->order(array('ptags.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function deleteByProductId($product_id)
	{
		return $this->getDbTable()->delete(array('product_id = ?' => $product_id));
	}

	public function fetchStringList($product_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('ptags'=>$this->_table_prefix.'product_tags'),'*')
			->order(array('ptags.title ASC'));
		if ($product_id) {
			$select->joinLeft(array('pt'=>$this->_table_prefix.'product_tag'),'pt.product_tag_id = ptags.id','')
				->where("pt.product_id =?",$product_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.$item->title.'"';
		}

		return implode(",",$all);
	}

	public function fetchJSON($product_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('ptags'=>$this->_table_prefix.'product_tags'),'*')
			->order(array('tags.title ASC'));
		if ($product_id) {
			$select->joinLeft(array('pt'=>$this->_table_prefix.'product_tag'),'pt.product_tag_id = ptags.id','')
				->where("pt.product_id =?",$product_id);
		}
		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = array("id"=>$item->id,"text"=>$item->title);
		}

		return json_encode($all);
	}

	public function fetchTagNamesByCategoryId($category)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pc'=>$this->_table_prefix.'product_categories'),'')
			->where("pc.category_id = ?", $category)
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'pc.product_id = p.id','')
			->joinLeft(array('pt'=>$this->_table_prefix.'product_tag'),'pt.product_id = p.id','')
			->joinLeft(array('ptags'=>$this->_table_prefix.'product_tags'),'pt.product_tag_id = ptags.id',array('id','title'))
			->order(array('ptags.title ASC'))
			->distinct();

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[$row->id] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchTagNamesByCollectionId($collection)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pc'=>$this->_table_prefix.'product_collections'),'')
			->where("pc.collection_id = ?", $collection)
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'pc.product_id = p.id','')
			->joinLeft(array('pt'=>$this->_table_prefix.'product_tag'),'pt.product_id = p.id','')
			->joinLeft(array('ptags'=>$this->_table_prefix.'product_tags'),'pt.product_tag_id = ptags.id',array('id','title'))
			->order(array('ptags.title ASC'))
			->distinct();

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[$row->id] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}
}
