<?php
class Admin_Model_Mapper_Category extends Application_Model_Mapper_Category
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select();

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('name '.$match.' OR
				                            code '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('name '.$match);
							break;
						case 'parent_id':
							$select->where('parent_id = ?', $val);
							break;
						case 'status':
							$select->where('status = ?', $val);
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchIdPairs($top_level_only = false, $ignore_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('c'=>$this->_table_prefix.'categories'),'*')
			->columns(array('parent_name'=>"(SELECT NAME FROM {$this->_table_prefix}categories WHERE c.parent_id = categories.id)"))
			->order('name')
		;

		if ($top_level_only) {
			$select->where('parent_id = ?', 0);
		}
		if ($ignore_id) {
			$select->where('id <> ?', $ignore_id);
		}

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$name = '';
			if ($row->parent_name) {
				$name .= $row->parent_name." -> ";
			}
			$name .= $row->name;

			$pairs[$row->id] = $name;
		}

		return $pairs;
	}

	public function fetchCodePairs($top_level_only = false, $ignore_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('c'=>$this->_table_prefix.'categories'),'*')
			->columns(array('parent_name'=>"(SELECT NAME FROM {$this->_table_prefix}categories WHERE c.parent_id = categories.id)"))
			->order('name')
		;

		if ($top_level_only) {
			$select->where('parent_id = ?', 0);
		}
		if ($ignore_id) {
			$select->where('id <> ?', $ignore_id);
		}

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$name = '';
			if ($row->parent_name) {
				$name .= $row->parent_name." -> ";
			}
			$name .= $row->name;

			$pairs[$row->code] = $name;
		}

		return $pairs;
	}

	public function fetchStringList($product_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('c'=>$this->_table_prefix.'categories'),'*')
			->columns(array('parent_name'=>"(SELECT NAME FROM {$this->_table_prefix}categories WHERE c.parent_id = categories.id)"))
			->order(array('c.name ASC'));
		if ($product_id) {
			$select->joinLeft(array('pc'=>$this->_table_prefix.'product_categories'),'pc.category_id = c.id','')
				->joinLeft(array('p'=>$this->_table_prefix.'products'),'pc.product_id = p.id', '')
				->where("pc.product_id = ?",$product_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$name = '';
			if ($item->parent_name) {
				$name .= $item->parent_name." -> ";
			}
			$name .= $item->name;

			$all[] = '"'.$name.'"';
		}

		return implode(",",$all);
	}

	public function fetchJSON($product_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('c'=>$this->_table_prefix.'categories'),'*')
			->columns(array('parent_name'=>"(SELECT NAME FROM {$this->_table_prefix}categories WHERE c.parent_id = categories.id)"))
			->order(array('c.name ASC'));
		if ($product_id) {
			$select->joinLeft(array('pc'=>$this->_table_prefix.'product_categories'),'pc.category_id = c.id','')
				->joinLeft(array('p'=>$this->_table_prefix.'products'),'pc.product_id = p.id', '')
				->where("pc.product_id = ?",$product_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$name = '';
			if ($item->parent_name) {
				$name .= $item->parent_name." -> ";
			}
			$name .= $item->name;

			$all[] = array("id"=>$item->id,"text"=>$name);
		}

		return json_encode($all);
	}

	public function nestedArray()
	{
		$select = $this->getDbTable()->select()
			//->where('status = ?','Enabled')
			->order('sort_order')
		;
		$results = $this->fetchAll($select);

		$rows = [];
		foreach ($results as $result) {
			$rows[] = $result->toArray();
		}

		return parent::buildTree($rows);
	}
}
