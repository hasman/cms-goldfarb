<?php
class Admin_Model_Mapper_LibraryModule extends Application_Model_Mapper_LibraryModule
{
	public function listAll(){
		$select = $this->getDbTable()->select()->order('label');
		return $this->fetchAll($select);
	}

	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select();

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (!is_array($val)) {
						if (strpos($val, '"') !== false) {
							$match = '= "' . str_replace('"', '', $val) . '"';
						} else {
							$match = 'LIKE "%' . addslashes($val) . '%"';
						}
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('label '.$match);
							break;
						case 'type':
							$select->where('type =?', $val);
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}

		return $select;
	}
}
