<?php
class Admin_Model_Mapper_VideoPlatformCategory extends Application_Model_Mapper_VideoPlatformCategory
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select();

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('title '.$match.' OR
				                            code '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('title '.$match);
							break;
						case 'status':
							$select->where('status = ?', $val);
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchIdPairs($ignore_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('c'=>$this->_table_prefix.'video_platform_categories'),'*')
			->order('title')
		;

		if ($ignore_id) {
			$select->where('id <> ?', $ignore_id);
		}

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->id] = $row->title;
		}

		return $pairs;
	}

	public function fetchCodePairs($ignore_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('c'=>$this->_table_prefix.'video_platform_categories'),'*')
			->order('title')
		;

		if ($ignore_id) {
			$select->where('id <> ?', $ignore_id);
		}

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->code] = $row->title;
		}

		return $pairs;
	}

	public function fetchStringList($video_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('c'=>$this->_table_prefix.'video_platform_categories'),'*')
			->order(array('c.title ASC'));
		if ($video_id) {
			$select->joinLeft(array('pc'=>$this->_table_prefix.'video_platform_video_categories'),'pc.video_platform_category_id = c.id','')
				->joinLeft(array('p'=>$this->_table_prefix.'video_platform_videos'),'pc.video_platform_video_id = p.id', '')
				->where("pc.video_platform_video_id = ?",$video_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$name = $item->title;

			$all[] = '"'.$name.'"';
		}

		return implode(",",$all);
	}

	public function fetchJSON($video_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('c'=>$this->_table_prefix.'video_platform_categories'),'*')
			->order(array('c.title ASC'));
		if ($video_id) {
			$select->joinLeft(array('pc'=>$this->_table_prefix.'video_platform_video_categories'),'pc.video_platform_category_id = c.id','')
				->joinLeft(array('p'=>$this->_table_prefix.'video_platform_videos'),'pc.video_platform_video_id = p.id', '')
				->where("pc.video_platform_video_id = ?",$video_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$name = $item->title;

			$all[] = array("id"=>$item->id,"text"=>$name);
		}

		return json_encode($all);
	}

	public function doesCategoryExist($title)
	{
		$select = $this->getDbTable()->select()
			->where("title = ?", $title)
		;

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function fetchCategoryNamesByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('vt'=>$this->_table_prefix.'video_platform_video_categories'), '')
			->where("vt.video_platform_video_id = ?", $video_id)
			->joinLeft(array('vtags'=>$this->_table_prefix.'video_platform_categories'),'vt.video_platform_category_id = vtags.id','title')
			->order(array('vt.sort_order ASC', 'vtags.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchPairsByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('vt'=>$this->_table_prefix.'video_platform_video_categories'), '')
			->where("vt.video_platform_video_id = ?", $video_id)
			->joinLeft(array('vtags'=>$this->_table_prefix.'video_platform_categories'),'vt.video_platform_category_id = vtags.id',array('id','title'))
			->order(array('vt.sort_order ASC', 'vtags.title ASC'));

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->id] = $row->title;
		}

		return $pairs;
	}
}
