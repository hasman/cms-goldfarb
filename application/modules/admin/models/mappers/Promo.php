<?php
class Admin_Model_Mapper_Promo extends Application_Model_Mapper_Promo
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('p'=>$this->_table_prefix.'promos'),'*')
			->columns(array('used_cnt'=> new Zend_Db_Expr("
			(CASE p.trigger 
				WHEN 'single_use_code' THEN (SELECT COUNT(*) FROM {$this->_table_prefix}promo_single_codes AS psc WHERE psc.promo_id = p.id AND psc.used_timestamp IS NOT NULL)
			    WHEN 'code' THEN (SELECT COUNT(*) FROM {$this->_table_prefix}order_profiles AS op WHERE op.promo_code = p.code) 
			    WHEN 'auto' THEN (SELECT COUNT(*) FROM {$this->_table_prefix}order_profiles AS op WHERE op.promo_code = p.id)
			    END
			)")))
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('code '.$match.' OR
				                            title '.$match);
							break;
						case 'title':
							$select->where('title '.$match);
							break;
						case 'code':
							$select->where('code ='. $match);
							break;
						case 'type':
							$select->where('type =?', $val);
							break;
						case 'sitewide':
							$select->where('sitewide =?', $val);
							break;
						case 'start_date_from':
							$select->where('start_date >= ?',$val);
							break;
						case 'start_date_to':
							$select->where('start_date <= ?',$val);
							break;
						case 'end_date_from':
							$select->where('end_date >= ?',$val);
							break;
						case 'end_date_to':
							$select->where('end_date <= ?',$val);
							break;
						case 'alpha':
							if ($val == '.') {
								$select->where('UCASE(MID(title, 1, 1)) NOT BETWEEN "A" AND "Z"
								');
							}else {
								$match = 'LIKE "' . $val . '%"';
								$select->where('lastname '.$match);
							}
							break;
						case 'status':
							$select->where('status =?', $val);
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}
}
