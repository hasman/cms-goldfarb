<?php
class Admin_Model_Mapper_PromoThresholdProduct extends Application_Model_Mapper_PromoThresholdProduct
{
	public function fetchProductIdsByPromoThresholdId($promo_threshold_id, $type='include')
	{
		$select = $this->getDbTable()->select()->where("promo_threshold_id=?",$promo_threshold_id)->where('type =?',$type);
		$results = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($results as $row) {
			$array[$row->product_id] = $row->product_id;
		}

		return $array;
	}

	public function fetchProductsByPromoThresholdId($promo_threshold_id, $type='include')
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pp'=>$this->_table_prefix.'promo_threshold_products'),'')
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'pp.product_id = p.id',array('product_title'=>'title','product_id'=>'id', 'product_sku'=>'sku'))
			->joinLeft(array('pv'=>$this->_table_prefix.'product_variants'),'pp.variant_id = pv.id',array('variant_title'=>'title','variant_id'=>'id', 'variant_sku'=>'sku'))
			->joinLeft(array('pvt'=>$this->_table_prefix.'product_variant_types'),'pv.product_variant_type_id = pvt.id',array('type' => 'title', 'type_code' => 'code'))
			->where("promo_threshold_id=?",$promo_threshold_id)->where('pp.type =?',$type);
		$results = $this->getDbTable()->fetchAll($select);

		return $results;
	}

	public function deleteByPromoThresholdId($promo_threshold_id)
	{
		$this->getDbTable()->delete("promo_threshold_id = '$promo_threshold_id'");

		return;
	}
}
