<?php
class Admin_Model_Mapper_ProductImageTag extends Application_Model_Mapper_ProductImageTag
{
	public function deleteByProductImageId($image_id)
	{
		return $this->getDbTable()->delete(array('product_image_id = ?' => $image_id));
	}
}
