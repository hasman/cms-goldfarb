<?php
class Admin_Model_Mapper_Product extends Application_Model_Mapper_Product
{
	protected $events;

	public function events(Zend_EventManager_EventCollection $events = null)
	{
		if (null !== $events) {
			$this->events = $events;
		} elseif (null === $this->events) {
			$this->events = new Zend_EventManager_EventManager(__CLASS__);
		}
		return $this->events;
	}

	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('p'=>'products'), '*')
			->joinLeft(array('pc'=>'product_categories'), 'pc.product_id = p.id','')
			->joinLeft(array('pcol'=>'product_collections'), 'pcol.product_id = p.id','')
			->group('p.id')
		;

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('p.title '.$match.' OR
				                            p.sku '.$match.' OR
				                            p.barcode '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('p.title_sort '.$match);
							break;
						case 'status':
							$select->where('p.status =?', $val);
							break;
						case 'category_id':
							$select->where('pc.category_id = ?', $val);
							break;
						case 'collection_id':
							$select->where('pcol.collection_id = ?', $val);
							break;
						default:
							$select->where('p.id =?', $val);
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function save($entity)
	{
		$params = compact('entity');
		$this->events()->trigger(__FUNCTION__.'.pre', $this, $params);

		$output = parent::save($entity);

		$params = compact('entity', 'output');
		$this->events()->trigger(__FUNCTION__.'.post', $this, $params);

		return $output;
	}

}
