<?php
class Admin_Model_Mapper_VideoPlatformVideoStream extends Application_Model_Mapper_VideoPlatformVideoStream
{
	public function fetchProcessing()
	{
		$select = $this->getDbTable()->select()
			->where("transcode_status = ?", 'Processing')
		;

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}
}
