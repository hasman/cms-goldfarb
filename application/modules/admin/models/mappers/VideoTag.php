<?php
class Admin_Model_Mapper_VideoTag extends Application_Model_Mapper_VideoTag
{
	public function deleteByVideoId($video_id)
	{
		return $this->getDbTable()->delete(array('video_id = ?' => $video_id));
	}
}
