<?php
class Admin_Model_Mapper_TemplateBlock extends Application_Model_Mapper_TemplateBlock
{
	public function deleteByTemplateId($template_id)
	{
		return $this->getDbTable()->delete(array('template_id = ?' => $template_id));
	}
}
