<?php
class Admin_Model_Mapper_Font extends Application_Model_Mapper_Font
{
	public function getAllFonts()
	{
		$fonts = Zend_Registry::get('cms_fonts');
		if (!is_array($fonts)) {
			$fonts = $fonts->toArray();
		}

		$db_fonts = $this->fetchAll();
		foreach ($db_fonts as $row) {
			if (!array_key_exists($row->code,$fonts)) {
				$fonts[$row->code] = array(
					'title' => $row->title,
					'type' => $row->type,
					'url' => $row->url
				);
			}
		}

		return $fonts;
	}
}
