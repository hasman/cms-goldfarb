<?php
class Admin_Model_Mapper_PageSectionRowColumnModule extends Application_Model_Mapper_PageSectionRowColumnModule
{
	public function deleteModule($module_id)
	{
		$this->getDbTable()->delete(array('id = ?' => $module_id));
	}

	public function deleteColumnModules($column_id)
	{
		$this->getDbTable()->delete(array('page_section_row_column_id = ?' => $column_id));
	}

	public function cloneRowColumnModules($column_id, $clone_id)
	{
		$modules = parent::getPageSectionRowColumnModules($clone_id);

		if ($modules) {
			foreach ($modules as $module) {
				$module = $module->toArray();
				unset($module['id']);
				$module['page_section_row_column_id'] = $column_id;
				$module['created'] = $row['modified'] = new Zend_Db_Expr("NOW()");

				$module_id = parent::save(new Application_Model_PageSectionRowColumnModule($module));
			}
		}
	}

	public function cloneRowColumnModule($clone_id)
	{
		$clone_module = parent::find($clone_id);

		if ($clone_module) {
			$module = $clone_module->toArray();
			unset($module['id']);
			$module['sort_order'] = $module['sort_order'] +1;
			$module['created'] = $row['modified'] = new Zend_Db_Expr("NOW()");

			$module_id = parent::save(new Application_Model_PageSectionRowColumnModule($module));

			return $module_id;
		}
	}
}
