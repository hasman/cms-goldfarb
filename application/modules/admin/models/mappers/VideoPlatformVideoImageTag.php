<?php
class Admin_Model_Mapper_VideoPlatformVideoImageTag extends Application_Model_Mapper_VideoPlatformVideoImageTag
{
	public function deleteByVideoImageId($image_id)
	{
		return $this->getDbTable()->delete(array('video_platform_video_image_id = ?' => $image_id));
	}
}
