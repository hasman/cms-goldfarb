<?php
class Admin_Model_Mapper_VideoCategory extends Application_Model_Mapper_VideoCategory
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select();

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('name '.$match.' OR
				                            code '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('name '.$match);
							break;
						case 'status':
							$select->where('status = ?', $val);
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchOrdered()
	{
		$select = $this->getDbTable()->select()
			->order('sort_order ASC');

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()
			->order('name')
		;

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->id] = $row->name;
		}

		return $pairs;
	}

	public function fetchCodePairs()
	{
		$select = $this->getDbTable()->select()
			->order('name')
		;

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->code] = $row->name;
		}

		return $pairs;
	}

	public function fetchStringList($video_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('c'=>$this->_table_prefix.'video_categories'),'*')
			->order(array('c.name ASC'));
		if ($video_id) {
			$select->joinLeft(array('v'=>$this->_table_prefix.'videos'),'v.video_category_id = c.id', '')
				->where("v.id = ?",$video_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.$item->name.'"';
		}

		return implode(",",$all);
	}

	public function fetchJSON($video_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('c'=>$this->_table_prefix.'video_categories'),'*')
			->order(array('c.name ASC'));
		if ($video_id) {
			$select->joinLeft(array('v'=>$this->_table_prefix.'videos'),'v.video_category_id = c.id', '')
				->where("v.id = ?",$video_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = array("id"=>$item->id,"text"=>$item->name);
		}

		return json_encode($all);
	}
}
