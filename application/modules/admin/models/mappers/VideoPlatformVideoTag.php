<?php
class Admin_Model_Mapper_VideoPlatformVideoTag extends Application_Model_Mapper_VideoPlatformVideoTag
{
	public function deleteByVideoId($video_id)
	{
		return $this->getDbTable()->delete(array('video_platform_video_id = ?' => $video_id));
	}
}
