<?php
class Admin_Model_Mapper_PageSection extends Application_Model_Mapper_PageSection
{
	public function isEditable($section_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('p'=>$this->_table_prefix.'pages'),'editable')
			->from(array('ps'=>$this->_table_prefix.'page_sections'),null)
			->where('ps.page_id = p.id')
			->where('ps.id = ?', $section_id)
		;
		$row = $this->getDbTable()->fetchRow($select);
		if ($row['editable'] == 'Yes' || $row['editable'] == 'Section') return true;

		return false;
	}

	public function addSection($page_id, $type, $prev_id = 0)
	{
		$section = parent::find($prev_id);
		$prev_sort = ($section)?$section->sort_order:0;

		$this->getDbTable()->update(array('sort_order'=>new Zend_Db_Expr('sort_order +1')),"page_id = '$page_id' AND sort_order > '$prev_sort'");

		$section_id = parent::save(new Application_Model_PageSection(array('page_id'=>$page_id,'type'=>$type,'sort_order'=>($section)?$prev_sort+1:0)));

		//add default row
		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$mapper->addRow($section_id);

		return $section_id;
	}

	public function deleteSection($page_id, $id)
	{
		$this->getDbTable()->delete(array('id = ?' => $id, 'page_id = ?' => $page_id));

		//delete section rows
		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$rows = $mapper->getPageSectionRows($id);

		foreach ($rows as $row) {
			$mapper->deleteRow($row->id);
		}
	}

	public function cloneSection($clone_id)
	{
		$section = parent::find($clone_id);

		$this->getDbTable()->update(array('sort_order'=>new Zend_Db_Expr('sort_order +1')),"page_id = '{$section->page_id}' AND sort_order > '{$section->sort_order}'");

		$section_id = parent::save(new Application_Model_PageSection(array('page_id'=>$section->page_id,'type'=>$section->type,'class'=>$section->class,'json_settings'=>$section->json_settings,'json_styles'=>$section->json_styles,'desktop_status'=>$section->desktop_status,'tablet_status'=>$section->tablet_status,'mobile_status'=>$section->mobile_status,'sort_order'=>($section)?$section->sort_order+1:0)));

		//clone rows
		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$mapper->cloneRows($section_id, $clone_id);

		return $section_id;
	}

	public function deleteByPageId($page_id)
	{
		$sections = $this->getPageSections($page_id);

		if ($sections) {
			foreach ($sections as $section) {
				$this->deleteSection($page_id, $section->id);
			}
		}

		return true;
	}
}
