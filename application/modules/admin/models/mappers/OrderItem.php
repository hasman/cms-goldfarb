<?php
class Admin_Model_Mapper_OrderItem extends Application_Model_Mapper_OrderItem
{
	public function fulfillByOrderId($order_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$options = Zend_Registry::get('configuration')->toArray();
		$preorder_drop = $options['preorder']['drop'];
		if (!$preorder_drop) $preorder_drop = '0 DAY';

		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('oi'=>$this->_table_prefix."order_items"),"*")->where("oi.order_id = ?", $order_id)
			->joinLeft(array('p'=>$this->_table_prefix."products"),"oi.product_id = p.id",array("warehouse","weight"))
			->joinLeft(array('pv'=>$this->_table_prefix."product_variants"),"oi.variant_id = pv.id",array("variant_warehouse"=>"warehouse","variant_weight"=>"weight"))
			->where("(p.available_date <= DATE_ADD(NOW(), INTERVAL $preorder_drop) OR p.available_date IS NULL)")
			->where("(pv.available_date <= DATE_ADD(NOW(), INTERVAL $preorder_drop) OR pv.available_date IS NULL)")
			->where("oi.status = 'Order Taken'")
			;
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			foreach ($result as $product) {
				$db->update($this->_table_prefix."order_items",array("status"=>"Sending"),"id = '{$product['id']}'");
			}

			return $result;
		} else {
			return false;
		}
	}
}
