<?php
class Admin_Model_Mapper_MediaTag extends Application_Model_Mapper_MediaTag
{
	public function fetchOrdered()
	{
		$select = $this->getDbTable()->select()->order(array('title ASC'));
		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}

	public function fetchTagNamesByProductMediaId($media_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pit'=>$this->_table_prefix.'product_media_tag'))
			->where("product_media_id = ?", $media_id)
			->joinLeft(array('it'=>$this->_table_prefix.'media_tags'),'pit.media_tag_id = it.id','title')
			->order(array('it.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchTagNamesByProductVariantMediaId($media_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pvit'=>$this->_table_prefix.'product_variant_media_tag'))
			->where("pvit.product_variant_media_id = ?", $media_id)
			->joinLeft(array('it'=>$this->_table_prefix.'media_tags'),'pvit.media_tag_id = it.id','title')
			->order(array('it.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function doesTagExist($tag_title)
	{
		$select = $this->getDbTable()->select()
			->where("title = ?", $tag_title)
		;

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function fetchStringList($type = 'product', $media_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('it'=>$this->_table_prefix.'media_tags'),'*')
			->order(array('it.title ASC'));
		if ($media_id && $type == 'product') {
			$select->joinLeft(array('pit'=>$this->_table_prefix.'product_media_tag'),'pit.media_tag_id = it.id','')
				->where("pit.product_media_id = ?",$media_id);
		}elseif ($media_id && $type == 'variant') {
			$select->joinLeft(array('pvit'=>$this->_table_prefix.'product_variant_media_tag'),'pvit.media_tag_id = it.id','')
				->where("pvit.product_variant_media_id = ?",$media_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.addslashes($item->title).'"';
		}

		return implode(",",$all);
	}

	public function fetchJSON($type = 'product', $media_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('it'=>$this->_table_prefix.'media_tags'),'*')
			->order(array('it.title ASC'));
		if ($media_id && $type == 'product') {
			$select->joinLeft(array('pit'=>$this->_table_prefix.'product_media_tag'),'pit.media_tag_id = it.id','')
				->where("pit.product_media_id = ?",$media_id);
		}elseif ($media_id && $type == 'variant') {
			$select->joinLeft(array('pvit'=>$this->_table_prefix.'product_variant_media_tag'),'pvit.media_tag_id = it.id','')
				->where("pvit.product_variant_media_id = ?",$media_id);
		}
		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = array("id"=>$item->id,"text"=>$item->title);
		}

		return json_encode($all);
	}
}
