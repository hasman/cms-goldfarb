<?php
class Admin_Model_Mapper_ApiKey extends Api_Model_Mapper_ApiKey
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select();

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('title '.$match.'');
							break;
						case 'alpha':
							$match = 'LIKE "'.$val.'%"';
							$select->where('title '.$match);
							break;
						case 'status':
							$select->where('status =?', $val);
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function generateKey($length = 35)
	{
		// set list of valid chars to remove O,V letters to avoid confusion
		$valid_chars = array("0","1","2","3","4","5","6","7","8","9","0","a","b","c","d","e","f","g","h","j","k","l","m","n","p","q","r","s","t","u","w","x","y","z");;

		$code = '';
		for ($i=0; $i<$length; $i++) {
			$code .= $valid_chars[rand(0,count($valid_chars)-1)];
		}

		return $code;
	}
}
