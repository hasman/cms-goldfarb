<?php
class Admin_Model_Mapper_AdminUserLicense extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Admin_Model_DbTable_AdminUserLicense');
		$this->setEntity('Admin_Model_AdminUserLicense');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAllByUserId($user_id)
	{
		$select = $this->getDbTable()->select()->where("admin_user_id = ?",$user_id);
		$result = $this->fetchAll($select);

		return $result;
	}

	public function checkUpToDate($user_id)
	{
		$license_config = null;
		$license_path = APPLICATION_PATH . '/../public/license/config.json';
		if (file_exists($license_path)) {
			$license_config = json_decode(file_get_contents($license_path), TRUE);
		}
		if ($license_config) {
			foreach ($license_config as $license_type => $license) {
				$license = current($license);

				$select = $this->getDbTable()->select()->where("type = '$license_type' AND version = '{$license['version']}' AND admin_user_id = ?",$user_id);
				$lic_row = $this->fetchAll($select);

				if (!$lic_row) {
					return false;
				}
			}
		}

		return true;
	}

}
