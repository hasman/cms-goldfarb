<?php
class Admin_Model_Mapper_ProductMediaTag extends Application_Model_Mapper_ProductMediaTag
{
	public function deleteByProductMediaId($media_id)
	{
		return $this->getDbTable()->delete(array('product_media_id = ?' => $media_id));
	}
}
