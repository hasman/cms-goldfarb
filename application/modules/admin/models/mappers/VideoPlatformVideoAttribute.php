<?php
class Admin_Model_Mapper_VideoPlatformVideoAttribute extends Application_Model_Mapper_VideoPlatformVideoAttribute
{
	public function listSearch($sort = '', $dir = 'ASC', $search = ''){
		$select = $this->getDbTable()->select();

		if( !empty($search) ){
			foreach( $search AS $key=>$val ){
				if( !empty($val) ){
					if (strpos($val,'"') !== false) {
						$match = '= "'.str_replace('"','',$val).'"';
					}else {
						$match = 'LIKE "%'.addslashes($val).'%"';
					}

					switch($key){
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('title '.$match);
							break;
						default:
							$select->where('id '.$match.'');
							break;
					}
				}
			}
		}

		if( !empty($sort) ){
			if( $sort == 'status' ){
				$select->order(array($sort.' '.$dir));
			}else{
				$select->order($sort.' '.$dir);
			}
		}
//echo $select->__toString();exit;
		return $select;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()
			->order('title')
		;

		$results = $this->getDbTable()->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->id] = $row->title;
		}

		return $pairs;
	}
}
