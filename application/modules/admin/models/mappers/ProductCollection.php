<?php
class Admin_Model_Mapper_ProductCollection extends Application_Model_Mapper_ProductCollection
{
	public function deleteByProductId($product_id)
	{
		return $this->getDbTable()->delete(array('product_id = ?' => $product_id));
	}

	public function fetchProductsByCollectionId($collection_id, $sort_type = null, $limit = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pc'=>$this->_table_prefix."product_collections"),array('product_collection_id'=>'id'))
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'pc.product_id = p.id', '*')
			->where("pc.collection_id = ?", $collection_id)
			->order('p.title_sort ASC');
		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}

	public function fetchCollectionNamesByProductId($product_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('pc'=>$this->_table_prefix.'product_collections'))
			->where("pc.product_id = ?", $product_id)
			->joinLeft(array('c'=>$this->_table_prefix.'collections'),'pc.collection_id = c.id','name')
			->order(array('pc.primary_collection DESC','c.name ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->name;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function save($entity)
	{
		$select = $this->getDbTable()->select()
			->where("collection_id = ?", $entity->collection_id)
			->where("product_id = ?", $entity->product_id)
		;
		$row = $this->getDbTable()->fetchRow($select);

		if ($row) {
			$output = $row;
		} else {
			$output = parent::save($entity);
		}

		return $output;
	}

	public function deleteByCollectionIdTagId($collection_id, $tag_id)
	{
		$mapper = new Application_Model_Mapper_ProductTag();
		$products = $mapper->fetchProductsByTagId($tag_id);

		foreach ($products as $product) {
			$this->getDbTable()->delete(array("collection_id = '$collection_id' AND product_id = ?" => $product->id));
		}

		return true;
	}
}
