<?php
class Admin_Model_Mapper_PromoSingleCode extends Application_Model_Mapper_PromoSingleCode
{
	public function findByPromoId($promo_id)
	{
		$select = $this->getDbTable()->select()->from(array('psc'=>$this->_table_prefix."promo_single_codes"))->where("psc.promo_id = ?", $promo_id);

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function generateCode($length = 10)
	{
		// set list of valid chars to remove O,V letters to avoid confusion
		$valid_chars = array("0","1","2","3","4","5","6","7","8","9","0","A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","W","X","Y","Z");;

		$code = '';
		for ($i=0; $i<$length; $i++) {
			$code .= $valid_chars[rand(0,count($valid_chars)-1)];
		}

		return $code;
	}
}
