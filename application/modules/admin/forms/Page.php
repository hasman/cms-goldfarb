<?php
class Admin_Form_Page extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/default.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/cms/page-save' );
		$this->setAttrib( 'id', 'pageForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');
		$enum = array('Yes'=>'Yes','No'=>'No');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['created_by_user_id'] = $this->createElement( 'hidden' , 'created_by_user_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['editable'] = $this->createElement( 'hidden' , 'editable' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['block_type'] = $this->createElement( 'hidden' , 'block_type' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Page Title')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['code'] = $this->createElement( 'text', 'code' )
			->setLabel('URL Code')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		/*
		$elements['template'] = $this->createElement( 'select', 'template' )
			->setLabel('Template')
			->setMultiOptions($templates)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		*/
		$elements['meta_title'] = $this->createElement( 'text', 'meta_title' )
			->setLabel('Meta Page Title')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['meta_keywords'] = $this->createElement( 'text', 'meta_keywords' )
			->setLabel('Meta Keywords')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['meta_description'] = $this->createElement( 'textarea', 'meta_description' )
			->setLabel('Meta Description')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>4))
			->setDecorators( $decor_element );
		$elements['tracking_code'] = $this->createElement( 'textarea', 'tracking_code' )
			->setLabel('Campaign Tracking Code')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>4))
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
