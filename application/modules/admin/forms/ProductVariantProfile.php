<?php
class Admin_Form_ProductVariantProfile extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/variant-profile.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/product/variant-profile-save' );
		$this->setAttrib( 'id', 'variantProfileForm' );

		//Options
		$mapper = new Admin_Model_Mapper_ProductAttribute();
		$attributes = $mapper->fetchPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['product_variant_id'] = $this->createElement( 'hidden' , 'product_variant_id' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['product_attribute_id'] = $this->createElement( 'select', 'product_attribute_id' )
			->setLabel('Attribute')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setMultiOptions($attributes)
			//->setDecorators( $decor_element )
			->removeDecorator('label')->removeDecorator('HtmlTag')
		;

		/*
		$elements['value'] = $this->createElement( 'textarea', 'value' )
			->setLabel('Value')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control","rows"=>3))
			->setDecorators( $decor_element );
		*/
		$elements['value'] = $this->createElement( 'hidden' , 'value' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$this->addElements( $elements );
	}

}
