<?php
class Admin_Form_UserForm extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/user.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/users/save' );
		$this->setAttrib( 'id', 'UserForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');
		$acl_roles = array_merge(array(
			'admin' => 'Admin',
			//'guest' => 'Guest'
		), Zend_Registry::get('admin_acl_roles'));

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['first_name'] = $this->createElement( 'text', 'first_name' )
			->setLabel('First Name')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['last_name'] = $this->createElement( 'text', 'last_name' )
			->setLabel('Last Name')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		/*
		$elements['username'] = $this->createElement( 'text', 'username' )
			->setLabel('Username')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		*/

		$elements['email'] = $this->createElement( 'text', 'email' )
			->setLabel('Email')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['crop_data'] = $this->createElement( 'hidden' , 'crop_data' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['avatar'] = $this->createElement( 'hidden', 'avatar')->removeDecorator('label')->setRequired(false)->removeDecorator('HtmlTag');
		$elements['image'] = $this->createElement( 'hidden', 'image')->removeDecorator('label')->setRequired(false)->removeDecorator('HtmlTag');

		$elements['password'] = $this->createElement( 'text', 'password' )
			->setLabel('Password')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['role'] = $this->createElement( 'select', 'role' )
			->setLabel('Role')
			->setMultiOptions($acl_roles)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
