<?php
class Admin_Form_VideoPlatformVideoProfile extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/video-platform-video-profile.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/video-platform/profile-save' );
		$this->setAttrib( 'id', 'videoPlatformVideoProfileForm' );

		//Options
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoAttribute();
		$attributes = $mapper->fetchPairs();

		$visibility = array('Visible'=>'Visible','Hidden'=>'Hidden');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['video_platform_video_id'] = $this->createElement( 'hidden' , 'video_platform_video_id' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['video_platform_video_attribute_id'] = $this->createElement( 'select', 'video_platform_video_attribute_id' )
			->setLabel('Attribute')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setMultiOptions($attributes)
			//->setDecorators( $decor_element )
			->removeDecorator('label')->removeDecorator('HtmlTag')
		;

		/*
		$elements['value'] = $this->createElement( 'textarea', 'value' )
			->setLabel('Value')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control","rows"=>3))
			->setDecorators( $decor_element );
		*/
		$elements['value'] = $this->createElement( 'hidden' , 'value' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['options'] = $this->createElement( 'hidden' , 'options' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['visibility'] = $this->createElement( 'select', 'visibility' )
			->setLabel('Visibility')
			->setMultiOptions($visibility)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
