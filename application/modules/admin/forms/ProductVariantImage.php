<?php
class Admin_Form_ProductVariantImage extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/variant-image.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/product/variant-image-save' );
		$this->setAttrib( 'id', 'variantImageForm' );

		//Options
		$ny = array('No'=>'No', 'Yes'=>'Yes');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['product_variant_id'] = $this->createElement( 'hidden' , 'product_variant_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden' , 'sort_order' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['image_src'] = $this->createElement( 'text', 'image_src' )
			->setLabel('Attribute')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['alt_text'] = $this->createElement( 'text', 'alt_text' )
			->setLabel('Alt Text')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['primary'] = $this->createElement( 'select', 'primary' )
			->setLabel('Primary Image')
			->setMultiOptions($ny)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['secondary'] = $this->createElement( 'select', 'secondary' )
			->setLabel('Secondary Image<br/><small>(swap with primary on mouseover)</small>')
			->setMultiOptions($ny)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		//tags
		$elements['tags'] = $this->createElement( 'text', 'tags' )
			->setLabel('Tags')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
