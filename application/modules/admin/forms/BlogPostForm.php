<?php
class Admin_Form_BlogPostForm extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/blog-post.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/blog/save' );
		$this->setAttrib( 'id', 'blogForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');
		$enum = array('Yes'=>'Yes', 'No'=>'No');
		$date_formats = array(
			'F j, Y' => date('F j, Y'),
			'M. j, Y' => date('M. j, Y'),
			'F Y' => date('F Y'),
			'M. Y' => date('M. Y'),
			'j F Y' => date('j F Y'),
			'j M. Y' => date('j M. Y'),
			'Y' => date('Y'),
			'none' => "Don't Show Date"
		);


		//Form fields
		$elements['cmd'] = $this->createElement( 'hidden', 'cmd' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');
		$elements['id'] = $this->createElement( 'hidden', 'id' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');

		$elements['code'] = $this->createElement( 'text', 'code' )
			->setLabel('URL Code')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		/*
		$elements['external_url'] = $this->createElement( 'text', 'external_url' )
			->setLabel('External URL <br/><small>(to link directly to external post)</small>')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		*/

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Title')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['meta_keywords'] = $this->createElement( 'text', 'meta_keywords' )
			->setLabel('Meta Keywords')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['meta_description'] = $this->createElement( 'textarea', 'meta_description' )
			->setLabel('Meta Description')
			->setRequired(false)
			->setAttribs(array('class' => 'form-control','rows'=>4))
			->setDecorators( $decor_element );

		$elements['summary'] = $this->createElement( 'textarea', 'summary' )
			->setLabel('Short Summary')
			->setRequired(false)
			->setAttribs(array('class' => 'form-control','rows'=>2))
			->setDecorators( $decor_element );
		$elements['content'] = $this->createElement( 'textarea', 'content' )
			->setLabel('Content')
			->setRequired(false)
			->setAttribs(array('class' => 'form-control tinymce','rows'=>30))
			->setDecorators( $decor_element );

		$elements['external_url'] = $this->createElement( 'text', 'external_url' )
			->setLabel('External URL<br/><small>read more/continue will link to external site</small>')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setAttrib('placeholder', 'Enter URL to have read more/continue link go to external site instead of content page')
			->setDecorators( $decor_element );

		$elements['byline'] = $this->createElement( 'text', 'byline' )
			->setLabel('Author')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		//tags
		$elements['tags'] = $this->createElement( 'text', 'tags' )
			->setLabel('Tags')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );


		$elements['image'] = $this->createElement( 'hidden', 'image' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');

		$elements['image_alt_tag'] = $this->createElement( 'text', 'image_alt_tag' )
			->setLabel('Main Article Image Alt Tag')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['thumbnail'] = $this->createElement( 'hidden', 'thumbnail' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');

		$elements['thumbnail_alt_tag'] = $this->createElement( 'text', 'thumbnail_alt_tag' )
			->setLabel('Square Thumbnail Alt Tag')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['posted_date'] = $this->createElement( 'text', 'posted_date' )
			->setLabel('Posted Date')
			->setRequired(false)
			->setAttrib('class', 'form-control datepicker')
			->setDecorators( $decor_element );
		$elements['posted_date_format'] = $this->createElement( 'select', 'posted_date_format' )
			->setLabel('Posted Date Format<br/><small>Example using current date</small>')
			->setMultiOptions($date_formats)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements($elements);
	}

	public function loadDefaultDecorators() {
		parent::loadDefaultDecorators();

		// remove the 'fieldset' decorator from all subforms
		$subforms = $this->getSubForms();
		foreach($subforms as $subform) {
			$subform->removeDecorator('Fieldset');
			$subform->removeDecorator('DtDdWrapper');
			$subform->removeDecorator('label');
			$subform->removeDecorator('HtmlTag');
		}

		return $this;
	}
}
