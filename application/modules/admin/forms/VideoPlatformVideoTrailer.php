<?php
class Admin_Form_VideoPlatformVideoTrailer extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/video-platform-trailer.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/video-platform/trailer-save' );
		$this->setAttrib( 'id', 'videoPlatformTrailerForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');

		$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
		$videos = $mapper->fetchPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['video_platform_video_id'] = $this->createElement( 'hidden' , 'video_platform_video_id' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['filename'] = $this->createElement( 'hidden' , 'filename' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['filename_internal'] = $this->createElement( 'hidden' , 'filename_internal' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['filesize'] = $this->createElement( 'hidden' , 'filesize' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['mimetype'] = $this->createElement( 'hidden' , 'mimetype' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['etag'] = $this->createElement( 'hidden' , 'etag' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['transcode_status'] = $this->createElement( 'hidden' , 'transcode_status' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['trailer_video_platform_video_id'] = $this->createElement( 'select', 'trailer_video_platform_video_id' )
			->setLabel('Existing Video')
			->setMultiOptions(array(""=>"")+$videos)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['embed_url'] = $this->createElement( 'text', 'embed_url' )
			->setLabel('External Video URL')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['embed_code'] = $this->createElement( 'textarea', 'embed_code' )
			->setLabel('External Embed Code')
			->setRequired(false)
			->setAttribs(array('class'=>'form-control', 'rows'=>6))
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
