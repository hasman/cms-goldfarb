<?php
class Admin_Form_AdminUserForm extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/admin_user.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/users/save' );
		$this->setAttrib( 'id', 'UserForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');

		$roles = Zend_Registry::get('admin_acl_roles');
		$acl_member_roles = [];
		$acl_roles = array(
			'admin' => 'Admin',
			//'guest' => 'Guest'
		);
		foreach ($roles as $role_id => $role) {
			if (!$role['is_website_member']) {
				$acl_roles[$role_id] = $role['name'];
			}else {
				$acl_member_roles[$role_id] = $role['name'];
			}
		}

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['current_user_role'] = $this->createElement( 'hidden' , 'current_user_role' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['first_name'] = $this->createElement( 'text', 'first_name' )
			->setLabel('First Name')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['last_name'] = $this->createElement( 'text', 'last_name' )
			->setLabel('Last Name')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		/*
		$elements['username'] = $this->createElement( 'text', 'username' )
			->setLabel('Username')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		*/

		$elements['email'] = $this->createElement( 'text', 'email' )
			->setLabel('Email')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['crop_data'] = $this->createElement( 'hidden' , 'crop_data' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['avatar'] = $this->createElement( 'hidden', 'avatar')->removeDecorator('label')->setRequired(false)->removeDecorator('HtmlTag');
		$elements['image'] = $this->createElement( 'hidden', 'image')->removeDecorator('label')->setRequired(false)->removeDecorator('HtmlTag');

		$elements['password'] = $this->createElement( 'text', 'password' )
			->setLabel('Password')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['role'] = $this->createElement( 'select', 'role' )
			->setLabel('Role')
			->setMultiOptions($acl_roles)
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['member_role'] = $this->createElement( 'select', 'member_role' )
			->setLabel('Role')
			->setMultiOptions($acl_member_roles)
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		//profile fields
		$elements['company'] = $this->createElement( 'text', 'company' )
			->setLabel('Company')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['date_of_birth'] = $this->createElement( 'text', 'date_of_birth' )
			->setLabel('Date of Birth')
			->setRequired(false)
			->setAttrib('class', 'form-control datepicker')
			->setDecorators( $decor_element );
		$elements['phone'] = $this->createElement( 'text', 'phone' )
			->setLabel('Phone')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['address'] = $this->createElement( 'text', 'address' )
			->setLabel('Address')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['address2'] = $this->createElement( 'text', 'address2' )
			->setLabel('Address 2')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['city'] = $this->createElement( 'text', 'city' )
			->setLabel('City')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['region'] = $this->createElement( 'text', 'region' )
			->setLabel('State/Region')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['postcode'] = $this->createElement( 'text', 'postcode' )
			->setLabel('Zip/Postcode')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['country'] = $this->createElement( 'text', 'country' )
			->setLabel('Country')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['website'] = $this->createElement( 'text', 'website' )
			->setLabel('Website URL')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['facebook_url'] = $this->createElement( 'text', 'facebook_url' )
			->setLabel('Facebook URL')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['twitter_url'] = $this->createElement( 'text', 'twitter_url' )
			->setLabel('Twitter URL')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['instagram_url'] = $this->createElement( 'text', 'instagram_url' )
			->setLabel('Instagram URL')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['youtube_url'] = $this->createElement( 'text', 'youtube_url' )
			->setLabel('YouTube URL')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['linkedin_url'] = $this->createElement( 'text', 'linkedin_url' )
			->setLabel('LinkedIn URL')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );


		$this->addElements( $elements );
	}

}
