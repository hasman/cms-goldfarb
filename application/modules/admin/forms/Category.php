<?php
class Admin_Form_Category extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/category.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/category/save' );
		$this->setAttrib( 'id', 'categoryForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');
		$positions = array('Top'=>'Top','Bottom'=>'Bottom','Disabled'=>'Disabled');
		$positions2 = array('above_hero'=>'Above Hero','below_hero'=>'Below Hero');
		$alignments = array('Center'=>'Center','Left'=>'Left','Right'=>'Right');
		$ny = array('No'=>'No', 'Yes'=>'Yes');
		$nav_statuses = array('Disabled'=>'Disabled', 'Enabled'=>'Enabled');

		$mapper = new Admin_Model_Mapper_Category();
		$parent_categories = $mapper->fetchIdPairs(true);

		$mapper = new Admin_Model_Mapper_Page();
		$blocks = $mapper->fetchBlockPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['name'] = $this->createElement( 'text', 'name' )
			->setLabel('Category Name')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['code'] = $this->createElement( 'text', 'code' )
			->setLabel('URL Code')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['parent_id'] = $this->createElement( 'select', 'parent_id' )
			->setLabel('Assign To')
			->setMultiOptions(array(0=>'Top Level')+$parent_categories)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['block_id'] = $this->createElement( 'select', 'block_id' )
			->setLabel('CMS Block')
			->setMultiOptions(array(''=>'None')+$blocks)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['main_image'] = $this->createElement( 'hidden', 'main_image' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');
		$elements['main_image_alt_tag'] = $this->createElement( 'text', 'main_image_alt_tag' )
			->setLabel('Thumbnail Image Alt Tag')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['banner_image'] = $this->createElement( 'hidden', 'banner_image' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');
		$elements['banner_image_alt_tag'] = $this->createElement( 'text', 'banner_image_alt_tag' )
			->setLabel('Banner Image Alt Tag')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['banner_mobile_image'] = $this->createElement( 'hidden', 'banner_mobile_image' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');
		$elements['banner_mobile_image_alt_tag'] = $this->createElement( 'text', 'banner_mobile_image_alt_tag' )
			->setLabel('Mobile Banner Image Alt Tag')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['banner_alignment'] = $this->createElement( 'select', 'banner_alignment' )
			->setLabel('Banner Alignment')
			->setMultiOptions($alignments)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['banner_force_fullwidth'] = $this->createElement( 'select', 'banner_force_fullwidth' )
			->setLabel('Banner Force Fullwidth')
			->setMultiOptions($ny)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['description'] = $this->createElement( 'textarea', 'description' )
			->setLabel('Description')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control tinymce","rows"=>8))
			->setDecorators( $decor_element );
		$elements['description_position'] = $this->createElement( 'select', 'description_position' )
			->setLabel('Description Position')
			->setMultiOptions($positions)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['meta_title'] = $this->createElement( 'text', 'meta_title' )
			->setLabel('Meta Page Title')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['meta_keywords'] = $this->createElement( 'text', 'meta_keywords' )
			->setLabel('Meta Keywords')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['meta_description'] = $this->createElement( 'textarea', 'meta_description' )
			->setLabel('Meta Description')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>4))
			->setDecorators( $decor_element );

		$elements['banner_display_status'] = $this->createElement( 'select', 'banner_display_status' )
			->setLabel('Banner Display')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['title_display_status'] = $this->createElement( 'select', 'title_display_status' )
			->setLabel('Title Display')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['title_display_position'] = $this->createElement( 'select', 'title_display_position' )
			->setLabel('Title Position')
			->setMultiOptions($positions2)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['breadcrumb_display_status'] = $this->createElement( 'select', 'breadcrumb_display_status' )
			->setLabel('Breadcrumb Display')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['breadcrumb_display_position'] = $this->createElement( 'select', 'breadcrumb_display_position' )
			->setLabel('Breadcrumb Position')
			->setMultiOptions($positions2)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['nav_display'] = $this->createElement( 'select', 'nav_display' )
			->setLabel('Show in Navigation')
			->setMultiOptions($nav_statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
