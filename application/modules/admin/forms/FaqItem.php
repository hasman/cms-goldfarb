<?php
class Admin_Form_FaqItem extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/default.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/faq/item-save' );
		$this->setAttrib( 'id', 'faqItemForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');

		$mapper = new Admin_Model_Mapper_FaqSection();
		$sections = $mapper->fetchPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden' , 'sort_order' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['faq_section_id'] = $this->createElement( 'select', 'faq_section_id' )
			->setLabel('Category')
			->setMultiOptions($sections)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['question'] = $this->createElement( 'text', 'question' )
			->setLabel('Question')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['answer'] = $this->createElement( 'textarea', 'answer' )
			->setLabel('Answer')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control tinymce"))
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
