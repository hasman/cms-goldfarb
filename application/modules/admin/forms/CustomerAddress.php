<?php
class Admin_Form_CustomerAddress extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/customer-address.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/customer/address-save' );
		$this->setAttrib( 'id', 'customerAddressForm' );

		//options
		$shipping_countries = Zend_Registry::get('countries')->toArray();
		$billing_countries = Zend_Registry::get('countries')->toArray();
		$usstates = Zend_Registry::get('regions')->us->region->toArray();
		$caprovinces = Zend_Registry::get('regions')->ca->region->toArray();
		$usstates = $usstates + $caprovinces;

		$types = array('shipping'=>'Shipping','billing'=>'Billing');
		$ny = array('No'=>'No','Yes'=>'Yes');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['customer_id'] = $this->createElement( 'hidden' , 'customer_id' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['address_type'] = $this->createElement( 'select', 'address_type' )
			->setLabel('Address Type')
			->setRequired(true)
			->setMultiOptions($types)
			->setAttribs(array('class'=> 'form-control'))
			->setDecorators( $decor_element );
		$elements['default_address'] = $this->createElement( 'select', 'default_address' )
			->setLabel('Default Address')
			->setRequired(true)
			->setMultiOptions($ny)
			->setAttribs(array('class'=> 'form-control'))
			->setDecorators( $decor_element );

		$elements['address_name'] = $this->createElement( 'text', 'address_name' )
			->setLabel('Address Name (optional)')
			->setRequired(false)
			->setAttribs(array('class' => 'form-control','placeholder'=>'Address Name (optional)'))
			->setDecorators( $decor_element );

		$elements['first_name'] = $this->createElement( 'text', 'first_name' )
			->setLabel('First Name')
			->setRequired(true)
			->setAttribs(array('class' => 'form-control','placeholder'=>'First Name'))
			->setDecorators( $decor_element );
		$elements['last_name'] = $this->createElement( 'text', 'last_name' )
			->setLabel('Last Name')
			->setRequired(true)
			->setAttribs(array('class' => 'form-control','placeholder'=>'Last Name'))
			->setDecorators( $decor_element );
		$elements['company'] = $this->createElement( 'text', 'company' )
			->setLabel('Company')
			->setRequired(false)
			->setAttribs(array('class' => 'form-control','placeholder'=>'Company'))
			->setDecorators( $decor_element );
		$elements['address1'] = $this->createElement( 'text', 'address1' )
			->setLabel('Street Address')
			->setRequired(true)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Street Address'))
			->setDecorators( $decor_element );
		$elements['address2'] = $this->createElement( 'text', 'address2' )
			->setLabel('Street Address 2')
			->setRequired(false)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Unit / PO Box'))
			->setDecorators( $decor_element );
		$elements['city'] = $this->createElement( 'text', 'city' )
			->setLabel('City')
			->setRequired(true)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'City'))
			->setDecorators( $decor_element );
		$elements['region'] = $this->createElement( 'select', 'region' )
			->setLabel('State/Province')
			->setRequired(false)
			->setMultiOptions($usstates)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'State/Province'))
			->setDecorators( $decor_element );
		$elements['postcode'] = $this->createElement( 'text', 'postcode' )
			->setLabel('Zip Code')
			->setRequired(true)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Zip Code'))
			->setDecorators( $decor_element );
		$elements['country'] = $this->createElement( 'select', 'country' )
			->setLabel('Country')
			->setRequired(true)
			->setMultiOptions($billing_countries)
			//->setMultiOptions(array("US"=>"United States","CA"=>"Canada"))
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Country'))
			->setDecorators( $decor_element );
		$elements['phone'] = $this->createElement( 'text', 'phone' )
			->setLabel('Phone')
			->setRequired(false)
			->setAttribs(array('class'=> 'form-control full-width','placeholder'=>'Phone'))
			->setDecorators( $decor_element );


		$this->addElements( $elements );
	}

}
