<?php
class Admin_Form_ZypeVideo extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/default.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/video-platform/zype-video-save' );
		$this->setAttrib( 'id', 'zypeVideoForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');
		$off_on = array('Off'=>'Off','On'=>'On');
		$poster_types = array('portrait'=>'Portrait','landscape'=>'Landscape');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Title')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['friendly_title'] = $this->createElement( 'text', 'friendly_title' )
			->setLabel('URL Friendly Title')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['short_description'] = $this->createElement( 'text', 'short_description' )
			->setLabel('Short Description')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['description'] = $this->createElement( 'textarea', 'description' )
			->setLabel('Description')
			->setRequired(false)
			->setAttribs(array('class' => 'form-control', 'rows' => 6))
			->setDecorators( $decor_element );
		$elements['keywords'] = $this->createElement( 'text', 'keywords' )
			->setLabel('Additional Keywords <br/> <small>You can specify additional keywords separated by comma that will be used for searching.</small>')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		/*
		$elements['source_id'] = $this->createElement( 'text', 'source_id' )
			->setLabel('Source ID')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		*/
		$elements['episode'] = $this->createElement( 'text', 'episode' )
			->setLabel('Episode Number')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['season'] = $this->createElement( 'text', 'season' )
			->setLabel('Season Number')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['purchase_required'] = $this->createElement( 'select', 'purchase_required' )
			->setLabel('Purchase Required')
			->setMultiOptions($off_on)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['purchase_price'] = $this->createElement( 'text', 'purchase_price' )
			->setLabel('Purchase Price ($)')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['rental_required'] = $this->createElement( 'select', 'rental_required' )
			->setLabel('Rental Required')
			->setMultiOptions($off_on)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['rental_price'] = $this->createElement( 'text', 'rental_price' )
			->setLabel('Rental Price ($)')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['rental_duration'] = $this->createElement( 'text', 'rental_duration' )
			->setLabel('Rental Duration (days)')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['subscription_required'] = $this->createElement( 'select', 'subscription_required' )
			->setLabel('Subscription Required')
			->setMultiOptions($off_on)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['registration_required'] = $this->createElement( 'select', 'registration_required' )
			->setLabel('Registration Required')
			->setMultiOptions($off_on)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );


		$elements['active'] = $this->createElement( 'select', 'active' )
			->setLabel('Active')
			->setMultiOptions($off_on)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
