<?php
class Admin_Form_VideoPlatformVideoImage extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/video-platform-video-image.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/video-platform/image-save' );
		$this->setAttrib( 'id', 'videoImageForm' );

		//Options
		$ny = array('No'=>'No', 'Yes'=>'Yes');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['video_platform_video_id'] = $this->createElement( 'hidden' , 'video_platform_video_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden' , 'sort_order' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['image_src'] = $this->createElement( 'text', 'image_src' )
			->setLabel('Image')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Title')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['caption'] = $this->createElement( 'text', 'caption' )
			->setLabel('Caption')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		//tags
		$elements['tags'] = $this->createElement( 'text', 'tags' )
			->setLabel('Tags')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
