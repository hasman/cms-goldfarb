<?php
class Admin_Form_PromoForm extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/promo.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/promotion/save' );
		$this->setAttrib( 'id', 'PromoForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');
		$triggers = array('code'=>'Must Enter a Code','auto'=>'Automatically Applied', 'single_use_code'=>'Single Use Coupon Codes');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');


		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Display Title')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['internal_name'] = $this->createElement( 'text', 'internal_name' )
			->setLabel('Internal Title')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['trigger'] = $this->createElement( 'select', 'trigger' )
			->setLabel('Trigger')
			->setRequired(true)
			->setMultiOptions($triggers)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['code'] = $this->createElement( 'text', 'code' )
			->setLabel('Code')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['start_date'] = $this->createElement( 'text', 'start_date' )
			->setLabel('Start Date (UTC)')
			->setRequired(true)
			->setAttribs(array('class'=>'form-control datepicker'))
			->setDecorators( $decor_element );
		$elements['start_time'] = $this->createElement( 'text', 'start_time' )
			->setLabel('Start Time (UTC)')
			->setRequired(true)
			->setAttribs(array('class'=>'form-control timepicker'))
			->setDecorators( $decor_element );
		$elements['end_date'] = $this->createElement( 'text', 'end_date' )
			->setLabel('End Date (UTC)')
			->setRequired(true)
			->setAttribs(array('class'=>'form-control datepicker'))
			->setDecorators( $decor_element );
		$elements['end_time'] = $this->createElement( 'text', 'end_time' )
			->setLabel('End Time (UTC)')
			->setRequired(true)
			->setAttribs(array('class'=>'form-control timepicker'))
			->setDecorators( $decor_element );

		$elements['notes'] = $this->createElement( 'textarea', 'notes' )
			->setLabel('Internal Note <span class="minitip">(not displayed)</span>')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>"8"))
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
