<?php
class Admin_Form_Video extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/default.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/video/item-save' );
		$this->setAttrib( 'id', 'videoForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');

		$mapper = new Admin_Model_Mapper_VideoCategory();
		$sections = $mapper->fetchPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden' , 'sort_order' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['video_category_id'] = $this->createElement( 'select', 'video_category_id' )
			->setLabel('Category')
			->setMultiOptions($sections)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Title')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['video_url'] = $this->createElement( 'text', 'video_url' )
			->setLabel('Video URL (YouTube/Vimeo/Direct MP4 URL)')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['embed_code'] = $this->createElement( 'textarea', 'embed_code' )
			->setLabel('Generic Embed Code')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>4))
			->setDecorators( $decor_element );
		$elements['caption'] = $this->createElement( 'text', 'caption' )
			->setLabel('Caption')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['description'] = $this->createElement( 'textarea', 'description' )
			->setLabel('Description')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control tinymce"))
			->setDecorators( $decor_element );
		//tags
		$elements['tags'] = $this->createElement( 'text', 'tags' )
			->setLabel('Tags')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['posted_date'] = $this->createElement( 'text', 'posted_date' )
			->setLabel('Posted Date')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control datepicker"))
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
