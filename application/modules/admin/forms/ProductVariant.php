<?php
class Admin_Form_ProductVariant extends Zend_Form
{

	public function init()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/ajax-default.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/product/variant-save' );
		$this->setAttrib( 'id', 'variantForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled', 'Unavailable' => 'Unavailable', 'Backordered' => 'Backordered');
		$warehouses = array('' => 'None or N/A');
		if ($settings_mapper->getValueByCode('shipstation_status') == 'Enabled') {
			$warehouses['shipstation'] = 'ShipStation';
		}
		if ($settings_mapper->getValueByCode('custom_warehouse_status') == 'Enabled') {
			$warehouses[$settings_mapper->getValueByCode('custom_warehouse_name')] = $settings_mapper->getValueByCode('custom_warehouse_name');
		}
		if ($settings_mapper->getValueByCode('custom_warehouse2_status') == 'Enabled') {
			$warehouses[$settings_mapper->getValueByCode('custom_warehouse2_name')] = $settings_mapper->getValueByCode('custom_warehouse2_name');
		}
		if ($settings_mapper->getValueByCode('custom_warehouse3_status') == 'Enabled') {
			$warehouses[$settings_mapper->getValueByCode('custom_warehouse3_name')] = $settings_mapper->getValueByCode('custom_warehouse3_name');
		}


		$mapper = new Admin_Model_Mapper_ProductVariantType();
		$variant_types = $mapper->fetchIdPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['parent_product_id'] = $this->createElement( 'hidden' , 'parent_product_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden' , 'sort_order' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['product_variant_type_id'] = $this->createElement( 'select', 'product_variant_type_id' )
			->setLabel('Variant Type')
			->setMultiOptions($variant_types)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Product Name')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['code'] = $this->createElement( 'text', 'code' )
			->setLabel('URL Code')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['sku'] = $this->createElement( 'text', 'sku' )
			->setLabel('Product SKU')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control data-label","data-label-original"=>"Product SKU", "data-label-digital"=>"Download URL / Link"))
			->setDecorators( $decor_element );
		$elements['title_sort'] = $this->createElement( 'text', 'title_sort' )
			->setLabel('Sorting Name<br/><small>(for example "Product Name, The")</small>')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['summary'] = $this->createElement( 'textarea', 'summary' )
			->setLabel('Summary')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>4))
			->setDecorators( $decor_element );
		$elements['description'] = $this->createElement( 'textarea', 'description' )
			->setLabel('Description')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control tinymce","rows"=>8))
			->setDecorators( $decor_element );
		$elements['barcode'] = $this->createElement( 'text', 'barcode' )
			->setLabel('Barcode (ISBN, UPC, GTIN, etc.)')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control conditional digital-hide"))
			->setDecorators( $decor_element );
		$elements['inventory'] = $this->createElement( 'text', 'inventory' )
			->setLabel('Inventory')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control conditional digital-hide"))
			->setDecorators( $decor_element );
		$elements['warehouse'] = $this->createElement( 'select', 'warehouse' )
			->setLabel('Warehouse')
			->setRequired(false)
			->setMultiOptions($warehouses)
			->setAttribs(array("class"=>"form-control conditional digital-hide"))
			->setDecorators( $decor_element );
		$elements['available_date'] = $this->createElement( 'text', 'available_date' )
			->setLabel('Available Date')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control datepicker"))
			->setDecorators( $decor_element );
		$elements['price'] = $this->createElement( 'text', 'price' )
			->setLabel('Price')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['msrp'] = $this->createElement( 'text', 'msrp' )
			->setLabel('MSRP (Compare at)')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['weight'] = $this->createElement( 'text', 'weight' )
			->setLabel('Weight (lbs to nearest tenth)')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control conditional digital-hide"))
			->setDecorators( $decor_element );
		$elements['length'] = $this->createElement( 'text', 'length' )
			->setLabel('Length (in inches)')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control conditional digital-hide"))
			->setDecorators( $decor_element );
		$elements['width'] = $this->createElement( 'text', 'width' )
			->setLabel('Width (in inches)')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control conditional digital-hide"))
			->setDecorators( $decor_element );
		$elements['height'] = $this->createElement( 'text', 'height' )
			->setLabel('Height (in inches)')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control conditional digital-hide"))
			->setDecorators( $decor_element );

		$elements['meta_title'] = $this->createElement( 'text', 'meta_title' )
			->setLabel('Meta Page Title')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['meta_keywords'] = $this->createElement( 'text', 'meta_keywords' )
			->setLabel('Meta Keywords')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['meta_description'] = $this->createElement( 'textarea', 'meta_description' )
			->setLabel('Meta Description')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>4))
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
