<?php
class Admin_Form_InstalinksLink extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/instalinks-link.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/instalink/link-save' );
		$this->setAttrib( 'id', 'instalinksLinkForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');
		$border_sizes = array('0'=>'None','1px'=>'1px','2px'=>'2px','3px'=>'3px','4px'=>'4px','5px'=>'5px','6px'=>'6px','7px'=>'7px','8px'=>'8px','9px'=>'9px','10px'=>'10px');
		$icon_positions = array(
			'top_right' => 'Top Right',
			'top_left' => 'Top Left',
			'bottom_right' => 'Bottom Right',
			'bottom_left' => 'Bottom Left',
		);
		$text_alignments = array(
			'' => 'Top',
			'flex-align-center' => 'Center',
			'flex-align-end' => 'Bottom'
		);


		//Form fields
		$elements['cmd'] = $this->createElement( 'hidden', 'cmd' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');
		$elements['id'] = $this->createElement( 'hidden', 'id' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden', 'sort_order' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Label')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['link_url'] = $this->createElement( 'text', 'link_url' )
			->setLabel('Link URL')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['content'] = $this->createElement( 'textarea', 'content' )
			->setLabel('Text/Content')
			->setRequired(false)
			->setAttrib('class', 'form-control tinymce')
			->setDecorators( $decor_element );
		$elements['text_valign'] = $this->createElement( 'select', 'text_valign' )
			->setLabel('Text Vertical Alignment')
			->setMultiOptions($text_alignments)
			->setAttrib('class', 'form-control conditional layout-tile')
			->setDecorators( $decor_element );

		$elements['button_color'] = $this->createElement( 'text', 'button_color' )
			->setLabel('Button Color')
			->setRequired(false)
			->setAttrib('class', 'form-control complex-colorpicker conditional layout-button')
			->setDecorators( $decor_element );
		$elements['button_border_color'] = $this->createElement( 'text', 'button_border_color' )
			->setLabel('Border Color')
			->setRequired(false)
			->setAttrib('class', 'form-control complex-colorpicker conditional layout-button')
			->setDecorators( $decor_element );
		$elements['button_border_size'] = $this->createElement( 'select', 'button_border_size' )
			->setLabel('Border Size')
			->setMultiOptions($border_sizes)
			->setAttrib('class', 'form-control conditional layout-button')
			->setDecorators( $decor_element );

		$elements['image'] = $this->createElement( 'hidden', 'image' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');
		$elements['icon'] = $this->createElement( 'text', 'icon' )
			->setLabel('Icon')
			->setRequired(false)
			->setAttrib('class', 'form-control conditional layout-tile')
			->setDecorators( $decor_element );
		$elements['icon_position'] = $this->createElement( 'select', 'icon_position' )
			->setLabel('Icon Position')
			->setMultiOptions($icon_positions)
			->setAttrib('class', 'form-control conditional layout-tile')
			->setDecorators( $decor_element );
		$elements['icon_color'] = $this->createElement( 'text', 'icon_color' )
			->setLabel('Icon Color')
			->setRequired(false)
			->setAttrib('class', 'form-control complex-colorpicker conditional layout-tile')
			->setDecorators( $decor_element );


		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements($elements);
	}

	public function loadDefaultDecorators() {
		parent::loadDefaultDecorators();

		// remove the 'fieldset' decorator from all subforms
		$subforms = $this->getSubForms();
		foreach($subforms as $subform) {
			$subform->removeDecorator('Fieldset');
			$subform->removeDecorator('DtDdWrapper');
			$subform->removeDecorator('label');
			$subform->removeDecorator('HtmlTag');
		}

		return $this;
	}
}
