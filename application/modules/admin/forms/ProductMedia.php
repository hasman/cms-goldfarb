<?php
class Admin_Form_ProductMedia extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/product-media.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/product/media-save' );
		$this->setAttrib( 'id', 'productMediaForm' );

		//Options
		$ny = array('No'=>'No', 'Yes'=>'Yes');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['product_id'] = $this->createElement( 'hidden' , 'product_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden' , 'sort_order' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['media_src'] = $this->createElement( 'text', 'media_src' )
			->setLabel('Attribute')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Title')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		//tags
		$elements['tags'] = $this->createElement( 'text', 'tags' )
			->setLabel('Tags')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
