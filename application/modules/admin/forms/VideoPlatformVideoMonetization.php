<?php
class Admin_Form_VideoPlatformVideoMonetization extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/video-platform-video-monetization.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/video-platform/monetization-save' );
		$this->setAttrib( 'id', 'videoMonetizationForm' );

		//Options
		$ny = array('No'=>'No', 'Yes'=>'Yes');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['video_platform_video_id'] = $this->createElement( 'hidden' , 'video_platform_video_id' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['purchase_required'] = $this->createElement( 'select', 'purchase_required' )
			->setLabel('Purchase Required')
			->setMultiOptions($ny)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['purchase_price'] = $this->createElement( 'text', 'purchase_price' )
			->setLabel('Purchase Price')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['rental_required'] = $this->createElement( 'select', 'rental_required' )
			->setLabel('Rental Required')
			->setMultiOptions($ny)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['rental_price'] = $this->createElement( 'text', 'rental_price' )
			->setLabel('Rental Price')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['rental_duration_days'] = $this->createElement( 'text', 'rental_duration_days' )
			->setLabel('Rental Duration')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );


		$this->addElements( $elements );
	}

}
