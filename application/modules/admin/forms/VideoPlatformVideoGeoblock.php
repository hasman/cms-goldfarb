<?php
class Admin_Form_VideoPlatformVideoGeoblock extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/video-platform-video-geoblock.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/video-platform/geoblock-save' );
		$this->setAttrib( 'id', 'videoGeoblockForm' );

		//Options
		$countries = Zend_Registry::get('countries')->toArray();
		/*
		$usstates = Zend_Registry::get('regions')->us->region->toArray();
		$caprovinces = Zend_Registry::get('regions')->ca->region->toArray();
		$usstates = $usstates + $caprovinces;
		*/

		$types = array('country'=>'Country Code', 'region'=>'State/Region', 'city'=>'City Name', 'postcode'=>'Zip/Postcode');
		$statuses = array('Allow'=>'Allow', 'Disallow'=>'Disallow');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['video_platform_video_id'] = $this->createElement( 'hidden' , 'video_platform_video_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden' , 'sort_order' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['geo_code'] = $this->createElement( 'hidden' , 'geo_code' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['country'] = $this->createElement( 'select', 'country' )
			->setLabel('Country')
			->setRequired(false)
			->setMultiOptions(array(''=>'','all'=>'All Countries') + $countries)
			->setAttribs(array('class'=> 'form-control conditional','placeholder'=>'Country'))
			->setDecorators( $decor_element );
		$elements['region'] = $this->createElement( 'text', 'region' )
			->setLabel('Region <br/><small>(2 char code for US/CA, full name elsewhere)</small>')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control conditional"))
			->setDecorators( $decor_element );
		$elements['city'] = $this->createElement( 'text', 'city' )
			->setLabel('City Name')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control conditional"))
			->setDecorators( $decor_element );
		$elements['postcode'] = $this->createElement( 'text', 'postcode' )
			->setLabel('Zip/Postcode')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control conditional"))
			->setDecorators( $decor_element );

		$elements['granularity'] = $this->createElement( 'select', 'granularity' )
			->setLabel('Granularity')
			->setMultiOptions($types)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Type')
			->setMultiOptions($statuses)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
