<?php
class Admin_Form_CustomerProfile extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/customer-profile.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/customer/profile-save' );
		$this->setAttrib( 'id', 'customerProfileForm' );

		//Options

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['customer_id'] = $this->createElement( 'hidden' , 'customer_id' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['customer_attribute_id'] = $this->createElement( 'text', 'customer_attribute_id' )
			->setLabel('Attribute')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		/*
		$elements['value'] = $this->createElement( 'textarea', 'value' )
			->setLabel('Value')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control","rows"=>3))
			->setDecorators( $decor_element );
		*/
		$elements['value'] = $this->createElement( 'hidden' , 'value' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['options'] = $this->createElement( 'hidden' , 'options' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$this->addElements( $elements );
	}

}
