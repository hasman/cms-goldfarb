<?php
class Admin_Form_VideoPlatformVideoTrack extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/video-platform-video-track.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/video-platform/track-save' );
		$this->setAttrib( 'id', 'videoPlatformTrackForm' );

		//Options
		$ny = array('No'=>'No', 'Yes'=>'Yes');
		$kinds = array('subtitles'=>'Subtitle','captions'=>'Closed Caption','descriptions'=>'Description Track','chapters'=>'Chapter Titles','metadata'=>'Metadata Track');
		$languages = Zend_Registry::get('languages')->toArray();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['video_platform_video_id'] = $this->createElement( 'hidden' , 'video_platform_video_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['public_url'] = $this->createElement( 'hidden' , 'public_url' )->removeDecorator('label')->removeDecorator('HtmlTag');


		$elements['label'] = $this->createElement( 'text', 'label' )
			->setLabel('Label')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['file'] = $this->createElement( 'file', 'file' )
			->setLabel('File')
			->setRequired(false)
			->setDecorators(
				array('File', 'Description', 'Errors',
					array(array('data'=>'HtmlTag'), array('tag' => 'td')),
					array('Label', array('tag' => 'td')),
					array(array('row'=>'HtmlTag'),array('tag'=>'tr'))
				))
			->setDestination(realpath('./../')."/aws")
			->addValidator('ExcludeExtension', false, 'php,exe,js')
		;

		$elements['offset_seconds'] = $this->createElement( 'text', 'offset_seconds' )
			->setLabel('Offset (in seconds)')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['kind'] = $this->createElement( 'select', 'kind' )
			->setLabel('Track Kind')
			->setMultiOptions($kinds)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['srclang'] = $this->createElement( 'select', 'srclang' )
			->setLabel('Language')
			->setRequired(false)
			->setMultiOptions(array(''=>'Not Applicable')+$languages)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['default_track'] = $this->createElement( 'select', 'default_track' )
			->setLabel('Default Track')
			->setMultiOptions($ny)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
