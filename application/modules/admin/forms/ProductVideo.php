<?php
class Admin_Form_ProductVideo extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/product-video.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/product/video-save' );
		$this->setAttrib( 'id', 'productVideoForm' );

		//Options

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['product_id'] = $this->createElement( 'hidden' , 'product_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden' , 'sort_order' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['video_src'] = $this->createElement( 'text', 'video_src' )
			->setLabel('YouTube/Direct MP4 URL')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['image_src'] = $this->createElement( 'text', 'image_src' )
			->setLabel('Thumbnail')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['alt_text'] = $this->createElement( 'text', 'alt_text' )
			->setLabel('Alt Text')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
