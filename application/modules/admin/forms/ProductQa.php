<?php
class Admin_Form_ProductQa extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/default.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/qa/product-save' );
		$this->setAttrib( 'id', 'productQAForm' );

		//Options
		$statuses = array('Pending'=>'Pending', 'Approved'=>'Approved', 'Denied'=>'Denied');
		$ny = array('No'=>'No', 'Yes'=>'Yes');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['product_id'] = $this->createElement( 'hidden' , 'product_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['product_variant_id'] = $this->createElement( 'hidden' , 'product_variant_id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['remote_addr'] = $this->createElement( 'hidden' , 'remote_addr' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['user_agent'] = $this->createElement( 'hidden' , 'user_agent' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['referrer'] = $this->createElement( 'hidden' , 'referrer' )->removeDecorator('label')->removeDecorator('HtmlTag');


		$elements['name'] = $this->createElement( 'text', 'name' )
			->setLabel('Name')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['email'] = $this->createElement( 'text', 'email' )
			->setLabel('Email')
			->setRequired(false)
			->addValidator('EmailAddress')
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['question'] = $this->createElement( 'textarea', 'question' )
			->setLabel('Question')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control","rows"=>8))
			->setDecorators( $decor_element );
		$elements['answer'] = $this->createElement( 'textarea', 'answer' )
			->setLabel('Answer')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control tinymce"))
			->setDecorators( $decor_element );

		$elements['is_spam'] = $this->createElement( 'select', 'is_spam' )
			->setLabel('Is Spam')
			->setMultiOptions($ny)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
