<?php
class Admin_Form_VideoPlatformVideo extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/video-platform-item.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/video-platform/item-save' );
		$this->setAttrib( 'id', 'videoPlatformVideoForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');
		$poster_types = array('portrait'=>'Portrait','landscape'=>'Landscape');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['admin_user_id'] = $this->createElement( 'hidden' , 'admin_user_id' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['filename'] = $this->createElement( 'hidden' , 'filename' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['filename_internal'] = $this->createElement( 'hidden' , 'filename_internal' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['filesize'] = $this->createElement( 'hidden' , 'filesize' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['mimetype'] = $this->createElement( 'hidden' , 'mimetype' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['etag'] = $this->createElement( 'hidden' , 'etag' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['transcode_status'] = $this->createElement( 'hidden' , 'transcode_status' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Title')
			->setRequired(true)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['code'] = $this->createElement( 'text', 'code' )
			->setLabel('URL Code')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['summary'] = $this->createElement( 'text', 'summary' )
			->setLabel('Short Description')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['description'] = $this->createElement( 'textarea', 'description' )
			->setLabel('Description')
			->setRequired(false)
			->setAttribs(array('class' => 'form-control tinymce', 'rows' => 6))
			->setDecorators( $decor_element );
		$elements['purchase_support_text'] = $this->createElement( 'textarea', 'purchase_support_text' )
			->setLabel('Purchase Support Text')
			->setRequired(false)
			->setAttribs(array('class' => 'form-control tinymce', 'rows' => 4))
			->setDecorators( $decor_element );

		//categories
		$elements['categories'] = $this->createElement( 'text', 'categories' )
			->setLabel('Categories')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		//genres
		$elements['genres'] = $this->createElement( 'text', 'genres' )
			->setLabel('Genres')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		//tags
		$elements['tags'] = $this->createElement( 'text', 'tags' )
			->setLabel('Tags')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['meta_keywords'] = $this->createElement( 'text', 'meta_keywords' )
			->setLabel('Meta Keywords')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['meta_description'] = $this->createElement( 'textarea', 'meta_description' )
			->setLabel('Meta Description')
			->setRequired(false)
			->setAttribs(array('class' => 'form-control', 'rows' => 6))
			->setDecorators( $decor_element );

		$elements['thumbnail_src'] = $this->createElement( 'hidden', 'thumbnail_src' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');
		$elements['video_platform_video_thumbnail_id'] = $this->createElement( 'hidden', 'video_platform_video_thumbnail_id' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');

		$elements['poster_src'] = $this->createElement( 'hidden', 'poster_src' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');
		$elements['poster_type'] = $this->createElement( 'select', 'poster_type' )
			->setLabel('Poster Type')
			->setMultiOptions($poster_types)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$elements['embed_url'] = $this->createElement( 'text', 'embed_url' )
			->setLabel('External Video URL')
			->setDescription('<br/><small>YouTube/Vimeo/direct MP4 URL <br/>(instead of platform hosted video)</small>')
			->setRequired(false)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );
		$elements['embed_code'] = $this->createElement( 'textarea', 'embed_code' )
			->setLabel('External Video Embed Code')
			->setRequired(false)
			->setAttribs(array('class'=>'form-control', 'rows'=>6))
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttrib('class', 'form-control')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
