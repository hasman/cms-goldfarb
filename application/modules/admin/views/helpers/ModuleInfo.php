<?php
class Admin_View_Helper_ModuleInfo
{
	public function __construct()
	{
		$cms_modules = Zend_Registry::get('cms_modules');
		if (!is_array($cms_modules)) {
			$cms_modules = $cms_modules->toArray();
		}

		$this->_module_types = array();
		foreach ($cms_modules as $code => $module) {
			if (!isset($module['status']) || !$module['status']) $module['status'] = 'Enabled';
			if (!isset($module['sort_order']) || !$module['sort_order']) $module['sort_order'] = 99999;

			$this->_module_types[$code] = array('title' => $module['title'], 'icon' => $module['icon'], 'status' => $module['status'], 'sort_order' => $module['sort_order']);
		}
	}

	public function moduleCodes($show_disabled = false)
	{
		$module_keys = []; $counter = 0;
		foreach ($this->_module_types as $code => $module) {
			if (!$show_disabled) {
				if ($module['status'] == 'Disabled') continue;
			}

			$module_keys[$module['sort_order'].".".$counter] = $code;

			$counter++;
		}
		ksort($module_keys);

		return $module_keys;

		//return array_keys($this->_module_types);
	}

	public function moduleTitle($code)
	{
		return $this->_module_types[$code]['title'];
	}

	public function moduleIcon($code)
	{
		$string = '<span class="icons" title="'.$this->moduleTitle($code).'">';
		foreach ($this->_module_types[$code]['icon'] as $icon) {
			$string .= '<i class="'.$icon.'"></i> ';
		}
		$string .= '</span>';

		return $string;
	}

	public function moduleStatus($code)
	{
		return $this->_module_types[$code]['status'];
	}
}
