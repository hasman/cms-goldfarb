<?php

class Admin_QaController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/qa/product-list');
	}

	public function productListAction()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'product_reviews', 'view');
		if (!$acl_status) {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$this->view->placeholder('page_title')->set("Product Q&As");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Product Q&As</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','pq.created');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_ProductQa();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'product_qas';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function productEditAction()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'product_reviews', 'view');
		if (!$acl_status) {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductQa();
			$this->view->item = $entity = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Product Q&As");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/qa/product-list">Product Q&As</a></li>
				<li class="active">View Product Q&A: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_ProductQa();

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('form');
		}else{
			$this->getResponse()->setRedirect('/admin/product/');
		}
	}

	public function productViewAction()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'product_reviews', 'view');
		if (!$acl_status) {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductQa();
			$this->view->qa = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Product Q&As");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/qa/product-list">Product Q&As</a></li>
				<li class="active">View Product Q&A: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);
		}else{
			$this->getResponse()->setRedirect('/admin/product/');
		}
	}

	public function productSaveAction()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'product_reviews', 'view');
		if (!$acl_status) {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$form = new Admin_Form_ProductQa();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_ProductQa();

		if( $form->isValid($this->getRequest()->getParams())){
			if ($data['id']) {
				$entity = $mapper->find($data['id']);
			}

			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
			if ($settings_mapper->getValueByCode('akismet_api_key')) {
				$akismet = new Application_Model_Akismet();

				$verify_data = array(
					'user_ip' => $data['remote_addr'],
					'referrer' => $data['referrer'],
					'comment_type' => 'question',
					'comment_author' => $data['name'],
					'comment_author_email' => $data['email'],
					'comment_content' => $data['question'],
					'comment_date_gmt' => date("c", strtotime($data['created']))
				);

				if ($data['is_spam'] == 'Yes' && $entity->is_spam == 'No') {
					$akismet->submitSpam($verify_data);
				}elseif($data['is_spam'] == 'No' && $entity->is_spam == 'Yes') {
					$akismet->submitHam($verify_data);
				}
			}

			$mapper->save(new Application_Model_ProductQa($data));

			if ($settings_mapper->getValueByCode('product_qas_auto_email_response') == 'Enabled' && $data['email'] && $data['answer']) {
				if ($data['product_variant_id']) {
					$mapper = new Application_Model_Mapper_ProductVariant();
					$product = $mapper->find($data['product_variant_id']);
				}else{
					$mapper = new Application_Model_Mapper_Product();
					$product = $mapper->find($data['product_id']);
				}

				$mailbody = '
<p>Hello '.$data['name'].',</p>
<p>Your question about '.$product->title.' has been answered.</p>
<p><strong>Question:</strong> '.$data['question'].'</p>
<p><strong>Answer:</strong> '.$data['answer'].'</p>
<br/><br/>
<p>Thank you for your question.</p>
				';

				$emails = new Application_Model_Emails();
				$emails->generic($mailbody, $data['email'], $this->_site.' answered your question','');
			}

			$this->getResponse()->setRedirect('/admin/qa/product-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$this->view->id = $data['id'];

			$form->populate($data);
			$this->view->form = $form;

			$this->render('form');
		}

	}

	public function productDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductQa();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/qa/product-list');
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'ecommerce', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Pending' => 'Pending',
			'Approved' => 'Approved',
			'Denied' => 'Denied'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');

		$this->_site = ($settings_mapper->getValueByCode('site_name'))?$settings_mapper->getValueByCode('site_name'):$options['site']['name'];
		if (!$this->_site && isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$this->_site = $_SERVER["SERVER_NAME"];
		}
	}
}
