<?php
use Cocur\Slugify\Slugify;

class Admin_ProductController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/product/list');
	}

	public function listAction()
	{
		$this->view->placeholder('page_title')->set("Products");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Products</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$mapper = new Admin_Model_Mapper_Category();
		$this->view->categories = $mapper->fetchIdPairs();

		$mapper = new Admin_Model_Mapper_Collection();
		$this->view->collections = $mapper->fetchIdPairs();

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','title_sort');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');
		$this->view->category_id = $category_id = $this->_getParam('category_id','');
		$this->view->collection_id = $collection_id = $this->_getParam('collection_id','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha,
			'category_id' => $category_id,
			'collection_id' => $collection_id,
		);
		$mapper = new Admin_Model_Mapper_Product();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'products';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function addAction()
	{
		$this->view->placeholder('page_title')->set("Products");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/product">Products</a></li>
				<li class="active">Add Product</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_Product();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('add-form');
	}

	public function editAction()
	{
		$this->view->product_id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Products");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/product">Products</a></li>
				<li class="active">Edit Product: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_Product();
			$mapper = new Admin_Model_Mapper_Product();
			$this->view->product = $entity = $mapper->find($id);

			$data = $entity->toArray();

			//tags
			$mapper = new Admin_Model_Mapper_ProductTags();
			$data['tags'] = implode("|",$mapper->fetchTagNamesByProductId($id));

			//related products
			$mapper = new Admin_Model_Mapper_ProductRelated();
			$products = $mapper->fetchRelatedProductsByProductId($id);
			$prod_list = array();
			foreach ($products as $product) {
				if ($product->product_id) {
					$item_type = 'product';
					$item = array(
						'id' => $product->product_id,
						'title' => $product->product_title,
						'type' => '',
						'sku' => $product->product_sku
					);
				}

				$prod_list[] = '<span class="hidden">'.$item_type.'~~'.$item['id'].'</span>'.str_ireplace("'","`",$item['title']).' '.$item['type'].' '.$item['sku'];
			}
			$data['related'] = implode("|",$prod_list);

			//categories
			$mapper = new Admin_Model_Mapper_ProductCategory();
			$primary_category = $mapper->fetchProductPrimaryCategory($id);
			$data['primary_category'] = ($primary_category && $primary_category->category_id)?$primary_category->category_id:null;
			$extra_categories = $mapper->fetchCategoriesByProductId($id, false);
			$extra_category_ids = array();
			foreach ($extra_categories as $extra_category) {
				$extra_category_ids[] = $extra_category->id;
			}
			$data['extra_categories'] = $extra_category_ids;

			//collections
			$mapper = new Admin_Model_Mapper_ProductCollection();
			$primary_collection = $mapper->fetchProductPrimaryCollection($id);
			$data['primary_collection'] = ($primary_collection && $primary_collection->collection_id)?$primary_collection->collection_id:null;
			$extra_collections = $mapper->fetchCollectionsByProductId($id, false);
			$extra_collection_ids = array();
			foreach ($extra_collections as $extra_collection) {
				$extra_collection_ids[] = $extra_collection->id;
			}
			$data['extra_collections'] = $extra_collection_ids;

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('form');
		}else{
			$this->getResponse()->setRedirect('/admin/product/list');
		}
	}

	public function viewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Product();
			$this->view->product = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Products");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/product">Products</a></li>
				<li class="active">View Product: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$mapper = new Admin_Model_Mapper_ProductProfile();
			$this->view->profile = $mapper->getProfileByProductId($id);

			$mapper = new Admin_Model_Mapper_ProductImage();
			$this->view->images = $mapper->fetchOrderedByProductId($id);

			$mapper = new Admin_Model_Mapper_ProductVideo();
			$this->view->videos = $mapper->fetchOrderedByProductId($id);

			$mapper = new Admin_Model_Mapper_ProductMedia();
			$this->view->media = $mapper->fetchOrderedByProductId($id);

			//categories
			$mapper = new Admin_Model_Mapper_ProductCategory();
			$this->view->primary_category = $mapper->fetchProductPrimaryCategory($id);
			$this->view->extra_categories = $mapper->fetchCategoriesByProductId($id, false);

			//collections
			$mapper = new Admin_Model_Mapper_ProductCollection();
			$this->view->primary_collection = $mapper->fetchProductPrimaryCollection($id);
			$this->view->extra_collections = $mapper->fetchCollectionsByProductId($id, false);

			//variants
			$this->view->variants = array();
			$mapper = new Admin_Model_Mapper_ProductVariant();
			$variants = $mapper->getVariantsByProductId($id);

			foreach ($variants as $variant) {
				$this->view->variants[$variant->id]['data'] = $variant;

				$mapper = new Admin_Model_Mapper_ProductVariantProfile();
				$this->view->variants[$variant->id]['profile'] = $mapper->getProfileByProductVariantId($variant->id);

				$mapper = new Admin_Model_Mapper_ProductVariantImage();
				$this->view->variants[$variant->id]['images'] = $mapper->fetchOrderedByProductVariantId($variant->id);
			}
		}else{
			$this->getResponse()->setRedirect('/admin/product/');
		}
	}

	public function saveAction()
	{
		$form = new Admin_Form_Product();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_Product();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($this->getRequest()->getParams())) {
			if (!$data['title_sort']) $data['title_sort'] = $data['title'];

			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
				if ($k == 'available_date') {
					$data[$k] = date("Y-m-d",strtotime($data[$k]));
				}
			}

			$entity = new Application_Model_Product($data);
			$id = $mapper->save($entity);

			//tags
			$t_mapper = new Admin_Model_Mapper_ProductTags();
			$tc_mapper = new Admin_Model_Mapper_ProductTag();

			$tc_mapper->deleteByProductId($id);

			$tags = explode("|",$data['product_tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Application_Model_ProductTags(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Application_Model_ProductTag(array('product_id' => $id, 'product_tag_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			//related products
			$mapper = new Admin_Model_Mapper_ProductRelated();
			$mapper->deleteByProductId($id);

			$products = explode("|",$data['related']);
			if ($products) {
				foreach ($products as $k => $product_string) {
					$pattern = "/<span ?.*>(.*)<\/span>/";
					preg_match($pattern, $product_string, $matches);
					if (count($matches) > 1) {
						$related_product_id = $variant_id = '';
						$split = explode("~~", $matches[1]);
						if ($split[0] == 'product') {
							$related_product_id = $split[1];
						}

						if ($related_product_id) {
							$prod = new Application_Model_ProductRelated(
								array('product_id' => $id, 'product_related_id' => $related_product_id, 'sort_order' => $k)
							);
							$mapper->save($prod);
						}
					}
				}
			}

			//categories
			$mapper = new Admin_Model_Mapper_ProductCategory();
			$mapper->deleteByProductId($id);

			if ($data['primary_category']) {
				$mapper->save(new Application_Model_ProductCategory(array(
					'product_id' => $id,
					'category_id' => $data['primary_category'],
					'primary_category' => 'Yes'
				)));
			}
			if ($data['extra_categories']) {
				foreach ($data['extra_categories'] as $k => $category_id) {
					$mapper->save(new Application_Model_ProductCategory(array(
						'product_id' => $id,
						'category_id' => $category_id,
						'primary_category' => 'No'
					)));
				}
			}

			//collections
			$mapper = new Admin_Model_Mapper_ProductCollection();
			$mapper->deleteByProductId($id);

			if ($data['primary_collection']) {
				$mapper->save(new Application_Model_ProductCollection(array(
					'product_id' => $id,
					'collection_id' => $data['primary_collection'],
					'primary_collection' => 'Yes'
				)));
			}
			if ($data['extra_collections']) {
				foreach ($data['extra_collections'] as $k => $collection_id) {
					$mapper->save(new Application_Model_ProductCollection(array(
						'product_id' => $id,
						'collection_id' => $collection_id,
						'primary_collection' => 'No'
					)));
				}
			}

			if ($data['cmd'] == 'add') {
				$this->getResponse()->setRedirect('/admin/product/edit/id/'.$id);
			}else {
				$this->getResponse()->setRedirect('/admin/product/list');
			}
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$this->view->product_id = $data['id'];

			$form->populate($data);
			$this->view->form = $form;

			if ($data['cmd'] == 'add') {
				$this->render('add-form');
			}else {
				$this->render('form');
			}
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Product();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/product/list');
	}

	public function copyModalAction()
	{
		$this->view->id = $id = $this->getRequest()->getParam('id', 0);
	}

	public function copyAction()
	{
		$id = $this->getRequest()->getParam('id', 0); //id of product to clone
		$auto_enable = $this->getRequest()->getParam('ae','no');
		$clone_variants = $this->getRequest()->getParam('cv','no');

		$product_mapper = new Admin_Model_Mapper_Product();
		$product_tag_mapper = new Admin_Model_Mapper_ProductTag();
		$related_mapper = new Admin_Model_Mapper_ProductRelated();
		$profile_mapper = new Admin_Model_Mapper_ProductProfile();
		$image_mapper = new Admin_Model_Mapper_ProductImage();
		$image_tag_mapper = new Admin_Model_Mapper_ProductImageTag();
		$video_mapper = new Admin_Model_Mapper_ProductVideo();
		$category_mapper = new Admin_Model_Mapper_ProductCategory();
		$variant_mapper = new Admin_Model_Mapper_ProductVariant();
		$media_mapper = new Admin_Model_Mapper_ProductMedia();
		$media_tag_mapper = new Admin_Model_Mapper_ProductMediaTag();
		$collection_mapper = new Admin_Model_Mapper_ProductCollection();

		//get and clone product
		$product = $product_mapper->find($id);
		$product->id = null;
		$product->title = $product->title . " COPY";
		$product->status = ($auto_enable == 'yes')?'Enabled':'Disabled';

		$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
		$code = $orig_code = $slugify->slugify($product->title);
		$counter = 1;
		while ($product_mapper->doesExists(array('code' => $code)) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$product->code = $code;

		$new_product_id = $product_mapper->save($product);

		//get and clone categories
		$categories = $category_mapper->fetchCategoriesByProductId($id);
		foreach ($categories as $category) {
			$category_mapper->save(new Application_Model_ProductCategory(array('product_id' => $new_product_id, 'category_id' => $category['category_id'], 'primary_category' => $category['primary_category'])));
		}

		//get and clone collections
		$collections = $collection_mapper->fetchCollectionByProductId($id);
		foreach ($collections as $collection) {
			$collection_mapper->save(new Application_Model_ProductCollection(array('product_id' => $new_product_id, 'collection_id' => $collection['collection_id'], 'primary_collection' => $collection['primary_collection'])));
		}

		//get and clone product tags
		$tags = $product_tag_mapper->fetchByProductId($id);
		foreach ($tags as $tag) {
			$product_tag_mapper->save(new Application_Model_ProductTag(array('product_id' => $new_product_id, 'product_tag_id' => $tag->product_tag_id, 'sort_order' => $tag->sort_order)));
		}

		//get and clone related products
		$relateds = $related_mapper->fetchByProductId($id);
		foreach ($relateds as $related) {
			$related_mapper->save(new Application_Model_ProductRelated(array('product_id' => $new_product_id, 'product_related_id' => $related['product_related_id'], 'sort_order' => $related['sort_order'])));
		}

		//get and clone images
		$images = $image_mapper->fetchOrderedByProductId($id);
		foreach ($images as $image) {
			$image_id = $image->id;

			$image->id = null;
			$image->product_id = $new_product_id;

			$new_image_id = $image_mapper->save($image);

			//get and clone image tags
			$image_tags = $image_tag_mapper->fetchByProductImageId($image_id);
			foreach ($image_tags as $image_tag) {
				$image_tag_mapper->save(new Application_Model_ProductImageTag(array('product_image_id'=>$new_image_id,'image_tag_id'=>$image_tag['image_tag_id'],'sort_order'=>$image_tag['sort_order'])));
			}
		}

		//get and clone videos
		$videos = $video_mapper->fetchOrderedByProductId($id);
		foreach ($videos as $video) {
			$video->id = null;
			$video->product_id = $new_product_id;

			$video_mapper->save($video);
		}

		//get and clone media
		$medias = $media_mapper->fetchOrderedByProductId($id);
		foreach ($medias as $media) {
			$media_id = $media->id;

			$media->id = null;
			$media->product_id = $new_product_id;

			$new_media_id = $media_mapper->save($media);

			//get and clone media tags
			$media_tags = $media_tag_mapper->fetchByProductMediaId($media_id);
			foreach ($media_tags as $media_tag) {
				$media_tag_mapper->save(new Application_Model_ProductMediaTag(array('product_media_id'=>$new_media_id,'media_tag_id'=>$media_tag['media_tag_id'],'sort_order'=>$media_tag['sort_order'])));
			}
		}

		//get and clone attributes
		$attributes = $profile_mapper->getProfileByProductId($id);
		foreach ($attributes as $attribute) {
			$profile_mapper->save(new Application_Model_ProductProfile(array('product_id' => $new_product_id, 'product_attribute_id' => $attribute['product_attribute_id'], 'value' => $attribute['value'])));
		}

		if ($clone_variants == 'yes') {
			$variant_profile_mapper = new Admin_Model_Mapper_ProductVariantProfile();
			$variant_image_mapper = new Admin_Model_Mapper_ProductVariantImage();
			$variant_image_tag_mapper = new Admin_Model_Mapper_ProductVariantImageTag();
			$variant_video_mapper = new Admin_Model_Mapper_ProductVariantVideo();
			$variant_media_mapper = new Admin_Model_Mapper_ProductVariantMedia();
			$variant_media_tag_mapper = new Admin_Model_Mapper_ProductVariantMediaTag();

			//get and clone variants
			$variants = $variant_mapper->getVariantsByProductId($id);

			foreach ($variants as $variant) {
				$variant_id = $variant->id;

				$variant->id = null;
				$variant->parent_product_id = $new_product_id;
				$variant->title = $variant->title . " COPY";
				$variant->status = ($auto_enable == 'yes')?'Enabled':'Disabled';

				$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
				$code = $orig_code = $slugify->slugify($variant->title);
				$counter = 1;
				while ($variant_mapper->doesExists(array('code' => $code)) !== false) {
					$code = $orig_code . "-" . $counter;
					$counter++;
				}
				$variant->code = $code;

				$new_variant_id = $variant_mapper->save($variant);

				//get and clone images
				$images = $variant_image_mapper->fetchOrderedByProductVariantId($variant_id);
				foreach ($images as $image) {
					$image_id = $image->id;

					$image->id = null;
					$image->product_variant_id = $new_variant_id;

					$new_image_id = $variant_image_mapper->save($image);

					//get and clone image tags
					$image_tags = $variant_image_tag_mapper->fetchByProductImageId($image_id);
					foreach ($image_tags as $image_tag) {
						$variant_image_tag_mapper->save(new Application_Model_ProductVariantImageTag(array('product_vatiant_image_id'=>$new_image_id,'image_tag_id'=>$image_tag['image_tag_id'],'sort_order'=>$image_tag['sort_order'])));
					}
				}

				//get and clone videos
				$videos = $variant_video_mapper->fetchOrderedByProductVariantId($variant_id);
				foreach ($videos as $video) {
					$video->id = null;
					$video->product_variant_id = $new_variant_id;

					$variant_video_mapper->save($video);
				}

				//get and clone media
				$medias = $variant_media_mapper->fetchOrderedByProductVariantId($variant_id);
				foreach ($medias as $media) {
					$media_id = $media->id;

					$media->id = null;
					$media->product_variant_id = $new_variant_id;

					$new_media_id = $variant_media_mapper->save($media);

					//get and clone media tags
					$media_tags = $variant_media_tag_mapper->fetchByProductMediaId($media_id);
					foreach ($media_tags as $media_tag) {
						$variant_media_tag_mapper->save(new Application_Model_ProductVariantMediaTag(array('product_vatiant_media_id'=>$new_media_id,'media_tag_id'=>$media_tag['media_tag_id'],'sort_order'=>$media_tag['sort_order'])));
					}
				}

				//get and clone attributes
				$attributes = $variant_profile_mapper->getProfileByProductVariantId($variant_id);
				foreach ($attributes as $attribute) {
					$variant_profile_mapper->save(new Application_Model_ProductVariantProfile(array('product_variant_id' => $new_variant_id, 'product_attribute_id' => $attribute['product_attribute_id'], 'value' => $attribute['value'])));
				}
			}
		}

		$this->redirect("/admin/product/edit/id/" . $new_product_id);
	}

	public function imageViewAction()
	{
		$this->view->product_id = $product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){
			$mapper = new Admin_Model_Mapper_ProductImage();
			$this->view->images = $mapper->fetchOrderedByProductId($product_id);
		}
	}

	public function imageAddAction()
	{
		$product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){

			$form = new Admin_Form_ProductImage();

			$data = array(
				'product_id' => $product_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('image-form');
		}
	}

	public function imageEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_ProductImage();
			$mapper = new Admin_Model_Mapper_ProductImage();
			$item = $mapper->find($id);

			$data = $item->toArray();

			//tags
			$t_mapper = new Admin_Model_Mapper_ImageTag();
			$data['tags'] = implode("|",$t_mapper->fetchTagNamesByProductImageId($id));

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('image-form');
		}
	}

	public function imageSaveAction()
	{
		$form = new Admin_Form_ProductImage();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Admin_Model_Mapper_ProductImage();
			$item = new Application_Model_ProductImage($data);
			$id = $mapper->save($item);

			if ($data['primary'] == 'Yes') {
				$mapper->setPrimaryByProductId($data['product_id'], $id);
			}
			if ($data['secondary'] == 'Yes') {
				$mapper->setSecondaryByProductId($data['product_id'], $id);
			}

			//tag groups
			$t_mapper = new Admin_Model_Mapper_ImageTag();
			$tc_mapper = new Admin_Model_Mapper_ProductImageTag();

			$tc_mapper->deleteByProductImageId($id);

			$tags = explode("|",$data['tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Application_Model_ImageTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Application_Model_ProductImageTag(array('product_image_id' => $id, 'image_tag_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			$this->getResponse()->setRedirect("/admin/product/image-view/product_id/{$data['product_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('image-form');
		}
	}


	public function imageDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$product_id = $this->getRequest()->getParam('product_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductImage();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/product/image-view/product_id/{$product_id}");
	}

	public function imageSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_ProductImage();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function videoViewAction()
	{
		$this->view->product_id = $product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){
			$mapper = new Admin_Model_Mapper_ProductVideo();
			$this->view->videos = $mapper->fetchOrderedByProductId($product_id);
		}
	}

	public function videoAddAction()
	{
		$product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){

			$form = new Admin_Form_ProductVideo();

			$data = array(
				'product_id' => $product_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('video-form');
		}
	}

	public function videoEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_ProductVideo();
			$mapper = new Admin_Model_Mapper_ProductVideo();
			$item = $mapper->find($id);

			$data = $item->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('video-form');
		}
	}

	public function videoSaveAction()
	{
		$form = new Admin_Form_ProductVideo();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Admin_Model_Mapper_ProductVideo();
			$item = new Application_Model_ProductVideo($data);
			$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/product/video-view/product_id/{$data['product_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('video-form');
		}
	}


	public function videoDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$product_id = $this->getRequest()->getParam('product_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductVideo();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/product/video-view/product_id/{$product_id}");
	}

	public function videoSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_ProductVideo();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function mediaViewAction()
	{
		$this->view->product_id = $product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){
			$mapper = new Admin_Model_Mapper_ProductMedia();
			$this->view->medias = $mapper->fetchOrderedByProductId($product_id);
		}
	}

	public function mediaAddAction()
	{
		$product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){

			$form = new Admin_Form_ProductMedia();

			$data = array(
				'product_id' => $product_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('media-form');
		}
	}

	public function mediaEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_ProductMedia();
			$mapper = new Admin_Model_Mapper_ProductMedia();
			$item = $mapper->find($id);

			$data = $item->toArray();

			//tags
			$t_mapper = new Admin_Model_Mapper_MediaTag();
			$data['tags'] = implode("|",$t_mapper->fetchTagNamesByProductMediaId($id));

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('media-form');
		}
	}

	public function mediaSaveAction()
	{
		$form = new Admin_Form_ProductMedia();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Admin_Model_Mapper_ProductMedia();
			$item = new Application_Model_ProductMedia($data);
			$id = $mapper->save($item);

			if ($data['primary'] == 'Yes') {
				$mapper->setPrimaryByProductId($data['product_id'], $id);
			}
			if ($data['secondary'] == 'Yes') {
				$mapper->setSecondaryByProductId($data['product_id'], $id);
			}

			//tag groups
			$t_mapper = new Admin_Model_Mapper_MediaTag();
			$tc_mapper = new Admin_Model_Mapper_ProductMediaTag();

			$tc_mapper->deleteByProductMediaId($id);

			$tags = explode("|",$data['tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Application_Model_MediaTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Application_Model_ProductMediaTag(array('product_media_id' => $id, 'media_tag_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			$this->getResponse()->setRedirect("/admin/product/media-view/product_id/{$data['product_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('media-form');
		}
	}


	public function mediaDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$product_id = $this->getRequest()->getParam('product_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductMedia();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/product/media-view/product_id/{$product_id}");
	}

	public function mediaSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_ProductMedia();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function profileViewAction()
	{
		$this->view->product_id = $product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){
			$mapper = new Admin_Model_Mapper_ProductProfile();
			$this->view->profile = $mapper->getProfileByProductId($product_id);
		}
	}

	public function profileAddAction()
	{
		$product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){

			$form = new Admin_Form_ProductProfile();

			$data = array(
				'product_id' => $product_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('profile-form');
		}
	}

	public function profileEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_ProductProfile();
			$mapper = new Admin_Model_Mapper_ProductProfile();
			$item = $mapper->find($id);

			$form->populate($item->toArray());
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';
			$this->view->attribute_id = $item->product_attribute_id;

			$this->render('profile-form');
		}
	}

	public function profileSaveAction()
	{
		$form = new Admin_Form_ProductProfile();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$mapper = new Admin_Model_Mapper_ProductProfile();
			$item = new Application_Model_ProductProfile($data);
			$mapper->save($item);

			//clear cache
			$cache_id = 'product_profile_' . $data['product_id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->remove($cache_id);

			$this->getResponse()->setRedirect("/admin/product/profile-view/product_id/{$data['product_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('profile-form');
		}
	}

	public function profileDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$product_id = $this->getRequest()->getParam('product_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductProfile();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		//clear cache
		$cache_id = 'product_profile_' . $product_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->remove($cache_id);

		$this->getResponse()->setRedirect("/admin/product/profile-view/product_id/{$product_id}");
	}

	public function attributeAddAction()
	{
		//
	}

	public function attributeSaveAction()
	{
		$data = $this->getRequest()->getParam('data', array());

		$mapper = new Admin_Model_Mapper_ProductAttribute();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify($data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $this->getRequest()->isPost() && trim($data['title']) ){
			$entity = new Application_Model_ProductAttribute($data);
			$id = $mapper->save($entity);
		}

		echo "1";
		exit;
	}

	public function attributeJsonSearchAction()
	{
		$search_results = array();

		$sort = 'title';
		$dir = 'asc';

		$q = $this->getRequest()->getParam('q', '');
		$id = $this->getRequest()->getParam('id', '');

		if ($q || $id) {
			$search_keys = array(
				'keyword' => $q,
				'id' => $id
			);
			$mapper = new Admin_Model_Mapper_ProductAttribute();
			$results = $mapper->listSearch($sort, $dir, $search_keys);

			$stmt = $this->_db->query($results->__toString());
			$search_results = $stmt->fetchAll();
		}

		$return = array();
		foreach ($search_results as $row) {
			$return[] = array(
				'id' => $row['id'],
				'title' => $row['title']
			);
		}

		echo json_encode($return);
		exit;
	}

	public function customViewAction()
	{
		$this->view->product_id = $product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){

		}
		exit;
	}

	public function jsonSearchAction()
	{
		$search_results = array();

		$sort = 'created';
		$dir = 'desc';

		$q = $this->getRequest()->getParam('q', '');
		$id = $this->getRequest()->getParam('id', '');

		if ($q || $id) {
			$search_keys = array(
				'keyword' => $q,
				'id' => $id,
				'type' => null,
				'status' => 'Enabled',
				'alpha' => null
			);
			$mapper = new Admin_Model_Mapper_Product();
			$results = $mapper->listSearch($sort, $dir, $search_keys);

			$stmt = $this->_db->query($results->__toString());
			$search_results = $stmt->fetchAll();
		}

		$return = array();
		foreach ($search_results as $row) {
			$return[] = array(
				'id' => $row['id'],
				'title' => $row['title'],
				'sku' => $row['sku']
			);
		}

		echo json_encode($return);
		exit;
	}

	public function variantViewAction()
	{
		$this->view->product_id = $product_id = $this->getRequest()->getParam('product_id', 0);
		if(!empty($product_id) && is_numeric($product_id)){
			$mapper = new Admin_Model_Mapper_Product();
			$this->view->product = $mapper->find($product_id);

			//search query params
			$this->view->mask = $mask = $this->getRequest()->getParam('mask', '');
			$this->view->type = $type = $this->getRequest()->getParam('type', '');
			$this->view->status = $status = $this->getRequest()->getParam('status', '');

			$mapper = new Admin_Model_Mapper_ProductVariant();
			$this->view->variants = $mapper->getVariantsByProductId($product_id, $type, $status, $mask);

			$mapper = new Admin_Model_Mapper_ProductVariantType();
			$this->view->variant_types = $mapper->fetchIdPairs();

			$this->view->variant_statuses = array("Enabled"=>"Enabled","Disabled"=>"Disabled");
		}
	}

	public function variantAddAction()
	{
		$this->view->product_id = $product_id = $this->getRequest()->getParam('product_id', 0);

		$mapper = new Admin_Model_Mapper_Product();
		$product = $mapper->find($product_id);

		if ($product) {
			$this->view->placeholder('page_title')->set("Product Variant");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/product">Products</a></li>
				<li><a href="/admin/product/edit/id/' . $product->id . '">' . $product->title . '</a></li>
				<li class="active">Add Product Variant</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_ProductVariant();
			$form->getElement('cmd')->setValue('add');
			$form->getElement('parent_product_id')->setValue($product->id);

			$this->view->cmd = 'add';
			$this->view->form = $form;
			$this->render('variant-add-form');
		}else{
			$this->getResponse()->setRedirect('/admin/product/list');
		}
	}

	public function variantEditAction()
	{
		$this->view->product_variant_id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$form = new Admin_Form_ProductVariant();
			$mapper = new Admin_Model_Mapper_ProductVariant();
			$this->view->variant = $entity = $mapper->find($id);

			$mapper = new Admin_Model_Mapper_Product();
			$this->view->product = $product = $mapper->find($entity->parent_product_id);

			$this->view->placeholder('page_title')->set("Product Variant");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/product">Products</a></li>
				<li><a href="/admin/product/edit/id/'.$entity->parent_product_id.'">'.$product->title.'</a></li>
				<li class="active">Edit Product Variant: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('variant-form');
		}else{
			$this->getResponse()->setRedirect('/admin/product/list');
		}
	}

	public function variantSaveAction()
	{
		$form = new Admin_Form_ProductVariant();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_ProductVariant();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($this->getRequest()->getParams())) {
			if (!$data['title_sort']) $data['title_sort'] = $data['title'];
			if (!$data['sort_order']) $data['sort_order'] = 99999;

			if ($data['available_date']) {
				$data['available_date'] = date("Y-m-d",strtotime($data['available_date']));
			}

			$entity = new Application_Model_ProductVariant($data);
			$id = $mapper->save($entity);

			if ($data['cmd'] == 'add') {
				$this->getResponse()->setRedirect('/admin/product/variant-edit/id/'.$id);
			}else {
				$this->getResponse()->setRedirect('/admin/product/edit/id/'.$data['parent_product_id']);
			}
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$this->view->product_variant_id = $data['id'];

			$form->populate($data);
			$this->view->form = $form;

			if ($data['cmd'] == 'add') {
				$this->render('variant-add-form');
			}else {
				$this->render('variant-form');
			}
		}
	}

	public function variantDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$product_id = $this->getRequest()->getParam('product_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductVariant();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/product/variant-view/product_id/{$product_id}");
	}

	public function variantSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_ProductVariant();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function variantCopyAction()
	{
		$id = $this->getRequest()->getParam('id', 0); //id of product variant to clone
		$auto_enable = $this->getRequest()->getParam('ae','no');

		$variant_mapper = new Admin_Model_Mapper_ProductVariant();
		$variant_profile_mapper = new Admin_Model_Mapper_ProductVariantProfile();
		$variant_image_mapper = new Admin_Model_Mapper_ProductVariantImage();
		$variant_image_tag_mapper = new Admin_Model_Mapper_ProductVariantImageTag();
		$variant_video_mapper = new Admin_Model_Mapper_ProductVariantVideo();
		$variant_media_mapper = new Admin_Model_Mapper_ProductVariantMedia();
		$variant_media_tag_mapper = new Admin_Model_Mapper_ProductVariantMediaTag();

		//get and clone variant
		$variant = $variant_mapper->find($id);

		$variant->id = null;
		$variant->title = $variant->title . " COPY";
		$variant->status = ($auto_enable == 'yes')?'Enabled':'Disabled';

		$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
		$code = $orig_code = $slugify->slugify($variant->title);
		$counter = 1;
		while ($variant_mapper->doesExists(array('code' => $code)) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$variant->code = $code;

		$new_variant_id = $variant_mapper->save($variant);

		//get and clone images
		$images = $variant_image_mapper->fetchOrderedByProductVariantId($id);
		foreach ($images as $image) {
			$image_id = $image->id;

			$image->id = null;
			$image->product_variant_id = $new_variant_id;

			$new_image_id = $variant_image_mapper->save($image);

			//get and clone image tags
			$image_tags = $variant_image_tag_mapper->fetchByProductImageId($image_id);
			foreach ($image_tags as $image_tag) {
				$image_tag->id = null;
				$image_tag->product_variant_image_id = $new_image_id;

				$variant_image_tag_mapper->save($image_tag);
			}
		}

		//get and clone videos
		$videos = $variant_video_mapper->fetchOrderedByProductVariantId($id);
		foreach ($videos as $video) {
			$video->id = null;
			$video->product_variant_id = $new_variant_id;

			$variant_video_mapper->save($video);
		}

		//get and clone medias
		$medias = $variant_media_mapper->fetchOrderedByProductVariantId($id);
		foreach ($medias as $media) {
			$media_id = $media->id;

			$media->id = null;
			$media->product_variant_id = $new_variant_id;

			$new_media_id = $variant_media_mapper->save($media);

			//get and clone media tags
			$media_tags = $variant_media_tag_mapper->fetchByProductMediaId($media_id);
			foreach ($media_tags as $media_tag) {
				$media_tag->id = null;
				$media_tag->product_variant_media_id = $new_media_id;

				$variant_media_tag_mapper->save($media_tag);
			}
		}

		//get and clone attributes
		$attributes = $variant_profile_mapper->getProfileByProductVariantId($id);
		foreach ($attributes as $attribute) {
			$variant_profile_mapper->save(new Application_Model_ProductVariantProfile(array('product_variant_id' => $new_variant_id, 'product_attribute_id' => $attribute['product_attribute_id'], 'value' => $attribute['value'])));
		}

		$this->getResponse()->setRedirect('/admin/product/variant-edit/id/'.$new_variant_id);
	}

	public function variantImageViewAction()
	{
		$this->view->product_variant_id = $product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		if(!empty($product_variant_id) && is_numeric($product_variant_id)){
			$mapper = new Admin_Model_Mapper_ProductVariantImage();
			$this->view->images = $mapper->fetchOrderedByProductVariantId($product_variant_id);
		}
	}

	public function variantImageAddAction()
	{
		$product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		if(!empty($product_variant_id) && is_numeric($product_variant_id)){

			$form = new Admin_Form_ProductVariantImage();

			$data = array(
				'product_variant_id' => $product_variant_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('variant-image-form');
		}
	}

	public function variantImageEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_ProductVariantImage();
			$mapper = new Admin_Model_Mapper_ProductVariantImage();
			$item = $mapper->find($id);

			$data = $item->toArray();

			//tags
			$t_mapper = new Admin_Model_Mapper_ImageTag();
			$data['tags'] = implode("|",$t_mapper->fetchTagNamesByProductVariantImageId($id));

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('variant-image-form');
		}
	}

	public function variantImageSaveAction()
	{
		$form = new Admin_Form_ProductVariantImage();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Admin_Model_Mapper_ProductVariantImage();
			$item = new Application_Model_ProductVariantImage($data);
			$id = $mapper->save($item);

			if ($data['primary'] == 'Yes') {
				$mapper->setPrimaryByProductVariantId($data['product_variant_id'], $id);
			}
			if ($data['secondary'] == 'Yes') {
				$mapper->setSecondaryByProductVariantId($data['product_variant_id'], $id);
			}

			//tag groups
			$t_mapper = new Admin_Model_Mapper_ImageTag();
			$tc_mapper = new Admin_Model_Mapper_ProductVariantImageTag();

			$tc_mapper->deleteByProductVariantImageId($id);

			$tags = explode("|",$data['tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Application_Model_ImageTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Application_Model_ProductVariantImageTag(array('product_variant_image_id' => $id, 'image_tag_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			$this->getResponse()->setRedirect("/admin/product/variant-image-view/product_variant_id/{$data['product_variant_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('variant-image-form');
		}
	}


	public function variantImageDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductVariantImage();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/product/variant-image-view/product_variant_id/{$product_variant_id}");
	}

	public function variantImageSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_ProductVariantImage();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function variantVideoViewAction()
	{
		$this->view->product_variant_id = $product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		if(!empty($product_variant_id) && is_numeric($product_variant_id)){
			$mapper = new Admin_Model_Mapper_ProductVariantVideo();
			$this->view->videos = $mapper->fetchOrderedByProductVariantId($product_variant_id);
		}
	}

	public function variantVideoAddAction()
	{
		$product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		if(!empty($product_variant_id) && is_numeric($product_variant_id)){

			$form = new Admin_Form_ProductVariantVideo();

			$data = array(
				'product_variant_id' => $product_variant_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('variant-video-form');
		}
	}

	public function variantVideoEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_ProductVariantVideo();
			$mapper = new Admin_Model_Mapper_ProductVariantVideo();
			$item = $mapper->find($id);

			$data = $item->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('variant-video-form');
		}
	}

	public function variantVideoSaveAction()
	{
		$form = new Admin_Form_ProductVariantVideo();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Admin_Model_Mapper_ProductVariantVideo();
			$item = new Application_Model_ProductVariantVideo($data);
			$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/product/variant-video-view/product_variant_id/{$data['product_variant_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('variant-video-form');
		}
	}


	public function variantVideoDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductVariantVideo();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/product/variant-video-view/product_variant_id/{$product_variant_id}");
	}

	public function variantVideoSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_ProductVariantVideo();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function variantMediaViewAction()
	{
		$this->view->product_variant_id = $product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		if(!empty($product_variant_id) && is_numeric($product_variant_id)){
			$mapper = new Admin_Model_Mapper_ProductVariantMedia();
			$this->view->medias = $mapper->fetchOrderedByProductVariantId($product_variant_id);
		}
	}

	public function variantMediaAddAction()
	{
		$product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		if(!empty($product_variant_id) && is_numeric($product_variant_id)){

			$form = new Admin_Form_ProductVariantMedia();

			$data = array(
				'product_variant_id' => $product_variant_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('variant-media-form');
		}
	}

	public function variantMediaEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_ProductVariantMedia();
			$mapper = new Admin_Model_Mapper_ProductVariantMedia();
			$item = $mapper->find($id);

			$data = $item->toArray();

			//tags
			$t_mapper = new Admin_Model_Mapper_MediaTag();
			$data['tags'] = implode("|",$t_mapper->fetchTagNamesByProductVariantMediaId($id));

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('variant-media-form');
		}
	}

	public function variantMediaSaveAction()
	{
		$form = new Admin_Form_ProductVariantMedia();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Admin_Model_Mapper_ProductVariantMedia();
			$item = new Application_Model_ProductVariantMedia($data);
			$id = $mapper->save($item);

			if ($data['primary'] == 'Yes') {
				$mapper->setPrimaryByProductVariantId($data['product_variant_id'], $id);
			}
			if ($data['secondary'] == 'Yes') {
				$mapper->setSecondaryByProductVariantId($data['product_variant_id'], $id);
			}

			//tag groups
			$t_mapper = new Admin_Model_Mapper_MediaTag();
			$tc_mapper = new Admin_Model_Mapper_ProductVariantMediaTag();

			$tc_mapper->deleteByProductVariantMediaId($id);

			$tags = explode("|",$data['tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Application_Model_MediaTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Application_Model_ProductVariantMediaTag(array('product_variant_media_id' => $id, 'media_tag_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			$this->getResponse()->setRedirect("/admin/product/variant-media-view/product_variant_id/{$data['product_variant_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('variant-media-form');
		}
	}


	public function variantMediaDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductVariantMedia();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/product/variant-media-view/product_variant_id/{$product_variant_id}");
	}

	public function variantMediaSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_ProductVariantMedia();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function variantProfileViewAction()
	{
		$this->view->product_variant_id = $product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		if(!empty($product_variant_id) && is_numeric($product_variant_id)){
			$mapper = new Admin_Model_Mapper_ProductVariantProfile();
			$this->view->profile = $mapper->getProfileByProductVariantId($product_variant_id);
		}
	}

	public function variantProfileAddAction()
	{
		$product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		if(!empty($product_variant_id) && is_numeric($product_variant_id)){

			$form = new Admin_Form_ProductVariantProfile();

			$data = array(
				'product_variant_id' => $product_variant_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('variant-profile-form');
		}
	}

	public function variantProfileEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_ProductVariantProfile();
			$mapper = new Admin_Model_Mapper_ProductVariantProfile();
			$item = $mapper->find($id);

			$form->populate($item->toArray());
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';
			$this->view->attribute_id = $item->product_attribute_id;

			$this->render('variant-profile-form');
		}
	}

	public function variantProfileSaveAction()
	{
		$form = new Admin_Form_ProductVariantProfile();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$mapper = new Admin_Model_Mapper_ProductVariantProfile();
			$item = new Application_Model_ProductVariantProfile($data);
			$mapper->save($item);

			//clear cache
			$cache_id = 'product_variant_profile_' . $data['product_variant_id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->remove($cache_id);

			$this->getResponse()->setRedirect("/admin/product/variant-profile-view/product_variant_id/{$data['product_variant_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('variant-profile-form');
		}
	}

	public function variantProfileDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductVariantProfile();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		//clear cache
		$cache_id = 'product_variant_profile_' . $product_variant_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->remove($cache_id);

		$this->getResponse()->setRedirect("/admin/product/variant-profile-view/product_variant_id/{$product_variant_id}");
	}

	public function variantCustomViewAction()
	{
		$this->view->product_variant_id = $product_variant_id = $this->getRequest()->getParam('product_variant_id', 0);
		if(!empty($product_variant_id) && is_numeric($product_variant_id)){

		}
		exit;
	}

	public function loadAttributeInputAction()
	{
		$product_id = $this->getRequest()->getParam('product_id',0);
		$product_variant_id = $this->getRequest()->getParam('product_variant_id',0);
		$attribute_id = $this->getRequest()->getParam('attribute_id',0);
		$mapper = new Admin_Model_Mapper_ProductAttribute();
		$attribute = $mapper->find($attribute_id);
		$field_type = (isset($attribute->field_type))?$attribute->field_type:'text';

		$item = $value = $options = null;
		if ($product_id) {
			$mapper = new Admin_Model_Mapper_ProductProfile();
			$item = $mapper->getProfileByProductId($product_id);
		}elseif($product_variant_id) {
			$mapper = new Admin_Model_Mapper_ProductVariantProfile();
			$item = $mapper->getProfileByProductVariantId($product_variant_id);
		}
		if ($item && $attribute) {
			$value = $item[$attribute->code]['value'];
			$options = (isset($item[$attribute->code]['options']))?json_decode($item[$attribute->code]['options']):array();
		}

		echo $this->view->partial('/product/partials/'.$field_type.'.phtml', array('value'=>$value,'options'=>$options));

		exit;
	}

	public function ajaxListAction()
	{
		$type = $this->_getParam('type','all');

		$search_keys = array(
			'keyword' => $this->_getParam('q',''),
			'status' => $this->_getParam('status','')
		);
		if ($id = $this->getRequest()->getParam('id','')) {
			$type = 'product';
			$split = explode("~~", $id);
			if (is_array($split) && count($split) == 2) {
				if ($split[0] == 'product') {
					$id = $split[1];
				} elseif ($split[0] == 'variant') {
					$id = $split[1];
					$type = 'variant';
				}
			}

			$search_keys = array(
				'id' => $id
			);
		}

		$array = array();

		if ($type == 'all' || $type == 'product') {
			$mapper = new Admin_Model_Mapper_Product();
			$results = $mapper->listSearch('title_sort', 'ASC', $search_keys);

			$stmt = $this->_db->query($results->__toString());
			$items = $stmt->fetchAll();

			foreach ($items as $item) {
				$array['results'][] = array("id" => $item['id'], "text" => "{$item['title']} {$item['sku']}", "product_title" => $item['title'], "sku" => $item['sku'], 'type' => 'product');
			}
		}

		if ($type == 'all' || $type == 'variant') {
			$mapper = new Admin_Model_Mapper_ProductVariant();
			$results = $mapper->listSearch('title_sort', 'ASC', $search_keys);

			$stmt = $this->_db->query($results->__toString());
			$items = $stmt->fetchAll();

			foreach ($items as $item) {
				$array['results'][] = array("id" => $item['id'], "text" => "{$item['title']} [{$item['type']}] {$item['sku']}", "product_title" => $item['title'], "sku" => $item['sku'], 'type' => 'variant');
			}
		}

		echo json_encode($array);exit;
	}

	public function variantTypeAddAction()
	{
		//
	}

	public function variantTypeSaveAction()
	{
		$data = $this->getRequest()->getParam('data', array());

		$mapper = new Admin_Model_Mapper_ProductVariantType();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify($data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $this->getRequest()->isPost() && trim($data['title']) ){
			$entity = new Application_Model_ProductVariantType($data);
			$id = $mapper->save($entity);
		}

		echo "1";
		exit;
	}

	public function variantTypeAjaxAction()
	{
		$id = $this->getRequest()->getParam("id");

		$mapper = new Admin_Model_Mapper_ProductVariantType();
		$data = $mapper->find($id);

		if ($data) {
			echo json_encode($data);
		}

		exit;
	}

	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'ecommerce', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled',
			'Unavailable' => 'Unavailable',
			'Backordered' => 'Backordered'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');

	}
}
