<?php
use Cocur\Slugify\Slugify;

class Admin_CmsController extends Zend_Controller_Action
{
	/** @var  Zend_Db_Adapter_Abstract */
	protected $_db;
	static $_cache_manager;

	public function indexAction()
	{
		$this->view->messages = $this->_flashMessenger->getMessages();
		$this->view->placeholder('page_title')->set("CMS");
	}

	public function pageListAction()
	{
		$this->view->placeholder('section')->set("detailview page-list-view");
		$this->view->messages = $this->_flashMessenger->getMessages();
		$this->view->placeholder('page_title')->set("Page Manager");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/cms">CMS</a></li><li class="active">Page Manager</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page', 1);
		$this->view->dir = $dir = $this->_getParam('dir', 'ASC');
		$this->view->sort = $sort = $this->_getParam('sort', 'title');

		$this->view->mask = $mask = trim($this->_getParam('mask', ''));
		$this->view->code = $code = trim($this->_getParam('code', ''));
		$this->view->status = $status = $this->_getParam('status', '');
		$this->view->alpha = $alpha = $this->_getParam('alpha', '');

		$search_keys = array(
			'keyword' => $mask,
			'code' => $code,
			'status' => $status,
			'alpha' => $alpha,
			'editable' => 'Yes',
		);
		$mapper = new Admin_Model_Mapper_Page();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function pageAddAction()
	{
		$this->view->placeholder('page_title')->set("Page Manager - Add");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/cms/page-list">Page Manager</a></li><li class="active">Add Page</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_Page();
		$form->getElement('cmd')->setValue('add');
		$form->getElement('editable')->setValue('Yes');
		$form->getElement('block_type')->setValue('page');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->view->back_url = 'cms/page-list';

		$this->render('form');
	}

	public function blockAddAction()
	{
		$this->view->placeholder('page_title')->set("Block Manager - Add Block");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/cms/block-list">Page Manager</a></li><li class="active">Add Block</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_Page();
		$form->getElement('cmd')->setValue('add');
		$form->getElement('editable')->setValue('Section');
		$form->getElement('block_type')->setValue('block');
		$form->getElement('title')->setLabel('Block Title');
		$form->removeElement('code');
		$form->removeElement('meta_title');
		$form->removeElement('meta_keywords');
		$form->removeElement('meta_description');
		$form->removeElement('tracking_code');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->view->back_url = 'cms/block-list';

		$this->render('form');
	}

	public function modalAddAction()
	{
		$this->view->placeholder('page_title')->set("Block Manager - Add Modal");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/cms/block-list">Page Manager</a></li><li class="active">Add Modal</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_Page();
		$form->getElement('cmd')->setValue('add');
		$form->getElement('editable')->setValue('Section');
		$form->getElement('block_type')->setValue('modal');
		$form->getElement('title')->setLabel('Modal Title');
		$form->removeElement('code');
		$form->removeElement('meta_title');
		$form->removeElement('meta_keywords');
		$form->removeElement('meta_description');
		$form->removeElement('tracking_code');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->view->back_url = 'cms/block-list';

		$this->render('form');
	}

	public function navPanelAddAction()
	{
		$this->view->placeholder('page_title')->set("Block Manager - Add Navigation Panel");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/cms/block-list">Page Manager</a></li><li class="active">Add Navigation Panel</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_Page();
		$form->getElement('cmd')->setValue('add');
		$form->getElement('editable')->setValue('Section');
		$form->getElement('block_type')->setValue('navigation');
		$form->getElement('title')->setLabel('Navigation Panel Title');
		$form->removeElement('code');
		$form->removeElement('meta_title');
		$form->removeElement('meta_keywords');
		$form->removeElement('meta_description');
		$form->removeElement('tracking_code');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->view->back_url = 'cms/block-list';

		$this->render('form');
	}

	public function pageEditAction()
	{
		$this->view->placeholder('page_title')->set("Page Manager - Edit");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/cms/page-list">Page Manager</a></li><li class="active">Edit Page</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->view->id = $id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {

			$form = new Admin_Form_Page();
			$mapper = new Admin_Model_Mapper_Page();
			$entity = $mapper->find($id);

			$form->populate($entity->toArray());
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';
			$this->view->back_url = 'cms/page-list';

			$this->render('form');
		} else {
			$this->getResponse()->setRedirect('/admin/cms/page-list');
		}
	}

	public function pageSaveAction()
	{
		$this->view->placeholder('page_title')->set("Page Manager");

		$form = new Admin_Form_Page();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_Page();

		$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if ($form->isValid($this->getRequest()->getParams())) {
			if ($data['cmd'] == 'add') {
				$data['created_by_user_id'] = $this->view->user->id;
			}
			$data['last_modified_by_user_id'] = $this->view->user->id;

			$entity = new Application_Model_Page($data);
			$id = $mapper->save($entity);

			if ($data['status'] == 'Enabled') {
				//delete cache
				$cache_id = 'page_' . $id;
				$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
				$cache->clean(
					Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
					array('page_' . $id)
				);
				$cache->remove($cache_id);

				//css file cache
				$this->_cache_manager->refreshCSS($id);
				/*
				$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
				$cache->remove('css_site');
				$cache->remove('css_site'.$id);
				*/
			}

			$this->getResponse()->setRedirect('/admin/cms/page-editor/id/' . $id);
		} else {
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->view->back_url = 'cms/page-list';

			$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/cms/page-list">Page Manager</a></li><li class="active">' . ucwords($data['cmd']) . ' Page</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$this->render('form');
		}
	}

	public function pageSeoEditAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);

		$mapper = new Admin_Model_Mapper_Page();
		$this->view->page = $mapper->find($page_id);
	}

	public function pageSavePartialAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);
		$data = ($this->getRequest()->getParam('data', array()));

		$mapper = new Admin_Model_Mapper_Page();
		$data['id'] = $page_id;

		if (isset($data['code']) && $data['code']) {
			$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
			$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
			$counter = 1;
			while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
				$code = $orig_code . "-" . $counter;
				$counter++;
			}
			$data['code'] = $code;

			//update URL in navigation
			$nav_mapper = new Admin_Model_Mapper_NavigationItem();
			$nav_entity = $nav_mapper->findByPageId($page_id);

			if (!$nav_entity) {
				$nav_entity = new Application_Model_NavigationItem();
				$nav_entity->page_id = $page_id;
			}

			$nav_entity->url = "/{$data['code']}";
			$nav_entity->title = $data['title'];
			$nav_mapper->save($nav_entity);

			//delete cache
			$cache_id = 'navigation';
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('navigation')
			);
			$cache->remove($cache_id);
		}

		//ignore password field
		unset($data['password']);

		$data['last_modified_by_user_id'] = $this->view->user->id;

		$entity = $mapper->find($data['id']);

		$entity_data = array_merge($entity->toArray(), $data);
		$mapper->save(new Application_Model_Page($entity_data));

		if ($entity_data['status'] == 'Enabled') {
			//delete cache
			$cache_id = 'page_' . $data['id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $data['id'])
			);
			$cache->remove($cache_id);

			if ($entity->status == 'Disabled') { //if it just went from disabled to enabled
				//css file cache
				$this->_cache_manager->refreshCSS($data['id']);
				/*
				$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
				$cache->remove('css_site');
				$cache->remove('css_site'.$data['id']);
				*/
			}
		}

		echo json_encode($data);exit;
	}

	public function slugTitleAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);
		$title = $this->getRequest()->getParam('title', '');

		$mapper = new Admin_Model_Mapper_Page();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify($title);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $page_id) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}

		echo $code;
		exit;
	}

	public function pageDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_Page();
			$entity = $mapper->find($id);
		}

		if ($entity->system_block == 'No') {
			$mapper->delete($entity);

			//delete from navigation
			$nav_mapper = new Admin_Model_Mapper_NavigationItem();
			$nav_entity = $nav_mapper->findByPageId($id);
			if ($nav_entity) {
				$nav_mapper->delete($nav_entity);
			}

			//delete cache
			$cache_id = 'navigation';
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('navigation')
			);
			$cache->remove($cache_id);
		}

		if ($entity->editable == 'No' || $entity->editable == 'Row' || $entity->editable == 'Section') {
			$this->getResponse()->setRedirect('/admin/cms/block-list');
		}else {
			$this->getResponse()->setRedirect('/admin/cms/page-list');
		}
	}

	public function pageEditorAction()
	{
		$this->view->placeholder('section')->set("page-editor");

		$this->view->id = $id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_Page();
			$this->view->page = $entity = $mapper->find($id);

			//get pages enabled in nav
			$mapper = new Admin_Model_Mapper_NavigationItem();
			$this->view->nav_pages = [];
			$nav_pages = $mapper->mainNavPagePairs(true);
			foreach ($nav_pages as $nav_id => $nav_row) {
				if ($nav_row->page_id != $id) {
					$this->view->nav_pages[$nav_row->id] = $nav_row->title;
				}
			}

			//get this page's nav settings
			$this->view->page_nav = $mapper->findByPageId($id);

			$mapper = new Admin_Model_Mapper_PageSection();
			$this->view->sections = $mapper->getPageSections($id);
			if (!$this->view->sections) {
				$mapper->addSection($id, 'standard');
				$this->view->sections = $mapper->getPageSections($id);
			}
		} else {
			$this->getResponse()->setRedirect('/admin/cms/page-list');
		}
	}

	public function pageCopyAction()
	{
		$id = $this->getRequest()->getParam('id', 0); //id of page to clone
		$auto_enable = $this->getRequest()->getParam('ae','no');

		$page_mapper = new Admin_Model_Mapper_Page();
		$section_mapper = new Admin_Model_Mapper_PageSection();
		$row_mapper = new Admin_Model_Mapper_PageSectionRow();
		$column_mapper = new Admin_Model_Mapper_PageSectionRowColumn();
		$module_mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();

		//get and clone page
		$page = $page_mapper->find($id);
		$page->id = null;
		$page->title = $page->title . " COPY";
		$page->status = ($auto_enable == 'yes')?'Enabled':'Disabled';

		$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
		$code = $orig_code = $slugify->slugify($page->title);
		$counter = 1;
		while ($page_mapper->doesExists(array('code' => $code)) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$page->code = $code;

		$new_page_id = $page_mapper->save($page);

		//get and clone sections
		$sections = $section_mapper->getPageSections($id);
		foreach ($sections as $section) {
			$section_id = $section->id;

			$section->id = null;
			$section->page_id = $new_page_id;

			$new_section_id = $section_mapper->save($section);

			//get and clone section rows
			$rows = $row_mapper->getPageSectionRows($section_id);
			foreach ($rows as $row) {
				$row_id = $row->id;

				$row->id = null;
				$row->page_section_id = $new_section_id;

				$new_row_id = $row_mapper->save($row);

				//get and clone row columns
				$columns = $column_mapper->getPageSectionRowColumns($row_id);
				foreach ($columns as $column) {
					$column_id = $column->id;

					$column->id = null;
					$column->page_section_row_id = $new_row_id;

					$new_column_id = $column_mapper->save($column);

					//get and clone column modules
					$modules = $module_mapper->getPageSectionRowColumnModules($column_id);
					foreach ($modules as $module) {
						$module->id = null;
						$module->page_section_row_column_id = $new_column_id;

						$module_mapper->save(new Application_Model_PageSectionRowColumnModule($module->toArray()));
					}
				}
			}
		}

		$this->redirect("/admin/cms/page-editor/id/" . $new_page_id);
	}

	public function pageSaveNavAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);
		$main_nav_status = $this->getRequest()->getParam('main_nav_status', null);
		$main_nav_parent_id = $this->getRequest()->getParam('main_nav_parent_id', null);

		$nav_mapper = new Admin_Model_Mapper_NavigationItem();
		$nav_entity = $nav_mapper->findByPageId($page_id);

		if (!$nav_entity) {
			$page_mapper = new Admin_Model_Mapper_Page();
			$page = $page_mapper->find($page_id);

			$nav_entity = new Application_Model_NavigationItem(array(
				'title' => $page->title,
				'url' => "/" . $page->code,
				'page_id' => $page_id,
				'status' => 'Enabled',
			));
		}

		if ($main_nav_status) {
			$nav_entity->main_nav_status = $main_nav_status;
		}

		if ($main_nav_parent_id) {
			$nav_entity->main_nav_parent_id = $main_nav_parent_id;
		}

		$nav_mapper->save($nav_entity);

		//delete cache
		$cache_id = 'navigation';
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('navigation')
		);
		$cache->remove($cache_id);

		//delete cache
		$cache_id = 'page_' . $page_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('page_' . $page_id)
		);
		$cache->remove($cache_id);

		exit;
	}

	public function pageSavePasswordAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);
		$password = $this->getRequest()->getParam('password', '');

		$mapper = new Admin_Model_Mapper_Page();

		if ($password) {
			$bcrypt = new Jr_Crypt_Password_Bcrypt();
			$data['password'] = $bcrypt->create($password);

			$data['last_modified_by_user_id'] = $this->view->user->id;

			$entity = $mapper->find($page_id);

			$entity_data = array_merge($entity->toArray(), $data);
			$mapper->save(new Application_Model_Page($entity_data));
		}

		exit;
	}

	public function pageSettingsEditAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);

		$mapper = new Admin_Model_Mapper_Page();
		$this->view->page = $mapper->find($page_id);
	}

	public function pageSettingsSaveAction()
	{
		$data = $this->getRequest()->getParam('data', array());
		$styles = $this->getRequest()->getParam('styles', array());
		$settings = $this->getRequest()->getParam('settings', array());

		//json encode all the content items and save into data json_content
		$data['json_styles'] = json_encode($styles);
		$data['json_settings'] = json_encode($settings);

		$mapper = new Admin_Model_Mapper_Page();
		$entity = $mapper->find($data['id']);

		$entity_data = array_merge($entity->toArray(), $data);
		$mapper->save(new Application_Model_Page($entity_data));

		if ($entity->status == 'Enabled' || (isset($data['status']) && $data['status'] == 'Enabled')) {
			//delete cache
			$cache_id = 'page_' . $data['id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $entity->id)
			);
			$cache->remove($cache_id);
			//css file cache
			$this->_cache_manager->refreshCSS($entity->id);
			/*
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$entity->id);
			*/
		}

		echo json_encode($entity_data);
		exit;
	}

	public function pageCustomVarsEditAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);

		$mapper = new Admin_Model_Mapper_Page();
		$this->view->page = $mapper->find($page_id);
	}

	public function pageCustomVarsSaveAction()
	{
		$data = $this->getRequest()->getParam('data', array());

		$mapper = new Admin_Model_Mapper_Page();
		$entity = $mapper->find($data['id']);

		$entity_data = array_merge($entity->toArray(), $data);
		$mapper->save(new Application_Model_Page($entity_data));

		if ($entity->status == 'Enabled' || (isset($data['status']) && $data['status'] == 'Enabled')) {
			//delete cache
			$cache_id = 'page_' . $data['id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $entity->id)
			);
			$cache->remove($cache_id);
			//css file cache
			$this->_cache_manager->refreshCSS($entity->id);
			/*
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$entity->id);
			*/
		}

		echo json_encode($entity_data);
		exit;
	}

	public function addSectionAction()
	{
		//create a section in the DB and pass the ID
		$page_id = $this->getRequest()->getParam('page_id', 0);
		$prev_id = $this->getRequest()->getParam('prev_id', 0);
		$type = $this->getRequest()->getParam('type', 'standard');

		$mapper = new Admin_Model_Mapper_PageSection();
		$section_id = $mapper->addSection($page_id, $type, $prev_id);
		$this->view->section = $mapper->find($section_id);

		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$this->view->rows = $mapper->getPageSectionRows($section_id);

		$this->view->runJS = false;
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->runJS = true;
		}

		$this->render('component-section');
	}

	public function deleteSectionAction()
	{
		//delete a section in the DB
		$page_id = $this->getRequest()->getParam('page_id', 0);
		$id = $this->getRequest()->getParam('id', 0);

		$mapper = new Admin_Model_Mapper_PageSection();
		$mapper->deleteSection($page_id, $id);

		if (!$mapper->getPageSections($page_id) || count($mapper->getPageSections($page_id)) == 0) {
			//if page edit level set to Section then lets check if they just deleted the last section and create a new section (otherwise no way to add content back in)
			$page_mapper = new Admin_Model_Mapper_Page();
			$page = $page_mapper->find($page_id);

			if ($page->editable == 'Section') {
				$mapper->addRow($page_id);
			}
		}

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'page_' . $page_id;
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id, 'section_' . $id)
			);
			$cache->remove($cache_id);
		}

		exit;
	}

	public function sortSectionAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$mapper = new Admin_Model_Mapper_PageSection();
		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach ($values as $k => $id) {
			if (!empty($id)) {
				$mapper->setSortOrder($id, $sort);
				$sort++;
			}
		}

		$section = $mapper->find(current($values));
		$page_id = $section->page_id;

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($page_id);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'page_' . $page_id;
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id)
			);
			$cache->remove($cache_id);
		}

		exit;
	}

	public function cloneSectionAction()
	{
		$clone_id = $this->getRequest()->getParam('clone_id', 0);

		//clone section
		$mapper = new Admin_Model_Mapper_PageSection();

		$section_id = $mapper->cloneSection($clone_id);
		$this->view->section = $mapper->find($section_id);

		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$this->view->rows = $mapper->getPageSectionRows($section_id);

		$sql = $this->_db->quoteInto("SELECT ps.page_id FROM {$this->_table_prefix}page_sections AS ps WHERE ps.id = ?", $section_id);
		$page_id = $this->_db->fetchOne($sql);

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($page_id);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'page_' . $page_id;
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id)
			);
			$cache->remove($cache_id);
			//css file cache
			$this->_cache_manager->refreshCSS($page_id);
			/*
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$page_id);
			*/
		}

		$this->view->runJS = false;
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->runJS = true;
		}

		$this->render('component-section');
	}

	public function editSectionAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		$mapper = new Admin_Model_Mapper_PageSection();
		$this->view->section = $mapper->find($id);
	}

	public function saveSectionAction()
	{
		$data = $this->getRequest()->getParam('data', array());
		$styles = $this->getRequest()->getParam('styles', array());
		$settings = $this->getRequest()->getParam('settings', array());

		//json encode all the content items and save into data json_content
		$data['json_styles'] = json_encode($styles);
		$data['json_settings'] = json_encode($settings);

		$mapper = new Admin_Model_Mapper_PageSection();
		$entity = $mapper->find($data['id']);

		$entity_data = array_merge($entity->toArray(), $data);
		$mapper->save(new Application_Model_PageSection($entity_data));

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($entity->page_id);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'section_' . $data['id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $entity->page_id)
			);
			$cache->remove($cache_id);
			//css file cache
			$this->_cache_manager->refreshCSS($entity->page_id);
			/*
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$entity->page_id);
			*/
		}

		echo json_encode($entity_data);
		exit;
	}

	public function addRowAction()
	{
		$section_id = $this->getRequest()->getParam('section_id', 0);
		$prev_id = $this->getRequest()->getParam('prev_id', 0);

		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$row_id = $mapper->addRow($section_id, $prev_id);
		$this->view->row = $mapper->find($row_id);

		$this->render('component-row');
	}

	public function updateRowAction()
	{
		$column_id = $this->getRequest()->getParam('column_id', 0);

		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$row_id = $mapper->getRowFromColumnId($column_id);
		$this->view->row = $mapper->find($row_id);

		$return = array();
		$return['row_id'] = $row_id;
		$return['content'] = $this->view->render('/cms/component-row.phtml');

		echo json_encode($return);exit;
	}

	public function deleteRowAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		$sql = $this->_db->quoteInto("SELECT ps.page_id FROM {$this->_table_prefix}page_section_rows AS psr {$this->_table_prefix}LEFT JOIN page_sections AS ps ON psr.page_section_id = ps.id WHERE psr.id = ?", $id);
		$page_id = $this->_db->fetchOne($sql);

		$psr_mapper = new Admin_Model_Mapper_PageSectionRow();
		$old = $psr_mapper->find($id);
		$psr_mapper->deleteRow($id);

		if (!$psr_mapper->getPageSectionRows($old->page_section_id) || count($psr_mapper->getPageSectionRows($old->page_section_id)) == 0) {
			//if page edit level set to Row then lets check if they just deleted the last row and create a new row (otherwise no way to add content back in)
			$page_mapper = new Admin_Model_Mapper_Page();
			$page = $page_mapper->find($page_id);

			if ($page->editable == 'Row') {
				$psr_mapper->addRow($old->page_section_id);
			}
		}

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'page_' . $page_id;
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id)
			);
			$cache->remove($cache_id);
		}

		exit;
	}

	public function sortRowAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$values = $this->getRequest()->getParam('section_row');
		$sort = 1;
		foreach ($values as $k => $id) {
			if (!empty($id)) {
				$mapper->setSortOrder($id, $sort);
				$sort++;
			}
		}

		$sql = $this->_db->quoteInto("SELECT ps.page_id FROM {$this->_table_prefix}page_section_rows AS psr LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id WHERE psr.id = ?", current($values));
		$page_id = $this->_db->fetchOne($sql);

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($page_id);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'page_' . $page_id;
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id)
			);
			$cache->remove($cache_id);
		}

		exit;
	}

	public function cloneRowAction()
	{
		$clone_id = $this->getRequest()->getParam('clone_id', 0);

		//clone row
		$mapper = new Admin_Model_Mapper_PageSectionRow();

		$row_id = $mapper->cloneRow($clone_id);
		$this->view->row = $mapper->find($row_id);

		$sql = $this->_db->quoteInto("SELECT ps.page_id FROM {$this->_table_prefix}page_section_rows AS psr LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id WHERE psr.id = ?", $row_id);
		$page_id = $this->_db->fetchOne($sql);

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($page_id);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'page_' . $page_id;
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id)
			);
			$cache->remove($cache_id);
			//css file cache
			$this->_cache_manager->refreshCSS($page_id);
			/*
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$page_id);
			*/
		}

		$this->render('component-row');
	}

	public function editRowAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$this->view->row = $mapper->find($id);
	}

	public function saveRowAction()
	{
		$data = $this->getRequest()->getParam('data', array());
		$styles = $this->getRequest()->getParam('styles', array());
		$settings = $this->getRequest()->getParam('settings', array());

		//json encode all the content items and save into data json_content
		$data['json_styles'] = json_encode($styles);
		$data['json_settings'] = json_encode($settings);

		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$entity = $mapper->find($data['id']);

		$entity_data = array_merge($entity->toArray(), $data);
		$mapper->save(new Application_Model_PageSectionRow($entity_data));

		$sql = $this->_db->quoteInto("SELECT ps.page_id
			FROM {$this->_table_prefix}page_section_rows AS psr
			LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
			WHERE psr.id = ?", $data['id']);
		$page_id = $this->_db->fetchOne($sql);

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($page_id);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'row_' . $data['id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id)
			);
			$cache->remove($cache_id);
			//css file cache
			$this->_cache_manager->refreshCSS($page_id);
			/*
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$page_id);
			*/
		}

		echo json_encode($entity_data);
		exit;
	}

	public function chooseColumnsAction()
	{
		$this->view->row_id = $row_id = $this->getRequest()->getParam('row_id', 0);
	}

	public function addColumnsAction()
	{
		$row_id = $this->getRequest()->getParam('row_id', 0);
		$col_type = $this->getRequest()->getParam('col_type', 0);
		$columns = explode("|", $col_type);
		$sort = 0;

		$mapper = new Admin_Model_Mapper_PageSectionRowColumn();
		foreach ($columns as $column) {
			$mapper->save(new Application_Model_PageSectionRowColumn(array(
				'page_section_row_id' => $row_id,
				'col_class' => $column,
				'sort_order' => $sort,
			)));
			$sort++;
		}

		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$this->view->row = $mapper->find($row_id);

		$this->render('component-row');
	}

	public function chooseModuleAction()
	{
		$this->view->column_id = $column_id = $this->getRequest()->getParam('column_id', 0);
	}

	public function addModuleAction()
	{
		$column_id = $this->getRequest()->getParam('column_id', 0);
		$type = $this->getRequest()->getParam('type', 'wysiwyg');
		$library_module_id = $this->getRequest()->getParam('library_module_id', 0);

		$sql = $this->_db->quoteInto("SELECT MAX(sort_order) FROM {$this->_table_prefix}page_section_row_column_modules WHERE page_section_row_column_id = ?", $column_id);
		$sort = $this->_db->fetchOne($sql);

		$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
		if ($library_module_id) {
			$lm_mapper = new Admin_Model_Mapper_LibraryModule();
			$library_module = $lm_mapper->find($library_module_id);

			$id = $mapper->save(new Application_Model_PageSectionRowColumnModule(array(
				'page_section_row_column_id' => $column_id,
				'type' => $library_module->type,
				'label' => $library_module->label,
				'class' => $library_module->class,
				'json_settings' => $library_module->json_settings,
				'json_styles' => $library_module->json_styles,
				'desktop_status' => $library_module->desktop_status,
				'tablet_status' => $library_module->tablet_status,
				'mobile_status' => $library_module->mobile_status,
				'json_content' => $library_module->json_content,
				'wireframe_json' => $library_module->wireframe_json,
				'sort_order' => ($sort) ? $sort + 1 : 0,
			)));
		}else {
			$id = $mapper->save(new Application_Model_PageSectionRowColumnModule(array(
				'page_section_row_column_id' => $column_id,
				'type' => $type,
				'sort_order' => ($sort) ? $sort + 1 : 0,
			)));
		}
		$this->view->module = $mapper->find($id);

		$this->render('component-module');
	}

	public function editModuleAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
		$this->view->module = $mapper->find($id);

		$mapper = new Admin_Model_Mapper_HistoryModule();
		$this->view->history = $mapper->getByModule($id);

		$this->render('component-module');
	}

	public function saveModuleAction()
	{
		$data = $this->getRequest()->getParam('data', array());
		$content = $this->getRequest()->getParam('content', array());
		$padding = $this->getRequest()->getParam('padding', array());
		$styles = $this->getRequest()->getParam('styles', array());
		$settings = $this->getRequest()->getParam('settings', array());
		$wireframe_settings = $this->getRequest()->getParam('wireframe_settings', array());

		//json encode all the content items and save into data json_content
		$data['json_content'] = json_encode($content);
		$data['json_styles'] = json_encode($styles);
		$data['json_settings'] = json_encode($settings);
		$data['wireframe_json'] = json_encode($wireframe_settings);

		$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
		$entity = $mapper->find($data['id']);

		//convert padding to string
		$data['padding'] = '';
		$data_pad = array();
		foreach ($padding as $pad) {
			if ($clean = str_replace(" ", "", $pad)) {
				if (!is_null($clean)) {
					$data_pad[] = preg_replace("/[^0-9]/", "", $clean) . "px";
				}
			} else {
				$data_pad[] = "auto";
			}
		}

		$data['padding'] = implode(" ", $data_pad);

		if (trim($data['padding']) == '') {
			$data['padding'] = new Zend_Db_Expr("NULL");
		}

		//geocode address for specific modules
		if (in_array($entity->type,['google_map'])) {
			$geocode = new Application_Model_GoogleGeocode();
			foreach ($content['panels'] as $k => $item) {
				if (!$item['latitude'] || !$item['longitude']) {
					$geo_loc = $geocode->getGeocodeData($item['address'].' '.$item['city']. ' '.$item['region'].' '.$item['postcode'].' '.$item['country']);
					if ($geo_loc) {
						$content['panels'][$k]['latitude'] = $geo_loc[0];
						$content['panels'][$k]['longitude'] = $geo_loc[1];
					}
				}
			}
			$data['json_content'] = json_encode($content);
		}
		//exit;

		$entity_data = array_merge($entity->toArray(), $data);
		$module_id = $mapper->save(new Application_Model_PageSectionRowColumnModule($entity_data));
		$module = $mapper->find($module_id);

		$sql = $this->_db->quoteInto("SELECT ps.page_id
			FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
			LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
			LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
			LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
			WHERE psrcm.id = ?", $data['id']);
		$page_id = $this->_db->fetchOne($sql);

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($page_id);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'module_' . $data['id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id)
			);
			$cache->remove($cache_id);
			//css file cache
			$this->_cache_manager->refreshCSS($page_id);
			/*
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$page_id);
			*/
		}

		$module_types = new Admin_View_Helper_ModuleInfo();
		$label = ($data['label']) ? $data['label'] : $module_types->moduleTitle($entity->type);

		echo json_encode(array('id' => $data['id'], 'column_id' => $module->page_section_row_column_id, 'label' => $module_types->moduleIcon($entity->type) . ' ' . $label));

		exit;
	}

	public function cloneModuleAction()
	{
		$clone_id = $this->getRequest()->getParam('clone_id', 0);

		//clone module
		$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();

		$module_id = $mapper->cloneRowColumnModule($clone_id);
		$this->view->module = $module = $mapper->find($module_id);

		$sql = $this->_db->quoteInto("SELECT ps.page_id
			FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
			LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
			LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
			LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
			WHERE psrcm.id = ?", $module_id);
		$page_id = $this->_db->fetchOne($sql);

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($page_id);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'module_' . $module_id;
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id)
			);
			$cache->remove($cache_id);
			//css file cache
			$this->_cache_manager->refreshCSS($page_id);
			/*
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$page_id);
			*/
		}

		$this->forward('update-row', null, null, array('column_id' => $module->page_section_row_column_id));
	}

	public function sortModuleAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$column_id = $this->getRequest()->getParam('column_id', 0);
		$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
		$values = $this->getRequest()->getParam('column_module');
		$sort = 1;
		foreach ($values as $k => $id) {
			if (!empty($id)) {
				$mapper->setSortOrder($id, $sort, array('page_section_row_column_id' => $column_id));
				$sort++;
			}
		}

		$sql = $this->_db->quoteInto("SELECT ps.page_id
			FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
			LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
			LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
			LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
			WHERE psrcm.id = ?", current($values));
		$page_id = $this->_db->fetchOne($sql);

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($page_id);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'page_' . $page_id;
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page_id)
			);
			$cache->remove($cache_id);
		}

		exit;
	}

	public function revertModuleAction()
	{
		$history_id = $this->getRequest()->getParam('history_id', 0);

		$mapper = new Admin_Model_Mapper_HistoryModule();
		$history = $mapper->find($history_id);

		if ($history) {
			$data = json_decode($history->table_field_json, true);
			$data['id'] = $history->module_id;
			$data['json_content'] = $history->json_content;

			$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
			$mapper->save(new Application_Model_PageSectionRowColumnModule($data), false);

			$sql = $this->_db->quoteInto("SELECT ps.page_id
			FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
			LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
			LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
			LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
			WHERE psrcm.id = ?", $data['id']);
			$page_id = $this->_db->fetchOne($sql);

			$page_mapper = new Admin_Model_Mapper_Page();
			$page = $page_mapper->find($page_id);

			if ($page->status == 'Enabled') {
				//delete cache
				$cache_id = 'module_' . $data['id'];
				$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
				$cache->clean(
					Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
					array('page_' . $page_id)
				);
				$cache->remove($cache_id);
				//css file cache
				$this->_cache_manager->refreshCSS($page_id);
				/*
				$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
				$cache->remove('css_site');
				$cache->remove('css_site'.$page_id);
				*/
			}

			$module_types = new Admin_View_Helper_ModuleInfo();
			echo json_encode(array('id' => $data['id'], 'label' => ($data['label']) ? $data['label'] : $module_types->moduleTitle($data['type'])));
		}
		exit;
	}

	public function deleteModuleAction()
	{
		$module_id = $this->getRequest()->getParam('module_id', 0);

		$sql = $this->_db->quoteInto("SELECT ps.page_id, psrcm.id, psrcm.page_section_row_column_id
			FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
			LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
			LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
			LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
			WHERE psrcm.id = ?", $module_id);
		$data = $this->_db->fetchRow($sql);

		$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
		$mapper->deleteModule($module_id);

		$page_mapper = new Admin_Model_Mapper_Page();
		$page = $page_mapper->find($data['page_id']);

		if ($page->status == 'Enabled') {
			//delete cache
			$cache_id = 'module_' . $data['id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $data['page_id'])
			);
			$cache->remove($cache_id);
		}

		$this->forward('update-row', null, null, array('column_id' => $data['page_section_row_column_id']));
	}

	public function cacheRefreshAction()
	{
		//delete a section in the DB
		$page_id = $this->getRequest()->getParam('page_id', 0);

		//delete cache
		$cache_id = 'page_' . $page_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('page_' . $page_id)
		);
		$cache->remove($cache_id);
		//css file cache
		$this->_cache_manager->refreshCSS($page_id);
		/*
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);
		$cache->remove('css_site');
		$cache->remove('css_site'.$page_id);
		*/

		$this->redirect("/admin/cms/page-editor/id/" . $page_id);
	}

	public function blockListAction()
	{
		$this->view->messages = $this->_flashMessenger->getMessages();
		$this->view->placeholder('page_title')->set("Block Manager");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/cms">CMS</a></li><li class="active">Block Manager</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page', 1);
		$this->view->dir = $dir = $this->_getParam('dir', 'ASC');
		$this->view->sort = $sort = $this->_getParam('sort', 'title');

		$this->view->mask = $mask = trim($this->_getParam('mask', ''));
		$this->view->code = $code = trim($this->_getParam('code', ''));
		$this->view->status = $status = $this->_getParam('status', '');
		$this->view->alpha = $alpha = $this->_getParam('alpha', '');

		$search_keys = array(
			'keyword' => $mask,
			'code' => $code,
			'status' => $status,
			'alpha' => $alpha,
			'editable' => array('No', 'Row', 'Section'),
		);
		$mapper = new Admin_Model_Mapper_Page();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function navListAction()
	{
		$this->view->messages = $this->_flashMessenger->getMessages();
		$this->view->placeholder('page_title')->set("Navigation Manager");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/cms">CMS</a></li><li class="active">Navigation Manager</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$mapper = new Admin_Model_Mapper_NavigationItem();
		$this->view->main_nav = $mapper->mainNavArray();

		ksort($this->view->main_nav);
	}

	public function navListSortAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$mapper = new Admin_Model_Mapper_NavigationItem();
		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach ($values as $k => $id) {
			if (!empty($id)) {
				$mapper->setMainNavSortOrder($id, $sort);
				$sort++;
			}
		}

		//delete cache
		$cache_id = 'navigation';
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('navigation')
		);
		$cache->remove($cache_id);
	}

	public function navSettingsFormAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		$mapper = new Admin_Model_Mapper_NavigationItem();
		$this->view->item = $item = $mapper->find($id);

		//get pages enabled in nav
		$this->view->nav_pages = [];
		$this->view->nav_pages = $mapper->mainNavPagePairs();
		unset($this->view->nav_pages[$id]);

		if ($this->getRequest()->isPost()) {
			$nav_item = $this->getRequest()->getParam('data', $item->toArray());

			foreach ($nav_item as $k => $v) {
				if ($this->isempty($v)) {
					$nav_item[$k] = new Zend_Db_Expr("NULL");
				}
			}

			if ($nav_item['link_type'] == 'url') {
				$nav_item['block_id'] = new Zend_Db_Expr("NULL");
			}else {
				$nav_item['url'] = new Zend_Db_Expr("NULL");
			}
			unset($nav_item['link_type']);

			$this->view->settings = $settings = $this->getRequest()->getParam('settings', array());
			$this->view->styles = $styles = $this->getRequest()->getParam('styles', array());

			$nav_item['json_settings'] = json_encode($settings);
			$nav_item['json_styles'] = json_encode($styles);

			$mapper->save(new Application_Model_NavigationItem($nav_item));

			//delete cache
			$cache_id = 'navigation';
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('navigation')
			);
			$cache->remove($cache_id);

			$this->redirect("/admin/cms/nav-list");
		}
	}

	public function navExternalFormAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		$mapper = new Admin_Model_Mapper_NavigationItem();
		$this->view->item = $mapper->find($id);

		//get pages enabled in nav
		$this->view->nav_pages = [];
		$this->view->nav_pages = $mapper->mainNavPagePairs();
		unset($this->view->nav_pages[$id]);

		if ($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getParam('data', array());

			$mapper->save(new Application_Model_NavigationItem($data));

			//delete cache
			$cache_id = 'navigation';
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('navigation')
			);
			$cache->remove($cache_id);

			$this->redirect("/admin/cms/nav-list");
		}
	}

	public function navExternalDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		if ($id && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_NavigationItem();
			$entity = $mapper->find($id);

			$mapper->delete($entity);
		}

		//delete cache
		$cache_id = 'navigation';
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('navigation')
		);
		$cache->remove($cache_id);

		$this->redirect("/admin/cms/nav-list");
	}

	public function libraryStoreModuleAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		if ($id && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
			$entity = $mapper->find($id);

			if ($entity->label) {
				$mapper = new Admin_Model_Mapper_LibraryModule();
				$data = $entity->toArray();
				unset($data['id']);
				unset($data['created']);
				unset($data['modified']);
				$mapper->save(new Application_Model_LibraryModule($data));

				$sql = $this->_db->quoteInto("SELECT ps.page_id
				FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
				LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
				LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
				LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
				WHERE psrcm.id = ?", $id);
				$page_id = $this->_db->fetchOne($sql);
			}

			$this->redirect("/admin/cms/page-editor/id/" . $page_id);
		}

		$this->redirect("/admin/cms/page-list");
	}

	public function libraryListModuleAction()
	{
		$this->view->column_id = $column_id = $this->getRequest()->getParam('column_id', 0);

		$mapper = new Admin_Model_Mapper_LibraryModule();
		$this->view->modules = $mapper->listAll();
	}

	public function libraryDeleteModuleAction()
	{
		$this->view->column_id = $column_id = $this->getRequest()->getParam('column_id', 0);
		$id = $this->getRequest()->getParam('id', 0);

		if ($id && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_LibraryModule();
			$mapper->delete($mapper->find($id));
		}

		$this->redirect("/admin/cms/library-list-module/column_id/".$column_id);
	}

	public function templateListAction()
	{
		$this->view->placeholder('page_title')->set("Templates");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Templates</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$mapper = new Admin_Model_Mapper_Template();
		$this->view->list = $mapper->listAll();
	}

	public function templateAddAction()
	{
		$this->view->placeholder('page_title')->set("Templates");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/cms/template-list">Templates</a></li>
				<li class="active">Add Template</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->render("template-form");
	}

	public function templateEditAction()
	{
		$this->view->template_id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)) {
			$this->view->placeholder('page_title')->set("Templates");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/cms/template-list">Templates</a></li>
					<li class="active">Edit Template</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$mapper = new Admin_Model_Mapper_Template();
			$this->view->template = $mapper->find($id);

			$mapper = new Admin_Model_Mapper_TemplateBlock();
			$this->view->blocks = $mapper->findByTemplateId($id);

			$this->render("template-form");
		}else {
			$this->redirect("/admin/cms/template-list");
		}
	}

	public function templateSaveAction()
	{
		$this->view->errors = array();

		if ($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getParam('data',array());
			$blocks = $this->getRequest()->getParam('block',array());
			$block_settings = $this->getRequest()->getParam('settings',array());

			if ($data['title']) {
				$mapper = new Admin_Model_Mapper_Template();

				$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
				$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
				$counter = 1;
				while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
					$code = $orig_code . "-" . $counter;
					$counter++;
				}
				$data['code'] = $code;

				$id = $mapper->save(new Application_Model_Template($data));

				$mapper = new Admin_Model_Mapper_TemplateBlock();
				$mapper->deleteByTemplateId($id);

				foreach ($blocks as $position_code => $block_id) {
					$mapper->save(new Application_Model_TemplateBlock(array(
						'template_id' => $id,
						'position_code' => $position_code,
						'block_id' => $block_id,
						//'width' => (isset($block_widths[$position_code]))?$block_widths[$position_code]:null
						'json_settings' => json_encode($block_settings[$position_code])
					)));
				}

				$this->redirect("/admin/cms/template-list");
			}else {
				$this->view->errors[] = "Template Title Required";

				$this->render("template-form");
			}
		}else {
			$this->redirect("/admin/cms/template-list");
		}
	}

	public function templateDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		if ($id && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_Template();
			$mapper->delete($mapper->find($id));
		}

		$this->redirect("/admin/cms/template-list");
	}

	public function templateCopyAction()
	{
		$id = $this->getRequest()->getParam('id', 0); //id of page to clone

		$template_mapper = new Admin_Model_Mapper_Template();
		$block_mapper = new Admin_Model_Mapper_TemplateBlock();

		//get and clone page
		$template = $template_mapper->find($id);
		$template->id = null;
		$template->code = '';
		$template->title = $template->title . " COPY";
		$new_template_id = $template_mapper->save($template);

		//get and clone blocks
		$blocks = $block_mapper->findByTemplateId($id);
		foreach ($blocks as $row) {
			$block = $block_mapper->find($row->id);
			$block->id = null;
			$block->template_id = $new_template_id;

			$block_mapper->save($block);
		}

		$this->redirect("/admin/cms/template-edit/id/" . $new_template_id);
	}

	public function fontWeightAjaxAction()
	{
		$font_name = $this->getRequest()->getParam('font_name', '');

		$mapper = new Admin_Model_Mapper_Font();

		if ($weights = $mapper->getWeights($font_name)) {
			echo json_encode($weights);
		}else {
			echo json_encode('');
		}
		exit;
	}

	public function pageExportAction()
	{
		$data = [];

		$id = $this->getRequest()->getParam('id', 0); //id of page to export
		$include_media = $this->getRequest()->getParam('include_media', 'No');

		$page_mapper = new Admin_Model_Mapper_Page();
		$section_mapper = new Admin_Model_Mapper_PageSection();
		$row_mapper = new Admin_Model_Mapper_PageSectionRow();
		$column_mapper = new Admin_Model_Mapper_PageSectionRowColumn();
		$module_mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();

		//get page
		$page = $page_mapper->find($id);

		$data['page']['data'] = $page->toArray();

		//get sections
		$sections = $section_mapper->getPageSections($id);
		foreach ($sections as $section) {
			$data['sections'][$section->id]['data'] = $section->toArray();

			//get section rows
			$rows = $row_mapper->getPageSectionRows($section->id);
			foreach ($rows as $row) {
				$data['sections'][$section->id]['rows'][$row->id]['data'] = $row->toArray();

				//get row columns
				$columns = $column_mapper->getPageSectionRowColumns($row->id);
				foreach ($columns as $column) {
					$data['sections'][$section->id]['rows'][$row->id]['columns'][$column->id]['data'] = $column->toArray();

					//get column modules
					$modules = $module_mapper->getPageSectionRowColumnModules($column->id);
					foreach ($modules as $module) {
						$data['sections'][$section->id]['rows'][$row->id]['columns'][$column->id]['modules'][$module->id]['data'] = $module->toArray();
					}
				}
			}
		}

		$media = [];
		if ($include_media == 'Yes') {
			//TODO - need to fix the regex so the double quote at the end of some matches is stripped automatically
			//I think there are still some other errors in the regex
			//ideally I just want it to find any files in /userFiles/uploads/ but those paths may also be escaped with slashes.

			//disable media for now until I can fix up the zip portion
			$exp = "#(\/.*?\/)((?:[^\/]|\\\\/)+?)(?:(?<!\\\)\s|$|\")#";
			preg_match_all($exp, serialize($data), $media, PREG_SET_ORDER);
		}

		$encoded = openssl_encrypt(serialize($data),'aes-128-ctr','LOGIC', null, 'cyberny.comLOGIC');

		$slugify = new Slugify();
		$slug = $slugify->slugify($page->code);
		$filename = $slug.'.clz';
		$file = sys_get_temp_dir().'/'.$filename;

		$zip = new ZipArchive;
		$res = $zip->open($file, ZipArchive::CREATE);
		if ($res === TRUE) {
			$zip->addFromString($slug.'.clp', $encoded);

			foreach ($media as $match) {
				$path = str_ireplace(array("\/",'"'),array("/",""),$match[0]);
				if (stripos($path, '/userFiles/uploads') !== false) {
					$zip->addFile($_SERVER['DOCUMENT_ROOT'].$path, substr($path,1));

					//thumbnail file
					$thumb_path = str_replace("userFiles/uploads", "userThumbs", $path);

					$zip->addFile($_SERVER['DOCUMENT_ROOT'].$thumb_path, substr($thumb_path,1));
				}
			}

			$zip->close();
		} else {
			echo 'export failed';
			exit;
		}

		header('Content-Type: application/octet-stream');
		header('Content-disposition: attachment; filename='.$filename);
		header('Content-Length: ' . filesize($file));
		readfile($file);

		unlink($file);

		exit;
	}

	public function pageImportAction()
	{
		$page_mapper = new Admin_Model_Mapper_Page();
		$section_mapper = new Admin_Model_Mapper_PageSection();
		$row_mapper = new Admin_Model_Mapper_PageSectionRow();
		$column_mapper = new Admin_Model_Mapper_PageSectionRowColumn();
		$module_mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();

		$page_id = $this->getRequest()->getParam('id');

		if ($this->getRequest()->getParam('type') == 'new') {
			$data = [];
			$data['title'] = 'Importer Temp';
			$data['code'] = 'importer-temp-'.rand();
			$data['status'] = 'Enabled';
			$data['nav_display'] = 'Enabled';
			$data['footer_display'] = 'Enabled';
			$data['password_protect'] = 'Disabled';
			$data['created_by_user_id'] = $this->view->user->id;

			$entity = new Application_Model_Page($data);
			$page_id = $page_mapper->save($entity);
		}

		if ($page_id && is_numeric($page_id) && $this->getRequest()->isPost()) {
			$zip = new ZipArchive;
			$res = $zip->open($_FILES['archive']['tmp_name']);
			if ($res === TRUE) {
				$info = pathinfo($_FILES['archive']['name']);
				$dir = sys_get_temp_dir().'/'.$info['filename'];
				mkdir($dir);

				$zip->extractTo($dir);
				$zip->close();

				//copy media files if they exist
				if (is_dir($dir.'/userFiles')) {
					$dest = realpath(APPLICATION_PATH . '/../public/userFiles');
					foreach ($iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir . '/userFiles', \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST) as $item) {
						if ($item->isDir()) {
							mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
						} else {
							copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
						}
					}
				}
				//thumbnails
				if (is_dir($dir.'/userThumbs')) {
					$dest = realpath(APPLICATION_PATH . '/../public/userThumbs');
					foreach ($iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir . '/userThumbs', \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST) as $item) {
						if ($item->isDir()) {
							mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
						} else {
							copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
						}
					}
				}

				//read data file
				$data_array = unserialize(openssl_decrypt(file_get_contents($dir.'/'.$info['filename'].'.clp'),'aes-128-ctr','LOGIC', null, 'cyberny.comLOGIC'));

				if ($data_array) {
					//create/update page
					$page = $data_array['page']['data'];

					$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
					$code = $orig_code = $slugify->slugify(($page['code']) ? $page['code'] : $page['title']);
					$counter = 1;
					while ($page_mapper->doesExists(array('code' => $code), $page_id) !== false) {
						$code = $orig_code . "-" . $counter;
						$counter++;
					}
					$page['code'] = $code;
					$page['id'] = $page_id;
					$page['modified'] = new Zend_Db_Expr("NOW()");
					$page_mapper->save(new Application_Model_Page($page));

					//remove old page sections
					$section_mapper->deleteByPageId($page_id);

					//create page sections
					foreach ($data_array['sections'] as $sid => $section) {
						$section_data = $section['data'];

						unset($section_data['id']);
						$section_data['page_id'] = $page_id;
						$section_data['modified'] = new Zend_Db_Expr("NOW()");

						$section_id = $section_mapper->save(new Application_Model_PageSection($section_data));

						//create page section rows
						foreach ($section['rows'] as $rid => $row) {
							$row_data = $row['data'];

							unset($row_data['id']);
							$row_data['page_section_id'] = $section_id;
							$row_data['modified'] = new Zend_Db_Expr("NOW()");

							$row_id = $row_mapper->save(new Application_Model_PageSectionRow($row_data));

							//create page section row columns
							foreach ($row['columns'] as $cid => $column) {
								$column_data = $column['data'];

								unset($column_data['id']);
								$column_data['page_section_row_id'] = $row_id;
								$column_data['modified'] = new Zend_Db_Expr("NOW()");

								$column_id = $column_mapper->save(new Application_Model_PageSectionRowColumn($column_data));

								//create page section row column modules
								foreach ($column['modules'] as $mid => $module) {
									$module_data = $module['data'];

									unset($module_data['id']);
									$module_data['page_section_row_column_id'] = $column_id;
									$module_data['modified'] = new Zend_Db_Expr("NOW()");

									$module_id = $module_mapper->save(new Application_Model_PageSectionRowColumnModule($module_data));
								}
							}
						}
					}

					//clear page cache
					$cache_id = 'page_' . $page_id;
					$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
					$cache->clean(
						Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
						array('page_' . $page_id)
					);
					$cache->remove($cache_id);

					$this->_flashMessenger->setNamespace('info')->addMessage("Page import successful");
				}else {
					$this->_flashMessenger->setNamespace('error')->addMessage("There was a problem with the import. File incorrect or corrupted.");
				}

				//clean up files
				if (is_dir($dir)) {
					$objects = scandir($dir);
					foreach ($objects as $object) {
						if ($object != "." && $object != "..") {
							if (filetype($dir . "/" . $object) == "dir")
								rmdir($dir . "/" . $object);
							else unlink($dir . "/" . $object);
						}
					}
					reset($objects);
					rmdir($dir);
				}
			} else {
				$this->_flashMessenger->setNamespace('error')->addMessage("There was a problem with the import: $res");
			}


			$this->redirect("/admin/cms/page-editor/id/$page_id");
		}else {
			$this->redirect("/admin");
		}
	}

	public function pageAddImportAction()
	{
		//
	}

	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'cms', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array('automatic_serialization' => true, 'lifetime' => $settings_mapper->getValueByCode('page_cache_lifetime'));
		$this->frontendOptionsCSS = array('automatic_serialization' => true, 'lifetime' => $settings_mapper->getValueByCode('css_cache_lifetime'));
		$this->backendOptions = array('cache_dir' => $cache_dir);

		$this->view->filemanager_root = $this->_filemanager_root = "/userFiles/uploads/";

		$this->_cache_manager = new Application_Model_CacheManager();

		$this->view->statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled'
		);
		$this->view->page_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled',
			'Pending' => 'Pending'
		);

		//layouts/templates
		/*
		$this->view->templates = array();
		$layouts = Zend_Registry::get('cms_layouts');
		if (!is_array($layouts)) {
			$layouts = $layouts->toArray();
		}
		foreach ($layouts as $row) {
			$this->view->templates[$row['filename']] = $row['title'];
		}
		*/
		$mapper = new Admin_Model_Mapper_Template();
		$this->view->templates = array(""=>"Default") + $mapper->fetchCodePairs();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');

		//page custom variable names
		$this->view->page_custom_variable_names = (array_key_exists('page_custom_variable', $options))?$options['page_custom_variable']:array();
	}
}
