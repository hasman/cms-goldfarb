<?php
class Admin_VideoTransactionController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/video-transaction/list');
	}

	public function listAction()
	{
		$this->view->placeholder('page_title')->set("Transactions");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Transactions</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','created');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->purchase_type = $purchase_type = $this->_getParam('purchase_type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');
		$this->view->date_from = $date_from = $this->_getParam('date_from','');
		$this->view->date_to = $date_to = $this->_getParam('date_to','');

		$sql_date_from = $sql_date_to = '';
		if ($date_from) {
			$sql_date_from = new DateTime($date_from . ' 00:00:00', new DateTimeZone($this->view->site_timezone));
			$sql_date_from->setTimeZone(new DateTimeZone('UTC'));
			$sql_date_from = $sql_date_from->format('Y-m-d H:i:s');
		}
		if ($date_to) {
			$sql_date_to = new DateTime($date_to . ' 23:59:59', new DateTimeZone($this->view->site_timezone));
			$sql_date_to->setTimeZone(new DateTimeZone('UTC'));
			$sql_date_to = $sql_date_to->format('Y-m-d H:i:s');
		}

		$search_keys = array(
			'keyword' => $mask,
			'purchase_type' => $purchase_type,
			'alpha' => $alpha,
			'date_from' => $sql_date_from,
			'date_to' => $sql_date_to
		);
		$mapper = new Admin_Model_Mapper_VideoPlatformOrder();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();
			$this->view->file_name = 'video_transactions';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function viewAction()
	{
		$this->view->return_to = $this->getRequest()->getParam('return_to','');

		//Get the transaction ID passed in
		$id = $this->getRequest()->getParam('id');
		//Verify if the $id is not null and is a valid number
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformOrder();

			$this->view->placeholder('page_title')->set("Videos");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-transaction/list">Video Transactions</a></li><li class="active">Transactions: '.$id.'</li>';

			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			//Get transaction details
			$this->view->item = $mapper->fetchTransaction($id);
		}else{
			$this->getResponse()->setRedirect('/admin/video-transaction/list');
		}

	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'video_platform', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled'
		);

		$this->view->purchase_types = array(
			'purchase' => 'Purchase',
			'rent' => 'Rental',
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->_site_name = ($site_name = $settings_mapper->getValueByCode('email_site_name'))?$site_name:$settings_mapper->getValueByCode('site_name');

		$this->view->shipping_countries = $this->view->billing_countries =  $all_countries = Zend_Registry::get('countries')->toArray();
		$this->view->usstates = $this->_states = $this->_provinces = array();

		if ($settings_mapper->getValueByCode('ecommerce_shipping_countries')) {
			$selected = json_decode($settings_mapper->getValueByCode('ecommerce_shipping_countries'));
			$this->view->shipping_countries = array();
			foreach ($selected as $k => $country_code) {
				$this->view->shipping_countries[$country_code] = $all_countries[$country_code];
			}
		}
		if ($settings_mapper->getValueByCode('ecommerce_billing_countries')) {
			$selected = json_decode($settings_mapper->getValueByCode('ecommerce_billing_countries'));
			$this->view->billing_countries = array();
			foreach ($selected as $k => $country_code) {
				$this->view->billing_countries[$country_code] = $all_countries[$country_code];
			}
		}

		$regions = Zend_Registry::get('regions')->toArray();
		if (array_key_exists('US',$this->view->shipping_countries) || array_key_exists('US',$this->view->billing_countries)) {
			$this->_states = $regions['us']['region'];
		}
		if (array_key_exists('CA',$this->view->shipping_countries) || array_key_exists('CA',$this->view->billing_countries)) {
			$this->_provinces = $regions['ca']['region'];
		}
		//combine states/provinces into single list
		$this->view->usstates = $this->_states + $this->_provinces;

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone');
	}


}
