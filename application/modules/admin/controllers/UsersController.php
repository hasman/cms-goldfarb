<?php
class Admin_UsersController extends Zend_Controller_Action
{
	/** @var  Zend_Db_Adapter_Abstract */
	protected $_db;
	public $_status, $_roles, $_user;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/users/list');
	}

	public function listAction()
	{
		if ($this->_admin_user->role != "admin") {
			$this->redirect("/admin/users/edit/id/".$this->_admin_user->id);
		}

		$this->view->placeholder('page_title')->set("Admin User Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li class="active">Admin User Manager</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','email');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->role = $role = trim($this->_getParam('role',''));
		$this->view->username = $username = trim($this->_getParam('username',''));
		$this->view->email = $email = trim($this->_getParam('email',''));
		$this->view->first_name = $first_name = trim($this->_getParam('first_name',''));
		$this->view->last_name = $last_name = trim($this->_getParam('last_name',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'role' => $role,
			'username' => $username,
			'email' => $email,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'status' => $status,
			'alpha' => $alpha,
			'is_website_member' => 'No'
		);
		$mapper = new Admin_Model_Mapper_AdminUser();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$results = $mapper->listSearch($sort, $dir, $search_keys);
			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'admin_users';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function addAction()
	{
		if ($this->_admin_user->role != "admin") {
			$this->redirect("/admin/users/edit/id/".$this->_admin_user->id);
		}

		$this->view->placeholder('page_title')->set("Admin User Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/users/list">Admin User Manager</a></li><li class="active">Add User</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_AdminUserForm();
		$form->getElement('cmd')->setValue('add');
		$form->removeElement('member_role');

		//role of the currently logged in user to determine if they can edit the role or not
		$form->getElement('current_user_role')->setValue($this->_admin_user->role);

		$this->view->back_list = "list";
		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('form');
	}

	public function editAction()
	{
		$this->view->placeholder('page_title')->set("Admin User Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/users/list">Admin User Manager</a></li><li class="active">Edit User</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->view->id = $id = $this->getRequest()->getParam('id', 0);

		if ($this->_admin_user->role != "admin") {
			$this->view->id = $id = $this->_admin_user->id;
		}

		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_AdminUserForm();
			$mapper = new Admin_Model_Mapper_AdminUser();
			$entity = $mapper->find($id);

			$data = $entity->toArray();
			unset($data['password']);

			$mapper = new Admin_Model_Mapper_AdminUserProfile();
			$entity = $mapper->findByUserId($id);
			if ($entity) {
				$user_profile = $entity->toArray();
				unset($user_profile['id']);
				$data = array_merge($data, $user_profile);
			}

			if (array_key_exists($data['role'], $this->_roles)) {
				$form->removeElement('member_role');
				$this->view->back_list = "list";
			}else {
				if (array_key_exists($this->_admin_user->role, $this->_roles)) {
					$form->removeElement('role');
				}
				$this->view->back_list = "member-list";
			}

			//role of the currently logged in user to determine if they can edit the role or not
			$data['current_user_role'] = $this->_admin_user->role;

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$form->getElement('password')->setLabel('New Password');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			if (array_key_exists($entity->role, $this->_website_member_roles)) {
				$this->view->placeholder('page_title')->set("Sub-User Manager");
				$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/users/member-list">Sub-User Manager</a></li><li class="active">View User</li>';
				$this->view->placeholder('breadcrumb')->set($breadcrumb);
			}

			$this->render('form');
		}else{
			$this->getResponse()->setRedirect('/admin/users/list');
		}
	}

	public function viewAction()
	{
		$this->view->placeholder('page_title')->set("Admin User Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/users/list">Admin User Manager</a></li><li class="active">View User</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		if ($this->_admin_user->role != 'admin') {
			$id = $this->_admin_user->id;
		}else {
			$id = $this->getRequest()->getParam('id');
		}
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_AdminUser();
			$this->view->user = $mapper->find($id);

			$mapper = new Admin_Model_Mapper_AdminUserProfile();
			$this->view->user_profile = $mapper->findByUserId($id);

			if (array_key_exists($this->view->user->role, $this->_website_member_roles)) {
				$this->view->placeholder('page_title')->set("Sub-User Manager");
				$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/users/member-list">Sub-User Manager</a></li><li class="active">View User</li>';
				$this->view->placeholder('breadcrumb')->set($breadcrumb);
			}
		}else{
			$this->getResponse()->setRedirect('/admin/users/');
		}
	}

	public function saveAction()
	{
		$this->view->placeholder('page_title')->set("Admin User Manager");

		$form = new Admin_Form_AdminUserForm();
		//$form->isValid($this->getRequest()->getParams());
		$data = $this->getRequest()->getParams();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_AdminUser();
		$profile_mapper = new Admin_Model_Mapper_AdminUserProfile();

		$exist = $mapper->checkDuplicate($data['email'], $data['id']);
		if ($exist)
			$this->view->errors[] = "Email already in use";

		if (array_key_exists($data['role'], $this->_roles)) {
			$form->removeElement('member_role');
		}else {
			$form->getElement('role')->setRequired(false);
			$form->getElement('member_role')->setRequired(false);
			if (array_key_exists($this->_admin_user->role, $this->_roles)) {
				$form->removeElement('role');
			}
			$data['role'] = $data['member_role'];
		}

		if( $form->isValid($data) && !$exist ){
			if ($data['id']) {
				$old = $mapper->find($data['id']);

				$old_profile = $profile_mapper->findByUserId($data['id']);
			}

			if ($data['password']) {
				$bcrypt = new Jr_Crypt_Password_Bcrypt();
				$data['password'] = $bcrypt->create($data['password']);
			}else {
				$data['password'] = $old->password;
			}

			if ($data['crop_data']) {
				$data['avatar'] = $this->cropImage(realpath('.').$data['image'],$data['crop_data']);
			}

			//don't let non-admin change role
			if ($this->_admin_user->role != 'admin') {
				$data['role'] = $old->role;
			}

			$entity = new Admin_Model_AdminUser($data);
			$id = $mapper->save($entity);

			$profile_data = array(
				'id' => ($old_profile)?$old_profile->id:null,
				'admin_user_id' => ($old_profile)?$old_profile->admin_user_id:$id,
				'company' => $data['company'],
				'date_of_birth' => ($data['date_of_birth'])?date("Y-m-d",strtotime($data['date_of_birth'])):null,
				'phone' => $data['phone'],
				'address' => $data['address'],
				'address2' => $data['address2'],
				'city' => $data['city'],
				'region' => $data['region'],
				'postcode' => $data['postcode'],
				'country' => $data['country'],
				'website' => $data['website'],
				'facebook_url' => $data['facebook_url'],
				'twitter_url' => $data['twitter_url'],
				'instagram_url' => $data['instagram_url'],
				'youtube_url' => $data['youtube_url'],
				'linkedin_url' => $data['linkedin_url'],
			);
			$profile_mapper->save(new Admin_Model_AdminUserProfile($profile_data));

			unlink(realpath('.').$data['image']);

			if ($this->_admin_user->role != 'admin') {
				$this->getResponse()->setRedirect('/admin/users/view/id/'.$id);
			}else {
				if (array_key_exists($data['role'], $this->_roles)) {
					$this->getResponse()->setRedirect('/admin/users/list');
				}else {
					$this->getResponse()->setRedirect('/admin/users/member-list');
				}
			}
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;

			if (array_key_exists($this->view->user->role, $this->_website_member_roles)) {
				$this->view->placeholder('page_title')->set("Sub-User Manager");
				$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/users/member-list">Sub-User Manager</a></li><li class="active">' . ucwords($data['cmd']) . ' User</li>';
			}else {
				$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/users/list">Admin User Manager</a></li><li class="active">' . ucwords($data['cmd']) . ' User</li>';
			}
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$this->render('form');
		}
	}

	public function deleteAction()
	{
		if ($this->_admin_user->role != "admin") {
			$this->redirect("/admin/users/edit/id/".$this->_admin_user->id);
		}

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_AdminUser();
			$entity = $mapper->find($id);
			$mapper->delete($entity);

			$mapper = new Admin_Model_Mapper_AdminUserProfile();
			$profile = $mapper->findByUserId($id);
			$mapper->delete($profile);
		}

		if (array_key_exists($entity->role,$this->_website_member_roles)) {
			$this->getResponse()->setRedirect('/admin/users/member-list');
		}else {
			$this->getResponse()->setRedirect('/admin/users/list');
		}
	}

	public function avatarUploadAction()
	{
		if( $this->getRequest()->isPost() ){
			$file = $_FILES['image'];
			if (file_exists($file['tmp_name'])) {
				$parts = explode(".",$file['name']);
				$ext = end($parts);
				$path = realpath('.').$this->_tmp_avatar_dir;
				$tmp_name = md5($file['name'].'-'.time()).'.'.$ext;

				move_uploaded_file($file['tmp_name'],$path.$tmp_name);

				$imagesizedata = getimagesize($path.$tmp_name);
				if ($imagesizedata === FALSE) {
					unlink($path.$tmp_name);
					echo "0";
				}
				else {
					//use $imagesizedata to get extra info


					echo json_encode(array('image'=>$this->_tmp_avatar_dir.$tmp_name));
				}
			}
			else {
				echo "0";
			}
		}

		exit;
	}

	public function memberListAction()
	{
		if ($this->_admin_user->role != "admin") {
			$this->redirect("/admin/users/edit/id/".$this->_admin_user->id);
		}

		$this->view->placeholder('page_title')->set("Sub-User Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li class="active">Sub-User Manager</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','email');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->role = $role = trim($this->_getParam('role',''));
		$this->view->username = $username = trim($this->_getParam('username',''));
		$this->view->email = $email = trim($this->_getParam('email',''));
		$this->view->first_name = $first_name = trim($this->_getParam('first_name',''));
		$this->view->last_name = $last_name = trim($this->_getParam('last_name',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'role' => $role,
			'username' => $username,
			'email' => $email,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'status' => $status,
			'alpha' => $alpha,
			'is_website_member' => 'Yes'
		);
		$mapper = new Admin_Model_Mapper_AdminUser();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$results = $mapper->listSearch($sort, $dir, $search_keys);
			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'website_member_users';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function memberAddAction()
	{
		if ($this->_admin_user->role != "admin") {
			$this->redirect("/admin/users/edit/id/".$this->_admin_user->id);
		}

		$this->view->placeholder('page_title')->set("Admin User Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/users/member-list">Admin User Manager</a></li><li class="active">Add Sub-User</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_AdminUserForm();
		$form->getElement('cmd')->setValue('add');
		$form->removeElement('role');

		//role of the currently logged in user to determine if they can edit the role or not
		$form->getElement('current_user_role')->setValue($this->_admin_user->role);

		$this->view->back_list = "member-list";
		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('form');
	}

	public function profileAction()
	{
		$this->redirect("/admin/users/view/id/".$this->_admin_user->id);
	}

	private function cropImage($imagepath, $crop_data)
	{
		$spec_width = 240;
		$spec_height = 240;

		$imginfo = getimagesize($imagepath);
		switch( $imginfo['mime'] ){
			case 'image/gif':
				$original_canvas = imagecreatefromgif($imagepath);
				break;
			case 'image/jpeg':
			case 'image/pjpeg':
				$original_canvas = imagecreatefromjpeg($imagepath);
				break;
			case 'image/png':
				$original_canvas = imagecreatefrompng($imagepath);
				break;
			default:
				$original_canvas = imagecreatetruecolor(1,1);
		}

		$original_width = imagesx($original_canvas);
		$original_height = imagesy($original_canvas);
		$orig_ratio = $original_width/$original_height;

		if($spec_width == 0){
			$spec_width = $original_width;
		}
		if($spec_height == 0){
			$spec_height = $original_height;
		}

		if ($original_height < $spec_height && $original_width < $spec_width) {
			$spec_width = $original_width;
			$spec_height = $original_height;
		}

		$new_canvas = imagecreatetruecolor($spec_width, $spec_height);

		imagesavealpha($new_canvas, true);
		imagefill($new_canvas, 0, 0, imagecolorallocatealpha($new_canvas, 255, 255, 255, 127));

		$data = json_decode($crop_data);
		$tmp_img_w = $data -> width;
		$tmp_img_h = $data -> height;
		$dst_img_w = $spec_width;
		$dst_img_h = $spec_height;
		$src_x = $data -> x;
		$src_y = $data -> y;
		if ($src_x <= -$tmp_img_w || $src_x > $original_width) {
			$src_x = $src_w = $dst_x = $dst_w = 0;
		} else if ($src_x <= 0) {
			$dst_x = -$src_x;
			$src_x = 0;
			$src_w = $dst_w = min($original_width, $tmp_img_w + $src_x);
		} else if ($src_x <= $original_width) {
			$dst_x = 0;
			$src_w = $dst_w = min($tmp_img_w, $original_width - $src_x);
		}
		if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $original_height) {
			$src_y = $src_h = $dst_y = $dst_h = 0;
		} else if ($src_y <= 0) {
			$dst_y = -$src_y;
			$src_y = 0;
			$src_h = $dst_h = min($original_height, $tmp_img_h + $src_y);
		} else if ($src_y <= $original_height) {
			$dst_y = 0;
			$src_h = $dst_h = min($tmp_img_h, $original_height - $src_y);
		}
		// Scale to destination position and size
		$ratio = $tmp_img_w / $dst_img_w;
		$dst_x /= $ratio;
		$dst_y /= $ratio;
		$dst_w /= $ratio;
		$dst_h /= $ratio;

		$new_canvas = imagecreatetruecolor($dst_img_w, $dst_img_h);
		// Add transparent background to destination image
		imagefill($new_canvas, 0, 0, imagecolorallocatealpha($new_canvas, 0, 0, 0, 127));
		imagesavealpha($new_canvas, true);
		imagecopyresampled($new_canvas, $original_canvas, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

		ob_start();
		imagepng($new_canvas, NULL, 9);
		return ob_get_clean();
		die;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));

		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'users', 'view');
		if($acl_status){
			$this->view->user = $this->_admin_user =  $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->_admin_user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		}else{
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$options = $bootstrap->getOptions();
		$this->_auth_key = $options['auth']['key'];
		$this->_auth_vector = $options['auth']['vector'];

		$this->_tmp_avatar_dir = '/avatar_tmp/';

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->admin_acl_roles = $this->_admin_acl_roles = Zend_Registry::get('admin_acl_roles');
		$this->_roles = array(
			'admin' => 'Admin',
			//'guest' => 'Guest'
		);
		$this->_website_member_roles = [];
		foreach ($this->_admin_acl_roles as $role_id => $role) {
			if (!$role['is_website_member']) {
				$this->_roles[$role_id] = $role['name'];
			}else {
				$this->_website_member_roles[$role_id] = $role['name'];
			}
		}
		$this->view->roles = $this->_roles;
		$this->view->website_member_roles = $this->_website_member_roles;

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
