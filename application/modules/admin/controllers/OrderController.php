<?php
class Admin_OrderController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/order/list');
	}

	public function listAction()
	{
		$this->view->placeholder('page_title')->set("Orders");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Orders</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->shipping_countries = $this->view->billing_countries =  $all_countries = Zend_Registry::get('countries')->toArray();
		$this->view->usstates = $this->_states = $this->_provinces = array();

		if ($settings_mapper->getValueByCode('ecommerce_shipping_countries')) {
			$selected = json_decode($settings_mapper->getValueByCode('ecommerce_shipping_countries'));
			$this->view->shipping_countries = array();
			foreach ($selected as $k => $country_code) {
				$this->view->shipping_countries[$country_code] = $all_countries[$country_code];
			}
		}
		if ($settings_mapper->getValueByCode('ecommerce_billing_countries')) {
			$selected = json_decode($settings_mapper->getValueByCode('ecommerce_billing_countries'));
			$this->view->billing_countries = array();
			foreach ($selected as $k => $country_code) {
				$this->view->billing_countries[$country_code] = $all_countries[$country_code];
			}
		}

		$regions = Zend_Registry::get('regions')->toArray();
		if (array_key_exists('US',$this->view->shipping_countries) || array_key_exists('US',$this->view->billing_countries)) {
			$this->_states = $regions['us']['region'];
		}
		if (array_key_exists('CA',$this->view->shipping_countries) || array_key_exists('CA',$this->view->billing_countries)) {
			$this->_provinces = $regions['ca']['region'];
		}
		//combine states/provinces into single list
		$this->view->usstates = $this->_states + $this->_provinces;

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','id');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');
		$this->view->date_from = $date_from = $this->_getParam('date_from','');
		$this->view->date_to = $date_to = $this->_getParam('date_to','');
		$this->view->shipping_region = $shipping_region = $this->_getParam('shipping_region','');

		$sql_date_from = $sql_date_to = '';
		if ($date_from) {
			$sql_date_from = new DateTime($date_from . ' 00:00:00', new DateTimeZone($this->view->site_timezone));
			$sql_date_from->setTimeZone(new DateTimeZone('UTC'));
			$sql_date_from = $sql_date_from->format('Y-m-d H:i:s');
		}
		if ($date_to) {
			$sql_date_to = new DateTime($date_to . ' 23:59:59', new DateTimeZone($this->view->site_timezone));
			$sql_date_to->setTimeZone(new DateTimeZone('UTC'));
			$sql_date_to = $sql_date_to->format('Y-m-d H:i:s');
		}

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha,
			'date_from' => $sql_date_from,
			'date_to' => $sql_date_to,
			'shipping_region' => $shipping_region
		);
		$mapper = new Admin_Model_Mapper_Order();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$results = $mapper->listSearch('o.created', 'asc', $search_keys);
			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'orders';

			$this->render('csvexport-orders');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function viewAction()
	{
		$this->view->return_to = $this->getRequest()->getParam('return_to','');

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Order();
			$this->view->order = $mapper->find($id);

			$mapper = new Admin_Model_Mapper_OrderAddress();
			$this->view->billing_address = $mapper->findByOrderId($id,'billing');
			$this->view->shipping_address = $mapper->findByOrderId($id,'shipping');
			if (!$this->view->shipping_address) $this->view->shipping_address = $this->view->billing_address;

			$mapper = new Admin_Model_Mapper_OrderNote();
			$this->view->notes = $mapper->findByOrderId($id);

			$mapper = new Admin_Model_Mapper_OrderPayment();
			$this->view->payment = $mapper->findByOrderId($id);

			$mapper = new Admin_Model_Mapper_OrderProfile();
			$this->view->profile = $mapper->findByOrderId($id);

			$mapper = new Admin_Model_Mapper_OrderShipping();
			$this->view->shipping = $mapper->findByOrderId($id);

			$mapper = new Admin_Model_Mapper_OrderItem();
			$this->view->items = $mapper->findByOrderId($id);

			$mapper = new Admin_Model_Mapper_OrderTotal();
			$this->view->totals = $mapper->findByOrderId($id);
		}else{
			$this->getResponse()->setRedirect('/admin/');
		}
	}

	public function statusAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$status = $this->_getParam("status","processing");
			//$tracking_method = $this->_getParam("tracking_method","");
			$tracking_numbers = $this->_getParam("tracking_numbers","");

			$mapper = new Admin_Model_Mapper_Order();
			$order = $mapper->find($id);
			$order->status = $status;
			$mapper->save($order);

			$mapper = new Admin_Model_Mapper_OrderItem();
			$items = $mapper->findByOrderId($id);

			foreach ($items as $item) {
				$item->status = $status;

				$mapper->save($item);
			}


			$order_history_mapper = new Admin_Model_Mapper_OrderNote();
			$order_history = new Application_Model_OrderNote();
			$order_history->order_id = $id;
			$order_history->type = 'invoice';
			$order_history->creator = $this->view->user->first_name.' '.$this->view->user->last_name;
			$order_history->creator_email = $this->view->user->email;
			$order_history->sent_to_customer = 'No';
			$order_history->display_to_customer = 'Yes';
			$order_history->note = 'Status changed to '.$status.' by '.$this->view->user->first_name.' '.$this->view->user->last_name;

			$order_history_mapper->save($order_history);

			//email customer if new status is shipped
			if ($status == 'Shipped') {
				$mapper = new Admin_Model_Mapper_OrderItem();
				foreach ($items as $item) {
					$item->status = $status;
					$item->tracking_code = $tracking_numbers;
					$item->shipped_date = new Zend_Db_Expr("DATE(NOW())");

					$mapper->save($item);
				}

				$mapper = new Admin_Model_Mapper_OrderShipping();
				$shipping = $mapper->findByOrderId($id);
				//$shipping->service = $tracking_method;
				$mapper->save($shipping);

				$mailbody = '<p>Thank you for your purchase.</p>
<p>Your order has shipped and is on it\'s way! See tracking number below.</p>

<p>
Tracking Number: '.$tracking_numbers.'
</p>
';

				$emails = new Application_Model_Emails();
				$emails->generic($mailbody, $order->email, 'Shipping notification from ' . $this->_site_name . ' - Order #'.$order->id, '');
			}

			$this->redirect('/admin/order/list');
		}else{
			$this->redirect('/admin/order/list');
		}
	}

	public function addressesAction()
	{
		$this->view->errors = array();

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)) {
			$note_data = $this->getRequest()->getParam('note','');
			$billing_address = $this->getRequest()->getParam('billing_address',array());
			if ($billing_address['country'] != 'US' && $billing_address['country'] != 'CA') $billing_address['region'] = $billing_address['region_alt'];
			$shipping_address = $this->getRequest()->getParam('shipping_address',array());
			if ($shipping_address['country'] != 'US' && $shipping_address['country'] != 'CA') $shipping_address['region'] = $shipping_address['region_alt'];
			$order_data = $this->getRequest()->getParam('order',array());

			$changes = array();

			$order_mapper = new Admin_Model_Mapper_Order();
			$order = $order_mapper->find($id);

			$order_old_data = $order->toArray();

			if (isset($order_data['email']) && $order_data['email']) {
				if ($order_data['email'] != $order_old_data['email']) {
					$order_old_data['email'] = $order_data['email'];
					$changes[] = "Customer Email Changed :: " . $order_old_data['email'] . " -> " . $order_data['email'];
				}
				$order_old_data['email'] = $order_data['email'];
			}

			if (isset($order_data['customer_id']) && $order_data['customer_id']) {
				if ($order_data['customer_id'] != $order_old_data['customer_id']) {
					$changes[] = "Customer Account Added - ".$order_data['customer_id'];
				}
				$order_old_data['customer_id'] = $order_data['customer_id'];
			}

			$order_info = new Application_Model_Order($order_old_data);
			$order_mapper->save($order_info);

			$note_mapper = new Admin_Model_Mapper_OrderNote();
			if ($note_data) {
				$note = new Application_Model_OrderNote();
				$note->type = 'note';
				$note->creator = $this->view->user->first_name. ' '. $this->view->user->last_name;
				$note->creator_email = $this->view->user->email;
				$note->display_to_customer = "No";
				$note->sent_to_customer = 'No';
				$note->order_id = $id;
				$note->note = $note_data;
				$note_mapper->save($note);
			}

			$mapper = new Admin_Model_Mapper_OrderAddress();
			//billing address
			$old_billing_address = $mapper->findByOrderId($id,'billing');

			$fields = array(
				'first_name' => 'Billing First Name Updated',
				'last_name' => 'Billing Laast Name Updated',
				'company' => 'Billing Company Updated',
				'address' => 'Billing Address 1 Updated',
				'address2' => 'Billing Address 2 Updated',
				'city' => 'Billing City Updated',
				'region' => 'Billing Region Updated',
				'postcode' => 'Billing Postcode Updated',
				'country' => 'Billing Country Updated',
				'phone' => 'Billing Phone Number Updated',
			);
			foreach ($fields as $key => $reason) {
				if ($billing_address[$key] != $old_billing_address->$key) {
					$changes[] = $reason.' :: '.$old_billing_address->$key.' -> '.$billing_address[$key];
				}
			}

			$customer_address_data = array(
				'id' => $old_billing_address->id,
				'order_id' => $id,
				'type' => 'billing',
				'first_name' => $billing_address['first_name'],
				'last_name' => $billing_address['last_name'],
				'company' => $billing_address['company'],
				'address' => $billing_address['address'],
				'address2' => $billing_address['address2'],
				'city' => $billing_address['city'],
				'region' => $billing_address['region'],
				'postcode' => $billing_address['postcode'],
				'country' => $billing_address['country'],
				'phone' => $billing_address['phone']
			);
			$customer_address_info = new Application_Model_OrderAddress($customer_address_data);
			$mapper->save($customer_address_info);

			//shipping address
			$old_shipping_address = $mapper->findByOrderId($id,'shipping');

			$fields = array(
				'first_name' => 'Shipping First Name Updated',
				'last_name' => 'Shipping Laast Name Updated',
				'company' => 'Shipping Company Updated',
				'address' => 'Shipping Address 1 Updated',
				'address2' => 'Shipping Address 2 Updated',
				'city' => 'Shipping City Updated',
				'region' => 'Shipping Region Updated',
				'postcode' => 'Shipping Postcode Updated',
				'country' => 'Shipping Country Updated',
				'phone' => 'Shipping Phone Number Updated',
			);
			foreach ($fields as $key => $reason) {
				if ($shipping_address[$key] != $old_shipping_address->$key) {
					$changes[] = $reason.' :: '.$old_shipping_address->$key.' -> '.$shipping_address[$key];
				}
			}

			$customer_address_data = array(
				'id' => ($old_shipping_address)?$old_shipping_address->id:'',
				'order_id' => $id,
				'type' => 'shipping',
				'first_name' => $shipping_address['first_name'],
				'last_name' => $shipping_address['last_name'],
				'company' => $shipping_address['company'],
				'address' => $shipping_address['address'],
				'address2' => $shipping_address['address2'],
				'city' => $shipping_address['city'],
				'region' => $shipping_address['region'],
				'postcode' => $shipping_address['postcode'],
				'country' => $shipping_address['country'],
				'phone' => $shipping_address['phone']
			);
			$customer_address_info = new Application_Model_OrderAddress($customer_address_data);
			$mapper->save($customer_address_info);

			//save auto note on what changed
			if ($changes) {
				$note_data = implode("<br/>", $changes);

				$note = new Application_Model_OrderNote();
				$note->type = 'note';
				$note->creator = $this->view->user->first_name . ' ' . $this->view->user->last_name;
				$note->creator_email = $this->view->user->email;
				$note->display_to_customer = "No";
				$note->sent_to_customer = 'No';
				$note->order_id = $id;
				$note->note = $note_data;
				$note_mapper->save($note);
			}

			$goto_id = $id;
			$this->getResponse()->setRedirect('/admin/order/view/id/'.$goto_id);
		}else {
			$this->getResponse()->setRedirect('/admin/order/list');
		}
	}

	public function itemsAction()
	{
		$this->view->errors = array();

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)) {
			$billing_address = $this->getRequest()->getParam('billing_address',array());
			if ($billing_address['country'] != 'US' && $billing_address['country'] != 'CA') $billing_address['region'] = $billing_address['region_alt'];
			$shipping_address = $this->getRequest()->getParam('shipping_address',array());
			if ($shipping_address['country'] != 'US' && $shipping_address['country'] != 'CA') $shipping_address['region'] = $shipping_address['region_alt'];

			$changes = array();

			$order_mapper = new Admin_Model_Mapper_Order();
			//$order = $order_mapper->find($id);

			$note_mapper = new Admin_Model_Mapper_OrderNote();

			//update product statuses if changed
			$mapper = new Admin_Model_Mapper_OrderItem();
			$items = $mapper->findByOrderId($id);

			$product_mapper = new Admin_Model_Mapper_Product();

			$canceled_items = $returned_items = $reship_items = array();
			$prod_updates = $this->getRequest()->getParam('products',array());

			foreach ($items as $item) {
				if ($prod_updates[$item->id]['status'] != $item->status) {
					if ($prod_updates[$item->id]['status'] == 'Canceled') {
						$canceled_items[] = array('sku' => $item->sku, 'qty' => $prod_updates[$item->id]['qty'], 'paid_price' => $item->paid_price);
						$changes[] = "Item Canceled :: {$item->sku} qty: {$prod_updates[$item->id]['qty']}";
					}elseif ($prod_updates[$item->id]['status'] == 'Product Returned') {
						$returned_items[] = array('sku' => $item->sku, 'qty' => $prod_updates[$item->id]['qty']);
						$changes[] = "Item Returned :: {$item->sku} qty: {$prod_updates[$item->id]['qty']}";
					}elseif ($prod_updates[$item->id]['status'] == 'Reship') {
						$reship_items[] = array('sku' => $item->sku, 'qty' => $prod_updates[$item->id]['qty'], 'order_item_id'=>$item->id);
						$changes[] = "Item Reshipped :: {$item->sku} qty: {$prod_updates[$item->id]['qty']}";
					}else {
						//$product = $product_mapper->find($item->product_id);

						$item->status = $prod_updates[$item->id]['status'];
						$mapper->save($item, false);

						$changes[] = "Item Status Changed to {$item->status} :: {$item->sku} qty: {$prod_updates[$item->id]['qty']}";
					}
				}
			}
			//update overall order status (in case item statuses changed)
			$order_mapper->setShipStatus($id);

			if ($canceled_items) {
				$refund_amount = 0;
				foreach ($canceled_items as $item) {
					$refund_amount += ($item['qty'] * $item['paid_price']);
				}

				$response = $this->refund($id, $refund_amount);
				if ($response['status'] == 'success') {

				}
				$this->_flashMessenger->setNamespace('info')->addMessage($response['message']);
			}
/*
			$op = new Admin_Model_OrderProcess();
			if ($canceled_items) {
				$op->notifyCancelEmail($id,$canceled_items, false);
			}
			if ($returned_items) {
				$op->returns($id,$returned_items);
			}
			if ($reship_items) {
				$op->reshipItems($id,$reship_items);
			}
*/
			//save auto note on what changed
			if ($changes) {
				$note_data = implode("<br/>", $changes);

				$note = new Application_Model_OrderNote();
				$note->type = 'note';
				$note->creator = $this->view->user->first_name . ' ' . $this->view->user->last_name;
				$note->creator_email = $this->view->user->email;
				$note->display_to_customer = "No";
				$note->sent_to_customer = 'No';
				$note->order_id = $id;
				$note->note = $note_data;
				$note_mapper->save($note);
			}


			$goto_id = $id;
			$this->getResponse()->setRedirect('/admin/order/view/id/'.$goto_id);
		}else {
			$this->getResponse()->setRedirect('/admin/order/list');
		}
	}

	public function cancelAction()
	{
		$this->view->errors = array();

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)) {
			$order_mapper = new Admin_Model_Mapper_Order();
			$order = $order_mapper->find($id);

			$totals_mapper = new Admin_Model_Mapper_OrderTotal();
			$amount = $totals_mapper->findByOrderIdAndCode($id,'total');

			$response = $this->refund($id, $amount->amount);
			if ($response['status'] == 'success') {
				$order->status = 'Canceled';
				$order_mapper->save($order);

				$mapper = new Admin_Model_Mapper_OrderItem();
				$items = $mapper->findByOrderId($id);

				foreach ($items as $item) {
					$item->status = 'Canceled';
					$mapper->save($item);
				}
			}
			$this->_flashMessenger->setNamespace('info')->addMessage($response['message']);

			$goto_id = $id;
			$this->getResponse()->setRedirect('/admin/order/view/id/'.$goto_id);
		}else {
			$this->getResponse()->setRedirect('/admin/order/list');
		}
	}

	public function noteAddAction()
	{
		$this->view->errors = array();

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)) {
			$note_mapper = new Admin_Model_Mapper_OrderNote();

			if ($note_data = $this->getRequest()->getParam('note','')) {
				$note = new Application_Model_OrderNote();
				$note->type = 'note';
				$note->creator = $this->view->user->first_name . ' ' . $this->view->user->last_name;
				$note->creator_email = $this->view->user->email;
				$note->display_to_customer = $this->getRequest()->getParam('display_to_customer', 'No');
				$note->sent_to_customer = $this->getRequest()->getParam('sent_to_customer', 'No');
				$note->order_id = $id;
				$note->note = $note_data;
				$note_mapper->save($note);

				if ($this->getRequest()->getParam('sent_to_customer', 'No') == 'Yes') {
					$order_mapper = new Admin_Model_Mapper_Order();
					$order = $order_mapper->find($id);

					$emails = new Application_Model_Emails();
					$emails->generic(nl2br($note_data), $order->email, 'Order #'.$order->id.' Note', '');
				}
			}

			$goto_id = $id;
			$this->getResponse()->setRedirect('/admin/order/view/id/'.$goto_id);
		}else {
			$this->getResponse()->setRedirect('/admin/order/list');
		}
	}

	public function refundAction()
	{
		$this->view->errors = array();

		if ($this->getRequest()->isPost()) {
			$id = $this->getRequest()->getParam('order_id', 0);
			$amount = $this->getRequest()->getParam('amount', 0);
			$comment = $this->getRequest()->getParam('note', '');
			if (!empty($id) && is_numeric($id) && $amount) {
				$order_mapper = new Admin_Model_Mapper_Order();
				$order = $order_mapper->find($id);

				if (!$order) {
					$this->_flashMessenger->setNamespace('error')->addMessage("Order not found");
					$this->getResponse()->setRedirect('/admin/order/list');
				}else {
					$totals_mapper = new Admin_Model_Mapper_OrderTotal();
					$total = $totals_mapper->findByOrderIdAndCode($id, 'total');

					$sql = $this->_db->quoteInto("SELECT SUM(amount) FROM payment_history WHERE order_id = ? AND status = 'refund' ", $id);
					$refunded = $this->_db->fetchOne($sql);

					if ($amount <= ($total->amount - $refunded)) {
						$response = $this->refund($id, $amount);
						if ($response['status'] == 'success' && $amount == ($total->amount - $refunded)) {
							$order->status = 'Canceled';
							$order_mapper->save($order);
						}

						$note_mapper = new Admin_Model_Mapper_OrderNote();
						$note = new Application_Model_OrderNote();
						$note->type = 'note';
						$note->creator = $this->view->user->first_name . ' ' . $this->view->user->last_name;
						$note->creator_email = $this->view->user->email;
						$note->display_to_customer = $this->getRequest()->getParam('display_to_customer', 'Yes');
						$note->sent_to_customer = $this->getRequest()->getParam('sent_to_customer', 'No');
						$note->order_id = $id;
						$note->note = "$" . $amount . " refunded \r\n $comment";
						$note_mapper->save($note);

						$this->_flashMessenger->setNamespace('info')->addMessage($response['message']);
					} else {
						$this->_flashMessenger->setNamespace('error')->addMessage("Refund amount exceeds available amount");
					}

					$goto_id = $id;
					$this->getResponse()->setRedirect('/admin/order/view/id/' . $goto_id);
				}
			} else {
				$this->getResponse()->setRedirect('/admin/order/list');
			}
		}
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'ecommerce', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Order Taken' => 'Order Taken',
			'Shipped' => 'Shipped',
			'Canceled' => 'Canceled'
		);

		$this->view->product_statuses = array(
			'Order Taken' => 'Order Taken',
			'Processing' => 'Processing (at warehouse)',
			'Shipped' => 'Shipped',
			'Canceled' => 'Canceled',
			'Product Returned' => 'Product Returned',
			'Error' => 'Error',
			'Hold' => 'Hold'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->_site_name = ($site_name = $settings_mapper->getValueByCode('email_site_name'))?$site_name:$settings_mapper->getValueByCode('site_name');

		$this->view->shipping_countries = $this->view->billing_countries =  $all_countries = Zend_Registry::get('countries')->toArray();
		$this->view->usstates = $this->_states = $this->_provinces = array();

		if ($settings_mapper->getValueByCode('ecommerce_shipping_countries')) {
			$selected = json_decode($settings_mapper->getValueByCode('ecommerce_shipping_countries'));
			$this->view->shipping_countries = array();
			foreach ($selected as $k => $country_code) {
				$this->view->shipping_countries[$country_code] = $all_countries[$country_code];
			}
		}
		if ($settings_mapper->getValueByCode('ecommerce_billing_countries')) {
			$selected = json_decode($settings_mapper->getValueByCode('ecommerce_billing_countries'));
			$this->view->billing_countries = array();
			foreach ($selected as $k => $country_code) {
				$this->view->billing_countries[$country_code] = $all_countries[$country_code];
			}
		}

		$regions = Zend_Registry::get('regions')->toArray();
		if (array_key_exists('US',$this->view->shipping_countries) || array_key_exists('US',$this->view->billing_countries)) {
			$this->_states = $regions['us']['region'];
		}
		if (array_key_exists('CA',$this->view->shipping_countries) || array_key_exists('CA',$this->view->billing_countries)) {
			$this->_provinces = $regions['ca']['region'];
		}
		//combine states/provinces into single list
		$this->view->usstates = $this->_states + $this->_provinces;

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}

	private function refund($order_id, $amount)
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($settings_mapper->getValueByCode('ecommerce_authnet_status') == 'Enabled') {
			$payment_method_cc = 'authorize.net';
		}elseif ($settings_mapper->getValueByCode('ecommerce_mes_status') == 'Enabled') {
			$payment_method_cc = 'mes';
		}elseif ($settings_mapper->getValueByCode('ecommerce_stripe_status') == 'Enabled') {
			$payment_method_cc = 'stripe';
		}elseif ($settings_mapper->getValueByCode('ecommerce_payflowpro_status') == 'Enabled') {
			$payment_method_cc = 'payflowpro';
		}

		$sql = "SELECT * FROM payment_history WHERE order_id = '$order_id' AND status = 'sale' ORDER BY created DESC LIMIT 1";
		$history = $this->_db->fetchRow($sql);

		if ($history['type'] == "paypal") {
			$paypal = new Application_Model_Paypal();
			$response = $paypal->refundTransaction($history['transaction_id'], $amount);

			if (!$response['error_message']) {
				$mapper = new Application_Model_Mapper_PaymentHistory();
				$payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
					'order_id' => $order_id,
					'type' => 'paypal',
					'cc_type' => $history['cc_type'],
					'amount' => $amount,
					'amount_remaining' => null,
					'transaction_id' => $response['response']['PNREF'],
					'response' => serialize($response['response']),
					'status' => 'refund'
				)));

				return array('status'=>'success','message'=>"$".$amount." Refunded");
			}else {
				//error
				return array('status'=>'error','message'=>$response['error_message']);
			}
		} elseif ($history['type'] == "cc") {
			if ($payment_method_cc == 'payflowpro') {
				$payflow = new Application_Model_PayflowPro();
				$response = $payflow->credit_transaction($history['transaction_id'], $amount);

				if (isset($response['response']['RESULT']) && $response['response']['RESULT'] == 0) {
					$mapper = new Application_Model_Mapper_PaymentHistory();
					$payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
						'order_id' => $order_id,
						'type' => 'cc',
						'cc_type' => $history['cc_type'],
						'amount' => $amount,
						'amount_remaining' => null,
						'transaction_id' => $response['response']['PNREF'],
						'response' => serialize($response['response']),
						'status' => 'refund'
					)));

					return array('status'=>'success','message'=>"$".$amount." Refunded");
				}else {
					//error
					return array('status'=>'error','message'=>$response['response']['RESPMSG']);
					//return array('status'=>'error','message'=>$response['error_message']);
				}
			} elseif ($payment_method_cc == 'authorize.net') {
				$authnet = new Application_Model_AuthorizeNet();
				$response = $authnet->refundTransaction($history['transaction_id'], $amount);

				if (($response['response'] != null) && ($response['response']->getResponseCode() == "1")) {
					$mapper = new Application_Model_Mapper_PaymentHistory();
					$payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
						'order_id' => $order_id,
						'type' => 'cc',
						'cc_type' => $history['cc_type'],
						'amount' => $amount,
						'amount_remaining' => null,
						'transaction_id' => $response['response']->getTransId(),
						'response' => json_encode($response['response']),
						'status' => 'refund'
					)));

					return array('status'=>'success','message'=>"$".$amount." Refunded");
				}else {
					//error
					return array('status'=>'error','message'=>$response['error_message']);
				}
			} elseif ($payment_method_cc == 'stripe') {
				$stripe = new Application_Model_Stripe();
				$response = $stripe->refund($history['transaction_id'], $amount);

				if (!$response['error_message']) {
					$mapper = new Application_Model_Mapper_PaymentHistory();
					$payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
						'order_id' => $order_id,
						'type' => 'cc',
						'cc_type' => $history['cc_type'],
						'amount' => $amount,
						'amount_remaining' => null,
						'transaction_id' => $response['response']->id,
						'response' => serialize($response['response']),
						'status' => 'refund'
					)));

					return array('status'=>'success','message'=>"$".$amount." Refunded");
				}else {
					//error
					return array('status'=>'error','message'=>$response['error_message']);
				}
			}
		} elseif ($history['type'] == "amazonpay") {
			$amazon_pay = new Application_Model_AmazonPay();
			$response = $amazon_pay->refundTransaction($history['transaction_id'], $amount, $history['order_id']);

			if (!$response['error_message']) {
				$mapper = new Application_Model_Mapper_PaymentHistory();
				$payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
					'order_id' => $order_id,
					'type' => 'amazonpay',
					'cc_type' => $history['cc_type'],
					'amount' => $amount,
					'amount_remaining' => null,
					'transaction_id' => $response['response']->RefundResult->RefundDetails->AmazonRefundId,
					'response' => json_encode($response['response']),
					'status' => 'refund'
				)));

				return array('status'=>'success','message'=>"$".$amount." Refunded");
			}else {
				//error
				return array('status'=>'error','message'=>$response['error_message']);
			}
		}

	}
}
