<?php
use Cocur\Slugify\Slugify;

class Admin_LibraryController extends Zend_Controller_Action
{
	/** @var  Zend_Db_Adapter_Abstract */
	protected $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/library/list');
	}

	public function listAction()
	{
		$this->view->messages = $this->_flashMessenger->getMessages();
		$this->view->placeholder('page_title')->set("Library Manager");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Library Manager</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page', 1);
		$this->view->dir = $dir = $this->_getParam('dir', 'ASC');
		$this->view->sort = $sort = $this->_getParam('sort', 'label');

		$this->view->mask = $mask = trim($this->_getParam('mask', ''));
		$this->view->type = $type = $this->_getParam('type', '');
		$this->view->alpha = $alpha = $this->_getParam('alpha', '');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_LibraryModule();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function chooseModuleAction()
	{
		//
	}

	public function addAction()
	{
		$type = $this->getRequest()->getParam('type', 'wysiwyg');

		$entity = new Application_Model_LibraryModule();
		$entity->type = $type;
		$this->view->module = $entity;

		$this->render('component-module');
	}

	public function editAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		$mapper = new Admin_Model_Mapper_LibraryModule();
		$this->view->module = $mapper->find($id);

		$this->render('component-module');
	}

	public function saveAction()
	{
		$data = $this->getRequest()->getParam('data', array());
		$content = $this->getRequest()->getParam('content', array());
		$padding = $this->getRequest()->getParam('padding', array());
		$styles = $this->getRequest()->getParam('styles', array());
		$settings = $this->getRequest()->getParam('settings', array());

		//json encode all the content items and save into data json_content
		$data['json_content'] = json_encode($content);
		$data['json_styles'] = json_encode($styles);
		$data['json_settings'] = json_encode($settings);

		$mapper = new Admin_Model_Mapper_LibraryModule();
		$entity = $mapper->find($data['id']);

		if (!$entity) {
			$entity = new Application_Model_LibraryModule();

			if (!$data['label']) {
				$data['label'] = "Library Module - ".time();
			}
		}

		/*
		//convert padding to string
		$data['padding'] = '';
		$data_pad = array();
		foreach ($padding as $pad) {
			if ($clean = str_replace(" ", "", $pad)) {
				if (!is_null($clean)) {
					$data_pad[] = preg_replace("/[^0-9]/", "", $clean) . "px";
				}
			} else {
				$data_pad[] = "auto";
			}
		}

		$data['padding'] = implode(" ", $data_pad);

		if (trim($data['padding']) == '') {
			$data['padding'] = new Zend_Db_Expr("NULL");
		}
		*/

		$entity_data = array_merge($entity->toArray(), $data);
		$mapper->save(new Application_Model_LibraryModule($entity_data));

		echo "0";
		exit;
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);

		if ($id && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_LibraryModule();
			$mapper->delete($mapper->find($id));
		}

		$this->redirect("/admin/library/list");
	}

	public function copyAction()
	{

		$id = $this->getRequest()->getParam('id', 0);

		if ($id && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_LibraryModule();
			$module = $mapper->find($id);

			$module->id = null;
			$module->label = $module->label.' - '.time();

			$mapper->save(new Application_Model_LibraryModule($module->toArray()));

			$this->redirect("/admin/library/list");
		}
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'cms', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array('automatic_serialization' => true, 'lifetime' => 0);
		$this->backendOptions = array('cache_dir' => $cache_dir);

		$this->view->filemanager_root = $this->_filemanager_root = "/userFiles/uploads/";

		$this->view->statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled',
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
