<?php
use Cocur\Slugify\Slugify;

class Admin_InstalinkController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/instalink/link-list');
	}

	public function linkListAction()
	{
		$this->view->placeholder('page_title')->set("InstaLinks Links");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">InstaLinks</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','sort_order');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');

		$search_keys = array(
			'keyword' => $mask,
			'status' => $status,
		);
		$mapper = new Admin_Model_Mapper_InstalinksLink();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'instalinks_links';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function linkSortAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$mapper = new Admin_Model_Mapper_InstalinksLink();
		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach ($values as $k => $id) {
			if (!empty($id)) {
				$mapper->setSortOrder($id, $sort);
				$sort++;
			}
		}
	}

	public function linkAddAction()
	{
		$this->view->placeholder('page_title')->set("InstaLinks");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/instalink/link-list">InstaLinks</a></li>
				<li class="active">Add Link</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_InstalinksLink();
		$form->getElement('cmd')->setValue('add');

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($settings_mapper->getValueByCode('instalinks_layout_style') == 'tile' || $settings_mapper->getValueByCode('instalinks_layout_style') == '') {
			$form->getElement('icon')->setValue($settings_mapper->getValueByCode('instalinks_tile_icon'));
		}

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('link-form');
	}

	public function linkEditAction()
	{
		$this->view->category_id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Categories");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/instalink/link-list">InstaLinks</a></li>
				<li class="active">Edit InstaLinks Link: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_InstalinksLink();
			$mapper = new Admin_Model_Mapper_InstalinksLink();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
			if (($settings_mapper->getValueByCode('instalinks_layout_style') == 'tile' || $settings_mapper->getValueByCode('instalinks_layout_style') == '') && !$data['icon']) {
				$form->getElement('icon')->setValue($settings_mapper->getValueByCode('instalinks_tile_icon'));
			}

			$json = json_decode($data['json_content']);
			foreach ($json as $k => $v) {
				$data[$k] = $v;
			}
			$json = json_decode($data['json_settings']);
			foreach ($json as $k => $v) {
				$data[$k] = $v;
			}

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('link-form');
		}else{
			$this->getResponse()->setRedirect('/admin/instalink/link-list');
		}
	}

	public function linkViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Category();
			$this->view->category = $mapper->find($id);

			$this->view->placeholder('page_title')->set("InstaLinks");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/instalink/link-list">InstaLinks</a></li>
				<li class="active">View InstaLinks Link: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);
		}else{
			$this->getResponse()->setRedirect('/admin/instalink/link-list');
		}
	}

	public function linkSaveAction()
	{
		$form = new Admin_Form_InstalinksLink();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_InstalinksLink();

		if( $form->isValid($this->getRequest()->getParams())) {
			if (!$data['sort_order']) $data['sort_order'] = 9999;
			/*
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}
			*/
			if ($this->isempty($data['image'])) {
				$data['image'] = new Zend_Db_Expr("NULL");
			}

			//aggregate fields into json blocks
			$json_content = []; $json_settings = [];

			$json_content['content'] = $data['content'];
			$json_content['text_valign'] = $data['text_valign'];
			$json_settings['button_color'] = $data['button_color'];
			$json_settings['button_border_color'] = $data['button_border_color'];
			$json_settings['button_border_size'] = $data['button_border_size'];
			$json_settings['icon'] = $data['icon'];
			$json_settings['icon_position'] = $data['icon_position'];
			$json_settings['icon_color'] = $data['icon_color'];

			$data['json_content'] = json_encode($json_content);
			$data['json_settings'] = json_encode($json_settings);


			$entity = new Application_Model_InstalinksLink($data);
			$id = $mapper->save($entity);

			$this->getResponse()->setRedirect('/admin/instalink/link-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$this->view->category_id = $data['id'];

			$form->populate($data);
			$this->view->form = $form;

			$this->render('link-form');
		}
	}

	public function linkDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_InstalinksLink();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/instalink/link-list');
	}

	public function linkCopyAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_InstalinksLink();
			$entity = $mapper->find($id);
			$data = $entity->toArray();

			$data['title'] = $data['title'].' - COPY';
			unset($data['id']);
			unset($data['modified']);
			$data['created'] = new Zend_Db_Expr("NOW()");

			$entity = new Application_Model_InstalinksLink($data);
			$new_id = $mapper->save($entity);
		}

		$this->getResponse()->setRedirect('/admin/instalink/link-edit/id/'.$new_id);
	}

	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'cms', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
