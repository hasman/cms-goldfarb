<?php
class Admin_AnalyticsController extends Zend_Controller_Action
{
	/** @var  Zend_Db_Adapter_Mysqli */
	protected $db;
	protected $svc;
	protected $reports = [];
	protected $labels = [
		'ga:date' => 'Date',
		'ga:sessions' => 'User Sessions',
		'ga:users' => 'Users',
		'ga:pageviews' => 'Page Views',
		'ga:pageviewsPerSession' => 'Page Views Per Sessions',
		'ga:avgSessionDuration' => 'Avg. Session Duration',
		'ga:bounceRate' => 'Bounce Rate',
		'ga:percentNewSessions' => '% New Sessions'

	];

	public function overviewAction()
	{
		$startdate = $this->getRequest()->getParam('startdate', date('Y-m-d', strtotime('-7 days')));
		$enddate = $this->getRequest()->getParam('enddate', date('Y-m-d', strtotime('today')));
		$dimensions = $this->getRequest()->getParam('dimensions', 'ga:date');
		$metrics = $this->getRequest()->getParam('metrics', 'ga:sessions');
		$filters = $this->getRequest()->getParam('filters', '');
		$sorts = $this->getRequest()->getParam('sorts', 'ga:date');
		$options = $this->getRequest()->getParam('options', []);

		$metrics = explode(',', $metrics);
		$svc = new Application_Service_GoogleAnalytics();
		$dimensions = explode(',', $dimensions);
		if (empty($filters)) {
			$filters = [];
		} else {
			$filters = explode(',', $filters);
		}
		$sorts = explode(',', $sorts);
		/** @var Google_Service_Analytics_GaData $data */
		$data = $svc->report($startdate, $enddate, $dimensions, $metrics, $filters, $sorts, $options);
		$output = [];
		if (!empty($data)) {
			$output = $data->getDataTable()->toSimpleObject();

			/**
			 * @var integer $index
			 * @var Google_Service_Analytics_GaDataColumnHeaders $col
			 */
			foreach ($output->cols as $index => $col) {
				if (isset($this->labels[$col->label])) {
					$output->cols[$index]->label = $this->labels[$col->label];
				}
			}
		}
		$this->_helper->json->sendJson($output);
	}

	public function detailsAction()
	{
		$output = [];
		$metric = $this->getParam('metric', null);
		if (is_null($metric)) {
			return $this->_helper->json->sendJson([]);
		}

		$metrics = $this->listOfMetrics();
		if (isset($metrics[$metric])) {
			$output = $metrics[$metric];
		} else {
			return $this->_helper->json->sendJson([]);
		}

		$output = $this->getReport($metric);
		/**
		 * @var integer $index
		 * @var Google_Service_Analytics_GaDataColumnHeaders $col
		 */
		foreach ($output->cols as $index => $col) {
			if (isset($this->labels[$col->label])) {
				$output->cols[$index]->label = $this->labels[$col->label];
			}
		}

		return $this->_helper->json->sendJson($output);
	}

	public function getReport($metric)
	{
		$report = [];
		switch ($metric) {
			case 'ga:users':
			default:
				$startdate = $this->getRequest()->getParam('startdate', date('Y-m-d', strtotime('-7 days')));
				$enddate = $this->getRequest()->getParam('enddate', date('Y-m-d', strtotime('today')));
				$dimensions = $this->getRequest()->getParam('dimensions', 'ga:date');
				$filters = $this->getRequest()->getParam('filters', '');
				$sorts = $this->getRequest()->getParam('sorts', 'ga:date');
				$options = $this->getRequest()->getParam('options', []);
				$svc = new Application_Service_GoogleAnalytics();
				$metrics = explode(',', $metric);
				$dimensions = explode(',', $dimensions);
				if (empty($filters)) {
					$filters = [];
				} else {
					$filters = explode(',', $filters);
				}
				$sorts = explode(',', $sorts);
				/** @var Google_Service_Analytics_GaData $data */
				$data = $svc->report($startdate, $enddate, $dimensions, $metrics, $filters, $sorts, $options);
				$report = $data->getDataTable()->toSimpleObject();
				break;
		}
		return $report;
	}

	public function listOfMetrics()
	{
		$metrics = [
			'ga:sessions' => [
				'code' => 'ga:sessions',
				'label' => '',
				'color' => '',
				'logo' => '',
			],
			'ga:users' => [
				'code' => 'ga:users',
				'label' => '',
				'color' => '',
				'logo' => '',
			],
			'ga:pageviews' => [
				'code' => 'ga:pageviews',
				'label' => '',
				'color' => '',
				'logo' => '',
			],
			'ga:pageviewsPerSession' => [
				'code' => 'ga:pageviewsPerSession',
				'label' => '',
				'color' => '',
				'logo' => '',
			],
			'ga:avgSessionDuration' => [
				'code' => 'ga:avgSessionDuration',
				'label' => '',
				'color' => '',
				'logo' => '',
			],
			'ga:bounceRate' => [
				'code' => 'ga:bounceRate',
				'label' => '',
				'color' => '',
				'logo' => '',
			],
			'ga:percentNewSessions' => [
				'code' => 'ga:percentNewSessions',
				'label' => '',
				'color' => '',
				'logo' => '',
			]
		];
		return $metrics;

	}

	function init()
	{
		// Disable Layout and View Rendering
		/** @var Zend_Controller_Request_Http $request */
		$request = $this->getRequest();
		if ($request->isXmlHttpRequest()) {
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
		}

		$this->db = Zend_Db_Table::getDefaultAdapter();

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}else{
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		}

		$this->svc = new Application_Service_GoogleAnalytics();

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}
}
