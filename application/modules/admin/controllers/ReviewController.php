<?php

class Admin_ReviewController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/review/product-list');
	}

	public function productListAction()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'product_reviews', 'view');
		if (!$acl_status) {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$this->view->placeholder('page_title')->set("Product Reviews");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Product Reviews</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','pr.created');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_ProductReview();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'product_reviews';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function productViewAction()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'product_reviews', 'view');
		if (!$acl_status) {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductReview();
			$this->view->review = $review = $mapper->find($id);
			//Get the list of all review images
			$review = $review->toArray();
			$mapper = new Admin_Model_Mapper_ProductReviewFile();
			$this->view->images = $mapper->fetchByReviewId($id);

			$this->view->placeholder('page_title')->set("Product Reviews");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/review/product-list">Product Reviews</a></li>
				<li class="active">View Product Review: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);
		}else{
			$this->getResponse()->setRedirect('/admin/product/');
		}
	}

	public function productSaveAction()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'product_reviews', 'view');
		if (!$acl_status) {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_ProductReview();

		if ($this->getRequest()->isPost()) {
			$id = $this->getRequest()->getParam('id',0);
            $name = $this->getRequest()->getParam('name');
            $review = $this->getRequest()->getParam('review');

			if(!empty($id) && is_numeric($id)) {
				$entity = $mapper->find($id);

				$status = $this->getRequest()->getParam('status','Pending');
				$is_spam = $this->getRequest()->getParam('is_spam','Yes');

				$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
				if ($settings_mapper->getValueByCode('akismet_api_key')) {
					$akismet = new Application_Model_Akismet();

					$verify_data = array(
						'user_ip' => $entity->remote_addr,
						'referrer' => $entity->referrer,
						'comment_type' => 'review',
						'comment_author' => $entity->name,
						'comment_author_email' => $entity->email,
						'comment_content' => $entity->review,
						'comment_date_gmt' => date("c", strtotime($entity->created))
					);

					if ($is_spam == 'Yes' && $entity->is_spam == 'No') {
						$akismet->submitSpam($verify_data);
					}elseif($is_spam == 'No' && $entity->is_spam == 'Yes') {
						$akismet->submitHam($verify_data);
					}
				}

				$entity->status = $status;
				$entity->is_spam = $is_spam;
                $entity->name = $name;
                $entity->review = $review;

				$mapper->save($entity);
			}
		}

		$this->getResponse()->setRedirect('/admin/review/product-list');
	}

	public function productDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductReview();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/review/product-list');
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'ecommerce', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Pending' => 'Pending',
			'Approved' => 'Approved',
			'Denied' => 'Denied'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');

	}
}
