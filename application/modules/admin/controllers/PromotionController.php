<?php
class Admin_PromotionController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/promotion/list');
	}

	public function listAction()
	{
		$this->view->placeholder('page_title')->set("Promotions");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Promotions</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','title');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->sitewide = $sitewide = $this->_getParam('sitewide','');
		$this->view->start_date_from = $start_date_from = $this->getRequest()->getParam('start_date_from', '');
		$this->view->start_date_to = $start_date_to = $this->getRequest()->getParam('start_date_to', '');
		$this->view->end_date_from = $end_date_from = $this->getRequest()->getParam('end_date_from', '');
		$this->view->end_date_to = $end_date_to = $this->getRequest()->getParam('end_date_to', '');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'sitewide' => $sitewide,
			'start_date_from' => $start_date_from,
			'start_date_to' => $start_date_to,
			'end_date_from' => $end_date_from,
			'end_date_to' => $end_date_to,
		);
		$mapper = new Admin_Model_Mapper_Promo();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->view->layout()->disableLayout();
			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'promotions';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function addAction()
	{
		$this->view->placeholder('page_title')->set("Promotions");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/promotion">Promotions</a></li>
				<li class="active">Add Promotion</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_PromoForm();
		$form->getElement('cmd')->setValue('add');
		$form->getElement('start_time')->setValue('00:00');
		$form->getElement('end_time')->setValue('23:59');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('form');
	}

	public function editAction()
	{
		$this->view->placeholder('page_title')->set("Promotions");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/promotion">Promotions</a></li>
				<li class="active">Edit Promotion</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->view->promotion_id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_PromoForm();
			$mapper = new Admin_Model_Mapper_Promo();
			$this->view->promotion = $entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$mapper = new Admin_Model_Mapper_PromoSingleCode();
			$this->view->promo_codes = $mapper->findByPromoId($id);

			$this->render('edit-form');
		}else{
			$this->getResponse()->setRedirect('/admin/promotion/list');
		}
	}

	public function viewAction()
	{
		$this->view->placeholder('page_title')->set("Promotions");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/promotion">Promotions</a></li>
				<li class="active">View Promotion</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Promo();
			$this->view->promotion = $mapper->find($id);

			$mapper = new Admin_Model_Mapper_PromoThreshold();
			$this->view->thresholds = $mapper->fetchByPromoId($id);

			$mapper = new Admin_Model_Mapper_PromoSingleCode();
			$this->view->promo_codes = $mapper->findByPromoId($this->view->promotion->id);
		}else{
			$this->getResponse()->setRedirect('/admin/promotion/');
		}
	}

	public function saveAction()
	{
		$form = new Admin_Form_PromoForm();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_Promo();

		if( $form->isValid($this->getRequest()->getParams())){
			$tz_from = 'UTC'; //TODO - leaving this here so we can set site timezone as a setting in the future
			$tz_to = 'UTC';
			$format = 'Y-m-d H:i:s';

			$datetime = $data['start_date'].' '.$data['start_time'];
			$dt = new DateTime($datetime, new DateTimeZone($tz_from));
			$dt->setTimeZone(new DateTimeZone($tz_to));
			$data['utc_start_datetime'] = $dt->format($format);

			$datetime = $data['end_date'].' '.$data['end_time'];
			$dt = new DateTime($datetime, new DateTimeZone($tz_from));
			$dt->setTimeZone(new DateTimeZone($tz_to));
			$data['utc_end_datetime'] = $dt->format($format);

			$entity = new Application_Model_Promo($data);
			$id = $mapper->save($entity);

			if ($data['cmd'] == 'add') {
				$this->getResponse()->setRedirect('/admin/promotion/edit/id/'.$id);
			}else {
				$this->getResponse()->setRedirect('/admin/promotion/list');
			}
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$this->view->promotion_id = $data['id'];

			$form->populate($data);
			$this->view->form = $form;

			$this->view->errors[] = "Please fill in all required fields highlighted";

			if ($data['cmd'] == 'edit') {
				$this->render('edit-form');
			}else {
				$this->render('form');
			}
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Promo();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/promotion/list');
	}

	public function createCodesAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		$count = $this->getRequest()->getParam('count',0);

		$new_list = array();
		$mapper = new Admin_Model_Mapper_PromoSingleCode();
		for ($i=0; $i<$count; $i++) {
			$valid = false;
			while ($valid === false) {
				$code = $mapper->generateCode();
				if ($mapper->doesExists(array('code'=>$code)) === false) {
					$valid = true;
				}else {
					$valid = false;
				}
			}
			$mapper->save(new Application_Model_PromoSingleCode(array('promo_id'=>$id,'code'=>$code,'used_timestamp'=>new Zend_Db_Expr("NULL"))));
			$new_list[] = $code;
		}

		echo json_encode($new_list);
		exit;
	}

	public function thresholdListAction()
	{
		$this->view->promo_id = $promo_id = $this->getRequest()->getParam('promo_id', 0);

		$mapper = new Admin_Model_Mapper_PromoThreshold();
		$this->view->thresholds = $mapper->fetchByPromoId($promo_id);
	}

	public function thresholdViewAction()
	{
		$this->view->threshold_id = $id = $this->getRequest()->getParam('id', 0);

		$mapper = new Admin_Model_Mapper_PromoThreshold();
		$this->view->threshold = $mapper->find($id);

		$mapper = new Admin_Model_Mapper_PromoThresholdProduct();
		$products = $mapper->fetchProductsByPromoThresholdId($id);
		$this->view->products = ''; $prod_list = array();
		foreach ($products as $product) {
			if ($product->product_id) {
				$item_type = 'product';
				$item = array(
					'id' => $product->product_id,
					'title' => $product->product_title,
					'type' => '',
					'sku' => $product->product_sku
				);
			}else {
				$item_type = 'variant';
				$item = array(
					'id' => $product->variant_id,
					'title' => $product->variant_title,
					'type' => "[{$product->type}]",
					'sku' => $product->variant_sku
				);
			}

			$prod_list[] = '<span class="hidden">'.$item_type.'~~'.$item['id'].'</span>'.str_ireplace("'","`",$item['title']).' '.$item['type'].' '.$item['sku'];
		}
		$this->view->products = implode("|",$prod_list);

		$products = $mapper->fetchProductsByPromoThresholdId($id,'exclude');
		$this->view->ex_products = ''; $prod_list = array();
		foreach ($products as $product) {
			if ($product->product_id) {
				$item_type = 'product';
				$item = array(
					'id' => $product->product_id,
					'title' => $product->product_title,
					'type' => '',
					'sku' => $product->product_sku
				);
			}else {
				$item_type = 'variant';
				$item = array(
					'id' => $product->variant_id,
					'title' => $product->variant_title,
					'type' => "[{$product->type}]",
					'sku' => $product->variant_sku
				);
			}

			$prod_list[] = '<span class="hidden">'.$item_type.'~~'.$item['id'].'</span>'.str_ireplace("'","`",$item['title']).' '.$item['type'].' '.$item['sku'];
		}
		$this->view->ex_products = implode("|",$prod_list);

		$products = $mapper->fetchProductsByPromoThresholdId($id,'bogo');
		$this->view->bogo_products = ''; $prod_list = array();
		foreach ($products as $product) {
			if ($product->product_id) {
				$item_type = 'product';
				$item = array(
					'id' => $product->product_id,
					'title' => $product->product_title,
					'type' => '',
					'sku' => $product->product_sku
				);
			}else {
				$item_type = 'variant';
				$item = array(
					'id' => $product->variant_id,
					'title' => $product->variant_title,
					'type' => "[{$product->type}]",
					'sku' => $product->variant_sku
				);
			}

			$prod_list[] = '<span class="hidden">'.$item_type.'~~'.$item['id'].'</span>'.str_ireplace("'","`",$item['title']).' '.$item['type'].' '.$item['sku'];
		}
		$this->view->bogo_products = implode("|",$prod_list);
	}

	public function thresholdAddAction()
	{
		$this->view->promo_id = $promo_id = $this->getRequest()->getParam('promo_id', 0);
		if(!empty($promo_id) && is_numeric($promo_id)){
			$this->view->products = '';

			$this->view->cmd = 'add';

			$this->render('threshold-form');
		}
	}

	public function thresholdEditAction()
	{
		$this->view->promo_id = $promo_id = $this->getRequest()->getParam('promo_id', 0);
		$this->view->id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_PromoThreshold();
			$this->view->threshold = $entity = $mapper->find($id);

			$mapper = new Admin_Model_Mapper_PromoThresholdProduct();
			$products = $mapper->fetchProductsByPromoThresholdId($id);
			$this->view->products = ''; $prod_list = array();
			foreach ($products as $product) {
				if ($product->product_id) {
					$item_type = 'product';
					$item = array(
						'id' => $product->product_id,
						'title' => $product->product_title,
						'type' => '',
						'sku' => $product->product_sku
					);
				}else {
					$item_type = 'variant';
					$item = array(
						'id' => $product->variant_id,
						'title' => $product->variant_title,
						'type' => "[{$product->type}]",
						'sku' => $product->variant_sku
					);
				}

				$prod_list[] = '<span class="hidden">'.$item_type.'~~'.$item['id'].'</span>'.str_ireplace("'","`",$item['title']).' '.$item['type'].' '.$item['sku'];
			}
			$this->view->products = implode("|",$prod_list);

			$products = $mapper->fetchProductsByPromoThresholdId($id,'exclude');
			$this->view->ex_products = ''; $prod_list = array();
			foreach ($products as $product) {
				if ($product->product_id) {
					$item_type = 'product';
					$item = array(
						'id' => $product->product_id,
						'title' => $product->product_title,
						'type' => '',
						'sku' => $product->product_sku
					);
				}else {
					$item_type = 'variant';
					$item = array(
						'id' => $product->variant_id,
						'title' => $product->variant_title,
						'type' => "[{$product->type}]",
						'sku' => $product->variant_sku
					);
				}

				$prod_list[] = '<span class="hidden">'.$item_type.'~~'.$item['id'].'</span>'.str_ireplace("'","`",$item['title']).' '.$item['type'].' '.$item['sku'];
			}
			$this->view->ex_products = implode("|",$prod_list);

			$products = $mapper->fetchProductsByPromoThresholdId($id,'bogo');
			$this->view->bogo_products = ''; $prod_list = array();
			foreach ($products as $product) {
				if ($product->product_id) {
					$item_type = 'product';
					$item = array(
						'id' => $product->product_id,
						'title' => $product->product_title,
						'type' => '',
						'sku' => $product->product_sku
					);
				}else {
					$item_type = 'variant';
					$item = array(
						'id' => $product->variant_id,
						'title' => $product->variant_title,
						'type' => "[{$product->type}]",
						'sku' => $product->variant_sku
					);
				}

				$prod_list[] = '<span class="hidden">'.$item_type.'~~'.$item['id'].'</span>'.str_ireplace("'","`",$item['title']).' '.$item['type'].' '.$item['sku'];
			}
			$this->view->bogo_products = implode("|",$prod_list);

			$this->view->cmd = 'edit';

			$this->render('threshold-form');
		}
	}

	public function thresholdSaveAction()
	{
		if( $this->getRequest()->isPost() ) {
			$data = $this->getRequest()->getParam('data', array());

			$email_coupon_code_expiration_utc= null;
			if ($data['email_coupon_code_expiration']) {
				$tz_from = 'UTC'; //TODO - leaving this here so we can set site timezone as a setting in the future
				$tz_to = 'UTC';
				$format = 'Y-m-d H:i:s';

				$datetime = $data['email_coupon_code_expiration'] . ' 23:59:59';
				$dt = new DateTime($datetime, new DateTimeZone($tz_from));
				$dt->setTimeZone(new DateTimeZone($tz_to));
				$email_coupon_code_expiration_utc = $dt->format($format);
			}

			//section
			$mapper = new Admin_Model_Mapper_PromoThreshold();
			$clean = array(
				'id' => $data['id'],
				'promo_id' => $data['promo_id'],
				'threshold' => $data['threshold'],
				'amount' => $data['amount'],
				'type' => $data['type'],
				'shipping_amount' => $data['shipping_amount'],
				'shipping_type' => $data['shipping_type'],
				'shipping_restriction' => $data['shipping_restriction'],
				'shipping_qty_threshold' => $data['shipping_qty_threshold'],
				'shipping_amount_over_threshold' => $data['shipping_amount_over_threshold'],
				'sitewide' => $data['sitewide'],
				'email_coupon_code_count' => $data['email_coupon_code_count'],
				'email_coupon_code_amount' => $data['email_coupon_code_amount'],
				'email_coupon_code_expiration' => $data['email_coupon_code_expiration'],
				'email_coupon_code_expiration_utc' => $email_coupon_code_expiration_utc
			);
			$threshold_id = $mapper->save(new Application_Model_PromoThreshold($clean));

			//products/items
			$prod_mapper = new Admin_Model_Mapper_PromoThresholdProduct();
			$prod_mapper->deleteByPromoThresholdId($threshold_id);

			$products = explode("|",$data['products']);
			if ($products) {
				foreach ($products as $k => $product_string) {
					$pattern = "/<span ?.*>(.*)<\/span>/";
					preg_match($pattern, $product_string, $matches);
					if (count($matches) > 1) {
						$product_id = $variant_id = '';
						$split = explode("~~", $matches[1]);
						if ($split[0] == 'product') {
							$product_id = $split[1];
						}elseif ($split[0] == 'variant') {
							$variant_id = $split[1];
						}

						if ($product_id || $variant_id) {
							$prod = new Application_Model_PromoThresholdProduct(
								array('promo_threshold_id' => $threshold_id, 'product_id' => $product_id, 'variant_id' => $variant_id, 'type' => 'include')
							);
							$prod_mapper->save($prod);
						}
					}
				}
			}

			$products = explode("|",$data['ex_products']);
			if ($products) {
				foreach ($products as $k => $product_string) {
					$pattern = "/<span ?.*>(.*)<\/span>/";
					preg_match($pattern, $product_string, $matches);
					if (count($matches) > 1) {
						$product_id = $variant_id = '';
						$split = explode("~~", $matches[1]);
						if ($split[0] == 'product') {
							$product_id = $split[1];
						}elseif ($split[0] == 'variant') {
							$variant_id = $split[1];
						}

						if ($product_id || $variant_id) {
							$prod = new Application_Model_PromoThresholdProduct(
								array('promo_threshold_id' => $threshold_id, 'product_id' => $product_id, 'variant_id' => $variant_id, 'type' => 'exclude')
							);
							$prod_mapper->save($prod);
						}
					}
				}
			}

			$products = explode("|",$data['bogo_products']);
			if ($products) {
				foreach ($products as $k => $product_string) {
					$pattern = "/<span ?.*>(.*)<\/span>/";
					preg_match($pattern, $product_string, $matches);
					$product_id = $variant_id = '';
					$split = explode("~~", $matches[1]);
					if ($split[0] == 'product') {
						$product_id = $split[1];
					}elseif ($split[0] == 'variant') {
						$variant_id = $split[1];
					}

					if ($product_id || $variant_id) {
						$prod = new Application_Model_PromoThresholdProduct(
							array('promo_threshold_id' => $threshold_id, 'product_id' => $product_id, 'variant_id' => $variant_id, 'type'=>'bogo')
						);
						$prod_mapper->save($prod);
					}
				}
			}

			/*
			//banner
			if ($_FILES['banner_src']['name']) {
				$image_path =  "/media/promos/".$data['promo_id']."/".$_FILES['banner_src']['name'];

				$s3 = new Zend_Service_Amazon_S3($this->_s3['key'], $this->_s3['secret']);
				$s3->putObject($this->_s3['container'] . $image_path, file_get_contents($_FILES['banner_src']['tmp_name']),
					array(Zend_Service_Amazon_S3::S3_ACL_HEADER =>
						Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ));

				$clean['banner'] = $this->_s3['url'] . $image_path;

				$mapper->save(new Application_Model_PromoThreshold($clean));
			}
			*/
		}

		$this->redirect("/admin/promotion/threshold-list/promo_id/".$data['promo_id']);
	}

	public function thresholdDeleteAction()
	{
		$promo_id = $this->getRequest()->getParam('promo_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_PromoThreshold();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->redirect("/admin/promotion/threshold-list/promo_id/".$promo_id);
	}

	public function init()
	{
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->view->layout()->disableLayout();
        }
		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'promotion', 'view');
		if($acl_status){
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}else{
			$auth->clearIdentity();
			$this->getResponse()->setRedirect('/admin/auth');
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled'
		);
		$this->view->types = $this->_types = array(
			'fixed' => 'Fixed $ Amount Off Sale Price',
			'percent' => 'Percent Off Sale Price',
			'percent_msrp' => 'Percent Off SRP',
			'price' => 'Set Price',
			'fixed_order' => 'Fixed $ Amount Off Entire Order',
			//'bogo' => 'BOGO'
		);

		$this->view->shipping_types = $this->_shipping_types = array(
			'' => 'No Shipping Discount',
			'fixed' => 'Fixed $ Amount Off',
			'price' => 'Set Price'
		);
		$this->view->triggers = $this->_triggers = array(
			'code' => 'Must Enter a Code',
			'auto' => 'Automatically Applied',
			'single_use_code' => 'Single Use Coupon Codes'
		);
		$this->view->shipping_restrictions = $this->_shipping_restrictions = array(
			'' => 'None',
			'US' => 'US Only',
			'CA' => 'CA Only'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');

	}
}
