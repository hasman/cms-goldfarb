<?php
class Admin_LicenseController extends Zend_Controller_Action
{
	protected $_db;

	function init()
	{
		$this->view->placeholder('section')->set("home");

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}else{
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();
		$this->view->sitename = $options['site']['name'];

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function indexAction()
	{
		$this->view->licenses = array();
		$license_agreed = array();

		$mapper = new Admin_Model_Mapper_AdminUserLicense();
		if (isset($this->view->user)) {
			$agreed = $mapper->fetchAllByUserId($this->view->user->id);

			foreach ($agreed as $row) {
				$license_agreed[$row->type] = $row->version;
			}
		}

		$license_config = null;
		$license_path = realpath(APPLICATION_PATH . '/../public/license');
		if (file_exists($license_path.'/config.json')) {
			$license_config = json_decode(file_get_contents($license_path.'/config.json'), TRUE);
		}
		if ($license_config) {
			foreach ($license_config as $license_type => $license) {
				$license = current($license);

				if (isset($license_agreed[$license_type]) && $license_agreed[$license_type] == $license['version']) {
					continue;
				}

				$this->view->licenses[] = array(
					'type' => $license_type,
					'content' => file_get_contents($license_path.'/'.$license['filename']),
					'version' => $license['version'],
					'title' => $license['title']
				);
			}
		}
	}

	public function processAction()
	{
		$licenses = $this->getRequest()->getParam("license",array());
		$versions = $this->getRequest()->getParam("version",array());

		$mapper = new Admin_Model_Mapper_AdminUserLicense();
		foreach ($licenses as $type => $value) {
			if ($value == 'agree') {
				$mapper->save(new Admin_Model_AdminUserLicense(array(
					'admin_user_id' => $this->view->user->id,
					'type' => $type,
					'version' => $versions[$type],
					'agree_timestamp' => new Zend_Db_Expr("NOW()")
				)));
			}
		}

		$redirectUrl = Cny_Auth::fetchPreviousRequestUrl();
		$url_redirect = ($redirectUrl) ? $redirectUrl : '/admin/';
		$this->getResponse()->setRedirect($url_redirect);
	}

}
