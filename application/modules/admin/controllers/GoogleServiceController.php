<?php
class Admin_GoogleServiceController extends Zend_Controller_Action
{
	public function authAction()
	{
		Zend_Debug::dump($_SERVER);die;
		$client = new Google_Client();
		$client->setAuthConfig(dirname(__FILE__).'/../application/data/google_service_account.json');
		$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
		$redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$client->setRedirectUri($redirect_uri);

		if (isset($_GET['code'])) {
			$token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
		}

	}

	function init()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}else{
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		}

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();
		$this->view->sitename = $options['site']['name'];

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}
}
