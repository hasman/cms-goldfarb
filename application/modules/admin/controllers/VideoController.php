<?php
use Cocur\Slugify\Slugify;

class Admin_VideoController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$mapper = new Admin_Model_Mapper_VideoCategory();
		$this->view->sections = $mapper->fetchOrdered();
	}

	public function categorySortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_VideoCategory();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function itemSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_Video();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function categoryListAction()
	{
		$this->view->placeholder('page_title')->set("Video Categories");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video">Video Library</a></li><li class="active">Video Categories</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','sort_order');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_VideoCategory();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'video_categories';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function categoryAddAction()
	{
		$this->view->placeholder('page_title')->set("Video Categories");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video">Video Library</a></li>
				<li class="active">Add Video Category</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_VideoCategory();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('category-form');
	}

	public function categoryEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Video Categories");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video">Video Library</a></li>
				<li class="active">Edit Video Category: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_VideoCategory();
			$mapper = new Admin_Model_Mapper_VideoCategory();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('category-form');
		}else{
			$this->getResponse()->setRedirect('/admin/video/category-list');
		}
	}

	public function categoryViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoCategory();
			$this->view->section = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Video Categories");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video">Video Library</a></li>
				<li class="active">View Video Category: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			//videos in section
			$mapper = new Admin_Model_Mapper_Video();
			$this->view->items = $mapper->fetchByCategoryId($id);
		}else{
			$this->getResponse()->setRedirect('/admin/video/category-list');
		}
	}

	public function categorySaveAction()
	{
		$form = new Admin_Form_VideoCategory();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_VideoCategory();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['name']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($this->getRequest()->getParams())) {
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}

			$entity = new Application_Model_VideoCategory($data);
			$id = $mapper->save($entity);

			$this->getResponse()->setRedirect('/admin/video/category-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('category-form');
		}
	}

	public function categoryDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoCategory();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/video/category-list');
	}

	public function itemListAction()
	{
		$this->view->placeholder('page_title')->set("Videos");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video">Video Library</a></li><li class="active">Videos</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','sort_order');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');
		$this->view->video_category_id = $video_category_id = $this->_getParam('video_category_id','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha,
			'video_category_id' => $video_category_id
		);
		$mapper = new Admin_Model_Mapper_Video();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'videos';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function itemAddAction()
	{
		$this->view->placeholder('page_title')->set("Videos");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video">Video Library</a></li>
				<li class="active">Add Video</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_Video();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('item-form');
	}

	public function itemEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Videos");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video">Video Library</a></li>
				<li class="active">Edit Video: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_Video();
			$mapper = new Admin_Model_Mapper_Video();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			//tags
			$t_mapper = new Admin_Model_Mapper_VideoTags();
			$data['tags'] = implode("|",$t_mapper->fetchTagNamesByVideoId($id));

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('item-form');
		}else{
			$this->getResponse()->setRedirect('/admin/video/item-list');
		}
	}

	public function itemViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Video();
			$this->view->item = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Videos");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video">Video Library</a></li>
				<li class="active">View Video: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);
		}else{
			$this->getResponse()->setRedirect('/admin/video/item-list');
		}
	}

	public function itemSaveAction()
	{
		$form = new Admin_Form_Video();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_Video();

		if( $form->isValid($this->getRequest()->getParams())) {
			if ($data['posted_date'] == '0000-00-00' || $data['posted_date'] == '1969-12-31' || $data['posted_date'] == '') $data['posted_date'] = "today";
			if ($data['posted_date']) $data['posted_date'] = date("Y-m-d",strtotime($data['posted_date']));

			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}

			$entity = new Application_Model_Video($data);
			$id = $mapper->save($entity);

			//tag groups
			$t_mapper = new Admin_Model_Mapper_VideoTags();
			$tc_mapper = new Admin_Model_Mapper_VideoTag();

			$tc_mapper->deleteByVideoId($id);

			$tags = explode("|",$data['tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Application_Model_VideoTags(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'Enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Application_Model_VideoTag(array('video_id' => $id, 'video_tag_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			$this->getResponse()->setRedirect('/admin/video/item-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('item-form');
		}
	}

	public function itemDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Video();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/video/item-list');
	}

	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'videos', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled',
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');

	}
}
