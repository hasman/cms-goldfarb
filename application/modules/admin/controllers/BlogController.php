<?php
use Cocur\Slugify\Slugify;

class Admin_BlogController extends Zend_Controller_Action
{

	function indexAction()
	{
		$this->_forward('list');
	}

	public function listAction()
	{
		$this->view->placeholder('section')->set("detailview blog-list-view");
		$this->view->placeholder('page_title')->set("Blog Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li class="active">Blog Manager</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','posted_date');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->name = $name = $this->_getParam('name','');
		$this->view->status = $status = $this->_getParam('status','');

		$search_keys = array(
			'keyword' => $mask,
			'title' => $name,
			'status' => $status
		);
		$mapper = new Admin_Model_Mapper_BlogPost();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function addAction()
	{
		$this->view->placeholder('page_title')->set("Blog Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/blog/list">Blog Manager</a></li><li class="active">Add Post</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_BlogPostForm();
		$form->getElement('status')->setValue('Disabled');
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('form');
	}

	public function editAction()
	{
		$this->view->placeholder('section')->set("blog-editor");

		$this->view->placeholder('page_title')->set("Blog Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/blog/list">Blog Manager</a></li><li class="active">Edit Post</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->view->id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_BlogPost();
			$this->view->page = $entity = $mapper->find($id);

			//category tags
			$t_mapper = new Admin_Model_Mapper_BlogTag();
			$data['tags'] = implode("|",$t_mapper->fetchTagNamesByBlogId($id));

			$this->render('editor');
		}else{
			$this->getResponse()->setRedirect('/admin/blog/list');
		}
	}

	public function saveAction()
	{
		$this->view->placeholder('page_title')->set("Blog Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/blog/list">Blog Manager</a></li><li class="active">Edit Post</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_BlogPostForm();
		$mapper = new Admin_Model_Mapper_BlogPost();

		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code'=>$code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($data) ){
			if ($data['posted_date'] == '0000-00-00' || $data['posted_date'] == '1969-12-31' || $data['posted_date'] == '') $data['posted_date'] = "today";
			if ($data['posted_date']) $data['posted_date'] = date("Y-m-d",strtotime($data['posted_date']));

			$entity = new Application_Model_BlogPost($data);
			$data['id'] = $id = $mapper->save($entity);

			$entity = new Application_Model_BlogPost($data);
			$mapper->save($entity);

			//tag groups
			$t_mapper = new Admin_Model_Mapper_BlogTag();
			$tc_mapper = new Admin_Model_Mapper_BlogPostTag();

			$tc_mapper->deleteByBlogId($id);

			$tags = explode("|",$data['tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Application_Model_BlogTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Application_Model_BlogPostTag(array('blog_post_id' => $id, 'blog_tag_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			//clear blog feed cache
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('blog_feed')
			);
			//need to find every page that has a blog with not URL (internal blog system) and clear the page caches too
			//TODO - better limit this to just internal blog system pages instead of all with blog_feed module
			$sql = $this->_db->quoteInto("SELECT ps.page_id
				FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
				LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
				LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
				LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
				WHERE psrcm.type = ?", 'blog');
			$pages = $this->_db->fetchAll($sql);
			foreach ($pages as $page) {
				$cache_id = 'page_' . $page['page_id'];
				$cache->clean(
					Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
					array('page_' . $page['page_id'])
				);
				$cache->remove($cache_id);
			}

			if ($data['cmd'] == 'add') {
				$this->getResponse()->setRedirect('/admin/blog/edit/id/'.$id);
			}else {
				$this->getResponse()->setRedirect('/admin/blog/list');
			}
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('form');
		}
	}

	public function copyAction()
	{
		$id = $this->getRequest()->getParam('id', 0); //id of post to clone
		$auto_enable = $this->getRequest()->getParam('ae','no');

		$post_mapper = new Admin_Model_Mapper_BlogPost();
		$tc_mapper = new Admin_Model_Mapper_BlogPostTag();

		//get and clone page
		$post = $post_mapper->find($id);
		$post->id = null;
		$post->title = $post->title . " COPY";
		$post->status = ($auto_enable == 'yes')?'Enabled':'Disabled';

		$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
		$code = $orig_code = $slugify->slugify($post->title);
		$counter = 1;
		while ($post_mapper->doesExists(array('code' => $code)) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$post->code = $code;

		$new_post_id = $post_mapper->save($post);

		//get and clone tags
		$tags = $tc_mapper->fetchAllByBlogId($id);
		if ($tags) {
			foreach ($tags as $tag) {
				$tc_mapper->save(new Application_Model_BlogPostTag([
					'blog_post_id' => $new_post_id,
					'blog_tag_id' => $tag['blog_tag_id'],
					'sort_order' => $tag['sort_order'],
				]));
			}
		}

		$this->redirect("/admin/blog/edit/id/" . $new_post_id);
	}

	public function seoEditAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);

		$mapper = new Admin_Model_Mapper_BlogPost();
		$this->view->page = $mapper->find($page_id);
	}

	public function tagEditAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);

		$mapper = new Admin_Model_Mapper_BlogPost();
		$this->view->page = $mapper->find($page_id);

		//category tags
		$t_mapper = new Admin_Model_Mapper_BlogTag();
		$this->view->tags = implode("|",$t_mapper->fetchTagNamesByBlogId($page_id));
	}

	public function imageEditAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);

		$mapper = new Admin_Model_Mapper_BlogPost();
		$this->view->page = $mapper->find($page_id);
	}

	public function savePartialAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);
		$data = ($this->getRequest()->getParam('data', array()));

		$mapper = new Admin_Model_Mapper_BlogPost();
		$data['id'] = $page_id;

		if (isset($data['code']) && $data['code']) {
			$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
			$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
			$counter = 1;
			while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
				$code = $orig_code . "-" . $counter;
				$counter++;
			}
			$data['code'] = $code;
		}

		//clear blog feed cache
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('blog_feed')
		);
		//need to find every page that has a blog with not URL (internal blog system) and clear the page caches too
		//TODO - better limit this to just internal blog system pages instead of all with blog_feed module
		$sql = $this->_db->quoteInto("SELECT ps.page_id
				FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
				LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
				LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
				LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
				WHERE psrcm.type = ?", 'blog');
		$pages = $this->_db->fetchAll($sql);
		foreach ($pages as $page) {
			$cache_id = 'page_' . $page['page_id'];
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page['page_id'])
			);
			$cache->remove($cache_id);
		}

		$entity = $mapper->find($data['id']);

		$entity_data = array_merge($entity->toArray(), $data);
		$mapper->save(new Application_Model_BlogPost($entity_data));

		if ($this->getRequest()->isXmlHttpRequest()) {
			echo json_encode($data);
			exit;
		}else {
			$this->getResponse()->setRedirect('/admin/blog/list');
		}
	}

	public function saveTagsAction()
	{
		$page_id = $this->getRequest()->getParam('page_id', 0);
		$data = ($this->getRequest()->getParam('data', array()));

		$mapper = new Admin_Model_Mapper_BlogPost();
		$data['id'] = $page_id;

		//tag groups
		$t_mapper = new Admin_Model_Mapper_BlogTag();
		$tc_mapper = new Admin_Model_Mapper_BlogPostTag();

		$tc_mapper->deleteByBlogId($page_id);

		$tags = explode("|",$data['tags']);
		$sort = 0;
		foreach ($tags as $k=>$v) {
			if ($v && trim($v) != '' && $v != 'NULL') {
				$entity = $t_mapper->doesTagExist($v);
				if (!$entity) {
					$tag_id = $t_mapper->save(new Application_Model_BlogTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'enabled')));
				} else {
					$tag_id = $entity->id;
				}

				$tc_mapper->save(new Application_Model_BlogPostTag(array('blog_post_id' => $page_id, 'blog_tag_id' => $tag_id, 'sort_order' => $sort)));

				$sort++;
			}
		}

		//clear blog feed cache
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('blog_feed')
		);
		//need to find every page that has a blog with not URL (internal blog system) and clear the page caches too
		//TODO - better limit this to just internal blog system pages instead of all with blog_feed module
		$sql = $this->_db->quoteInto("SELECT ps.page_id
				FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
				LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
				LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
				LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
				WHERE psrcm.type = ?", 'blog');
		$pages = $this->_db->fetchAll($sql);
		foreach ($pages as $page) {
			$cache_id = 'page_' . $page['page_id'];
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('page_' . $page['page_id'])
			);
			$cache->remove($cache_id);
		}

		echo json_encode($data);exit;
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_BlogPost();
			$entity = $mapper->find($id);
			$mapper->delete($entity);

			//clear blog feed cache
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('blog_feed')
			);
			//need to find every page that has a blog with not URL (internal blog system) and clear the page caches too
			//TODO - better limit this to just internal blog system pages instead of all with blog_feed module
			$sql = $this->_db->quoteInto("SELECT ps.page_id
				FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
				LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
				LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
				LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
				WHERE psrcm.type = ?", 'blog');
			$pages = $this->_db->fetchAll($sql);
			foreach ($pages as $page) {
				$cache_id = 'page_' . $page['page_id'];
				$cache->clean(
					Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
					array('page_' . $page['page_id'])
				);
				$cache->remove($cache_id);
			}
		}

		$this->getResponse()->setRedirect('/admin/blog/list');
	}

	public function viewAction()
	{
		$this->view->placeholder('page_title')->set("Blog Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/blog/list">Blog Manager</a></li><li class="active">View Post</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_BlogPost();
			$entity = $mapper->find($id);
			$this->view->article = $entity->toArray();
		}else{
			$this->getResponse()->setRedirect('/admin/blog/list');
		}
	}

	public function slugTitleAction()
	{
		$page_id = $this->getRequest()->getParam('page_id',0);
		$title = $this->getRequest()->getParam('title','');

		$mapper = new Admin_Model_Mapper_BlogPost();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify($title);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $page_id) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}

		echo $code;
		exit;
	}

	public function exportAction()
	{
		$data = [];

		$id = $this->getRequest()->getParam('id', 0); //id of post to export
		$include_media = $this->getRequest()->getParam('include_media', 'No');

		$post_mapper = new Admin_Model_Mapper_BlogPost();
		$tc_mapper = new Admin_Model_Mapper_BlogPostTag();

		//get post
		$post = $post_mapper->find($id);

		$data['post']['data'] = $post->toArray();

		//get tags
		$tags = $tc_mapper->fetchAllByBlogId($id);
		if ($tags) {
			foreach ($tags as $tag) {
				$data['tags'][] = [
					'code' => $tag['code'],
					'title' => $tag['title'],
					'sort_order' => $tag['sort_order'],
					'status' => $tag['status']
				];
			}
		}

		$media = [];
		if ($include_media == 'Yes') {
			//TODO - need to fix the regex so the double quote at the end of some matches is stripped automatically
			//I think there are still some other errors in the regex
			//ideally I just want it to find any files in /userFiles/uploads/ but those paths may also be escaped with slashes.

			//disable media for now until I can fix up the zip portion
			$exp = "#(\/.*?\/)((?:[^\/]|\\\\/)+?)(?:(?<!\\\)\s|$|\")#";
			preg_match_all($exp, serialize($data), $media, PREG_SET_ORDER);
		}

		$encoded = openssl_encrypt(serialize($data),'aes-128-ctr','LOGIC', null, 'cyberny.comLOGIC');

		$slugify = new Slugify();
		$slug = $slugify->slugify($post->code);
		$filename = $slug.'.clz';
		$file = sys_get_temp_dir().'/'.$filename;

		$zip = new ZipArchive;
		$res = $zip->open($file, ZipArchive::CREATE);
		if ($res === TRUE) {
			$zip->addFromString($slug.'.clb', $encoded);

			foreach ($media as $match) {
				$path = str_ireplace(array("\/",'"'),array("/",""),$match[0]);
				if (stripos($path, '/userFiles/uploads') !== false) {
					$zip->addFile($_SERVER['DOCUMENT_ROOT'].$path, substr($path,1));

					//thumbnail file
					$thumb_path = str_replace("userFiles/uploads", "userThumbs", $path);

					$zip->addFile($_SERVER['DOCUMENT_ROOT'].$thumb_path, substr($thumb_path,1));
				}
			}

			$zip->close();
		} else {
			echo 'export failed';
			exit;
		}

		header('Content-Type: application/octet-stream');
		header('Content-disposition: attachment; filename='.$filename);
		header('Content-Length: ' . filesize($file));
		readfile($file);

		unlink($file);

		exit;
	}

	public function importAction()
	{
		$post_mapper = new Admin_Model_Mapper_BlogPost();
		$t_mapper = new Admin_Model_Mapper_BlogTag();
		$tc_mapper = new Admin_Model_Mapper_BlogPostTag();

		$post_id = $this->getRequest()->getParam('id');

		if ($this->getRequest()->getParam('type') == 'new') {
			$data = [];
			$data['title'] = 'Importer Temp';
			$data['code'] = 'importer-temp-'.rand();
			$data['status'] = 'Enabled';

			$entity = new Application_Model_BlogPost($data);
			$post_id = $post_mapper->save($entity);
		}

		if ($post_id && is_numeric($post_id) && $this->getRequest()->isPost()) {
			$zip = new ZipArchive;
			$res = $zip->open($_FILES['archive']['tmp_name']);
			if ($res === TRUE) {
				$info = pathinfo($_FILES['archive']['name']);
				$dir = sys_get_temp_dir().'/'.$info['filename'];
				mkdir($dir);

				$zip->extractTo($dir);
				$zip->close();

				//copy media files if they exist
				if (is_dir($dir.'/userFiles')) {
					$dest = realpath(APPLICATION_PATH . '/../public/userFiles');
					foreach ($iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir . '/userFiles', \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST) as $item) {
						if ($item->isDir()) {
							mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
						} else {
							copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
						}
					}
				}
				//thumbnails
				if (is_dir($dir.'/userThumbs')) {
					$dest = realpath(APPLICATION_PATH . '/../public/userThumbs');
					foreach ($iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir . '/userThumbs', \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST) as $item) {
						if ($item->isDir()) {
							mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
						} else {
							copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
						}
					}
				}

				//read data file
				$data_array = unserialize(openssl_decrypt(file_get_contents($dir.'/'.$info['filename'].'.clb'),'aes-128-ctr','LOGIC', null, 'cyberny.comLOGIC'));

				if ($data_array) {
					//create/update post
					$post = $data_array['post']['data'];

					$slugify = new Slugify(['regexp' => '/([^A-Za-z0-9\/]|-)+/']);
					$code = $orig_code = $slugify->slugify(($post['code']) ? $post['code'] : $post['title']);
					$counter = 1;
					while ($post_mapper->doesExists(array('code' => $code), $post_id) !== false) {
						$code = $orig_code . "-" . $counter;
						$counter++;
					}
					$post['code'] = $code;
					$post['id'] = $post_id;
					$post['modified'] = new Zend_Db_Expr("NOW()");
					$post_mapper->save(new Application_Model_BlogPost($post));

					//remove old post tags
					$tc_mapper->deleteByBlogId($post_id);

					//create post tags
					foreach ($data_array['tags'] as $k => $tag) {
						if ($tag['title'] && trim($tag['title']) != '' && $tag['title'] != 'NULL') {
							$entity = $t_mapper->doesTagExist($tag['title']);
							if (!$entity) {
								$tag_id = $t_mapper->save(new Application_Model_BlogTag(array('code' => $tag['code'], 'title' => $tag['title'], 'status' => $tag['status'])));
							} else {
								$tag_id = $entity->id;
							}

							$tc_mapper->save(new Application_Model_BlogPostTag(array('blog_post_id' => $post_id, 'blog_tag_id' => $tag_id, 'sort_order' => $tag['sort_order'])));
						}
					}

					//clear blog feed cache
					$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
					$cache->clean(
						Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
						array('blog_feed')
					);
					//need to find every page that has a blog with not URL (internal blog system) and clear the page caches too
					//TODO - better limit this to just internal blog system pages instead of all with blog_feed module
					$sql = $this->_db->quoteInto("SELECT ps.page_id
						FROM {$this->_table_prefix}page_section_row_column_modules AS psrcm
						LEFT JOIN {$this->_table_prefix}page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
						LEFT JOIN {$this->_table_prefix}page_section_rows AS psr ON psr.id = psrc.page_section_row_id
						LEFT JOIN {$this->_table_prefix}page_sections AS ps ON psr.page_section_id = ps.id
						WHERE psrcm.type = ?", 'blog');
					$pages = $this->_db->fetchAll($sql);
					foreach ($pages as $page) {
						$cache_id = 'page_' . $page['page_id'];
						$cache->clean(
							Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
							array('page_' . $page['page_id'])
						);
						$cache->remove($cache_id);
					}

					$this->_flashMessenger->setNamespace('info')->addMessage("Post import successful");
				}else {
					$this->_flashMessenger->setNamespace('error')->addMessage("There was a problem with the import. File incorrect or corrupted.");
				}

				//clean up files
				if (is_dir($dir)) {
					$objects = scandir($dir);
					foreach ($objects as $object) {
						if ($object != "." && $object != "..") {
							if (filetype($dir . "/" . $object) == "dir")
								rmdir($dir . "/" . $object);
							else unlink($dir . "/" . $object);
						}
					}
					reset($objects);
					rmdir($dir);
				}
			} else {
				$this->_flashMessenger->setNamespace('error')->addMessage("There was a problem with the import: $res");
			}


			$this->redirect("/admin/blog/edit/id/$post_id");
		}else {
			$this->redirect("/admin");
		}
	}

	public function addImportAction()
	{
		//
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'blog', $this->getRequest()->getActionName());
		if($acl_status){
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		}else{
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap');

		$options = $bootstrap->getOptions();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array('automatic_serialization' => true, 'lifetime' => 0);
		$this->backendOptions = array('cache_dir' => $cache_dir);

		$this->view->statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled'
		);

		$this->view->posted_date_formats = array(
			'F j, Y' => date('F j, Y'),
			'M. j, Y' => date('M. j, Y'),
			'F Y' => date('F Y'),
			'M. Y' => date('M. Y'),
			'j F Y' => date('j F Y'),
			'j M. Y' => date('j M. Y'),
			'Y' => date('Y'),
			'none' => "Don't Show Date"
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
