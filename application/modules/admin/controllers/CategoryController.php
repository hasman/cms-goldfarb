<?php
use Cocur\Slugify\Slugify;

class Admin_CategoryController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/category/list');
	}

	public function listAction()
	{
		$this->view->placeholder('page_title')->set("Categories");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Categories</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		/*
		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','name');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->parent_id = $parent_id = $this->_getParam('parent_id','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'parent_id' => $parent_id,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_Category();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'categories';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
		*/
		$mapper = new Admin_Model_Mapper_Category();
		$this->view->list = $mapper->nestedArray();

		ksort($this->view->list);
	}

	public function sortAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$mapper = new Admin_Model_Mapper_Category();
		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach ($values as $k => $id) {
			if (!empty($id)) {
				$mapper->setSortOrder($id, $sort);
				$sort++;
			}
		}
	}

	public function addAction()
	{
		$this->view->placeholder('page_title')->set("Categories");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/category">Categories</a></li>
				<li class="active">Add Category</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_Category();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('form');
	}

	public function editAction()
	{
		$this->view->category_id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Categories");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/category">Categories</a></li>
				<li class="active">Edit Category: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_Category();
			$mapper = new Admin_Model_Mapper_Category();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			//override parent_id options so you can't have yourself as the parent
			$mapper = new Admin_Model_Mapper_Category();
			$parent_categories = $mapper->fetchIdPairs(true);
			unset($parent_categories[$id]);
			$form->getElement('parent_id')->setMultiOptions(array(0=>'Top Level')+$parent_categories);

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('form');
		}else{
			$this->getResponse()->setRedirect('/admin/category/list');
		}
	}

	public function viewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Category();
			$this->view->category = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Categories");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/category">Categories</a></li>
				<li class="active">View Category: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			//products in category
			$mapper = new Admin_Model_Mapper_ProductCategory();
			$this->view->products = $mapper->fetchProductsByCategoryId($id);
		}else{
			$this->getResponse()->setRedirect('/admin/category/');
		}
	}

	public function saveAction()
	{
		$form = new Admin_Form_Category();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_Category();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['name']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($this->getRequest()->getParams())) {
			if (!$data['sort_order']) $data['sort_order'] = 9999;
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}

			$entity = new Application_Model_Category($data);
			$id = $mapper->save($entity);

			$nav_mapper = new Admin_Model_Mapper_NavigationItem();
			$nav_entity = $nav_mapper->findByCategoryId($id);

			if ($data['nav_display'] == 'Enabled') {
				if (!$nav_entity) {
					$nav_entity = new Application_Model_NavigationItem(array(
						'title' => $data['name'],
						'url' => "/category/" . $data['code'],
						'category_id' => $id,
						'main_nav_status' => 'Enabled',
						'status' => 'Disabled',
					));
				}
				$nav_mapper->save($nav_entity);
			}elseif ($nav_entity) {
				$nav_mapper->delete($nav_entity);
			}

			//delete cache
			$cache_id = 'navigation';
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('navigation')
			);
			$cache->remove($cache_id);

			if ($data['cmd'] == 'add') {
				//$this->getResponse()->setRedirect('/admin/category/edit/id/'.$id);
				$this->getResponse()->setRedirect('/admin/category/list');
			}else {
				$this->getResponse()->setRedirect('/admin/category/list');
			}
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$this->view->category_id = $data['id'];

			$form->populate($data);
			$this->view->form = $form;

			if ($data['cmd'] == 'add') {
				$this->render('add-form');
			}else {
				$this->render('form');
			}
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Category();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/category/list');
	}

	public function productViewAction()
	{
		$this->view->category_id = $category_id = $this->getRequest()->getParam('category_id', 0);

		$mapper = new Admin_Model_Mapper_ProductCategory();
		$this->view->products = $mapper->fetchProductsByCategoryId($category_id);

		$mapper = new Admin_Model_Mapper_ProductTags();
		$this->view->product_tags = $mapper->fetchTagNamesByCategoryId($category_id);
	}

	public function productAddAction()
	{
		$this->view->category_id = $category_id = $this->getRequest()->getParam('category_id', 0);
		if(!empty($category_id) && is_numeric($category_id)){
			$this->view->cmd = 'add';

			//$mapper = new Admin_Model_Mapper_ProductTags();
			//$this->view->product_tags = $mapper->fetchTagNamesByCategoryId($category_id);

			$this->render('product-form');
		}
	}

	public function productSaveAction()
	{
		if( $this->getRequest()->isPost() ) {
			$data = $this->getRequest()->getParam('data', array());

			$pc_mapper = new Admin_Model_Mapper_ProductCategory();

			//tags
			$t_mapper = new Admin_Model_Mapper_ProductTags();
			$tc_mapper = new Admin_Model_Mapper_ProductTag();

			//$old_tags = $t_mapper->fetchTagNamesByCategoryId($data['category_id']);

			$tags = explode("|",$data['product_tags']);
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if ($entity) {
						$tag_id = $entity->id;

						$products = $tc_mapper->fetchProductsByTagId($tag_id);

						foreach ($products as $product) {
							$prod = new Application_Model_ProductCategory(
								array('category_id' => $data['category_id'], 'product_id' => $product->id)
							);
							$pc_mapper->save($prod);
						}
					}
				}
			}

			//individual products
			$products = explode("|",$data['products']);
			if ($products) {
				foreach ($products as $k => $product_string) {
					$pattern = "/<span ?.*>(.*)<\/span>/";
					preg_match($pattern, $product_string, $matches);
					if (count($matches) > 1) {
						$product_id = $variant_id = '';
						$split = explode("~~", $matches[1]);
						if ($split[0] == 'product') {
							$product_id = $split[1];
						}elseif ($split[0] == 'variant') {
							$variant_id = $split[1];
						}

						if ($product_id || $variant_id) {
							$prod = new Application_Model_ProductCategory(
								array('category_id' => $data['category_id'], 'product_id' => $product_id)
							);
							$pc_mapper->save($prod);
						}
					}
				}
			}

			$this->getResponse()->setRedirect("/admin/category/product-view/category_id/{$data['category_id']}");
		}else{
			$this->view->cmd = 'add';
			$this->render('product-form');
		}
	}

	public function productDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$category_id = $this->getRequest()->getParam('category_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductCategory();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/category/product-view/category_id/{$category_id}");
	}

	public function productDeleteTagAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$category_id = $this->getRequest()->getParam('category_id', 0);
		$tag_id = $this->getRequest()->getParam('tag_id', 0);
		if(!empty($category_id) && is_numeric($category_id) && !empty($tag_id) && is_numeric($tag_id)){
			$mapper = new Admin_Model_Mapper_ProductCategory();
			$mapper->deleteByCategoryIdTagId($category_id, $tag_id);
		}

		$this->redirect("/admin/category/edit/id/{$category_id}");
	}

	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'ecommerce', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
