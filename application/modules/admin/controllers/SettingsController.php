<?php
use Cocur\Slugify\Slugify;

class Admin_SettingsController extends Zend_Controller_Action
{
	/** @var  Zend_Db_Adapter_Abstract */
	protected $_db;

	public function siteAction()
	{
		$this->view->placeholder('page_title')->set("Website Settings");

		$this->saveSettings("/admin/settings/site");
	}

	public function fontAction()
	{
		$this->view->placeholder('page_title')->set("Font Management");

		$mapper = new Admin_Model_Mapper_Font();
		$results = $mapper->fetchAll();

		$this->view->fonts = array();
		$db_fonts = array();
		foreach ($results as $row) {
			$this->view->fonts[$row->code] = $row->toArray();
			$db_fonts[$row->code] = $row;
		}

		//font list
		$fonts = Zend_Registry::get('cms_fonts');
		if (!is_array($fonts)) {
			$fonts = $fonts->toArray();
		}
		foreach ($fonts as $code => $row) {
			if (!array_key_exists($code,$db_fonts)) {
				$this->view->fonts[$code] = array('id'=>'', 'code'=>$code, 'title'=>$row['title'], 'type'=>$row['type'], 'url'=>$row['url'], 'status'=>'Enabled');
			}
		}
		//ignore "default" since it's not a real font
		unset($this->view->fonts['default']);

		//save data
		if( $this->getRequest()->isPost() ){
			$font_statuses = $this->getRequest()->getParam('status',array());

			foreach ($font_statuses as $code => $status) {
				if (!$code) continue;

				if (array_key_exists($code,$db_fonts)) {
					$entity = $db_fonts[$code];
				}else {
					$entity = new Application_Model_Font();
					$entity->code = $code;
					$entity->title = $this->view->fonts[$code]['title'];
					$entity->type = $this->view->fonts[$code]['type'];
					$entity->url = $this->view->fonts[$code]['url'];
				}
				$entity->status = $status;

				$mapper->save($entity);
			}

			//check if new font uploaded
			$new = $this->getRequest()->getParam("new",array());

			if ((isset($new['title']) && $new['title']) && ((isset($new['url']) && $new['url']) || $_FILES['new_file']['name'])) {
				$slugify = new Slugify();
				$code = $orig_code = $slugify->slugify($new['title']);
				$counter = 1;
				while ($mapper->doesExists(array('code' => $code)) !== false) {
					$code = $orig_code . "-" . $counter;
					$counter++;
				}

				if (isset($new['url']) && $new['url']) {
					$url = $new['url'];
				}elseif ($_FILES['new_file']['name']) {
					$base_dir = APPLICATION_PATH ."/..";
					$url = "/userFonts/".$_FILES['new_file']['name'];

					move_uploaded_file($_FILES['new_file']['tmp_name'], $base_dir."/public".$url);
				}

				$entity = new Application_Model_Font();
				$entity->code = $code;
				$entity->title = $new['title'];
				$entity->type = "custom";
				$entity->url = $url;
				$entity->status = "Enabled";

				$mapper->save($entity);
			}

			$this->_flashMessenger->setNamespace('info')->addMessage("Fonts Updated");

			$this->redirect("/admin/settings/font");
		}
	}

	public function fontDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_Font();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->_flashMessenger->setNamespace('info')->addMessage("Font Deleted");

		$this->redirect("/admin/settings/font");
	}

	public function flushCacheAction()
	{
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->clean(Zend_Cache::CLEANING_MODE_ALL);

		$this->_flashMessenger->setNamespace('info')->addMessage("Site Cache Flushed");

		$this->redirect("/admin/");
	}

	public function flushZypeCacheAction()
	{
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('zype')
		);

		$this->_flashMessenger->setNamespace('info')->addMessage("Zype Cache Flushed");

		$this->redirect("/admin/");
	}

	public function ecommerceAction()
	{
		$this->view->placeholder('page_title')->set("Store Settings");

		$this->saveSettings("/admin/settings/ecommerce");
	}

	public function seoAction()
	{
		$this->view->placeholder('page_title')->set("SEO Settings");

		$this->saveSettings("/admin/settings/seo");
	}

	public function blogAction()
	{
		$this->view->placeholder('page_title')->set("Blog Settings");

		$this->saveSettings("/admin/settings/blog");
	}

	public function blogLayoutAction()
	{
		$this->view->placeholder('page_title')->set("Blog Layout");

		$this->saveSettings("/admin/settings/blog-layout");
	}

	public function announcementAction()
	{
		$this->view->placeholder('page_title')->set("Announcements");

		$this->saveSettings("/admin/settings/announcement");
	}

	public function emailAction()
	{
		$this->view->placeholder('page_title')->set("Email");

		$this->saveSettings("/admin/settings/email");
	}

	public function faqAction()
	{
		$this->view->placeholder('page_title')->set("FAQ");

		$this->saveSettings("/admin/settings/faq");
	}

	public function videoAction()
	{
		$this->view->placeholder('page_title')->set("Video Library");

		$this->saveSettings("/admin/settings/video");
	}

	public function complianceAction()
	{
		$this->view->placeholder('page_title')->set("Compliance");

		if( $this->getRequest()->isPost() ){
			$fields = $this->getRequest()->getParam('fields',array());
			$json_fields = $this->getRequest()->getParam('json_fields',array());

			foreach ($json_fields as $code => $row) {
				foreach ($row as $k => $items) {
					$valid = false;
					foreach ($items as $item) {
						if ($item) $valid = true;
					}

					if (!$valid) unset($row[$k]);
				}

				$fields[$code] = $row;
			}

			$mapper = new Admin_Model_Mapper_SiteSettingValue();
			foreach ($fields as $code => $value) {
				if (is_array($value)) {
					$value = json_encode($value);
				}

				$entity = $mapper->findByCode($code);

				//if enabled and wasn't enabled before make API call
				if ($code == 'userway_status' && $value == 'Enabled' && $entity->content != 'Enabled') {
					$options = Zend_Registry::get('configuration')->toArray();

					if (isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
						$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
						$full_domain = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
					}

					$ch = curl_init("https://userway.org/api/widget?key=".$options['userway']['api_key']);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
						'email' => 'support@cyber-ny.com',
						'url' => $full_domain
					)));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
					));

					$result = curl_exec($ch);
					curl_close($ch);

					$json = json_decode($result);

					if ($json->code == 200) {
						$entity2 = $mapper->findByCode("userway_account_code");
						$entity2->code = "userway_account_code";
						$entity2->content = $json->data->widget->code;
						$mapper->save($entity2);
					}else {
						$this->_flashMessenger->setNamespace('error')->addMessage("There was a problem enabling UserWay. If this persists please contact Cyber-NY support");
						$value = 'Disabled';
					}
				}

				$entity->code = $code;
				$entity->content = trim($value);
				$mapper->save($entity);
			}

			//delete cache
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(Zend_Cache::CLEANING_MODE_ALL);

			$this->_flashMessenger->setNamespace('info')->addMessage("Settings Updated");

			$this->redirect("/admin/settings/compliance");
		}
	}

	public function sitemapRegenerateAction()
	{
		$sitemap = new Application_Model_Sitemap();
		$sitemap->generate();

		$this->_flashMessenger->setNamespace('info')->addMessage("Site Map Updated");

		$this->redirect("/admin/settings/seo");
	}

	public function instalinksAction()
	{
		$this->view->placeholder('page_title')->set("InstaLinks Page");

		$this->saveSettings("/admin/settings/instalinks");
	}

	public function videoPlatformLayoutAction()
	{
		$this->view->placeholder('page_title')->set("Video Platform Layout");

		$this->saveSettings("/admin/settings/video-platform-layout");
	}

	public function videoPlatformAction()
	{
		$this->view->placeholder('page_title')->set("Video Platform");

		$this->saveSettings("/admin/settings/video-platform");
	}

	private function saveSettings($redirect_url)
	{
		if( $this->getRequest()->isPost() ){
			$fields = $this->getRequest()->getParam('fields',array());
			$json_fields = $this->getRequest()->getParam('json_fields',array());

			foreach ($json_fields as $code => $row) {
				foreach ($row as $k => $items) {
					$valid = false;
					foreach ($items as $item) {
						if ($item) $valid = true;
					}

					if (!$valid) unset($row[$k]);
				}

				$fields[$code] = $row;
			}

			$mapper = new Admin_Model_Mapper_SiteSettingValue();
			foreach ($fields as $code => $value) {
				if (is_array($value)) {
					$value = json_encode($value);
				}

				$entity = $mapper->findByCode($code);
				$entity->code = $code;
				$entity->content = trim($value);
				$mapper->save($entity);
			}

			//delete cache
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(Zend_Cache::CLEANING_MODE_ALL);

			$this->_flashMessenger->setNamespace('info')->addMessage("Settings Updated");

			$this->redirect($redirect_url);
		}
	}

	function init()
	{
		$this->view->placeholder('section')->set("site-settings");

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}else{
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		}

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();
		$this->view->sitename = $options['site']['name'];

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
