<?php
class Admin_AuthController extends Zend_Controller_Action
{
	protected $_redirectUrl = '/admin/';
	/** @var  Zend_Db_Adapter_Abstract */
	protected $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/auth/login');
	}

	public function loginAction()
	{
		$this->view->remember = "";
		$form = new Admin_Form_Login();

		if (isset($_COOKIE['RememberMe'])) {
			$form->getElement('email')->setValue($_COOKIE['RememberMe']);
			$this->view->remember = "checked";
		}

		$this->view->loginform = $form;
	}

	public function logoutAction()
	{
		//manually destroy session if handler is memcache due to PHP7 memcache bug @see https://github.com/php-memcached-dev/php-memcached/issues/199
		if(session_status() !== PHP_SESSION_DISABLED && ini_get('session.save_handler') === 'memcache') {
			//surpress warnings just in case
			@session_destroy();
		}

		Zend_Session :: forgetMe();

		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		$this->getResponse()->setRedirect('/admin/auth');
	}

	public function identifyAction()
	{
		if( $this->getRequest()->isPost() ){
			$formData = $this->getRequest()->getPost();
			if( empty($formData['email']) || empty($formData['password']) ){
				$error = 'Empty email or password.';
			}else{
				if ($this->getRequest()->getParam('remember',0) == '1') {
					setcookie("RememberMe", $formData['email'], time()+(3600 * 365 * 20));
				}else {
					setcookie("RememberMe", '', time()-3600);
				}

				// do the authentication
				$authAdapter = $this->_getAuthAdapter($formData);
				$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
				$result = $auth->authenticate($authAdapter);
				if( !$result->isValid() ){
					$db = Zend_Db_Table::getDefaultAdapter();
					$au_c = $db->fetchOne("SELECT COUNT(*) FROM {$this->_table_prefix}.admin_users WHERE status='Enabled'");

					//check if special root user
					if ($au_c == 0 && $formData['email'] == $this->_rootuser && md5($formData['password']) == $this->_rootpass) {
						$data = array("id"=>0,"username"=>$this->_rootuser,"first_name"=>"Cyber-NY","last_name"=>"","email"=>"support@cyber-ny.com","status"=>"enabled","role"=>"admin");
						$auth->getStorage()->write((object)$data);
						$this->_redirectUrl = Cny_Auth::fetchPreviousRequestUrl();
						$url_redirect = ($this->_redirectUrl) ? $this->_redirectUrl : '/admin/';
						$this->getResponse()->setRedirect($url_redirect);
						return;
					}else {
						$sql = "SELECT * FROM {$this->_table_prefix}.admin_users WHERE email = '{$formData['email']}' LIMIT 1";
						$user = $this->_db->fetchRow($sql);

						$max_attempts = 3;
						if ($user) {
							$this->_db->update($this->_table_prefix.'admin_users', array('failed_attempts' => $user['failed_attempts'] + 1), 'id = ' . $user['id']);
							$left = $max_attempts - ($user['failed_attempts'] + 1);

							$error = 'Login failed. You have '.$left.' attempt(s) before your account will automatically be locked';

							if ($left <= 0) {
								$this->_db->update($this->_table_prefix.'admin_users', array('password' => new Zend_Db_Expr("NULL")), 'id = ' . $user['id']);

								$error = 'Login failed. Your account has been locked. Contact admin or use the forgot password to reset your password';
							}
						}else {
							$error = 'Login failed';
						}
					}
				}else{
					$last_login = date("Y-m-d H:i:s");
					$data = $authAdapter->getResultRowObject(null,'password');
					$db = Zend_Db_Table::getDefaultAdapter();
					$db->update($this->_table_prefix.'admin_users', array('last_login'=>$last_login, 'failed_attempts'=>new Zend_Db_Expr("NULL")), 'id = '.$data->id );
					$auth->getStorage()->write($data);

					//check if they have accepted latest licenses
					$mapper = new Admin_Model_Mapper_AdminUserLicense();
					if (!$mapper->checkUpToDate($data->id)) {
						$this->getResponse()->setRedirect("/admin/license");
						return;
					}

					$this->_redirectUrl = Cny_Auth::fetchPreviousRequestUrl();
					$url_redirect = ($this->_redirectUrl) ? $this->_redirectUrl : '/admin/';
					$this->getResponse()->setRedirect($url_redirect);
					return;
				}
			}
		}

		$this->_flashMessenger->addMessage($error);
		$this->getResponse()->setRedirect('/admin/auth/login');
	}

	public function forgotAction()
	{
		if( $this->getRequest()->isPost() ){
			$email = $this->_getParam("email","");

			$sql = $this->_db->quoteInto("SELECT * FROM {$this->_table_prefix}admin_users WHERE email = ? AND status='enabled' LIMIT 1",$email);
			$user = $this->_db->fetchRow($sql);

			if (!$user || !$email) {
				$this->view->error = "Invalid Email";
			}else {
				$code = md5(uniqid(rand()));
				$this->_db->update($this->_table_prefix."admin_users",array("reset_expire"=>new Zend_Db_Expr("DATE_ADD(NOW(),INTERVAL 1 HOUR)"),"reset_code"=>$code),"id='{$user['id']}'");

				$pageURL = 'http';
				if (array_key_exists("HTTPS",$_SERVER) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
				$pageURL .= "://";
				$pageURL .= $_SERVER["SERVER_NAME"];

				$mail = new Zend_Mail();
				$mail->setBodyHtml('
<p>Click on the link below to reset your password:</p>
<p><a href="'.$pageURL."/admin/auth/reset/c/".$code.'">'.$pageURL."/admin/auth/reset/c/".$code.'</a></p>
<p>If you did not request for your password to be reset you don\'t need to take any action, just ignore this email.</p>
<p>The link to reset your password will expire in one(1) hour.</p>
				');
				$mail->setFrom($this->_email, $this->_from);
				$mail->addTo($user['email'], $user['first_name']." ".$user['last_name']);
				$mail->setSubject('Password reset for '.$this->_site.' ');
				$mail->send();

				$this->_flashMessenger->addMessage("Password reset email sent");
				$this->getResponse()->setRedirect('/admin/auth/login');
			}
		}
	}

	public function resetAction()
	{
		$this->view->placeholder('section')->set("login password-reset");

		$this->view->code = $code = $this->_getParam("c","");

		$sql = $this->_db->quoteInto("SELECT * FROM {$this->_table_prefix}admin_users WHERE reset_code = ? AND status='enabled' AND reset_expire > NOW() LIMIT 1",$code);
		$user = $this->_db->fetchRow($sql);

		if (!$user || !$code) {
			$this->view->error = 'Email Address not found or link expired';
		}else {
			if( $this->getRequest()->isPost() ){
				$password = $this->_getParam("password","");
				$v_password = $this->_getParam("v_password","");

				if( $password && $v_password == $password ){
					$mapper = new Admin_Model_Mapper_AdminUser();

					$entity = $mapper->find($user['id']);
					$adminuser = $entity->toArray();

					$bcrypt = new Jr_Crypt_Password_Bcrypt();
					$password = $bcrypt->create($password);

					$adminuser['password'] = $password;
					$mapper->save(new Admin_Model_AdminUser($adminuser));

					$this->_db->update($this->_table_prefix."admin_users",array('failed_attempts'=>new Zend_Db_Expr("NULL"),"reset_code"=>new Zend_Db_Expr("NULL"),"reset_expire"=>new Zend_Db_Expr("NULL")),"id='{$user['id']}'");

					$this->_flashMessenger->addMessage("Password successfully saved");
					$this->getResponse()->setRedirect('/admin/auth/login');
				}

				$this->view->error = "Password doesn't match";
			}
		}
	}

	protected function _getAuthAdapter($formData)
	{
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		$authAdapter = new Jr_Auth_Adapter_DbTableBcrypt( $dbAdapter );
		$authAdapter->setTableName($this->_table_prefix.'admin_users')
			->setIdentityColumn('email')
			->setCredentialColumn('password')
			->setCredentialTreatment('AND status = "enabled"');

		$authAdapter->setIdentity($formData['email']);
		$authAdapter->setCredential($formData['password']);
		return $authAdapter;
	}

	public function checkAction()
	{
		$this->view->layout()->disableLayout();

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		echo json_encode(array('isLoggedIn' => ($auth->hasIdentity())?true:false ));

		exit;
	}

	function init()
	{
		$this->view->layout()->setLayout("clean");

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->_from = ($options['auth']['from_name'])?$options['auth']['from_name']:"Password Recovery";
		$this->_email = ($settings_mapper->getValueByCode('email_from_address'))?$settings_mapper->getValueByCode('email_from_address'):$options['auth']['from_email'];
		if (!$this->_email && isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$this->_email = "do-not-reply@{$_SERVER["SERVER_NAME"]}";
		}
		$this->_site = ($settings_mapper->getValueByCode('site_name'))?$settings_mapper->getValueByCode('site_name'):$options['site']['name'];
		if (!$this->_site && isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$this->_site = $_SERVER["SERVER_NAME"];
		}


		$this->_rootuser = $options['root']['user'];
		$this->_rootpass = $options['root']['password'];

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		if( $auth->hasIdentity() && $_SERVER['REQUEST_URI'] != "/admin/auth/logout" && $_SERVER['REQUEST_URI'] != "/admin/auth/check"){
			$this->getResponse()->setRedirect('/');
		}else{
			$this->view->placeholder('logged_in')->set(false);
			$this->view->placeholder('section')->set("login");
		}

		$bg_url = new Cny_View_Helper_LoginBG();
		$this->view->headStyle()->appendStyle("body.login .login-register {background:url('".$bg_url->random()."') no-repeat center center / cover !important;}");
	}
}
