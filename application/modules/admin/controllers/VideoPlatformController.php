<?php
use Cocur\Slugify\Slugify;
use Done\Subtitles\Subtitles;

class Admin_VideoPlatformController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->forward("item-list");
	}
	public function libraryAction()
	{
		$this->forward("item-list");
	}

	public function itemListAction()
	{
		$this->view->placeholder('page_title')->set("Videos");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li><li class="active">Video Library</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','title');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');
		$this->view->has_video = $has_video = $this->_getParam('has_video','');
		$this->view->transcode_status = $transcode_status = $this->_getParam('transcode_status','');
		$this->view->stream_transcode_status = $stream_transcode_status = $this->_getParam('stream_transcode_status','');

		$search_keys = array(
			'keyword' => $mask,
			'status' => $status,
			'alpha' => $alpha,
			'admin_user_id' => ($this->view->user->role == 'video_member')?$this->view->user->id:null,
			'has_video' => $has_video,
			'transcode_status' => $transcode_status,
			'stream_transcode_status' => $stream_transcode_status,
		);
		$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'videos';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function itemAddAction()
	{
		$this->view->placeholder('page_title')->set("Videos");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li class="active">Add Video</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_VideoPlatformVideo();
		$form->getElement('cmd')->setValue('add');
		$form->getElement('admin_user_id')->setValue($this->view->user->id);

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('item-form');
	}

	public function itemEditAction()
	{
		$this->view->video_id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Videos");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li class="active">Edit Video: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_VideoPlatformVideo();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
			$this->view->video = $entity = $mapper->find($id);

			if ($this->view->user->role == 'video_member') {
				if ($entity->admin_user_id != $this->view->user->id) {
					$this->getResponse()->setRedirect('/admin/video-platform/library');
				}
			}

			$data = $entity->toArray();

			//categories
			$c_mapper = new Admin_Model_Mapper_VideoPlatformCategory();
			$data['categories'] = implode(",",array_keys($c_mapper->fetchPairsByVideoId($id)));

			//genres
			$g_mapper = new Admin_Model_Mapper_VideoPlatformGenre();
			$data['genres'] = implode(",",array_keys($g_mapper->fetchPairsByVideoId($id)));

			//tags
			$t_mapper = new Admin_Model_Mapper_VideoPlatformTag();
			$data['tags'] = implode("|",$t_mapper->fetchTagNamesByVideoId($id));

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('item-form');
		}else{
			$this->getResponse()->setRedirect('/admin/video-platform/library');
		}
	}

	public function itemViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
			$this->view->item = $video = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Videos");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li class="active">View Video: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			//get streams (transcoded files)
			$stream_mapper = new Admin_Model_Mapper_VideoPlatformVideoStream();
			$this->view->streams = $streams = $stream_mapper->fetchByVideoId($id);

			$s3 = new Application_Model_AmazonS3();
			$complete_count = 0;
			foreach ($streams as $stream) {
				if ($stream->aws_job_id && $stream->transcode_status == 'Processing') {
					$result = $s3->jobStatus($stream->aws_job_id);

					//Submitted , Progressing , Complete , Canceled , or Error
					switch ($result->Job->Status) {
						case "Submitted":
						case "Progressing":
							$stream->transcode_status = "Processing";
							break;
						case "Canceled":
							$stream->transcode_status = "Error";
							break;
						case "Complete":
							$complete_count++;
							$stream->transcode_status = $result->Job->Status;
							break;
						default:
							$stream->transcode_status = $result->Job->Status;
							break;
					}
					$stream_mapper->save($stream);
				}elseif ($stream->transcode_status == 'Complete') {
					$complete_count++;
				}
			}

			//update main video transcode status
			$transcode_status = (count($streams) == $complete_count)?'Complete':'Processing';
			if ($transcode_status != $video->transcode_status) {
				$video->transcode_status = $transcode_status;
				$mapper->save($video);
			}

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoImage();
			$this->view->images = $mapper->fetchByVideoId($video->id);

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
			$this->view->trailer = $trailer = $mapper->fetchByVideoId($video->id);

			if ($trailer->id) {
				$s3 = new Application_Model_AmazonS3();
				$this->view->trailer_url = $s3->getVideoTrailerURL($trailer->id);

				$stream_mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
				$streams = $stream_mapper->fetchByTrailerId($trailer->id);

				$complete_count = 0;
				foreach ($streams as $stream) {
					if ($stream->aws_job_id && $stream->transcode_status == 'Processing') {
						$result = $s3->jobStatus($stream->aws_job_id);

						//Submitted , Progressing , Complete , Canceled , or Error
						switch ($result->Job->Status) {
							case "Submitted":
							case "Progressing":
								$stream->transcode_status = "Processing";
								break;
							case "Canceled":
								$stream->transcode_status = "Error";
								break;
							case "Complete":
								$complete_count++;
								$stream->transcode_status = $result->Job->Status;
								break;
							default:
								$stream->transcode_status = $result->Job->Status;
								break;
						}
						$stream_mapper->save($stream);
					}elseif ($stream->transcode_status == 'Complete') {
						$complete_count++;
					}
				}
				$this->view->trailer_streams = $stream_mapper->fetchByTrailerId($trailer->id);

				//update main video transcode status
				$transcode_status = (count($streams) == $complete_count)?'Complete':'Processing';
				if ($transcode_status != $trailer->transcode_status) {
					$trailer = $mapper->find($trailer->id);
					$trailer->transcode_status = $transcode_status;
					$mapper->save($trailer);
				}
			}

			//cast & crew / director
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoCast();
			$this->view->directors = $mapper->fetchByVideoId($video->id, 'director');
			$this->view->cast = $mapper->fetchByVideoId($video->id, 'cast');
			$this->view->crew = $mapper->fetchByVideoId($video->id, 'crew');

			//News & Reviews
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoNews();
			$this->view->news = $mapper->fetchByVideoId($video->id, 'news');
			$this->view->reviews = $mapper->fetchByVideoId($video->id, 'reviews');

			//monetization
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetization();
			$this->view->monetization = $mapper->fetchByVideoId($video->id);

			//monetization links
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetizationLink();
			$this->view->monetization_links = $mapper->fetchByVideoId($video->id);

			//tracks
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrack();
			$this->view->tracks = $mapper->fetchByVideoId($video->id);

			//extras / bonus features
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
			$this->view->extras = $extras = $mapper->fetchByVideoId($video->id);

			if ($extras) {
				$stream_mapper = new Admin_Model_Mapper_VideoPlatformVideoExtrasStream();

				foreach ($extras as $extra) {
					//$this->view->trailer_url = $s3->getVideoExtrasURL($extra->id);

					$streams = $stream_mapper->fetchByExtrasId($extra->id);

					$complete_count = 0;
					foreach ($streams as $stream) {
						if ($stream->aws_job_id && $stream->transcode_status == 'Processing') {
							$result = $s3->jobStatus($stream->aws_job_id);

							//Submitted , Progressing , Complete , Canceled , or Error
							switch ($result->Job->Status) {
								case "Submitted":
								case "Progressing":
									$stream->transcode_status = "Processing";
									break;
								case "Canceled":
									$stream->transcode_status = "Error";
									break;
								case "Complete":
									$complete_count++;
									$stream->transcode_status = $result->Job->Status;
									break;
								default:
									$stream->transcode_status = $result->Job->Status;
									break;
							}
							$stream_mapper->save($stream);
						} elseif ($stream->transcode_status == 'Complete') {
							$complete_count++;
						}
					}
					//$this->view->streams = $stream_mapper->fetchByExtrasId($extra->id);

					//update main video transcode status
					$transcode_status = (count($streams) == $complete_count) ? 'Complete' : 'Processing';
					if ($transcode_status != $extra->transcode_status) {
						$extra->transcode_status = $transcode_status;
						$mapper->save($extra);
					}
				}
			}

			//geoblocks
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoGeoblock();
			$this->view->geoblocks = $mapper->fetchByVideoId($video->id);

			//categories
			$c_mapper = new Admin_Model_Mapper_VideoPlatformCategory();
			$this->view->categories = implode(",",array_values($c_mapper->fetchPairsByVideoId($video->id)));

			//genres
			$g_mapper = new Admin_Model_Mapper_VideoPlatformGenre();
			$this->view->genres = implode(",",array_values($g_mapper->fetchPairsByVideoId($video->id)));

			//tags
			$t_mapper = new Admin_Model_Mapper_VideoPlatformTag();
			$this->view->tags = implode(", ",$t_mapper->fetchTagNamesByVideoId($video->id));

		}else{
			$this->getResponse()->setRedirect('/admin/video-platform/library');
		}
	}

	public function itemSaveAction()
	{
		$form = new Admin_Form_VideoPlatformVideo();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_VideoPlatformVideo();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($this->getRequest()->getParams())) {
			$entity = $mapper->find($data['id']);
			if (!$entity) {
				$entity = new Application_Model_VideoPlatformVideo();
			}
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					unset($data[$k]);
				}
			}

			if ($this->view->user->role == 'video_member') {
				if (!$entity->admin_user_id) {
					$entity->admin_user_id = $this->view->user->id;
				}elseif ($entity->admin_user_id != $this->view->user->id) {
					$this->getResponse()->setRedirect('/admin/video-platform/library');
				}
			}

			if (!$entity->video_hash) {
				$entity->video_hash = $mapper->createVideoHash();
			}

			$entity_data = array_merge($entity->toArray(), $data);
			$id = $mapper->save(new Application_Model_VideoPlatformVideo($entity_data));

			//categories
			$vt_mapper = new Admin_Model_Mapper_VideoPlatformVideoCategory();
			$vt_mapper->deleteByVideoId($id);

			$sort = 0;
			foreach (explode(",",$data['categories']) as $k => $tag_id) {
				if ($tag_id) {
					$vt_mapper->save(new Application_Model_VideoPlatformVideoCategory(array('video_platform_video_id' => $id, 'video_platform_category_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			//genres
			$vt_mapper = new Admin_Model_Mapper_VideoPlatformVideoGenre();
			$vt_mapper->deleteByVideoId($id);

			$sort = 0;
			foreach (explode(",",$data['genres']) as $k => $tag_id) {
				if ($tag_id) {
					$vt_mapper->save(new Application_Model_VideoPlatformVideoGenre(array('video_platform_video_id' => $id, 'video_platform_genre_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			//tag groups
			$t_mapper = new Admin_Model_Mapper_VideoPlatformTag();
			$vt_mapper = new Admin_Model_Mapper_VideoPlatformVideoTag();

			$vt_mapper->deleteByVideoId($id);

			$tags = explode("|",$data['tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Application_Model_VideoPlatformTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'Enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$vt_mapper->save(new Application_Model_VideoPlatformVideoTag(array('video_platform_video_id' => $id, 'video_platform_tag_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			if ($this->getRequest()->getParam('new_upload','') == 'transcode') {
				$s3 = new Application_Model_AmazonS3();
				$s3->transcode($id);
			}

			$this->getResponse()->setRedirect('/admin/video-platform/library');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('item-form');
		}
	}

	public function itemChunkInitAction()
	{
		$s3 = new Application_Model_AmazonS3();

		//AWS has a minimum upload of 5MB
		if ($this->getRequest()->getParam('file_size',0) < 5242880) {
			echo json_encode(array('status'=>'errors','info'=>'File is too small'));
			exit;
		}

		$result = $s3->initiateMultipartFile(array(
			'file_id' => $this->getRequest()->getParam('dzuuid'),
			'file_name' => $this->getRequest()->getParam('filename',''),
			'total_size' => $this->getRequest()->getParam('file_size',0),
			'mime_type' => $this->getRequest()->getParam('mime_type',''),
		));

		echo json_encode($result);
		exit;
	}

	public function itemChunkUploadAction()
	{
		$s3 = new Application_Model_AmazonS3();
		$response = $s3->uploadChunk(array(
			'multipart_upload_id' => $this->getRequest()->getParam('multipart_upload_id',''),
			'key' => $this->getRequest()->getParam('key',''),
			'file_id' => $_POST['dzuuid'],
			'chunk_index' => $_POST['dzchunkindex'] + 1,
			'total_chunks' => $_POST['dztotalchunkcount'],
			'file_name' => $_FILES['file']['name'],
			'chunk_file_size' => (isset($_POST['dzchunksize'])) ? (int)$_POST['dzchunksize'] : $_FILES["file"]["size"],
			'chunk_byte_offest' => (isset($_POST['dzchunkbyteoffset'])) ? (int)$_POST['dzchunkbyteoffset'] : 0,
			'file_tmp_name' => $_FILES['file']['tmp_name'],
			'total_size' => (isset($_POST['dztotalfilesize'])) ? (int)$_POST['dztotalfilesize'] : 0,
		));

		if ($response['status'] == 'success') {
			header("HTTP/1.0 200 OK");
		}else {
			header("HTTP/1.0 401 Error");
		}
		echo json_encode( array(
			"status" => $response['status'],
			"info" => $response['info'],
		));

		exit;
	}

	public function itemChunkConcatAction()
	{
		$s3 = new Application_Model_AmazonS3();
		$response = $s3->chunkConcatenate(array(
			'multipart_upload_id' => $this->getRequest()->getParam('multipart_upload_id',''),
			'key' => $this->getRequest()->getParam('key',''),
			'file_id' => $this->getRequest()->getParam('dzuuid'),
			'total_chunks' => $this->getRequest()->getParam('dztotalchunkcount',0),
			'file_name' => $this->getRequest()->getParam('filename',''),
			'total_size' => $this->getRequest()->getParam('file_size',0),
			'mime_type' => $this->getRequest()->getParam('mime_type',''),
		),$this->getRequest()->getParam('id',null));

		if ($response['status'] == 'success') {
			header("HTTP/1.0 200 OK");
		}else {
			header("HTTP/1.0 401 Error");
		}
		echo json_encode( array(
			"status" => $response['status'],
			"info" => $response['info'],
			"video_id" => $response['video_id']
		));

		exit;
	}

	public function itemDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$s3 = new Application_Model_AmazonS3();
			$s3->deleteVideo($id);

			$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
			$entity = $mapper->find($id);

			if ($this->view->user->role == 'video_member') {
				if ($entity->admin_user_id != $this->view->user->id) {
					$this->getResponse()->setRedirect('/admin/video-platform/library');
				}
			}

			$mapper->delete($entity);

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoStream();
			$videos = $mapper->fetchByVideoId($id);

			foreach ($videos as $stream) {
				$mapper->delete($stream);
			}

			//delete trailer
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
			$trailer = $mapper->fetchByVideoId($id);
			if ($trailer) {
				$entity = $mapper->find($trailer->id);
				$mapper->delete($entity);

				$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
				$videos = $mapper->fetchByTrailerId($trailer->id);

				foreach ($videos as $stream) {
					$mapper->delete($stream);
				}
			}
		}

		$this->getResponse()->setRedirect('/admin/video-platform/library');
	}

	public function imageViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoImage();
			$this->view->images = $mapper->fetchByVideoId($video_id);
		}
	}

	public function imageAddAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){

			$form = new Admin_Form_VideoPlatformVideoImage();

			$data = array(
				'video_platform_video_id' => $video_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('image-form');
		}
	}

	public function imageEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_VideoPlatformVideoImage();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoImage();
			$item = $mapper->find($id);

			$data = $item->toArray();

			//tags
			$t_mapper = new Admin_Model_Mapper_VideoPlatformImageTag();
			$data['tags'] = implode("|",$t_mapper->fetchTagNamesByVideoImageId($id));

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('image-form');
		}
	}

	public function imageSaveAction()
	{
		$form = new Admin_Form_VideoPlatformVideoImage();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoImage();
			$item = new Application_Model_VideoPlatformVideoImage($data);
			$id = $mapper->save($item);

			//tag groups
			$t_mapper = new Admin_Model_Mapper_VideoPlatformImageTag();
			$tc_mapper = new Admin_Model_Mapper_VideoPlatformVideoImageTag();

			$tc_mapper->deleteByVideoImageId($id);

			$tags = explode("|",$data['tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Application_Model_VideoPlatformImageTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v, 'status' => 'Enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Application_Model_VideoPlatformVideoImageTag(array('video_platform_video_image_id' => $id, 'video_platform_image_tag_id' => $tag_id, 'sort_order' => $sort)));

					$sort++;
				}
			}

			$this->getResponse()->setRedirect("/admin/video-platform/image-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('image-form');
		}
	}


	public function imageDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$video_id = $this->getRequest()->getParam('video_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoImage();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/video-platform/image-view/video_id/{$video_id}");
	}

	public function imageSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_VideoPlatformVideoImage();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function monetizationViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetization();
			$this->view->monetization = $mapper->fetchByVideoId($video_id);

			if (!$this->view->monetization) {
				$item = new Application_Model_VideoPlatformVideoMonetization(array(
					'video_platform_video_id' => $video_id
				));
				$mapper->save($item);
				$this->view->monetization = $mapper->fetchByVideoId($video_id);
			}

			//monetization links
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetizationLink();
			$this->view->monetization_links = $mapper->fetchByVideoId($video_id);
		}
	}

	public function monetizationEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)) {
			$form = new Admin_Form_VideoPlatformVideoMonetization();

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetization();
			$item = $mapper->find($id);

			$data = $item->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('monetization-form');
		}
	}

	public function monetizationSaveAction()
	{
		$form = new Admin_Form_VideoPlatformVideoMonetization();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetization();
			$item = new Application_Model_VideoPlatformVideoMonetization($data);
			$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/video-platform/monetization-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('monetization-link-form');
		}
	}

	public function monetizationLinkViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetizationLink();
			$this->view->monetization_links = $mapper->fetchByVideoId($video_id);
		}
	}

	public function monetizationLinkAddAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)) {
			$form = new Admin_Form_VideoPlatformVideoMonetizationLink();
			$form->getElement('video_platform_video_id')->setValue($video_id);
			$form->getElement('sort_order')->setValue(9999);
			$form->getElement('cmd')->setValue('add');

			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('monetization-link-form');
		}
	}

	public function monetizationLinkEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)) {
			$form = new Admin_Form_VideoPlatformVideoMonetizationLink();

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetizationLink();
			$item = $mapper->find($id);

			$data = $item->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('monetization-link-form');
		}
	}

	public function monetizationLinkSaveAction()
	{
		$form = new Admin_Form_VideoPlatformVideoMonetizationLink();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetizationLink();
			$item = new Application_Model_VideoPlatformVideoMonetizationLink($data);
			$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/video-platform/monetization-link-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('monetization-link-form');
		}
	}

	public function monetizationLinkDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		$video_id = $this->getRequest()->getParam('video_id', 0);

		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetizationLink();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect("/admin/video-platform/monetization-link-view/video_id/{$video_id}");
	}

	public function monetizationLinkSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_VideoPlatformVideoMonetizationLink();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function trailerViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
			$video = $mapper->find($video_id);

			//get thumbnail image either from custom or selected
			//TODO - currently only custom thumbnail exists
			$this->view->thumbnail_url = ($video->thumbnail_src)?$video->thumbnail_src:(($video->video_platform_video_thumbnail_id)?'':null);

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
			$this->view->trailer = $trailer = $mapper->fetchByVideoId($video_id);

			if ($trailer->id) {
				if (!$trailer->embed_url && !$trailer->embed_code) {
					$s3 = new Application_Model_AmazonS3();
					$this->view->trailer_url = $s3->getVideoTrailerURL($trailer->id);

					$stream_mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
					$streams = $stream_mapper->fetchByTrailerId($trailer->id);

					$complete_count = 0;
					foreach ($streams as $stream) {
						if ($stream->aws_job_id && $stream->transcode_status == 'Processing') {
							$result = $s3->jobStatus($stream->aws_job_id);

							//Submitted , Progressing , Complete , Canceled , or Error
							switch ($result->Job->Status) {
								case "Submitted":
								case "Progressing":
									$stream->transcode_status = "Processing";
									break;
								case "Canceled":
									$stream->transcode_status = "Error";
									break;
								case "Complete":
									$complete_count++;
									$stream->transcode_status = $result->Job->Status;
									break;
								default:
									$stream->transcode_status = $result->Job->Status;
									break;
							}
							$stream_mapper->save($stream);
						} elseif ($stream->transcode_status == 'Complete') {
							$complete_count++;
						}
					}
					$this->view->streams = $stream_mapper->fetchByTrailerId($trailer->id);

					//update main video transcode status
					$transcode_status = (count($streams) == $complete_count) ? 'Complete' : 'Processing';
					if ($transcode_status != $trailer->transcode_status) {
						$trailer = $mapper->find($trailer->id);
						$trailer->transcode_status = $transcode_status;
						$mapper->save($trailer);
					}
				}
			}
		}
	}

	public function trailerAddAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)) {
			$form = new Admin_Form_VideoPlatformVideoTrailer();
			$form->getElement('video_platform_video_id')->setValue($video_id);
			$form->getElement('cmd')->setValue('add');

			$this->view->cmd = 'add';
			$this->view->form = $form;

			$this->render('trailer-form');
		}
	}

	public function trailerEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)) {
			$form = new Admin_Form_VideoPlatformVideoTrailer();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('trailer-form');
		}
	}

	public function trailerChunkConcatAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)) {
			$s3 = new Application_Model_AmazonS3();
			$response = $s3->chunkConcatenate(array(
				'multipart_upload_id' => $this->getRequest()->getParam('multipart_upload_id', ''),
				'key' => $this->getRequest()->getParam('key', ''),
				'file_id' => $this->getRequest()->getParam('dzuuid'),
				'total_chunks' => $this->getRequest()->getParam('dztotalchunkcount', 0),
				'file_name' => $this->getRequest()->getParam('filename', ''),
				'total_size' => $this->getRequest()->getParam('file_size', 0),
				'mime_type' => $this->getRequest()->getParam('mime_type', ''),
			),$this->getRequest()->getParam('id',null), 'trailer', $video_id);
		}

		if ($response['status'] == 'success') {
			header("HTTP/1.0 200 OK");
		}else {
			header("HTTP/1.0 401 Error");
		}
		echo json_encode( array(
			"status" => $response['status'],
			"info" => $response['info'],
			"video_id" => $response['video_id']
		));

		exit;
	}

	public function trailerSaveAction()
	{
		$this->view->errors = array();

		$form = new Admin_Form_VideoPlatformVideoTrailer();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();

		if( $form->isValid($this->getRequest()->getParams())) {
			$entity = $mapper->find($data['id']);
			if (!$entity) {
				$entity = new Application_Model_VideoPlatformVideoTrailer();
			}
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					unset($data[$k]);
				}
			}
			$entity_data = array_merge($entity->toArray(), $data);
			$id = $mapper->save(new Application_Model_VideoPlatformVideoTrailer($entity_data));

			if ($this->getRequest()->getParam('new_upload','') == 'transcode') {
				$s3 = new Application_Model_AmazonS3();
				$s3->transcode($id, null, 'trailer');
			}

			$this->getResponse()->setRedirect("/admin/video-platform/trailer-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('trailer-form');
		}
	}

	public function trailerDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		$video_id = $this->getRequest()->getParam('video_id', 0);

		if(!empty($id) && is_numeric($id)){
			$s3 = new Application_Model_AmazonS3();
			$s3->deleteTrailer($id);

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
			$entity = $mapper->find($id);
			$mapper->delete($entity);

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
			$videos = $mapper->fetchByTrailerId($id);

			foreach ($videos as $stream) {
				$mapper->delete($stream);
			}
		}

		$this->getResponse()->setRedirect("/admin/video-platform/trailer-view/video_id/{$video_id}");
	}

	public function castViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoCast();
			$this->view->directors = $mapper->fetchByVideoId($video_id, 'director');
			$this->view->cast = $mapper->fetchByVideoId($video_id, 'cast');
			$this->view->crew = $mapper->fetchByVideoId($video_id, 'crew');
		}
	}

	public function castAddAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){

			$form = new Admin_Form_VideoPlatformVideoCast();

			$data = array(
				'video_platform_video_id' => $video_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('cast-form');
		}
	}

	public function castEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_VideoPlatformVideoCast();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoCast();
			$item = $mapper->find($id);

			$data = $item->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('cast-form');
		}
	}

	public function castSaveAction()
	{
		$form = new Admin_Form_VideoPlatformVideoCast();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoCast();
			$item = new Application_Model_VideoPlatformVideoCast($data);
			$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/video-platform/cast-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('cast-form');
		}
	}


	public function castDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$video_id = $this->getRequest()->getParam('video_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoCast();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/video-platform/cast-view/video_id/{$video_id}");
	}

	public function castSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_VideoPlatformVideoCast();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function profileViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoProfile();
			$profile = $mapper->getProfileByVideoId($video_id);

			//if the standard attributes don't exist create them
			$attribute_mapper = new Application_Model_Mapper_VideoPlatformVideoAttribute();
			foreach ($this->view->standard_attributes as $standard_attribute) {
				$attribute = $attribute_mapper->doesExists(array('code' => $standard_attribute));
				if ($attribute && !isset($profile[$standard_attribute])) {
					$mapper->save(new Application_Model_VideoPlatformVideoProfile(array(
						'video_platform_video_id' => $video_id,
						'video_platform_video_attribute_id' => $attribute->id,
						'value' => null,
						'visibility' => 'Visible',
					)));
				}
			}

			$this->view->profile = $mapper->getProfileByVideoId($video_id);
		}
	}

	public function profileAddAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){

			$form = new Admin_Form_VideoPlatformVideoProfile();

			$data = array(
				'video_platform_video_id' => $video_id
			);

			if ($attribute_id = $this->getRequest()->getParam('attribute_id', 0)) {
				$data['video_platform_video_attribute_id'] = $attribute_id;
			}

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('profile-form');
		}
	}

	public function profileEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_VideoPlatformVideoProfile();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoProfile();
			$item = $mapper->find($id);

			$form->populate($item->toArray());
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';
			$this->view->video_platform_video_attribute_id = $item->video_platform_video_attribute_id;

			$this->render('profile-form');
		}
	}

	public function profileSaveAction()
	{
		$form = new Admin_Form_VideoPlatformVideoProfile();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoProfile();
			$item = new Application_Model_VideoPlatformVideoProfile($data);
			$mapper->save($item);

			//clear cache
			$cache_id = 'video_platform_video_profile_' . $data['video_platform_video_id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->remove($cache_id);

			$this->getResponse()->setRedirect("/admin/video-platform/profile-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('profile-form');
		}
	}

	public function profileDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$video_id = $this->getRequest()->getParam('video_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoProfile();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		//clear cache
		$cache_id = 'video_platform_video_profile_' . $video_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->remove($cache_id);

		$this->getResponse()->setRedirect("/admin/video-platform/profile-view/video_id/{$video_id}");
	}

	public function profileSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$video_id = $this->getRequest()->getParam('video_id', 0);
		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoProfile();
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}

		$mapper->flushCacheByVideoId($video_id);
	}

	public function loadAttributeInputAction()
	{
		$video_id = $this->getRequest()->getParam('video_id',0);
		$attribute_id = $this->getRequest()->getParam('attribute_id',0);
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoAttribute();
		$attribute = $mapper->find($attribute_id);
		$field_type = (isset($attribute->field_type))?$attribute->field_type:'text';

		$item = $value = $options = null;
		if ($video_id) {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoProfile();
			$item = $mapper->getProfileByVideoId($video_id);
		}
		if ($item && $attribute) {
			$value = $item[$attribute->code]['value'];
			$options = (isset($item[$attribute->code]['options']))?json_decode($item[$attribute->code]['options']):array();
		}

		echo $this->view->partial('/product/partials/'.$field_type.'.phtml', array('value'=>$value,'options'=>$options));

		exit;
	}

	public function attributeAddAction()
	{
		//
	}

	public function attributeSaveAction()
	{
		$data = $this->getRequest()->getParam('data', array());

		$mapper = new Admin_Model_Mapper_VideoPlatformVideoAttribute();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify($data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $this->getRequest()->isPost() && trim($data['title']) ){
			$entity = new Application_Model_VideoPlatformVideoAttribute($data);
			$id = $mapper->save($entity);
		}

		echo "1";
		exit;
	}

	public function attributeJsonSearchAction()
	{
		$search_results = array();

		$sort = 'title';
		$dir = 'asc';

		$q = $this->getRequest()->getParam('q', '');
		$id = $this->getRequest()->getParam('id', '');

		if ($q || $id) {
			$search_keys = array(
				'keyword' => $q,
				'id' => $id
			);
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoAttribute();
			$results = $mapper->listSearch($sort, $dir, $search_keys);

			$stmt = $this->_db->query($results->__toString());
			$search_results = $stmt->fetchAll();
		}

		$return = array();
		foreach ($search_results as $row) {
			$return[] = array(
				'id' => $row['id'],
				'title' => $row['title']
			);
		}

		echo json_encode($return);
		exit;
	}

	public function trackViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrack();
			$this->view->tracks = $mapper->fetchByVideoId($video_id);
		}
	}

	public function trackAddAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){

			$form = new Admin_Form_VideoPlatformVideoTrack();

			$data = array(
				'video_platform_video_id' => $video_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('track-form');
		}
	}

	public function trackEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_VideoPlatformVideoTrack();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrack();
			$item = $mapper->find($id);

			$data = $item->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('track-form');
		}
	}

	public function trackSaveAction()
	{
		$form = new Admin_Form_VideoPlatformVideoTrack();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams()) || (!$_FILES && $data['public_url']) ) {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrack();

			if ($data['kind'] == 'subtitles' || $data['kind'] == 'captions') {
				$s3 = new Application_Model_AmazonS3();

				//get video entity so we can put the track file in the S3 folder for the video
				$video_mapper = new Admin_Model_Mapper_VideoPlatformVideo();
				$video = $video_mapper->find($data['video_platform_video_id']);

				if ($data['file']) {
					$path_parts = pathinfo($data['file']);

					if ($path_parts['extension'] != 'vtt') {
						//convert to webVTT
						Subtitles::convert(realpath('./../').'/aws/'.$data['file'], realpath('./../').'/aws/'.$path_parts['filename'].'.vtt');
						unlink(realpath('./../').'/aws/'.$data['file']);

						$data['filename'] = $path_parts['filename'].'.vtt';
					}else {
						$data['filename'] = $data['file'];
					}

					//offset subtitles
					if ($data['offset_seconds']) {
						$subtitles = Subtitles::load(realpath('./../').'/aws/'.$data['filename']);
						$subtitles->shiftTime($data['offset_seconds']);
						$subtitles->save(realpath('./../').'/aws/'.$data['filename']);
					}

					$public_url = $s3->uploadFileToS3(realpath('./../').'/aws/'.$data['filename'], $video->hash);
					$data['public_url'] = $public_url;
				}elseif($data['offset_seconds']) {
					$path_parts = pathinfo($data['filename']);
					$filename = $path_parts['filename'].'.vtt';

					if (copy($data['public_url'], realpath('./../').'/aws/'.$filename)) {

						//offset subtitles
						$subtitles = Subtitles::load(realpath('./../') . '/aws/' . $filename);
						$subtitles->shiftTime($data['offset_seconds']);
						$subtitles->save(realpath('./../') . '/aws/' . $filename);

						$public_url = $s3->uploadFileToS3(realpath('./../') . '/aws/' . $filename, $video->hash);
						unlink(realpath('./../').'/aws/'.$filename);

						$data['public_url'] = $public_url;
					}
				}
			}

			$item = new Application_Model_VideoPlatformVideoTrack($data);
			$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/video-platform/track-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('track-form');
		}
	}


	public function trackDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$video_id = $this->getRequest()->getParam('video_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrack();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/video-platform/track-view/video_id/{$video_id}");
	}

	public function extrasViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
			$video = $mapper->find($video_id);

			//get thumbnail image either from custom or selected
			//TODO - currently only custom thumbnail exists
			//$this->view->thumbnail_url = ($video->thumbnail_src)?$video->thumbnail_src:(($video->video_platform_video_thumbnail_id)?'':null);

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
			$this->view->extras = $extras = $mapper->fetchByVideoId($video_id);

			if ($extras) {
				$s3 = new Application_Model_AmazonS3();
				$stream_mapper = new Admin_Model_Mapper_VideoPlatformVideoExtrasStream();

				foreach ($extras as $extra) {
					//$this->view->trailer_url = $s3->getVideoExtrasURL($extra->id);

					$streams = $stream_mapper->fetchByExtrasId($extra->id);

					$complete_count = 0;
					foreach ($streams as $stream) {
						if ($stream->aws_job_id && $stream->transcode_status == 'Processing') {
							$result = $s3->jobStatus($stream->aws_job_id);

							//Submitted , Progressing , Complete , Canceled , or Error
							switch ($result->Job->Status) {
								case "Submitted":
								case "Progressing":
									$stream->transcode_status = "Processing";
									break;
								case "Canceled":
									$stream->transcode_status = "Error";
									break;
								case "Complete":
									$complete_count++;
									$stream->transcode_status = $result->Job->Status;
									break;
								default:
									$stream->transcode_status = $result->Job->Status;
									break;
							}
							$stream_mapper->save($stream);
						} elseif ($stream->transcode_status == 'Complete') {
							$complete_count++;
						}
					}
					//$this->view->streams = $stream_mapper->fetchByExtrasId($extra->id);

					//update main video transcode status
					$transcode_status = (count($streams) == $complete_count) ? 'Complete' : 'Processing';
					if ($transcode_status != $extra->transcode_status) {
						$extra->transcode_status = $transcode_status;
						$mapper->save($extra);
					}
				}
			}
		}
	}

	public function extrasAddAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)) {
			$form = new Admin_Form_VideoPlatformVideoExtra();
			$form->getElement('video_platform_video_id')->setValue($video_id);
			$form->getElement('cmd')->setValue('add');

			$this->view->cmd = 'add';
			$this->view->form = $form;

			$this->render('extras-form');
		}
	}

	public function extrasEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)) {
			$form = new Admin_Form_VideoPlatformVideoExtra();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('extras-form');
		}
	}

	public function extrasChunkConcatAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)) {
			$s3 = new Application_Model_AmazonS3();
			$response = $s3->chunkConcatenate(array(
				'multipart_upload_id' => $this->getRequest()->getParam('multipart_upload_id', ''),
				'key' => $this->getRequest()->getParam('key', ''),
				'file_id' => $this->getRequest()->getParam('dzuuid'),
				'total_chunks' => $this->getRequest()->getParam('dztotalchunkcount', 0),
				'file_name' => $this->getRequest()->getParam('filename', ''),
				'total_size' => $this->getRequest()->getParam('file_size', 0),
				'mime_type' => $this->getRequest()->getParam('mime_type', ''),
			),$this->getRequest()->getParam('id',null), 'extra', $video_id);
		}

		if ($response['status'] == 'success') {
			header("HTTP/1.0 200 OK");
		}else {
			header("HTTP/1.0 401 Error");
		}
		echo json_encode( array(
			"status" => $response['status'],
			"info" => $response['info'],
			"video_id" => $response['video_id']
		));

		exit;
	}

	public function extrasSaveAction()
	{
		$this->view->errors = array();

		$form = new Admin_Form_VideoPlatformVideoExtra();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();

		if( $form->isValid($this->getRequest()->getParams())) {
			$entity = $mapper->find($data['id']);
			if (!$entity) {
				$entity = new Application_Model_VideoPlatformVideoExtra();
			}
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					unset($data[$k]);
				}
			}
			//Cny_Debug::prettyPrint($data);
			$entity_data = array_merge($entity->toArray(), $data);
			$id = $mapper->save(new Application_Model_VideoPlatformVideoExtra($entity_data));
			if ($this->getRequest()->getParam('new_upload','') == 'transcode') {
				$s3 = new Application_Model_AmazonS3();
				$s3->transcode($id, null, 'extra');
			}

			$this->getResponse()->setRedirect("/admin/video-platform/extras-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('extras-form');
		}
	}

	public function extrasDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		$video_id = $this->getRequest()->getParam('video_id', 0);

		if(!empty($id) && is_numeric($id)){
			$s3 = new Application_Model_AmazonS3();
			$s3->deleteExtra($id);

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
			$entity = $mapper->find($id);
			$mapper->delete($entity);

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtrasStream();
			$videos = $mapper->fetchByExtrasId($id);

			foreach ($videos as $stream) {
				$mapper->delete($stream);
			}
		}

		$this->getResponse()->setRedirect("/admin/video-platform/extras-view/video_id/{$video_id}");
	}

	public function geoblockViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoGeoblock();
			$this->view->geoblocks = $mapper->fetchByVideoId($video_id);
		}
	}

	public function geoblockAddAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){

			$form = new Admin_Form_VideoPlatformVideoGeoblock();

			$data = array(
				'video_platform_video_id' => $video_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('geoblock-form');
		}
	}

	public function geoblockEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_VideoPlatformVideoGeoblock();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoGeoblock();
			$item = $mapper->find($id);

			$data = $item->toArray();
			$data[$data['granularity']] = $data['geo_code'];

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('geoblock-form');
		}
	}

	public function geoblockSaveAction()
	{
		$form = new Admin_Form_VideoPlatformVideoGeoblock();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$data['geo_code'] = $data[$data['granularity']];

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoGeoblock();
			$item = new Application_Model_VideoPlatformVideoGeoblock($data);
			$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/video-platform/geoblock-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('geoblock-form');
		}
	}


	public function geoblockDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$video_id = $this->getRequest()->getParam('video_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoGeoblock();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/video-platform/geoblock-view/video_id/{$video_id}");
	}

	public function geoblockSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_VideoPlatformVideoGeoblock();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}


	//News & Reviews
	public function newsViewAction()
	{
		$this->view->video_id = $video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoNews();
			$this->view->news = $mapper->fetchByVideoId($video_id, 'news');
			$this->view->reviews = $mapper->fetchByVideoId($video_id, 'reviews');
		}
	}

	public function newsAddAction()
	{
		$video_id = $this->getRequest()->getParam('video_id', 0);
		if(!empty($video_id) && is_numeric($video_id)){

			$form = new Admin_Form_VideoPlatformVideoNews();

			$data = array(
				'video_platform_video_id' => $video_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('news-form');
		}
	}

	public function newsEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_VideoPlatformVideoNews();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoNews();
			$item = $mapper->find($id);

			$data = $item->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('news-form');
		}
	}

	public function newsSaveAction()
	{
		$form = new Admin_Form_VideoPlatformVideoNews();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Admin_Model_Mapper_VideoPlatformVideoNews();
			$item = new Application_Model_VideoPlatformVideoNews($data);
			$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/video-platform/news-view/video_id/{$data['video_platform_video_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('news-form');
		}
	}


	public function newsDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$video_id = $this->getRequest()->getParam('video_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoNews();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/video-platform/news-view/video_id/{$video_id}");
	}

	public function newsSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_VideoPlatformVideoNews();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function categoryListAction()
	{
		$this->view->placeholder('page_title')->set("Video Categories");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li class="active">Video Categories</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','title');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_VideoPlatformCategory();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'video_platform_categories';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function categoryAddAction()
	{
		$this->view->placeholder('page_title')->set("Video Categories");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li><a href="/admin/video-platform/category-list">Video Categories</a></li>
				<li class="active">Add Video Category</li>';

		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_VideoPlatformCategory();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('category-form');
	}

	public function categoryEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Video Categories");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li><a href="/admin/video-platform/category-list">Video Categories</a></li>
				<li class="active">Edit Video Category: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_VideoPlatformCategory();
			$mapper = new Admin_Model_Mapper_VideoPlatformCategory();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('category-form');
		}else{
			$this->getResponse()->setRedirect('/admin/video-platform/category-list');
		}
	}

	public function categoryViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformCategory();
			$this->view->section = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Video Categories");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li><a href="/admin/video-platform/category-list">Video Categories</a></li>
				<li class="active">View Video Category: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			//videos in section
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoCategory();
			$this->view->items = $mapper->fetchVideosByCategoryId($id);
		}else{
			$this->getResponse()->setRedirect('/admin/video-platform/category-list');
		}
	}

	public function categorySaveAction()
	{
		$form = new Admin_Form_VideoPlatformCategory();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_VideoPlatformCategory();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($this->getRequest()->getParams())) {
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}

			$entity = new Application_Model_VideoPlatformCategory($data);
			$id = $mapper->save($entity);

			$this->getResponse()->setRedirect('/admin/video-platform/category-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('category-form');
		}
	}

	public function categoryDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformCategory();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/video-platform/category-list');
	}

	public function genreListAction()
	{
		$this->view->placeholder('page_title')->set("Video Genres");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li class="active">Video Genres</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','title');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_VideoPlatformGenre();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'video_platform_genres';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function genreAddAction()
	{
		$this->view->placeholder('page_title')->set("Video Genres");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li><a href="/admin/video-platform/genre-list">Video Genres</a></li>
				<li class="active">Add Video Genre</li>';

		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_VideoPlatformGenre();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('genre-form');
	}

	public function genreEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Video Categories");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li><a href="/admin/video-platform/genre-list">Video Genres</a></li>
				<li class="active">Edit Video Genre: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_VideoPlatformGenre();
			$mapper = new Admin_Model_Mapper_VideoPlatformGenre();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('genre-form');
		}else{
			$this->getResponse()->setRedirect('/admin/video-platform/genre-list');
		}
	}

	public function genreViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformGenre();
			$this->view->section = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Video Categories");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
				<li><a href="/admin/video-platform/genre-list">Video Genres</a></li>
				<li class="active">View Video Genre: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			//videos in section
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoGenre();
			$this->view->items = $mapper->fetchVideosByGenreId($id);
		}else{
			$this->getResponse()->setRedirect('/admin/video-platform/genre-list');
		}
	}

	public function genreSaveAction()
	{
		$form = new Admin_Form_VideoPlatformGenre();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_VideoPlatformGenre();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($this->getRequest()->getParams())) {
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}

			$entity = new Application_Model_VideoPlatformGenre($data);
			$id = $mapper->save($entity);

			$this->getResponse()->setRedirect('/admin/video-platform/genre-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('genre-form');
		}
	}

	public function genreDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformGenre();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/video-platform/genre-list');
	}

	public function ajaxListAction()
	{
		$this->view->layout()->disableLayout();

		$search_keys = array(
			'keyword' => $this->_getParam('q','')
		);
		$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
		$results = $mapper->listSearch('title', 'ASC', $search_keys);

		$stmt = $this->_db->query($results->__toString());
		$items = $stmt->fetchAll();

		$array = array();
		foreach ($items as $item) {
			$array['results'][] = array("id"=>$item['id'],"text"=>"{$item['title']}");
		}

		echo json_encode($array);exit;
	}

	public function ajaxViewAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
			$entity = $mapper->find($id);

			$item = $entity->toArray();
			$array = array("id"=>$item['id'],"text"=>"{$item['title']}");

			echo json_encode($array);exit;
		}
	}

	public function categoryAjaxListAction()
	{
		$this->view->layout()->disableLayout();

		$search_keys = array(
			'keyword' => $this->_getParam('q','')
		);
		$mapper = new Admin_Model_Mapper_VideoPlatformCategory();
		$results = $mapper->listSearch('title', 'ASC', $search_keys);

		$stmt = $this->_db->query($results->__toString());
		$items = $stmt->fetchAll();

		$array = array();
		foreach ($items as $item) {
			$array['results'][] = array("id"=>$item['id'],"text"=>"{$item['title']}");
		}

		echo json_encode($array);exit;
	}

	public function categoryAjaxViewAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformCategory();
			$entity = $mapper->find($id);

			$item = $entity->toArray();
			$array = array("id"=>$item['id'],"text"=>"{$item['title']}");

			echo json_encode($array);exit;
		}
	}

	public function genreAjaxListAction()
	{
		$this->view->layout()->disableLayout();

		$search_keys = array(
			'keyword' => $this->_getParam('q','')
		);
		$mapper = new Admin_Model_Mapper_VideoPlatformGenre();
		$results = $mapper->listSearch('title', 'ASC', $search_keys);

		$stmt = $this->_db->query($results->__toString());
		$items = $stmt->fetchAll();

		$array = array();
		foreach ($items as $item) {
			$array['results'][] = array("id"=>$item['id'],"text"=>"{$item['title']}");
		}

		echo json_encode($array);exit;
	}

	public function genreAjaxViewAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformGenre();
			$entity = $mapper->find($id);

			$item = $entity->toArray();
			$array = array("id"=>$item['id'],"text"=>"{$item['title']}");

			echo json_encode($array);exit;
		}
	}

	public function tagAjaxListAction()
	{
		$this->view->layout()->disableLayout();

		$search_keys = array(
			'keyword' => $this->_getParam('q','')
		);
		$mapper = new Admin_Model_Mapper_VideoPlatformTag();
		$results = $mapper->listSearch('title', 'ASC', $search_keys);

		$stmt = $this->_db->query($results->__toString());
		$items = $stmt->fetchAll();

		$array = array();
		foreach ($items as $item) {
			$array['results'][] = array("id"=>$item['id'],"text"=>"{$item['title']}");
		}

		echo json_encode($array);exit;
	}

	public function tagAjaxViewAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_VideoPlatformTag();
			$entity = $mapper->find($id);

			$item = $entity->toArray();
			$array = array("id"=>$item['id'],"text"=>"{$item['title']}");

			echo json_encode($array);exit;
		}
	}

	public function zypeLibraryAction()
	{
		$zype = new Application_Model_Zype();

		$this->view->placeholder('page_title')->set("Videos");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li><li class="active">Zype Video Library</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$per_page = 100;
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','title');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));

		$search_keys = array(
			'page' => $page,
			'per_page' => $per_page,
			'order' => ($dir == 'ASC')?'asc':'desc',
			'sort' => $sort,

			'q' => $mask,
		);
		$results = $zype->listVideos($search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			if ($results) {
				$all = [];

				$all = array_merge($all, $results->response);
				for ($i = 2; $i <= $results->pagination->pages; $i++) {
					$response2 = $zype->listVideos(array('page'=>$i,'per_page'=>$per_page));
					$all = array_merge($all, $response2->response);
				}
			}

			$this->view->items = $all;

			$this->view->file_name = 'zype_videos';

			$this->render('csvexport-zype-videos');
		}

		$this->view->response = $results->response;

		$paginator_fix = array_fill(0, $results->pagination->pages * $per_page, '');

		$paginator = Zend_Paginator::factory($paginator_fix);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage($per_page);
		$this->view->list = $paginator;
	}

	public function zypeVideoEditAction()
	{
		$zype = new Application_Model_Zype();

		$this->view->placeholder('page_title')->set("Videos");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
		li><a href="/admin/video-platform/zype-library">Zype Video Library </a></li><li class="active">Edit Video</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id)){

			$form = new Admin_Form_ZypeVideo();
			$item = $zype->getVideo($id);

			$this->view->video_title = $item->title;

			$data = [
				'id' => $item->_id,
				'title' => $item->title,
				'friendly_title' => $item->friendly_title,
				'short_description' => $item->short_description,
				'description' => $item->description,
				'keywords' => implode(", ",$item->keywords),
				'episode' => $item->episode,
				'season' => $item->season,
				'purchase_required' => ($item->purchase_required)?"On":"Off",
				'purchase_price' => $item->purchase_price,
				'rental_required' => ($item->rental_required)?"On":"Off",
				'rental_price' => $item->rental_price,
				'rental_duration' => $item->rental_duration,
				'subscription_required' => ($item->subscription_required)?"On":"Off",
				'registration_required' => ($item->registration_required)?"On":"Off",
				'active' => ($item->active)?"On":"Off",
			];

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('zype-video-form');
		}
	}

	public function zypeVideoSaveAction()
	{
		$zype = new Application_Model_Zype();

		$this->view->errors = array();

		$form = new Admin_Form_ZypeVideo();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$result = $zype->updateVideo($data['id'], [
				'title' => $data['title'],
				'friendly_title' => $data['friendly_title'],
				'short_description' => $data['short_description'],
				'description' => $data['description'],
				'keywords' => explode(",",$data['keywords']),
				'episode' => $data['episode'],
				'season' => $data['season'],
				'purchase_required' => ($data['purchase_required'] == 'On')?true:false,
				'purchase_price' => $data['purchase_price'],
				'rental_required' => ($data['rental_required'] == 'On')?true:false,
				'rental_price' => $data['rental_price'],
				'rental_duration' => $data['rental_duration'],
				'subscription_required' => ($data['subscription_required'] == 'On')?true:false,
				'registration_required' => ($data['registration_required'] == 'On')?true:false,
				'active' => ($data['active'] == 'On')?true:false,
			]);

			$this->_flashMessenger->setNamespace('info')->addMessage("Video Updated");

			$this->getResponse()->setRedirect("/admin/video-platform/zype-library");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('zype-video-form');
		}
	}

	public function zypePlaylistListAction()
	{
		$zype = new Application_Model_Zype();

		$this->view->placeholder('page_title')->set("Videos");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li><li class="active">Categories (Zype Playlists)</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$per_page = 100;
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','title');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));

		$search_keys = array(
			'page' => $page,
			'per_page' => $per_page,
			'order' => ($dir == 'ASC')?'asc':'desc',
			'sort' => $sort,

			'q' => $mask,
		);
		$results = $zype->listPlaylists($search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			if ($results) {
				$all = [];

				$all = array_merge($all, $results->response);
				for ($i = 2; $i <= $results->pagination->pages; $i++) {
					$response2 = $zype->listPlaylists(array('page'=>$i,'per_page'=>$per_page));
					$all = array_merge($all, $response2->response);
				}
			}

			$this->view->items = $all;

			$this->view->file_name = 'zype_playlists';

			$this->render('csvexport-zype-playlists');
		}

		$this->view->response = $results->response;

		$paginator_fix = array_fill(0, $results->pagination->pages * $per_page, '');

		$paginator = Zend_Paginator::factory($paginator_fix);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage($per_page);
		$this->view->list = $paginator;
	}

	public function zypePlaylistEditAction()
	{
		$zype = new Application_Model_Zype();

		$this->view->placeholder('page_title')->set("Videos");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform">Video </a></li>
		li><a href="/admin/video-platform/zype-playlist-list">Categories (Zype Playlists) </a></li><li class="active">Edit Category (Zype Playlist)</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id)){

			$form = new Admin_Form_ZypePlaylist();
			$item = $zype->getPlaylist($id);

			$this->view->playlist_title = $item->title;

			$data = [
				'id' => $item->_id,
				'title' => $item->title,
				'friendly_title' => $item->friendly_title,
				'description' => $item->description,
				'keywords' => implode(", ",$item->keywords),
				'purchase_required' => ($item->purchase_required)?"On":"Off",
				'purchase_price' => $item->purchase_price,
				'rental_required' => ($item->rental_required)?"On":"Off",
				'rental_price' => $item->rental_price,
				'rental_duration' => $item->rental_duration,
				'active' => ($item->active)?"On":"Off",
			];

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('zype-playlist-form');
		}
	}

	public function zypePlaylistSaveAction()
	{
		$zype = new Application_Model_Zype();

		$this->view->errors = array();

		$form = new Admin_Form_ZypePlaylist();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$result = $zype->updatePlaylist($data['id'], [
				'title' => $data['title'],
				'friendly_title' => $data['friendly_title'],
				'description' => $data['description'],
				'keywords' => explode(",",$data['keywords']),
				'purchase_required' => ($data['purchase_required'] == 'On')?true:false,
				'purchase_price' => $data['purchase_price'],
				'rental_required' => ($data['rental_required'] == 'On')?true:false,
				'rental_price' => $data['rental_price'],
				'rental_duration' => $data['rental_duration'],
				'active' => ($data['active'] == 'On')?true:false,
			]);

			$this->_flashMessenger->setNamespace('info')->addMessage("Playlist Updated");

			$this->getResponse()->setRedirect("/admin/video-platform/zype-playlist-list");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('zype-playlist-form');
		}
	}

	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'video_platform', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled',
		);

		$this->view->transcode_statuses = $this->_transcode_statuses = array(
			'Processing' => 'Processing',
			'Complete' => 'Complete',
			'Error' => 'Error',
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');

		$this->view->standard_attributes = $this->_standard_attributes = $options['lvp']['standard_attributes'];

	}

	public function getJobDetailsAction()
	{
		$job_id = $this->getRequest()->getParam('job_id','');

		$s3 = new Application_Model_AmazonS3();
		$result = $s3->jobStatus($job_id);

		Cny_Debug::prettyPrint($result, true, true);
	}

	public function testAction()
	{
		$job_id = $this->getRequest()->getParam('job_id','');

		$s3 = new Application_Model_AmazonS3();
		$result = $s3->jobStatus($job_id);

		//$result = $s3->transcode(11);

		Cny_Debug::prettyPrint($result, true, true);

		exit;
	}

}
