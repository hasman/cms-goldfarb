<?php
use Cocur\Slugify\Slugify;

class Admin_FaqController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$mapper = new Admin_Model_Mapper_FaqSection();
		$this->view->sections = $mapper->fetchOrdered();
	}

	public function sectionSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_FaqSection();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function itemSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Admin_Model_Mapper_FaqItem();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function sectionListAction()
	{
		$this->view->placeholder('page_title')->set("FAQ Categories");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li><li class="active">FAQ Categories</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','sort_order');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_FaqSection();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'faq_sections';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function sectionAddAction()
	{
		$this->view->placeholder('page_title')->set("FAQ Sections");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li>
				<li class="active">Add FAQ Category</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_FaqSection();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('section-form');
	}

	public function sectionEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("FAQ Sections");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li>
				<li class="active">Edit FAQ Category: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_FaqSection();
			$mapper = new Admin_Model_Mapper_FaqSection();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('section-form');
		}else{
			$this->getResponse()->setRedirect('/admin/faq/section-list');
		}
	}

	public function sectionViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_FaqSection();
			$this->view->section = $mapper->find($id);

			$this->view->placeholder('page_title')->set("FAQ Sections");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li>
				<li class="active">View FAQ Category: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			//questions in section
			$mapper = new Admin_Model_Mapper_FaqItem();
			$this->view->items = $mapper->fetchBySectionId($id);
		}else{
			$this->getResponse()->setRedirect('/admin/faq/section-list');
		}
	}

	public function sectionSaveAction()
	{
		$form = new Admin_Form_FaqSection();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_FaqSection();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($this->getRequest()->getParams())) {
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}

			$entity = new Application_Model_FaqSection($data);
			$id = $mapper->save($entity);

			$this->getResponse()->setRedirect('/admin/faq/section-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('section-form');
		}
	}

	public function sectionDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_FaqSection();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/faq/section-list');
	}

	public function itemListAction()
	{
		$this->view->placeholder('page_title')->set("FAQ Items");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li><li class="active">FAQ Items</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','sort_order');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');
		$this->view->section_id = $section_id = $this->_getParam('section_id','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha,
			'section_id' => $section_id
		);
		$mapper = new Admin_Model_Mapper_FaqItem();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'faq_items';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function itemAddAction()
	{
		$this->view->placeholder('page_title')->set("FAQ Items");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li>
				<li class="active">Add FAQ Item</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_FaqItem();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('item-form');
	}

	public function itemEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("FAQ Items");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li>
				<li class="active">Edit FAQ Item: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_FaqItem();
			$mapper = new Admin_Model_Mapper_FaqItem();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('item-form');
		}else{
			$this->getResponse()->setRedirect('/admin/faq/item-list');
		}
	}

	public function itemViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_FaqItem();
			$this->view->item = $mapper->find($id);

			$this->view->placeholder('page_title')->set("FAQ Items");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li>
				<li class="active">View FAQ Item: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);
		}else{
			$this->getResponse()->setRedirect('/admin/faq/item-list');
		}
	}

	public function itemSaveAction()
	{
		$form = new Admin_Form_FaqItem();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_FaqItem();

		if( $form->isValid($this->getRequest()->getParams())) {
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}

			$entity = new Application_Model_FaqItem($data);
			$id = $mapper->save($entity);

			$this->getResponse()->setRedirect('/admin/faq/item-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('item-form');
		}
	}

	public function itemDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_FaqItem();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/faq/item-list');
	}

	public function submissionListAction()
	{
		$this->view->placeholder('page_title')->set("FAQ Submissions");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li><li class="active">FAQ Submissions</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','created');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');
		$this->view->section_id = $section_id = $this->_getParam('section_id','');
		$this->view->is_spam = $is_spam = $this->_getParam('is_spam','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha,
			'section_id' => $section_id,
			'is_spam' => $is_spam,
		);
		$mapper = new Admin_Model_Mapper_FaqSubmission();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'faq_submissions';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function submissionEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("FAQ Submissions");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li>
				<li class="active">Edit FAQ Submission: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_FaqSubmission();
			$mapper = new Admin_Model_Mapper_FaqSubmission();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('submission-form');
		}else{
			$this->getResponse()->setRedirect('/admin/faq/submission-list');
		}
	}

	public function submissionViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_FaqSubmission();
			$this->view->item = $mapper->find($id);

			$this->view->placeholder('page_title')->set("FAQ Submissions");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/faq">FAQ</a></li>
				<li class="active">View FAQ Submission: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);
		}else{
			$this->getResponse()->setRedirect('/admin/faq/submission-list');
		}
	}

	public function submissionSaveAction()
	{
		$form = new Admin_Form_FaqSubmission();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_FaqSubmission();

		if( $form->isValid($this->getRequest()->getParams())) {
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}

			$id = $data['id'];
			if(!empty($id) && is_numeric($id)) {
				$entity = $mapper->find($id);

				$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
				if ($settings_mapper->getValueByCode('akismet_api_key')) {
					$akismet = new Application_Model_Akismet();

					$verify_data = array(
						'user_ip' => $entity->remote_addr,
						'referrer' => $entity->referrer,
						'comment_type' => 'question',
						'comment_author' => $entity->name,
						'comment_author_email' => $entity->email,
						'comment_content' => $entity->question,
						'comment_date_gmt' => date("c", strtotime($entity->created))
					);

					if ($data['is_spam'] == 'Yes' && $entity->is_spam == 'No') {
						$akismet->submitSpam($verify_data);
					}elseif($data['is_spam'] == 'No' && $entity->is_spam == 'Yes') {
						$akismet->submitHam($verify_data);
					}
				}

				//if publish copy to FAQ Item
				if ($data['status'] == 'Published' && $entity->status != 'Published' && $data['answer']) {
					$fi_mapper = new Admin_Model_Mapper_FaqItem();

					$fi_mapper->save(new Application_Model_FaqItem(array(
						'faq_section_id' => $data['faq_section_id'],
						'question' => $data['question'],
						'answer' => $data['answer'],
						'sort_order' => 9999,
						'status' => 'Enabled'
					)));
				}

				//email response
				if ($data['status'] != 'Pending' && $entity->status == 'Pending' && $data['answer']) {
					$mail_body = "<center><h3>Thank You For Your Question</h3></center>
						<p><strong>Question:</strong><br/> {$data['question']}</p>
						<p><strong>Answer:</strong><br/> {$data['answer']}</p>
					";

					$emails = new Application_Model_Emails();
					$emails->generic($mail_body, $data['email'], ' FAQ Submission Response', '');
				}

				$mapper->save(new Application_Model_FaqSubmission($data));
			}

			$this->getResponse()->setRedirect('/admin/faq/submission-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$form->populate($data);
			$this->view->form = $form;

			$this->render('submission-form');
		}
	}

	public function submissionDeleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_FaqSubmission();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/faq/submission-list');
	}


	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'cms', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled',
		);

		$this->view->submission_statuses = $this->_submission_statuses = array(
			'Pending' => 'Pending',
			'Published' => 'Published',
			'Private' => 'Private'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
