<?php
class Admin_CustomerController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/customer/list');
	}

	public function listAction()
	{
		ini_set('memory_limit','256M');

		$this->view->placeholder('page_title')->set("Customers");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Customers</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','c.id');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_Customer();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'customers';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function addAction()
	{
		$this->view->placeholder('page_title')->set("Customers");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/customer">Customers</a></li>
				<li class="active">Add Customer</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_Customer();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('add-form');
	}

	public function editAction()
	{
		$this->view->customer_id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Customers");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/customer">Customers</a></li>
				<li class="active">Edit Customer: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_Customer();
			$mapper = new Admin_Model_Mapper_Customer();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('form');
		}else{
			$this->getResponse()->setRedirect('/admin/customer/list');
		}
	}

	public function viewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Customer();
			$this->view->customer = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Customers");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/customer">Customers</a></li>
				<li class="active">View Customer: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$mapper = new Admin_Model_Mapper_CustomerProfile();
			$this->view->profile = $mapper->getProfileByCustomerId($id);

			$mapper = new Admin_Model_Mapper_CustomerAddress();
			$this->view->addresses = $mapper->findByCustomerId($id);

			//orders
			$page = $this->getRequest()->getParam('page',1);
			$this->view->dir = $dir = $this->_getParam('dir','DESC');
			$this->view->sort = $sort = $this->_getParam('sort','created');

			$search_keys = array(
				'customer_id' => $id
			);
			$mapper = new Admin_Model_Mapper_Order();
			$results = $mapper->listSearch($sort, $dir, $search_keys);

			$paginator = Zend_Paginator::factory($results);
			$paginator->setCurrentPageNumber($page);
			$paginator->setItemCountPerPage(25);
			$this->view->orders = $paginator;

			$mapper = new Admin_Model_Mapper_CustomerWishlist();
			$this->view->wishlist = $mapper->fetchByCustomerId($id);

			//entitlement
			$page = $this->getRequest()->getParam('page',1);
			$this->view->dir = $dir = $this->_getParam('dir','DESC');
			$this->view->sort = $sort = $this->_getParam('sort','po.created');

			$search_keys = array(
				'customer_id' => $id
			);

			$mapper = new Admin_Model_Mapper_VideoPlatformEntitlement();
			$results = $mapper->fetchByCustomerId($id);

			$paginator = Zend_Paginator::factory($results);
			$paginator->setCurrentPageNumber($page);
			$paginator->setItemCountPerPage(25);
			$this->view->entitlement = $paginator;
		}else{
			$this->getResponse()->setRedirect('/admin/customer/');
		}
	}

	public function saveAction()
	{
		$form = new Admin_Form_Customer();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_Customer();

		if( $form->isValid($this->getRequest()->getParams())){
			if (isset($data['merge_id']) && $data['merge_id']) {
				$this->_db->update($this->_table_prefix."orders",array("customer_id"=>$data['merge_id']),"customer_id = '{$data['id']}' AND customer_id > 0");
			}

			if ($data['new_password']) {
				//zype integration
				$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
				if ($settings_mapper->getValueByCode('zype_api_key_admin') != '' || $settings_mapper->getValueByCode('zype_api_key_read_only') != '') {
					$zype = new Application_Model_Zype();

					$response = $zype->getConsumerByEmail($data['email']);
					if ($response && $response->_id) {
						$response = $zype->updateConsumer($response->_id, array(
							'password' => $data['new_password'],
							'password_confirmation' => $data['new_password']
						));
					}
				}

				$bcrypt = new Jr_Crypt_Password_Bcrypt();
				$password = $bcrypt->create($data['new_password']);
				$data['password'] = $password;

				unset($data['new_password']);
			}

			$entity = new Application_Model_Customer($data);
			$id = $mapper->save($entity);

			$this->getResponse()->setRedirect('/admin/customer/list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$this->view->customer_id = $data['id'];

			$form->populate($data);
			$this->view->form = $form;

			if ($data['cmd'] == 'add') {
				$this->render('add-form');
			}else {
				$this->render('form');
			}
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Customer();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/customer/list');
	}

	public function addressViewAction()
	{
		$this->view->customer_id = $customer_id = $this->getRequest()->getParam('customer_id', 0);
		if(!empty($customer_id) && is_numeric($customer_id)) {
			$mapper = new Admin_Model_Mapper_CustomerAddress();
			$this->view->addresses = $mapper->findByCustomerId($customer_id);
		}
	}

	public function addressAddAction()
	{
		$customer_id = $this->getRequest()->getParam('customer_id', 0);
		if(!empty($customer_id) && is_numeric($customer_id)){

			$form = new Admin_Form_CustomerAddress();

			$data = array(
				'customer_id' => $customer_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('address-form');
		}
	}

	public function addressEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_CustomerAddress();
			$mapper = new Admin_Model_Mapper_CustomerAddress();
			$item = $mapper->find($id);

			$form->populate($item->toArray());
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('address-form');
		}
	}

	public function addressSaveAction()
	{
		$form = new Admin_Form_CustomerAddress();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			if ($data['default_address'] == 'Yes' && $data['customer_id']) {
				$this->_db->update($this->_table_prefix.'customer_addresses',array('default_address'=>'No'), "customer_id = '{$data['customer_id']}' AND address_type = '{$data['address_type']}' ");
			}

			$mapper = new Admin_Model_Mapper_CustomerAddress();
			$item = new Application_Model_CustomerAddress($data);
			$mapper->save($item);

			$this->getResponse()->setRedirect("/admin/customer/address-view/customer_id/{$data['customer_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('address-form');
		}
	}

	public function addressDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$customer_id = $this->getRequest()->getParam('customer_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_CustomerAddress();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/customer/address-view/customer_id/{$customer_id}");
	}

	public function profileViewAction()
	{
		$this->view->customer_id = $customer_id = $this->getRequest()->getParam('customer_id', 0);
		if(!empty($customer_id) && is_numeric($customer_id)){
			$mapper = new Admin_Model_Mapper_CustomerProfile();
			$this->view->profile = $mapper->getProfileByCustomerId($customer_id);
		}
	}

	public function profileAddAction()
	{
		$customer_id = $this->getRequest()->getParam('customer_id', 0);
		if(!empty($customer_id) && is_numeric($customer_id)){

			$form = new Admin_Form_CustomerProfile();

			$data = array(
				'customer_id' => $customer_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('profile-form');
		}
	}

	public function profileEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Admin_Form_CustomerProfile();
			$mapper = new Admin_Model_Mapper_CustomerProfile();
			$item = $mapper->find($id);

			$form->populate($item->toArray());
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';
			$this->view->attribute_id = $item->customer_attribute_id;

			$this->render('profile-form');
		}
	}

	public function profileSaveAction()
	{
		$form = new Admin_Form_CustomerProfile();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$mapper = new Admin_Model_Mapper_CustomerProfile();
			$item = new Application_Model_CustomerProfile($data);
			$mapper->save($item);

			//clear cache
			$cache_id = 'customer_profile_' . $data['customer_id'];
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->remove($cache_id);

			$this->getResponse()->setRedirect("/admin/customer/profile-view/customer_id/{$data['customer_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('profile-form');
		}
	}

	public function profileDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$customer_id = $this->getRequest()->getParam('customer_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_CustomerProfile();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		//clear cache
		$cache_id = 'customer_profile_' . $customer_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
		$cache->remove($cache_id);

		$this->getResponse()->setRedirect("/admin/customer/profile-view/customer_id/{$customer_id}");
	}

	public function attributeAddAction()
	{
		//
	}

	public function attributeSaveAction()
	{
		$data = $this->getRequest()->getParam('data', array());

		$mapper = new Admin_Model_Mapper_CustomerAttribute();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify($data['title']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $this->getRequest()->isPost() && trim($data['title']) ){
			$entity = new Application_Model_CustomerAttribute($data);
			$id = $mapper->save($entity);
		}

		echo "1";
		exit;
	}

	public function attributeJsonSearchAction()
	{
		$search_results = array();

		$sort = 'title';
		$dir = 'asc';

		$q = $this->getRequest()->getParam('q', '');
		$id = $this->getRequest()->getParam('id', '');

		if ($q || $id) {
			$search_keys = array(
				'keyword' => $q,
				'id' => $id
			);
			$mapper = new Admin_Model_Mapper_CustomerAttribute();
			$results = $mapper->listSearch($sort, $dir, $search_keys);

			$stmt = $this->_db->query($results->__toString());
			$search_results = $stmt->fetchAll();
		}

		$return = array();
		foreach ($search_results as $row) {
			$return[] = array(
				'id' => $row['id'],
				'title' => $row['title']
			);
		}

		echo json_encode($return);
		exit;
	}

	public function jsonSearchAction()
	{
		$search_results = array();

		$sort = 'c.created';
		$dir = 'desc';

		if ($q = $this->getRequest()->getParam('q')) {
			$search_keys = array(
				'keyword' => $q,
				'type' => null,
				'status' => 'Enabled',
				'alpha' => null
			);
			$mapper = new Admin_Model_Mapper_Customer();
			$results = $mapper->listSearch($sort, $dir, $search_keys);

			$stmt = $this->_db->query($results->__toString());
			$search_results = $stmt->fetchAll();
		}

		//$mapper = new Application_Model_Mapper_CustomerAddress();
		$return = array();
		foreach ($search_results as $row) {
			$return[] = array(
				'id' => $row['id'],
				'first_name' => $row['first_name'],
				'last_name' => $row['last_name'],
				'email' => $row['email']
			);
		}

		echo json_encode($return);
		exit;
	}

	public function loadAttributeInputAction()
	{
		$customer_id = $this->getRequest()->getParam('customer_id',0);
		$attribute_id = $this->getRequest()->getParam('attribute_id',0);
		$mapper = new Admin_Model_Mapper_CustomerAttribute();
		$attribute = $mapper->find($attribute_id);
		$field_type = (isset($attribute->field_type))?$attribute->field_type:'text';

		$item = $value = null;
		if ($customer_id) {
			$mapper = new Admin_Model_Mapper_CustomerProfile();
			$item = $mapper->getProfileByCustomerId($customer_id);
		}
		if ($item) {
			$value = $item[$attribute->code]['value'];
			$options = json_decode($item[$attribute->code]['options']);
		}

		echo $this->view->partial('/customer/partials/'.$field_type.'.phtml', array('value'=>$value,'options'=>$options));

		exit;
	}

/*
	public function blockAction()
	{
		$this->view->placeholder('page_title')->set("Customers");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/customer/list">Customers</a></li><li class="active">Block Customer Checkout</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','value');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_CustomerBlock();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'customer_blocks';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}
	public function blockSaveAction()
	{
		$mapper = new Admin_Model_Mapper_CustomerBlock();

		if( $this->getRequest()->isPost() ){
			$type = $this->getRequest()->getParam('type','');
			$value = $this->getRequest()->getParam($type,'');

			if ($type == 'address') {
				$value = json_encode($value);
			}

			$mapper->save(new Application_Model_CustomerBlock(array("type"=>$type,"value"=>$value)));
		}

		$this->getResponse()->setRedirect('/admin/customer/block');
	}
	public function blockDeleteAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_CustomerBlock();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/customer/block');
	}

	public function wishlistListAction()
	{
		$this->view->placeholder('page_title')->set("Customers");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/customer">Customers</a></li><li class="active">Customer Wishlists</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','id');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->type = $type = $this->_getParam('type','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'type' => $type,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_CustomerWishlist();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'customer_wishlists';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function wishlistViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)) {
			$mapper = new Admin_Model_Mapper_Customer();
			$this->view->customer = $mapper->find($id);

			$mapper = new Admin_Model_Mapper_CustomerWishlist();
			$this->view->wishlist = $mapper->findByCustomerId($id);
		}else {
			$this->getResponse()->setRedirect('/admin/customer/wishlist-list');
		}
	}
*/

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'ecommerce', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled'
		);


		$this->_address = array(
			'first_name' => '',
			'last_name' => '',
			'company' => '',
			'address1' => '',
			'address2' => '',
			'city' => '',
			'region' => '',
			'postcode' => '',
			'country' => '',
			'phone' => ''
		);

		$this->view->purchase_type = array(
			'purchase' => 'Purchase',
			'rent' => 'Rental',
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
