<?php
class Admin_WebsiteController extends Zend_Controller_Action
{
	/** @var  Zend_Db_Adapter_Abstract */
	protected $_db;

	public function indexAction()
	{
		$this->view->messages = $this->_flashMessenger->getMessages();
		$this->view->placeholder('page_title')->set("Section Layout");
	}

	public function loremAction()
	{
		//
	}

	public function modalColumnAction()
	{
		$this->_helper->layout()->setLayout('modal');
	}

	public function componentsAction()
	{
		//$this->_helper->layout()->setLayout('components');
	}

	public function settingsAction()
	{
		//$this->_helper->layout()->setLayout('components');
	}

	public function navListAction()
	{
		//$this->_helper->layout()->setLayout('components');
	}

	public function dashboardAction()
	{
		$this->view->messages = $this->_flashMessenger->getMessages();
		$this->view->placeholder('page_title')->set("Home");
	}

	function init()
	{
		$this->view->placeholder('section')->set("home");

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}else{
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();
		$this->view->sitename = $options['site']['name'];
	}
}
