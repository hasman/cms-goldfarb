<?php

class Admin_VideoPlatformReportController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->forward("analytics");
	}

	public function analyticsAction()
	{
		$this->view->placeholder('page_title')->set("Videos");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/video-platform-report">Video </a></li><li class="active">Video Analytics</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','title');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');
		$this->view->date_from = $date_from = $this->_getParam('date_from','');
		$this->view->date_to = $date_to = $this->_getParam('date_to','');

		$sql_date_from = $sql_date_to = '';
		if ($date_from) {
			$sql_date_from = new DateTime($date_from . ' 00:00:00', new DateTimeZone($this->view->site_timezone));
			$sql_date_from->setTimeZone(new DateTimeZone('UTC'));
			$sql_date_from = $sql_date_from->format('Y-m-d H:i:s');
		}
		if ($date_to) {
			$sql_date_to = new DateTime($date_to . ' 23:59:59', new DateTimeZone($this->view->site_timezone));
			$sql_date_to->setTimeZone(new DateTimeZone('UTC'));
			$sql_date_to = $sql_date_to->format('Y-m-d H:i:s');
		}

		$search_keys = array(
			'keyword' => $mask,
			'status' => $status,
			'alpha' => $alpha,
			'date_from' => $sql_date_from,
			'date_to' => $sql_date_to,
		);
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoAnalytic();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'video_analytics';

			$this->render('csvexport-analytics');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'video_platform', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled',
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}

}
