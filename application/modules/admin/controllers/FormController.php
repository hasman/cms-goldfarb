<?php
class Admin_FormController extends Zend_Controller_Action
{

	function indexAction()
	{
		$this->forward('list');
	}

	public function listAction()
	{
		$this->view->placeholder('page_title')->set("Form Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li class="active">Form Manager</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','created');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->name = $name = $this->_getParam('name','');

		$search_keys = array(
			'keyword' => $mask,
			'title' => $name,
		);
		$mapper = new Admin_Model_Mapper_FormSubmission();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'form_submissions';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_FormSubmission();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/form/list');
	}

	public function viewAction()
	{
		$this->view->placeholder('page_title')->set("Form Manager");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/form/list">Form Manager</a></li><li class="active">View Submission</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_FormSubmission();
			$entity = $mapper->find($id);
			$this->view->submission = $entity->toArray();
		}else{
			$this->getResponse()->setRedirect('/admin/form/list');
		}
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'cms', $this->getRequest()->getActionName());
		if($acl_status){
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		}else{
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap');

		$options = $bootstrap->getOptions();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

	}
}
