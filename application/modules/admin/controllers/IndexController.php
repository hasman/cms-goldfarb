<?php
class Admin_IndexController extends Zend_Controller_Action
{
	/** @var  Zend_Db_Adapter_Abstract */
	protected $_db;
	protected $decimal_point = 2;
	protected $date_format = 'Y-m-d';
	protected $time_format = 'H:i:s';
	protected $field_data = [
		'ga:date' => ['label' => 'Date', 'type' => 'date', 'icon' => 'fas fa-globe', 'bgcolor' => '#FB9678'],
		'ga:sessions' => ['label' => 'User Sessions', 'type' => 'int', 'icon' => 'fas fa-signal', 'bgcolor' => '#9575CD'],
		'ga:users' => ['label' => 'Users', 'type' => 'int', 'icon' => 'fas fa-users', 'bgcolor' => '#fb9678'],
		'ga:pageviews' => ['label' => 'Page Views', 'type' => 'int', 'icon' => 'fas fa-laptop', 'bgcolor' => '#9675ce'],
		'ga:pageviewsPerSession' => ['label' => 'Page Views Per Sessions', 'type' => 'decimal', 'icon' => 'fas fa-eye', 'bgcolor' => '#fec107'],
		'ga:avgSessionDuration' => ['label' => 'Avg. Session Duration', 'type' => 'time', 'icon' => 'fas fa-clock-o', 'bgcolor' => '#7be462'],
		'ga:bounceRate' => ['label' => 'Bounce Rate', 'type' => 'percent', 'icon' => 'fas fa-bar-chart', 'bgcolor' => '#abacab'],
		'ga:percentNewSessions' => ['label' => '% New Sessions', 'type' => 'percent', 'icon' => 'far fa-user-circle', 'bgcolor' => '#01c0c8']
	];

	public function indexAction()
	{
		//if they are a website user forward to the welcome screen
		if (array_key_exists($this->view->user->role, $this->_website_member_roles)) {
			if ($this->view->user->role == 'video_member') {
				$this->forward("video-member");
			}else {
				$this->forward("welcome");
			}
		}

		$this->view->messages = $this->_flashMessenger->getMessages();
		$this->view->placeholder('page_title')->set("Dashboard");
		$startdate = $this->getRequest()->getParam('startdate', date('Y-m-d', strtotime('-7 days')));
		$enddate = $this->getRequest()->getParam('enddate', date('Y-m-d', strtotime('today')));

		$this->view->startdate = $startdate;
		$this->view->enddate = $enddate;

		$site_settings = new Application_Model_Mapper_SiteSettingValue();
		$default_view_id = $site_settings->getValueByCode('dashboard_view_id'); //161290111
		$this->view->has_analytics = empty($default_view_id)?false:true;

		$this->view->reports = [
			'ga:users', 'ga:pageviews', 'ga:pageviewsPerSession',
			'ga:avgSessionDuration', 'ga:bounceRate', 'ga:percentNewSessions'
		];
		$this->view->dashboard_widgets = [];
		if ($this->view->has_analytics) {
			try {
				$svc = new Application_Service_GoogleAnalytics();

				$result = $svc->report($startdate, $enddate, [], $this->view->reports, [], [], []);

				if ($result) {
					$data = $result->getDataTable()->toSimpleObject();
					$cols = $data->cols;
					if ($result->totalResults > 0) {
						$rows = null;
						if (isset($data->rows)) {
							$rows = $data->rows[0];
						}
						foreach ($cols as $index => $col) {
							if (isset($this->field_data[$col->id])) {
								$tmp = $this->field_data[$col->id];
								if (!empty($rows)) {
									$tmp['data']['count'] = $this->formatResult($rows->c[$index]->v, $tmp['type']);
								} else {
									$tmp['data']['count'] = $this->formatResult(0, $tmp['type']);
								}
							}
							$this->view->dashboard_widgets[$index] = $tmp;
						}
					}
				}
			}catch (Exception $e) {
				//echo 'Caught exception: ',  $e->getMessage(), "\n";

				$this->view->has_analytics = false;
			}
		}

		//sales chart date ranges
		$this->view->date_range = $date_range = $this->getRequest()->getParam("date_range","month_to_date");
		$this->view->date_from = $date_from = $this->getRequest()->getParam("date_from","");
		$this->view->date_to = $date_to = $this->getRequest()->getParam("date_to","");

		$sql_date_from = $sql_date_to = '';
		$this->view->chart_title = '';
		switch($date_range){
			case 'month_to_date':
				$this->view->chart_title = 'Month to Date';
				$date_from = date("Y-m-01");
				break;
			case 'previous_month':
				$this->view->chart_title = 'Previous Month';
				$date_from = date("Y-m-01", strtotime("last month"));
				$date_to = date("Y-m-t", strtotime(date("Y-m-01")) -1);
				break;
			case 'year_to_date':
				$this->view->chart_title = 'Year to Date';
				$date_from = date("Y").'-01-01';
				break;
			case 'last_12_months':
				$this->view->chart_title = 'Last 12 Months';
				$date_from = date("Y-m-d", strtotime("-1 year"));
				$date_to = date("Y-m-d");
				break;
			default:
				$this->view->chart_title = '';
				if ($date_from) {
					$this->view->chart_title .= ' From '.date('n/j/Y', strtotime($date_from));
				}
				if ($date_to) {
					$this->view->chart_title .= ' To '.date('n/j/Y', strtotime($date_to));
				}

				break;
		}

		if ($date_from) {
			$sql_date_from = new DateTime($date_from . ' 00:00:00', new DateTimeZone($this->view->site_timezone));
			$sql_date_from->setTimeZone(new DateTimeZone('UTC'));
			$sql_date_from = $sql_date_from->format('Y-m-d H:i:s');
		}
		if ($date_to) {
			$sql_date_to = new DateTime($date_to . ' 23:59:59', new DateTimeZone($this->view->site_timezone));
			$sql_date_to->setTimeZone(new DateTimeZone('UTC'));
			$sql_date_to = $sql_date_to->format('Y-m-d H:i:s');
		}

		//ecommerce sales data
		$this->view->ecomm_status = $ecomm_status = $site_settings->getValueByCode('ecomm_status');
		if ($ecomm_status == 'Enabled') {
			$mapper = new Admin_Model_Mapper_Order();

			$results = null;
			$results = $mapper->fetchSales($sql_date_from, $sql_date_to);
			$sales_mtd = $results;
			$this->view->sales_mtd = $results;

			$this->view->sales_mtd_total = 0;
			foreach ($results as $sale) {
				$this->view->sales_mtd_total += $sale['amount'];
			}

			//sales per month past 12 months
			$results = $mapper->fetchSalesYear();
			$this->view->sales_year = $results;

			$this->view->sales_year_total = 0;
			foreach ($results as $sale) {
				$this->view->sales_year_total += $sale['amount'];
			}

			//top products ordered past month
			$results = $mapper->fetchProductOrder();
			$this->view->products_month = $results;


			//top products ordered all time
			$results = $mapper->fetchProductOrderAllTime();
			$this->view->products_year = $results;

			//recent orders
			$results = $mapper->recentOrder();
			$recentOrder = $results;
			$this->view->orders = $results;
		}

		//video platform data
		$this->view->video_platform_status = $video_platform_status = $site_settings->getValueByCode('video_platform_status');
		if ($video_platform_status == 'Enabled') {
			//video sales per day - month to date
			$mapper = new Admin_Model_Mapper_VideoPlatformOrder();
			$results = null;
			$results = $mapper->videoSales($sql_date_from, $sql_date_to);
			$this->view->videosales_mtd = $results;
			//$video_sales = $results;

			$this->view->videosales_mtd_total = 0;
			foreach ($results as $vsale) {
				$this->view->videosales_mtd_total += $vsale['amount'];
			}
			//Combine Order and Video Sales
			//Only if both Commerce and Viedo Platform are enabled
			if ($ecomm_status == 'Enabled') {
				// Step-1- Transform array $results
				$videosales_mtd_prime = array();
				foreach($results as $videosales) {
					$date = $videosales["period"];
					$videosales_mtd_prime[$date] = array("amount" => $videosales["amount"], "orders" => $videosales["orders"]);
				}
				// Step-2- Iterate through array $sales_mtd
				$combine_sales = array();

				foreach($sales_mtd as $sales) {
					$period = $sales["period"];
					//$admin_IdxController=new Admin_IndexController();
					//$idx = $admin_IdxController->search_in_2D_array($period, $videosales_mtd_prime);
					//TODO - Should be called from the search_in_2D_array() function
					$idx = false;
					$i = 0;
					foreach($videosales_mtd_prime as $key=>$val) {
						if($key === $period) {
							$idx = $i;
						}
						$i++;
					}

					$newRecord = null;
					if($idx !== false) {
						$newRecord = array("period" => $period, "amount" => $videosales_mtd_prime[$period]["amount"] + $sales["amount"], "orders" => $videosales_mtd_prime[$period]["orders"] + $sales["orders"]);
						unset($videosales_mtd_prime[$period]);
					} else {
						$newRecord = $sales;
					}
					array_push($combine_sales, $newRecord);
				}
				// Step-3- Iterate through remaining items in $videosales_mtd_prime
				//TODO - Modify videosales_mtd_prime to use array_merge instead of the foreach loop like this: $combine_sales=array_merge($combine_sales,$videosales_mtd_prime);
				foreach($videosales_mtd_prime as $key=>$value) {
					array_push($combine_sales, array("period" => $key, "amount" => $value["amount"], "orders" => $value["orders"]));
				}

				//Prep combine_sales data for the view
				$this->view->combine_mtd = $combine_sales;
				//Combine order + Video sales total
				$this->view->combine_sales_total = 0;
				foreach ($combine_sales as $csale) {
					$this->view->combine_sales_total += $csale['amount'];
				}
			}

			//Get the top 10 transaction with unformated period
			$vResults = $mapper->videoSalesTop10();

			//Merge both Orders and Transactions arrays
			$tempArray = array_merge($vResults->toArray(), ($recentOrder)?$recentOrder->toArray():array());
			//var_dump($tempArray);exit;
			//Sort the $tempArray by DESC
			uasort($tempArray, function($a,$b){
				return strcmp($b['created'], $a['created']);
			});
			//Get a chunk of 10 entries per array
			$chunk = array_chunk($tempArray,10);
			//Override the $this->view->orders = $results;
			$this->view->orders = $chunk[0];
		}
	}

/*	public function search_in_2D_array($needle, $haystack) {
		$i = 0;
		foreach($haystack as $key=>$val) {
			if($key === $needle) {
				return $i;
			}
			$i++;
		}

		return false;
	}*/

	function formatResult($data, $type)
	{
		$output = $data;
		if (!empty($data)) {
			switch ($type) {
				case 'date':
					$output = date($this->date_format, strtotime($data));
					break;
				case 'time':
					$output = gmdate($this->time_format, $data);
					break;
				case 'datetime':
					$output = date($this->date_format.' '.$this->time_format, strtotime($data));
					break;
				case 'decimal':
					$output = number_format($data, $this->decimal_point);
					break;
				case 'percent':
					$output = number_format($data, 0).'%';
					break;
			}
		}
		return $output;
	}

	public function welcomeAction()
	{
		//
	}

	public function videoMemberAction()
	{
		$form = new Admin_Form_VideoPlatformVideo();
		$form->getElement('cmd')->setValue('add');
		$form->getElement('admin_user_id')->setValue($this->view->user->id);

		$form->removeElement('code');

		$this->view->cmd = 'add';
		$this->view->form = $form;
	}

	function init()
	{
		$this->view->placeholder('section')->set("home");

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}else{
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		}

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();
		$this->view->sitename = $options['site']['name'];
		$this->view->service_account = $options['google']['service_account'];

		$this->view->date_ranges = array(
			"month_to_date"=>"Month To Date",
			'previous_month' => 'Previous Month',
			'year_to_date' => 'Year To Date',
			'last_12_months' => 'Last 12 Months',
			'custom_date_range' => 'Custom Date Range'
		);

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->admin_acl_roles = $this->_admin_acl_roles = Zend_Registry::get('admin_acl_roles');
		$this->_roles = array(
			'admin' => 'Admin',
			//'guest' => 'Guest'
		);
		$this->_website_member_roles = [];
		foreach ($this->_admin_acl_roles as $role_id => $role) {
			if (!$role['is_website_member']) {
				$this->_roles[$role_id] = $role['name'];
			}else {
				$this->_website_member_roles[$role_id] = $role['name'];
			}
		}
		$this->view->roles = $this->_roles;
		$this->view->website_member_roles = $this->_website_member_roles;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');

	}
}
