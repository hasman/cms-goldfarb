<?php
use Cocur\Slugify\Slugify;

class Admin_CollectionController extends Zend_Controller_Action
{
	public $_db;

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/collection/list');
	}

	public function listAction()
	{
		$this->view->placeholder('page_title')->set("Collections");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Collections</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','name');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->status = $status = $this->_getParam('status','');
		$this->view->alpha = $alpha = $this->_getParam('alpha','');

		$search_keys = array(
			'keyword' => $mask,
			'status' => $status,
			'alpha' => $alpha
		);
		$mapper = new Admin_Model_Mapper_Collection();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		if($this->_getParam("csv",0) == 1) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($results->__toString());
			$this->view->items = $stmt->fetchAll();

			$this->view->file_name = 'collections';

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;

	}

	public function addAction()
	{
		$this->view->placeholder('page_title')->set("Collections");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/collection">Collections</a></li>
				<li class="active">Add Collection</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Admin_Form_Collection();
		$form->getElement('cmd')->setValue('add');

		$this->view->cmd = 'add';
		$this->view->form = $form;
		$this->render('form');
	}

	public function editAction()
	{
		$this->view->collection_id = $id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$this->view->placeholder('page_title')->set("Collections");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/collection">Collections</a></li>
				<li class="active">Edit Collection: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			$form = new Admin_Form_Collection();
			$mapper = new Admin_Model_Mapper_Collection();
			$entity = $mapper->find($id);

			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('form');
		}else{
			$this->getResponse()->setRedirect('/admin/collection/list');
		}
	}

	public function viewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Collection();
			$this->view->collection = $mapper->find($id);

			$this->view->placeholder('page_title')->set("Collections");
			$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/collection">Collections</a></li>
				<li class="active">View Collection: '.$id.'</li>';
			$this->view->placeholder('breadcrumb')->set($breadcrumb);

			//products in collection
			$mapper = new Admin_Model_Mapper_ProductCollection();
			$this->view->products = $mapper->fetchProductsByCollectionId($id);
		}else{
			$this->getResponse()->setRedirect('/admin/collection/');
		}
	}

	public function saveAction()
	{
		$form = new Admin_Form_Collection();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		$mapper = new Admin_Model_Mapper_Collection();

		$slugify = new Slugify();
		$code = $orig_code = $slugify->slugify(($data['code']) ? $data['code'] : $data['name']);
		$counter = 1;
		while ($mapper->doesExists(array('code' => $code), $data['id']) !== false) {
			$code = $orig_code . "-" . $counter;
			$counter++;
		}
		$data['code'] = $code;

		if( $form->isValid($this->getRequest()->getParams())) {
			if (!$data['sort_order']) $data['sort_order'] = 9999;
			foreach ($data as $k => $v) {
				if ($this->isempty($v)) {
					$data[$k] = new Zend_Db_Expr("NULL");
				}
			}

			$entity = new Application_Model_Collection($data);
			$id = $mapper->save($entity);

			/*
			$nav_mapper = new Admin_Model_Mapper_NavigationItem();
			$nav_entity = $nav_mapper->findByCollectionId($id);

			if ($data['nav_display'] == 'Enabled') {
				if (!$nav_entity) {
					$nav_entity = new Application_Model_NavigationItem(array(
						'title' => $data['name'],
						'url' => "/collection/" . $data['code'],
						'collection_id' => $id,
						'main_nav_status' => 'Enabled',
						'status' => 'Disabled',
					));
				}
				$nav_mapper->save($nav_entity);
			}elseif ($nav_entity) {
				$nav_mapper->delete($nav_entity);
			}

			//delete cache
			$cache_id = 'navigation';
			$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
			$cache->clean(
				Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
				array('navigation')
			);
			$cache->remove($cache_id);
			*/

			$this->getResponse()->setRedirect('/admin/collection/list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);

			$this->view->collection_id = $data['id'];

			$form->populate($data);
			$this->view->form = $form;

			if ($data['cmd'] == 'add') {
				$this->render('add-form');
			}else {
				$this->render('form');
			}
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_Collection();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/collection/list');
	}

	public function productViewAction()
	{
		$this->view->collection_id = $collection_id = $this->getRequest()->getParam('collection_id', 0);

		$mapper = new Admin_Model_Mapper_ProductCollection();
		$this->view->products = $mapper->fetchProductsByCollectionId($collection_id);

		$mapper = new Admin_Model_Mapper_ProductTags();
		$this->view->product_tags = $mapper->fetchTagNamesByCollectionId($collection_id);
	}

	public function productAddAction()
	{
		$this->view->collection_id = $collection_id = $this->getRequest()->getParam('collection_id', 0);
		if(!empty($collection_id) && is_numeric($collection_id)){
			$this->view->cmd = 'add';

			//$mapper = new Admin_Model_Mapper_ProductTags();
			//$this->view->product_tags = $mapper->fetchTagNamesByCollectionId($collection_id);

			$this->render('product-form');
		}
	}

	public function productSaveAction()
	{
		if( $this->getRequest()->isPost() ) {
			$data = $this->getRequest()->getParam('data', array());

			$pc_mapper = new Admin_Model_Mapper_ProductCollection();

			//tags
			$t_mapper = new Admin_Model_Mapper_ProductTags();
			$tc_mapper = new Admin_Model_Mapper_ProductTag();

			//$old_tags = $t_mapper->fetchTagNamesByCollectionId($data['collection_id']);

			$tags = explode("|",$data['product_tags']);
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if ($entity) {
						$tag_id = $entity->id;

						$products = $tc_mapper->fetchProductsByTagId($tag_id);

						foreach ($products as $product) {
							$prod = new Application_Model_ProductCollection(
								array('collection_id' => $data['collection_id'], 'product_id' => $product->id)
							);
							$pc_mapper->save($prod);
						}
					}
				}
			}

			//individual products
			$products = explode("|",$data['products']);
			if ($products) {
				foreach ($products as $k => $product_string) {
					$pattern = "/<span ?.*>(.*)<\/span>/";
					preg_match($pattern, $product_string, $matches);
					if (count($matches) > 1) {
						$product_id = $variant_id = '';
						$split = explode("~~", $matches[1]);
						if ($split[0] == 'product') {
							$product_id = $split[1];
						}elseif ($split[0] == 'variant') {
							$variant_id = $split[1];
						}

						if ($product_id || $variant_id) {
							$prod = new Application_Model_ProductCollection(
								array('collection_id' => $data['collection_id'], 'product_id' => $product_id)
							);
							$pc_mapper->save($prod);
						}
					}
				}
			}

			$this->getResponse()->setRedirect("/admin/collection/product-view/collection_id/{$data['collection_id']}");
		}else{
			$this->view->cmd = 'add';
			$this->render('product-form');
		}
	}

	public function productDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$collection_id = $this->getRequest()->getParam('collection_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Admin_Model_Mapper_ProductCollection();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/collection/product-view/collection_id/{$collection_id}");
	}

	public function productDeleteTagAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$collection_id = $this->getRequest()->getParam('collection_id', 0);
		$tag_id = $this->getRequest()->getParam('tag_id', 0);
		if(!empty($collection_id) && is_numeric($collection_id) && !empty($tag_id) && is_numeric($tag_id)){
			$mapper = new Admin_Model_Mapper_ProductCollection();
			$mapper->deleteByCollectionIdTagId($collection_id, $tag_id);
		}

		$this->redirect("/admin/collection/edit/id/{$collection_id}");
	}

	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'ecommerce', 'view');
		if ($acl_status) {
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper

		$options = $bootstrap->getOptions();
		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->statuses = $this->_statuses = array(
			'Enabled' => 'Enabled',
			'Disabled' => 'Disabled'
		);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
