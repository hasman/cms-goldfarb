<?php
class Admin_CssController extends Zend_Controller_Action
{
	/** @var  Zend_Db_Adapter_Abstract */
	protected $_db;

	public function fontsAction()
	{
		header("Content-type: text/css");
		//header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (60 * 60 * 24 * 365))); // 1 year

		$mapper = new Admin_Model_Mapper_Font();
		$fonts = $mapper->getActiveFonts();

		foreach ($fonts as $font) {
			if (stripos($font['url'],"http://") !== false || stripos($font['url'],"https://") !== false) {
				echo '@import url(\'' . $font['url'] . '\');' . "\n";
			}else {
				if ($font['url']) {
					echo '@font-face {' . "\n".
						'font-family: \''.$font['title'].'\';' . "\n".
						'src: url(\''.$font['url'].'\');' . "\n".
						'font-weight: normal;' . "\n".
						'font-style: normal;' . "\n".
						'font-display: swap;' . "\n".
						'}' . "\n";
				}
			}
		}

		exit;
	}

	function init()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}
}
