<?php
class Custom_Model_GfRegion extends Application_Model_Abstract
{
	public $code;
	public $name;
	public $sub_title;
	public $image;
	public $phone;
	public $email;
	public $hours;
	public $apply_link;
	public $status;
	public $sort_order;
}
