<?php
class Custom_Model_Mapper_GfBuildingAmenity extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfBuildingAmenity');
		$this->setEntity('Custom_Model_GfBuildingAmenity');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

}
