<?php
class Custom_Model_Mapper_GfUnitFeature extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfUnitFeature');
		$this->setEntity('Custom_Model_GfUnitFeature');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

}
