<?php
class Custom_Model_Mapper_GfCoi extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfCoi');
		$this->setEntity('Custom_Model_GfCoi');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}
	public function getAllCoi()
	{
		$select = $this->getDbTable()->select();
		$result = $this->fetchAll($select);
		return $result;
	}	

}