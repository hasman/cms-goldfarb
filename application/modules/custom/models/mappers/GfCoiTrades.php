<?php
class Custom_Model_Mapper_GfCoiTrades extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfCoiTrades');
		$this->setEntity('Custom_Model_GfCoiTrades');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}
	public function getAllCoiTrades()
	{
		$select = $this->getDbTable()->select()->order('name','ASC');
		$result = $this->fetchAll($select);
		return $result;
	}	

}