<?php
class Custom_Model_Mapper_GfBuilding extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfBuilding');
		$this->setEntity('Custom_Model_GfBuilding');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function getAllResidentialRentals()
	{
		$select = $this->getDbTable()->select()
			->where('residential LIKE ?', '%"Rentals"%')
		;
		$result = $this->fetchAll($select);

		return $result;
	}

	public function validateByCodeAndRegionId($code, $region_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('b'=>$this->_table_prefix.'gf_buildings'),'*')
			->from(array('n'=>$this->_table_prefix.'gf_neighborhoods'),'')
			->where('b.neighborhood_id = n.id')
			->where('b.code = ?', $code)
			->where('n.region_id = ?', $region_id)
		;

		$result = $this->fetchAll($select);

		if ($result) {
			return current($result);
		} else {
			return false;
		}
	}

	public function fetchByRegionId($region_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('b'=>$this->_table_prefix.'gf_buildings'),'*')
			->from(array('n'=>$this->_table_prefix.'gf_neighborhoods'),'')
			->where('b.neighborhood_id = n.id')
			->where('n.region_id = ?', $region_id)
		;

		$result = $this->fetchAll($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function getSimilar($region_id, $building_id = null, $limit = 2)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('b'=>$this->_table_prefix.'gf_buildings'),'*')
			->from(array('n'=>$this->_table_prefix.'gf_neighborhoods'),'')
			->where('b.neighborhood_id = n.id')
			->where('n.region_id = ?', $region_id)
		;

		if ($building_id) {
			$select->where('b.id <> ?', $building_id);
		}

		if ($limit) {
			$select->limit($limit);
		}

		$result = $this->fetchAll($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function fetchByBuildingCodeAndRegionCode($building_code, $region_code = null, $limit = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(['b'=>$this->_table_prefix.'gf_buildings'],'b.*')
			->joinInner(['n'=>$this->_table_prefix.'gf_neighborhoods'],'b.neighborhood_id = n.id', null)
			->joinInner(['r'=>$this->_table_prefix.'gf_regions'],'n.region_id = r.id', null)
			->where('b.code = ?', $building_code)
			->where('r.code = ?', $region_code)
		;

		if ($limit) {
			$select->limit($limit);
		}

		$result = $this->fetchAll($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function fetchYardiBuildings()
	{
		$select = $this->getDbTable()->select()
			->where('yardi_code <> "" ')
		;

		$result = $this->fetchAll($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function hasVacancy($building)
	{
		if (is_numeric($building)) {
			$building = $this->find($building);
		}else {
			$entity_type = $this->getEntity();
			if (!$building instanceof $entity_type) {
				throw new Exception('Invalid entity provided');
			}
		}

		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('u'=>$this->_table_prefix.'gf_units'),'')
			->join(array('b'=>$this->_table_prefix.'gf_buildings'),'u.building_id = b.id','')
			->where('u.status = ?', 'Available')
			->columns(array('count'=>new Zend_Db_Expr('(COUNT(*))')))
		;
		$result = $this->getDbTable()->fetchRow($select);

		if ($result->count > 0) return true;

		return false;
	}
}
