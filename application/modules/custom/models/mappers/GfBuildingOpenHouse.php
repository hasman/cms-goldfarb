<?php
class Custom_Model_Mapper_GfBuildingOpenHouse extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfBuildingOpenHouse');
		$this->setEntity('Custom_Model_GfBuildingOpenHouse');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByBuildingId($building_id)
	{
		$select = $this->getDbTable()->select()
			->where('building_id = ?', $building_id)
			->where('end_time > NOW()')
			->order(array('start_time ASC'));

		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
