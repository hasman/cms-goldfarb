<?php
class Custom_Model_Mapper_GfAmenity extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfAmenity');
		$this->setEntity('Custom_Model_GfAmenity');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAmenityNamesByBuildingId($building_id, $tag_override = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('ba'=>$this->_table_prefix.'gf_building_amenities'))
			->where("ba.building_id = ?", $building_id)
			->where("a.status = ?", "Enabled")
			->joinLeft(array('a'=>$this->_table_prefix.'gf_amenities'),'ba.amenity_id = a.id',array('name','streeteasy_tag','zillow_tag','apartmentscom_tag'))
			->order(array('a.name ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = ($tag_override && isset($row->$tag_override) && $row->$tag_override)?$row->$tag_override:$row->name;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchAmenityNameTagPairsByBuildingId($building_id, $tag)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('ba'=>$this->_table_prefix.'gf_building_amenities'))
			->where("ba.building_id = ?", $building_id)
			->where("a.status = ?", "Enabled")
			->joinLeft(array('a'=>$this->_table_prefix.'gf_amenities'),'ba.amenity_id = a.id',array('name','streeteasy_tag','zillow_tag','apartmentscom_tag'))
			->order(array('a.name ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = [
					'name' => $row->name,
					'tag' => (isset($row->$tag) && !empty($row->$tag))? $row->$tag : '',
				];
			}
			return $collection;
		} else {
			return false;
		}
	}
}
