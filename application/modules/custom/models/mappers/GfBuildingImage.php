<?php
class Custom_Model_Mapper_GfBuildingImage extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfBuildingImage');
		$this->setEntity('Custom_Model_GfBuildingImage');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchOrderedByBuildingId($building_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('bi'=>$this->_table_prefix.'gf_building_images'),'')
			->joinLeft(array('i'=>$this->_table_prefix.'gf_images'),'bi.image_id = i.id','*')
			->where('bi.building_id = ?', $building_id)
			->order('bi.sort_order');
		$result = $this->getDbTable()->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}

	public function fetchPrimaryByBuildingId($building_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('bi'=>$this->_table_prefix.'gf_building_images'),'')
			->joinLeft(array('i'=>$this->_table_prefix.'gf_images'),'bi.image_id = i.id','*')
			->where('bi.building_id = ?', $building_id)
			->order('bi.sort_order')
			->limit(1);
		$result = $this->getDbTable()->fetchRow($select);

		if (!$result) return null;
		else return $result;
	}
}
