<?php
class Custom_Model_Mapper_GfRegion extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfRegion');
		$this->setEntity('Custom_Model_GfRegion');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchOrdered()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?', 'Enabled')
			->order(array('sort_order ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchUnitsByRegionId($region_id, $type = 'residential-rentals')
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('u'=>$this->_table_prefix.'gf_units'))
			->joinLeft(array('b'=>$this->_table_prefix.'gf_buildings'),'u.building_id = b.id','')
			->joinLeft(array('n'=>$this->_table_prefix.'gf_neighborhoods'),'b.neighborhood_id = n.id','')
			->where("n.region_id = ?", $region_id)
			->where("u.status = ?", "Available")
			->where("n.status = ?", "Enabled")
			->order(array('u.price DESC'));

		switch ($type) {
			case "residential":
				$select->where('u.residential <> "" AND u.residential <> "[]"');
				break;
			case "residential-rentals":
				$select->where('u.residential LIKE "%Rentals%"');
				break;
			case "commercial":
				$select->where('u.commercial <> "" AND u.commercial <> "[]"');
				break;
			case "commercial-office":
				$select->where('u.commercial LIKE "%Office%"');
				break;
			case "commercial-retail":
				$select->where('u.commercial LIKE "%Retail%"');
				break;
		}

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}
}
