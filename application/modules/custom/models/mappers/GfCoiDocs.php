<?php
class Custom_Model_Mapper_GfCoiDocs extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfCoiDocs');
		$this->setEntity('Custom_Model_GfCoiDocs');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}
	public function getAllCoiDocs()
	{
		$select = $this->getDbTable()->select();
		$result = $this->fetchAll($select);
		return $result;
	}	

}