<?php
class Custom_Model_Mapper_GfFeature extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfFeature');
		$this->setEntity('Custom_Model_GfFeature');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchFeatureNamesByUnitId($unit_id, $tag_override = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('uf'=>$this->_table_prefix.'gf_unit_features'))
			->where("uf.unit_id = ?", $unit_id)
			->joinLeft(array('f'=>$this->_table_prefix.'gf_features'),'uf.feature_id = f.id',array('name','streeteasy_tag','zillow_tag','apartmentscom_tag'))
			->order(array('f.name ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = ($tag_override && isset($row->$tag_override) && $row->$tag_override)?$row->$tag_override:$row->name;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchFeatureNameTagPairsByUnitId($unit_id, $tag = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('uf'=>$this->_table_prefix.'gf_unit_features'))
			->where("uf.unit_id = ?", $unit_id)
			->joinLeft(array('f'=>$this->_table_prefix.'gf_features'),'uf.feature_id = f.id',array('name','streeteasy_tag','zillow_tag','apartmentscom_tag'))
			->order(array('f.name ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = [
					'name' => $row->name,
					'tag' => (isset($row->$tag) && !empty($row->$tag))? $row->$tag : '',
				];
			}
			return $collection;
		} else {
			return false;
		}
	}
}
