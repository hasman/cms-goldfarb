<?php
class Custom_Model_Mapper_GfRegionAttraction extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfRegionAttraction');
		$this->setEntity('Custom_Model_GfRegionAttraction');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByRegionId($region_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('ra'=>$this->_table_prefix.'gf_region_attractions'),'')
			->joinLeft(array('a'=>$this->_table_prefix.'gf_attractions'),'ra.attraction_id = a.id','*')
			->where('ra.region_id = ?', $region_id)
			;

		$result = $this->getDbTable()->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
