<?php
class Custom_Model_Mapper_GfVirtualTour extends Application_Model_Mapper_Abstract
{
    protected $_apiKey = 'ycbp013elqjem21602526089705';
    protected $_agentId = '5f316e224f270e71ee724d0b';
    protected $_listingsEndpointUrl = 'https://developers.peek.us/api/get_listing_urls?key={%_apiKey}&agent_id={%_agentId}';
    
    public function __construct()
	{
        $this->_listingsEndpointUrl = str_replace(array('{%_apiKey}','{%_agentId}'),array($this->_apiKey,$this->_agentId),$this->_listingsEndpointUrl); 

        $options = Zend_Registry::get('configuration')->toArray();
    }

    public function __destruct()
    {
        $cache_dir = APPLICATION_PATH . "/../tmp";
        $frontendOptions = array('automatic_serialization' => true, 'lifetime' => 3600);
        $backendOptions = array('cache_dir' => $cache_dir);
        $cache_id = 'units_virtual_tours_cache';
        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        $cache->remove($cache_id);
    }
    
    public function getUnits()
    {
		$cache_dir = APPLICATION_PATH . "/../tmp";
        $frontendOptions = array('automatic_serialization' => true, 'lifetime' => 3600);
        $backendOptions = array('cache_dir' => $cache_dir);
        $cache_id = 'units_virtual_tours_cache';
        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

        #kill caching temporarily
        if (true || !($cache->test($cache_id))) {   
            $return = $cache->load($cache_id);
            $data = file_get_contents($this->_listingsEndpointUrl);

            $data = json_decode($data,true);

            $return = array();
            foreach($data['data']['listings'] as $listing) {

                $gfvt = new Custom_Model_GfVirtualTour();
                $gfvt->address = $listing['address'];
                $gfvt->unit = $listing['Unit'];
                $gfvt->postal_code = $listing['Postal Code'];
                $gfvt->vimeo_url = $listing['Vimeo URL'];
                $gfvt->virtual_tour_url = $listing['Virtual Tour URL'];
                $gfvt->date_uploaded = $listing['Date Uploaded'];

                $return[$gfvt->unit] = $gfvt;
                
            }

            $cache->save($return, $cache_id);

        }else {
            $return = $cache->load($cache_id);
        }
        
        
        return $return;
    }

    public function getUnit($id)
    {
        return ($this->getUnits())[$id] ?? array();
    }
}
