<?php
class Custom_Model_Mapper_GfUnitImage extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfUnitImage');
		$this->setEntity('Custom_Model_GfUnitImage');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchOrderedByUnitId($unit_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('ui'=>$this->_table_prefix.'gf_unit_images'),array('sort_order'))
			->joinLeft(array('i'=>$this->_table_prefix.'gf_images'),'ui.image_id = i.id','*')
			->columns(array('alt_text'=>new Zend_Db_Expr('IF (ui.alt_text = "" OR ui.alt_text IS NULL, i.alt_text, ui.alt_text)')))
			->where('ui.unit_id = ?', $unit_id)
			->order('ui.sort_order');
		$result = $this->getDbTable()->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}

	public function fetchPrimaryByUnitId($unit_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('ui'=>$this->_table_prefix.'gf_unit_images'),'')
			->joinLeft(array('i'=>$this->_table_prefix.'gf_images'),'ui.image_id = i.id','*')
			->where('ui.unit_id = ?', $unit_id)
			->order('ui.sort_order')
			->limit(1);
		$result = $this->getDbTable()->fetchRow($select);

		if (!$result) return null;
		else return $result;
	}
}
