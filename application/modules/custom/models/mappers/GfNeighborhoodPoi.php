<?php
class Custom_Model_Mapper_GfNeighborhoodPoi extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfNeighborhoodPoi');
		$this->setEntity('Custom_Model_GfNeighborhoodPoi');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByNeighborhoodId($neighborhood_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('p'=>$this->_table_prefix.'gf_pois'),'*')
			->joinLeft(array('np'=>$this->_table_prefix.'gf_neighborhood_pois'),'np.poi_id = p.id','')
			->joinLeft(array('pc'=>$this->_table_prefix.'gf_poi_categories'),'p.poi_category_id = pc.id',array('category'=>'name'))
			->where("np.neighborhood_id = ?", $neighborhood_id)
			->order(array('pc.name ASC','p.name ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[$row['category']][] = $row['name'];
			}
			return $collection;
		} else {
			return false;
		}
	}
}
