<?php
class Custom_Model_Mapper_GfAttraction extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfAttraction');
		$this->setEntity('Custom_Model_GfAttraction');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByRegionId($region_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('a'=>$this->_table_prefix.'gf_attractions'),'*')
			->joinLeft(array('ra'=>$this->_table_prefix.'gf_region_attractions'),'ra.attraction_id = a.id','')
			->where("ra.region_id = ?", $region_id)
			->order(array('a.name ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}
}
