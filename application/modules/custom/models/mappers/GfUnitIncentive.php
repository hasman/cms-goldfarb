<?php
class Custom_Model_Mapper_GfUnitIncentive extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfUnitIncentive');
		$this->setEntity('Custom_Model_GfUnitIncentive');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchNameByUnitId($unit_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('ui'=>$this->_table_prefix.'gf_unit_incentives'),'')
			->joinLeft(array('i'=>$this->_table_prefix.'gf_incentives'),'ui.incentive_id = i.id','name')
			->where('ui.unit_id = ?', $unit_id)
		;

		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			return $result->name;
		} else {
			return false;
		}
	}
}
