<?php
class Custom_Model_Mapper_GfUnit extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfUnit');
		$this->setEntity('Custom_Model_GfUnit');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function getUnitNetEffectivePrice($unit)
	{
		if (is_numeric($unit)) {
			$unit = $this->find($unit);
		}else {
			$entity_type = $this->getEntity();
			if (!$unit instanceof $entity_type) {
				throw new Exception('Invalid entity provided');
			}
		}

		if (!empty($unit->price) && !empty($unit->lease_term) && !empty($unit->concession_months) && $unit->concession_months && !empty($unit->lease_term) && $unit->concession_language) {
			return round(($unit->price * ($unit->lease_term - $unit->concession_months)) / $unit->lease_term);
		}

		return $unit->price;
	}

	public function getUnitConcessionMessage($unit)
	{
		if (is_numeric($unit)) {
			$unit = $this->find($unit);
		}else {
			$entity_type = $this->getEntity();
			if (!$unit instanceof $entity_type) {
				throw new Exception('Invalid entity provided');
			}
		}

		$expired = false;
		if($unit->concession_expiration != ''){
			$expiration = strtotime($unit->concession_expiration);
			$date = date_create();
			$current = date_timestamp_get($date);
			if($current > $expiration){
				$expired = true;
			}
		}

		if ($unit->concession_months > 0) {
			if (!$expired) {
				$effective_price = $this->getUnitNetEffectivePrice($unit);

				if ($unit->concession_language != '') {
					switch($unit->concession_language) {
						case 'A':
							return sprintf("1 month free or landlord pays 1 month broker's fee on an 12 month lease* rent is $%s net effective rent is $%s. (promotions apply to new residents and immediate move in only) " , number_format($unit->price), number_format($effective_price));
						case 'B':
							return sprintf("Your rent is $%s per month. You will receive the 2nd month free, making your net effective rent $%s (promotions apply to new residents and immediate move in only) ", number_format($unit->price), number_format($effective_price));
						case 'C':
							return sprintf("%s month(s) free on a %s month lease rent is $%s net effective rent is $%s. (promotions apply to new residents and immediate move in only) ", $unit->concession_months, $unit->lease_term, number_format($unit->price), number_format($effective_price));
						case 'D':
							return sprintf("$%s reflects a concession of %s months(s) free. Monthly rent is $%s for the lease term. (promotions apply to new residents and immediate move in only)", number_format($effective_price), $unit->concession_months, number_format($unit->price));
					}
				} else {
					if($unit->concession_months < 2 && $unit->lease_term == 12) {
						return sprintf("1 month free or landlord pays 1 month broker's fee on an 12 month lease* rent is $%s net effective rent is $%s. (promotions apply to new residents and immediate move in only) ", number_format($unit->price), number_format($effective_price));
					} else {
						return sprintf("%s month(s) free on a %s month lease rent is $%s net effective rent is $%s. (promotions apply to new residents and immediate move in only) ", $unit->concession_months, $unit->lease_term, number_format($unit->price), number_format($effective_price));
					}
				}
			}
		}
		return null;
	}

	public function convertApartmentNumber($unit_number) {
		switch(strtoupper(substr($unit_number, 0, 4))) {
			case 'COD-':
			case 'FAD-':
			case 'WES-':
			case 'RSD-':
			case 'OVA-':
			case 'CTC-':
			case 'DTC-':
			case 'FOC-':
			case 'GRC-':
			case 'REV-':
			case 'SOC-':
			case 'WAC-':
			case 'MAT-':
			case 'PPT-':
			case 'PPN-':
			case 'PPS-':
			case 'GCC-':
			case 'WPC-':
				return strtoupper(substr($unit_number, 4));
				break;
			case '315-':
			case '330-':
			case '197-':
			case '207-':
			case '217-':
			case '930-':
			case '939-':
				return strtoupper($unit_number);
				break;
		}

		switch(strtoupper(substr($unit_number, 0,1))) {
			case 'R':
			case 'M':
			case 'H':
			case 'J':
				return strtoupper(substr($unit_number, 1));
				break;
		}

		return null;
	}

	public function streeteasyApartmentNumber($unit)
	{
		if (is_numeric($unit)) {
			$unit = $this->find($unit);
		}else {
			$entity_type = $this->getEntity();
			if (!$unit instanceof $entity_type) {
				throw new Exception('Invalid entity provided');
			}
		}

		if($unit->feed_number_override){
			return $unit->feed_number_override ?: $unit->number;
		} else {
			$number = $unit->number;
			// Ignore formating if wait units
			if (strpos(strtolower($number), 'wait') !== false) {
				return $number;
			}

			return substr($number, 4);
		}
	}

	public function getFormattedBedrooms($value)
	{
		if ($value == 0) {
			$value = 'Studio';
		} elseif ($value == 1) {
			$value = $value.' '.'bed';
		} else {
			$value = $value.' '.'beds';
		}

		return $value;
	}

	public function getFormattedBathrooms($value)
	{
		if ($value == 1) {
			$value = $value.' '.'bath';
		} else {
			$value = $value.' '.'baths';
		}

		return $value;
	}

	public function validateByCodeAndBuilding($code, $building_id)
	{
		//the code comes with "apartment-" prepended to it so lets strip that
		$code = str_ireplace("apartment-","",$code);
		//commercial has unit- prepended
		$code = str_ireplace("unit-","",$code);

		$select = $this->getDbTable()->select()
			->where('code = ?', $code)
			->where('building_id = ?', $building_id)
		;

		$result = $this->fetchAll($select);

		if ($result) {
			return current($result);
		} else {
			return false;
		}
	}

	public function fetchUnitsByBuildingId($building_id)
	{
		$select = $this->getDbTable()->select()
			->where('building_id = ?', $building_id)
		;

		$result = $this->fetchAll($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function fetchAvailableUnitsByBuildingId($building_id)
	{
		$select = $this->getDbTable()->select()
			->where('building_id = ?', $building_id)
			->where('status = ?', 'Available')
		;

		$result = $this->fetchAll($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function getSimilar($building_id, $unit_id, $limit = 2)
	{
		$unit = $this->find($unit_id);

		$select = $this->getDbTable()->select()
			->where('building_id = ?', $building_id)
			->where('status = ?', 'Available')
			->where('price >= ?', $unit->price * .85)
			->where('price <= ?', $unit->price * 1.15)
			->where('residential LIKE "%Rentals%"')
			->limit($limit)
		;

		if ($unit_id) {
			$select->where('id <> ?', $unit_id);
		}

		$result = $this->fetchAll($select);

		if (count($result) < $limit) {
			$select = $this->getDbTable()->select()
				->where('building_id <> ?', $building_id)
				->where('status = ?', 'Available')
				->where('price >= ?', $unit->price * .85)
				->where('price <= ?', $unit->price * 1.15)
				->where('residential LIKE "%Rentals%"')
				->limit($limit - count($result))
			;

			$result2 = $this->fetchAll($select);

			$result = $result + $result2;
		}

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function availableRentals($model_units_only = false)
	{//This query was made for the feeds (FeedController) so modify with caution if trying to use it elsewhere

		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('u'=>$this->_table_prefix.'gf_units'),'*')
			->joinLeft(array('b'=>$this->_table_prefix.'gf_buildings'),'u.building_id = b.id',array('building'=>'name','building_code'=>'code','building_yadi_code'=>'yardi_code','voyager_code','active_streeteasy','street1','street2','city','state','zip','building_description'=>'description','latitude','longitude'))
			//->joinLeft(array('ba'=>$this->_table_prefix.'gf_building_amenities'),'ba.building_id = b.id','')
			//->joinLeft(array('a'=>$this->_table_prefix.'gf_amenities'),'ba.amenity_id = a.id','')
			//->joinLeft(array('uf'=>$this->_table_prefix.'gf_unit_features'),'uf.unit_id = u.id','')
			//->joinLeft(array('f'=>$this->_table_prefix.'gf_features'),'uf.feature_id = f.id','')
			->joinLeft(array('ufp'=>$this->_table_prefix.'gf_floorplans'),'ufp.id = u.floorplan_id',array('floorplan'=>'image','floorplan_title'=>'title'))
			//->joinLeft(array('t'=>$this->_table_prefix.'gf_building_transportations'),'t.building_id = b.id','')
			//->joinLeft(array('boh'=>$this->_table_prefix.'gf_building_open_houses'),'boh.building_id = b.id','')
			->joinLeft(array('n'=>$this->_table_prefix.'gf_neighborhoods'),'b.neighborhood_id = n.id',array('neighborhood'=>'name','neighborhood_code'=>'code'))
			->joinLeft(array('r'=>$this->_table_prefix.'gf_regions'),'n.region_id = r.id',array('region'=>'name','region_code'=>'code','region_email'=>'email','region_phone'=>'phone','apply_link'))
		;

		if ($model_units_only) {
			$select->where('u.number LIKE "%WAIT%"');
		}else {
			$select->where('u.status = ?', 'Available');
		}

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}

	}

	public function getStreeteasyStreetAddress($unit_id)
	{//used in the feeds (FeedController)
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('u'=>$this->_table_prefix.'gf_units'),'*')
			->joinLeft(array('b'=>$this->_table_prefix.'gf_buildings'),'u.building_id = b.id',array('building'=>'name','building_code'=>'code','yardi_code','voyager_code','active_streeteasy','street1','street2','city','state','zip','building_description'=>'description','latitude','longitude'))
			->joinLeft(array('n'=>$this->_table_prefix.'gf_neighborhoods'),'b.neighborhood_id = n.id',array('neighborhood'=>'name','neighborhood_code'=>'code'))
			->joinLeft(array('r'=>$this->_table_prefix.'gf_regions'),'n.region_id = r.id',array('region'=>'name','region_code'=>'code','region_email'=>'email','region_phone'=>'phone','apply_link'))
			->where('u.id = ?', $unit_id)
		;
		$unit = $this->getDbTable()->fetchRow($select);

		// Ignore formating if wait units
		if (strpos(strtolower($unit['number']), 'wait') !== false) {
			return $unit['street1'];
		}

		switch (strtolower($unit['voyager_code'])) {

			// Park Tower South
			case 'ptsr':
				$addressNumber = substr($unit['number'], 0 , 3);

				// street is based of address number if 315 the street is 57th otherwise 58th;
				$street = ($addressNumber == '315') ? '57' : '58';

				return "$addressNumber West {$street}th Street";
				break;

			// Noonan Towers
			case 'ntcr':
				$addressNumber = substr($unit['number'], 0 , 3);

				// street is based of address number if 939 the street is Woodycrest otherwise Ogden;
				$street = ($addressNumber == '939') ? 'Woodycrest' : 'Ogden';

				return "$addressNumber $street Ave";
				break;

			// Wavecrest
			case 'rocr':
				return "20-".substr($unit['number'], 1 , 2)." Seagirt Blvd";
				break;

			// Drake
			case 'doc':
				return "20-".substr($unit['number'], 0 , 3)." Drake Ave";
				break;

			// Harbor One
			case 'hoc':
				return substr($unit['number'], 1 , 2)." Davenport Ave";
				break;

			// Maple Gardens
			case 'irvr':
				return substr($unit['number'], 1 , 2)." Marshall Street";
				break;

			// Wavecrest
			case 'mhcr':
				return "19".substr($unit['number'], 1 , 2)." Sedgewick Avenue";
				break;

			// Cedar Arms (building that was broken up as three buildings)
			case 'ctc, foc, dtc':
				$code =  strtolower(substr($unit['number'], 0 , 3));

				if($code == 'ctc'){
					return '2175 Cedar Avenue';
				}  else if($code == 'foc'){
					return '2121 Cedar Avenue';
				} else if($code == 'dtc'){
					return '2101 Cedar Avenue';
				}
				break;
			default:
				return $unit['street1'];
				break;
		}
	}

	public function setRentedUnitsToOccupied($availableUnits)
	{
		$this->getDbTable()->update(array(
			'consec_unavail_count' => new Zend_Db_Expr('consec_unavail_count + 1'),
			'modified' => new Zend_Db_Expr("NOW()"),
		),
		"residential <> '[]' AND `number` NOT LIKE '%wait%' AND yardi_code NOT IN (".implode(',',$availableUnits).")");

		$this->getDbTable()->update(array(
			'status' => 'Occupied',
			'modified' => new Zend_Db_Expr("NOW()"),
		),
		"residential <> '[]' AND `number` NOT LIKE '%wait%' AND yardi_code NOT IN (".implode(',',$availableUnits).") 
		AND consec_unavail_count > 2");
	}

	public function updateUnit($unitData, $building_id){
		if (isset($unitData->ApartmentId)) {
			$unit = $this->doesExists(array('yardi_code' => $unitData->ApartmentId));

			//if unit doesn't exist create it
			if (!$unit) {
				$unit = new Custom_Model_GfUnit();
				$unit->status = 'Unavailable';
				$unit->residential = '["Rentals"]';
				$unit->yardi_code = $unitData->ApartmentId;
				$unit->building_id = $building_id;
			};

			// remove random period from  aptartment number
			$unit->number = str_replace('.', '', $unitData->ApartmentName);
			$unit->bedrooms = $unitData->Beds;
			$unit->square_footage = $unitData->SQFT;
			$unit->price = $unitData->MaximumRent;
			$unit->apply_link = $unitData->ApplyOnlineURL;
			$unit->bathrooms = $unitData->Baths;
			$unit->rooms = ceil($unitData->Beds + $unitData->Baths + 1);
			$unit->consec_unavail_count = 0;

			if ($unit->status == 'Occupied') {
				$unit->status = 'Unavailable';
			}

			$id = $this->save($unit);

			Custom_Service_Audit::recordUnitActivity($id, 'rentcafe', 'rentcafe', $unitData->UnitStatus);
		}
	}

	/**
	 * @param  array  $units
	 * @param  string  $type
	 * @param  int  $interval
	 * @return array
	 * @throws Exception
	 */
	public function activity($units, $type, $interval = 21)
	{
		if (!empty($units)) {
			$all = [];
			foreach ($units as $unit) {
				$all[] = $unit->id;
			};
			$all_units = "'".implode("', '", $all)."'";
			$sql = <<<sql
SELECT u.number, a.type, a.price, a.net_effective, a.concession, a.openhouse, a.status, a.rentcafe_status, a.created 
FROM gf_audits a 
    LEFT JOIN gf_units u ON a.unit_id = u.id 
WHERE a.type = '{$type}' 
  AND a.created >= DATE_ADD(NOW(), INTERVAL -{$interval} DAY) 
  AND a.unit_id IN ({$all_units}) 
ORDER BY a.unit_id, a.id DESC;
sql;
			return $this->getDbTable()->getAdapter()->fetchAll($sql);
		}
		return [];
	}
}
