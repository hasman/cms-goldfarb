<?php
class Custom_Model_Mapper_GfQuickApplyUnit extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Custom_Model_DbTable_GfQuickApplyUnit');
		$this->setEntity('Custom_Model_GfQuickApplyUnit');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchUnitByRegionId($region_id = 1, $bedrooms = 0)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('qau' => $this->_table_prefix . 'gf_quick_apply_units'))
			->joinLeft(array('u' => $this->_table_prefix . 'gf_units'), 'qau.unit_id = u.id', array('yardi_code','apply_link'))
			->joinLeft(array('b' => $this->_table_prefix . 'gf_buildings'), 'u.building_id = b.id', array('building_code'=>'b.code','propertycode'=>'b.yardi_code','addr'=>'b.street1','b.state','b.zip'))
			->joinLeft(array('n' => $this->_table_prefix . 'gf_neighborhoods'), 'b.neighborhood_id = n.id', '')
			->where("qau.region_id = ?", $region_id)
			->where('qau.bedrooms = ?', $bedrooms)
			;

		$result = $this->getDbTable()->fetchRow($select);
		
		if ($result) {
			return $result;
		} else {
			return false;
		}
	}
}