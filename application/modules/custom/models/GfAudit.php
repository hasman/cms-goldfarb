<?php
class Custom_Model_GfAudit extends Application_Model_Abstract
{
	public $unit_id;
	public $type;
	public $price;
	public $net_effective;
	public $concession;
	public $openhouse;
	public $status;
	public $rentcafe_status;
	public $user;
}
