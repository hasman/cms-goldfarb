<?php
class Custom_Model_Yardi
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Registry::get('configuration')->toArray();

		$this->cfg = $options['yardi'];
	}

	private function execute($query_params = array())
	{
		$qs_array = [];
		foreach ($query_params as $k => $param) {
			$qs_array[$k] = $param;
		}

		$queryString = http_build_query($qs_array);

		$queryString .= "&apitoken=" .$this->cfg['api_token'];
		$queryString .= "&username=" .$this->cfg['username']; //only needed for post types
		$queryString .= "&password=" .$this->cfg['password']; //only needed for post types

		$url = $this->cfg['api_url'] . "?$queryString";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json'
		));

		$result = curl_exec($ch);
		curl_close($ch);

		$json = json_decode($result);

		return $json;
	}

	public function postLead($tenantLead)
	{
		if($tenantLead->building()->count()){
			$yardiCodes = $tenantLead->building->yardi_code;
		} else {
			$yardiCodes = $tenantLead->region->defaultBuilding->yardi_code;
		}
		//some buildings have more then one yardi_code so we must select first one.
		$yardiCodes = explode(',', $yardiCodes);
		$propertyCode = $yardiCodes[0];

		$query = [
			'requestType' => 'lead',
			'firstName' => $tenantLead->first_name,
			'lastName' => $tenantLead->last_name,
			'email' => $tenantLead->email,
			'source' => $tenantLead->source,
			'propertyCode' => $propertyCode,
			'phone' => $tenantLead->phone,
			'message' => $tenantLead->questions
		];

		return $this->execute($query);
	}

	public function getAvailability($propertyCode)
	{
		$query = [
			'requestType' => 'apartmentAvailability',
			'propertyCode' => $propertyCode
		];

		return $this->execute($query);
	}
}
