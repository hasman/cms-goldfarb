<?php
class Custom_Model_GfBuilding extends Application_Model_Abstract
{
	public $code;
	public $name;
	public $neighborhood_id;
	public $description;
	public $commercial;
	public $residential;
	public $voyager_code;
	public $yardi_code;
	public $active_streeteasy;
	public $elise_text;
	public $address_display_name;
	public $cross_street;
	public $street1;
	public $street2;
	public $city;
	public $state;
	public $zip;
	public $latitude;
	public $longitude;
	public $meta_title;
	public $meta_description;
}
