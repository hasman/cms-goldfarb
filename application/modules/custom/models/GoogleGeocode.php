<?php
class Custom_Model_GoogleGeocode
{
	private $_cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_cfg = $options['google'];

		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array('automatic_serialization' => true, 'lifetime' => 600);
		$this->_backendOptions = array('cache_dir' => $cache_dir);
	}

	public function getGeocodeData($address)
	{
		$address = urlencode($address);

		$cache_id = 'geocode_' . preg_replace('/[^\da-z]/i', '_', $address);
		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);

		if (!($cache->test($cache_id))) {
			$googleMapUrl = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=" . $this->_cfg['geocode']['api_key'];
			$geocodeResponseData = file_get_contents($googleMapUrl);
			$responseData = json_decode($geocodeResponseData, true);
			if ($responseData['status'] == 'OK') {
				$latitude = isset($responseData['results'][0]['geometry']['location']['lat']) ? $responseData['results'][0]['geometry']['location']['lat'] : "";
				$longitude = isset($responseData['results'][0]['geometry']['location']['lng']) ? $responseData['results'][0]['geometry']['location']['lng'] : "";
				$formattedAddress = isset($responseData['results'][0]['formatted_address']) ? $responseData['results'][0]['formatted_address'] : "";
				if ($latitude && $longitude && $formattedAddress) {
					$geocodeData = array();
					array_push(
						$geocodeData,
						$latitude,
						$longitude,
						$formattedAddress
					);
					//return $geocodeData;

					$cache->save($geocodeData, $cache_id, array('geocode'));
				} else {
					return false;
				}
			} else {
				echo "ERROR: {$responseData['status']}";
				return false;
			}
		}else {
			$geocodeData = $cache->load($cache_id);
		}

		return $geocodeData;
	}
}
