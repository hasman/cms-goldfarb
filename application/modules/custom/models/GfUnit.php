<?php
class Custom_Model_GfUnit extends Application_Model_Abstract
{
	public $code;
	public $yardi_code;
	public $building_id;
	public $floorplan_id;
	public $commercial;
	public $residential;
	public $number;
	public $feed_number_override;
	public $show_number = 'No';
	public $floor;
	public $square_footage;
	public $square_footage_status = 'Enabled';
	public $rooms;
	public $bedrooms;
	public $bathrooms;
	public $price;
	public $lease_term;
	public $concession_months;
	public $concession_expiration;
	public $concession_language;
	public $streeteasy_status = 'Enabled';
	public $disable_openhouse = 'No';
	public $description;
	public $featured = 'No';
	public $version = 0;
	public $apply_link;
	public $carousel_vimeo_url;
	public $virtual_tour_id;
	public $walkin_key;
	public $meta_title;
	public $meta_description;
	public $status;
	public $consec_unavail_count;
}
