<?php
class Custom_FeedController extends Zend_Controller_Action
{
	/** @var Zend_Db_Adapter_Mysqli */
	var $_db;
	/** @var string */
	var $_domain;
	var $units;
	var $model_units;
	/** @var Custom_Model_Mapper_GfUnit */
	var $_unit_mapper;
	/** @var Custom_Model_Mapper_GfAmenity */
	var $_amenity_mapper;
	/** @var Custom_Model_Mapper_GfFeature */
	var $_feature_mapper;
	/** @var Custom_Model_Mapper_GfBuildingOpenHouse */
	var $_openhouse_mapper;
	/** @var Custom_Model_Mapper_GfUnitImage */
	var $_unit_image_mapper;
	/** @var Custom_Model_Mapper_GfFloorplan */
	var $_floorplan_mapper;
	/** @var Custom_Model_Mapper_GfBuildingTransportation */
	var $_trasportation_mapper;
	/** @var Custom_Model_Mapper_GfUnitIncentive */
	var $_incentive_mapper;
	var $_building_mapper;

	public function init()
	{
		/* Initialize action controller here */

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		/** @var Zend_Application_Resource_Multidb $resource */
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();

		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
		$host = (array_key_exists('HTTP_HOST', $_SERVER)) ? $_SERVER['HTTP_HOST'] : 'goldfarbproperties.com';
		$this->_domain = $protocol."://".$host;

		$this->_unit_mapper = new Custom_Model_Mapper_GfUnit();
		$this->_amenity_mapper = new Custom_Model_Mapper_GfAmenity();
		$this->_feature_mapper = new Custom_Model_Mapper_GfFeature();
		$this->_openhouse_mapper = new Custom_Model_Mapper_GfBuildingOpenHouse();
		$this->_unit_image_mapper = new Custom_Model_Mapper_GfUnitImage();
		$this->_floorplan_mapper = new Custom_Model_Mapper_GfFloorplan();
		$this->_trasportation_mapper = new Custom_Model_Mapper_GfBuildingTransportation();
		$this->_incentive_mapper = new Custom_Model_Mapper_GfUnitIncentive();
		$this->_building_mapper = new Custom_Model_Mapper_GfBuilding();
	}

	private function getUnits() {
		return $this->_unit_mapper->availableRentals();
	}

	private function getModelUnits() {
		return $this->_unit_mapper->availableRentals(true);
	}

	public function streeteasyAction()
	{
		$units = $this->getUnits();

		$xml = new DomDocument('1.0', 'UTF-8');
		$main_element = $xml->createElement('streeteasy');
		$main_element->setAttribute('version', '1.6');

		$properties = $xml->createElement('properties');
		$main_element->appendChild($properties);

		foreach ($units as $unit) {
			//this logic seems flawed. Why building active_streeteasy = "no" still posts?
			if ($unit['active_streeteasy'] == "No" || ($unit['active_streeteasy'] == 'Yes' && $unit['streeteasy_status'] == 'Enabled')) {
				$number = preg_replace("/[^A-Za-z0-9 ]/", '', ($unit['feed_number_override'])?$unit['feed_number_override']:$unit['number']);
				$unique_id = strtolower(sprintf('%sv%s', $number, $unit['version']));

				$property = $xml->createElement('property');
				$property->setAttribute('type', 'rental');
				$property->setAttribute('status', 'active');
				$property->setAttribute('id', $unique_id);
				$property->setAttribute('url',$this->_domain.'/rentals/'.$unit['region_code'].'/'.$unit['building_code'].'/apartment-'.$unit['code']);
				$properties->appendChild($property);

				//start location
				$location = $xml->createElement('location');
				$property->appendChild($location);

				$building_address = $this->_unit_mapper->getStreeteasyStreetAddress($unit['id']);

				$address = $xml->createElement('address',trim($building_address));
				$location->appendChild($address);

				$apartment = $xml->createElement('apartment',$this->_unit_mapper->streeteasyApartmentNumber($unit['id']));
				$location->appendChild($apartment);

				$city = $xml->createElement('city',$unit['city']);
				$location->appendChild($city);

				$state = $xml->createElement('state',$unit['state']);
				$location->appendChild($state);

				$zipcode = $xml->createElement('zipcode',$unit['zip']);
				$location->appendChild($zipcode);

				$neighborhood = $xml->createElement('neighborhood',$unit['neighborhood']);
				$location->appendChild($neighborhood);
				//end location

				//start details
				$details = $xml->createElement('details');
				$property->appendChild($details);

				$price = $xml->createElement('price',$this->_unit_mapper->getUnitNetEffectivePrice($unit['id']));
				$details->appendChild($price);

				$bedrooms = $xml->createElement('bedrooms',floor($unit['bedrooms']));
				$details->appendChild($bedrooms);

				$bathrooms = $xml->createElement('bathrooms',floor($unit['bathrooms']));
				$details->appendChild($bathrooms);

				$half_baths = $xml->createElement('half_baths',ceil(fmod($unit['bathrooms'],1)));
				$details->appendChild($half_baths);

				$totalrooms = $xml->createElement('totalrooms',$unit['rooms']);
				$details->appendChild($totalrooms);

				$description = $xml->createElement('description');
				$details->appendChild($description);
				$concession_language = ($text = $this->_unit_mapper->getUnitConcessionMessage($unit['id']))?"Incentive: $text ":"";
				$description_data = $xml->createCDATASection($concession_language.$unit['description']);
				$description->appendChild($description_data);

				$property_type = $xml->createElement('propertyType','rental');
				$details->appendChild($property_type);

				$amenities = $xml->createElement('amenities');
				$details->appendChild($amenities);

				//start amenities
				$building_amenities = $this->_amenity_mapper->fetchAmenityNamesByBuildingId($unit['building_id'],'streeteasy_tag');
				$unit_features = $this->_feature_mapper->fetchFeatureNamesByUnitId($unit['id'],'streeteasy_tag');
				$extraAmenities = [];

				foreach ($building_amenities as $amenity) {
					$extraAmenities[] = trim($amenity);
				}
				foreach ($unit_features as $amenity) {
					$extraAmenities[] = trim($amenity);
				}

				$amenities_other = $xml->createElement('other',implode(', ',$extraAmenities));
				$amenities->appendChild($amenities_other);
				//end amenities
				//end details

				//open houses
				if ($unit['disable_openhouse'] != 'Yes') {
					$open_houses = $xml->createElement('openHouses');
					$property->appendChild($open_houses);

					$building_open_houses = $this->_openhouse_mapper->fetchByBuildingId($unit['building_id']);
					if ($building_open_houses) {
						foreach ($building_open_houses as $building_open_house) {
							$open_house = $xml->createElement('openHouse');
							$open_houses->appendChild($open_house);

							$open_house_start = $xml->createElement('startsAt', date('Y-m-d g:ia', strtotime($building_open_house->start_time)));
							$open_house->appendChild($open_house_start);

							$open_house_end = $xml->createElement('endsAt', date('Y-m-d g:ia', strtotime($building_open_house->end_time)));
							$open_house->appendChild($open_house_end);
						}
					}
				}

				//agent
				$agents = $xml->createElement('agents');
				$property->appendChild($agents);

				$agent = $xml->createElement('agent');
				$agents->appendChild($agent);

				$agent_name = $xml->createElement('name','Goldfarb Properties');
				$agent->appendChild($agent_name);

				$agent_company = $xml->createElement('name','Goldfarb Properties');
				$agent->appendChild($agent_company);

				$agent_email = $xml->createElement('email',$unit['region_email']);
				$agent->appendChild($agent_email);

				$agent_phone = $xml->createElement('phone_numbers');
				$agent->appendChild($agent_phone);

				$agent_phone_main = $xml->createElement('main',$unit['region_phone']);
				$agent_phone->appendChild($agent_phone_main);
				//end agents

				//media / floorplans
				$media = $xml->createElement('media');
				$property->appendChild($media);

				if(!empty($unit['carousel_vimeo_url'])){
					$video = $xml->createElement('video');
					$video->setAttribute('url',$unit['carousel_vimeo_url']);
					$media->appendChild($video);
				}
				if(!empty($unit['virtual_tour_id'])){
					$video = $xml->createElement('_3DTour');
					$video->setAttribute('url','https://my.matterport.com/show/?m='.$unit['virtual_tour_id']);
					$media->appendChild($video);
				}

				$unit_images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit['id']);
				foreach ($unit_images as $image) {
					$photo = $xml->createElement('photo');
					$photo->setAttribute('url',$this->_domain.$image->image);
					$photo->setAttribute('description',$image->alt_text);
					$photo->setAttribute('position',$image->sort_order);
					$media->appendChild($photo);
				}

				$unit_floorplan = $this->_floorplan_mapper->find($unit['floorplan_id']);
				if ($unit_floorplan) {
					$floorplan = $xml->createElement('floorplan');
					$floorplan->setAttribute('url',$this->_domain.$unit_floorplan->image);
					$floorplan->setAttribute('description',$image->title);
					$media->appendChild($floorplan);
				}
			}
		}

		$xml->appendChild($main_element);
		$xml->formatOutput = TRUE;

		header('Content-type: text/xml');
		echo $xml->saveXML();
		exit;

/*
 <?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>
<streeteasy version="1.6">
  <properties>
  @foreach ($units as $unit)
  <?php
    //BEGIN: Will Post?
    $post = false;
    if($unit->building->active_streeteasy == 'no') {
      $post = true;
    } else if($unit->building->active_streeteasy == 'yes' && $unit->active_streeteasy == 'yes') {
      $post = true;
    } else {
      $post = false;
    }
    //END: Will Post?
    ?>
    @if ($post)
      <property type="rental" status="active" id="{{$unit->unique_version_id}}" url="{{url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug]))}}">
        <location>
          <address>{{$unit->getStreeteasyStreetAddress()}}{{(!!$unit->building->address->street_2) ? ',' . $unit->building->address->street_2 : null}}</address>
          <apartment>{{$unit->getStreeteasyNumber()}}</apartment>
          <city>{{$unit->building->address->city}}</city>
          <state>{{$unit->building->address->state}}</state>
          <zipcode>{{$unit->building->address->zip}}</zipcode>
          <neighborhood>{{$unit->building->neighborhood->name}}</neighborhood>
        </location>

        <details>
          <price>{{$unit->net_effective_price}}</price>
          <noFee/>
          <bedrooms>{{floor($unit->bedrooms)}}</bedrooms>
          <bathrooms>{{floor($unit->bathrooms)}}</bathrooms>
          <half_baths>{{ceil(fmod($unit->bathrooms, 1))}}</half_baths>
          <totalrooms>{{$unit->rooms}}</totalrooms> <!-- total # rooms in the property -->
          <description>
          <![CDATA[@if (!!$unit->getConcessionMessage()) {{$unit->getConcessionMessage()}}<br><br><br>@endif @if (!!$unit->body) Apartment Description:<br>{{$unit->body}}<br><br>@endif @if (!!$unit->building->body) Building Description: <br> {{$unit->building->body}} @endif ]]>
          </description>
          <propertyType>rental</propertyType>
          <amenities>
              <?php $extraAmenities = []?>
              @foreach ($unit->building->amenities as $amenity)
                  @if (!!$amenity->feed_meta['streeteasy'])
                      <{{$amenity->feed_meta['streeteasy']}}/>
                  @else
                      <?php $extraAmenities[] = $amenity->name ?>
                  @endif
              @endforeach
              @foreach ($unit->features as $amenity)
                  @if (!!$amenity->feed_meta['streeteasy'])
                      <{{$amenity->feed_meta['streeteasy']}}/>
                  @else
                      <?php $extraAmenities[] = $amenity->name ?>
                  @endif
              @endforeach
              <other>{{implode(', ', $extraAmenities)}}</other>
            </amenities>
        </details>


        {{-- OPEN HOUSES--}}
        @if (count($unit->building->openHouses))
          @if ($unit->disable_openhouse != 'yes')
          <openHouses>
            @foreach ($unit->building->openHouses as $openHouse)
            <openHouse>
              <startsAt>{{$openHouse->start_time->format('Y-m-d g:ia')}}</startsAt>
              <endsAt>{{$openHouse->end_time->format('Y-m-d g:ia')}}</endsAt>
            </openHouse>
             @endforeach
          </openHouses>
          @endif
        @endif

        <agents>
          <agent>
            <name>Goldfarb Properties</name>
            <company>Goldfarb Properties</company>
             <email>{{$unit->building->neighborhood->region->email}}</email>
             <phone_numbers>
              <main>{{$unit->building->neighborhood->region->phone}}</main>
            </phone_numbers>
          </agent>
        </agents>

        <media>
          @foreach ($unit->getGalleryImages() as $index => $image)
            <photo url="{!!url($image->styleSrc("carousel-slide"))!!}" description="{{$image->title}}" position="{{$index}}" />
          @endforeach

          @if (count($unit->floorplan))
             <?php /**
             <floorplan url="{{url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug]))}}/floorplan" description="{{$unit->floorplan->title}}"/>
             * / ?>
<floorplan url="{{asset($unit->basicFloorplanSrc())}}" description="{{$unit->floorplan->title}}"/>

@endif

</media>
</property>
@endif
@endforeach
</properties>
</streeteasy>
 */
	}

	/**
	 * converted: Completed [CSV]
	 */
	public function streeteasycsvAction()
	{
		/**
		 * The order entered into this array will be the order of the CSV columns.
		 */
		$headerNames = [
			"id", "yardi_code", "building_id", "building_name", "floorplan_id",
			"type", "number", "feed_number_override", "show_number", "slug",
			"floor", "square_footage", "square_footage_toggle", "rooms", "bedrooms",
			"bathrooms", "price", "lease_term", "concession_months", "active_streeteasy",
			"concession_expiration", "concession_language", "disable_openhouse", "status", "featured",
			"version", "apply_link"
		];

		date_default_timezone_set("America/New_York");
		$stamp = date("Ymd_hia");
		$filename = "streeteasy_" . $stamp . ".csv";

		header("Content-type: text/plain");
		header("Content-Disposition: attachment; filename=$filename");

		$fh = fopen('php://output', 'w');

		//region Render Header
		$headers = [];
		foreach ($headerNames as $v) {
			if ($v == 'id') {
				//need to keep id not all caps otherwise Excel thinks the CSV is a SYLK file.
				$headers[] = str_replace("_", " ", ucfirst($v));
			} else {
				$headers[] = str_replace("_", " ", strtoupper($v));
			}
		}
		fputcsv($fh, $headers);
		//endregion

		//region Render Lines
		$units = $this->getUnits();
		foreach ($units as $unit) {
			$line = [];
			if ($unit['active_streeteasy'] == "No" || ($unit['active_streeteasy'] == 'Yes'
					&& $unit['streeteasy_status'] == 'Enabled')) {
				foreach ($headerNames as $header) {
					switch ($header) {
						case 'square_footage_toggle':
							$line[] = $unit->square_footage_status;
							break;
						case 'type':
							$line[] = 'rental';
							break;
						case 'slug':
							$line[] = $unit->code;
							break;
						case 'building_name':
							$line[] = $unit->building;
							break;
						default:
							if (isset($unit->$header)) {
								$line[] = $unit->$header;
							} else {
								exit("Property {$header} does not exists in the 'unit'.");
							}
					}
				}
				fputcsv($fh, $line);
			}
		}
		//endregion

		fclose($fh);
		exit;
	}

	public function walkinAction()
	{
		$units = $this->getUnits();

		$xml = new DomDocument('1.0', 'UTF-8');
		$main_element = $xml->createElement('streeteasy');
		$main_element->setAttribute('version', '1.6');

		$properties = $xml->createElement('properties');
		$main_element->appendChild($properties);

		foreach ($units as $unit) {   

			/**
			 * Task Name: 12288 - Walk.in Feed Building Address Matching
			 **/
			
			$commercial = json_decode($unit['commercial']);
			if (isset($commercial) && is_array($commercial) && count($commercial) == 1) continue;

			/**
			 * TASK NAME: 12218 - Walk.in remove all wait units from feed
			 * BEGIN
			 **/

			$apartmentStringWait = ($unit['feed_number_override'])?$unit['feed_number_override']:$unit['number'];
			if (preg_match('/^WAIT/',trim($unit['number'])) == 1) continue;
			/**
			 * TASK NAME: 12218 - Walk.in remove all wait units from feed 
			 * END
			 **/
			

			if ($unit['commercial'] !== '[]' && $unit['commercial'] !== '') continue;

			$unit['street1'] = preg_replace("/Ave/","Avenue",$unit['street1']);
			$unit['street2'] = preg_replace("/Ave/","Avenue",$unit['street2']);
			$unit['street1'] = preg_replace("/20\-217\sDrake/","217 Drake",$unit['street1']);
			$unit['street2'] = preg_replace("/20\-217\sDrake/","217 Drake",$unit['street2']);

			$number = preg_replace("/[^A-Za-z0-9 ]/", '', ($unit['feed_number_override'])?$unit['feed_number_override']:$unit['number']);
			$unique_id = strtolower(sprintf('%sv%s', $number, $unit['version']));
			$applyLink =  false;
            if(!empty($unit['apply_link'])){
                $applyLink = $unit['apply_link'];
            }

			$applyurl = $xml->createElement('applyUrl',htmlspecialchars($applyLink));

			$property = $xml->createElement('property');
			$property->setAttribute('type', 'rental');
			$property->setAttribute('status', 'active');
			$property->setAttribute('id', $unique_id);
			$property->setAttribute('url',$this->_domain.'/rentals/'.$unit['region_code'].'/'.$unit['building_code'].'/apartment-'.$unit['code']);
			$properties->appendChild($property);

			//start location
			//region name mapping
			switch ($unit['region']) {
				case 'The Bronx':
					$region_name = 'Bronx';
					break;
				case 'Rockaway':
					$region_name = 'Queens';
					break;
				case 'Essex':
					$region_name = 'New Jersey';
					break;
				default:
					$region_name = $unit['region'];
					break;
			}


			$location = $xml->createElement('location');
			$property->appendChild($applyurl);
			$property->appendChild($location);

			$building_address = $this->_unit_mapper->getStreeteasyStreetAddress($unit['id']);

			$building_address = preg_replace("/Ave/","Avenue",$building_address);
			$building_address = preg_replace("/20\-217\sDrake/","217 Drake",$building_address);  

			/**
			 * TASK NAME: 12147 - Walk IN Feed - change IRV address updates
			 * BEGIN
			 **/

			$unitArray = $unit->toArray();
			$apartmentString = ($unit['feed_number_override'])?$unit['feed_number_override']:$unit['number'];

			if(preg_match("/^J/",trim($apartmentString)) == 1 && preg_match("/[0-9]+(|\-)\s/",trim($building_address)) == 1) {

				$apartmentStringStripped = preg_replace("/^0/","",substr(preg_replace("/^J/",'',$apartmentString),0,2));
				$building_address = preg_replace("/[0-9]+(|\-)\s/",$apartmentStringStripped . " ",trim($building_address));

				$address = $xml->createElement('address',trim($building_address)); 
				$location->appendChild($address);

				$apartment = $xml->createElement('apartment',trim($apartmentString));
				$location->appendChild($apartment); 

			} else {

				$address = $xml->createElement('address',trim($building_address));
				$location->appendChild($address);

				$apartment = $xml->createElement('apartment',trim($apartmentString));
				$location->appendChild($apartment);

			} 

			 /**
			 * TASK NAME: 12147 - Walk IN Feed - change IRV address updates 
			 * END
			 **/

			$city = $xml->createElement('city',$unit['city']);
			$location->appendChild($city);

			$state = $xml->createElement('state',$unit['state']);
			$location->appendChild($state);

			$zipcode = $xml->createElement('zipcode',$unit['zip']);
			$location->appendChild($zipcode);
			$neighborhood = $xml->createElement('neighborhood',$unit['neighborhood']);
			$location->appendChild($neighborhood);
			$latitude = $xml->createElement('latitude',$unit['latitude']);
			$location->appendChild($latitude);
			$longitude = $xml->createElement('longitude',$unit['longitude']);
			$location->appendChild($longitude);
			$region = $xml->createElement('region',$region_name);
			$location->appendChild($region);
			//end location

			//start details
			$details = $xml->createElement('details');
			$property->appendChild($details);

			$gross = $xml->createElement('gross',$this->_unit_mapper->getUnitNetEffectivePrice($unit['id']));
			$details->appendChild($gross);

			$price = $xml->createElement('price',str_replace('.00','',$this->_unit_mapper->getUnitNetEffectivePrice($unit['id'])));
			$details->appendChild($price);

			$nofee = $xml->createElement('noFee');
			$details->appendChild($nofee);

			$bedrooms = $xml->createElement('bedrooms',floor($unit['bedrooms']));
			$details->appendChild($bedrooms);

			$bathrooms = $xml->createElement('bathrooms',floor($unit['bathrooms']));
			$details->appendChild($bathrooms);

			$half_baths = $xml->createElement('half_baths',ceil(fmod($unit['bathrooms'],1)));
			$details->appendChild($half_baths);

			$totalrooms = $xml->createElement('totalrooms',$unit['rooms']);
			$details->appendChild($totalrooms);

			$description = $xml->createElement('description');
			$details->appendChild($description);

			$unitdes = $xml->createElement('unit');
			$description->appendChild($unitdes);
			$concession_language = ($text = $this->_unit_mapper->getUnitConcessionMessage($unit['id']))?"Incentive: $text ":"";
			$description_data = $xml->createCDATASection($concession_language.$unit['description']);
			$unitdes->appendChild($description_data);

			$buildingdes = $xml->createElement('building');
			$description->appendChild($buildingdes);
			$building_data = $xml->createCDATASection($unit['building_description']);
			$buildingdes->appendChild($building_data);

			$property_type = $xml->createElement('propertyType','rental');
			$details->appendChild($property_type);

			$amenities = $xml->createElement('amenities');
			$details->appendChild($amenities);
			$bamenities = $xml->createElement('building');
			$amenities->appendChild($bamenities);
			$uamenities = $xml->createElement('unit');
			$amenities->appendChild($uamenities);
			//start amenities
			$building_amenities = $this->_amenity_mapper->fetchAmenityNamesByBuildingId($unit['building_id']);
			$unit_features = $this->_feature_mapper->fetchFeatureNamesByUnitId($unit['id']);

			foreach ($building_amenities as $amenity) {
				$ba = $xml->createElement('amenity');
				$bamenity = $xml->createTextNode($amenity);
				$ba->appendChild($bamenity);
				$bamenities->appendChild($ba);
			}
			foreach ($unit_features as $amenity) {
				$ua = $xml->createElement('amenity',$amenity);
				$uamenity = $xml->createTextNode($amenity);
				$ua->appendChild($uamenity);
				$uamenities->appendChild($ua);
			}
			//end amenities

			//incentives
			$incentives = $xml->createElement('incentives');
			$details->appendChild($incentives);
			$incentive = $xml->createElement('incentive');

			$incentives->appendChild($incentive);

			$incname = $xml->createElement('name',$this->_incentive_mapper->fetchNameByUnitId($unit['id']));
			$incentive->appendChild($incname);
			$incdesc = $xml->createElement('description','');
			$incentive->appendChild($incdesc);

			//concession
			$consession = $xml->createElement('consession');
			$details->appendChild($consession);

			$consession_data = $xml->createCDATASection($this->_unit_mapper->getUnitConcessionMessage($unit['id']));
			$consession->appendChild($consession_data);

			//transportation
			$transportation = $xml->createElement('transportation');
			$details->appendChild($transportation);
			$trains = $xml->createElement('trains');
			$transportation->appendChild($trains);

			$building_transportation = $this->_trasportation_mapper->fetchByBuildingId($unit['building_id']);
			if ($building_transportation) {
				foreach ($building_transportation as $value) {
					$lines = json_decode($value->station_service);

					foreach ($lines as $line) {
						if ($line == 'LIRR') {
							$train = $xml->createElement('other');
							$trains->appendChild($train);
						} else {
							$train = $xml->createElement('train');
							$trains->appendChild($train);
						}

						$line = $xml->createElement('line', $line);
						$train->appendChild($line);
						$station = $xml->createElement('station', $value->station_name);
						$train->appendChild($station);
						$proximity = $xml->createElement('proximity', $value->distance_to_station);
						$train->appendChild($proximity);
					}

				}
			}
			//end details

			//open houses
			if ($unit['disable_openhouse'] != 'Yes') {
				$open_houses = $xml->createElement('openHouses');
				$property->appendChild($open_houses);

				$building_open_houses = $this->_openhouse_mapper->fetchByBuildingId($unit['building_id']);
				if ($building_open_houses) {
					foreach ($building_open_houses as $building_open_house) {
						$open_house = $xml->createElement('openHouse');
						$open_houses->appendChild($open_house);

						$open_house_start = $xml->createElement('startsAt', date('Y-m-d g:ia', strtotime($building_open_house->start_time)));
						$open_house->appendChild($open_house_start);

						$open_house_end = $xml->createElement('endsAt', date('Y-m-d g:ia', strtotime($building_open_house->end_time)));
						$open_house->appendChild($open_house_end);
					}
				}
			}

			//agent
			$agents = $xml->createElement('agents');
			$property->appendChild($agents);

			$agent = $xml->createElement('agent');
			$agents->appendChild($agent);

			$agent_name = $xml->createElement('name','Goldfarb Properties');
			$agent->appendChild($agent_name);

			$agent_company = $xml->createElement('name','Goldfarb Properties');
			$agent->appendChild($agent_company);

			$agent_email = $xml->createElement('email',$unit['region_email']);
			$agent->appendChild($agent_email);

			$agent_phone = $xml->createElement('phone_numbers');
			$agent->appendChild($agent_phone);

			$agent_phone_main = $xml->createElement('main',$unit['region_phone']);
			$agent_phone->appendChild($agent_phone_main);
			//end agents

			//media / floorplans
			$media = $xml->createElement('media');
			$property->appendChild($media);

			$unit_images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit['id']);
			if ($unit_images) {
				foreach ($unit_images as $image) {
					$photo = $xml->createElement('photo');
					$photo->setAttribute('url', $this->_domain . $image->image);
					$photo->setAttribute('description', $image->alt_text);
					$photo->setAttribute('position', $image->sort_order);
					$media->appendChild($photo);
				}
			}

			$unit_floorplan = $this->_floorplan_mapper->find($unit['floorplan_id']);
			if ($unit_floorplan) {
				$floorplan = $xml->createElement('floorplan');
				$floorplan->setAttribute('url',$this->_domain.$unit_floorplan->image);
				$floorplan->setAttribute('description',$image->title);
				$media->appendChild($floorplan);
			}
		}

		$xml->appendChild($main_element);
		$xml->formatOutput = TRUE;

		header('Content-type: text/xml');
		echo $xml->saveXML();
		exit;   

/*
<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>

<streeteasy version="1.6">
  <properties>
  @foreach ($units as $unit)
      <property type="rental" status="active" id="{{$unit->unique_version_id}}" url="{{url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug]))}}">
        <?php
          $applyLink =  false;
          if (isset($unit) && $unit->isResidential() && !!$unit->apply_link) {
            $applyLink = $unit->apply_link;
          } elseif(!isset($unit)) {
            $applyLink =  $building->neighborhood->region->apply_link;
          }
        ?>
        <applyUrl>
          {{$applyLink}}
        </applyUrl>
        <location>
          <address>{{$unit->getStreeteasyStreetAddress()}}{{(!!$unit->building->address->street_2) ? ',' . $unit->building->address->street_2 : null}}</address>
          <apartment>{{$unit->getStreeteasyNumber()}}</apartment>
          <city>{{$unit->building->address->city}}</city>
          <state>{{$unit->building->address->state}}</state>
          <zipcode>{{$unit->building->address->zip}}</zipcode>
          <neighborhood>{{$unit->building->neighborhood->name}}</neighborhood>
          <latitude>{{$unit->building->address->getLatLngAttribute()['lat']}}</latitude>
          <longitude>{{$unit->building->address->getLatLngAttribute()['lng']}}</longitude>
          <?php
          $region = $unit->building->neighborhood->region->name;
          switch($region){
            case 'The Bronx':
              $region = 'Bronx';
              break;
            case 'Rockaway':
              $region = 'Queens';
              break;
            case 'Essex':
              $region = 'New Jersey';
              break;
          }
          ?>
          <region>{{$region}}</region>
        </location>

        <details>
          <gross>{{str_replace(".00", "", $unit->price)}}</gross>
          <price>{{$unit->net_effective_price}}</price>
          <noFee/>
          <bedrooms>{{floor($unit->bedrooms)}}</bedrooms>
          <bathrooms>{{floor($unit->bathrooms)}}</bathrooms>
          <half_baths>{{ceil(fmod($unit->bathrooms, 1))}}</half_baths>
          <totalrooms>{{$unit->rooms}}</totalrooms> <!-- total # rooms in the property -->
          <description>
            @if (!!!$unit->body)
              <unit />
            @else
            <unit><![CDATA[ @if (!!$unit->body) {{$unit->body}} @endif ]]></unit>
            @endif
            @if (!!!$unit->building->body)
            <building />
            @else
            <building><![CDATA[ @if (!!$unit->building->body) {{$unit->building->body}} @endif ]]></building>
            @endif
          </description>
          <propertyType>rental</propertyType>
          <amenities>
            <building>
              @foreach ($unit->building->amenities as $amenity)
                <amenity>{{$amenity->name}}</amenity>
              @endforeach
            </building>
            <unit>
              @foreach ($unit->features as $amenity)
                <amenity>{{$amenity->name}}</amenity>
              @endforeach
            </unit>
          </amenities>
            <?php
              $incentives_list = [];
              foreach($unit->incentives as $additional) {
                array_push($incentives_list, $additional->name);
              }
            ?>
            <?php
              if(isset($incentives_list)){ ?>
              <incentives>
              <?php
                foreach($incentives_list as $incentive){ ?>
                  <incentive>
                    <name>{{$incentive}}</name>
                    <description></description>
                  </incentive>
                <?php
                }
                ?>
              </incentives>
            <?php
              }
            ?>
            <?php
              if($unit->getConcessionMessage()) {
              ?>
              <concession>
                <![CDATA[ {{$unit->getConcessionMessage()}}]]>
              </concession>
              <?php
              }
            ?>
            <?php $trs = []; ?>
            @if (count($unit->building->transportations) > 0)
              <?php
                foreach($unit->building->transportations as $item){
                  $trs[] = [
                    'name' => $item->station_name,
                    'service' => $item->station_service,
                    'distance' => $item->distance_to_station
                  ];
                }
                $trs = array_slice($trs, 0, 5, true);
              ?>
            @endif
            <transportation>
              <trains>
                @foreach ($trs as $item)
                  <?php $trains = explode(" ", $item['service']); ?>
                  @foreach ($trains as $train)
                    @if ($train != 'LIRR')
                      <train>
                        <line>{{$train}}</line>
                        <station>{{$item['name']}}</station>
                        <proximity>{{$item['distance']}}</proximity>
                      </train>
                    @endif
                   @endforeach
                @endforeach
              </trains>
                @foreach ($trs as $item)
                  <?php $trains = explode(" ", $item['service']); ?>
                  @foreach ($trains as $train)
                    @if ($train == 'LIRR')
                      <other>
                        <line>{{$train}}</line>
                        <station>{{$item['name']}}</station>
                        <proximity>{{$item['distance']}}</proximity>
                      </other>
                    @endif
                  @endforeach
                @endforeach
            </transportation>
        </details>

        {{-- OPEN HOUSES--}}
        @if (count($unit->building->openHouses))
          @if ($unit->disable_openhouse != 'yes')
          <openHouses>
            @foreach ($unit->building->openHouses as $openHouse)
            <openHouse>
              <startsAt>{{$openHouse->start_time->format('Y-m-d g:ia')}}</startsAt>
              <endsAt>{{$openHouse->end_time->format('Y-m-d g:ia')}}</endsAt>
            </openHouse>
             @endforeach
          </openHouses>
          @endif
        @endif

        <agents>
          <agent>
            <name>Goldfarb Properties</name>
            <company>Goldfarb Properties</company>
             <email>{{$unit->building->neighborhood->region->email}}</email>
             <phone_numbers>
              <main>{{$unit->building->neighborhood->region->phone}}</main>
            </phone_numbers>
          </agent>
        </agents>

        <media>
          @foreach ($unit->getGalleryImages() as $index => $image)
            <photo url="{!!url($image->styleSrc("carousel-slide"))!!}" description="{{$image->title}}" position="{{$index}}" />
          @endforeach

          @if (count($unit->floorplan))
             <floorplan url="{{asset($unit->basicFloorplanSrc())}}" description="{{$unit->floorplan->title}}"/>
          @endif
        </media>
      </property>
  @endforeach
  </properties>
</streeteasy>
 */
	}

	/**
	 * converted: Completed [XML]
	 */
	public function eliseAction()
	{
		$units = $this->getUnits();

		$buildings = $this->getBuildingsResidentialRentals();
		$unit_elise_text = '';
		$unit_yardi_data = [];
		foreach($buildings as $building) {
			foreach ($units as $unit) {
				if ($unit['building_code'] == $building->code) {
					foreach ($building->yardi_data as $yardi_data) {
						if ($yardi_data->ApartmentId == $unit['yardi_code']) {
							$unit_yardi_data[$unit['yardi_code']] = $yardi_data;
						}
					}
					if(!empty($building->elise_text) && !empty($unit['walkin_key'])){
						$unit_elise_text = str_replace('{{walkin_key}}',$unit['walkin_key'],$building->elise_text);
					}
					elseif(!empty($unit['walkin_key'])){
						$unit_elise_text = 'Additionally, you can visit our community by taking a +SelfTour through the '.$unit['walkin_key'].', which will allow you to download a virtual key and access the building and unit on your own.';
					}
				}
			}
		}

		$xml = new DomDocument('1.0', 'UTF-8');
		$main_element = $xml->createElement('streeteasy');
		$main_element->setAttribute('version', '1.6');
		$properties = $xml->createElement('properties');
		$main_element->appendChild($properties);
		foreach ($units as $unit) {
			$number = preg_replace("/[^A-Za-z0-9 ]/", '', ($unit['feed_number_override'])?$unit['feed_number_override']:$unit['number']);
			$unique_id = strtolower(sprintf('%sv%s', $number, $unit['version']));

			$property = $xml->createElement('property');
			$property->setAttribute('type', 'rental');
			$property->setAttribute('status', 'active');
			$property->setAttribute('id', $unique_id);
			$property->setAttribute('url',$this->_domain.'/rentals/'.$unit['region_code'].'/'.$unit['building_code'].'/apartment-'.$unit['code']);
			$properties->appendChild($property);

			//region location
			$location = $xml->createElement('location');
			$property->appendChild($location);

			$building_address = $this->_unit_mapper->getStreeteasyStreetAddress($unit['id']);

			$address = $xml->createElement('address',trim($building_address));
			$location->appendChild($address);

			$apartment = $xml->createElement('apartment',($unit['feed_number_override'])?$unit['feed_number_override']:$unit['number']);
			$location->appendChild($apartment);

			$city = $xml->createElement('city',$unit['city']);
			$location->appendChild($city);

			$state = $xml->createElement('state',$unit['state']);
			$location->appendChild($state);

			$zipcode = $xml->createElement('zipcode',$unit['zip']);
			$location->appendChild($zipcode);

			$neighborhood = $xml->createElement('neighborhood',$unit['neighborhood']);
			$location->appendChild($neighborhood);
			//endregion location

			//region details
			$details = $xml->createElement('details');
			$property->appendChild($details);

			$price = $xml->createElement('price', $unit['price']);
			$details->appendChild($price);

			$fee = $xml->createElement('noFee','');
			$details->appendChild($fee);

			$bedrooms = $xml->createElement('bedrooms',floor($unit['bedrooms']));
			$details->appendChild($bedrooms);

			$bathrooms = $xml->createElement('bathrooms',floor($unit['bathrooms']));
			$details->appendChild($bathrooms);

			$half_baths = $xml->createElement('half_baths',ceil(fmod($unit['bathrooms'],1)));
			$details->appendChild($half_baths);

			if(!strstr(strtolower($unit['number']), 'wait') && array_key_exists($unit['yardi_code'], $unit_yardi_data)) {
				$date_avail = $xml->createElement('dateAvailable', $unit_yardi_data[$unit['yardi_code']]->AvailableDate);
				$details->appendChild($date_avail);
			}

			$yardi_code = $xml->createElement('yardiId',$unit['yardi_code']);
			$details->appendChild($yardi_code);

			$number = $xml->createElement('unitNumber',($unit['feed_number_override'])?$unit['feed_number_override']:$unit['number']);
			$details->appendChild($number);

			$totalrooms = $xml->createElement('totalrooms',$unit['rooms']);
			$details->appendChild($totalrooms);

			$description = $xml->createElement('description');
			$details->appendChild($description);
			$description_data = $xml->createCDATASection($unit['description']);
			$description->appendChild($description_data);

			$property_type = $xml->createElement('propertyType','rental');
			$details->appendChild($property_type);

			$amenities = $xml->createElement('amenities');
			$details->appendChild($amenities);

			//region amenities
			$building_amenities = $this->_amenity_mapper->fetchAmenityNamesByBuildingId($unit['building_id'],'streeteasy_tag');
			$unit_features = $this->_feature_mapper->fetchFeatureNamesByUnitId($unit['id'],'streeteasy_tag');
			$extraAmenities = [];

			foreach ($building_amenities as $amenity) {
				$extraAmenities[] = htmlspecialchars(trim($amenity));
			}
			foreach ($unit_features as $amenity) {
				$extraAmenities[] = htmlspecialchars(trim($amenity));
			}
			$amenities_other = $xml->createElement('other', implode(', ', $extraAmenities));
			$amenities->appendChild($amenities_other);
			//endregion amenities
			//endregion details

			//region walkin_key
			if (!empty($unit['walkin_key'])) {
				$walkin = $xml->createElement('walkin');
				if($unit_elise_text){
				$walkin->nodeValue = $unit_elise_text;
				}
				else{
				$walkin->nodeValue = $unit['walkin_key'];
				}
				
				$property->appendChild($walkin);
			}
			//endregion

			//region open houses
			if ($unit['disable_openhouse'] != 'Yes') {
				$open_houses = $xml->createElement('openHouses');
				$property->appendChild($open_houses);

				$building_open_houses = $this->_openhouse_mapper->fetchByBuildingId($unit['building_id']);
				if ($building_open_houses) {
					foreach ($building_open_houses as $building_open_house) {
						$open_house = $xml->createElement('openHouse');
						$open_houses->appendChild($open_house);

						$open_house_start = $xml->createElement('startsAt', date('Y-m-d g:ia', strtotime($building_open_house->start_time)));
						$open_house->appendChild($open_house_start);

						$open_house_end = $xml->createElement('endsAt', date('Y-m-d g:ia', strtotime($building_open_house->end_time)));
						$open_house->appendChild($open_house_end);
					}
				}
			}
			//endregion

			//region agent
			$agents = $xml->createElement('agents');
			$property->appendChild($agents);

			$agent = $xml->createElement('agent');
			$agents->appendChild($agent);

			$agent_name = $xml->createElement('name','Goldfarb Properties');
			$agent->appendChild($agent_name);

			$agent_company = $xml->createElement('name','Goldfarb Properties');
			$agent->appendChild($agent_company);

			$agent_email = $xml->createElement('email',$unit['region_email']);
			$agent->appendChild($agent_email);

			$agent_phone = $xml->createElement('phone_numbers');
			$agent->appendChild($agent_phone);

			$agent_phone_main = $xml->createElement('main',$unit['region_phone']);
			$agent_phone->appendChild($agent_phone_main);
			//endregion agents

			//region media / floorplans
			$media = $xml->createElement('media');
			$property->appendChild($media);

			$unit_images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit['id']);
			if ($unit_images) {
				foreach ($unit_images as $image) {
					$photo = $xml->createElement('photo');
					$photo->setAttribute('url', $this->_domain . $image->image);
					$photo->setAttribute('description', $image->alt_text);
					$photo->setAttribute('position', $image->sort_order);
					$media->appendChild($photo);
				}
			}

			$unit_floorplan = $this->_floorplan_mapper->find($unit['floorplan_id']);
			if ($unit_floorplan) {
				$floorplan = $xml->createElement('floorplan');
				$floorplan->setAttribute('url',$this->_domain.$unit_floorplan->image);
				$floorplan->setAttribute('description',$image->title);
				$media->appendChild($floorplan);
			}
			//endregion
		}
		$xml->appendChild($main_element);
		$xml->formatOutput = TRUE;

		header('Content-type: text/xml');
		echo $xml->saveXML();
		exit;
		/*
		$buildings = Building::residential()->whereNotNull('yardi_code')->get();
				foreach ($buildings as $building) {
					$properties = explode(',', $building->yardi_code);

					foreach ($properties as $propertyCode) {
						$data = Yardi::getAvailability($propertyCode);
						$building->yardi_data = $data;
					}
				}

		<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>
		<?php
		foreach($buildings as $building){
		  $building->has_vacancy = false;
		  foreach($units as $unit){
			if($unit->building->slug == $building->slug){
			  $building->has_vacancy = true;
			  foreach($building->yardi_data as $yardi_data){
				if($yardi_data['ApartmentId'] == $unit->yardi_code){
				  $unit->date_available = $yardi_data['AvailableDate'];
				  $unit->floorplan_id = $yardi_data['FloorplanId'];
				}
			  }
			}
		  }
		}
		?>
		<streeteasy version="1.6">
		  <properties>
		  @foreach ($units as $unit)
			  <property type="rental" status="active" id="{{$unit->unique_version_id}}" url="{{url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug]))}}">
				<location>
				  <address>{{$unit->getStreeteasyStreetAddress()}}{{(!!$unit->building->address->street_2) ? ',' . $unit->building->address->street_2 : null}}</address>
				  <apartment>{{$unit->getStreeteasyNumber()}}</apartment>
				  <city>{{$unit->building->address->city}}</city>
				  <state>{{$unit->building->address->state}}</state>
				  <zipcode>{{$unit->building->address->zip}}</zipcode>
				  <neighborhood>{{$unit->building->neighborhood->name}}</neighborhood>
				</location>

				<details>
				  <price>{{$unit->net_effective_price}}</price>
				  <noFee/>
				  <bedrooms>{{floor($unit->bedrooms)}}</bedrooms>
				  <bathrooms>{{floor($unit->bathrooms)}}</bathrooms>
				  <half_baths>{{ceil(fmod($unit->bathrooms, 1))}}</half_baths>
				  <?php if(!strstr(strtolower($unit->number), 'wait')) : ?>
				  <dateAvailable>{{$unit->date_available}}</dateAvailable>
				  <?php endif; ?>
				  <yardiId>{{$unit->yardi_code}}</yardiId>
				  <unitNumber>@if ($unit->feed_number_override) {{$unit->feed_number_override}} @else {{$unit->number}} @endif</unitNumber>
				  <totalrooms>{{$unit->rooms}}</totalrooms> <!-- total # rooms in the property -->
				  <description>
				  <![CDATA[@if (!!$unit->getConcessionMessage()) {{$unit->getConcessionMessage()}}<br><br><br>@endif @if (!!$unit->body) Apartment Description:<br>{{$unit->body}}<br><br>@endif @if (!!$unit->building->body) Building Description: <br> {{$unit->building->body}} @endif ]]>
				  </description>
				  <propertyType>rental</propertyType>
				  <amenities>
					  <?php $extraAmenities = []?>
					  @foreach ($unit->building->amenities as $amenity)
						  @if (!!$amenity->feed_meta['streeteasy'])
							  <{{$amenity->feed_meta['streeteasy']}}/>
						  @else
							  <?php $extraAmenities[] = $amenity->name ?>
						  @endif
					  @endforeach
					  @foreach ($unit->features as $amenity)
						  @if (!!$amenity->feed_meta['streeteasy'])
							  <{{$amenity->feed_meta['streeteasy']}}/>
						  @else
							  <?php $extraAmenities[] = $amenity->name ?>
						  @endif
					  @endforeach
					  <other>{{implode(', ', $extraAmenities)}}</other>
					</amenities>
				</details>
				<?php if(!empty($unit['attributes']['walkin_key'])){ ?>
		<walkin><?php echo $unit['attributes']['walkin_key'] ?></walkin>
				<?php } ?>
				{{-- OPEN HOUSES--}}
				@if (count($unit->building->openHouses))
				  @if ($unit->disable_openhouse != 'yes')
				  <openHouses>
					@foreach ($unit->building->openHouses as $openHouse)
					<openHouse>
					  <startsAt>{{$openHouse->start_time->format('Y-m-d g:ia')}}</startsAt>
					  <endsAt>{{$openHouse->end_time->format('Y-m-d g:ia')}}</endsAt>
					</openHouse>
					 @endforeach
				  </openHouses>
				  @endif
				@endif

				<agents>
				  <agent>
					<name>Goldfarb Properties</name>
					<company>Goldfarb Properties</company>
					 <email>{{$unit->building->neighborhood->region->email}}</email>
					 <phone_numbers>
					  <main>{{$unit->building->neighborhood->region->phone}}</main>
					</phone_numbers>
				  </agent>
				</agents>

				<media>
				  @foreach ($unit->getGalleryImages() as $index => $image)
					<photo url="{!!url($image->styleSrc("carousel-slide"))!!}" description="{{$image->title}}" position="{{$index}}" />
				  @endforeach

				  @if (count($unit->floorplan))
					 <?php /**
					 <floorplan url="{{url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug]))}}/floorplan" description="{{$unit->floorplan->title}}"/>
					 * / ?>
		<floorplan url="{{asset($unit->basicFloorplanSrc())}}" description="{{$unit->floorplan->title}}"/>

		@endif

		</media>
		</property>
		@endforeach
		</properties>
		</streeteasy>
		 */
	}

	public function olrAction()
	{
/*
<?php print '<?xml version="1.0" encoding="utf-8" ?>' ?>
<properties>
    @foreach ($units as $unit)
        <property>
            <building>
                <address>{{$unit->building->address->street_1}}{{(!!$unit->building->address->street_2) ? ',' . $unit->building->address->street_2 : null}}</address>
                <city>{{$unit->building->address->city}}</city>
                <state>{{$unit->building->address->state}}></state>
                <zipcode>{{$unit->building->address->zip}}</zipcode>
                <buildingname>{{$unit->building->name}}</buildingname>
                <ownership>R</ownership>
            </building>
            <details>
                <distributiontype>P</distributiontype>
                <saleorrent>R</saleorrent>
                <weblistingid>{{$unit->number}}</weblistingid>
                <aptnumber>{{$unit->number}}</aptnumber>
                <apartmentsize>
                    <?php
                    switch ($unit->bedrooms) {
                        case '0':
                            //studio
                            $apartmentSize = 1;
                            break;

                        case '1':
                            // 1 bedroom
                            $apartmentSize = 7;
                            break;

                        case '2':
                            // 2 bedroom
                            $apartmentSize = 'H';
                            break;

                        case '3':
                            // 3 bedroom
                            $apartmentSize = 'L';
                            break;

                        case '4':
                            // 4 bedroom
                            $apartmentSize = 'M';
                            break;

                        case '5':
                            // 5 bedroom
                            $apartmentSize = 'N';
                            break;

                        default:
                            $apartmentSize = '';
                            break;
                    }

                    echo $apartmentSize;
                    ?>
                </apartmentsize>
                @if (!!$unit->floor)
                    <floor>{{$unit->floor}}</floor>
                @endif
                <numrooms>{{$unit->rooms}}</numrooms>
                <numbedrooms>{{floor($unit->bedrooms)}}</numbedrooms>
                <numbaths>{{$unit->bathrooms}}</numbaths>

                <listingtype>O</listingtype>
                <status>A</status>
                <listeddate>{{$unit->updated_at->toFormattedDateString()}}</listeddate>
                <availabledate>{{$unit->updated_at->toFormattedDateString()}}</availabledate>
                <modifieddate>{{$unit->updated_at->toFormattedDateString()}}</modifieddate>

                <rent>{{$unit->net_effective_price}}</rent>
                <minterm>12</minterm>
                <maxterm>24</maxterm>

                <petpolicy>A</petpolicy>
                {{--         <?php
                          //logic to mirror streeteasy amenities with goldfarb
                          $features = $unit->features->pluck('name')->toArray();
                          $amenities = $unit->building->amenities->pluck('name')->toArray();
                          $amenitiesAndFeatures = array_merge($amenities, $features);

                          //map from gold to streeteasy
                          $amenitiesMap = [
                             'patio' => 'patio',
                             'outdoor space' => 'patio',
                             'balcony' => 'balcony',
                             'washer/dryer' => 'washerdryer',
                             'dishwasher' => 'dishwasher'
                             ];

                          foreach ($amenitiesAndFeatures as $index => $amenity) {
                            if(array_has($amenitiesMap, strtolower($amenity))){
                                $tag = array_get($amenitiesMap, strtolower($amenity));
                                echo "<$tag>true</$tag>";
                              }
                          }
                        ?> --}}
                @if (!!$unit->body)
                    <brokersummary><![CDATA[{{$unit->body}}]]></brokersummary>
                @endif;
                @if (!!$unit->building->body)
                    <buildingnotes><![CDATA[{{$unit->building->body}}]]></buildingnotes>
                @endif
            </details>
            {{--     <agents>
                  @if (count($unit->building->users))
                    @foreach ($unit->building->users as $agent)
                      @if ($agent->role == 'Agent')
                      <agent>
                        <firstname>{{$agent->first_name}}</firstname>
                        <lastname>{{$agent->last_name}}</lastname>
                        <email>{{$agent->email}}</email>
                      </agent>
                      @endif
                    @endforeach
                  @endif
                </agents> --}}

            {{-- OPEN HOUSES--}}
            @if (count($unit->building->openHouses))
                @if ($unit->disable_openhouse != 'yes')
                    <openhouses>
                        @foreach ($unit->building->openHouses as $openHouse)
                            <openhouse>
                                <openhousedate>{{$openHouse->start_time->format('m/d/Y')}}</openhousedate>
                                <starttime>{{$openHouse->start_time->format('g:i')}}</starttime>
                                <endtime>{{$openHouse->end_time->format('g:i')}}</endtime>
                                <partoftour>n</partoftour>
                                <brokeronly>n</brokeronly>
                                <byappointmentonly>n</byappointmentonly>
                                <foodserved>n</foodserved>
                                <comments>{{$openHouse->details}}</comments>
                            </openhouse>
                        @endforeach
                    </openhouses>
                @endif
            @endif

            <photos>
                @foreach ($unit->getGalleryImages() as $image)

                    <media>
                        <url>{!!url($image->styleSrc("carousel-slide"))!!}</url>
                        <caption>{{$image->title}}</caption>
                    </media>
                @endforeach
            </photos>
            @if (count($unit->floorplan))
                <floorplans>
                    <media> <!-- repeatable (max 2 floorplans, jpg/gif only) -->
                        <url>{!!url($unit->floorplan->src())!!}</url>
                        <caption>{{$unit->floorplan->title}}</caption>
                    </media>
                </floorplans>
            @endif
        </property>
    @endforeach
</properties>
 */
	}

	public function zillowAction()
	{
/*
<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>

<Listings>
    @foreach ($units as $unit)
        <?php
        $zillowAmenities = [];
        foreach ($unit->features as $feature) {
            if (isset($feature->feed_meta['zillow'])) {
                $zillowAmenities[] = $feature->feed_meta['zillow'];
            }
        }
        foreach ($unit->building->amenities as $amenity) {
            if (isset($amenity->feed_meta['zillow'])) {
                $zillowAmenities[] = $amenity->feed_meta['zillow'];
            }
        }
        ?>
        <Listing id="{{$unit->unique_version_id}}">
            <Location>
                <StreetAddress>{{$unit->getStreeteasyStreetAddress()}}{{(!!$unit->building->address->street_2) ? ',' . $unit->building->address->street_2 : null}}</StreetAddress>
                <UnitNumber>{{$unit->getStreeteasyNumber()}}</UnitNumber>
                <City>{{$unit->building->address->city}}</City>
                <State>{{$unit->building->address->state}}</State>
                <Zip>{{$unit->building->address->zip}}</Zip>
                <Lat>{{$unit->building->address->LatLng['lat']}}</Lat>
                <Long>{{$unit->building->address->LatLng['lng']}}</Long>
                <DisplayAddress>Yes</DisplayAddress>
            </Location>
            <ListingDetails>
                <Status>For Rent</Status>
                <Price>{{$unit->net_effective_price}}</Price>
                <ListingUrl>{{url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug]))}}</ListingUrl>
                {{-- <MlsId/> --}}
                {{-- <MlsName/> --}}
                {{-- <ProviderListingId/> --}}
                {{-- <ListingEmail/> --}}
                {{-- <AlwaysEmailAgent/> --}}
                <ShortSale>No</ShortSale>
                <REO>No</REO>
                {{-- <ComingSoonOnMarketDate/> --}}
            </ListingDetails>
            <RentalDetails>
                <Availability>{{$unit->updated_at->toDateString()}}</Availability>
                <LeaseTerm>OneYear</LeaseTerm>
                {{-- <DepositFees/> --}}
                <UtilitiesIncluded>
                    <Water>Yes</Water>
                    <Sewage>Yes</Sewage>
                    <Garbage>Yes</Garbage>
                    <Electricity>No</Electricity>
                    <Gas>No</Gas>
                    <Internet>No</Internet>
                    <Cable>No</Cable>
                    <SatTV>No</SatTV>
                </UtilitiesIncluded>
                <PetsAllowed>
                    <NoPets>{{(in_array('NoPets', $zillowAmenities)) ? 'no' : 'yes'}}</NoPets>
                    <Cats>{{(in_array('Cats', $zillowAmenities)) ? 'yes' : 'no'}}</Cats>
                    <SmallDogs>{{(in_array('SmallDogs', $zillowAmenities)) ? 'yes' : 'no'}}</SmallDogs>
                    <LargeDogs>{{(in_array('LargeDogs', $zillowAmenities)) ? 'yes' : 'no'}}</LargeDogs>
                </PetsAllowed>
            </RentalDetails>
            <BasicDetails>
                <PropertyType>Apartment</PropertyType>
                <Title>{{$unit->building->name}}</Title>
                <Description>
                    <![CDATA[@if (!!$unit->body) Apartment Description:<br>{{$unit->body}}
                    <br>@endif @if (!!$unit->building->body) Building Description: <br> {{$unit->building->body}} @endif
                    ]]>
                </Description>
                <Bedrooms>{{floor($unit->bedrooms)}}</Bedrooms>
                <Bathrooms>{{$unit->bathrooms}}</Bathrooms>
                @if ($unit->square_footage_toggle == 'enabled')
                    <LivingArea>{{$unit->square_footage}}</LivingArea>
                @endif
                {{-- <LotSize/> --}}
                {{-- <YearBuilt/> --}}
            </BasicDetails>
            <Pictures>
                @foreach ($unit->getGalleryImages() as $image)
                    <Picture>
                        <PictureUrl>{!!url($image->styleSrc("carousel-slide"))!!}</PictureUrl>
                        <Caption>{{$image->title}}</Caption>
                    </Picture>
                @endforeach
            </Pictures>
            <Agent>
                <FirstName>Goldfarb</FirstName>
                <LastName>Properties</LastName>
                <EmailAddress>{{$unit->building->neighborhood->region->email}}</EmailAddress>
                {{-- <PictureUrl/> --}}
                <OfficeLineNumber>{{$unit->building->neighborhood->region->phone}}</OfficeLineNumber>
                {{-- <MobilePhoneLineNumber/> --}}
                {{-- <FaxLineNumber/> --}}
                {{-- <LicenseNum/> --}}
            </Agent>
            {{-- <Office> --}}
            {{-- <BrokerageName/> --}}
            {{-- <BrokerPhone/> --}}
            {{-- <BrokerEmail/> --}}
            {{-- <BrokerWebsite/> --}}
            {{-- <StreetAddress/> --}}
            {{-- <UnitNumber/> --}}
            {{-- <City/> --}}
            {{-- <State/> --}}
            {{-- <Zip/> --}}
            {{-- <OfficeName/> --}}
            {{-- <FranchiseName/> --}}
            {{-- </Office>  --}}

            @if (count($unit->building->openHouses))
                @if ($unit->disable_openhouse != 'yes')
                    <OpenHouses>
                        @foreach ($unit->building->openHouses as $openHouse)
                            <OpenHouse>
                                <Date>{{$openHouse->start_time->toDateString()}}</Date>
                                <StartTime>{{$openHouse->start_time->format('H:i')}}</StartTime>
                                <EndTime>{{$openHouse->end_time->format('H:i')}}</EndTime>
                            </OpenHouse>
                        @endforeach
                    </OpenHouses>
                @endif
            @endif
            {{-- <Fees>
                <Fee>
                    <FeeType/>
                    <FeeAmount/>
                    <FeePeriod/>
                </Fee>
            </Fees> --}}
            <Neighborhood>
                <Name>{{$unit->building->neighborhood->name}}</Name>
                <Description><![CDATA[ {{$unit->building->neighborhood->body}} ]]></Description>
            </Neighborhood>
            <RichDetails>
                <CableReady>Yes</CableReady>
                <DisabledAccess>{{(in_array('DisabledAccess', $zillowAmenities)) ? 'yes' : 'no'}}</DisabledAccess>
                <Doorman>{{(in_array('Doorman', $zillowAmenities)) ? 'yes' : 'no'}}</Doorman>
                <Elevator>{{(in_array('Elevator', $zillowAmenities)) ? 'yes' : 'no'}}</Elevator>
                <Patio>{{(in_array('Patio', $zillowAmenities)) ? 'yes' : 'no'}}</Patio>
                <Pool>{{(in_array('Pool', $zillowAmenities)) ? 'yes' : 'no'}}</Pool>
                <RoomCount>{{$unit->rooms}}</RoomCount>
                <Waterfront>{{(in_array('Waterfront', $zillowAmenities)) ? 'yes' : 'no'}}</Waterfront>
                <FitnessCenter>{{(in_array('FitnessCenter', $zillowAmenities)) ? 'yes' : 'no'}}</FitnessCenter>
                <NearTransportation>{{(in_array('NearTransportation', $zillowAmenities)) ? 'yes' : 'no'}}</NearTransportation>
                <PropertyName>{{$unit->building->name}}</PropertyName>
                <Fireplace>{{(in_array('Fireplace', $zillowAmenities)) ? 'yes' : 'no'}}</Fireplace>
                <Garden>{{(in_array('Garden', $zillowAmenities)) ? 'yes' : 'no'}}</Garden>
                <OnsiteLaundry>{{(in_array('OnsiteLaundry', $zillowAmenities)) ? 'yes' : 'no'}}</OnsiteLaundry>
            </RichDetails>
        </Listing>
    @endforeach
</Listings>
 */
	}

	/**
	 * converted: Completed [XML]
	 */
	public function apartmentsAction()
	{
		try {
			$buildings = $this->getBuildingsResidentialRentals();
		} catch (Exception $e) {
			echo $e->getMessage()."\n";
			die;
		}
		$display = ['unavailable' => [], 'occupied' => [],];
		foreach($buildings as $building) {
			$all = [];
			$active = "";
			$units = $this->_unit_mapper->fetchUnitsByBuildingId($building->id);
			if ($units) {
				foreach($units as $unit) {
					$all[] .= $unit->id;
				}
				$sql = "SELECT * FROM gf_audits WHERE type = 'goldfarb' AND status = 'available' 
							AND created >= DATE_ADD(NOW(), INTERVAL -3 DAY) AND unit_id IN ('".implode("','", $all)."') 
						ORDER BY unit_id ASC, created DESC;";
				$listed = $this->_db->fetchAll($sql);
				$func = function($obj) {
					return $obj->unit_id;
				};
				$unique = array_unique(array_map($func, $listed));

				foreach($unique as $id) {
					$active .= "'".$id."', ";
				}
				$active = substr_replace($active, '', -2);
			}
			if($active) {
				foreach ($unique as $id) {
					$sql = "SELECT * FROM gf_audits WHERE type = 'goldfarb' AND status = 'available'  AND created >= DATE_ADD(NOW(), INTERVAL -3 DAY) AND unit_id = '".$id."' ORDER BY created DESC LIMIT 1;";
					$available = $this->_db->fetchAll($sql);
					$sql = "SELECT * FROM gf_audits WHERE type = 'goldfarb' AND status = 'unavailable' AND created >= DATE_ADD(NOW(), INTERVAL -3 DAY) AND unit_id = '".$id."' ORDER BY created DESC LIMIT 1;";
					$unavailable = $this->_db->fetchAll($sql);
					$sql = "SELECT * FROM gf_audits WHERE type = 'goldfarb' AND status = 'occupied'  AND created >= DATE_ADD(NOW(), INTERVAL -3 DAY) AND unit_id = '".$id."' ORDER BY created DESC LIMIT 1;";
					$occupied = $this->_db->fetchAll($sql);
					if($available && $unavailable && $available[0]->id < $unavailable[0]->id) {
						array_push($display['unavailable'], $id);
					}
					if($available && $occupied && $available[0]->id < $occupied[0]->id) {
						array_push($display['occupied'], $id);
					}
				}
			}
		}
		$xml = new DomDocument('1.0', 'UTF-8');
		$main_element = $xml->createElement('PhysicalProperty');
		$management = $xml->createElement('Management');
		$management->setAttribute('IDValue', '1');
		$management->setAttribute('OrganizationName', 'Goldfarb Properties');
		$management->setAttribute('IDType', 'managmentID');
		$main_element->appendChild($management);

		foreach ($buildings as $building) {
			//region Property
			$property = $xml->createElement('Property');

			//region PropertyID
			$property_id = $xml->createElement('PropertyID');

			$identification = $xml->createElement('Identification');
			$identification->setAttribute('IDValue', $building->id);
			$identification->setAttribute('IDRank', 'primary');
			$property_id->appendChild($identification);

			$marketing_name = $xml->createElement('MarketingName');
			$marketing_name->nodeValue = $building->name;
			$property_id->appendChild($marketing_name);

			$web_site = $xml->createElement('WebSite');
			$web_site->nodeValue = $this->_domain."/rentals/{$building->region->code}/{$building->code}";
			$property_id->appendChild($web_site);

			$address = $xml->createElement('Address');
			$address->setAttribute('AddressType', 'property');

			$address_line_1 = $xml->createElement('AddressLine1');
			$address_line_1->nodeValue = $building->street1;
			$address->appendChild($address_line_1);

			$address_line_2 = $xml->createElement('AddressLine2');
			$address_line_2->nodeValue = $building->street2;
			$address->appendChild($address_line_2);

			$city = $xml->createElement('City');
			$city->nodeValue = $building->city;
			$address->appendChild($city);

			$state = $xml->createElement('State');
			$state->nodeValue = $building->state;
			$address->appendChild($state);

			$postal_code = $xml->createElement('PostalCode');
			$postal_code->nodeValue = $building->zip;
			$address->appendChild($postal_code);

			$property_id->appendChild($address);
			$property->appendChild($property_id);
			//endregion


			//region ILS_Identification
			$ils_identification = $xml->createElement('ILS_Identification');
			$ils_identification->setAttribute('ILS_IdentificationType', 'Apartment');
			$ils_identification->setAttribute('RentalType', 'Unspecified');
			$ils_identification->nodeValue = '';
			$property->appendChild($ils_identification);
			//endregion

			//region Information
			$information = $xml->createElement('Information');
			$short_description = $xml->createElement('ShortDescription');
			$short_description->appendChild($xml->createCDATASection($building->description));
			$information->appendChild($short_description);
			$long_description = $xml->createElement('LongDescription');
			$long_description->appendChild($xml->createCDATASection($building->description));
			$information->appendChild($long_description);
			$property->appendChild($information);
			//endregion

			//region Amenities
			$amenities = $this->_amenity_mapper->fetchAmenityNameTagPairsByBuildingId($building->id,'apartmentscom_tag');
			if ($amenities) {
				foreach ($amenities as $amenity) {
					$amenity_tag = $xml->createElement('Amenity');
					$amenity_tag->setAttribute('AmenityType', (!empty($amenity['tag']))?$amenity['tag']:'Other');
					$description = $xml->createElement('Description');
					$description->nodeValue = htmlspecialchars($amenity['name']);
					$amenity_tag->appendChild($description);
					$property->appendChild($amenity_tag);
				}
			}
			//endregion

			$units = $this->_unit_mapper->fetchAvailableUnitsByBuildingId($building->id);
			if ($units) {
				//region Floorplan
				foreach ($units as $unit) {
					$unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($unit->id);
					$unit_streeteasy_number = $this->_unit_mapper->streeteasyApartmentNumber($unit);

					$floorplan = $xml->createElement('Floorplan');
					$floorplan->setAttribute('IDValue', $unit->id);
					$floorplan->setAttribute('IDType', 'FloorplanID');
					$floorplan->setAttribute('IDRank', 'primary');

					//region FloorplanType
					$floorplan_type = $xml->createElement('FloorplanType');
					$floorplan_type->nodeValue = 'Internal';
					$floorplan->appendChild($floorplan_type);
					//endregion

					//region Name
					$name = $xml->createElement('Name');
					$name->nodeValue = 'Apartment '.$unit_streeteasy_number;
					$floorplan->appendChild($name);
					//endregion

					//region Comment
					$comment = $xml->createElement('Comment');
					$comment->appendChild($xml->createCDATASection($unit->description));
					$floorplan->appendChild($comment);
					//endregion

					//region UnitCount
					$unit_count = $xml->createElement('UnitCount');
					$unit_count->nodeValue = 1;
					$floorplan->appendChild($unit_count);
					//endregion

					//region UnitsAvailable
					$unit_available = $xml->createElement('UnitsAvailable');
					$unit_available->nodeValue = 1;
					$floorplan->appendChild($unit_available);
					//endregion

					//region Room - Bedroom
					$room = $xml->createElement('Room');
					$room->setAttribute('RoomType', 'Bedroom');
					$room_count = $xml->createElement('Count');
					$room_count->nodeValue = floor($unit->bedrooms);
					$room->appendChild($room_count);
					$floorplan->appendChild($room);
					//endregion

					//region Room - Bathroom
					$room = $xml->createElement('Room');
					$room->setAttribute('RoomType', 'Bathroom');
					$room_count = $xml->createElement('Count');
					$room_count->nodeValue = floor($unit->bathrooms);
					$room->appendChild($room_count);
					$floorplan->appendChild($room);
					//endregion

					//region EffectiveRent
					$effective_rent = $xml->createElement('EffectiveRent');
					$effective_rent->setAttribute('min', $unit_net_effective_price);
					$effective_rent->setAttribute('max', $unit_net_effective_price);
					$floorplan->appendChild($effective_rent);
					//endregion

					//region Unit Features (aka Amenity)
					$features = $this->_feature_mapper->fetchFeatureNameTagPairsByUnitId($unit->id,'apartmentscom_tag');
					if ($features) {
						foreach ($features as $feature) {
							$feature_tag = $xml->createElement('Amenity');
							$feature_tag->setAttribute('AmenityType', (!empty($feature['tag']))?$feature['tag']:'Other');
							$description = $xml->createElement('Description');
							$description->nodeValue = htmlspecialchars($feature['name']);
							$feature_tag->appendChild($description);
							$floorplan->appendChild($feature_tag);
						}
					}
					//endregion

					//region Gallery Images
					$images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit->id);
					if ($images) {
						foreach ($images as $index => $image) {
							$file = $xml->createElement('File');
							$file->setAttribute('FileID', $image->id);
							$file->setAttribute('Active', 'true');
							$file_type = $xml->createElement('FileType');
							$file_type->nodeValue = 'Photo';
							$file->appendChild($file_type);

							$caption = $xml->createElement('Caption');
							$captionamenity = $xml->createTextNode(''); //$image->title
							$caption->appendChild($captionamenity);

							$file->appendChild($caption);
							$src = $xml->createElement('Src');
							$src->nodeValue = $this->_domain.$image->image;
							$file->appendChild($src);
							$rank = $xml->createElement('Rank');
							$rank->nodeValue = $index;
							$file->appendChild($src);
							$floorplan->appendChild($file);
						}
					}
					//endregion

					//region Floorplan Images
					if (!empty($unit->floorplan_id)) {
						/** @var Custom_Model_GfFloorplan $floorplan_entity */
						$floorplan_entity = $this->_floorplan_mapper->find($unit->floorplan_id);
						if ($floorplan_entity) {
							$file = $xml->createElement('File');
							$file->setAttribute('FileID', $unit->floorplan_id);
							$file->setAttribute('Active', 'true');

							$file_type = $xml->createElement('FileType');
							$file_type->nodeValue = 'Floorplan';
							$file->appendChild($file_type);

							$caption = $xml->createElement('Caption');
							$captionamenity = $xml->createTextNode('');//$floorplan_entity->title
							$caption->appendChild($captionamenity);
							$file->appendChild($caption);

							$src = $xml->createElement('Src');
							$src->nodeValue = $floorplan_entity->image;
							$file->appendChild($src);

							$rank = $xml->createElement('Rank');
							$rank->nodeValue = 0;
							$file->appendChild($rank);
							$floorplan->appendChild($file);
						}
					}
					//endregion

					$property->appendChild($floorplan);
				}
				//endregion

				reset($units);

				//region ILS_Unit
				foreach ($units as $unit) {
					$unit_streeteasy_number = ($unit->feed_number_override)?$unit->feed_number_override:$unit->number;
					$unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($unit->id);

					$persist = in_array($unit->id, $display)? true : false;
					if(in_array($unit->id, $display['unavailable'])) {
						$unit_lease_status = 'unavailable';
					} else if(in_array($unit->id, $display['occupied'])) {
						$unit_lease_status = 'occupied';
					} else {
						$unit_lease_status = 'available';
					}
					$ils_unit = $xml->createElement('ILS_Unit');
					$ils_unit->setAttribute('IDValue', $unit_streeteasy_number);
					$ils_unit->setAttribute('IDType', 'UnitID');
					$ils_unit->setAttribute('IDRank', 'primary');

					$units_tag = $xml->createElement('Units');
					$unit_tag = $xml->createElement('Unit');

					//region Identification
					$identification = $xml->createElement('Identification');
					$identification->setAttribute('IDValue', $unit_streeteasy_number);
					$identification->setAttribute('IDType', 'UnitID');
					$identification->setAttribute('IDRank', 'primary');
					$unit_tag->appendChild($identification);
					//endregion

					//region Identification
					$identification = $xml->createElement('Identification');
					$identification->setAttribute('IDValue', $unit->id);
					$identification->setAttribute('IDType', 'FloorplanID');
					$identification->setAttribute('IDRank', 'primary');
					$unit_tag->appendChild($identification);
					//endregion

					//region MarketingName
					$marketing_name = $xml->createElement('MarketingName');
					$marketing_name->nodeValue = $unit_streeteasy_number;
					$unit_tag->appendChild($marketing_name);
					//endregion

					//region UnitBedrooms
					$unit_bedrooms = $xml->createElement('UnitBedrooms');
					$unit_bedrooms->nodeValue = $unit->bedrooms;
					$unit_tag->appendChild($unit_bedrooms);
					//endregion

					//region UnitBathrooms
					$unit_bathrooms = $xml->createElement('UnitBathrooms');
					$unit_bathrooms->nodeValue = $unit->bathrooms;
					$unit_tag->appendChild($unit_bathrooms);
					//endregion

					//region UnitRent
					$unit_rent = $xml->createElement('UnitRent');
					$unit_rent->nodeValue = $unit_net_effective_price;
					$unit_tag->appendChild($unit_rent);
					//endregion

					//region UnitLeasedStatus
					$unit_leased_status_tag = $xml->createElement('UnitLeasedStatus');
					$unit_leased_status_tag->nodeValue = $unit_lease_status;
					$unit_tag->appendChild($unit_leased_status_tag);
					//endregion

					$units_tag->appendChild($unit_tag);
					$ils_unit->appendChild($units_tag);

					//region Comment
					$comment = $xml->createElement('Comment');
					$comment->appendChild($xml->createCDATASection($unit->description));
					$ils_unit->appendChild($comment);
					//endregion

					//region EffectiveRent
					$effective_rent = $xml->createElement('EffectiveRent');
					$effective_rent->setAttribute('min', $unit_net_effective_price);
					$effective_rent->setAttribute('max', $unit_net_effective_price);
					$ils_unit->appendChild($effective_rent);
					//endregion

					//region Unit Features (aka Amenity)
					$features = $this->_feature_mapper->fetchFeatureNameTagPairsByUnitId($unit->id,'apartmentscom_tag');
					if ($features) {
						foreach ($features as $feature) {
							$feature_tag = $xml->createElement('Amenity');
							$feature_tag->setAttribute('AmenityType', (!empty($feature['tag']))?$feature['tag']:'Other');
							$description = $xml->createElement('Description');
							$description->nodeValue = htmlspecialchars($feature['name']);
							$feature_tag->appendChild($description);
							$ils_unit->appendChild($feature_tag);
						}
					}
					//endregion

					//region Gallery Images
					$images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit->id);
					if ($images) {
						foreach ($images as $index => $image) {
							$file = $xml->createElement('File');
							$file->setAttribute('FileID', $image->id);
							$file->setAttribute('Active', 'true');
							$file_type = $xml->createElement('FileType');
							$file_type->nodeValue = 'Photo';
							$file->appendChild($file_type);

							$caption = $xml->createElement('Caption');
							$captionamenity = $xml->createTextNode(''); //$image->title
							$caption->appendChild($captionamenity);

							$file->appendChild($caption);
							$src = $xml->createElement('Src');
							$src->nodeValue = $this->_domain.$image->image;
							$file->appendChild($src);
							$rank = $xml->createElement('Rank');
							$rank->nodeValue = $index;
							$file->appendChild($rank);
							$ils_unit->appendChild($file);
						}
					}
					//endregion

					$property->appendChild($ils_unit);
				}
				//endregion
			}

			//region Building Gallery Images
			$building_image_mapper = new Custom_Model_Mapper_GfBuildingImage();
			$images = $building_image_mapper->fetchOrderedByBuildingId($building->id);
			if ($images) {
				foreach ($images as $index => $image) {
					$file = $xml->createElement('File');
					$file->setAttribute('FileID', $image->id);
					$file->setAttribute('Active', 'true');
					$file_type = $xml->createElement('FileType');
					$file_type->nodeValue = 'Photo';
					$file->appendChild($file_type);

					$caption = $xml->createElement('Caption');
					$captionamenity = $xml->createTextNode(''); //$image->title
					$caption->appendChild($captionamenity);

					$file->appendChild($caption);
					$src = $xml->createElement('Src');
					$src->nodeValue = $this->_domain.$image->image;
					$file->appendChild($src);
					$rank = $xml->createElement('Rank');
					$rank->nodeValue = $index;
					$file->appendChild($src);
					$property->appendChild($file);
				}
			}
			//endregion

			$main_element->appendChild($property);
			//endregion
		}
		$xml->appendChild($main_element);
		$xml->formatOutput = TRUE;

		header('Content-type: text/xml');
		echo $xml->saveXML();
		exit;
		/*
		$buildings = Building::rentals()->with('neighborhood', 'address', 'images', 'units', 'openHouses')->get();
		$display = [];
		$display['unavailable'] = [];
		$display['occupied'] = [];
		foreach($buildings as $building) {
			$all = "";
			$active = "";
			foreach($building->units()->rentals()->get() as $unit) {
				$all .= "'".$unit->id."', ";
			}
			$all = substr_replace($all, '', -2);
			$listed = DB::select("SELECT * FROM audits WHERE type = 'goldfarb' AND status = 'available'  AND created_at >= DATE_ADD(NOW(), INTERVAL -3 DAY) AND unit_id IN (".$all.") ORDER BY unit_id ASC, created_at DESC;");

			$func = function($obj) {
					return $obj->unit_id;
				};
			$unique = array_unique(array_map($func, $listed));

			foreach($unique as $id) {
				$active .= "'".$id."', ";
			}
			$active = substr_replace($active, '', -2);
			if($active) {
				foreach ($unique as $id) {
					$available = DB::select("SELECT * FROM audits WHERE type = 'goldfarb' AND status = 'available'  AND created_at >= DATE_ADD(NOW(), INTERVAL -3 DAY) AND unit_id = '".$id."' ORDER BY created_at DESC LIMIT 1;");
					$unavailable = DB::select("SELECT * FROM audits WHERE type = 'goldfarb' AND status = 'unavailable' AND created_at >= DATE_ADD(NOW(), INTERVAL -3 DAY) AND unit_id = '".$id."' ORDER BY created_at DESC LIMIT 1;");
					$occupied = DB::select("SELECT * FROM audits WHERE type = 'goldfarb' AND status = 'occupied'  AND created_at >= DATE_ADD(NOW(), INTERVAL -3 DAY) AND unit_id = '".$id."' ORDER BY created_at DESC LIMIT 1;");
					if($available && $unavailable && $available[0]->id < $unavailable[0]->id) {
					   array_push($display['unavailable'], $id);
					}
					if($available && $occupied && $available[0]->id < $occupied[0]->id) {
					   array_push($display['occupied'], $id);
					}
				}

			}
		}

		<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>
		<PhysicalProperty>
			<Management IDValue="1" OrganizationName="Goldfarb Properties" IDType="managmentID"/>
			@foreach ($buildings as $building)
				<Property>
					<PropertyID>
						<Identification IDValue="{{$building->id}}" IDRank="primary"/>
						<MarketingName>{{$building->name}}</MarketingName>
						<WebSite>{{url(route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug, 'buildingSlug' => $building->slug]))}}</WebSite>
						<Address AddressType="property">
							<AddressLine1>{{$building->address->street_1}}</AddressLine1>
							<AddressLine2>{{$building->address->street_2}}</AddressLine2>
							<City>{{$building->address->city}}</City>
							<State>{{$building->address->state}}</State>
							<PostalCode>{{$building->address->zip}}</PostalCode>
						</Address>
					</PropertyID>
					<ILS_Identification ILS_IdentificationType="Apartment" RentalType="Unspecified">
					</ILS_Identification>
					<Information>
						<ShortDescription><![CDATA[ {!!$building->body!!}]]></ShortDescription>
						<LongDescription><![CDATA[ {!!$building->body!!}]]></LongDescription>
					</Information>
					@foreach ($building->amenities as $amenity)
						<Amenity AmenityType="{{$amenity->feed_meta['apartments-com'] or 'Other'}}">
							<Description>{{$amenity->name}}</Description>
						</Amenity>
					@endforeach
					@foreach ($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get() as $unit)
						<Floorplan IDValue="{{$unit->id}}" IDType="FloorplanID" IDRank="primary">
							<FloorplanType>Internal</FloorplanType>
							<Name>Apartment {{$unit->getStreeteasyNumber()}}</Name>
							<Comment><![CDATA[ {!!$unit->body!!}]]></Comment>
							<UnitCount>1</UnitCount>
							<UnitsAvailable>1</UnitsAvailable>
							<Room RoomType="Bedroom">
								<Count>{{floor($unit->bedrooms)}}</Count>
							</Room>
							<Room RoomType="Bathroom">
								<Count>{{$unit->bathrooms}}</Count>
							</Room>
							<EffectiveRent min="{{$unit->net_effective_price}}" max="{{$unit->net_effective_price}}"/>
							@foreach ($unit->features as $feature)
								<Amenity AmenityType="{{$feature->feed_meta['apartments-com'] or 'Other'}}">
									<Description>{{$feature->name}}</Description>
								</Amenity>
							@endforeach
							@foreach ($unit->getGalleryImages() as $index => $image)
								<File FileID="{{$image->id}}" Active="true">
									<FileType>Photo</FileType>
									<Caption>{{$image->title}}</Caption>
									<Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
									<Rank>{{$index}}</Rank>
								</File>
							@endforeach
							@if (count($unit->floorplan))
								<File FileID="{{$unit->floorplan->id}}" Active="false">
									<FileType>Floorplan</FileType>
									<Caption>{{$unit->floorplan->title}}</Caption>
									<Src>{!!url($unit->floorplanSrc())!!}</Src>
									<Rank>0</Rank>
								</File>
							@endif
						</Floorplan>
					@endforeach
					@foreach ($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get() as $unit)
						<?php $persist = in_array($unit->id, $display)? true : false; ?>
						<ILS_Unit IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID" IDRank="primary">
							<Units>
								<Unit>
									<Identification IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID"
													IDRank="primary"/>
									<Identification IDValue="{{$unit->id}}" IDType="FloorplanID" IDRank="primary"/>
									<MarketingName>{{$unit->getStreeteasyNumber()}}</MarketingName>
									<UnitBedrooms>{{$unit->bedrooms}}</UnitBedrooms>
									<UnitBathrooms>{{$unit->bathrooms}}</UnitBathrooms>
									<UnitRent>{{$unit->net_effective_price}}</UnitRent>
									<?php if(in_array($unit->id, $display['unavailable'])) { ?>
										<UnitLeasedStatus>unavailable</UnitLeasedStatus>
									<?php } else if(in_array($unit->id, $display['occupied'])) { ?>
										<UnitLeasedStatus>occupied</UnitLeasedStatus>
									<?php } else { ?>
										<UnitLeasedStatus>available</UnitLeasedStatus>
									<?php } ?>
								</Unit>
							</Units>
							<Comment><![CDATA[ {!!$unit->body!!}]]></Comment>
							<EffectiveRent Min="{{$unit->net_effective_price}}" Max="{{$unit->net_effective_price}}"/>
							@foreach ($unit->features as $feature)
								<Amenity AmenityType="{{$feature->feed_meta['apartments-com'] or 'Other'}}">
									<Description>{{$feature->name}}</Description>
								</Amenity>
							@endforeach
							@foreach ($unit->getGalleryImages() as $index => $image)
								<File FileID="{{$image->id}}" Active="true">
									<FileType>Photo</FileType>
									<Caption>{{$image->title}}</Caption>
									<Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
									<Rank>{{$index}}</Rank>
								</File>
							@endforeach
						</ILS_Unit>
					@endforeach
					@foreach ($building->getGalleryImages() as $index => $image)
						<File FileID="{{$image->id}}" Active="true">
							<FileType>Photo</FileType>
							<Caption>{{$image->title}}</Caption>
							<Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
							<Rank>{{$index}}</Rank>
						</File>
					@endforeach
				</Property>
			@endforeach
		</PhysicalProperty>

		 */
	}

	/**
	 * converted: Completed [XML]
	 */
	public function apartmentlistAction()
	{
		try {
			$buildings = $this->getBuildingsResidentialRentals();
		} catch (Exception $e) {
			echo $e->getMessage()."\n";
			die;
		}

		$xml = new DomDocument('1.0', 'UTF-8');
		$main_element = $xml->createElement('PhysicalProperty');
		$management = $xml->createElement('Management');
		$management->setAttribute('IDValue', '1');
		$management->setAttribute('OrganizationName', 'Goldfarb Properties');
		$management->setAttribute('IDType', 'managmentID');
		$main_element->appendChild($management);

		foreach ($buildings as $building) {
			//region Property
			$property = $xml->createElement('Property');

			//region PropertyID
			$property_id = $xml->createElement('PropertyID');

			$identification = $xml->createElement('Identification');
			$identification->setAttribute('IDValue', $building->id);
			$identification->setAttribute('IDRank', 'primary');
			$property_id->appendChild($identification);

			$marketing_name = $xml->createElement('MarketingName');
			$marketing_name->nodeValue = $building->name;
			$property_id->appendChild($marketing_name);

			$web_site = $xml->createElement('WebSite');
			$web_site->nodeValue = $this->_domain."/rentals/{$building->region->code}/{$building->code}";
			$property_id->appendChild($web_site);

			$address = $xml->createElement('Address');
			$address->setAttribute('AddressType', 'property');

			$address_line_1 = $xml->createElement('AddressLine1');
			$address_line_1->nodeValue = $building->street1;
			$address->appendChild($address_line_1);

			$address_line_2 = $xml->createElement('AddressLine2');
			$address_line_2->nodeValue = $building->street2;
			$address->appendChild($address_line_2);

			$city = $xml->createElement('City');
			$city->nodeValue = $building->city;
			$address->appendChild($city);

			$state = $xml->createElement('State');
			$state->nodeValue = $building->state;
			$address->appendChild($state);

			$postal_code = $xml->createElement('PostalCode');
			$postal_code->nodeValue = $building->zip;
			$address->appendChild($postal_code);

			$property_id->appendChild($address);
			$property->appendChild($property_id);
			//endregion

			//region ILS_Identification
			$ils_identification = $xml->createElement('ILS_Identification');
			$ils_identification->setAttribute('ILS_IdentificationType', 'Apartment');
			$ils_identification->setAttribute('RentalType', 'Unspecified');
			$ils_identification->nodeValue = '';
			$property->appendChild($ils_identification);
			//endregion

			//region Information
			$information = $xml->createElement('Information');
			$short_description = $xml->createElement('ShortDescription');
			$short_description->appendChild($xml->createCDATASection($building->description));
			$information->appendChild($short_description);
			$long_description = $xml->createElement('LongDescription');
			$long_description->appendChild($xml->createCDATASection($building->description));
			$information->appendChild($long_description);
			$property->appendChild($information);
			//endregion

			//region Amenities
			$amenities = $this->_amenity_mapper->fetchAmenityNameTagPairsByBuildingId($building->id,'apartmentscom_tag');
			if ($amenities) {
				foreach ($amenities as $amenity) {
					$amenity_tag = $xml->createElement('Amenity');
					$amenity_tag->setAttribute('AmenityType', (!empty($amenity['tag']))?$amenity['tag']:'Other');
					$description = $xml->createElement('Description');
					$description->nodeValue = htmlspecialchars($amenity['name']);
					$amenity_tag->appendChild($description);
					$property->appendChild($amenity_tag);
				}
			}
			//endregion

			$units = $this->_unit_mapper->fetchAvailableUnitsByBuildingId($building->id);
			if ($units) {
				//region Floorplan
				foreach ($units as $unit) {
					$unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($unit->id);
					$unit_streeteasy_number = ($unit->feed_number_override)?$unit->feed_number_override:$unit->number;

					$floorplan = $xml->createElement('Floorplan');
					$floorplan->setAttribute('IDValue', $unit->id);
					$floorplan->setAttribute('IDType', 'FloorplanID');
					$floorplan->setAttribute('IDRank', 'primary');

					//region FloorplanType
					$floorplan_type = $xml->createElement('FloorplanType');
					$floorplan_type->nodeValue = 'Internal';
					$floorplan->appendChild($floorplan_type);
					//endregion

					//region Name
					$name = $xml->createElement('Name');
					$name->nodeValue = 'Apartment '.$unit_streeteasy_number;
					$floorplan->appendChild($name);
					//endregion

					//region Comment
					$comment = $xml->createElement('Comment');
					$comment->appendChild($xml->createCDATASection($unit->description));
					$floorplan->appendChild($comment);
					//endregion

					//region UnitCount
					$unit_count = $xml->createElement('UnitCount');
					$unit_count->nodeValue = 1;
					$floorplan->appendChild($unit_count);
					//endregion

					//region UnitsAvailable
					$unit_available = $xml->createElement('UnitsAvailable');
					$unit_available->nodeValue = 1;
					$floorplan->appendChild($unit_available);
					//endregion

					//region Room - Bedroom
					$room = $xml->createElement('Room');
					$room->setAttribute('RoomType', 'Bedroom');
					$room_count = $xml->createElement('Count');
					$room_count->nodeValue = floor($unit->bedrooms);
					$room->appendChild($room_count);
					$floorplan->appendChild($room);
					//endregion

					//region Room - Bathroom
					$room = $xml->createElement('Room');
					$room->setAttribute('RoomType', 'Bathroom');
					$room_count = $xml->createElement('Count');
					$room_count->nodeValue = floor($unit->bathrooms);
					$room->appendChild($room_count);
					$floorplan->appendChild($room);
					//endregion

					//region EffectiveRent
					$effective_rent = $xml->createElement('EffectiveRent');
					$effective_rent->setAttribute('min', $unit_net_effective_price);
					$effective_rent->setAttribute('max', $unit_net_effective_price);
					$floorplan->appendChild($effective_rent);
					//endregion

					//region Unit Features (aka Amenity)
					$features = $this->_feature_mapper->fetchFeatureNameTagPairsByUnitId($unit->id,'apartmentscom_tag');
					if ($features) {
						foreach ($features as $feature) {
							$feature_tag = $xml->createElement('Amenity');
							$feature_tag->setAttribute('AmenityType', (!empty($feature['tag']))?$feature['tag']:'Other');
							$description = $xml->createElement('Description');
							$description->nodeValue = htmlspecialchars($feature['name']);
							$feature_tag->appendChild($description);
							$floorplan->appendChild($feature_tag);
						}
					}
					//endregion

					//region Gallery Images
					$images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit->id);
					if ($images) {
						foreach ($images as $index => $image) {
							$file = $xml->createElement('File');
							$file->setAttribute('FileID', $image->id);
							$file->setAttribute('Active', 'true');
							$file_type = $xml->createElement('FileType');
							$file_type->nodeValue = 'Photo';
							$file->appendChild($file_type);

							$caption = $xml->createElement('Caption');
							$captionamenity = $xml->createTextNode($image->title);
							$caption->appendChild($captionamenity);

							$file->appendChild($caption);
							$src = $xml->createElement('Src');
							$src->nodeValue = $this->_domain.$image->image;
							$file->appendChild($src);
							$rank = $xml->createElement('Rank');
							$rank->nodeValue = $index;
							$file->appendChild($src);
							$floorplan->appendChild($file);
						}
					}
					//endregion

					//region Floorplan Images
					if (!empty($unit->floorplan_id)) {
						/** @var Custom_Model_GfFloorplan $floorplan_entity */
						$floorplan_entity = $this->_floorplan_mapper->find($unit->floorplan_id);
						if ($floorplan_entity) {
							$file = $xml->createElement('File');
							$file->setAttribute('FileID', $unit->floorplan_id);
							$file->setAttribute('Active', 'true');

							$file_type = $xml->createElement('FileType');
							$file_type->nodeValue = 'Floorplan';
							$file->appendChild($file_type);

							$caption = $xml->createElement('Caption');
							$captionamenity = $xml->createTextNode($floorplan_entity->title);
							$caption->appendChild($captionamenity);
							$file->appendChild($caption);

							$src = $xml->createElement('Src');
							$src->nodeValue = $floorplan_entity->image;
							$file->appendChild($src);

							$rank = $xml->createElement('Rank');
							$rank->nodeValue = 0;
							$file->appendChild($src);
							$floorplan->appendChild($file);
						}
					}
					//endregion

					$property->appendChild($floorplan);
				}
				//endregion

				reset($units);

				//region ILS_Unit
				foreach ($units as $unit) {
					$unit_streeteasy_number = ($unit->feed_number_override)?$unit->feed_number_override:$unit->number;
					$unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($unit->id);

					$ils_unit = $xml->createElement('ILS_Unit');
					$ils_unit->setAttribute('IDValue', $unit_streeteasy_number);
					$ils_unit->setAttribute('IDType', 'UnitID');
					$ils_unit->setAttribute('IDRank', 'primary');

					$units_tag = $xml->createElement('Units');
					$unit_tag = $xml->createElement('Unit');

					//region Identification
					$identification = $xml->createElement('Identification');
					$identification->setAttribute('IDValue', $unit_streeteasy_number);
					$identification->setAttribute('IDType', 'UnitID');
					$identification->setAttribute('IDRank', 'primary');
					$unit_tag->appendChild($identification);
					//endregion

					//region Identification
					$identification = $xml->createElement('Identification');
					$identification->setAttribute('IDValue', $unit->id);
					$identification->setAttribute('IDType', 'FloorplanID');
					$identification->setAttribute('IDRank', 'primary');
					$unit_tag->appendChild($identification);
					//endregion

					//region MarketingName
					$marketing_name = $xml->createElement('MarketingName');
					$marketing_name->nodeValue = $unit_streeteasy_number;
					$unit_tag->appendChild($marketing_name);
					//endregion

					//region UnitBedrooms
					$unit_bedrooms = $xml->createElement('UnitBedrooms');
					$unit_bedrooms->nodeValue = $unit->bedrooms;
					$unit_tag->appendChild($unit_bedrooms);
					//endregion

					//region UnitBathrooms
					$unit_bathrooms = $xml->createElement('UnitBathrooms');
					$unit_bathrooms->nodeValue = $unit->bathrooms;
					$unit_tag->appendChild($unit_bathrooms);
					//endregion

					//region UnitRent
					$unit_rent = $xml->createElement('UnitRent');
					$unit_rent->nodeValue = $unit_net_effective_price;
					$unit_tag->appendChild($unit_rent);
					//endregion

					//region UnitLeasedStatus
					$unit_leased_status = $xml->createElement('UnitLeasedStatus');
					$unit_leased_status->nodeValue = 'available';
					$unit_tag->appendChild($unit_leased_status);
					//endregion

					$units_tag->appendChild($unit_tag);
					$ils_unit->appendChild($units_tag);

					//region Comment
					$comment = $xml->createElement('Comment');
					$comment->appendChild($xml->createCDATASection($unit->description));
					$ils_unit->appendChild($comment);
					//endregion

					//region EffectiveRent
					$effective_rent = $xml->createElement('EffectiveRent');
					$effective_rent->setAttribute('min', $unit_net_effective_price);
					$effective_rent->setAttribute('max', $unit_net_effective_price);
					$ils_unit->appendChild($effective_rent);
					//endregion

					//region Unit Features (aka Amenity)
					$features = $this->_feature_mapper->fetchFeatureNameTagPairsByUnitId($unit->id,'apartmentscom_tag');
					if ($features) {
						foreach ($features as $feature) {
							$feature_tag = $xml->createElement('Amenity');
							$feature_tag->setAttribute('AmenityType', (!empty($feature['tag']))?$feature['tag']:'Other');
							$description = $xml->createElement('Description');
							$description->nodeValue = htmlspecialchars($feature['name']);
							$feature_tag->appendChild($description);
							$ils_unit->appendChild($feature_tag);
						}
					}
					//endregion

					//region Gallery Images
					$images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit->id);
					if ($images) {
						foreach ($images as $index => $image) {
							$file = $xml->createElement('File');
							$file->setAttribute('FileID', $image->id);
							$file->setAttribute('Active', 'true');
							$file_type = $xml->createElement('FileType');
							$file_type->nodeValue = 'Photo';
							$file->appendChild($file_type);
							$caption = $xml->createElement('Caption');
							$captionamenity = $xml->createTextNode($image->title);
							$caption->appendChild($captionamenity);
							$file->appendChild($caption);
							$src = $xml->createElement('Src');
							$src->nodeValue = $this->_domain.$image->image;
							$file->appendChild($src);
							$rank = $xml->createElement('Rank');
							$rank->nodeValue = $index;
							$file->appendChild($src);
							$ils_unit->appendChild($file);
						}
					}
					//endregion

					$property->appendChild($ils_unit);
				}
				//endregion
			}

			//region Building Gallery Images
			$building_image_mapper = new Custom_Model_Mapper_GfBuildingImage();
			$images = $building_image_mapper->fetchOrderedByBuildingId($building->id);
			if ($images) {
				foreach ($images as $index => $image) {
					$file = $xml->createElement('File');
					$file->setAttribute('FileID', $image->id);
					$file->setAttribute('Active', 'true');
					$file_type = $xml->createElement('FileType');
					$file_type->nodeValue = 'Photo';
					$file->appendChild($file_type);
					$caption = $xml->createElement('Caption');
					$caption->nodeValue = $image->title;
					$file->appendChild($caption);
					$src = $xml->createElement('Src');
					$src->nodeValue = $this->_domain.$image->image;
					$file->appendChild($src);
					$rank = $xml->createElement('Rank');
					$rank->nodeValue = $index;
					$file->appendChild($src);
					$property->appendChild($file);
				}
			}
			//endregion

			$main_element->appendChild($property);
			//endregion
		}
		$xml->appendChild($main_element);
		$xml->formatOutput = TRUE;

		header('Content-type: text/xml');
		echo $xml->saveXML();
		exit;


		/*
		  $buildings = Building::rentals()->with('neighborhood', 'address', 'images', 'units', 'openHouses')->get();

		<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>
		<PhysicalProperty>
		  <Management IDValue="1" OrganizationName="Goldfarb Properties" IDType="managmentID"/>
		  @foreach ($buildings as $building)
		  <Property>
			<PropertyID>
			  <Identification IDValue="{{$building->id}}" IDRank="primary"/>
			  <MarketingName>{{$building->name}}</MarketingName>
			  <WebSite>{{url(route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug, 'buildingSlug' => $building->slug]))}}</WebSite>
			  <Address AddressType="property">
				<AddressLine1>{{$building->address->street_1}}</AddressLine1>
				<AddressLine2>{{$building->address->street_2}}</AddressLine2>
				<City>{{$building->address->city}}</City>
				<State>{{$building->address->state}}</State>
				<PostalCode>{{$building->address->zip}}</PostalCode>
			  </Address>
			</PropertyID>
			<ILS_Identification ILS_IdentificationType="Apartment" RentalType="Unspecified">
			</ILS_Identification>
			<Information>
			  <ShortDescription><![CDATA[ {!!$building->body!!}]]></ShortDescription>
			  <LongDescription><![CDATA[ {!!$building->body!!}]]></LongDescription>
			</Information>
			@foreach ($building->amenities as $amenity)
				<Amenity AmenityType="{{$amenity->feed_meta['apartments-com'] or 'Other'}}">
					<Description>{{$amenity->name}}</Description>
				</Amenity>
			@endforeach
			@foreach ($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get() as $unit)
				<Floorplan IDValue="{{$unit->id}}" IDType="FloorplanID" IDRank="primary">
				  <FloorplanType>Internal</FloorplanType>
				  <Name>Apartment {{$unit->getStreeteasyNumber()}}</Name>
				  <Comment><![CDATA[ {!!$unit->body!!}]]></Comment>
				  <UnitCount>1</UnitCount>
				  <UnitsAvailable>1</UnitsAvailable>
				  <Room RoomType="Bedroom">
					<Count>{{floor($unit->bedrooms)}}</Count>
				  </Room>
				  <Room RoomType="Bathroom">
					<Count>{{$unit->bathrooms}}</Count>
				  </Room>
				  <EffectiveRent min="{{$unit->net_effective_price}}" max="{{$unit->net_effective_price}}" />
				  @foreach ($unit->features as $feature)
					  <Amenity AmenityType="{{$feature->feed_meta['apartments-com'] or 'Other'}}">
						  <Description>{{$feature->name}}</Description>
					  </Amenity>
				  @endforeach
				  @foreach ($unit->getGalleryImages() as $index => $image)
					  <File FileID="{{$image->id}}" Active="true">
						<FileType>Photo</FileType>
						<Caption>{{$image->title}}</Caption>
						<Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
						<Rank>{{$index}}</Rank>
					  </File>
				  @endforeach
				  @if (count($unit->floorplan))
					  <File FileID="{{$unit->floorplan->id}}" Active="false">
						  <FileType>Floorplan</FileType>
						  <Caption>{{$unit->floorplan->title}}</Caption>
						  <Src>{!!url($unit->floorplanSrc())!!}</Src>
						  <Rank>0</Rank>
					  </File>
				  @endif
				</Floorplan>
			@endforeach
			@foreach ($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get() as $unit)
			<ILS_Unit IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID" IDRank="primary">
			  <Units>
				<Unit>
				  <Identification IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID" IDRank="primary"/>
				  <Identification IDValue="{{$unit->id}}" IDType="FloorplanID" IDRank="primary"/>
				  <MarketingName>{{$unit->getStreeteasyNumber()}}</MarketingName>
				  <UnitBedrooms>{{$unit->bedrooms}}</UnitBedrooms>
				  <UnitBathrooms>{{$unit->bathrooms}}</UnitBathrooms>
				  <UnitRent>{{$unit->net_effective_price}}</UnitRent>
				  <UnitLeasedStatus>available</UnitLeasedStatus>
				</Unit>
			  </Units>
			  <Comment><![CDATA[ {!!$unit->body!!}]]></Comment>
			  <EffectiveRent Min="{{$unit->net_effective_price}}" Max="{{$unit->net_effective_price}}" />
			  @foreach ($unit->features as $feature)
				  <Amenity AmenityType="{{$feature->feed_meta['apartments-com'] or 'Other'}}">
					  <Description>{{$feature->name}}</Description>
				  </Amenity>
			  @endforeach
			  @foreach ($unit->getGalleryImages() as $index => $image)
				  <File FileID="{{$image->id}}" Active="true">
					<FileType>Photo</FileType>
					<Caption>{{$image->title}}</Caption>
					<Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
					<Rank>{{$index}}</Rank>
				  </File>
			  @endforeach
			</ILS_Unit>
			@endforeach
			@foreach ($building->getGalleryImages() as $index => $image)
				<File FileID="{{$image->id}}" Active="true">
				  <FileType>Photo</FileType>
				  <Caption>{{$image->title}}</Caption>
				  <Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
				  <Rank>{{$index}}</Rank>
				</File>
			@endforeach
		  </Property>
		  @endforeach
		</PhysicalProperty>

		 */
	}

	public function hotpadsAction()
	{
/*
$buildings = Building::rentals()->get();


<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>
<hotPadsItems version="2.1">
<Company id="goldfarb">
  <name>Goldfarb Properties</name>
  <website>http://goldfarbproperties.com</website>
    <city>New York</city>
  <state>NY</state>
  <CompanyLogo source="{{asset('/img/email/goldfarb.png')}}"/>
</Company>
@foreach ($buildings as $building)
<Listing id="{{$building->slug}}" type="RENTAL" companyId="goldfarb" propertyType="LARGE">
  <name>{{$building->name}}</name>
  <street hide="false">{{$building->address->street_1}}{{(!!$building->address->street_2) ? ',' . $building->address->street_2 : null}}</street>
  <city>{{$building->address->city}}</city>
  <state>{{$building->address->state}}</state>
  <zip>{{$building->address->zip}}</zip>
  <country>United States of America</country>
  <latitude>{{$building->address->getLatLngAttribute()['lat']}}</latitude>
  <longitude>{{$building->address->getLatLngAttribute()['lng']}}</longitude>
  <lastUpdated>{{$building->updated_at}}</lastUpdated>
  <contactName>Goldfarb Properties</contactName>
  <contactEmail>{{$building->neighborhood->region->email}}</contactEmail>
  <contactPhone>{{$building->neighborhood->region->phone}}</contactPhone>
  @if (count($building->openHouses))
  <openHouses>
    @foreach ($building->openHouses as $openHouse)
    <openHouse>
      <date>{{$openHouse->start_time->toDateString()}}</date>
      <startTime>{{$openHouse->start_time->format('h:i a')}}</startTime>
      <endTime>{{$openHouse->end_time->format('h:i a')}}</endTime>
      <appointmentRequired>true</appointmentRequired>
    </openHouse>
    @endforeach
  </openHouses>
  @endif
  <description><![CDATA[{{$building->body}}]]></description>
  <leaseTerm>OneYear</leaseTerm>
  <website>{{route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug, 'buildingSlug' => $building->slug])}}</website>
@foreach ($building->amenities as $amenity)
  <ListingTag type="PROPERTY_AMENITY"><tag>{{$amenity->name}}</tag></ListingTag>
@endforeach
@foreach ($building->images as $image)
  <ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
    <label>{{$image->title}}</label>
  </ListingPhoto>
@endforeach
@foreach ($building->units()->available()->get() as $unit)
  <Model id="{{$unit->unique_version_id}}" type="unit" parentModelId="{{$building->slug}}" searchable="true" pricingType="FLAT">
    <unitNumber>{{$unit->getStreeteasyNumber()}}</unitNumber>
    <unitFloorNumber>{{$unit->floor}}</unitFloorNumber>
    <lowPrice>{{$unit->net_effective_price}}</lowPrice>
    <highPrice>{{$unit->net_effective_price}}</highPrice>
    <numBedrooms>{{floor($unit->bedrooms)}}</numBedrooms>
    <numFullBaths>{{floor($unit->bathrooms)}}</numFullBaths>
    <numHalfBaths>{{ceil(fmod($unit->bathrooms, 1))}}</numHalfBaths>
    <squareFeet>{{$unit->square_footage}}</squareFeet>
    <description><![CDATA[ @if (!!$unit->getConcessionMessage()) {{$unit->getConcessionMessage()}}<br><br>@endif @if (!!$unit->body){{$unit->body}}@endif ]]></description>
    @if (count($unit->floorplan))
      <ModelLayout source="{{asset($unit->floorplanSrc())}}">
        <label>{{$unit->getStreeteasyNumber() . ' Floorplan'}}</label>
      </ModelLayout>
    @endif
    @foreach ($unit->features as $feature)
      <ModelTag type="AMENITY_SELECT"><tag>{{$feature->title}}</tag></ModelTag>
    @endforeach
    @foreach ($unit->images as $image)
      <ModelPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
        <label>{{$image->title}}</label>
      </ModelPhoto>
    @endforeach
  </Model>
@endforeach
</Listing>
@endforeach
</hotPadsItems>

 */
	}

	/**
	 * @return stdClass[]
	 */
	public function getBuildingsResidentialRentals()
	{
		$output = [];
		$yardi = new Custom_Model_Yardi();
		$sql = "SELECT b.*, GROUP_CONCAT(DISTINCT a.name SEPARATOR '|') as _amenities, 
					n.id as '-neighborhood-id-', n.code as '-neighborhood-code-', n.name as '-neighborhood-name-', 
					n.description as '-neighborhood-description-',
					r.id as '-region-id-', r.code as '-region-code-', r.name as '-region-name-', 
					r.sub_title as '-region-sub_title-', r.image as '-region-image-', r.phone as '-region-phone-', 
       				r.email as '-region-email-' 
				FROM gf_buildings b
					LEFT JOIN gf_neighborhoods n ON b.neighborhood_id = n.id
					LEFT JOIN gf_regions r ON n.region_id = r.id
					LEFT JOIN gf_building_amenities ba ON b.id = ba.building_id
					INNER JOIN gf_amenities a on ba.amenity_id = a.id
				WHERE b.residential LIKE '%\"Rentals\"%' AND b.yardi_code IS NOT NULL AND b.yardi_code != ''
				GROUP BY b.id";
		/** @var stdClass $result */
		$result = $this->_db->fetchAll($sql, [], Zend_Db::FETCH_OBJ);
		foreach ($result as $building) {
			//region Process amenities
			if (!empty($building->_amenities)) {
				$building->amenities = explode('|', $building->_amenities);
			}
			unset($building->_amenities);
			//endregion

			//region process objects
			$regex = '/^-([^\-]*)-(.*)-$/m';
			foreach ($building as $property => $value) {
				preg_match($regex, $property, $matches, PREG_UNMATCHED_AS_NULL, 0);
				if (!empty($matches) && count($matches) == 3) {
					list(, $object_name, $object_prop) = $matches;
					if (!empty($object_name) && !empty($object_prop)) {
						if (!isset($building->$object_name)) {
							$building->$object_name = new stdClass();
						}
						$building->$object_name->$object_prop = $value;
						unset($building->$property);
					}
				}
			}
			//endregion

			//region fetch Yardi data
			$properties = explode(',', $building->yardi_code);
			foreach ($properties as $propertyCode) {
				// @todo: Need to validate this code. Migrated as is but in for loop it will overwrite previous entry
				$data = $yardi->getAvailability(trim($propertyCode));
				$building->yardi_data = $data;
			}
			//endregion

			$output[] = $building;
		}
		return $output;
	}

	/**
	 * converted: Completed [XML]
	 */
	public function hotpadsrevisedAction()
	{
		$building_image_mapper = new Custom_Model_Mapper_GfBuildingImage();
		//region Modified Asset Update for HotPads
		$now = time();
		$interval = 60 * 60 * 24 * 7;
		$valid = $now - $interval;
		$path = isset($_SERVER['SITE_FILE_ROOT'])?$_SERVER['SITE_FILE_ROOT']:realpath(APPLICATION_PATH.'/../public');
		//endregion

		//region Listing is repeatable
		$buildings = $this->getBuildingsResidentialRentals();
		$available_units = $this->getUnits();
		$model_units = $this->getModelUnits();

		$ppn = 0;
		$unit_yardi_data = [];
		foreach($buildings as $building){
			$building->has_vacancy = $this->_building_mapper->hasVacancy($building->id);
			foreach($available_units as $available_unit){
				if($available_unit->building_code == $building->code){
					foreach($building->yardi_data as $yardi_data){
						if($yardi_data->ApartmentId == $available_unit->yardi_code){
							$unit_yardi_data[$available_unit->yardi_code] = $yardi_data;
							//$available_unit->date_available = $yardi_data->AvailableDate;
							$available_unit->floorplan_id = $yardi_data->FloorplanId;
						}
					}
				}
			}
			foreach($model_units as $model_unit){
				if($model_unit->building_code == $building->code){
					foreach($building->yardi_data as $yardi_data){
						if($yardi_data->ApartmentId == $model_unit->yardi_code){
							$model_unit->floorplan_id = $yardi_data->FloorplanId;
						}
					}
				}
			}
			if($building->code == 'pelham-parkway-towers' || $building->code == 'pelham-terrace' || $building->code == 'pelham-park-view' || $building->code == 'pelham-place') {
				$ppn += 1;
			}
		}
		//endregion

		$xml = new DomDocument('1.0', 'UTF-8');
		$main_element = $xml->createElement('hotPadsItems');
		$main_element->setAttribute('version', '2.1');

		//region Company
		$company = $xml->createElement('Company');
		$company->setAttribute('id', 'goldfarb');

		//region name
		$name = $xml->createElement('name');
		$name->nodeValue = "Goldfarb Properties";
		$company->appendChild($name);
		//endregion

		//region website
		$website = $xml->createElement('website');
		$website->nodeValue = "http://goldfarbproperties.com";
		$company->appendChild($website);
		//endregion

		//region city
		$city = $xml->createElement('city');
		$city->nodeValue = "New York";
		$company->appendChild($city);
		//endregion

		//region state
		$state = $xml->createElement('state');
		$state->nodeValue = "NY";
		$company->appendChild($state);
		//endregion

		//region company_logo
		$company_logo = $xml->createElement('CompanyLogo');
		$company_logo->setAttribute('source', '/img/email/goldfarb.png');
		$company->appendChild($company_logo);
		//endregion

		$main_element->appendChild($company);
		//endregion

		$grouped_building['first'] = [
			'wavecrest-gardens', 'the-grand', 'drake-house', 'harbor-house', 'maple-gardens',
			'the-churchill', 'noonan-towers',
		];

		$grouped_building['second'] = [
			'pelham-parkway-towers', 'pelham-terrace', 'pelham-park-view', 'pelham-place',
		];

		$grouped_building['third'] = [
			'151-east-80th-street', '1160-fifth-avenue', 'park-towers-south', 'the-river-cliff', 'gracie-court',
			'cedar-arms', 'the-capri', 'river-hill-gardens', 'the-ross', 'sheridan-plaza', 'fordham-terrace',
		];

		foreach ($buildings as $building) {
			//region First Group Of Buildings
			if(in_array($building->code, $grouped_building['first'])) {
				if($this->_building_mapper->hasVacancy($building->id)) {
					//region Listing
					$listing = $xml->createElement('Listing');
					$listing->setAttribute('id', $building->code);
					$listing->setAttribute('type', 'RENTAL');
					// todo: This has to be an error: Should be companyId not companyld
					$listing->setAttribute('companyld', 'goldfarb');
					$listing->setAttribute('propertyType', 'LARGE');

					//region name
					$name = $xml->createElement('name');
					$name->nodeValue = $building->name;
					$listing->appendChild($name);
					//endregion

					//region street
					$street_str = $building->street1;
					if (!empty($building->street2)) {
						$street_str .= ','.$building->street2;
					}
					$street = $xml->createElement('street');
					$listing->setAttribute('hide', false);
					$street->nodeValue = $street_str;
					$listing->appendChild($street);
					//endregion

					//region city
					$city = $xml->createElement('city');
					$city->nodeValue = $building->city;
					$listing->appendChild($city);
					//endregion

					//region state
					$state = $xml->createElement('state');
					$state->nodeValue = $building->state;
					$listing->appendChild($state);
					//endregion

					//region zip
					$zip = $xml->createElement('zip');
					$zip->nodeValue = $building->zip;
					$listing->appendChild($zip);
					//endregion

					//region country
					$country = $xml->createElement('country');
					$country->nodeValue = 'United States of America';
					$listing->appendChild($country);
					//endregion

					//region latitude
					$latitude = $xml->createElement('latitude');
					$latitude->nodeValue = $building->latitude;
					$listing->appendChild($latitude);
					//endregion

					//region longitude
					$longitude = $xml->createElement('longitude');
					$longitude->nodeValue = $building->longitude;
					$listing->appendChild($longitude);
					//endregion

					//region lastUpdated
					$lastUpdated = $xml->createElement('lastUpdated');
					$lastUpdated->nodeValue = $building->modified;
					$listing->appendChild($lastUpdated);
					//endregion

					//region contactName
					$contactName = $xml->createElement('contactName');
					$contactName->nodeValue = 'Goldfarb Properties';
					$listing->appendChild($contactName);
					//endregion

					//region contactEmail
					$contactEmail = $xml->createElement('contactEmail');
					$contactEmail->nodeValue = $building->region->email;
					$listing->appendChild($contactEmail);
					//endregion

					//region contactPhone
					$contactPhone = $xml->createElement('contactPhone');
					$contactPhone->nodeValue = $building->region->phone;
					$listing->appendChild($contactPhone);
					//endregion

					//region contactMethodPreference
					$contactMethodPreference = $xml->createElement('contactMethodPreference');
					$contactMethodPreference->nodeValue = 'Email';
					$listing->appendChild($contactMethodPreference);
					//endregion

					//region previewMessage
					$previewMessage = $xml->createElement('previewMessage');
					$previewMessage->appendChild($xml->createCDATASection($building->neighborhood->description));
					$listing->appendChild($previewMessage);
					//endregion

					//region terms
					$terms = $xml->createElement('terms');
					$terms->nodeValue = 'Minimum one month security';
					$listing->appendChild($terms);
					//endregion

					//region leaseTerm
					$leaseTerm = $xml->createElement('leaseTerm');
					$leaseTerm->nodeValue = '12 or 24 months';
					$listing->appendChild($leaseTerm);
					//endregion

					//region website
					$website = $xml->createElement('website');
					$website->nodeValue = $this->_domain."/rentals/{$building->region->code}/{$building->code}";
					$listing->appendChild($website);
					//endregion

					//region ListingTag
					$amenities = $this->_amenity_mapper->fetchAmenityNamesByBuildingId($building->id);
					if ($amenities) {
						foreach ($amenities as $amenity) {
							$ListingTag = $xml->createElement('ListingTag');
							$ListingTag->setAttribute('type', 'PROPERTY_AMENITY');
							$tag = $xml->createElement('tag');
							$tagamenity = $xml->createTextNode($amenity);
							$tag->appendChild($tagamenity);
							$ListingTag->appendChild($tag);
							$listing->appendChild($ListingTag);
						}
					}
					//endregion

					//region ListingPhoto (building specific)
					if (count($model_units)> 0) {
						switch ($building->code) {
							case 'wavecrest-gardens':
								foreach ($model_units as $model_unit) {
									if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
										if($model_unit->number =='WAIT-Q1H'){
											if ($model_unit->floorplan_id =='1045539') {
												$model_images = $this->_unit_image_mapper->fetchOrderedByUnitId($model_unit->id);
												foreach ($model_images as $image) {
													$file = $this->_domain . $image->image;
													$modified = (file_exists($path.$image->image))
														? filemtime($path.$image->image) : 0;
													if ($modified >= $valid) {
														$file .= '?'.date ('W', $now) . date('N', $now);
													}
													$ListingPhoto = $xml->createElement('ListingPhoto');
													$ListingPhoto->setAttribute('source', $file);
													$label = $xml->createElement('label');
													$label->nodeValue = $image->title;
													$ListingPhoto->appendChild($label);
													$listing->appendChild($ListingPhoto);
												}
											}
										}
									}
								}
								break;
							case 'the-grand':
								foreach ($model_units as $model_unit) {
									if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
										// @todo: Makes no sense. The top condition negates this next one.
										if($model_unit->number == '') {
											if ($model_unit->floorplan_id =='') {
												$model_images = $this->_unit_image_mapper->fetchOrderedByUnitId($model_unit->id);
												foreach ($model_images as $image) {
													$file = $this->_domain . $image->image;
													$modified = (file_exists($path.$image->image))
														? filemtime($path.$image->image):0;
													if ($modified >= $valid) {
														$file .= '?'.date ('W', $now) . date('N', $now);
													}
													$ListingPhoto = $xml->createElement('ListingPhoto');
													$ListingPhoto->setAttribute('source', $file);
													$label = $xml->createElement('label');
													$label->nodeValue = $image->title;
													$ListingPhoto->appendChild($label);
													$listing->appendChild($ListingPhoto);
												}
											}
										}
									}
								}
								break;
							case 'drake-house':
								foreach ($model_units as $model_unit) {
									if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
										if($model_unit->number == 'WAIT-D0H') {
											if ($model_unit->floorplan_id =='2043074') {
												$model_images = $this->_unit_image_mapper->fetchOrderedByUnitId($model_unit->id);
												foreach ($model_images as $image) {
													$file = $this->_domain . $image->image;
													$modified = (file_exists($path.$image->image))
														? filemtime($path.$image->image) : 0;
													if ($modified >= $valid) {
														$file .= '?'.date ('W', $now) . date('N', $now);
													}
													$ListingPhoto = $xml->createElement('ListingPhoto');
													$ListingPhoto->setAttribute('source', $file);
													$label = $xml->createElement('label');
													$label->nodeValue = $image->title;
													$ListingPhoto->appendChild($label);
													$listing->appendChild($ListingPhoto);
												}
											}
										}
									}
								}
								break;
							case 'harbor-house':
								foreach ($model_units as $model_unit) {
									if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
										if($model_unit->number == 'WAIT-H0H') {
											if ($model_unit->floorplan_id =='1045542') {
												$model_images = $this->_unit_image_mapper->fetchOrderedByUnitId($model_unit->id);
												foreach ($model_images as $image) {
													$file = $this->_domain . $image->image;
													$modified = (file_exists($path.$image->image))
														? filemtime($path.$image->image) : 0;
													if ($modified >= $valid) {
														$file .= '?'.date ('W', $now) . date('N', $now);
													}
													$ListingPhoto = $xml->createElement('ListingPhoto');
													$ListingPhoto->setAttribute('source', $file);
													$label = $xml->createElement('label');
													$label->nodeValue = $image->title;
													$ListingPhoto->appendChild($label);
													$listing->appendChild($ListingPhoto);
												}
											}
										}
									}
								}
								break;
							case 'maple-gardens':
								foreach ($model_units as $model_unit) {
									if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
										if($model_unit->number == 'WAIT-I0H') {
											if ($model_unit->floorplan_id =='2043124') {
												$model_images = $this->_unit_image_mapper->fetchOrderedByUnitId($model_unit->id);
												foreach ($model_images as $image) {
													$file = $this->_domain . $image->image;
													$modified = (file_exists($path.$image->image))
														? filemtime($path.$image->image) : 0;
													if ($modified >= $valid) {
														$file .= '?'.date ('W', $now) . date('N', $now);
													}
													$ListingPhoto = $xml->createElement('ListingPhoto');
													$ListingPhoto->setAttribute('source', $file);
													$label = $xml->createElement('label');
													$label->nodeValue = $image->title;
													$ListingPhoto->appendChild($label);
													$listing->appendChild($ListingPhoto);
												}
											}
										}
									}
								}
								break;
							case 'the-churchill':
								foreach ($model_units as $model_unit) {
									if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
										if($model_unit->number == 'WAIT-W0H') {
											if ($model_unit->floorplan_id =='2043171') {
												$model_images = $this->_unit_image_mapper->fetchOrderedByUnitId($model_unit->id);
												foreach ($model_images as $image) {
													$file = $this->_domain . $image->image;
													$modified = (file_exists($path.$image->image))
														? filemtime($path.$image->image) : 0;
													if ($modified >= $valid) {
														$file .= '?'.date ('W', $now) . date('N', $now);
													}
													$ListingPhoto = $xml->createElement('ListingPhoto');
													$ListingPhoto->setAttribute('source', $file);
													$label = $xml->createElement('label');
													$label->nodeValue = $image->title;
													$ListingPhoto->appendChild($label);
													$listing->appendChild($ListingPhoto);
												}
											}
										}
									}
								}
								break;
							case 'noonan-towers':
								foreach ($model_units as $model_unit) {
									if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
										if($model_unit->number == 'WAIT-N0H') {
											if ($model_unit->floorplan_id =='2043117') {
												$model_images = $this->_unit_image_mapper->fetchOrderedByUnitId($model_unit->id);
												foreach ($model_images as $image) {
													$file = $this->_domain . $image->image;
													$modified = (file_exists($path.$image->image))
														? filemtime($path.$image->image) : 0;
													if ($modified >= $valid) {
														$file .= '?'.date ('W', $now) . date('N', $now);
													}
													$ListingPhoto = $xml->createElement('ListingPhoto');
													$ListingPhoto->setAttribute('source', $file);
													$label = $xml->createElement('label');
													$label->nodeValue = $image->title;
													$ListingPhoto->appendChild($label);
													$listing->appendChild($ListingPhoto);
												}
											}
										}
									}
								}
								break;
						}
					}
					$building_images = $building_image_mapper->fetchOrderedByBuildingId($building->id);
					foreach ($building_images as $image) {
						$file = $this->_domain . $image->image;
						$modified = (file_exists($path.$image->image))
							? filemtime($path.$image->image) : 0;
						if ($modified >= $valid) {
							$file .= '?'.date ('W', $now) . date('N', $now);
						}
						$ListingPhoto = $xml->createElement('ListingPhoto');
						$ListingPhoto->setAttribute('source', $file);
						$label = $xml->createElement('label');
						$label->nodeValue = $image->title;
						$ListingPhoto->appendChild($label);
						$listing->appendChild($ListingPhoto);
					}
					//endregion

					//region Model (Model Units)
					foreach ($model_units as $model_unit) {
						$unit_concession_message = $this->_unit_mapper->getUnitConcessionMessage($model_unit->id);
						if ($model_unit->building_code == $building->code) {
							if (substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
								$high_price = 0;
								foreach ($available_units as $available_unit) {
									if($available_unit->building_code == $model_unit->building_code) {
										if($available_unit->floorplan_id == $model_unit->floorplan_id) {
											$unit_net_effective_price
												= $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
											if($unit_net_effective_price > $high_price) {
												$high_price = $unit_net_effective_price;
											}
										}
									}
								}
								$low_price = $high_price;
								foreach ($available_units as $available_unit) {
									if($available_unit->building_code == $model_unit->building_code) {
										$unit_net_effective_price
											= $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
										if($available_unit->floorplan_id == $model_unit->floorplan_id) {
											if($unit_net_effective_price < $low_price) {
												$low_price = $unit_net_effective_price;
											}
										}
									}
								}
								//region Model
								$model = $xml->createElement('Model');
								$model->setAttribute('id', $model_unit->floorplan_id);
								$model->setAttribute('type', 'floorplan');
								$model->setAttribute('searchable', 'true');
								$model->setAttribute('pricingType', 'RANGE');

								$unit_number = !empty(($model_unit->feed_number_override))
									? ($model_unit->feed_number_override) : $model_unit->number;
								//region unitNumber
								$unitNumber = $xml->createElement('unitNumber', $unit_number);
								$model->appendChild($unitNumber);
								//endregion

								//region unitFloorNumber
								$unitFloorNumber = $xml->createElement('unitFloorNumber', $model_unit->floor);
								$model->appendChild($unitFloorNumber);
								//endregion

								//region lowPrice
								$lowPrice = $xml->createElement('lowPrice', $low_price);
								$model->appendChild($lowPrice);
								//endregion

								//region highPrice
								$highPrice = $xml->createElement('highPrice', $high_price);
								$model->appendChild($highPrice);
								//endregion

								//region name
								$name = $xml->createElement('name', $unit_number);
								$model->appendChild($name);
								//endregion

								//region numBedrooms
								$numBedrooms = $xml->createElement('numBedrooms', floor($model_unit->bedrooms));
								$model->appendChild($numBedrooms);
								//endregion

								//region numFullBaths
								$numFullBaths = $xml->createElement('numFullBaths', floor($model_unit->bathrooms));
								$model->appendChild($numFullBaths);
								//endregion

								//region numHalfBaths
								$numHalfBaths = $xml->createElement('numHalfBaths', ceil(fmod($model_unit->bathrooms, 1)));
								$model->appendChild($numHalfBaths);
								//endregion

								//region squareFeet
								if ($model_unit->square_footage_status == 'enabled') {
									$squareFeet = $xml->createElement('squareFeet', $model_unit->square_footage);
									$model->appendChild($squareFeet);
								}
								//endregion

								//region description
								$desc_str = ($unit_concession_message) ? $unit_concession_message : '';
								$desc_str .= ($model_unit->description) ? '<br><br>'.$model_unit->description : '';
								$description = $xml->createElement('description');
								$description->appendChild($xml->createCDATASection($desc_str));
								$model->appendChild($description);
								//endregion

								//region ModelLayout
								$file = $this->_domain.$model_unit->floorplan;
								$modified = (file_exists($path.$model_unit->floorplan))
									? filemtime($path.$model_unit->floorplan):0;
								if ($modified >= $valid) {
									$file .= '?week='.date ('W', $now) . date('N', $now);
								}
								$ModelLayout = $xml->createElement('ModelLayout');
								$ModelLayout->setAttribute('source', $file);
								$label = $xml->createElement('label');
								if ($model_unit->feed_number_override) {
									$label->nodeValue = $model_unit->feed_number_override;
								} else {
									$label->nodeValue = $model_unit->number;
								}
								$ModelLayout->appendChild($label);
								$model->appendChild($ModelLayout);
								//endregion

								//region ModelTag
								$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($model_unit->id);
								if ($features) {
									foreach ($features as $feature) {
										$ModelTag = $xml->createElement('ModelTag');
										$ModelTag->setAttribute('type', 'AMENITY_SELECT');
										$tag = $xml->createElement('tag', $feature);
										$ModelTag->appendChild($tag);
										$model->appendChild($ModelTag);
									}
								}
								//endregion

								$listing->appendChild($model);
								//endregion

								//region Model (Available Units)
								foreach ($available_units as $available_unit) {
									$available_unit_unique_id = strtolower(sprintf('%sv%s', $available_unit->number, $available_unit->version));
									$available_unit_concession_message = $this->_unit_mapper->getUnitConcessionMessage($available_unit->id);
									$available_unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
									if($available_unit->building_code == $model_unit->building_code) {
										if($available_unit->floorplan_id == $model_unit->floorplan_id) {
											//region Model
											$model = $xml->createElement('Model');
											$model->setAttribute(
												'id',
												$available_unit->floorplan_id
												.'_'.$available_unit_unique_id
											);
											$model->setAttribute('parentModelId', $available_unit->floorplan_id);
											$model->setAttribute('type', 'unit');
											$model->setAttribute('searchable', 'true');
											$model->setAttribute('pricingType', 'FLAT');

											$unit_number = !empty(($available_unit->feed_number_override))
												? ($available_unit->feed_number_override) : $available_unit->number;
											//region unitNumber
											$unitNumber = $xml->createElement('unitNumber', $unit_number);
											$model->appendChild($unitNumber);
											//endregion

											//region lowPrice
											$lowPrice = $xml->createElement('lowPrice', $available_unit_net_effective_price);
											$model->appendChild($lowPrice);
											//endregion

											//region highPrice
											$highPrice = $xml->createElement('highPrice', $available_unit_net_effective_price);
											$model->appendChild($highPrice);
											//endregion

											//region numBedrooms
											$numBedrooms = $xml->createElement('numBedrooms', floor($available_unit->bedrooms));
											$model->appendChild($numBedrooms);
											//endregion

											//region numFullBaths
											$numFullBaths = $xml->createElement('numFullBaths', floor($available_unit->bathrooms));
											$model->appendChild($numFullBaths);
											//endregion

											//region squareFeet
											if ($available_unit->square_footage_status == 'enabled') {
												$squareFeet = $xml->createElement('squareFeet', $available_unit->square_footage);
												$model->appendChild($squareFeet);
											}
											//endregion

											//region dateAvailable
											if (array_key_exists($available_unit->yardi_code,$unit_yardi_data)) {
												$dateAvailable = $xml->createElement('dateAvailable',	$unit_yardi_data[$available_unit->yardi_code]->dateAvailable);
												$model->appendChild($dateAvailable);
											}
											//endregion

											//region description
											$desc_str = ($available_unit_concession_message) ? $available_unit_concession_message : '';
											$desc_str .= ($available_unit->description) ? '<br><br>'.$available_unit->description : '';
											$description = $xml->createElement('description');
											$description->appendChild($xml->createCDATASection($desc_str));
											$model->appendChild($description);
											//endregion

											//region ModelLayout
											if ($available_unit->floorplan) {
												$file = $this->_domain.$available_unit->floorplan;
												$modified = (file_exists($path.$available_unit->floorplan))
													? filemtime($path.$available_unit->floorplan) : 0;
												if ($modified >= $valid) {
													$file .= '?week='.date('W', $now).date('N', $now);
												}
												$ModelLayout = $xml->createElement('ModelLayout');
												$ModelLayout->setAttribute('source', $file);
												$label = $xml->createElement('label');
												if ($available_unit->feed_number_override) {
													$label->nodeValue = $available_unit->feed_number_override;
												} else {
													$label->nodeValue = $available_unit->number;
												}
												$ModelLayout->appendChild($label);
												$model->appendChild($ModelLayout);
											}
											//endregion

											//region ModelTag
											$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($available_unit->id);
											if ($features) {
												foreach ($features as $feature) {
													$ModelTag = $xml->createElement('ModelTag');
													$ModelTag->setAttribute('type', 'AMENITY_SELECT');
													$tag = $xml->createElement('tag', $feature);
													$ModelTag->appendChild($tag);
													$model->appendChild($ModelTag);
												}
											}
											//endregion

											$listing->appendChild($model);
											//endregion
										}
									}
								}
								//endregion
							}
						}
					}
					//endregion

					//region providerType
					$providerType = $xml->createElement('providerType', 'Property Manager');
					$listing->appendChild($providerType);
					//endregion

					$main_element->appendChild($listing);
					//endregion
				}
			}
			//endregion

			//region Second Group Of Buildings
			if(in_array($building->code, $grouped_building['second'])) {
				$ppn = -1;
				if($building->code == 'pelham-park-view') {
					//region Listing
					$listing = $xml->createElement('Listing');
					$listing->setAttribute('id', $building->code);
					$listing->setAttribute('type', 'RENTAL');
					// todo: This has to be an error: Should be companyId not companyld
					$listing->setAttribute('companyld', 'goldfarb');
					$listing->setAttribute('propertyType', 'LARGE');

					//region name
					$name = $xml->createElement('name');
					$name->nodeValue = $building->name;
					$listing->appendChild($name);
					//endregion

					//region street
					$street_str = $building->street1;
					if (!empty($building->street2)) {
						$street_str .= ','.$building->street2;
					}
					$street = $xml->createElement('street');
					$listing->setAttribute('hide', false);
					$street->nodeValue = $street_str;
					$listing->appendChild($street);
					//endregion

					//region city
					$city = $xml->createElement('city');
					$city->nodeValue = $building->city;
					$listing->appendChild($city);
					//endregion

					//region state
					$state = $xml->createElement('state');
					$state->nodeValue = $building->state;
					$listing->appendChild($state);
					//endregion

					//region zip
					$zip = $xml->createElement('zip');
					$zip->nodeValue = $building->zip;
					$listing->appendChild($zip);
					//endregion

					//region country
					$country = $xml->createElement('country');
					$country->nodeValue = 'United States of America';
					$listing->appendChild($country);
					//endregion

					//region latitude
					$latitude = $xml->createElement('latitude');
					$latitude->nodeValue = $building->latitude;
					$listing->appendChild($latitude);
					//endregion

					//region longitude
					$longitude = $xml->createElement('longitude');
					$longitude->nodeValue = $building->longitude;
					$listing->appendChild($longitude);
					//endregion

					//region lastUpdated
					$lastUpdated = $xml->createElement('lastUpdated');
					$lastUpdated->nodeValue = $building->modified;
					$listing->appendChild($lastUpdated);
					//endregion

					//region contactName
					$contactName = $xml->createElement('contactName');
					$contactName->nodeValue = 'Goldfarb Properties';
					$listing->appendChild($contactName);
					//endregion

					//region contactEmail
					$contactEmail = $xml->createElement('contactEmail');
					$contactEmail->nodeValue = $building->region->email;
					$listing->appendChild($contactEmail);
					//endregion

					//region contactPhone
					$contactPhone = $xml->createElement('contactPhone');
					$contactPhone->nodeValue = $building->region->phone;
					$listing->appendChild($contactPhone);
					//endregion

					//region contactMethodPreference
					$contactMethodPreference = $xml->createElement('contactMethodPreference');
					$contactMethodPreference->nodeValue = 'Email';
					$listing->appendChild($contactMethodPreference);
					//endregion

					//region previewMessage
					$previewMessage = $xml->createElement('previewMessage');
					$previewMessage->appendChild($xml->createCDATASection($building->neighborhood->description));
					$listing->appendChild($previewMessage);
					//endregion

					//region terms
					$terms = $xml->createElement('terms');
					$terms->nodeValue = 'Minimum one month security';
					$listing->appendChild($terms);
					//endregion

					//region leaseTerm
					$leaseTerm = $xml->createElement('leaseTerm');
					$leaseTerm->nodeValue = '12 or 24 months';
					$listing->appendChild($leaseTerm);
					//endregion

					//region website
					$website = $xml->createElement('website');
					$website->nodeValue = $this->_domain."/rentals/{$building->region->code}/{$building->code}";
					$listing->appendChild($website);
					//endregion

					//region ListingTag
					$amenities = $this->_amenity_mapper->fetchAmenityNamesByBuildingId($building->id);
					if ($amenities) {
						foreach ($amenities as $amenity) {
							$ListingTag = $xml->createElement('ListingTag');
							$ListingTag->setAttribute('type', 'PROPERTY_AMENITY');
							$tag = $xml->createElement('tag');
							$tag->nodeValue = ($amenity == 'Parking included in rent') ? 'Parking' : $amenity;
							$ListingTag->appendChild($tag);
							$listing->appendChild($ListingTag);
						}
					}
					//endregion

					//region ListingPhoto (building specific)
					if (count($model_units)> 0) {
						switch ($building->code) {
							case 'pelham-park-view':
								foreach ($model_units as $model_unit) {
									if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
										if ($model_unit->floorplan_id =='2043139') {
											$model_images = $this->_unit_image_mapper->fetchOrderedByUnitId($model_unit->id);
											foreach ($model_images as $image) {
												$file = $this->_domain . $image->image;
												$modified = (file_exists($path.$image->image))
													? filemtime($path.$image->image) : 0;
												if ($modified >= $valid) {
													$file .= '?'.date ('W', $now) . date('N', $now);
												}
												$ListingPhoto = $xml->createElement('ListingPhoto');
												$ListingPhoto->setAttribute('source', $file);
												$label = $xml->createElement('label');
												$label->nodeValue = $image->title;
												$ListingPhoto->appendChild($label);
												$listing->appendChild($ListingPhoto);
											}
										}
									}
								}
								$building_images = $building_image_mapper->fetchOrderedByBuildingId($building->id);
								foreach ($building_images as $image) {
									$file = $this->_domain . $image->image;
									$modified = (file_exists($path.$image->image))
										? filemtime($path.$image->image) : 0;
									if ($modified >= $valid) {
										$file .= '?'.date ('W', $now) . date('N', $now);
									}
									$ListingPhoto = $xml->createElement('ListingPhoto');
									$ListingPhoto->setAttribute('source', $file);
									$label = $xml->createElement('label');
									$label->nodeValue = $image->title;
									$ListingPhoto->appendChild($label);
									$listing->appendChild($ListingPhoto);
								}
								break;
						}
					}
					//endregion

					//region Model (Model Units)
					foreach ($model_units as $model_unit) {
						$unit_concession_message = $this->_unit_mapper->getUnitConcessionMessage($model_unit->id);
						if ($model_unit->building_code == $building->code) {
							if (substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h') {
								if($model_unit->building_code == 'pelham-park-view') {
									if($model_unit->floorplan_id == '2043139' && $model_unit->number == 'WAIT-P0H') {
										$high_price = 0;
										foreach ($available_units as $available_unit) {
											if($available_unit->building_code == 'pelham-parkway-towers' || $available_unit->building_code == 'pelham-terrace' || $available_unit->building_code == 'pelham-park-view' || $available_unit->building_code == 'pelham-place') {
												if($available_unit->floorplan_id == '2043139' || $available_unit->floorplan_id == '2043129' || $available_unit->floorplan_id == '2043099') {
													$unit_net_effective_price
														= $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
													if ($unit_net_effective_price > $high_price) {
														$high_price = $unit_net_effective_price;
													}
												}
											}
										}
										$low_price = $high_price;
										foreach ($available_units as $available_unit) {
											if($available_unit->building_code == 'pelham-parkway-towers' || $available_unit->building_code == 'pelham-terrace' || $available_unit->building_code == 'pelham-park-view' || $available_unit->building_code == 'pelham-place') {
												if($available_unit->floorplan_id == '2043139' || $available_unit->floorplan_id == '2043129' || $available_unit->floorplan_id == '2043099') {
													$unit_net_effective_price
														= $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
													if ($unit_net_effective_price < $low_price) {
														$low_price = $unit_net_effective_price;
													}
												}
											}
										}
										//region Model
										$model = $xml->createElement('Model');
										$model->setAttribute('id', $model_unit->floorplan_id);
										$model->setAttribute('type', 'floorplan');
										$model->setAttribute('searchable', 'true');
										$model->setAttribute('pricingType', 'RANGE');

										$unit_number = !empty(($model_unit->feed_number_override))
											? ($model_unit->feed_number_override) : $model_unit->number;
										//region unitNumber
										$unitNumber = $xml->createElement('unitNumber', $unit_number);
										$model->appendChild($unitNumber);
										//endregion

										//region unitFloorNumber
										$unitFloorNumber = $xml->createElement('unitFloorNumber', $model_unit->floor);
										$model->appendChild($unitFloorNumber);
										//endregion

										//region lowPrice
										$lowPrice = $xml->createElement('lowPrice', $low_price);
										$model->appendChild($lowPrice);
										//endregion

										//region highPrice
										$highPrice = $xml->createElement('highPrice', $high_price);
										$model->appendChild($highPrice);
										//endregion

										//region name
										$name = $xml->createElement('name', $unit_number);
										$model->appendChild($name);
										//endregion

										//region numBedrooms
										$numBedrooms = $xml->createElement('numBedrooms', floor($model_unit->bedrooms));
										$model->appendChild($numBedrooms);
										//endregion

										//region numFullBaths
										$numFullBaths = $xml->createElement('numFullBaths',
											floor($model_unit->bathrooms));
										$model->appendChild($numFullBaths);
										//endregion

										//region numHalfBaths
										$numHalfBaths = $xml->createElement('numHalfBaths',
											ceil(fmod($model_unit->bathrooms, 1)));
										$model->appendChild($numHalfBaths);
										//endregion

										//region squareFeet
										if ($model_unit->square_footage_status == 'enabled') {
											$squareFeet = $xml->createElement('squareFeet',
												$model_unit->square_footage);
											$model->appendChild($squareFeet);
										}
										//endregion

										//region description
										$desc_str = ($unit_concession_message) ? $unit_concession_message : '';
										$desc_str .= ($model_unit->description) ? '<br><br>'.$model_unit->description : '';
										$description = $xml->createElement('description');
										$description->appendChild($xml->createCDATASection($desc_str));
										$model->appendChild($description);
										//endregion

										//region ModelLayout
										if (!empty($model_unit->floorplan)) {
											$file = $this->_domain.$model_unit->floorplan;
											$modified = (file_exists($path.$model_unit->floorplan))
												? filemtime($path.$model_unit->floorplan) : 0;
											if ($modified >= $valid) {
												$file .= '?week='.date('W', $now).date('N', $now);
											}
											$ModelLayout = $xml->createElement('ModelLayout');
											$ModelLayout->setAttribute('source', $file);
											$label = $xml->createElement('label');
											if ($model_unit->feed_number_override) {
												$label->nodeValue = $model_unit->feed_number_override;
											} else {
												$label->nodeValue = $model_unit->number;
											}
											$ModelLayout->appendChild($label);
											$model->appendChild($ModelLayout);
										}
										//endregion

										//region ModelTag
										$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($model_unit->id);
										if ($features) {
											foreach ($features as $feature) {
												$ModelTag = $xml->createElement('ModelTag');
												$ModelTag->setAttribute('type', 'AMENITY_SELECT');
												$tag = $xml->createElement('tag', $feature);
												$ModelTag->appendChild($tag);
												$model->appendChild($ModelTag);
											}
										}
										//endregion

										$listing->appendChild($model);
										//endregion

										//region Model (Available Units)
										foreach ($available_units as $available_unit) {
											$included_buildings = ['pelham-parkway-towers', 'pelham-terrace', 'pelham-park-view', 'pelham-place'];
											if(in_array($available_unit->building_code, $included_buildings)) {
												$included_floorplan_ids = ['2043139', '2043129', '2043099'];
												if(in_array($available_unit->floorplan_id, $included_floorplan_ids)) {
													$available_unit_unique_id = strtolower(sprintf('%sv%s', $available_unit->number, $available_unit->version));
													$available_unit_concession_message = $this->_unit_mapper->getUnitConcessionMessage($available_unit->id);
													$available_unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
													//region Model
													$model = $xml->createElement('Model');
													$model->setAttribute(
														'id',
														$available_unit->floorplan_id
														.'_'.$available_unit_unique_id
													);
													$model->setAttribute('parentModelId', $available_unit->floorplan_id);
													$model->setAttribute('type', 'unit');
													$model->setAttribute('searchable', 'true');
													$model->setAttribute('pricingType', 'FLAT');

													$unit_number = !empty(($available_unit->feed_number_override))
														? ($available_unit->feed_number_override) : $available_unit->number;
													//region unitNumber
													$unitNumber = $xml->createElement('unitNumber', $unit_number);
													$model->appendChild($unitNumber);
													//endregion

													//region lowPrice
													$lowPrice = $xml->createElement('lowPrice',
														$available_unit_net_effective_price);
													$model->appendChild($lowPrice);
													//endregion

													//region highPrice
													$highPrice = $xml->createElement('highPrice',
														$available_unit_net_effective_price);
													$model->appendChild($highPrice);
													//endregion

													//region numBedrooms
													$numBedrooms = $xml->createElement('numBedrooms',
														floor($available_unit->bedrooms));
													$model->appendChild($numBedrooms);
													//endregion

													//region numFullBaths
													$numFullBaths = $xml->createElement('numFullBaths',
														floor($available_unit->bathrooms));
													$model->appendChild($numFullBaths);
													//endregion

													//region squareFeet
													if ($available_unit->square_footage_status == 'enabled') {
														$squareFeet = $xml->createElement('squareFeet',
															$available_unit->square_footage);
														$model->appendChild($squareFeet);
													}
													//endregion

													//region dateAvailable
													if (array_key_exists($available_unit->yardi_code,$unit_yardi_data)) {
														$dateAvailable = $xml->createElement('dateAvailable',	$unit_yardi_data[$available_unit->yardi_code]->dateAvailable);
														$model->appendChild($dateAvailable);
													}
													//endregion

													//region description
													$desc_str = ($available_unit_concession_message) ? $available_unit_concession_message : '';
													$desc_str .= ($available_unit->description) ? '<br><br>'.$available_unit->description : '';
													$description = $xml->createElement('description');
													$description->appendChild($xml->createCDATASection($desc_str));
													$model->appendChild($description);
													//endregion

													//region ModelLayout
													if ($available_unit->floorplan) {
														$file = $this->_domain.$available_unit->floorplan;
														$modified = (file_exists($path.$available_unit->floorplan))
															? filemtime($path.$available_unit->floorplan) : 0;
														if ($modified >= $valid) {
															$file .= '?week='.date('W', $now).date('N', $now);
														}
														$ModelLayout = $xml->createElement('ModelLayout');
														$ModelLayout->setAttribute('source', $file);
														$label = $xml->createElement('label');
														if ($available_unit->feed_number_override) {
															$label->nodeValue = $available_unit->feed_number_override;
														} else {
															$label->nodeValue = $available_unit->number;
														}
														$ModelLayout->appendChild($label);
														$model->appendChild($ModelLayout);
													}
													//endregion

													//region ModelTag
													$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($available_unit->id);
													if ($features) {
														foreach ($features as $feature) {
															$ModelTag = $xml->createElement('ModelTag');
															$ModelTag->setAttribute('type', 'AMENITY_SELECT');
															$tag = $xml->createElement('tag', $feature);
															$ModelTag->appendChild($tag);
															$model->appendChild($ModelTag);
														}
													}
													//endregion

													$listing->appendChild($model);
													//endregion
												}
											}
										}
										//endregion
									}
									if($model_unit->floorplan_id == '2043140' && $model_unit->number == 'WAIT-P1H') {
										$high_price = 0;
										foreach ($available_units as $available_unit) {
											if($available_unit->building_code == 'pelham-parkway-towers' || $available_unit->building_code == 'pelham-terrace' || $available_unit->building_code == 'pelham-park-view' || $available_unit->building_code == 'pelham-place') {
												if($available_unit->floorplan_id == '2043140' || $available_unit->floorplan_id == '2043130' || $available_unit->floorplan_id == '2043153' || $available_unit->floorplan_id == '2043100') {
													$unit_net_effective_price
														= $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
													if ($unit_net_effective_price > $high_price) {
														$high_price = $unit_net_effective_price;
													}
												}
											}
										}
										$low_price = $high_price;
										foreach ($available_units as $available_unit) {
											if($available_unit->building_code == 'pelham-parkway-towers' || $available_unit->building_code == 'pelham-terrace' || $available_unit->building_code == 'pelham-park-view' || $available_unit->building_code == 'pelham-place') {
												if($available_unit->floorplan_id == '2043140' || $available_unit->floorplan_id == '2043130' || $available_unit->floorplan_id == '2043153' || $available_unit->floorplan_id == '2043100') {
													$unit_net_effective_price
														= $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
													if ($unit_net_effective_price < $low_price) {
														$low_price = $unit_net_effective_price;
													}
												}
											}
										}
										//region Model
										$model = $xml->createElement('Model');
										$model->setAttribute('id', $model_unit->floorplan_id);
										$model->setAttribute('type', 'floorplan');
										$model->setAttribute('searchable', 'true');
										$model->setAttribute('pricingType', 'RANGE');

										$unit_number = !empty(($model_unit->feed_number_override))
											? ($model_unit->feed_number_override) : $model_unit->number;
										//region unitNumber
										$unitNumber = $xml->createElement('unitNumber', $unit_number);
										$model->appendChild($unitNumber);
										//endregion

										//region unitFloorNumber
										$unitFloorNumber = $xml->createElement('unitFloorNumber', $model_unit->floor);
										$model->appendChild($unitFloorNumber);
										//endregion

										//region lowPrice
										$lowPrice = $xml->createElement('lowPrice', $low_price);
										$model->appendChild($lowPrice);
										//endregion

										//region highPrice
										$highPrice = $xml->createElement('highPrice', $high_price);
										$model->appendChild($highPrice);
										//endregion

										//region name
										$name = $xml->createElement('name', $unit_number);
										$model->appendChild($name);
										//endregion

										//region numBedrooms
										$numBedrooms = $xml->createElement('numBedrooms', floor($model_unit->bedrooms));
										$model->appendChild($numBedrooms);
										//endregion

										//region numFullBaths
										$numFullBaths = $xml->createElement('numFullBaths',
											floor($model_unit->bathrooms));
										$model->appendChild($numFullBaths);
										//endregion

										//region numHalfBaths
										$numHalfBaths = $xml->createElement('numHalfBaths',
											ceil(fmod($model_unit->bathrooms, 1)));
										$model->appendChild($numHalfBaths);
										//endregion

										//region squareFeet
										if ($model_unit->square_footage_status == 'enabled') {
											$squareFeet = $xml->createElement('squareFeet',
												$model_unit->square_footage);
											$model->appendChild($squareFeet);
										}
										//endregion

										//region description
										$desc_str = ($unit_concession_message) ? $unit_concession_message : '';
										$desc_str .= ($model_unit->description) ? '<br><br>'.$model_unit->description : '';
										$description = $xml->createElement('description');
										$description->appendChild($xml->createCDATASection($desc_str));
										$model->appendChild($description);
										//endregion

										//region ModelLayout
										if (!empty($model_unit->floorplan)) {
											$file = $this->_domain.$model_unit->floorplan;
											$modified = (file_exists($path.$model_unit->floorplan))
												? filemtime($path.$model_unit->floorplan) : 0;
											if ($modified >= $valid) {
												$file .= '?week='.date('W', $now).date('N', $now);
											}
											$ModelLayout = $xml->createElement('ModelLayout');
											$ModelLayout->setAttribute('source', $file);
											$label = $xml->createElement('label');
											if ($model_unit->feed_number_override) {
												$label->nodeValue = $model_unit->feed_number_override;
											} else {
												$label->nodeValue = $model_unit->number;
											}
											$ModelLayout->appendChild($label);
											$model->appendChild($ModelLayout);
										}
										//endregion

										//region ModelTag
										$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($model_unit->id);
										if ($features) {
											foreach ($features as $feature) {
												$ModelTag = $xml->createElement('ModelTag');
												$ModelTag->setAttribute('type', 'AMENITY_SELECT');
												$tag = $xml->createElement('tag', $feature);
												$ModelTag->appendChild($tag);
												$model->appendChild($ModelTag);
											}
										}
										//endregion

										$listing->appendChild($model);
										//endregion

										//region Model (Available Units)
										foreach ($available_units as $available_unit) {
											$included_buildings = ['pelham-parkway-towers', 'pelham-terrace', 'pelham-park-view', 'pelham-place'];
											if(in_array($available_unit->building_code, $included_buildings)) {
												$included_floorplan_ids = ['2043140', '2043130', '2043153', '2043100'];
												if(in_array($available_unit->floorplan_id, $included_floorplan_ids)) {
													$available_unit_unique_id = strtolower(sprintf('%sv%s', $available_unit->number, $available_unit->version));
													$available_unit_concession_message = $this->_unit_mapper->getUnitConcessionMessage($available_unit->id);
													$available_unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
													//region Model
													$model = $xml->createElement('Model');
													$model->setAttribute(
														'id',
														$available_unit->floorplan_id
														.'_'.$available_unit_unique_id
													);
													$model->setAttribute('parentModelId', $available_unit->floorplan_id);
													$model->setAttribute('type', 'unit');
													$model->setAttribute('searchable', 'true');
													$model->setAttribute('pricingType', 'FLAT');

													$unit_number = !empty(($available_unit->feed_number_override))
														? ($available_unit->feed_number_override) : $available_unit->number;
													//region unitNumber
													$unitNumber = $xml->createElement('unitNumber', $unit_number);
													$model->appendChild($unitNumber);
													//endregion

													//region lowPrice
													$lowPrice = $xml->createElement('lowPrice',
														$available_unit_net_effective_price);
													$model->appendChild($lowPrice);
													//endregion

													//region highPrice
													$highPrice = $xml->createElement('highPrice',
														$available_unit_net_effective_price);
													$model->appendChild($highPrice);
													//endregion

													//region numBedrooms
													$numBedrooms = $xml->createElement('numBedrooms',
														floor($available_unit->bedrooms));
													$model->appendChild($numBedrooms);
													//endregion

													//region numFullBaths
													$numFullBaths = $xml->createElement('numFullBaths',
														floor($available_unit->bathrooms));
													$model->appendChild($numFullBaths);
													//endregion

													//region squareFeet
													if ($available_unit->square_footage_status == 'enabled') {
														$squareFeet = $xml->createElement('squareFeet',
															$available_unit->square_footage);
														$model->appendChild($squareFeet);
													}
													//endregion

													//region dateAvailable
													if (array_key_exists($available_unit->yardi_code,$unit_yardi_data)) {
														$dateAvailable = $xml->createElement('dateAvailable',	$unit_yardi_data[$available_unit->yardi_code]->dateAvailable);
														$model->appendChild($dateAvailable);
													}
													//endregion

													//region description
													$desc_str = ($available_unit_concession_message) ? $available_unit_concession_message : '';
													$desc_str .= ($available_unit->description) ? '<br><br>'.$available_unit->description : '';
													$description = $xml->createElement('description');
													$description->appendChild($xml->createCDATASection($desc_str));
													$model->appendChild($description);
													//endregion

													//region ModelLayout
													if ($available_unit->floorplan) {
														$file = $this->_domain.$available_unit->floorplan;
														$modified = (file_exists($path.$available_unit->floorplan))
															? filemtime($path.$available_unit->floorplan) : 0;
														if ($modified >= $valid) {
															$file .= '?week='.date('W', $now).date('N', $now);
														}
														$ModelLayout = $xml->createElement('ModelLayout');
														$ModelLayout->setAttribute('source', $file);
														$label = $xml->createElement('label');
														if ($available_unit->feed_number_override) {
															$label->nodeValue = $available_unit->feed_number_override;
														} else {
															$label->nodeValue = $available_unit->number;
														}
														$ModelLayout->appendChild($label);
														$model->appendChild($ModelLayout);
													}
													//endregion

													//region ModelTag
													$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($available_unit->id);
													if ($features) {
														foreach ($features as $feature) {
															$ModelTag = $xml->createElement('ModelTag');
															$ModelTag->setAttribute('type', 'AMENITY_SELECT');
															$tag = $xml->createElement('tag', $feature);
															$ModelTag->appendChild($tag);
															$model->appendChild($ModelTag);
														}
													}
													//endregion

													$listing->appendChild($model);
													//endregion
												}
											}
										}
										//endregion
									}
									if($model_unit->floorplan_id == '2043141' && $model_unit->number == 'WAITP22H') {
										$high_price = 0;
										foreach ($available_units as $available_unit) {
											if($available_unit->building_code == 'pelham-parkway-towers' || $available_unit->building_code == 'pelham-terrace' || $available_unit->building_code == 'pelham-park-view' || $available_unit->building_code == 'pelham-place') {
												if($available_unit->floorplan_id == '2043141' || $available_unit->floorplan_id == '2043132' || $available_unit->floorplan_id == '2043155' || $available_unit->floorplan_id == '2043102') {
													$unit_net_effective_price
														= $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
													if ($unit_net_effective_price > $high_price) {
														$high_price = $unit_net_effective_price;
													}
												}
											}
										}
										$low_price = $high_price;
										foreach ($available_units as $available_unit) {
											if($available_unit->building_code == 'pelham-parkway-towers' || $available_unit->building_code == 'pelham-terrace' || $available_unit->building_code == 'pelham-park-view' || $available_unit->building_code == 'pelham-place') {
												if($available_unit->floorplan_id == '2043141' || $available_unit->floorplan_id == '2043132' || $available_unit->floorplan_id == '2043155' || $available_unit->floorplan_id == '2043102') {
													$unit_net_effective_price
														= $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
													if ($unit_net_effective_price < $low_price) {
														$low_price = $unit_net_effective_price;
													}
												}
											}
										}
										//region Model
										$model = $xml->createElement('Model');
										$model->setAttribute('id', $model_unit->floorplan_id);
										$model->setAttribute('type', 'floorplan');
										$model->setAttribute('searchable', 'true');
										$model->setAttribute('pricingType', 'RANGE');

										$unit_number = !empty(($model_unit->feed_number_override))
											? ($model_unit->feed_number_override) : $model_unit->number;
										//region unitNumber
										$unitNumber = $xml->createElement('unitNumber', $unit_number);
										$model->appendChild($unitNumber);
										//endregion

										//region unitFloorNumber
										$unitFloorNumber = $xml->createElement('unitFloorNumber', $model_unit->floor);
										$model->appendChild($unitFloorNumber);
										//endregion

										//region lowPrice
										$lowPrice = $xml->createElement('lowPrice', $low_price);
										$model->appendChild($lowPrice);
										//endregion

										//region highPrice
										$highPrice = $xml->createElement('highPrice', $high_price);
										$model->appendChild($highPrice);
										//endregion

										//region name
										$name = $xml->createElement('name', $unit_number);
										$model->appendChild($name);
										//endregion

										//region numBedrooms
										$numBedrooms = $xml->createElement('numBedrooms', floor($model_unit->bedrooms));
										$model->appendChild($numBedrooms);
										//endregion

										//region numFullBaths
										$numFullBaths = $xml->createElement('numFullBaths',
											floor($model_unit->bathrooms));
										$model->appendChild($numFullBaths);
										//endregion

										//region numHalfBaths
										$numHalfBaths = $xml->createElement('numHalfBaths',
											ceil(fmod($model_unit->bathrooms, 1)));
										$model->appendChild($numHalfBaths);
										//endregion

										//region squareFeet
										if ($model_unit->square_footage_status == 'enabled') {
											$squareFeet = $xml->createElement('squareFeet',
												$model_unit->square_footage);
											$model->appendChild($squareFeet);
										}
										//endregion

										//region description
										$desc_str = ($unit_concession_message) ? $unit_concession_message : '';
										$desc_str .= ($model_unit->description) ? '<br><br>'.$model_unit->description : '';
										$description = $xml->createElement('description');
										$description->appendChild($xml->createCDATASection($desc_str));
										$model->appendChild($description);
										//endregion

										//region ModelLayout
										if (!empty($model_unit->floorplan)) {
											$file = $this->_domain.$model_unit->floorplan;
											$modified = (file_exists($path.$model_unit->floorplan))
												? filemtime($path.$model_unit->floorplan) : 0;
											if ($modified >= $valid) {
												$file .= '?week='.date('W', $now).date('N', $now);
											}
											$ModelLayout = $xml->createElement('ModelLayout');
											$ModelLayout->setAttribute('source', $file);
											$label = $xml->createElement('label');
											if ($model_unit->feed_number_override) {
												$label->nodeValue = $model_unit->feed_number_override;
											} else {
												$label->nodeValue = $model_unit->number;
											}
											$ModelLayout->appendChild($label);
											$model->appendChild($ModelLayout);
										}
										//endregion

										//region ModelTag
										$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($model_unit->id);
										if ($features) {
											foreach ($features as $feature) {
												$ModelTag = $xml->createElement('ModelTag');
												$ModelTag->setAttribute('type', 'AMENITY_SELECT');
												$tag = $xml->createElement('tag', $feature);
												$ModelTag->appendChild($tag);
												$model->appendChild($ModelTag);
											}
										}
										//endregion

										$listing->appendChild($model);
										//endregion

										//region Model (Available Units)
										foreach ($available_units as $available_unit) {
											$included_buildings = ['pelham-parkway-towers', 'pelham-terrace', 'pelham-park-view', 'pelham-place'];
											if(in_array($available_unit->building_code, $included_buildings)) {
												$included_floorplan_ids = ['2043141', '2043132', '2043155', '2043102'];
												if(in_array($available_unit->floorplan_id, $included_floorplan_ids)) {
													$available_unit_unique_id = strtolower(sprintf('%sv%s', $available_unit->number, $available_unit->version));
													$available_unit_concession_message = $this->_unit_mapper->getUnitConcessionMessage($available_unit->id);
													$available_unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
													//region Model
													$model = $xml->createElement('Model');
													$model->setAttribute(
														'id',
														$available_unit->floorplan_id
														.'_'.$available_unit_unique_id
													);
													$model->setAttribute('parentModelId', $available_unit->floorplan_id);
													$model->setAttribute('type', 'unit');
													$model->setAttribute('searchable', 'true');
													$model->setAttribute('pricingType', 'FLAT');

													$unit_number = !empty(($available_unit->feed_number_override))
														? ($available_unit->feed_number_override) : $available_unit->number;
													//region unitNumber
													$unitNumber = $xml->createElement('unitNumber', $unit_number);
													$model->appendChild($unitNumber);
													//endregion

													//region lowPrice
													$lowPrice = $xml->createElement('lowPrice',
														$available_unit_net_effective_price);
													$model->appendChild($lowPrice);
													//endregion

													//region highPrice
													$highPrice = $xml->createElement('highPrice',
														$available_unit_net_effective_price);
													$model->appendChild($highPrice);
													//endregion

													//region numBedrooms
													$numBedrooms = $xml->createElement('numBedrooms',
														floor($available_unit->bedrooms));
													$model->appendChild($numBedrooms);
													//endregion

													//region numFullBaths
													$numFullBaths = $xml->createElement('numFullBaths',
														floor($available_unit->bathrooms));
													$model->appendChild($numFullBaths);
													//endregion

													//region squareFeet
													if ($available_unit->square_footage_status == 'enabled') {
														$squareFeet = $xml->createElement('squareFeet',
															$available_unit->square_footage);
														$model->appendChild($squareFeet);
													}
													//endregion

													//region dateAvailable
													if (array_key_exists($available_unit->yardi_code,$unit_yardi_data)) {
														$dateAvailable = $xml->createElement('dateAvailable',	$unit_yardi_data[$available_unit->yardi_code]->dateAvailable);
														$model->appendChild($dateAvailable);
													}
													//endregion

													//region description
													$desc_str = ($available_unit_concession_message) ? $available_unit_concession_message : '';
													$desc_str .= ($available_unit->description) ? '<br><br>'.$available_unit->description : '';
													$description = $xml->createElement('description');
													$description->appendChild($xml->createCDATASection($desc_str));
													$model->appendChild($description);
													//endregion

													//region ModelLayout
													if ($available_unit->floorplan) {
														$file = $this->_domain.$available_unit->floorplan;
														$modified = (file_exists($path.$available_unit->floorplan))
															? filemtime($path.$available_unit->floorplan) : 0;
														if ($modified >= $valid) {
															$file .= '?week='.date('W', $now).date('N', $now);
														}
														$ModelLayout = $xml->createElement('ModelLayout');
														$ModelLayout->setAttribute('source', $file);
														$label = $xml->createElement('label');
														if ($available_unit->feed_number_override) {
															$label->nodeValue = $available_unit->feed_number_override;
														} else {
															$label->nodeValue = $available_unit->number;
														}
														$ModelLayout->appendChild($label);
														$model->appendChild($ModelLayout);
													}
													//endregion

													//region ModelTag
													$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($available_unit->id);
													if ($features) {
														foreach ($features as $feature) {
															$ModelTag = $xml->createElement('ModelTag');
															$ModelTag->setAttribute('type', 'AMENITY_SELECT');
															$tag = $xml->createElement('tag', $feature);
															$ModelTag->appendChild($tag);
															$model->appendChild($ModelTag);
														}
													}
													//endregion

													$listing->appendChild($model);
													//endregion
												}
											}
										}
										//endregion
									}
								}
							}
						}
					}
					//endregion

					//region providerType
					//@todo: WTF! This is never altered. It is set to -1 up top. The XML is never completed in the
					//  original code. Going to append listing to the main element. In the original code a new listing
					//  would start without closing the old one.
					if($ppn == 0) {
						$providerType = $xml->createElement('providerType', 'Property Manager');
						$listing->appendChild($providerType);
					}
					//endregion

					$main_element->appendChild($listing);
					//endregion
				}
			}
			//endregion

			//region Third Group Of Buildings
			if(in_array($building->code, $grouped_building['third'])) {
				if ($this->_building_mapper->hasVacancy($building->id)) {
					//region Listing
					$listing = $xml->createElement('Listing');
					$listing->setAttribute('id', $building->code);
					$listing->setAttribute('type', 'RENTAL');
					// todo: This has to be an error: Should be companyId not companyld
					$listing->setAttribute('companyld', 'goldfarb');
					$listing->setAttribute('propertyType', 'LARGE');

					//region name
					$name = $xml->createElement('name');
					$name->nodeValue = $building->name;
					$listing->appendChild($name);
					//endregion

					//region street
					$street_str = $building->street1;
					if (!empty($building->street2)) {
						$street_str .= ','.$building->street2;
					}
					$street = $xml->createElement('street');
					$listing->setAttribute('hide', false);
					$street->nodeValue = $street_str;
					$listing->appendChild($street);
					//endregion

					//region city
					$city = $xml->createElement('city');
					$city->nodeValue = $building->city;
					$listing->appendChild($city);
					//endregion

					//region state
					$state = $xml->createElement('state');
					$state->nodeValue = $building->state;
					$listing->appendChild($state);
					//endregion

					//region zip
					$zip = $xml->createElement('zip');
					$zip->nodeValue = $building->zip;
					$listing->appendChild($zip);
					//endregion

					//region country
					$country = $xml->createElement('country');
					$country->nodeValue = 'United States of America';
					$listing->appendChild($country);
					//endregion

					//region latitude
					$latitude = $xml->createElement('latitude');
					$latitude->nodeValue = $building->latitude;
					$listing->appendChild($latitude);
					//endregion

					//region longitude
					$longitude = $xml->createElement('longitude');
					$longitude->nodeValue = $building->longitude;
					$listing->appendChild($longitude);
					//endregion

					//region lastUpdated
					$lastUpdated = $xml->createElement('lastUpdated');
					$lastUpdated->nodeValue = $building->modified;
					$listing->appendChild($lastUpdated);
					//endregion

					//region contactName
					$contactName = $xml->createElement('contactName');
					$contactName->nodeValue = 'Goldfarb Properties';
					$listing->appendChild($contactName);
					//endregion

					//region contactEmail
					$contactEmail = $xml->createElement('contactEmail');
					$contactEmail->nodeValue = $building->region->email;
					$listing->appendChild($contactEmail);
					//endregion

					//region contactPhone
					$contactPhone = $xml->createElement('contactPhone');
					$contactPhone->nodeValue = $building->region->phone;
					$listing->appendChild($contactPhone);
					//endregion

					//region contactMethodPreference
					$contactMethodPreference = $xml->createElement('contactMethodPreference');
					$contactMethodPreference->nodeValue = 'Email';
					$listing->appendChild($contactMethodPreference);
					//endregion

					//region previewMessage
					$previewMessage = $xml->createElement('previewMessage');
					$previewMessage->appendChild($xml->createCDATASection($building->neighborhood->description));
					$listing->appendChild($previewMessage);
					//endregion

					//region terms
					$terms = $xml->createElement('terms');
					$terms->nodeValue = 'Minimum one month security';
					$listing->appendChild($terms);
					//endregion

					//region leaseTerm
					$leaseTerm = $xml->createElement('leaseTerm');
					$leaseTerm->nodeValue = '12 or 24 months';
					$listing->appendChild($leaseTerm);
					//endregion

					//region website
					$website = $xml->createElement('website');
					$website->nodeValue = $this->_domain."/rentals/{$building->region->code}/{$building->code}";
					$listing->appendChild($website);
					//endregion

					//region ListingTag
					$amenities = $this->_amenity_mapper->fetchAmenityNamesByBuildingId($building->id);
					if ($amenities) {
						foreach ($amenities as $amenity) {
							$ListingTag = $xml->createElement('ListingTag');
							$ListingTag->setAttribute('type', 'PROPERTY_AMENITY');
							$tag = $xml->createElement('tag');
							$tag->nodeValue = $amenity;
							$ListingTag->appendChild($tag);
							$listing->appendChild($ListingTag);
						}
					}
					//endregion

					//region ListingPhoto (buildings)
					$building_images = $building_image_mapper->fetchOrderedByBuildingId($building->id);
					foreach ($building_images as $image) {
						$modified = (file_exists($path.$image->image))
							? filemtime($path.$image->image) : 0;
						$file = $this->_domain . $image->image;
						if ($modified >= $valid) {
							$file .= '?'.date ('W', $now) . date('N', $now);
						}
						$ListingPhoto = $xml->createElement('ListingPhoto');
						$ListingPhoto->setAttribute('source', $file);
						$label = $xml->createElement('label');
						$label->nodeValue = $image->title;
						$ListingPhoto->appendChild($label);
						$listing->appendChild($ListingPhoto);
					}
					//endregion

					//region ListingPhoto (available_units)
					/* TODO - Either this is a copy/paste mistake or the code needs to be checked.
					// $available_unit->images doesn't exist so ...?!?!

					foreach ($available_units as $available_unit) {
						if($available_unit->building_code == $building->code) {
							foreach ($available_unit->images as $image) {
								$modified = (file_exists($path.$image->image))
									? filemtime($path.$image->image) : 0;
								$file = $this->_domain . $image->image;
								if ($modified >= $valid) {
									$file .= '?'.date ('W', $now) . date('N', $now);
								}
								$ListingPhoto = $xml->createElement('ListingPhoto');
								$ListingPhoto->setAttribute('source', $file);
								$label = $xml->createElement('label');
								$label->nodeValue = $image->title;
								$ListingPhoto->appendChild($label);
								$listing->appendChild($ListingPhoto);
							}
						}
					}
					*/
					//endregion

					//region Model (Available Units)
					foreach ($available_units as $available_unit) {
						$available_unit_concession_message = $this->_unit_mapper->getUnitConcessionMessage($available_unit->id);
						$available_unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($available_unit->id);
						if($available_unit->building_code == $building->code) {
							//region Model
							$model = $xml->createElement('Model');
							$model->setAttribute('id', $available_unit->floorplan_id);
							$model->setAttribute('type', 'unit');
							$model->setAttribute('searchable', 'true');
							$model->setAttribute('pricingType', 'FLAT');

							$unit_number = !empty(($available_unit->feed_number_override))
								? ($available_unit->feed_number_override) : $available_unit->number;
							//region unitNumber
							$unitNumber = $xml->createElement('unitNumber', $unit_number);
							$model->appendChild($unitNumber);
							//endregion

							//region lowPrice
							$lowPrice = $xml->createElement('lowPrice', $available_unit_net_effective_price);
							$model->appendChild($lowPrice);
							//endregion

							//region highPrice
							$highPrice = $xml->createElement('highPrice', $available_unit_net_effective_price);
							$model->appendChild($highPrice);
							//endregion

							//region numBedrooms
							$numBedrooms = $xml->createElement('numBedrooms', floor($available_unit->bedrooms));
							$model->appendChild($numBedrooms);
							//endregion

							//region numFullBaths
							$numFullBaths = $xml->createElement('numFullBaths', floor($available_unit->bathrooms));
							$model->appendChild($numFullBaths);
							//endregion

							//region squareFeet
							if ($available_unit->square_footage_status == 'enabled') {
								$squareFeet = $xml->createElement('squareFeet', $available_unit->square_footage);
								$model->appendChild($squareFeet);
							}
							//endregion

							//region dateAvailable
							if (array_key_exists($available_unit->yardi_code,$unit_yardi_data)) {
								$dateAvailable = $xml->createElement('dateAvailable',	$unit_yardi_data[$available_unit->yardi_code]->dateAvailable);
								$model->appendChild($dateAvailable);
							}
							//endregion

							//region description
							$desc_str = ($available_unit_concession_message) ? $available_unit_concession_message : '';
							$desc_str .= ($available_unit->description) ? '<br><br>'.$available_unit->description : '';
							$description = $xml->createElement('description');
							$description->appendChild($xml->createCDATASection($desc_str));
							$model->appendChild($description);
							//endregion

							//region ModelTag
							$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($available_unit->id);
							if ($features) {
								foreach ($features as $feature) {
									$ModelTag = $xml->createElement('ModelTag');
									$ModelTag->setAttribute('type', 'AMENITY_SELECT');
									$tag = $xml->createElement('tag', $feature);
									$ModelTag->appendChild($tag);
									$model->appendChild($ModelTag);
								}
							}
							//endregion

							$listing->appendChild($model);
							//endregion
						}
					}
					//endregion


					//region providerType
					$providerType = $xml->createElement('providerType', 'Property Manager');
					$listing->appendChild($providerType);
					//endregion

					$main_element->appendChild($listing);
					//endregion
				}
			}
			//endregion
		}
		$xml->appendChild($main_element);
		$xml->formatOutput = TRUE;
		header('Content-type: text/xml');
		echo $xml->saveXML();
		exit;

		//region Reference Source
/*
<hotPadsItems version="2.1">
  <Company id="goldfarb">
    <name>Goldfarb Properties</name>
    <website>http://goldfarbproperties.com</website>
      <city>New York</city>
    <state>NY</state>
    <CompanyLogo source="{{asset('/img/email/goldfarb.png')}}"/>
  </Company>
  <!-- Listing is repeatable -->
<?php
$ppn = 0;
foreach($buildings as $building){
  $building->has_vacancy = false;
  foreach($available_units as $available_unit){
    if($available_unit->building->slug == $building->slug){
      $building->has_vacancy = true;
      foreach($building->yardi_data as $yardi_data){
        if($yardi_data['ApartmentId'] == $available_unit->yardi_code){
          $available_unit->date_available = $yardi_data['AvailableDate'];
          $available_unit->floorplan_id = $yardi_data['FloorplanId'];
        }
      }
    }
  }
  foreach($model_units as $model_unit){
    if($model_unit->building->slug == $building->slug){
      foreach($building->yardi_data as $yardi_data){
        if($yardi_data['ApartmentId'] == $model_unit->yardi_code){
          $model_unit->floorplan_id = $yardi_data['FloorplanId'];
        }
      }
    }
  }
  if($building->slug == 'pelham-parkway-towers' || $building->slug == 'pelham-terrace' || $building->slug == 'pelham-park-view' || $building->slug == 'pelham-place') {
    $ppn += 1;
  }
}
?>
@foreach ($buildings as $building)
  @if($building->slug == 'wavecrest-gardens' || $building->slug == 'the-grand' || $building->slug == 'drake-house' || $building->slug == 'harbor-house' || $building->slug == 'maple-gardens' || $building->slug == 'the-churchill' || $building->slug == 'noonan-towers')
    @if($building->has_vacancy)
      <Listing id="{{$building->slug}}" type="RENTAL" companyld="goldfarb" propertyType="LARGE">
      <name>{{$building->name}}</name>
      <street hide="false">{{$building->address->street_1}}{{(!!$building->address->street_2) ? ',' . $building->address->street_2 : null}}</street>
      <city>{{$building->address->city}}</city>
      <state>{{$building->address->state}}</state>
      <zip>{{$building->address->zip}}</zip>
      <country>United States of America</country>
      <latitude>{{$building->address->getLatLngAttribute()['lat']}}</latitude>
      <longitude>{{$building->address->getLatLngAttribute()['lng']}}</longitude>
      <lastUpdated>{{$building->updated_at}}</lastUpdated>
      <contactName>Goldfarb Properties</contactName>
      <contactEmail>{{$building->neighborhood->region->email}}</contactEmail>
      <contactPhone>{{$building->neighborhood->region->phone}}</contactPhone>
      <contactMethodPreference>Email</contactMethodPreference>
      <previewMessage><![CDATA[{{$building->neighborhood->body}}]]></previewMessage>
      <terms>Minimum one month security</terms>
      <leaseTerm>12 or 24 months</leaseTerm>
      <website>{{route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug, 'buildingSlug' => $building->slug])}}</website>
      @foreach ($building->amenities as $amenity)
        <ListingTag type="PROPERTY_AMENITY"><tag>{{$amenity->name}}</tag></ListingTag>
      @endforeach

@if (count($model_units)> 0)
@if($building->slug == 'wavecrest-gardens')
@foreach ($model_units as $model_unit)
@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')
@if($model_unit->number =='WAIT-Q1H')
@if($model_unit->floorplan_id =='1045539')
@foreach ($model_unit->images as $image)
<?php
$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
$modified = filemtime($path . $file);
if($modified >= $valid) { ?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
} else {
	?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
}
?>
@endforeach
@endif
@endif
@endif
@endforeach
@elseif($building->slug == 'the-grand')
@foreach ($model_units as $model_unit)
@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')
@if($model_unit->number == '')
@if($model_unit->floorplan_id == '')
@foreach ($model_unit->images as $image)
<?php
$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
$modified = filemtime($path . $file);
if($modified >= $valid) { ?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
} else {
	?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
}
?>
@endforeach
@endif
@endif
@endif
@endforeach
@elseif($building->slug == 'drake-house')
@foreach ($model_units as $model_unit)
@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')
@if($model_unit->number =='WAIT-D0H')
@if($model_unit->floorplan_id == '2043074')
@foreach ($model_unit->images as $image)
<?php
$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
$modified = filemtime($path . $file);
if($modified >= $valid) { ?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
} else {
	?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
}
?>
@endforeach
@endif
@endif
@endif
@endforeach
@elseif($building->slug == 'harbor-house')
@foreach ($model_units as $model_unit)
@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')
@if($model_unit->number =='WAIT-H0H')
@if($model_unit->floorplan_id == '1045542')
@foreach ($model_unit->images as $image)
<?php
$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
$modified = filemtime($path . $file);
if($modified >= $valid) { ?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
} else {
	?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
}
?>
@endforeach
@endif
@endif
@endif
@endforeach
@elseif($building->slug == 'maple-gardens')
@foreach ($model_units as $model_unit)
@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')
@if($model_unit->number =='WAIT-I0H')
@if($model_unit->floorplan_id == '2043124')
@foreach ($model_unit->images as $image)
<?php
$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
$modified = filemtime($path . $file);
if($modified >= $valid) { ?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
} else {
	?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
}
?>
@endforeach
@endif
@endif
@endif
@endforeach
@elseif($building->slug == 'the-churchill')
@foreach ($model_units as $model_unit)
@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')
@if($model_unit->number =='WAIT-W0H')
@if($model_unit->floorplan_id == '2043171')
@foreach ($model_unit->images as $image)
<?php
$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
$modified = filemtime($path . $file);
if($modified >= $valid) { ?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
} else {
	?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
}
?>
@endforeach
@endif
@endif
@endif
@endforeach
@elseif($building->slug == 'noonan-towers')
@foreach ($model_units as $model_unit)
@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')
@if($model_unit->number =='WAIT-N0H')
@if($model_unit->floorplan_id == '2043117')
@foreach ($model_unit->images as $image)
<?php
$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
$modified = filemtime($path . $file);
if($modified >= $valid) { ?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
} else {
	?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
}
?>
@endforeach
@endif
@endif
@endif
@endforeach
@endif
@endif
@foreach ($building->images as $image)
<?php
$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
$modified = filemtime($path . $file);
if($modified >= $valid) { ?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
} else {
	?>
	<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
	<label>{{$image->title}}</label>
	</ListingPhoto>
	<?php
}
?>
@endforeach
@foreach ($model_units as $model_unit)
@if($model_unit->building->slug == $building->slug)
@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')

<?php
$high_price = 0;
foreach ($available_units as $available_unit) {
	if($available_unit->building->slug == $model_unit->building->slug) {
		if($available_unit->floorplan_id == $model_unit->floorplan_id) {
			if($available_unit->net_effective_price > $high_price) {
				$high_price = $available_unit->net_effective_price;
			}
		}
	}
}
$low_price = $high_price;
foreach ($available_units as $available_unit) {
	if($available_unit->building->slug == $model_unit->building->slug) {
		if($available_unit->floorplan_id == $model_unit->floorplan_id) {
			if($available_unit->net_effective_price < $low_price) {
				$low_price = $available_unit->net_effective_price;
			}
		}
	}
}
?>
<Model id="{{$model_unit->floorplan_id}}" type="floorplan" searchable="true" pricingType="RANGE">
	<unitNumber>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</unitNumber>
	<unitFloorNumber>{{$model_unit->floor}}</unitFloorNumber>
	<lowPrice>{{$low_price}}</lowPrice>
	<highPrice>{{$high_price}}</highPrice>
	<name>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</name>
	<numBedrooms>{{floor($model_unit->bedrooms)}}</numBedrooms>
	<numFullBaths>{{floor($model_unit->bathrooms)}}</numFullBaths>
	<numHalfBaths>{{ceil(fmod($model_unit->bathrooms, 1))}}</numHalfBaths>
	@if ($model_unit->square_footage_toggle == 'enabled')
	<squareFeet>{{$model_unit->square_footage}}</squareFeet>
	@endif
	<description><![CDATA[ @if (!!$model_unit->getConcessionMessage()) {{$model_unit->getConcessionMessage()}}<br><br>@endif @if (!!$model_unit->body){{$model_unit->body}}@endif ]]></description>
	@if ($model_unit->hasBasicFloorplan())
	<?php
	$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $model_unit->basicFloorplanSrc());
	$modified = filemtime($path . $file);
	if($modified >= $valid) {
		?>
		<ModelLayout source="{{asset($model_unit->basicFloorplanSrc())}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
			<label>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</label>
		</ModelLayout>
	<?php } else { ?>
		<ModelLayout source="{{asset($model_unit->basicFloorplanSrc())}}">
			<label>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</label>
		</ModelLayout>
	<?php } ?>
	@endif
	@foreach ($model_unit->features as $feature)
	<ModelTag type="AMENITY_SELECT"><tag>{{$feature->name}}</tag></ModelTag>
	@endforeach
</Model>
@foreach ($available_units as $available_unit)
@if($available_unit->building->slug == $model_unit->building->slug)
@if($available_unit->floorplan_id == $model_unit->floorplan_id)
<Model id="{{$available_unit->floorplan_id}}_{{$available_unit->getUniqueVersionIdAttribute()}}" parentModelId="{{$available_unit->floorplan_id}}" type="unit" searchable="true" pricingType="FLAT">
	<unitNumber>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</unitNumber>
	<lowPrice>{{$available_unit->net_effective_price}}</lowPrice>
	<highPrice>{{$available_unit->net_effective_price}}</highPrice>
	<numBedrooms>{{floor($available_unit->bedrooms)}}</numBedrooms>
	<numFullBaths>{{floor($available_unit->bathrooms)}}</numFullBaths>
	@if ($available_unit->square_footage_toggle == 'enabled')
	<squareFeet>{{$available_unit->square_footage}}</squareFeet>
	@endif
	<dateAvailable>{{ $available_unit->date_available }}</dateAvailable>
	<description><![CDATA[ @if (!!$available_unit->getConcessionMessage()) {{$available_unit->getConcessionMessage()}}<br><br>@endif @if (!!$available_unit->body){{$available_unit->body}}@endif ]]></description>
	@if ($available_unit->hasBasicFloorplan())
	<?php
	$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $available_unit->basicFloorplanSrc());
	$modified = filemtime($path . $file);
	if($modified >= $valid) {
		?>
		<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
			<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
		</ModelLayout>
	<?php } else { ?>
		<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}">
			<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
		</ModelLayout>
	<?php } ?>
	@endif
	@foreach ($available_unit->features as $feature)
	<ModelTag type="AMENITY_SELECT"><tag>{{$feature->name}}</tag></ModelTag>
	@endforeach
</Model>
@endif
@endif
@endforeach
@endif
@endif
@endforeach
<providerType>Property Manager</providerType>
</Listing>
@endif
@endif

@if($building->slug == 'pelham-parkway-towers' || $building->slug == 'pelham-terrace' || $building->slug == 'pelham-park-view' || $building->slug == 'pelham-place')
<?php $ppn -= 1; ?>
@if($building->slug == 'pelham-park-view')
<Listing id="{{$building->slug}}" type="RENTAL" companyld="goldfarb" propertyType="LARGE">
	<name>{{$building->name}}</name>
	<street hide="false">{{$building->address->street_1}}{{(!!$building->address->street_2) ? ',' . $building->address->street_2 : null}}</street>
	<city>{{$building->address->city}}</city>
	<state>{{$building->address->state}}</state>
	<zip>{{$building->address->zip}}</zip>
	<country>United States of America</country>
	<latitude>{{$building->address->getLatLngAttribute()['lat']}}</latitude>
	<longitude>{{$building->address->getLatLngAttribute()['lng']}}</longitude>
	<lastUpdated>{{$building->updated_at}}</lastUpdated>
	<contactName>Goldfarb Properties</contactName>
	<contactEmail>{{$building->neighborhood->region->email}}</contactEmail>
	<contactPhone>{{$building->neighborhood->region->phone}}</contactPhone>
	<contactMethodPreference>Email</contactMethodPreference>
	<previewMessage><![CDATA[{{$building->neighborhood->body}}]]></previewMessage>
	<terms>Minimum one month security</terms>
	<leaseTerm>12 or 24 months</leaseTerm>
	<website>{{route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug, 'buildingSlug' => $building->slug])}}</website>
	@foreach ($building->amenities as $amenity)
	@if ($amenity->name == 'Parking included in rent')
	<ListingTag type="PROPERTY_AMENITY"><tag>Parking</tag></ListingTag>
	@else
	<ListingTag type="PROPERTY_AMENITY"><tag>{{$amenity->name}}</tag></ListingTag>
	@endif
	@endforeach

	@foreach ($model_units as $model_unit)
	@if($model_unit->building->slug == 'pelham-park-view')
	@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')
	@if($model_unit->floorplan_id == '2043139')
	@foreach ($model_unit->images as $image)
	<?php
	$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
	$modified = filemtime($path . $file);
	if($modified >= $valid) { ?>
		<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
		<label>{{$image->title}}</label>
		</ListingPhoto>
		<?php
	} else {
		?>
		<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
		<label>{{$image->title}}</label>
		</ListingPhoto>
		<?php
	}
	?>
	@endforeach
	@endif
	@endif
	@endif
	@endforeach
	@foreach ($building->images as $image)
	<?php
	$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
	$modified = filemtime($path . $file);
	if($modified >= $valid) { ?>
		<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
		<label>{{$image->title}}</label>
		</ListingPhoto>
		<?php
	} else {
		?>
		<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
		<label>{{$image->title}}</label>
		</ListingPhoto>
		<?php
	}
	?>
	@endforeach
	@endif
	@foreach ($model_units as $model_unit)
	@if($model_unit->building->slug == $building->slug)
	@if(substr($model_unit->number, -1) == 'H' || substr($model_unit->number, -1) == 'h')
	@if($model_unit->building->slug == 'pelham-park-view')
	@if($model_unit->floorplan_id == '2043139' && $model_unit->number == 'WAIT-P0H')
	<?php
	$high_price = 0;
	foreach ($available_units as $available_unit) {
		if($available_unit->building->slug == 'pelham-parkway-towers' || $available_unit->building->slug == 'pelham-terrace' || $available_unit->building->slug == 'pelham-park-view' || $available_unit->building->slug == 'pelham-place') {
			if($available_unit->floorplan_id == '2043139' || $available_unit->floorplan_id == '2043129' || $available_unit->floorplan_id == '2043099') {
				if($available_unit->net_effective_price > $high_price) {
					$high_price = $available_unit->net_effective_price;
				}
			}
		}
	}
	$low_price = $high_price;
	foreach ($available_units as $available_unit) {
		if($available_unit->building->slug == 'pelham-parkway-towers' || $available_unit->building->slug == 'pelham-terrace' || $available_unit->building->slug == 'pelham-park-view' || $available_unit->building->slug == 'pelham-place') {
			if($available_unit->floorplan_id == '2043139' || $available_unit->floorplan_id == '2043129' || $available_unit->floorplan_id == '2043099') {
				if($available_unit->net_effective_price < $low_price) {
					$low_price = $available_unit->net_effective_price;
				}
			}
		}
	}
	?>
	<Model id="{{$model_unit->floorplan_id}}" type="floorplan" searchable="true" pricingType="RANGE">
		<unitNumber>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</unitNumber>
		<unitFloorNumber>{{$model_unit->floor}}</unitFloorNumber>
		<lowPrice>{{$low_price}}</lowPrice>
		<highPrice>{{$high_price}}</highPrice>
		<name>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</name>
		<numBedrooms>{{floor($model_unit->bedrooms)}}</numBedrooms>
		<numFullBaths>{{floor($model_unit->bathrooms)}}</numFullBaths>
		<numHalfBaths>{{ceil(fmod($model_unit->bathrooms, 1))}}</numHalfBaths>
		@if ($model_unit->square_footage_toggle == 'enabled')
		<squareFeet>{{$model_unit->square_footage}}</squareFeet>
		@endif
		<description><![CDATA[ @if (!!$model_unit->getConcessionMessage()) {{$model_unit->getConcessionMessage()}}<br><br>@endif @if (!!$model_unit->body){{$model_unit->body}}@endif ]]></description>
		@if ($model_unit->hasBasicFloorplan())
		<?php
		$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $model_unit->basicFloorplanSrc());
		$modified = filemtime($path . $file);
		if($modified >= $valid) {
			?>
			<ModelLayout source="{{asset($model_unit->basicFloorplanSrc())}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
				<label>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</label>
			</ModelLayout>
		<?php } else { ?>
			<ModelLayout source="{{asset($model_unit->basicFloorplanSrc())}}">
				<label>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</label>
			</ModelLayout>
		<?php } ?>
		@endif
		@foreach ($model_unit->features as $feature)
		<ModelTag type="AMENITY_SELECT"><tag>{{$feature->name}}</tag></ModelTag>
		@endforeach
	</Model>
	@foreach ($available_units as $available_unit)
	@if($available_unit->building->slug == 'pelham-parkway-towers' || $available_unit->building->slug == 'pelham-terrace' || $available_unit->building->slug == 'pelham-park-view' || $available_unit->building->slug == 'pelham-place')
	@if($available_unit->floorplan_id == '2043139' || $available_unit->floorplan_id == '2043129' || $available_unit->floorplan_id == '2043099')
	<Model id="{{$model_unit->floorplan_id}}_{{$available_unit->getUniqueVersionIdAttribute()}}" parentModelId="{{$available_unit->floorplan_id}}" type="unit" searchable="true" pricingType="FLAT">
		<unitNumber>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</unitNumber>
		<lowPrice>{{$available_unit->net_effective_price}}</lowPrice>
		<highPrice>{{$available_unit->net_effective_price}}</highPrice>
		<numBedrooms>{{floor($available_unit->bedrooms)}}</numBedrooms>
		<numFullBaths>{{floor($available_unit->bathrooms)}}</numFullBaths>
		@if ($available_unit->square_footage_toggle == 'enabled')
		<squareFeet>{{$available_unit->square_footage}}</squareFeet>
		@endif
		<dateAvailable>{{ $available_unit->date_available }}</dateAvailable>
		<description><![CDATA[ @if (!!$available_unit->getConcessionMessage()) {{$available_unit->getConcessionMessage()}}<br><br>@endif @if (!!$available_unit->body){{$available_unit->body}}@endif ]]></description>
		@if ($available_unit->hasBasicFloorplan())
		<?php
		$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $available_unit->basicFloorplanSrc());
		$modified = filemtime($path . $file);
		if($modified >= $valid) {
			?>
			<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
				<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
			</ModelLayout>
		<?php } else { ?>
			<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}">
				<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
			</ModelLayout>
		<?php } ?>
		@endif
		@foreach ($available_unit->features as $feature)
		<ModelTag type="AMENITY_SELECT"><tag>{{$feature->name}}</tag></ModelTag>
		@endforeach
	</Model>
	@endif
	@endif
	@endforeach
	@endif
	@if($model_unit->floorplan_id == '2043140' && $model_unit->number == 'WAIT-P1H')
	<?php
	$high_price = 0;
	foreach ($available_units as $available_unit) {
		if($available_unit->building->slug == 'pelham-parkway-towers' || $available_unit->building->slug == 'pelham-terrace' || $available_unit->building->slug == 'pelham-park-view' || $available_unit->building->slug == 'pelham-place') {
			if($available_unit->floorplan_id == '2043140' || $available_unit->floorplan_id == '2043130' || $available_unit->floorplan_id == '2043153' || $available_unit->floorplan_id == '2043100') {
				if($available_unit->net_effective_price > $high_price) {
					$high_price = $available_unit->net_effective_price;
				}
			}
		}
	}
	$low_price = $high_price;
	foreach ($available_units as $available_unit) {
		if($available_unit->building->slug == 'pelham-parkway-towers' || $available_unit->building->slug == 'pelham-terrace' || $available_unit->building->slug == 'pelham-park-view' || $available_unit->building->slug == 'pelham-place') {
			if($available_unit->floorplan_id == '2043140' || $available_unit->floorplan_id == '2043130' || $available_unit->floorplan_id == '2043153' || $available_unit->floorplan_id == '2043100') {
				if($available_unit->net_effective_price < $low_price) {
					$low_price = $available_unit->net_effective_price;
				}
			}
		}
	}
	?>
	<Model id="{{$model_unit->floorplan_id}}" type="floorplan" searchable="true" pricingType="RANGE">
		<unitNumber>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</unitNumber>
		<unitFloorNumber>{{$model_unit->floor}}</unitFloorNumber>
		<lowPrice>{{$low_price}}</lowPrice>
		<highPrice>{{$high_price}}</highPrice>
		<name>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</name>
		<numBedrooms>{{floor($model_unit->bedrooms)}}</numBedrooms>
		<numFullBaths>{{floor($model_unit->bathrooms)}}</numFullBaths>
		<numHalfBaths>{{ceil(fmod($model_unit->bathrooms, 1))}}</numHalfBaths>
		@if ($model_unit->square_footage_toggle == 'enabled')
		<squareFeet>{{$model_unit->square_footage}}</squareFeet>
		@endif
		<description><![CDATA[ @if (!!$model_unit->getConcessionMessage()) {{$model_unit->getConcessionMessage()}}<br><br>@endif @if (!!$model_unit->body){{$model_unit->body}}@endif ]]></description>
		@if ($model_unit->hasBasicFloorplan())
		<?php
		$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $model_unit->basicFloorplanSrc());
		$modified = filemtime($path . $file);
		if($modified >= $valid) {
			?>
			<ModelLayout source="{{asset($model_unit->basicFloorplanSrc())}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
				<label>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</label>
			</ModelLayout>
		<?php } else { ?>
			<ModelLayout source="{{asset($model_unit->basicFloorplanSrc())}}">
				<label>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</label>
			</ModelLayout>
		<?php } ?>
		@endif
		@foreach ($model_unit->features as $feature)
		<ModelTag type="AMENITY_SELECT"><tag>{{$feature->name}}</tag></ModelTag>
		@endforeach
	</Model>
	@foreach ($available_units as $available_unit)
	@if($available_unit->building->slug == 'pelham-parkway-towers' || $available_unit->building->slug == 'pelham-terrace' || $available_unit->building->slug == 'pelham-park-view' || $available_unit->building->slug == 'pelham-place')
	@if($available_unit->floorplan_id == '2043140' || $available_unit->floorplan_id == '2043130' || $available_unit->floorplan_id == '2043153' || $available_unit->floorplan_id == '2043100')
	<Model id="{{$model_unit->floorplan_id}}_{{$available_unit->getUniqueVersionIdAttribute()}}" parentModelId="{{$available_unit->floorplan_id}}" type="unit" searchable="true" pricingType="FLAT">
		<unitNumber>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</unitNumber>
		<lowPrice>{{$available_unit->net_effective_price}}</lowPrice>
		<highPrice>{{$available_unit->net_effective_price}}</highPrice>
		<numBedrooms>{{floor($available_unit->bedrooms)}}</numBedrooms>
		<numFullBaths>{{floor($available_unit->bathrooms)}}</numFullBaths>
		@if ($available_unit->square_footage_toggle == 'enabled')
		<squareFeet>{{$available_unit->square_footage}}</squareFeet>
		@endif
		<dateAvailable>{{ $available_unit->date_available }}</dateAvailable>
		<description><![CDATA[ @if (!!$available_unit->getConcessionMessage()) {{$available_unit->getConcessionMessage()}}<br><br>@endif @if (!!$available_unit->body){{$available_unit->body}}@endif ]]></description>
		@if ($available_unit->hasBasicFloorplan())
		<?php
		$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $available_unit->basicFloorplanSrc());
		$modified = filemtime($path . $file);
		if($modified >= $valid) {
			?>
			<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
				<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
			</ModelLayout>
		<?php } else { ?>
			<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}">
				<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
			</ModelLayout>
		<?php } ?>
		@endif
		@foreach ($available_unit->features as $feature)
		<ModelTag type="AMENITY_SELECT"><tag>{{$feature->name}}</tag></ModelTag>
		@endforeach
	</Model>
	@endif
	@endif
	@endforeach
	@endif

	@if($model_unit->floorplan_id == '2043141' && $model_unit->number == 'WAITP22H')
	<?php
	$high_price = 0;
	foreach ($available_units as $available_unit) {
		if($available_unit->building->slug == 'pelham-parkway-towers' || $available_unit->building->slug == 'pelham-terrace' || $available_unit->building->slug == 'pelham-park-view' || $available_unit->building->slug == 'pelham-place') {
			if($available_unit->floorplan_id == '2043141' || $available_unit->floorplan_id == '2043132' || $available_unit->floorplan_id == '2043155' || $available_unit->floorplan_id == '2043102') {
				if($available_unit->net_effective_price > $high_price) {
					$high_price = $available_unit->net_effective_price;
				}
			}
		}
	}
	$low_price = $high_price;
	foreach ($available_units as $available_unit) {
		if($available_unit->building->slug == 'pelham-parkway-towers' || $available_unit->building->slug == 'pelham-terrace' || $available_unit->building->slug == 'pelham-park-view' || $available_unit->building->slug == 'pelham-place') {
			if($available_unit->floorplan_id == '2043141' || $available_unit->floorplan_id == '2043132' || $available_unit->floorplan_id == '2043155' || $available_unit->floorplan_id == '2043102') {
				if($available_unit->net_effective_price < $low_price) {
					$low_price = $available_unit->net_effective_price;
				}
			}
		}
	}
	?>
	<Model id="{{$model_unit->floorplan_id}}" type="floorplan" searchable="true" pricingType="RANGE">
		<unitNumber>{{$model_unit->number}}</unitNumber>
		<unitFloorNumber>{{$model_unit->floor}}</unitFloorNumber>
		<lowPrice>{{$low_price}}</lowPrice>
		<highPrice>{{$high_price}}</highPrice>
		<name>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</name>
		<numBedrooms>{{floor($model_unit->bedrooms)}}</numBedrooms>
		<numFullBaths>{{floor($model_unit->bathrooms)}}</numFullBaths>
		<numHalfBaths>{{ceil(fmod($model_unit->bathrooms, 1))}}</numHalfBaths>
		@if ($model_unit->square_footage_toggle == 'enabled')
		<squareFeet>{{$model_unit->square_footage}}</squareFeet>
		@endif
		<description><![CDATA[ @if (!!$model_unit->getConcessionMessage()) {{$model_unit->getConcessionMessage()}}<br><br>@endif @if (!!$model_unit->body){{$model_unit->body}}@endif ]]></description>
		@if ($model_unit->hasBasicFloorplan())
		<?php
		$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $model_unit->basicFloorplanSrc());
		$modified = filemtime($path . $file);
		if($modified >= $valid) {
			?>
			<ModelLayout source="{{asset($model_unit->basicFloorplanSrc())}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
				<label>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</label>
			</ModelLayout>
		<?php } else { ?>
			<ModelLayout source="{{asset($model_unit->basicFloorplanSrc())}}">
				<label>@if($model_unit->feed_number_override){{$model_unit->feed_number_override}}@else{{$model_unit->number}}@endif</label>
			</ModelLayout>
		<?php } ?>
		@endif
		@foreach ($model_unit->features as $feature)
		<ModelTag type="AMENITY_SELECT"><tag>{{$feature->name}}</tag></ModelTag>
		@endforeach
	</Model>
	@foreach ($available_units as $available_unit)
	@if($available_unit->building->slug == 'pelham-parkway-towers' || $available_unit->building->slug == 'pelham-terrace' || $available_unit->building->slug == 'pelham-park-view' || $available_unit->building->slug == 'pelham-place')
	@if($available_unit->floorplan_id == '2043141' || $available_unit->floorplan_id == '2043132' || $available_unit->floorplan_id == '2043155' || $available_unit->floorplan_id == '2043102')
	<Model id="{{$model_unit->floorplan_id}}_{{$available_unit->getUniqueVersionIdAttribute()}}" parentModelId="{{$available_unit->floorplan_id}}" type="unit" searchable="true" pricingType="FLAT">
		<unitNumber>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</unitNumber>
		<lowPrice>{{$available_unit->net_effective_price}}</lowPrice>
		<highPrice>{{$available_unit->net_effective_price}}</highPrice>
		<numBedrooms>{{floor($available_unit->bedrooms)}}</numBedrooms>
		<numFullBaths>{{floor($available_unit->bathrooms)}}</numFullBaths>
		@if ($available_unit->square_footage_toggle == 'enabled')
		<squareFeet>{{$available_unit->square_footage}}</squareFeet>
		@endif
		<dateAvailable>{{ $available_unit->date_available }}</dateAvailable>
		<description><![CDATA[ @if (!!$available_unit->getConcessionMessage()) {{$available_unit->getConcessionMessage()}}<br><br>@endif @if (!!$available_unit->body){{$available_unit->body}}@endif ]]></description>
		@if ($available_unit->hasBasicFloorplan())
		<?php
		$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $available_unit->basicFloorplanSrc());
		$modified = filemtime($path . $file);
		if($modified >= $valid) {
			?>
			<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
				<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
			</ModelLayout>
		<?php } else { ?>
			<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}">
				<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
			</ModelLayout>
		<?php } ?>
		@endif
		@foreach ($available_unit->features as $feature)
		<ModelTag type="AMENITY_SELECT"><tag>{{$feature->name}}</tag></ModelTag>
		@endforeach
	</Model>
	@endif
	@endif
	@endforeach
	@endif
	@endif
	@endif
	@endif
	@endforeach
	@if($ppn == 0)
	<providerType>Property Manager</providerType>
</Listing>
@endif
@endif

@if($building->slug == '151-east-80th-street' || $building->slug == '1160-fifth-avenue' || $building->slug == 'park-towers-south' || $building->slug == 'the-river-cliff' || $building->slug == 'gracie-court' || $building->slug == 'cedar-arms' || $building->slug == 'the-capri' || $building->slug == 'river-hill-gardens' || $building->slug == 'the-ross' || $building->slug == 'sheridan-plaza' || $building->slug == 'fordham-terrace')
@if($building->has_vacancy)
<Listing id="{{$building->slug}}" type="RENTAL" companyld="goldfarb" propertyType="LARGE">
	<name>{{$building->name}}</name>
	<street hide="false">{{$building->address->street_1}}{{(!!$building->address->street_2) ? ',' . $building->address->street_2 : null}}</street>
	<city>{{$building->address->city}}</city>
	<state>{{$building->address->state}}</state>
	<zip>{{$building->address->zip}}</zip>
	<country>United States of America</country>
	<latitude>{{$building->address->getLatLngAttribute()['lat']}}</latitude>
	<longitude>{{$building->address->getLatLngAttribute()['lng']}}</longitude>
	<lastUpdated>{{$building->updated_at}}</lastUpdated>
	<contactName>Goldfarb Properties</contactName>
	<contactEmail>{{$building->neighborhood->region->email}}</contactEmail>
	<contactPhone>{{$building->neighborhood->region->phone}}</contactPhone>
	<contactMethodPreference>Email</contactMethodPreference>
	<previewMessage><![CDATA[{{$building->neighborhood->body}}]]></previewMessage>
	<terms>Minimum one month security</terms>
	<leaseTerm>12 or 24 months</leaseTerm>
	<website>{{route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug, 'buildingSlug' => $building->slug])}}</website>
	@foreach ($building->amenities as $amenity)
	<ListingTag type="PROPERTY_AMENITY"><tag>{{$amenity->name}}</tag></ListingTag>
	@endforeach
	@foreach ($building->images as $image)
	<?php
	$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
	$modified = filemtime($path . $file);
	if($modified >= $valid) { ?>
		<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
		<label>{{$image->title}}</label>
		</ListingPhoto>
		<?php
	} else {
		?>
		<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
		<label>{{$image->title}}</label>
		</ListingPhoto>
		<?php
	}
	?>
	@endforeach
	@foreach ($available_units as $available_unit)
	@if($available_unit->building->slug == $building->slug)
	@foreach ($available_unit->images as $image)
	<?php
	$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $image->styleSrc("carousel-slide"));
	$modified = filemtime($path . $file);
	if($modified >= $valid) { ?>
		<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
		<label>{{$image->title}}</label>
		</ListingPhoto>
		<?php
	} else {
		?>
		<ListingPhoto source="{{asset($image->styleSrc("carousel-slide"))}}">
		<label>{{$image->title}}</label>
		</ListingPhoto>
		<?php
	}
	?>
	@endforeach
	@endif
	@endforeach
	@foreach ($available_units as $available_unit)
	@if($available_unit->building->slug == $building->slug)
	<Model id="{{$available_unit->floorplan_id}}" type="unit" searchable="true" pricingType="FLAT">
		<unitNumber>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</unitNumber>
		<lowPrice>{{$available_unit->net_effective_price}}</lowPrice>
		<highPrice>{{$available_unit->net_effective_price}}</highPrice>
		<numBedrooms>{{floor($available_unit->bedrooms)}}</numBedrooms>
		<numFullBaths>{{floor($available_unit->bathrooms)}}</numFullBaths>
		@if ($available_unit->square_footage_toggle == 'enabled')
		<squareFeet>{{$available_unit->square_footage}}</squareFeet>
		@endif
		<dateAvailable>{{ $available_unit->date_available }}</dateAvailable>
		<description><![CDATA[ @if (!!$available_unit->getConcessionMessage()) {{$available_unit->getConcessionMessage()}}<br><br>@endif @if (!!$available_unit->body){{$available_unit->body}}@endif ]]></description>
		@if ($available_unit->hasBasicFloorplan())
		<?php
		$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '' , $available_unit->basicFloorplanSrc());
		$modified = filemtime($path . $file);
		if($modified >= $valid) {
			?>
			<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}?<?php echo 'week=' . date ('W', $now) . date('N', $now);?>">
				<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
			</ModelLayout>
		<?php } else { ?>
			<ModelLayout source="{{asset($available_unit->basicFloorplanSrc())}}">
				<label>@if($available_unit->feed_number_override){{$available_unit->feed_number_override}}@else{{$available_unit->number}}@endif</label>
			</ModelLayout>
		<?php } ?>
		@endif
		@foreach ($available_unit->features as $feature)
		<ModelTag type="AMENITY_SELECT"><tag>{{$feature->name}}</tag></ModelTag>
		@endforeach
	</Model>
	@endif
	@endforeach
	<providerType>Property Manager</providerType>
</Listing>
@endif
@endif
@endforeach
</hotPadsItems>

 */
		//endregion
	}

	public function hotpadsrevisedcsvAction()
	{
/*
 $buildings = Building::residential()->whereNotNull('yardi_code')->get();
        foreach ($buildings as $building) {
            $properties = explode(',', $building->yardi_code);

            foreach ($properties as $propertyCode) {
                $data = Yardi::getAvailability($propertyCode);
                $building->yardi_data = $data;
            }
        }

$units = $available_units;
$aunits = [];
foreach($units as $unit) {
  $aunit = [];
  $aunit['id'] =  $unit->id;
  $aunit['yardi_code'] =  $unit->yardi_code;
  $aunit['building_id'] =  $unit->building_id;
  $aunit['building_name'] =  $unit->building->name;
  $aunit['floorplan_id'] =  $unit->floorplan_id;
  $aunit['type'] =  $unit->type;
  $aunit['number'] =  $unit->number;
  $aunit['feed_number_override'] =  $unit->feed_number_override;
  $aunit['show_number'] =  $unit->show_number;
  $aunit['slug'] =  $unit->slug;
  $aunit['floor'] =  $unit->floor;
  $aunit['square_footage'] =  $unit->square_footage;
  $aunit['square_footage_toggle'] =  $unit->square_footage_toggle;
  $aunit['rooms'] =  $unit->rooms;
  $aunit['bedrooms'] =  $unit->bedrooms;
  $aunit['bathrooms'] =  $unit->bathrooms;
  $aunit['price'] =  $unit->price;
  $aunit['lease_term'] =  $unit->lease_term;
  $aunit['concession_months'] =  $unit->concession_months;
  $aunit['active_streeteasy'] =  $unit->active_streeteasy;
  $aunit['concession_expiration'] =  $unit->concession_expiration;
  $aunit['concession_language'] =  $unit->concession_language;
  $aunit['disable_openhouse'] =  $unit->disable_openhouse;
  $aunit['status'] =  $unit->status;
  $aunit['featured'] =  $unit->featured;
  $aunit['version'] =  $unit->version;
  $aunit['apply_link'] =  $unit->apply_link;
  array_push($aunits, $aunit);
}

//ini_set('display_errors','1');
//error_reporting(E_ALL);

$fh = fopen( 'php://output', 'w' );

$members = "";
$headerNames = array(
  "id",
  "yardi_code",
  "building_id",
  "building_name",
  "floorplan_id",
  "type",
  "number",
  "feed_number_override",
  "show_number",
  "slug",
  "floor",
  "square_footage",
  "square_footage_toggle",
  "rooms",
  "bedrooms",
  "bathrooms",
  "price",
  "lease_term",
  "concession_months",
  "active_streeteasy",
  "concession_expiration",
  "concession_language",
  "disable_openhouse",
  "status",
  "featured",
  "version",
  "apply_link"
);

foreach ($headerNames as $k=>$v) {
  if ($v == 'id') {
    //need to keep id not all caps otherwise Excel thinks the CSV is a SYLK file.
    $headers[] = str_replace("_"," ",ucfirst($v));
  }else {
    $headers[] = str_replace("_"," ",strtoupper($v));
  }
}
fputcsv($fh, $headers);

foreach($aunits as $key => $val) {
  $memberRow = '';
  foreach ($headerNames as $k=>$v) {
    $memberRow[] .= $val[$v];
  }
  fputcsv($fh, $memberRow);
}

date_default_timezone_set("America/New_York");
$stamp = date("Ymd_hia");
$filename = "hotpads_zillow_trulia_" . $stamp . ".csv";

header("Content-type: text/plain");
header("Content-Disposition: attachment; filename=$filename");

fclose($fh);

 */
	}

	/**
	 * converted: Completed [XML]
	 */
	public function facebookAction()
	{
		$units = $this->getUnits();

		$xml = new DomDocument('1.0', 'UTF-8');
		$main_element = $xml->createElement('rss');
		$main_element->setAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
		$main_element->setAttribute('version', '2.0');

		$properties = $xml->createElement('channel');
		$title = $xml->createElement('title');
		$title->nodeValue = "Goldfarb Properties";
		$properties->appendChild($title);

		$link = $xml->createElement('link');
		$link->nodeValue = "http://goldfarbproperties.com";
		$properties->appendChild($link);

		$description = $xml->createElement('description');
		$description->appendChild($description->ownerDocument->createCDATASection("For over 60 years, Goldfarb has been a family-run and managed business dedicated to providing the finest, no-fee luxury apartments across five diverse regions. Originating in 1953, our company grew from just two buildings to over 6,000 luxury apartments. Our apartments, located in the area’s most desirable neighborhoods, are meticulously-designed to fit the needs and desires of our tenants. We strive to maintain strong relationships and provide the highest quality service and care to each and every of our valued tenants. Here is our list of apartment availability."));
		$properties->appendChild($description);

		foreach ($units as $idx => $unit) {
			$number = preg_replace(
				"/[^A-Za-z0-9 ]/",
				'',
				($unit['feed_number_override']) ? $unit['feed_number_override'] : $unit->number
			);
			$unique_id = strtolower(sprintf('%sv%s', $number, $unit['version']));

			$item = $xml->createElement('item');

			//region Building subitems

			//region g:id
			$subitem = $xml->createElement('g:id');
			$subitem->nodeValue = $unique_id;
			$item->appendChild($subitem);
			//endregion

			//region g:title
			$subitem = $xml->createElement('g:title');
			$subitem->nodeValue = $unit->building.' Apartment '.$unit->number;
			$item->appendChild($subitem);
			//endregion

			//region g:description
			$subitem = $xml->createElement('g:description');
			$description = '';
			//fixme: strange code. It checks if(!!$unit->body) and renders concession but never renders the "else"
			$unit_concession_message = $this->_unit_mapper->getUnitConcessionMessage($unit->id);
			if (empty($unit->description) && !empty($unit_concession_message)) {
				$description .= ' Apartment Description: <br> '.$unit_concession_message;
			}
			if (!empty($unit->building_description)) {
				$description .= ' Building Description: <br> '.$unit->building_description;
			}
			if (!empty($description)) {
				$subitem->appendChild($subitem->ownerDocument->createCDATASection($description));
			} else {
				$subitem->nodeValue = '';
			}
			$item->appendChild($subitem);
			//endregion

			//region g:link
			$subitem = $xml->createElement('g:link');
			$subitem->nodeValue = $this->_domain.'/rentals/'.$unit->region_code.'/'.$unit->building_code.'/apartment-'.$unit->code;
			$item->appendChild($subitem);
			//endregion

			//region g:image_link
			$subitem = $xml->createElement('g:image_link');
			$unit_images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit->id);
			$subitem->nodeValue = $this->_domain.$unit_images[0]->image;
			$item->appendChild($subitem);
			//endregion

			//region g:brand
			$subitem = $xml->createElement('g:brand');
			$subitem->nodeValue = 'Goldfarb Properties';
			$item->appendChild($subitem);
			//endregion

			//region g:condition
			$subitem = $xml->createElement('g:condition');
			$subitem->nodeValue = 'new';
			$item->appendChild($subitem);
			//endregion

			//region g:availability
			$subitem = $xml->createElement('g:availability');
			$subitem->nodeValue = 'in stock';
			$item->appendChild($subitem);
			//endregion

			//region g:price
			$subitem = $xml->createElement('g:price');
			$subitem->nodeValue = $this->_unit_mapper->getUnitNetEffectivePrice($unit->id);
			$item->appendChild($subitem);
			//endregion

			//region g:google_product_category
			$subitem = $xml->createElement('g:google_product_category');
			$subitem->appendChild($subitem->ownerDocument->createCDATASection('Home & Garden'));
			$item->appendChild($subitem);
			//endregion

			//endregion
			$item->appendChild($subitem);
			$properties->appendChild($item);
			unset($item);
			if ($idx % 10000 == 0) {
				gc_collect_cycles();
			}
		}

		$main_element->appendChild($properties);
		$xml->appendChild($main_element);
		$xml->formatOutput = true;

		header('Content-type: text/xml');
		echo $xml->saveXML();
		exit;

		/*
		<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>
		<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
				<channel>
				<title>Goldfarb Properties</title>
				<link>http://goldfarbproperties.com</link>
				<description>For over 60 years, Goldfarb has been a family-run and managed business dedicated to providing the finest, no-fee luxury apartments across five diverse regions. Originating in 1953, our company grew from just two buildings to over 6,000 luxury apartments. Our apartments, located in the area’s most desirable neighborhoods, are meticulously-designed to fit the needs and desires of our tenants. We strive to maintain strong relationships and provide the highest quality service and care to each and every of our valued tenants. Here is our list of apartment availability.</description>
				@foreach ($units as $unit)
				<item>
					<g:id>{{$unit->unique_version_id}}</g:id>
					<g:title>{{$unit->building->name}} Apartment {{$unit->number}}</g:title>
					<g:description><![CDATA[@if (!!$unit->body) Apartment Description:<br>{{$unit->body_with_concession}}<br>@endif @if (!!$unit->building->body) Building Description: <br> {{$unit->building->body}} @endif ]]></g:description>
					<g:link>{{url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug]))}}</g:link>
					<g:image_link>{!!url($unit->getGalleryImages()->first()->styleSrc("carousel-slide"))!!}</g:image_link>
					<g:brand>Goldfarb Properties</g:brand>
					<g:condition>new</g:condition>
					<g:availability>in stock</g:availability>
					<g:price>{{$unit->net_effective_price}} USD</g:price>
					<g:google_product_category><![CDATA[Home &amp; Garden]]></g:google_product_category>

				</item>
				@endforeach
				</channel>
				</rss>

				 */
	}

	public function truliaAction()
	{
/*

<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>

<properties>
    @foreach ($units as $unit)
        @if ($unit->disable_syndication != 'yes')
            <property>
                <listing-type>rental</listing-type>
                <status>for rent</status>

                <location>
                    <unit-number>{{$unit->number}}</unit-number>
                    <street-address>{{$unit->building->address->street_1}}{{(!!$unit->building->address->street_2) ? ',' . $unit->building->address->street_2 : null}}</street-address>
                    <display-address>yes</display-address>
                    <city-name>{{$unit->building->address->city}}</city-name>
                    <zipcode>{{$unit->building->address->zip}}</zipcode>
                    <state-code>{{$unit->building->address->state}}</state-code>
                    <street-intersection></street-intersection> {{-- do we have cross streets? --}}
                    <building-name>{{$unit->building->name}}</building-name>
                    <neighborhood-name>{{$unit->building->neighborhood->name}}</neighborhood-name>
                    <neighborhood-description><![CDATA[ {{$unit->building->neighborhood->body}}]]>
                    </neighborhood-description>
                    <longitude>{{$unit->building->address->LatLng['lng']}}</longitude>
                    <latitude>{{$unit->building->address->LatLng['lat']}}</latitude>
                </location>
                <details>
                    <price>{{$unit->net_effective_price}}</price>
                    <num-bedrooms>{{floor($unit->bedrooms)}}</num-bedrooms>
                    <num-full-bathrooms>{{floor($unit->bathrooms)}}</num-full-bathrooms>
                    <num-half-bathrooms>{{ceil(fmod($unit->bathrooms, 1))}}</num-half-bathrooms>
                    <num-bathrooms>{{ceil($unit->bathrooms)}}</num-bathrooms>
                    @if ($unit->square_footage_toggle == 'enabled')
                        <living-area-square-feet>{{$unit->square_footage}}</living-area-square-feet>
                    @endif
                    <property-type>apartment/condo/townhouse</property-type>
                    <description><![CDATA[@if (!!$unit->body) Apartment Description:<br>{{$unit->body}}
                        <br>@endif @if (!!$unit->building->body) Building Description:
                        <br> {{$unit->building->body}} @endif ]]>
                    </description>
                    <date-available>{{$unit->updated_at->toDateString()}}</date-available>
                </details>
                <landing-page>
                    <lp-url>{{url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug]))}}</lp-url>
                </landing-page>
                <site>
                    <site-url>{{url(route('portfolio.home'))}}</site-url>
                    <site-name>Elevated Living | Goldfarb Properties</site-name>
                </site>
                <pictures>
                    @foreach ($unit->getGalleryImages() as $index => $image)
                        <picture><!-- repeatable tag group-->
                            <picture-url>{!!url($image->styleSrc("carousel-slide"))!!}</picture-url>
                            <picture-caption>{{$image->title}}</picture-caption>
                            <picture-seq-number>{{$index}}</picture-seq-number>
                        </picture>
                    @endforeach
                </pictures>
                @if (count($unit->building->openHouses))
                    @if ($unit->disable_openhouse != 'yes')
                        <open-homes>
                            @foreach ($unit->building->openHouses as $openHouse)
                                <open-home><!-- repeatable tag group-->
                                    <start-time>{{$openHouse->start_time->format('H:i')}}</start-time>
                                    <end-time>{{$openHouse->end_time->format('H:i')}}</end-time>
                                    <date>$openHouse->start_time->toDateString()</date>
                                </open-home>
                            @endforeach
                        </open-homes>
                    @endif
                @endif
                {{-- <schools>
                   <school-district>
                     <elementary></elementary>
                     <middle></middle>
                     <juniorhigh></juniorhigh>
                     <high></high>
                     <district-name></district-name>
                     <district-website></district-website>
                     <district-phone-number></district-phone-number>
                   </school-district>
                </schools> --}}
                @if (count($unit->floorplan))
                    <floorplan-layouts>
                        <floorplan-layout><!-- repeatable tag group -->
                            <floorplan-layout-url>{!!url($unit->floorplanSrc())!!}</floorplan-layout-url>
                            <floorplan-layout-caption>{{$unit->floorplan->title}}</floorplan-layout-caption>
                        </floorplan-layout>
                    </floorplan-layouts>
                @endif
                {{--
                <agent>
                  <agent-name>John</agent-name>
                  <agent-email>johnsmith@realtorremax.com</agent-email>
                  <agent-phone>415-987-5555</agent-phone>
                  <agent-alternate-email></agent-alternate-email>
                  <agent-picture-url>http://www.realtorjohn.com/pic12345</agent-picture-url>
                  <agent-id>9876542</agent-id>
                  <agent-licenses>
                     <agent-license>
                       <agent-license-number>9648346</agent-license-number>
                       <agent-license-category>agent</agent-license-category>
                       <agent-license-state>CA</agent-license-state>
                     </agent-license>
                  </agent-licenses>
                </agent>

                 <property-manager> (for rental listings only)
                   <property-manager-name></property-manager-name>
                   <property-management-company-name></property-management-company-name>
                   <property-manager-phone></property-manager-phone>
                   <property-manager-email></property-manager-email>
                   <property-manager-lead-email></property-manager-lead-email>
                   <property-manager-website></property-manager-website>
                   <property-manager-logo-url></property-manager-logo-url>
                   <property-manager-office-hours>
                           <office-day><!-- repeatable tag -->
                             <day-of-the-week>(sun | mon | tue | wed | thu | fri | sat)</day-of-the-week>
                             <office-start-time></office-start-time>
                             <office-end-time></office-end-time>
                             <comment></comment>
                           </office-day>
                        </property-manager-office-hours>
                     </property-manager>
                <detailed-characteristics>
                        <appliances>
                           <has-washer>(yes|no)</has-washer>
                           <has-dryer>(yes|no)</has-dryer>
                           <has-dishwasher>(yes|no)</has-dishwasher>
                           <has-refrigerator>(yes|no)</has-refrigerator>
                           <has-disposal>(yes|no)</has-disposal>
                           <has-microwave>(yes|no)</has-microwave>
                           <range-type>(gas | electric | other)</range-type>
                           <appliances-comments></appliances-comments>
                           <additional-appliances>
                             <additional-appliance><!-- repeatable tag group. List each individually-->
                                <additional-appliance-name></additional-appliance-name>
                                <additional-appliance-description></additional-appliance-description>
                             </additional-appliance>
                           </additional-appliances>
                        </appliances>
                        <cooling-systems>
                           <has-air-conditioning>(yes|no)</has-air-conditioning>
                           <has-ceiling-fan>(yes|no)</has-ceiling-fan>
                           <other-cooling></other-cooling>
                        </cooling-systems>
                        <heating-systems>
                           <has-fireplace>(yes|no)</has-fireplace>
                           <fireplace-type>(gas | wood | electric | decorative)</fireplace-type>
                           <heating-system>(gas | electric | radiant | other)</heating-system>
                           <heating-fuel>(coal | oil | gas | electric | propane | butane | solar | woodpelet| other|
                none)</heating-fuel>
                        </heating-systems>
                        <floor-coverings></floor-coverings>
                        <total-unit-parking-spaces></total-unit-parking-spaces>
                        <has-garage>(yes|no)</has-garage>
                        <garage-type>(attached | detached)</garage-type>
                        <parking-types>
                           <parking-type>(surface lot | garage lot | covered lot | street | carport | none |
                other)</parking-type><!-- repeatable tag -->
                        </parking-types>
                        <has-assigned-parking-space>(yes|no)</has-assigned-parking-space>
                        <parking-space-fee>(free | paid | both)</parking-space-fee>
                        <assigned-parking-space-cost></assigned-parking-space-cost>
                        <parking-comment></parking-comment>
                        <foundation-type></foundation-type>
                        <roof-type></roof-type>
                        <architecture-style></architecture-style>
                        <exterior-type></exterior-type>
                        <room-count></room-count>
                        <rooms>
                           <room><!-- repeatable tag -->
                             <room-type></room-type>
                             <room-size></room-size>
                             <room-description></room-description>
                           </room>
                        </rooms>
                        <year-updated></year-updated>
                        <total-units-in-building></total-units-in-building>
                        <total-floors-in-building></total-floors-in-building>
                        <num-floors-in-unit></num-floors-in-unit>
                        <has-attic>(yes|no)</has-attic>
                        <has-balcony>(yes|no)</has-balcony>
                        <has-barbeque-area>(yes|no)</has-barbeque-area>
                        <has-basement>(yes|no)</has-basement>
                        <has-cable-satellite>(yes|no)</has-cable-satellite>
                        <has-courtyard>(yes|no)</has-courtyard>
                        <has-deck>(yes|no)</has-deck>
                        <has-disabled-access>(yes|no)</has-disabled-access>
                        <has-dock>(yes|no)</has-dock>
                        <has-doublepane-windows>(yes|no)</has-doublepane-windows>
                        <has-garden>(yes|no)</has-garden>
                        <has-gated-entry>(yes|no)</has-gated-entry>
                        <has-greenhouse>(yes|no)</has-greenhouse>
                        <has-handrails>(yes|no)</has-handrails>
                        <has-hot-tub-spa>(yes|no)</has-hot-tub-spa>
                        <has-intercom>(yes|no)</has-intercom>
                        <has-jetted-bath-tub>(yes|no)</has-jetted-bath-tub>
                        <has-lawn>(yes|no)</has-lawn>
                        <has-mother-in-law>(yes|no)</has-mother-in-law>
                        <has-patio>(yes|no)</has-patio>
                        <has-pond>(yes|no)</has-pond>
                        <has-pool>(yes|no)</has-pool>
                        <has-porch>(yes|no)</has-porch>
                        <has-private-balcony>(yes|no)</has-private-balcony>
                        <has-private-patio>(yes|no)</has-private-patio>
                        <has-rv-parking>(yes|no)</has-rv-parking>
                        <has-sauna>(yes|no)</has-sauna>
                        <has-security-system>(yes|no)</has-security-system>
                        <has-skylight>(yes|no)</has-skylight>
                        <has-sportscourt>(yes|no)</has-sportscourt>
                        <has-sprinkler-system>(yes|no)</has-sprinkler-system>
                        <has-terrace>(yes|no)</has-terrace>
                        <has-vaulted-ceiling>(yes|no)</has-vaulted-ceiling>
                        <has-view>(yes|no)</has-view>
                        <has-washer-dryer-hookup>(yes|no)</has-washer-dryer-hookup>
                        <has-wet-bar>(yes|no)</has-wet-bar>
                        <has-window-coverings>(yes|no)</has-window-coverings>
                        <building-has-concierge>(yes|no)</building-has-concierge>
                        <building-has-doorman>(yes|no)</building-has-doorman>
                        <building-has-elevator>(yes|no)</building-has-elevator>
                        <building-has-fitness-center>(yes|no)</building-has-fitness-center>
                        <building-has-on-site-maintenance>(yes|no)</building-has-on-site-maintenance>
                   <is-waterfront>(yes|no)</is-waterfront>
                   <is-new-construction>(yes|no)</is-new-construction>
                   <furnished>(yes|no)</furnished>
                   <view-type></view-type>
                    <other-amenities>
                     <other-amenity></other-amenity><!-- repeatable tag -->
                    </other-amenities>
                </detailed-characteristics>
                <rental-terms> (for rental listings only)
                   <price-term>(night | week | month | year)</price-term>
                   <rental-type>(standard | corporate | senior | military | campus | market rate apt | condo-
                   minium | cooperative | assisted living | subsidized | nursing home | student | vacation |
                   other)</rental-type>
                   <lease-type>(sublet | month-to-month | annual | bi-annual)</lease-type>
                   <lease-min-length-months></lease-min-length-months>
                   <lease-max-length-months></lease-max-length-months>
                   <lease-periods>
                     <lease-period></lease-period>
                   </lease-periods>
                   <lease-details></lease-details>
                   <security-deposit></security-deposit>
                   <security-deposit-description></security-deposit-description>
                   <application-fee></application-fee>
                   <application-fee-description></application-fee-description>
                   <credit-cards-accepted>(yes|no)</credit-cards-accepted>
                   <credit-cards>
                     <credit-card>(visa | mastercard | discover| american express | other)</credit-card><!--
                     repeatable tag -->
                   </credit-cards>
                   <pets>
                     <small-dogs-allowed>(yes|no)</small-dogs-allowed>
                     <large-dogs-allowed>(yes|no)</large-dogs-allowed>
                     <cats-allowed>(yes|no)</cats-allowed>
                     <pet-other-allowed>(yes|no)</pet-other-allowed>
                     <max-pets></max-pets>
                     <pet-deposit></pet-deposit>
                     <pet-fee></pet-fee>
                     <pet-rent></pet-rent>
                     <pet-weight></pet-weight>
                     <pet-comments>
                        <pet-comment></pet-comment><!-- repeatable tag -->
                     </pet-comments>
                   </pets>
                   <utilities-included>
                     <landlord-pays-aircon>(yes|no)</landlord-pays-aircon>
                     <landlord-pays-broadbandinternet>(yes|no)</landlord-pays-broadbandinternet>
                     <landlord-pays-cable>(yes|no)</landlord-pays-cable>
                     <landlord-pays-electric>(yes|no)</landlord-pays-electric>
                     <landlord-pays-gas>(yes|no)</landlord-pays-gas>
                     <landlord-pays-heat>(yes|no)</landlord-pays-heat>
                     <landlord-pays-hotwater>(yes|no)</landlord-pays-hotwater>
                     <landlord-pays-satellite>(yes|no)</landlord-pays-satellite>
                     <landlord-pays-sewer>(yes|no)</landlord-pays-sewer>
                     <landlord-pays-telephone>(yes|no)</landlord-pays-telephone>
                     <landlord-pays-trash>(yes|no)</landlord-pays-trash>
                     <landlord-pays-water>(yes|no)</landlord-pays-water>
                     <landlord-utilities-portion-included></landlord-utilities-portion-included>
                     <utilities-comments>
                        <utilities-comment></utilities-comment><!-- repeatable tag -->
                     </utilities-comments>
                   </utilities-included>
                    <property-manager-on-site>(yes|no)</property-manager-on-site>
                   <rent-control>(yes|no)</rent-control>
                   <subletting-allowed>(yes|no)</subletting-allowed>
                   <rental-broker-fee>(yes|no)</rental-broker-fee>
                   <rental-broker-fee-amount></rental-broker-fee-amount>
                </rental-terms>
                     <advertise-with-us>
                        <channel></channel>
                        <featured>(yes|no)</featured>
                        <branded>(yes|no)</branded>
                        <branded-logo-url></branded-logo-url>
                     </advertise-with-us> --}}
            </property>
        @endif
</properties>

 */
	}

	public function rentpathAction()
	{
/*
$buildings = Building::rentals()->with('neighborhood', 'address', 'images', 'units', 'openHouses')->get();

<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>
<PhysicalProperty>
    <Management IDValue="1" OrganizationName="Goldfarb Properties" IDType="managmentID"/>
    @foreach ($buildings as $building)
        @if(count($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get()) > 0)
        <Property IDValue="{{$building->id}}" IDRank="primary">
            <PropertyID>
                <Identification IDValue="{{$building->id}}" IDRank="primary"/>
                <MarketingName>{{$building->name}}</MarketingName>
                <WebSite>{{url(route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug, 'buildingSlug' => $building->slug]))}}</WebSite>
                <Address AddressType="property">
                    <AddressLine1>{{$building->address->street_1}}</AddressLine1>
                    <AddressLine2>{{$building->address->street_2}}</AddressLine2>
                    <City>{{$building->address->city}}</City>
                    <State>{{$building->address->state}}</State>
                    <PostalCode>{{$building->address->zip}}</PostalCode>
                </Address>
            </PropertyID>
            <ILS_Identification ILS_IdentificationType="Apartment" RentalType="Unspecified">
            </ILS_Identification>
            <Information>
                <StructureType>Mixed Use</StructureType>
                <UnitCount> 10 </UnitCount>
                <ShortDescription><![CDATA[ {{ str_limit($building->body, $limit = 450, $end = '...') }} ]]></ShortDescription>
                <LongDescription><![CDATA[ {{ str_limit($building->body, $limit = 950, $end = '...') }} ]]></LongDescription>
            </Information>
            @foreach ($building->amenities as $amenity)
                <?php
                    $type = 'Other';
                    if(isset($amenity->feed_meta['apartments-com']) && $amenity->feed_meta['apartments-com'] != ''){
                       $type = $amenity->feed_meta['apartments-com'];
                    }
                ?>
                @if($type != 'AirConditioner')
                <Amenity AmenityType="{{$type}}">
                        <Description>{{$amenity->name}}</Description>
                </Amenity>
                @endif
            @endforeach
            @foreach ($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get() as $unit)
                <Floorplan IDValue="{{$unit->id}}" IDType="FloorplanID" IDRank="primary">
                    <FloorplanType>Internal</FloorplanType>
                    <Name>Apartment {{$unit->getStreeteasyNumber()}}</Name>
                    <Comment><![CDATA[ {!!$unit->body!!}]]></Comment>
                    <UnitCount>1</UnitCount>
                    <UnitsAvailable>1</UnitsAvailable>
                    <Room RoomType="Bedroom">
                        <Count>{{floor($unit->bedrooms)}}</Count>
                        <Comment>Bedroom(s)</Comment>
                    </Room>
                    <Room RoomType="Bathroom">
                        <Count>{{$unit->bathrooms}}</Count>
                        <Comment>Bathroom(s)</Comment>
                    </Room>
                    <SquareFeet Min="{{$unit->square_footage}}" Max="{{$unit->square_footage}}" />

                    <?php
                        $net = (integer) $unit->net_effective_price;
                        $price = (integer) $unit->price;
                    ?>
                    <MarketRent Min="{{ $price }}" Max="{{ $price }}" />
                    <EffectiveRent Min="{{ $net }}" Max="{{ $net }}" />
                    @foreach ($unit->features as $feature)
                        <?php
                            $type = 'Other';
                            if(isset($feature->feed_meta['apartments-com']) && $feature->feed_meta['apartments-com'] != ''){
                               $type = $feature->feed_meta['apartments-com'];
                            }
                        ?>
                        @if($type != 'AirConditioner')
                        <Amenity AmenityType="{{$type}}">
                            <Description>{{$feature->name}}</Description>
                        </Amenity>
                        @endif
                    @endforeach
                    @foreach ($unit->getGalleryImages() as $index => $image)
                        <File FileID="{{$image->id}}" Active="true">
                            <FileType>Photo</FileType>
                            <Name>{{$image->title}}</Name>
                            <Caption>{{$image->title}}</Caption>
                            <Format>Image</Format>
                            <Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
                            <Rank>{{$index}}</Rank>
                        </File>
                    @endforeach
                    @if (count($unit->floorplan))
                        <File FileID="{{$unit->floorplan->id}}" Active="false">
                            <FileType>Floorplan</FileType>
                            <Name>{{$image->title}}</Name>
                            <Caption>{{$image->title}}</Caption>
                            <Format>Image</Format>
                            <Src>{!!url($unit->basicFloorplanSrc())!!}</Src>
                            <Rank>0</Rank>
                        </File>
                    @endif
                </Floorplan>
            @endforeach
            @foreach ($building->units()->available()->rentals()->with('features', 'floorplan', 'images')->get() as $unit)
                <ILS_Unit IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID" IDRank="primary">
                    <Units>
                        <Unit>
                            <Identification IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID"
                                            IDRank="primary"/>
                            <Identification IDValue="{{$unit->id}}" IDType="FloorplanID" IDRank="primary"/>
                            <MarketingName>{{$unit->getStreeteasyNumber()}}</MarketingName>
                            <UnitBedrooms>{{$unit->bedrooms}}</UnitBedrooms>
                            <UnitBathrooms>{{$unit->bathrooms}}</UnitBathrooms>
                            <UnitRent>{{$unit->net_effective_price}}</UnitRent>
                            <UnitLeasedStatus>available</UnitLeasedStatus>
                        </Unit>
                    </Units>
                    <Comment><![CDATA[ {!!$unit->body!!}]]></Comment>
                    <EffectiveRent Min="{{$unit->net_effective_price}}" Max="{{$unit->net_effective_price}}"/>
                    @foreach ($unit->features as $feature)
                        <?php
                            $type = 'Other';
                            if(isset($feature->feed_meta['apartments-com']) && $feature->feed_meta['apartments-com'] != ''){
                               $type = $feature->feed_meta['apartments-com'];
                            }
                        ?>
                        @if($type != 'AirConditioner')
                        <Amenity AmenityType="{{$type}}">
                            <Description>{{$feature->name}}</Description>
                        </Amenity>
                        @endif
                    @endforeach
                    @foreach ($unit->getGalleryImages() as $index => $image)
                        <File FileID="{{$image->id}}" Active="true">
                            <FileType>Photo</FileType>
                            <Name>{{$image->title}}</Name>
                            <Caption>{{$image->title}}</Caption>
                            <Format>Image</Format>
                            <Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
                            <Rank>{{$index}}</Rank>
                        </File>
                    @endforeach
                </ILS_Unit>
            @endforeach
            @foreach ($building->getGalleryImages() as $index => $image)
                <File FileID="{{$image->id}}" Active="true">
                    <FileType>Photo</FileType>
                    <Name>{{$image->title}}</Name>
                    <Caption>{{$image->title}}</Caption>
                    <Format>Image</Format>
                    <Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
                    <Rank>{{$index}}</Rank>
                </File>
            @endforeach
        </Property>
        @endif
    @endforeach
</PhysicalProperty>

 */
	}

	/**
	 * converted: Completed [XML]
	 */
	public function rentpathunitlevelAction()
	{
		//BEGIN: Modified Asset Update for HotPads
		$now = time();
		$interval = 60 * 60 * 24 * 7;
		$valid = $now - $interval;
		$path = realpath(APPLICATION_PATH.'/../public');
		//END: Modified Asset Update for HotPads

		try {
			$buildings = $this->getBuildingsResidentialRentals();
		} catch (Exception $e) {
			echo $e->getMessage()."\n";
			die;
		}

		$xml = new DomDocument('1.0', 'UTF-8');
		$main_element = $xml->createElement('PhysicalProperty');
		$management = $xml->createElement('Management');
		$management->setAttribute('IDValue', '1');
		$management->setAttribute('OrganizationName', 'Goldfarb Properties');
		$management->setAttribute('IDType', 'managmentID');
		$main_element->appendChild($management);

		foreach ($buildings as $building) {
			//region Property
			$property = $xml->createElement('Property');
			$property->setAttribute('IDValue', $building->id);
			$property->setAttribute('IDRank', 'primary');

			//region PropertyID
			$property_id = $xml->createElement('PropertyID');

			$identification = $xml->createElement('Identification');
			$identification->setAttribute('IDValue', $building->id);
			$identification->setAttribute('IDRank', 'primary');
			$property_id->appendChild($identification);

			$marketing_name = $xml->createElement('MarketingName');
			$marketing_name->nodeValue = $building->name;
			$property_id->appendChild($marketing_name);

			$web_site = $xml->createElement('WebSite');
			$web_site->nodeValue = $this->_domain."/rentals/{$building->region->code}/{$building->code}";
			$property_id->appendChild($web_site);

			$address = $xml->createElement('Address');
			$address->setAttribute('AddressType', 'property');

			$address_line_1 = $xml->createElement('AddressLine1');
			$address_line_1->nodeValue = $building->street1;
			$address->appendChild($address_line_1);

			$address_line_2 = $xml->createElement('AddressLine2');
			$address_line_2->nodeValue = $building->street2;
			$address->appendChild($address_line_2);

			$city = $xml->createElement('City');
			$city->nodeValue = $building->city;
			$address->appendChild($city);

			$state = $xml->createElement('State');
			$state->nodeValue = $building->state;
			$address->appendChild($state);

			$postal_code = $xml->createElement('PostalCode');
			$postal_code->nodeValue = $building->zip;
			$address->appendChild($postal_code);

			$property_id->appendChild($address);
			$property->appendChild($property_id);
			//endregion

			//region ILS_Identification
			$ils_identification = $xml->createElement('ILS_Identification');
			$ils_identification->setAttribute('ILS_IdentificationType', 'Apartment');
			$ils_identification->setAttribute('RentalType', 'Unspecified');
			$ils_identification->nodeValue = '';
			$property->appendChild($ils_identification);
			//endregion

			//region Information
			$information = $xml->createElement('Information');
			$short_description = $xml->createElement('ShortDescription');
			$short_description->appendChild($xml->createCDATASection($building->description));
			$information->appendChild($short_description);
			$long_description = $xml->createElement('LongDescription');
			$long_description->appendChild($xml->createCDATASection($building->description));
			$information->appendChild($long_description);
			$property->appendChild($information);
			//endregion

			//region Amenities
			$amenities = $this->_amenity_mapper->fetchAmenityNameTagPairsByBuildingId($building->id,'apartmentscom_tag');
			if ($amenities) {
				foreach ($amenities as $amenity) {
					$amenity_tag = $xml->createElement('Amenity');
					$amenity_tag->setAttribute('AmenityType', (!empty($amenity['tag']))?$amenity['tag']:'Other');
					$description = $xml->createElement('Description');
					$description->nodeValue = htmlspecialchars($amenity['name']);
					$amenity_tag->appendChild($description);
					$property->appendChild($amenity_tag);
				}
			}
			//endregion

			$units = $this->_unit_mapper->fetchAvailableUnitsByBuildingId($building->id);
			if ($units) {
				//region ILS_Unit
				foreach ($units as $unit) {
					$unit_streeteasy_number = ($unit->feed_number_override)?$unit->feed_number_override:$unit->number;
					$unit_net_effective_price = $this->_unit_mapper->getUnitNetEffectivePrice($unit->id);
					$unit_concession_message = $this->_unit_mapper->getUnitConcessionMessage($unit->id);

					$ils_unit = $xml->createElement('ILS_Unit');
					$ils_unit->setAttribute('IDValue', $unit_streeteasy_number);
					$ils_unit->setAttribute('IDType', 'UnitID');
					$ils_unit->setAttribute('IDRank', 'primary');

					//region Units
					$units_tag = $xml->createElement('Units');

					//region Unit
					$unit_tag = $xml->createElement('Unit');

					//region Identification
					$identification = $xml->createElement('Identification');
					$identification->setAttribute('IDValue', $unit_streeteasy_number);
					$identification->setAttribute('IDType', 'UnitID');
					$identification->setAttribute('IDRank', 'primary');
					$unit_tag->appendChild($identification);
					//endregion

					//region Identification
					$identification = $xml->createElement('Identification');
					$identification->setAttribute('IDValue', $unit->id);
					$identification->setAttribute('IDType', 'FloorplanID');
					$identification->setAttribute('IDRank', 'primary');
					$unit_tag->appendChild($identification);
					//endregion

					//region MarketingName
					$marketing_name = $xml->createElement('MarketingName');
					$marketing_name->nodeValue = $unit_streeteasy_number;
					$unit_tag->appendChild($marketing_name);
					//endregion

					//region WebSite
					$web_site = $xml->createElement('WebSite');
					$web_site->nodeValue = $this->_domain."/rentals/{$building->region->code}/{$building->code}";
					$unit_tag->appendChild($web_site);
					//endregion

					//region Policy
					$policy = $xml->createElement('Policy');
					$pet = $xml->createElement('Pet');
					$pet->setAttribute('Allowed', 'false');
					$policy->appendChild($pet);
					$unit_tag->appendChild($policy);
					//endregion

					//region UnitBedrooms
					$unit_bedrooms = $xml->createElement('UnitBedrooms');
					$unit_bedrooms->nodeValue = $unit->bedrooms;
					$unit_tag->appendChild($unit_bedrooms);
					//endregion

					//region UnitBathrooms
					$unit_bathrooms = $xml->createElement('UnitBathrooms');
					$unit_bathrooms->nodeValue = $unit->bathrooms;
					$unit_tag->appendChild($unit_bathrooms);
					//endregion

					//region UnitRent
					$unit_rent = $xml->createElement('UnitRent');
					$unit_rent->nodeValue = $unit_net_effective_price;
					$unit_tag->appendChild($unit_rent);
					//endregion

					//region MinSquareFeet/MaxSquareFeet
					if ($unit->square_footage_status == 'enabled') {
						//region MinSquareFeet
						$min_square_feet = $xml->createElement('MinSquareFeet');
						$min_square_feet->nodeValue = $unit->square_footage;
						$unit_tag->appendChild($min_square_feet);
						//endregion

						//region MaxSquareFeet
						$max_square_feet = $xml->createElement('MaxSquareFeet');
						$max_square_feet->nodeValue = $unit->square_footage;
						$unit_tag->appendChild($max_square_feet);
						//endregion
					}
					//endregion

					//region UnitLeasedStatus
					$unit_leased_status = $xml->createElement('UnitLeasedStatus');
					$unit_leased_status->nodeValue = 'available';
					$unit_tag->appendChild($unit_leased_status);
					//endregion

					$units_tag->appendChild($unit_tag);
					//endregion

					$ils_unit->appendChild($units_tag);
					//endregion

					//region Comment
					$comment = $xml->createElement('Comment');
					$comment_str = '';
					if ($unit_concession_message) {
						$comment_str .= $unit_concession_message.'<br>';
					}
					if ($unit->description) {
						$comment_str .= $unit->description;
					}
					$comment->appendChild($xml->createCDATASection($comment_str));
					$ils_unit->appendChild($comment);
					//endregion

					//region SquareFeet
					if ($unit->square_footage_status == 'enabled') {
						$square_rent = $xml->createElement('SquareFeet');
						$square_rent->setAttribute('Min', $unit->square_footage);
						$square_rent->setAttribute('Max', $unit->square_footage);
						$ils_unit->appendChild($square_rent);
					}
					//endregion

					//region EffectiveRent
					$effective_rent = $xml->createElement('EffectiveRent');
					$effective_rent->setAttribute('Min', $unit_net_effective_price);
					$effective_rent->setAttribute('Max', $unit_net_effective_price);
					$ils_unit->appendChild($effective_rent);
					//endregion

					//region Unit Features (aka Amenity)
					$features = $this->_feature_mapper->fetchFeatureNameTagPairsByUnitId($unit->id,'apartmentscom_tag');
					if ($features) {
						foreach ($features as $feature) {
							$feature_tag = $xml->createElement('Amenity');
							$feature_tag->setAttribute('AmenityType', (!empty($feature['tag']))?$feature['tag']:'Other');
							$description = $xml->createElement('Description');
							$description->nodeValue = htmlspecialchars($feature['name']);
							$feature_tag->appendChild($description);
							$ils_unit->appendChild($feature_tag);
						}
					}
					//endregion

					//region Gallery Images
					$images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit->id);
					if ($images) {
						foreach ($images as $index => $image) {
							//region File
							$file = $xml->createElement('File');
							$file->setAttribute('FileID', $image->id);
							$file->setAttribute('Active', 'true');
							//endregion

							//region FileType
							$file_type = $xml->createElement('FileType');
							$file_type->nodeValue = 'Photo';
							$file->appendChild($file_type);
							//endregion

							//region Caption
							$caption = $xml->createElement('Caption');
							$captionamenity = $xml->createTextNode($image->title);
							$caption->appendChild($captionamenity);
							$file->appendChild($caption);
							//endregion

							//region Src
							$src = $xml->createElement('Src');
							$src->nodeValue = $this->_domain.$image->image;
							$file->appendChild($src);
							//endregion

							//region Rank
							$rank = $xml->createElement('Rank');
							$rank->nodeValue = $index;
							$file->appendChild($rank);
							//endregion

							$ils_unit->appendChild($file);
						}
					}
					//endregion

					//region Floorplan Image
					if (!empty($unit->floorplan_id)) {
						/** @var Custom_Model_GfFloorplan $unit_floorplan */
						$unit_floorplan = $this->_floorplan_mapper->find($unit->floorplan_id);
						if ($unit_floorplan) {
							$file = $unit_floorplan->image;
							if(file_exists($path . $file)){
								$modified = filemtime($path . $file);
								if($modified >= $valid) {
									$file .= '?week=' . date('W', $now) . date('N', $now);
								}
								//region File
								$file_tag = $xml->createElement('File');
								//endregion

								//region FileType
								$file_type = $xml->createElement('FileType');
								$file_type->nodeValue = 'Floorplan';
								$file_tag->appendChild($file_type);
								//endregion

								//region Src
								$src = $xml->createElement('Src');
								$src->nodeValue = $this->_domain.$file;
								$file_tag->appendChild($src);
								//endregion

								$ils_unit->appendChild($file_tag);
							}
						}
					}
					//endregion
					$property->appendChild($ils_unit);

					$floorplan = $xml->createElement('Floorplan');
					$floorplan->setAttribute('IDValue', $unit->id);
					$floorplan->setAttribute('IDType', 'FloorplanID');
					$floorplan->setAttribute('IDRank', 'primary');

					//region FloorplanType
					$floorplan_type = $xml->createElement('FloorplanType');
					$floorplan_type->nodeValue = 'Internal';
					$floorplan->appendChild($floorplan_type);
					//endregion

					//region Name
					$name = $xml->createElement('Name');
					$name->nodeValue = 'Apartment '.$unit_streeteasy_number;
					$floorplan->appendChild($name);
					//endregion

					//region Comment
					$comment = $xml->createElement('Comment');
					$comment->appendChild($xml->createCDATASection($unit->description));
					$floorplan->appendChild($comment);
					//endregion

					//region UnitCount
					$unit_count = $xml->createElement('UnitCount');
					$unit_count->nodeValue = 1;
					$floorplan->appendChild($unit_count);
					//endregion

					//region UnitsAvailable
					$unit_available = $xml->createElement('UnitsAvailable');
					$unit_available->nodeValue = 1;
					$floorplan->appendChild($unit_available);
					//endregion

					//region Room - Bedroom
					$room = $xml->createElement('Room');
					$room->setAttribute('RoomType', 'Bedroom');
					$room_count = $xml->createElement('Count');
					$room_count->nodeValue = floor($unit->bedrooms);
					$room->appendChild($room_count);
					$floorplan->appendChild($room);
					//endregion

					//region Room - Bathroom
					$room = $xml->createElement('Room');
					$room->setAttribute('RoomType', 'Bathroom');
					$room_count = $xml->createElement('Count');
					$room_count->nodeValue = floor($unit->bathrooms);
					$room->appendChild($room_count);
					$floorplan->appendChild($room);
					//endregion

					//region EffectiveRent
					$effective_rent = $xml->createElement('EffectiveRent');
					$effective_rent->setAttribute('Min', $unit_net_effective_price);
					$effective_rent->setAttribute('Max', $unit_net_effective_price);
					$floorplan->appendChild($effective_rent);
					//endregion

					//square footage
					if ($unit->square_footage_status == 'enabled') {
						$square_rent = $xml->createElement('SquareFeet');
						$square_rent->setAttribute('Min', $unit->square_footage);
						$square_rent->setAttribute('Max', $unit->square_footage);
						$floorplan->appendChild($square_rent);
					}
					//end square footage

					//region Unit Features (aka Amenity)
					$features = $this->_feature_mapper->fetchFeatureNameTagPairsByUnitId($unit->id,'apartmentscom_tag');
					if ($features) {
						foreach ($features as $feature) {
							$feature_tag = $xml->createElement('Amenity');
							$feature_tag->setAttribute('AmenityType', (!empty($feature['tag']))?$feature['tag']:'Other');
							$description = $xml->createElement('Description');
							$description->nodeValue = htmlspecialchars($feature['name']);
							$feature_tag->appendChild($description);
							$floorplan->appendChild($feature_tag);
						}
					}
					//endregion

					//region Gallery Images
					$images = $this->_unit_image_mapper->fetchOrderedByUnitId($unit->id);
					if ($images) {
						foreach ($images as $index => $image) {
							$file = $xml->createElement('File');
							$file->setAttribute('FileID', $image->id);
							$file->setAttribute('Active', 'true');
							$file_type = $xml->createElement('FileType');
							$file_type->nodeValue = 'Photo';
							$file->appendChild($file_type);

							$caption = $xml->createElement('Caption');
							$captionamenity = $xml->createTextNode($image->title);
							$caption->appendChild($captionamenity);

							$file->appendChild($caption);
							$src = $xml->createElement('Src');
							$src->nodeValue = $this->_domain.$image->image;
							$file->appendChild($src);
							$rank = $xml->createElement('Rank');
							$rank->nodeValue = $index;
							$file->appendChild($src);
							$floorplan->appendChild($file);
						}
					}
					//endregion

					//region Floorplan Images
					if (!empty($unit->floorplan_id)) {
						/** @var Custom_Model_GfFloorplan $floorplan_entity */
						$floorplan_entity = $this->_floorplan_mapper->find($unit->floorplan_id);
						if ($floorplan_entity) {
							$file = $xml->createElement('File');
							$file->setAttribute('FileID', $unit->floorplan_id);
							$file->setAttribute('Active', 'true');

							$file_type = $xml->createElement('FileType');
							$file_type->nodeValue = 'Floorplan';
							$file->appendChild($file_type);

							$caption = $xml->createElement('Caption');
							$captionamenity = $xml->createTextNode($floorplan_entity->title);
							$caption->appendChild($captionamenity);
							$file->appendChild($caption);

							$src = $xml->createElement('Src');
							$src->nodeValue = $this->_domain.$floorplan_entity->image;
							$file->appendChild($src);

							$rank = $xml->createElement('Rank');
							$rank->nodeValue = 0;
							$file->appendChild($rank);
							$floorplan->appendChild($file);
						}
					}
					//endregion

					$property->appendChild($floorplan);
				}
				//endregion
			}

			//region Building Gallery Images
			$building_image_mapper = new Custom_Model_Mapper_GfBuildingImage();
			$images = $building_image_mapper->fetchOrderedByBuildingId($building->id);
			if ($images) {
				foreach ($images as $index => $image) {
					//region File
					$file = $xml->createElement('File');
					$file->setAttribute('FileID', $image->id);
					$file->setAttribute('Active', 'true');
					//endregion

					//region FileType
					$file_type = $xml->createElement('FileType');
					$file_type->nodeValue = 'Photo';
					$file->appendChild($file_type);
					//endregion

					//region Caption
					$caption = $xml->createElement('Caption');
					$caption->nodeValue = $image->title;
					$file->appendChild($caption);
					//endregion

					//region Src
					$src = $xml->createElement('Src');
					$src->nodeValue = $this->_domain.$image->image;
					$file->appendChild($src);
					//endregion

					//region Rank
					$rank = $xml->createElement('Rank');
					$rank->nodeValue = $index;
					$file->appendChild($src);
					//endregion

					$property->appendChild($file);
				}
			}
			//endregion

			$main_element->appendChild($property);
			//endregion
		}
		$xml->appendChild($main_element);
		$xml->formatOutput = TRUE;

		header('Content-type: text/xml');
		echo $xml->saveXML();
		exit;

		/*
		$buildings = Building::rentals()->with('neighborhood', 'address', 'images', 'units', 'openHouses')->get();

			<?php
	//BEGIN: Modified Asset Update for HotPads
	$now = time();
	$interval = 60 * 60 * 24 * 7;
	$valid = $now - $interval;
	$path = $_SERVER['SITE_FILE_ROOT'];
	//END: Modified Asset Update for HotPads
	?>
	<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>
	<PhysicalProperty>
		<Management IDValue="1" OrganizationName="Goldfarb Properties" IDType="managmentID"/>
		@foreach ($buildings as $building)
			<Property IDValue="{{$building->id}}" IDRank="primary">
				<PropertyID>
					<Identification IDValue="{{$building->id}}" IDRank="primary"/>
					<MarketingName>{{$building->name}}</MarketingName>
					<WebSite>{{url(route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug, 'buildingSlug' => $building->slug]))}}</WebSite>
					<Address AddressType="property">
						<AddressLine1>{{$building->address->street_1}}</AddressLine1>
						<AddressLine2>{{$building->address->street_2}}</AddressLine2>
						<City>{{$building->address->city}}</City>
						<State>{{$building->address->state}}</State>
						<PostalCode>{{$building->address->zip}}</PostalCode>
					</Address>
				</PropertyID>
				<ILS_Identification ILS_IdentificationType="Apartment" RentalType="Unspecified">
				</ILS_Identification>
				<Information>
					<ShortDescription><![CDATA[ {!!$building->body!!}]]></ShortDescription>
					<LongDescription><![CDATA[ {!!$building->body!!}]]></LongDescription>
				</Information>
				@foreach ($building->amenities as $amenity)
					<Amenity AmenityType="{{$amenity->feed_meta['apartments-com'] or 'Other'}}">
						<Description>{{$amenity->name}}</Description>
					</Amenity>
				@endforeach
				@foreach ($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get() as $unit)
					<ILS_Unit IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID" IDRank="primary">
						<Units>
							<Unit>
								<Identification IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID"
												IDRank="primary"/>
								<Identification IDValue="{{$unit->id}}" IDType="FloorplanID" IDRank="primary"/>
								<MarketingName>{{$unit->getStreeteasyNumber()}}</MarketingName>
								<WebSite>{{url(route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug]))}}</WebSite>
								<Policy>
									<Pet Allowed="false"/>
								</Policy>
								<UnitBedrooms>{{$unit->bedrooms}}</UnitBedrooms>
								<UnitBathrooms>{{$unit->bathrooms}}</UnitBathrooms>
								<UnitRent>{{$unit->net_effective_price}}</UnitRent>
								@if ($unit->square_footage_toggle == 'enabled')
									<MinSquareFeet>{{$unit->square_footage}}</MinSquareFeet>
									<MaxSquareFeet>{{$unit->square_footage}}</MaxSquareFeet>
								@endif
								<UnitLeasedStatus>available</UnitLeasedStatus>
							</Unit>
						</Units>
						<Comment><![CDATA[ @if (!!$unit->getConcessionMessage()) {{$unit->getConcessionMessage()}}<br>
							<br>@endif @if (!!$unit->body){{$unit->body}}@endif ]]>
						</Comment>
						@if ($unit->square_footage_toggle == 'enabled')
							<SquareFeet Min="{{$unit->square_footage}}" Max="{{$unit->square_footage}}"/>
						@endif
						<EffectiveRent Min="{{$unit->net_effective_price}}" Max="{{$unit->net_effective_price}}"/>
						@foreach ($unit->features as $feature)
							<Amenity AmenityType="{{$feature->feed_meta['apartments-com'] or 'Other'}}">
								<Description>{{$feature->name}}</Description>
							</Amenity>
						@endforeach
						@foreach ($unit->getGalleryImages() as $index => $image)
							<File FileID="{{$image->id}}" Active="true">
								<FileType>Photo</FileType>
								<Caption>{{$image->title}}</Caption>
								<Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
								<Rank>{{$index}}</Rank>
							</File>
						@endforeach
						<?php
						$file = str_replace($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'], '', $unit->basicFloorplanSrc());
						if(file_exists($path . $file)){
						$modified = filemtime($path . $file);
						if($modified >= $valid) {
						?>
						<File>
							<FileType>Floorplan</FileType>
							<Src>{{asset($unit->basicFloorplanSrc())}}
								?<?php echo 'week=' . date('W', $now) . date('N', $now);?></Src>
						</File>
						<?php } else { ?>
						<File>
							<FileType>Floorplan</FileType>
							<Src>{{asset($unit->basicFloorplanSrc())}}</Src>
						</File>
						<?php } ?>
						<?php } ?>
					</ILS_Unit>
				@endforeach
				@foreach ($building->getGalleryImages() as $index => $image)
					<File FileID="{{$image->id}}" Active="true">
						<FileType>Photo</FileType>
						<Caption>{{$image->title}}</Caption>
						<Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
						<Rank>{{$index}}</Rank>
					</File>
				@endforeach
			</Property>
		@endforeach
	</PhysicalProperty>

			 */
	}

	public function apartmentguideAction()
	{
		/*
		$buildings = Building::rentals()->with('neighborhood', 'address', 'images', 'units', 'openHouses')->get();

		<?php print '<?xml version="1.0" encoding="UTF-8"?>' ?>
<PhysicalProperty>
    <Management IDValue="1" OrganizationName="Goldfarb Properties" IDType="managmentID"/>
    @foreach ($buildings as $building)
        <Property IDValue="{{$building->id}}" IDRank="primary">
            <PropertyID>
                <Identification IDValue="{{$building->id}}" IDRank="primary"/>
                <MarketingName>{{$building->name}}</MarketingName>
                <WebSite>{{url(route('portfolio.building', ['regionSlug' => $building->neighborhood->region->slug, 'buildingSlug' => $building->slug]))}}</WebSite>
                <Address AddressType="property">
                    <AddressLine1>{{$building->address->street_1}}</AddressLine1>
                    <AddressLine2>{{$building->address->street_2}}</AddressLine2>
                    <City>{{$building->address->city}}</City>
                    <State>{{$building->address->state}}</State>
                    <PostalCode>{{$building->address->zip}}</PostalCode>
                </Address>
            </PropertyID>
            <ILS_Identification ILS_IdentificationType="Apartment" RentalType="Unspecified">
            </ILS_Identification>
            <Information>
                <ShortDescription><![CDATA[ {!!$building->body!!}]]></ShortDescription>
                <LongDescription><![CDATA[ {!!$building->body!!}]]></LongDescription>
            </Information>
            @foreach ($building->amenities as $amenity)
                <Amenity AmenityType="{{$amenity->feed_meta['apartments-com'] or 'Other'}}">
                    <Description>{{$amenity->name}}</Description>
                </Amenity>
            @endforeach
            @foreach ($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get() as $unit)
                <Floorplan IDValue="{{$unit->id}}" IDType="FloorplanID" IDRank="primary">
                    <FloorplanType>Internal</FloorplanType>
                    <Name>Apartment {{$unit->getStreeteasyNumber()}}</Name>
                    <Comment><![CDATA[ {!!$unit->body!!}]]></Comment>
                    <UnitCount>1</UnitCount>
                    <UnitsAvailable>1</UnitsAvailable>
                    <Room RoomType="Bedroom">
                        <Count>{{floor($unit->bedrooms)}}</Count>
                    </Room>
                    <Room RoomType="Bathroom">
                        <Count>{{$unit->bathrooms}}</Count>
                    </Room>
                    @if ($unit->square_footage_toggle == 'enabled')
                        <SquareFeet min="{{$unit->square_footage}}" max="{{$unit->square_footage}}"/>
                    @endif
                    <EffectiveRent min="{{$unit->net_effective_price}}" max="{{$unit->net_effective_price}}"/>
                    @foreach ($unit->features as $feature)
                        <Amenity AmenityType="{{$feature->feed_meta['apartments-com'] or 'Other'}}">
                            <Description>{{$feature->name}}</Description>
                        </Amenity>
                    @endforeach
                    @foreach ($unit->getGalleryImages() as $index => $image)
                        <File FileID="{{$image->id}}" Active="true">
                            <FileType>Photo</FileType>
                            <Caption>{{$image->title}}</Caption>
                            <Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
                            <Rank>{{$index}}</Rank>
                        </File>
                    @endforeach
                    @if (count($unit->floorplan))
                        <File FileID="{{$unit->floorplan->id}}" Active="false">
                            <FileType>Floorplan</FileType>
                            <Caption>{{$unit->floorplan->title}}</Caption>
                            <Src>{!!url($unit->floorplanSrc())!!}</Src>
                            <Rank>0</Rank>
                        </File>
                    @endif
                </Floorplan>
            @endforeach
            @foreach ($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get() as $unit)
                <ILS_Unit IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID" IDRank="primary">
                    <Units>
                        <Unit>
                            <Identification IDValue="{{$unit->getStreeteasyNumber()}}" IDType="UnitID"
                                            IDRank="primary"/>
                            <Identification IDValue="{{$unit->id}}" IDType="FloorplanID" IDRank="primary"/>
                            <MarketingName>{{$unit->getStreeteasyNumber()}}</MarketingName>
                            <UnitBedrooms>{{$unit->bedrooms}}</UnitBedrooms>
                            <UnitBathrooms>{{$unit->bathrooms}}</UnitBathrooms>
                            <UnitRent>{{$unit->net_effective_price}}</UnitRent>
                            <UnitLeasedStatus>available</UnitLeasedStatus>
                        </Unit>
                    </Units>
                    <Comment><![CDATA[ {!!$unit->body!!}]]></Comment>
                    @if ($unit->square_footage_toggle == 'enabled')
                        <SquareFeet min="{{$unit->square_footage}}" max="{{$unit->square_footage}}"/>
                    @endif
                    <EffectiveRent Min="{{$unit->net_effective_price}}" Max="{{$unit->net_effective_price}}"/>
                    @foreach ($unit->features as $feature)
                        <Amenity AmenityType="{{$feature->feed_meta['apartments-com'] or 'Other'}}">
                            <Description>{{$feature->name}}</Description>
                        </Amenity>
                    @endforeach
                    @foreach ($unit->getGalleryImages() as $index => $image)
                        <File FileID="{{$image->id}}" Active="true">
                            <FileType>Photo</FileType>
                            <Caption>{{$image->title}}</Caption>
                            <Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
                            <Rank>{{$index}}</Rank>
                        </File>
                    @endforeach
                </ILS_Unit>
            @endforeach
            @foreach ($building->getGalleryImages() as $index => $image)
                <File FileID="{{$image->id}}" Active="true">
                    <FileType>Photo</FileType>
                    <Caption>{{$image->title}}</Caption>
                    <Src>{!!url($image->styleSrc("carousel-slide"))!!}</Src>
                    <Rank>{{$index}}</Rank>
                </File>
            @endforeach
        </Property>
    @endforeach
</PhysicalProperty>

		 */
	}

	/**
	 * converted: Task# 60511 [JSON]
	 */
	public function goldfarbAction()
	{
		date_default_timezone_set("America/New_York");
		$stamp = date("Ymd_hia");
		$filename = "goldfarb_" . $stamp . ".csv";

		header("Content-type: text/plain");
		header("Content-Disposition: attachment; filename=$filename");

		$output = Custom_Service_Audit::goldfarb();
		$fh = fopen('php://output', 'w');
		fwrite($fh, json_encode($output, JSON_PRETTY_PRINT));
		fclose($fh);
		exit;
		/*
		$buildings = Building::rentals()->with('units')->get();

		<?php
$status = [];
foreach ($buildings as $building) {
  foreach ($building->units as $unit) {
    $array = [];
    $array['unit_id'] = $unit->id;
    $array['type'] = 'goldfarb';
    $array['status'] = $unit->status;
    $array['rentcafe_status'] = 'Not Applicable';
    $array['price'] = money_format('$%i', $unit->price);
    $array['net_effective'] = money_format('$%i', $unit->getNetEffectivePriceAttribute());
    $array['concession'] = ($unit->concession_language) ? ucwords(substr_replace($unit->concession_language, ' ', 3, 0)) : 'Not Set';
    $today = date("Y-m-d");
    $openhouse = 'No';
    foreach ($building->openHouses as $open) {
      $start = strtotime($open->start_time);
      $end = strtotime($open->end_time);
      $startdate = date("Y-m-d", $start);
      $starttime = date("H:i", $start);
      $endtime = date("H:i", $end);
      if((string) $today == (string) $startdate) {
        if($unit->disable_openhouse != "yes") {
          $openhouse = 'Yes';
        }
      }
    }
    $array['openhouse'] = $openhouse;
    array_push($status, $array);
  }
}
echo json_encode($status);
?>

		*/
	}

	public function rentcafeAction()
	{
/*
 $buildings = Building::residential()->whereNotNull('yardi_code')->get();
        foreach ($buildings as $building) {
            $properties = explode(',', $building->yardi_code);

            foreach ($properties as $propertyCode) {
                $data = Yardi::getAvailability($propertyCode);
                $building->yardi_data = $data;
            }
        }


<?php
$status = [];
$unique = [];
foreach ($buildings as $building) {
  foreach ($building->yardi_data as $yardi) {
    $unit = DB::select('select * from units where yardi_code = ?', [$yardi['ApartmentId']]);
    $unit = array_pop($unit);
    $array = [];
    if(isset($unit->id) && $unit->id != null) {
      $array['unit_id'] = $unit->id;
    }
    $array['type'] = 'rentcafe';
    $array['status'] = 'available';
    if($yardi['UnitStatus'] == 'Vacant Unrented Not Ready'){
      $array['status'] = 'unavailable';
    }
    $array['rentcafe_status'] = $yardi['UnitStatus'];
    if(!in_array($array['status'], $unique)) {
      array_push($unique, $array['status']);
    }
    $array['price'] = '';
    $array['net_effective'] = '';
    $array['concession'] = '';
    $array['openhouse'] = '';
    array_push($status, $array);
  }
}
echo json_encode($status);

 */
	}

	/**
	 * converted: Completed [CSV]
	 */
	public function googledynamicAction()
	{
		/**
		 * The order entered into this array will be the order of the CSV columns.
		 */
		$headerNames = [
			"Listing ID", "Listing name", "Final URL", "Image URL", "City name", "Description", "Price",
			"Property type", "Listing type", "Contextual keywords", "Address", "Tracking template",
			"Custom parameter", "Final mobile URL"
		];

		date_default_timezone_set("America/New_York");
		$stamp = date("Ymd_hia");
		$filename = "googledynamic_" . $stamp . ".csv";

		header("Content-type: text/plain");
		header("Content-Disposition: attachment; filename=$filename");

		$fh = fopen('php://output', 'w');

		//region Render Header
		fputcsv($fh, $headerNames);
		//endregion

		$buildings = $this->getBuildingsResidentialRentals();
		foreach ($buildings as $building){
			$units = $this->_unit_mapper->fetchAvailableUnitsByBuildingId($building->id);
			if ($units === false) {
				continue;
			}
			foreach ($units as $unit) {
				$line = [];
				// unit available
				if (strtolower($unit->status) !== 'available') {
					continue;
				}
				// unit residential rental
				if (stripos($unit->residential, '"Rentals"') === false) {
					continue;
				}
				$image = '';
				/** @var Custom_Model_GfImage $unit_image */
				$unit_image = $this->_unit_image_mapper->fetchPrimaryByUnitId($unit->id);
				if (!empty($unit_image)) {
					$image = $this->_domain.$unit_image->image;
				}
				$url = $this->_domain.'/rentals/'.$building->region->code.'/'.$building->code.'/apartment-'.$unit->code;

				$line[] = $unit->id;
				$line[] = $building->name;
				$line[] = $url;
				$line[] = $image;
				$line[] = $building->city.' '.$building->state;
				$formatted_bedrooms = $this->_unit_mapper->getFormattedBedrooms($unit->bedrooms);
				$formatted_bathrooms = $this->_unit_mapper->getFormattedBathrooms($unit->bathrooms);
				$numbers = $formatted_bedrooms.' '.$formatted_bathrooms;
				$formatted_square_footage = '';
				if ($unit->square_footage > 0) {
					$formatted_square_footage = $unit->square_footage.' sq ft';
					$numbers .= ' '.$formatted_square_footage;
				}
				$line[] = $numbers;
				$line[] = $this->_unit_mapper->getUnitNetEffectivePrice($unit->id).' USD';
				$line[] = 'Apartment';
				$line[] = 'Rental';
				//region Generate Keywords
				$keywords = [];
				$keywords[] = 'Apartment';
				$keywords[] = 'Rental';
				$keywords[] = $formatted_bedrooms;
				$keywords[] = $formatted_bathrooms;
				if (!empty($formatted_square_footage)) {
					$keywords[] = $formatted_square_footage;
				}
				// todo: add unit features to $keywords
				$features = $this->_feature_mapper->fetchFeatureNamesByUnitId($unit->id);

				$keywords = array_unique(array_merge($keywords, $building->amenities, $features));
				array_walk($keywords, function (&$item, $idx) {
					$item = str_replace(',', ';', $item);
				});
				//endregion
				$line[] = implode(';', $keywords);
				$line[] = $building->street1.' '.$building->city.' '.$building->state.' '.$building->zip;
				$line[] = '';
				$line[] = '';
				$line[] = $url;

				fputcsv($fh, $line);
			}
		}
		fclose($fh);
		exit;

		/*
		 $buildings = Building::rentals()->with('neighborhood', 'address', 'images', 'units', 'openHouses')->get();
		<?php
				echo "Listing ID,Listing name,Final URL,Image URL,City name,Description,Price,Property type,Listing type,Contextual keywords,Address,Tracking template,Custom parameter,Final mobile URL\r\n";
				foreach ($buildings as $building){
					$amenities = $building->amenities->sortBy('name')->pluck('name')->all();
					//echo "<pre>";
					//var_dump($amenities);
					//echo "</pre>";
					foreach($building->units()->available()->rentals()->with('features' , 'floorplan', 'images')->get() as $unit){
						if($building->id == $unit->building_id){
							$image = $unit->getThumbnail();
							echo "$unit->id" . "," . $building->name . ",";
							echo "" . url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug])) . ",";
							echo "" . url($image->src()) . ",";
							echo "" . $building->address->city . " " . $building->address->state . ",";
							echo "" . $unit->formatted_bedrooms . " " . $unit->formatted_bathrooms . " ";
							echo "" . ($unit->square_footage > 0) ? $unit->square_footage . " sq ft," : ",";
							echo "" . $unit->net_effective_price . " USD,";
							echo "" . "Apartment" . ",";
							echo "" . "Rental" . ",";
							echo "" .  "Apartment; Rental; " . $unit->formatted_bedrooms . "; " . $unit->formatted_bathrooms . "; ";
							echo ($unit->square_footage > 0) ? $unit->square_footage . " sq ft; " : "";
							$features = $unit->features->sortBy('name')->pluck('name')->all();
							foreach($features as $feature) {
								if(!in_array($feature, $amenities)) {
									array_push($amenities, $feature);
								}
							}
							$n = count($amenities);
							$i = 0;
							foreach($amenities as $amenity) {
								  if(++$i !== $n) {
									echo str_replace( ',', ';', $amenity ) . "; ";
								  } else {
									echo str_replace( ',', ';', $amenity );
								  }
							}
							echo ",";
							echo "" . $building->address->street_1 . " "  . $building->address->city . " " . $building->address->state . " " . $building->address->zip . "";
							echo "" .  ",";
							echo "" .  ",";
							echo "" .  ",";
							echo "" . url(route('portfolio.unit', ['regionSlug' => $unit->building->neighborhood->region->slug, 'buildingSlug' => $unit->building->slug, 'unitSlug' => $unit->slug])) . "\r\n";
						}
					}
				}

		 */
	}

	public function auditAction()
	{
		//$regionSlug, $buildingSlug, $type
		$regionSlug = $this->getRequest()->getParam('region', null);
		$buildingSlug = $this->getRequest()->getParam('building', null);
		$type = $this->getRequest()->getParam('type', null);
		if (is_null($regionSlug) || is_null($buildingSlug) || is_null($type)) {
			exit("Missing Parameters");
		}
		/**
		 * The order entered into this array will be the order of the CSV columns.
		 */
		$headerNames = [
			"number", "type", "price", "net_effective", "concession", "openhouse", "status",
			"rentcafe_status", "created",
		];

		date_default_timezone_set("America/New_York");
		$stamp = date("Ymd_hia");
		$filename = "audit_" . $stamp . ".csv";

		header("Content-type: text/plain");
		header("Content-Disposition: attachment; filename=$filename");

		$fh = fopen('php://output', 'w');

		//region Render Header
		$headers = [];
		foreach ($headerNames as $v) {
			if ($v == 'id') {
				//need to keep id not all caps otherwise Excel thinks the CSV is a SYLK file.
				$headers[] = str_replace("_", " ", ucfirst($v));
			} else {
				$headers[] = str_replace("_", " ", strtoupper($v));
			}
		}
		fputcsv($fh, $headers);
		//endregion

		//region Render Lines
		$buildings = $this->_building_mapper->fetchByBuildingCodeAndRegionCode($buildingSlug, $regionSlug, 1);
		if (!$buildings) {
			exit;
		}
		$building = $buildings[0];
		$units = $this->_unit_mapper->fetchUnitsByBuildingId($building->id);
		$activities = $this->_unit_mapper->activity($units, $type);
		foreach ($activities as $detail) {
			$line = [];
			foreach ($headerNames as $header) {
				switch ($header) {
					default:
						if (isset($detail[$header])) {
							$line[] = $detail[$header];
						} else {
							Zend_Debug::dump($headerNames);
							Zend_Debug::dump($detail);
							exit("Property {$header} does not exists in the 'audit activity'.");
						}
				}
			}
			fputcsv($fh, $line);
		}
		//endregion

		fclose($fh);
		exit;
		/*
		 $all = "";
        $building = Building::whereSlug($buildingSlug)->region($regionSlug)->firstOrFail();
        foreach($building->units()->rentals()->get() as $unit) {
            $all .= "'".$unit->id."', ";
        }
        $all = substr_replace($all, '', -2);
        $activity = DB::select("SELECT u.number, a.type, a.price, a.net_effective, a.concession, a.openhouse, a.status, a.rentcafe_status, a.created_at FROM audits a LEFT JOIN units u ON a.unit_id = u.id WHERE a.type = '".$type."' AND a.created_at >= DATE_ADD(NOW(), INTERVAL -21 DAY) AND a.unit_id IN (".$all.") ORDER BY a.unit_id ASC, a.id DESC;");

		<?php
$report = [];
foreach($activity as $detail) {
  $event = [];
  $event['number'] =  $detail->number;
  $event['type'] =  $detail->type;
  $event['price'] =  $detail->price;
  $event['net_effective'] =  $detail->net_effective;
  $event['concession'] =  $detail->concession;
  $event['openhouse'] =  $detail->openhouse;
  $event['status'] =  $detail->status;
  $event['rentcafe_status'] =  $detail->rentcafe_status;
  $event['created_at'] =  $detail->created_at;
  array_push($report, $event);
}

$fh = fopen( 'php://output', 'w' );

$members = "";
$headerNames = array(
  "number",
  "type",
  "price",
  "net_effective",
  "concession",
  "openhouse",
  "status",
  "rentcafe_status",
  "created_at"
);

foreach ($headerNames as $k=>$v) {
  if ($v == 'id') {
    //need to keep id not all caps otherwise Excel thinks the CSV is a SYLK file.
    $headers[] = str_replace("_"," ",ucfirst($v));
  }else {
    $headers[] = str_replace("_"," ",strtoupper($v));
  }
}
fputcsv($fh, $headers);

foreach($report as $key => $val) {
  $memberRow = '';
  foreach ($headerNames as $k=>$v) {
    $memberRow[] .= $val[$v];
  }
  fputcsv($fh, $memberRow);
}

date_default_timezone_set("America/New_York");
$stamp = date("Ymd_hia");
$filename = "audit_report_" . $buildingSlug . "_" .$stamp . ".csv";

header("Content-type: text/plain");
header("Content-Disposition: attachment; filename=$filename");

fclose($fh);
?>

		 */

	}
}
