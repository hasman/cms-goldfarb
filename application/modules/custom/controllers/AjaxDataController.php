<?php
class Custom_AjaxDataController extends Zend_Controller_Action
{

	public function init()
	{

		/* Initialize action controller here */

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
	}

	public function quickApplyAction()
	{
		$this->view->layout()->disableLayout();

		$region_mapper = new Custom_Model_Mapper_GfRegion();
		$quick_apply_mapper = new Custom_Model_Mapper_GfQuickApplyUnit();

		$regions = $region_mapper->fetchOrdered();

		$array = [];
		foreach ($regions as $region) {
			$apply_links = [];

			$bedrooms = [0,1,2,3];
			foreach ($bedrooms as $bedroom) {
				$result = $quick_apply_mapper->fetchUnitByRegionId($region->id, $bedroom);

				$url = '';
				$addr1='';
				$state='';
				$zip='';
				if ($result) {
					$url = $result['apply_link'];
					$addr1 = $result['addr'];
					$state = $result['state'];
					$zip = $result['zip'];
				}
				$apply_links[$bedroom] = $url;
			}

			$array[] = ["name"=>$region->name,"slug"=>$region->code,"wait_unit_apply_array"=>$apply_links,"addr1"=>$addr1,"state"=>$state,"zip"=>$zip];

		}

		/*
		$array = [
			["name"=>"Manhattan","slug"=>"manhattan","wait_unit_apply_array" => [
				"https://goldfarbproperties.securecafe.com/onlineleasing/park-towers-south/oleapplication.aspx?stepname=RentalOptions&myOlePropertyId=218693&FloorPlanID=2118009&UnitID=7702613&header=1",
				"https://goldfarbproperties.securecafe.com/onlineleasing/park-towers-south/oleapplication.aspx?stepname=RentalOptions&myOlePropertyId=218693&FloorPlanID=1045536&UnitID=7702614&header=1",
				"https://goldfarbproperties.securecafe.com/onlineleasing/park-towers-south/oleapplication.aspx?stepname=RentalOptions&myOlePropertyId=218693&FloorPlanID=2118011&UnitID=8316751&header=1",
				"https://goldfarbproperties.securecafe.com/onlineleasing/park-towers-south/oleapplication.aspx?stepname=RentalOptions&myOlePropertyId=218693&FloorPlanID=2118016&UnitID=8316752&header=1"
				]
			]
		];
		*/

		echo json_encode($array);
		exit;
	}

}
