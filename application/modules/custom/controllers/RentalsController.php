<?php
class Custom_RentalsController extends Zend_Controller_Action
{

	public function init()
	{

		/* Initialize action controller here */
		$this->view->placeholder('body_class')->append('unit-page');

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();

		$this->view->google_maps_api_key = $options['google']['maps']['api_key'];
	}

	public function indexAction()
	{
		$mapper = new Custom_Model_Mapper_GfRegion();
		$this->view->regions = $regions = $mapper->fetchOrdered();

		$results = [];
		foreach ($regions as $region) {
			$units = $mapper->fetchUnitsByRegionId($region->id, 'residential-rentals');

			if (count($units) > 0) {
				$results[$region->code] = $units;
			}
		}
		$this->view->results = $results;
	}

	public function regionAction()
	{
		$region_code = $this->getRequest()->getParam('region_code','');

		$this->view->placeholder('body_class')->append('region-landing-page');

		$mapper = new Custom_Model_Mapper_GfRegion();
		$this->view->region = $region = $mapper->doesExists(array('code'=>$region_code));

		if ($region) {
			$mapper = new Custom_Model_Mapper_GfNeighborhood();
			$this->view->neighborhoods = $mapper->fetchByRegionId($region->id);

			$mapper = new Custom_Model_Mapper_GfRegionAttraction();
			$this->view->attractions = $mapper->fetchByRegionId($region->id);

			$mapper = new Custom_Model_Mapper_GfRegion();
			$this->view->units = $mapper->fetchUnitsByRegionId($region->id, 'residential-rentals');

			/*
			if ($unit->meta_title) {
				$this->view->headTitle()->prepend($unit->meta_title);
			}
			if ($unit->meta_description) {
				$this->view->headMeta()->setName('description', $unit->meta_description);
			}
			*/
		} else {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}
	}

	public function buildingAction()
	{
		$region_code = $this->getRequest()->getParam('region_code','');
		$building_code = $this->getRequest()->getParam('building_code','');

		$mapper = new Custom_Model_Mapper_GfRegion();
		$this->view->region = $region = $mapper->doesExists(array('code'=>$region_code));

		$mapper = new Custom_Model_Mapper_GfBuilding();
		$this->view->building = $building = $mapper->validateByCodeAndRegionId($building_code, $region->id);
		$this->view->similar_buildings = $mapper->getSimilar($region->id, $building->id, 2);

		if ($region && $building) {
			$mapper = new Custom_Model_Mapper_GfBuildingImage();
			$this->view->building_images = $mapper->fetchOrderedByBuildingId($building->id);

			$mapper = new Custom_Model_Mapper_GfAmenity();
			$this->view->building_amenities = $mapper->fetchAmenityNamesByBuildingId($building->id);

			$mapper = new Custom_Model_Mapper_GfNeighborhood();
			$this->view->neighborhood = $neighborhood = $mapper->find($building->neighborhood_id);

			$mapper = new Custom_Model_Mapper_GfBuildingOpenHouse();
			$this->view->open_houses = $mapper->fetchByBuildingId($building->id);

			$mapper = new Custom_Model_Mapper_GfAttraction();
			$this->view->region_attractions = $mapper->fetchByRegionId($region->id);

			$mapper = new Custom_Model_Mapper_GfNeighborhoodPoi();
			$this->view->neighborhood_pois = $mapper->fetchByNeighborhoodId($neighborhood->id);

			$mapper = new Custom_Model_Mapper_GfUnit();
			$this->view->available_units = $mapper->fetchAvailableUnitsByBuildingId($building->id);

			if ($building->meta_title) {
				$this->view->headTitle()->prepend($building->meta_title);
			}
			if ($building->meta_description) {
				$this->view->headMeta()->setName('description', $building->meta_description);
			}
		} else {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}
	}

	public function unitAction()
	{
		$region_code = $this->getRequest()->getParam('region_code','');
		$building_code = $this->getRequest()->getParam('building_code','');
		$unit_code = $this->getRequest()->getParam('unit_code','');

		$mapper = new Custom_Model_Mapper_GfRegion();
		$this->view->region = $region = $mapper->doesExists(array('code'=>$region_code));

		$mapper = new Custom_Model_Mapper_GfBuilding();
		$this->view->building = $building = $mapper->validateByCodeAndRegionId($building_code, $region->id);

		$mapper = new Custom_Model_Mapper_GfUnit();
		$this->view->unit = $unit = $mapper->validateByCodeAndBuilding($unit_code, $building->id);
		$this->view->similar_units = $mapper->getSimilar($building->id, $unit->id, 2);
		$this->view->unit_concession_message = $mapper->getUnitConcessionMessage($unit->id);

		if ($region && $building && $unit) {
			$mapper = new Custom_Model_Mapper_GfAmenity();
			$this->view->building_amenities = $mapper->fetchAmenityNamesByBuildingId($building->id);

			$mapper = new Custom_Model_Mapper_GfNeighborhood();
			$this->view->neighborhood = $neighborhood = $mapper->find($building->neighborhood_id);

			if ($unit->disable_openhouse == 'Yes') {
				$this->view->open_houses = '';
			}else {
				$mapper = new Custom_Model_Mapper_GfBuildingOpenHouse();
				$this->view->open_houses = $mapper->fetchByBuildingId($building->id);
			}

			$mapper = new Custom_Model_Mapper_GfAttraction();
			$this->view->region_attractions = $mapper->fetchByRegionId($region->id);

			$mapper = new Custom_Model_Mapper_GfNeighborhoodPoi();
			$this->view->neighborhood_pois = $mapper->fetchByNeighborhoodId($neighborhood->id);

			$mapper = new Custom_Model_Mapper_GfBuildingTransportation();
			$this->view->transportations = $mapper->fetchByBuildingId($building->id);

			$mapper = new Custom_Model_Mapper_GfUnitIncentive();
			$this->view->incentive = $mapper->fetchNameByUnitId($unit->id);

			$mapper = new Custom_Model_Mapper_GfFeature();
			$this->view->unit_features = $mapper->fetchFeatureNamesByUnitId($unit->id);

			$mapper = new Custom_Model_Mapper_GfUnitImage();
			$this->view->unit_images = $mapper->fetchOrderedByUnitId($unit->id);

			$mapper = new Custom_Model_Mapper_GfFloorplan();
			$this->view->floorplan = $mapper->find($unit->floorplan_id);

			$mapper = new Custom_Model_Mapper_GfBuildingImage();
			$this->view->building_images = $mapper->fetchOrderedByBuildingId($building->id);

			$mapper = new Custom_Model_Mapper_GfVirtualTour();
			$this->view->unit_virtual_tour = $mapper->getUnit($unit->number);

			if ($unit->meta_title) {
				$this->view->headTitle()->prepend($unit->meta_title);
			}
			if ($unit->meta_description) {
				$this->view->headMeta()->setName('description', $unit->meta_description);
			}
		} else {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}
	}
}

