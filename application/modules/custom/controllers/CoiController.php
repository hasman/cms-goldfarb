<?php
class Custom_CoiController extends Zend_Controller_Action
{

	public function init()
	{

		/* Initialize action controller here */
		$this->view->placeholder('body_class')->append('coi');

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();


	}

	public function indexAction()
	{
        $this->view->placeholder('page_title')->set("COI Document Manager");
        $breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">COI</li>';
        $this->view->placeholder('breadcrumb')->set($breadcrumb);		

		$mapper = new Custom_Model_Mapper_GfCoi();
		$results = $mapper->getAllCoi();
		$this->view->coi = $results;
		
		$mapper = new Custom_Model_Mapper_GfCoiTrades();
		$results = $mapper->getAllCoiTrades();
		$this->view->trades = $results;
		
		$mapper = new Custom_Model_Mapper_GfCoiDocs();
		$results = $mapper->getAllCoiDocs();
		$this->view->docs = $results;
	}
	public  function viewAction()
	{
		$mapper = new Custom_Model_Mapper_GfCoi();
		$results = $mapper->getAllCoi();
		$this->view->coi = $results;
		
		$mapper = new Custom_Model_Mapper_GfCoiTrades();
		$results = $mapper->getAllCoiTrades();
		$this->view->trades = $results;
		
		$mapper = new Custom_Model_Mapper_GfCoiDocs();
		$results = $mapper->getAllCoiDocs();
		$this->view->docs = $results;	
	
	
       $this->view->placeholder('page_title')->set("COI Document Viewer");
        $breadcrumb = '<li><a href="/admin/">Home</a></li><li href="/admin/coi">COI</li><li class="active">Document</li>';
        $this->view->placeholder('breadcrumb')->set($breadcrumb);		
	
	}
}