<?php
use Cocur\Slugify\Slugify;

class Custom_MigrateController extends Zend_Controller_Action
{
	/** @var Zend_Db_Adapter_Mysqli */
	public $_db;

	function init()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();

		$this->_cache_dir = $cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->_backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_db_name = $options['resources']['multidb']['db']['dbname'];
		$this->_old_db_name = "gold_old";
	}

	public function fullSyncAction()
	{
		set_time_limit(120);

		$domain = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$_SERVER['HTTP_HOST'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,"$domain/migrate/tables");
		//$result=curl_exec($ch);
		//echo "blog run";	var_dump($result);


		curl_close($ch);

		echo "done";
		exit;
	}

	public function tablesAction()
	{
		set_time_limit(600);

		$image_dir = "/userFiles/uploads/";
		$dest_dir = realpath(APPLICATION_PATH."/../public".$image_dir);

		$slugify = new Slugify();

		//amenities
		$this->_db->query("TRUNCATE $this->_db_name.gf_amenities;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.amenities WHERE deleted_at IS NULL");
		foreach ($items as $item) {
			$json = json_decode($item['feed_meta'], true);

			$code = $slugify->slugify($item['name']);
			$this->_db->insert("{$this->_db_name}.gf_amenities", array(
				'id' => $item['id'],
				'code' => $code,
				'name' => $item['name'],
				'streeteasy_tag' => (isset($json['streeteasy']) && $json['streeteasy'])?$json['streeteasy']:'',
				'zillow_tag' => (isset($json['zillow']) && $json['zillow'])?$json['zillow']:'',
				'apartmentscom_tag' => (isset($json['apartments-com']) && $json['apartments-com'])?$json['apartments-com']:'',
				'status' => 'Enabled',
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		//regions
		$this->_db->query("TRUNCATE $this->_db_name.gf_regions;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.regions WHERE deleted_at IS NULL");
		foreach ($items as $item) {
			//$code = $slugify->slugify($item['name']);

			$image_path = '';
			if ($item['hero_id']) {
				$image = $this->_db->fetchRow("SELECT * FROM $this->_old_db_name.images WHERE id = '{$item['hero_id']}' ");

				$image_path = $image_dir . $image['path'] . "/" . $image['filename'];
			}

			$this->_db->insert("{$this->_db_name}.gf_regions", array(
				'id' => $item['id'],
				'code' => $item['slug'],
				'name' => $item['name'],
				'sub_title' => $item['description'],
				'phone' => $item['phone'],
				'email' => $item['email'],
				'hours' => $item['hours'],
				'apply_link' => $item['apply_link'],
				'image' => $image_path,
				'sort_order' => $item['weight'],
				'status' => 'Enabled',
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		//neighborhoods
		$this->_db->query("TRUNCATE $this->_db_name.gf_neighborhoods;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.neighborhoods WHERE deleted_at IS NULL");
		foreach ($items as $item) {
			//$code = $slugify->slugify($item['name']);
			$this->_db->insert("{$this->_db_name}.gf_neighborhoods", array(
				'id' => $item['id'],
				'code' => $item['slug'],
				'name' => $item['name'],
				'description' => $item['body'],
				'region_id' => $item['region_id'],
				'sort_order' => $item['weight'],
				'status' => 'Enabled',
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		//point of interest categories
		$this->_db->query("TRUNCATE $this->_db_name.gf_poi_categories;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.point_of_interest_categories WHERE deleted_at IS NULL");
		foreach ($items as $item) {
			$this->_db->insert("{$this->_db_name}.gf_poi_categories", array(
				'id' => $item['id'],
				'name' => $item['name'],
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		//points of interest
		$this->_db->query("TRUNCATE $this->_db_name.gf_pois;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.point_of_interests WHERE deleted_at IS NULL");
		foreach ($items as $item) {
			$this->_db->insert("{$this->_db_name}.gf_pois", array(
				'id' => $item['id'],
				'name' => $item['name'],
				'poi_category_id' => $item['point_of_interest_category_id'],
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		//neighborhood points of interest
		$this->_db->query("TRUNCATE $this->_db_name.gf_neighborhood_pois;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.neighborhood_point_of_interest");
		foreach ($items as $item) {
			$this->_db->insert("{$this->_db_name}.gf_neighborhood_pois", array(
				'neighborhood_id' => $item['neighborhood_id'],
				'poi_id' => $item['point_of_interest_id'],
				'created' => new Zend_Db_Expr("NOW()")
			));
		}

		//attractions
		$this->_db->query("TRUNCATE $this->_db_name.gf_attractions;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.attractions WHERE deleted_at IS NULL");
		foreach ($items as $item) {
			$image_path = '';
			if ($item['image_id']) {
				$image = $this->_db->fetchRow("SELECT * FROM $this->_old_db_name.images WHERE id = '{$item['image_id']}' ");

				$image_path = $image_dir . $image['path'] . "/" . $image['filename'];
			}

			$this->_db->insert("{$this->_db_name}.gf_attractions", array(
				'id' => $item['id'],
				'name' => $item['name'],
				'description' => $item['body'],
				'image' => $image_path,
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		//region attractions
		$this->_db->query("TRUNCATE $this->_db_name.gf_region_attractions;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.attraction_region");
		foreach ($items as $item) {
			$this->_db->insert("{$this->_db_name}.gf_region_attractions", array(
				'attraction_id' => $item['attraction_id'],
				'region_id' => $item['region_id'],
				'created' => new Zend_Db_Expr("NOW()")
			));
		}

		//building transportation
		$this->_db->query("TRUNCATE $this->_db_name.gf_building_transportations;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.transportations");
		foreach ($items as $item) {
			$this->_db->insert("{$this->_db_name}.gf_building_transportations", array(
				'building_id' => $item['building_id'],
				'station_name' => $item['station_name'],
				'station_service' => json_encode(explode(" ",$item['station_service'])),
				'distance_to_station' => trim(str_ireplace("miles","",(stripos($item['distance_to_station'], '500 feet') !== false)?"0.1":$item['distance_to_station'])),
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		//features
		$this->_db->query("TRUNCATE $this->_db_name.gf_features;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.features WHERE deleted_at IS NULL");
		foreach ($items as $item) {
			$json = json_decode($item['feed_meta'], true);

			//$code = $slugify->slugify($item['name']);
			$this->_db->insert("{$this->_db_name}.gf_features", array(
				'id' => $item['id'],
				'name' => $item['name'],
				'streeteasy_tag' => (isset($json['streeteasy']) && $json['streeteasy'])?$json['streeteasy']:'',
				'zillow_tag' => (isset($json['zillow']) && $json['zillow'])?$json['zillow']:'',
				'apartmentscom_tag' => (isset($json['apartments-com']) && $json['apartments-com'])?$json['apartments-com']:'',
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		//incentives
		$this->_db->query("TRUNCATE $this->_db_name.gf_incentives;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.incentives WHERE deleted_at IS NULL");
		foreach ($items as $item) {
			//$code = $slugify->slugify($item['name']);
			$this->_db->insert("{$this->_db_name}.gf_incentives", array(
				'id' => $item['id'],
				'name' => $item['name'],
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		//quick apply / wait unit
		$this->_db->query("TRUNCATE $this->_db_name.gf_quick_apply_units;");
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.region_waitunit");
		foreach ($items as $item) {
			//$code = $slugify->slugify($item['name']);
			$this->_db->insert("{$this->_db_name}.gf_quick_apply_units", array(
				'region_id' => $item['region_id'],
				'unit_id' => $item['unit_id'],
				'bedrooms' => $item['unit_size'],
				'created' => new Zend_Db_Expr("NOW()"),
				'modified' => null
			));
		}

		//audit table

		$sql = "SHOW COLUMNS FROM `gf_audits` LIKE 'user'";
		$result = $this->_db->fetchAll($sql);
		if (count($result) <= 0) {
			$add_user_field = <<<sql
ALTER TABLE {$this->_db_name}.gf_audits
ADD COLUMN user VARCHAR(255) NULL AFTER rentcafe_status
sql;
			$this->_db->query($add_user_field);
		}

		$this->_db->query("TRUNCATE $this->_db_name.gf_audits;");
		$this->_db->query("INSERT INTO $this->_db_name.gf_audits (id, unit_id, `type`, price, net_effective, concession, openhouse, `status`, rentcafe_status, user, created, modified) SELECT id, unit_id, `type`, price, net_effective, concession, openhouse, `status`, rentcafe_status, 'migration', created_at, updated_at FROM $this->_old_db_name.audits WHERE deleted_at IS NULL;");

		echo "done";
		exit;
	}

	public function buildingsAction()
	{
		set_time_limit(600);

		//$this->_db->query("TRUNCATE $this->_db_name.gf_buildings;");
		//$this->_db->query("TRUNCATE $this->_db_name.gf_building_amenities;");
		//$this->_db->query("TRUNCATE $this->_db_name.gf_building_open_houses;");
		$this->_db->query("TRUNCATE $this->_db_name.gf_building_images;");

		$slugify = new Slugify();

		//buildings
		$buildings = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.buildings WHERE deleted_at IS NULL");
		foreach ($buildings as $item) {
			/*
			//get address
			$address = $this->_db->fetchRow($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.addresses WHERE addressable_id = ? AND addressable_type = 'Goldfarb\\\Building' AND deleted_at IS NULL ",$item['id']));

			//get meta data
			$meta = $this->_db->fetchRow($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.meta_data WHERE metadatable_id = ? AND metadatable_type = 'Goldfarb\\\Building' AND deleted_at IS NULL ",$item['id']));

			$commercial = $residential = [];
			//convert commercial and residential flags into array
			if ($item['commercial_office'] == 1) $commercial[] = 'Office';
			if ($item['commercial_retail'] == 1) $commercial[] = 'Retail';
			if ($item['residential_rentals'] == 1) $residential[] = 'Rentals';

			//$code = $slugify->slugify($item['name']);
			$this->_db->insert("$this->_db_name.gf_buildings",array(
				'id' => $item['id'],
				'code' => $item['slug'],
				'name' => $item['name'],
				'neighborhood_id' => $item['neighborhood_id'],
				'description' => $item['body'],
				'commercial' => json_encode($commercial),
				'residential' => json_encode($residential),
				'voyager_code' => $item['voyager_code'],
				'yardi_code' => $item['yardi_code'],
				'active_streeteasy' => $item['active_streeteasy'],
				'address_display_name' => $address['display_name'],
				'cross_street' => $address['cross_street'],
				'street1' => $address['street_1'],
				'street2' => $address['street_2'],
				'city' => $address['city'],
				'state' => $address['state'],
				'zip' => $address['zip'],
				'meta_title' => $meta['title'],
				'meta_description' => $meta['description'],
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));

			//building amenities
			$building_amenities = $this->_db->fetchAll($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.amenity_building WHERE building_id = ? ",$item['id']));
			foreach ($building_amenities as $building_amenity) {
				$this->_db->insert("$this->_db_name.gf_building_amenities", array(
					'building_id' => $building_amenity['building_id'],
					'amenity_id' => $building_amenity['amenity_id'],
					'created' => new Zend_Db_Expr("NOW()")
				));
			}

			//building open houses
			$open_houses = $this->_db->fetchAll($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.open_houses WHERE building_id = ? AND deleted_at IS NULL",$item['id']));
			foreach ($open_houses as $open_house) {
				$this->_db->insert("$this->_db_name.gf_building_open_houses", array(
					'building_id' => $open_house['building_id'],
					'start_time' => $open_house['start_time'],
					'end_time' => $open_house['end_time'],
					'details' => $open_house['details'],
					'created' => $open_house['created_at'],
					'modified' => $open_house['updated_at']
				));
			}
			*/

			//building images
			$images = $this->_db->fetchAll($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.imageables WHERE imageables_id = ? AND imageables_type = 'Goldfarb\\\Building'",$item['id']));
			foreach ($images as $image) {
				$this->_db->insert("$this->_db_name.gf_building_images", array(
					'building_id' => $item['id'],
					'image_id' => $image['image_id'],
					'sort_order' => $image['weight'],
					'created' => new Zend_Db_Expr("NOW()")
				));
			}

		}

		echo "done";
		exit;
	}

	public function unitsAction()
	{
		set_time_limit(600);

		//$this->_db->query("TRUNCATE $this->_db_name.gf_units;");
		$this->_db->query("TRUNCATE $this->_db_name.gf_unit_incentives;");
		$this->_db->query("TRUNCATE $this->_db_name.gf_unit_features;");
		$this->_db->query("TRUNCATE $this->_db_name.gf_unit_images;");

		$slugify = new Slugify();

		//units
		$units = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.units WHERE deleted_at IS NULL");
		foreach ($units as $item) {
			/*
			//get meta data
			$meta = $this->_db->fetchRow($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.meta_data WHERE metadatable_id = ? AND metadatable_type = 'Goldfarb\\\Unit' AND deleted_at IS NULL ",$item['id']));

			$commercial = $residential = [];
			//convert commercial and residential flags into array
			if ($item['type'] == 'commercial_office') $commercial[] = 'Office';
			if ($item['type'] == 'commercial_retail') $commercial[] = 'Retail';
			if ($item['type'] == 'residential_rentals') $residential[] = 'Rentals';

			//$code = $slugify->slugify($item['name']);
			$this->_db->insert("$this->_db_name.gf_units",array(
				'id' => $item['id'],
				'code' => $item['slug'],
				'yardi_code' => $item['yardi_code'],
				'building_id' => $item['building_id'],
				'floorplan_id' => $item['floorplan_id'],
				'commercial' => json_encode($commercial),
				'residential' => json_encode($residential),
				'number' => $item['number'],
				'feed_number_override' => $item['feed_number_override'],
				'show_number' => ($item['show_number'])?"Yes":"No",
				'floor' => $item['floor'],
				'square_footage' => $item['square_footage'],
				'square_footage_status' => $item['square_footage_status'],
				'rooms' => $item['rooms'],
				'bedrooms' => $item['bedrooms'],
				'bathrooms' => $item['bathrooms'],
				'price' => $item['price'],
				'lease_term' => $item['lease_term'],
				'concession_months' => $item['concession_months'],
				'concession_expiration' => ($item['concession_expiration'])?date("Y-m-d",strtotime($item['concession_expiration'])):new Zend_Db_Expr("NULL"),
				'concession_language' => ($item['concession_language'])?str_ireplace("var","",$item['concession_language']):new Zend_Db_Expr("NULL"),
				'streeteasy_status' => (strtolower($item['active_streeteasy']) == 'yes')?"Enabled":"Disabled",
				'disable_openhouse' => (strtolower($item['disable_openhouse']) == 'yes')?"Yes":"No",
				'description' => $item['body'],
				'featured' => ($item['featured'])?"Yes":"No",
				'version' => $item['version'],
				'apply_link' => $item['apply_link'],
				'carousel_vimeo_url' => $item['carousel_vimeo_url'],
				'virtual_tour_id' => $item['virtual_tour_id'],
				'walkin_key' => $item['walkin_key'],
				'meta_title' => $meta['title'],
				'meta_description' => $meta['description'],
				'status' => $item['status'],
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));

			//unit features
			$unit_features = $this->_db->fetchAll($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.feature_unit WHERE unit_id = ? ",$item['id']));
			foreach ($unit_features as $unit_feature) {
				$this->_db->insert("{$this->_db_name}.gf_unit_features", array(
					'unit_id' => $unit_feature['unit_id'],
					'feature_id' => $unit_feature['feature_id'],
					'created' => new Zend_Db_Expr("NOW()")
				));
			}

			//unit incentives
			$unit_incentives = $this->_db->fetchAll($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.incentive_unit WHERE unit_id = ? ",$item['id']));
			foreach ($unit_incentives as $unit_incentive) {
				$this->_db->insert("{$this->_db_name}.gf_unit_incentives", array(
					'unit_id' => $unit_incentive['unit_id'],
					'incentive_id' => $unit_incentive['incentive_id'],
					'created' => new Zend_Db_Expr("NOW()")
				));
			}
			*/

			//get meta data
			$meta = $this->_db->fetchRow($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.meta_data WHERE metadatable_id = ? AND metadatable_type = 'Goldfarb\\\Unit' AND deleted_at IS NULL ",$item['id']));

			$this->_db->update("$this->_db_name.gf_units",array(
				'code' => $item['slug'],
				'yardi_code' => $item['yardi_code'],
				'building_id' => $item['building_id'],
				'floorplan_id' => $item['floorplan_id'],
				'number' => $item['number'],
				'feed_number_override' => $item['feed_number_override'],
				'show_number' => ($item['show_number'])?"Yes":"No",
				'floor' => $item['floor'],
				'square_footage' => $item['square_footage'],
				'square_footage_status' => $item['square_footage_status'],
				'rooms' => $item['rooms'],
				'bedrooms' => $item['bedrooms'],
				'bathrooms' => $item['bathrooms'],
				'price' => $item['price'],
				'lease_term' => $item['lease_term'],
				'concession_months' => $item['concession_months'],
				'concession_expiration' => ($item['concession_expiration'])?date("Y-m-d",strtotime($item['concession_expiration'])):new Zend_Db_Expr("NULL"),
				'concession_language' => ($item['concession_language'])?str_ireplace("var","",$item['concession_language']):new Zend_Db_Expr("NULL"),
				'streeteasy_status' => (strtolower($item['active_streeteasy']) == 'yes')?"Enabled":"Disabled",
				'disable_openhouse' => (strtolower($item['disable_openhouse']) == 'yes')?"Yes":"No",
				'description' => $item['body'],
				'featured' => ($item['featured'])?"Yes":"No",
				'version' => $item['version'],
				'apply_link' => $item['apply_link'],
				'carousel_vimeo_url' => $item['carousel_vimeo_url'],
				'virtual_tour_id' => $item['virtual_tour_id'],
				'walkin_key' => $item['walkin_key'],
				'meta_title' => $meta['title'],
				'meta_description' => $meta['description'],
				'status' => $item['status'],
				'modified' => $item['updated_at']
			),"id = '{$item['id']}' ");

			//unit features
			$unit_features = $this->_db->fetchAll($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.feature_unit WHERE unit_id = ? ",$item['id']));
			foreach ($unit_features as $unit_feature) {
				$this->_db->insert("{$this->_db_name}.gf_unit_features", array(
					'unit_id' => $unit_feature['unit_id'],
					'feature_id' => $unit_feature['feature_id'],
					'created' => new Zend_Db_Expr("NOW()")
				));
			}

			//unit incentives
			$unit_incentives = $this->_db->fetchAll($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.incentive_unit WHERE unit_id = ? ",$item['id']));
			foreach ($unit_incentives as $unit_incentive) {
				$this->_db->insert("{$this->_db_name}.gf_unit_incentives", array(
					'unit_id' => $unit_incentive['unit_id'],
					'incentive_id' => $unit_incentive['incentive_id'],
					'created' => new Zend_Db_Expr("NOW()")
				));
			}


			//unit images
			$images = $this->_db->fetchAll($this->_db->quoteInto("SELECT * FROM $this->_old_db_name.imageables WHERE imageables_id = ? AND imageables_type = 'Goldfarb\\\Unit'",$item['id']));
			foreach ($images as $image) {
				$this->_db->insert("$this->_db_name.gf_unit_images", array(
					'unit_id' => $item['id'],
					'image_id' => $image['image_id'],
					'sort_order' => $image['weight'],
					'created' => new Zend_Db_Expr("NOW()")
				));
			}

			$this->_db->update("$this->_db_name.gf_units",array(
				'floorplan_id' => $item['floorplan_id'],
			),"id = '{$item['id']}'");
		}

		echo "done";
		exit;
	}

	public function mediaAction()
	{
		set_time_limit(900);
		ini_set('memory_limit','512M');

		$this->_db->query("TRUNCATE $this->_db_name.gf_images;");
		$this->_db->query("TRUNCATE $this->_db_name.gf_image_tags;");
		$this->_db->query("TRUNCATE $this->_db_name.gf_tags;");

		$slugify = new Slugify();

		$image_mapper = new Custom_Model_Mapper_GfImage();
		$it_mapper = new Custom_Model_Mapper_GfImageTag();
		$tag_mapper = new Custom_Model_Mapper_GfTag();

		//image gallery data
		$sql = "SELECT * FROM $this->_old_db_name.images WHERE deleted_at IS NULL";
		$images = $this->_db->fetchAll($sql);

		$image_dir = "/userFiles/uploads/";
		$source_dir = realpath(APPLICATION_PATH . "/../old_media/");
		$dest_dir = realpath(APPLICATION_PATH."/../public".$image_dir);
		$thumb_dir = realpath(APPLICATION_PATH . "/../public/userThumbs/");

		foreach ($images as $image) {
			$dir = $image_dir.$image['path'].'/';

			if (!is_dir($dest_dir."/".$image['path'])) {
				mkdir($dest_dir."/".$image['path']);
			}
			if (!is_dir($thumb_dir."/".$image['path'])) {
				mkdir($thumb_dir."/".$image['path']);
			}

			if ($image['filename']) {
				$this->_db->insert("gf_images",array(
					"id" => $image['id'],
					"title" => $image['title'],
					"alt_text" => $image['alt'],
					"image" => $dir.str_ireplace(array(" "),array("_"),$image['filename']),
					"created" => $image['created_at'],
					"modified" => $image['updated_at'],
				));
				$image_id = $this->_db->lastInsertId();

				//tags
				$tags = explode(",",$image['tags']);
				foreach ($tags as $tag) {
					$tag = trim($tag);

					if (!$tag) continue;

					$code = $orig_code = $slugify->slugify($tag);
					if ($tag_row = $tag_mapper->doesExists(array('code' => $code), '')) {
						$tag_id = $tag_row->id;
					}else {
						$tag_id = $tag_mapper->save(new Custom_Model_GfTag(array(
							'code' => $code,
							'title' => $tag,
							'created' => new Zend_Db_Expr("NOW()")
						)));
					}

					$it_mapper->save(new Custom_Model_GfImageTag(array(
						'image_id' => $image_id,
						'tag_id' => $tag_id,
						'created' => new Zend_Db_Expr("NOW()")
					)));
				}

				if (!file_exists($dest_dir . "/" . $image['path'] . "/" . str_ireplace(array(" "), array("_"), $image['filename']))) {
					copy($source_dir . "/" . $image['path'] . "/" . $image['filename'], $dest_dir . "/" . $image['path'] . "/" . strtolower(str_ireplace(array(" "), array("_"), $image['filename'])));
				}
				if (!file_exists($thumb_dir . "/" . $image['path'] . "/" . str_ireplace(array(" "), array("_"), $image['filename']))) {
					$thumb = $this->resizeImage($source_dir . "/" . $image['path'] . "/" . $image['filename'], 800, 800, $thumb_dir . "/" . $image['path'] . "/" . strtolower(str_ireplace(array(" "), array("_"), $image['filename'])));
				}
			}
		}

		$this->_db->query("UPDATE $this->_db_name.gf_images SET image = REPLACE(image, '.JPG', '.jpg'); ");

		echo "done";
		exit;
	}

	public function floorplansAction()
	{
		set_time_limit(900);
		ini_set('memory_limit','512M');

		$this->_db->query("TRUNCATE $this->_db_name.gf_floorplans;");

		$slugify = new Slugify();

		$image_dir = "/userFiles/uploads/floorplans";
		$source_dir = realpath(APPLICATION_PATH . "/../old_media/floorplans/");
		$dest_dir = realpath(APPLICATION_PATH."/../public".$image_dir);
		$thumb_dir = realpath(APPLICATION_PATH . "/../public/userThumbs/floorplans/");

		if (!is_dir($dest_dir)) {
			mkdir($dest_dir);
		}
		if (!is_dir($thumb_dir)) {
			mkdir($thumb_dir);
		}

		//floorplans
		$items = $this->_db->fetchAll("SELECT * FROM $this->_old_db_name.floorplans WHERE deleted_at IS NULL");
		foreach ($items as $item) {
			$image_path = '';
			if ($item['filename']) {
				if (!is_dir($dest_dir . "/" . $item['path'])) {
					mkdir($dest_dir . "/" . $item['path']);
				}
				if (!is_dir($thumb_dir . "/" . $item['path'])) {
					mkdir($thumb_dir . "/" . $item['path']);
				}

				$image_path = $image_dir . "/" . $item['path'] . "/" . strtolower(str_ireplace(array(" "), array("_"),$item['filename']));

				if (!file_exists($dest_dir . "/" . $item['path'] . "/" . str_ireplace(array(" "), array("_"), $item['filename']))) {
					copy($source_dir . "/" . $item['path'] . "/" . $item['filename'], $dest_dir . "/" . $item['path'] . "/" . strtolower(str_ireplace(array(" "), array("_"), $item['filename'])));
				}
				if (!file_exists($thumb_dir . "/" . $item['path'] . "/" . str_ireplace(array(" "), array("_"), $item['filename']))) {
					$thumb = $this->resizeImage($source_dir . "/" . $item['path'] . "/" . $item['filename'], 800, 800, $thumb_dir . "/" . $item['path'] . "/" . strtolower(str_ireplace(array(" "), array("_"), $item['filename'])));
				}
			}

			$this->_db->insert("{$this->_db_name}.gf_floorplans", array(
				'id' => $item['id'],
				'title' => $item['title'],
				'building_id' => $item['building_id'],
				'image' => $image_path,
				'created' => $item['created_at'],
				'modified' => $item['updated_at']
			));
		}

		echo "done";
		exit;
	}

	function resizeImage($filename,$max_width,$max_height='',$newfilename="",$withSampling=true)
	{
		if($newfilename=="")
			$newfilename=$filename;
		// Get new sizes
		list($width, $height) = getimagesize($filename);

		/*
		//-- dont resize if the width of the image is smaller or equal than the new size.
		if($width<=$max_width)
			$max_width=$width;
		*/

		$percent = $max_width/$width;

		$newwidth = $width * $percent;
		if($max_height=='') {
			$newheight = round($height * $percent, 0);
		} else
			$newheight = $max_height;

		// Load
		$thumb = imagecreatetruecolor($newwidth, $newheight);
		$ext = strtolower($this->getExtension($filename));

		if($ext=='jpg' || $ext=='jpeg')
			$source = imagecreatefromjpeg($filename);
		if($ext=='gif')
			$source = imagecreatefromgif($filename);
		if($ext=='png')
			$source = imagecreatefrompng($filename);

		$bg_color = imagecolorallocate ($thumb, 255, 255, 255);
		imagefill($thumb, 0, 0, $bg_color);

		if($height >= $width){
			// Portrait, i.e. tall image
			$newwidth=intval($max_width*$width/$height);
			// Resize and composite original image onto output canvas
			imagecopyresampled(
				$thumb, $source,
				intval(($max_width-$newwidth)/2),0,
				0,0,
				$newwidth, $newheight,
				$width, $height);
		} else {
			// Landscape, i.e. wide image
			$newheight=intval($max_width*$height/$width);
			imagecopyresampled(
				$thumb, $source,
				0,intval(($max_width-$newheight)/2),
				0,0,
				$newwidth, $newheight,
				$width, $height);
		}

		/*
		// Resize
		if($withSampling)
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		else
			imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		*/

		// Output
		if($ext=='jpg' || $ext=='jpeg')
			return imagejpeg($thumb,$newfilename);
		if($ext=='gif')
			return imagegif($thumb,$newfilename);
		if($ext=='png')
			return imagepng($thumb,$newfilename);

	}

	function getExtension($file)
	{
		if (preg_match("/.+[\.](.*)$/", $file, $matches))
		{
			if (strlen($matches[1]) > 0)
				return $matches[1];
		}
		return ''; // no ext
	}

	public function auditAction()
	{
		//audit table
		$this->_db->query("TRUNCATE $this->_db_name.gf_audits;");
		$this->_db->query("INSERT INTO $this->_db_name.gf_audits (id, unit_id, `type`, price, net_effective, concession, openhouse, `status`, rentcafe_status, created, modified) SELECT id, unit_id, `type`, price, net_effective, concession, openhouse, `status`, rentcafe_status, created_at, updated_at FROM $this->_old_db_name.audits WHERE deleted_at IS NULL;");

		echo "done";
		exit;
	}

	public function testAction()
	{

		$_db = Zend_Db_Table::getDefaultAdapter();
		$options = Zend_Registry::get('configuration')->toArray();

		$domain = (APPLICATION_ENV == 'production')?"https://goldfarbproperties.com":"https://staging-goldfarb.cnyawscloud2.com";

		$media_cache = new Cny_View_Helper_MediaCache();

		$sql = "SELECT u.*, b.name AS building, b.code AS building_code, r.code AS region_code
	FROM gf_audits AS a
	LEFT JOIN gf_units AS u ON a.unit_id = u.id
	LEFT JOIN gf_buildings AS b ON u.building_id = b.id
	LEFT JOIN gf_neighborhoods AS n ON b.neighborhood_id = n.id
	LEFT JOIN gf_regions AS r ON n.region_id = r.id
	WHERE (
	TRIM(REPLACE(a.price,'$','')) <> TRIM(u.price) OR 
	(u.status = 'Available' AND a.status IN ('unavailable','occupied')) 
	)
	#AND u.status = 'Available'
	AND a.created >= DATE_SUB(NOW(), INTERVAL 5 DAY) 
	GROUP BY u.id";
		$units = $_db->fetchAll($sql);

		if ($units) {
			$body = [];

			$unit_mapper = new Custom_Model_Mapper_GfUnit();
			$unit_image_mapper = new Custom_Model_Mapper_GfUnitImage();

			foreach ($units as $unit) {
				$unit_image = $unit_image_mapper->fetchPrimaryByUnitId($unit['id']);
				$effective_price = $unit_mapper->getUnitNetEffectivePrice($unit['id']);

				$bedrooms = ($unit['bedrooms'])?"{$unit['bedrooms']} Bed":"Studio";
				$bathrooms = ($unit['bathrooms'])?"{$unit['bathrooms']} Bath":"";

				$price = "$".number_format($effective_price,0);
				if ($effective_price < $unit['price']) {
					$price .= "<sup>*</sup>";
				}

				$unit_url = "$domain/rentals/".$unit['region_code']."/".$unit['building_code']."/apartment-".$unit['code'];

				$body[] = "<tr>
                    <td class=\"location-container__image-cell\">
                       <img src=\"".$domain."/". $media_cache->getImage($unit_image['image'], 'unit-tile-mobile') ."\">
                    </td>
                </tr>
                <tr class=\"location-container__specific-detail-container\">
                    <td>
                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"location-container__specific-detail-table\">
                            <tr class=\"location-container--border\">
                                <td  class=\"location-container__location-name\">
                                    ".$unit['building']." <br>#".$unit['number']."
                                </td>
                                <td class=\"location-container__apartment-details\">
                                    ".$bedrooms."<br>".$bathrooms."
                                </td>
                                <td class=\"location-container__price\">
                                    ".$price."
                                </td>
                            </tr>
                            <tr>
                                <td class=\"location-container__view-unit-container\" colspan=\"3\">
                                    <a class=\"location-container__view-unit-link\" href=\"".$unit_url."\" ><span style=\"color:#d8b256;\">View Unit</span></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>";
			}

			$mailbody = "
	<style type=\"text/css\">
        table{
        border-collapse:collapse;
        }
        .body-container{
        background-color: #e6e6e6;
        font-family: Helvetica, sans-serif;
        margin: 0 auto !important;
        padding: 0;
        width: 100%;
        -webkit-text-size-adjust:100%;
        -ms-text-size-adjust:100%;
        }
        .container-table{
        max-width:600px;
        }
        .container-table__cell{
        padding: 10px 0 30px 0;
        max-width:600px;
        }
        .email-body-container{
        margin:0 auto;
        }
        .email-body-container__logo{
        padding: 40px 0;
        }
        .email-content-container__listing-container{
        padding-top:20px;
        }
        .email-content-container__listing-header{
        background-color: #ffffff;
        border-bottom: 1px solid #ddd8d3;
        font-size: 15px;
        font-weight: 100;
        letter-spacing: 7px;
        padding: 20px;
        text-align: center;
        text-transform: uppercase;
        line-height: 1.7;
        }
        .email-content-container__location-content{
        background-color: #f9f9f9;
        padding: 15px;
        }
        .location-container__borough-cell{
        background-color: #ffffff;
        font-size: 13px;
        padding: 10px 0px 10px 10px;
        text-transform: uppercase;
        text-align:left;
        }
        .location-container__spacer{
        height: 25px;
        }
        .location-container__specific-detail-table td{
        border: 1px solid #ddd8d3;
        }
        .location-container__creation-cell{
        background-color: #ffffff;
        color: #aaaaaa;
        font-size: small;
        font-weight: 100;
        padding: 10px 8px 10px 1px;
        text-align: right;
        }
        .location-container__image{
        width: 100%;
        height: auto;
        display: block;
        }
        .location-container__location-name{
        font-size: 15px;
        font-weight: 100;
        padding:10px;
        padding-left: 5px;
        text-align: center;
        }
        .location-container__apartment-details{
        font-size: 15px;
        font-weight: 100;
        padding: 15px;
        text-align: center;
        }
        .location-container__price{
        font-family: Helvetica;
        font-size: 15px;
        font-weight: 100;
        padding: 15px;
        text-align: center;
        }
        .location-container__address-information{
        padding: 15px 10px 10px 10px;
        /*border: 0;*/
        font-weight: 200;
        font-size: 14px;
        }
        .location-container__specific-detail-container{
        margin-bottom: 15px;
        }
        .location-container__apartment-description{
        padding: 15px 10px 15px 10px;
        font-weight: 100;
        font-size: 15px;
        }
        .location-container__view-unit-container{
        letter-spacing: 5px;
        padding-bottom: 20px;
        padding-top: 20px;
        text-align: center;
        }
        .location-container__view-unit-link{
        border: 2px solid #d8b256;
        color: #d8b256;
        padding: 10px 20% 10px 20%;
        text-decoration: none;
        text-transform: uppercase;
        width: 100%;
        }
        .footer-container__social-media-icons-container{
        padding: 20px;
        text-align: center;
        }
        .footer-container__social-media-icons-container a{
        padding: 0  20px;
        }
        .footer-container__dashboard-link-container{
        padding: 0 0 15px 0;
        text-align: center;
        }
        .footer-container__visit-dashboard-link{
        color: #aaaaaa;
        font-size: 15px;
        font-weight: 100;
        text-decoration: none;
        }
        .footer-container__view-online-container{
        text-align: center;
        padding :5px;
        }
        .footer-container__view-online-link{
        color: #aaaaaa;
        font-size: 15px;
        font-weight: 100;
        text-decoration: none;
        }
        .footer-container__copyright-container{
        padding-bottom: 20px;
        width: 100%;
        }
        .footer-container__copyright{
        color: #aaaaaa;
        font-size: 15px;
        font-weight: 100;
        text-align: center;
        }
        @media screen and (min-device-width: 482px) and (max-device-width: 600px) {
        table[class=\"container-table\"]{
        width:100% !important;
        }
        body{
        width:100% !important;
        }
        }
        @media screen and (max-device-width: 481px) {
        body{
        width:100% !important;
        }
        .email-content-container__listing-header{
        letter-spacing: 1px !important;
        max-width:600px !important;
        }
        .location-container__price, .location-container__apartment-details, .location-container__location-name {
        border: 0 !important;
        margin: auto !important;
        padding-bottom: 5px !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
        padding-top: 5px !important;
        text-align: center !important;
        width: 100% !important;
        }
        .location-container__address-information{
        border-top: 0;
        }
        .location-container__specific-detail-table{
        width: 100% !important;
        }
        .location-container__borough-cell{
        text-align: center !important;
        padding: 5px !important;
        }
        .location-container__creation-cell{
        text-align: center !important;
        padding: 5px !important;
        }
        .location-container__view-unit-link{
        padding: 10px 10% 10px 10% !important;
        font-size: small !important;
        font-weight: bold !important;
        }
        .footer-container__dashboard-link-container{
        padding-right: 5px !important;
        }
        .footer-container__view-online-link, .footer-container__visit-dashboard-link{
        font-size:13px !important;
        }
        </style>
        <!--[if gte mso 9]>
        <style type=\"text/css\">
        .location-container__address-information,
        .email-content-container__listing-header,
        .location-container__borough-cell,
        .location-container__creation-cell,
        .location-container__location-name,
        .location-container__price,
        .location-container__apartment-details,
        .location-container__apartment-description,
        .location-container__view-unit-link,
        .footer-container__visit-dashboard-link,
        .footer-container__view-online-link,
        .footer-container__unsubscribe-link,
        .footer-container__copyright{
        font-family: Helvetica, sans-serif;
        }
        </style>
         <![endif]-->
	";

			$mailbody .= " <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"  width=\"100%\">
                    <tr class=\"email-content-container__listing-container\">
                        <td class=\"email-content-container__listing-header\">
                            Yesterday's Updates <br> ".date('l, F j, Y', strtotime("yesterday"))."
                        </td>
                    </tr>";
			$mailbody .= implode("", $body);
			$mailbody .= "</table>";

			if (APPLICATION_ENV == 'production') {
				$subject = 'Website Updated Units';

				$email_address = 'marketing@goldprop.com';
			} else {
				$subject = 'STAGING :: Website Updated Units';

				$email_address = 'craig@cyber-ny.com';
				//$email_address = 'marketing@goldprop.com';
echo "craig";
				//exit;
			}

			$emails = new Application_Model_Emails();
			$emails->generic($mailbody, $email_address, $subject);
		}

		echo "done";exit;
	}
}
