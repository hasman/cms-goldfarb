<?php
class Custom_HomeFeedController extends Zend_Controller_Action
{

	public function init()
	{

		/* Initialize action controller here */
		$this->view->placeholder('body_class')->append('home-feed');

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
	}

	public function indexAction()
	{

	}

	public function timeLineAction()
	{

	}
}

