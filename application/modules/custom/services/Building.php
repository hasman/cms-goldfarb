<?php
class Custom_Service_Building
{
	/**
	 * Generates a list of building with nested amenities, neighborhood and region data
	 *
	 * @param  bool  $include_yardi_data Will provide data from the Yardi webservice
	 * @return array
	 */
	static function fetchNestedBuildingData($include_yardi_data = false)
	{
		$output = [];

		if ($include_yardi_data) {
			$yardi = new Custom_Model_Yardi();
		}

		$db = Zend_Db_Table::getDefaultAdapter();
		$sql = "SELECT b.*, GROUP_CONCAT(DISTINCT a.name SEPARATOR '|') as _amenities, 
					n.id as '-neighborhood-id-', n.code as '-neighborhood-code-', n.name as '-neighborhood-name-', 
					n.description as '-neighborhood-description-',
					r.id as '-region-id-', r.code as '-region-code-', r.name as '-region-name-', 
					r.sub_title as '-region-sub_title-', r.image as '-region-image-', r.phone as '-region-phone-', 
       				r.email as '-region-email-' 
				FROM gf_buildings b
					LEFT JOIN gf_neighborhoods n ON b.neighborhood_id = n.id
					LEFT JOIN gf_regions r ON n.region_id = r.id
					LEFT JOIN gf_building_amenities ba ON b.id = ba.building_id
					INNER JOIN gf_amenities a on ba.amenity_id = a.id
				WHERE b.residential LIKE '%\"Rentals\"%' AND b.yardi_code IS NOT NULL AND b.yardi_code != ''
				GROUP BY b.id";
		/** @var stdClass $result */
		$result = $db->fetchAll($sql, [], Zend_Db::FETCH_OBJ);
		foreach ($result as $building) {
			//region Process amenities
			if (!empty($building->_amenities)) {
				$building->amenities = explode('|', $building->_amenities);
			}
			unset($building->_amenities);
			//endregion

			//region process objects
			$regex = '/^-([^\-]*)-(.*)-$/m';
			foreach ($building as $property => $value) {
				preg_match($regex, $property, $matches, PREG_UNMATCHED_AS_NULL, 0);
				if (!empty($matches) && count($matches) == 3) {
					list(, $object_name, $object_prop) = $matches;
					if (!empty($object_name) && !empty($object_prop)) {
						if (!isset($building->$object_name)) {
							$building->$object_name = new stdClass();
						}
						$building->$object_name->$object_prop = $value;
						unset($building->$property);
					}
				}
			}
			//endregion

			//region fetch Yardi data
			if ($include_yardi_data) {
				$properties = explode(',', $building->yardi_code);
				foreach ($properties as $propertyCode) {
					// @todo: Need to validate this code. Migrated as is but in for loop it will overwrite previous entry
					$data = $yardi->getAvailability(trim($propertyCode));
					$building->yardi_data = $data;
				}
			}
			//endregion

			$output[] = $building;
		}
		return $output;
	}
}
