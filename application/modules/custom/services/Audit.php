<?php
class Custom_Service_Audit
{
	static public $lastErrors = [];
	static public $headerNames = [
		"unit_id", "type", "status", "rentcafe_status", "price", "net_effective", "concession", "openhouse",
	];

	/**
	 * @param $unit_id
	 * @return void
	 */
	static function recordUnitActivity($unit_id, $user = 'unknown', $type = 'goldfarb', $rentcafe_status = 'Not Applicable')
	{
		try {
			$unit_mapper = new Custom_Model_Mapper_GfUnit();
			/** @var Custom_Model_GfUnit $unit */
			$unit = $unit_mapper->find($unit_id);
			if (!$unit) {
				return;
			}
			$audit_mapper = new Custom_Model_Mapper_GfAudit();
			$audit = new Custom_Model_GfAudit();
			$audit->unit_id = $unit->id;
			$audit->type = $type;
			$audit->price = money_format('$%i', $unit->price);
			$audit->net_effective = money_format('$%i', $unit_mapper->getUnitNetEffectivePrice($unit));
			$audit->concession = ($unit->concession_language)
				? ucwords(substr_replace($unit->concession_language, ' ', 3, 0))
				: 'Not Set';
			$today = date("Y-m-d");
			$openhouse = 'No';
			if (!empty($openhouses)) {
				foreach ($openhouses as $open) {
					$start = strtotime($open->start_time);
					$startdate = date("Y-m-d", $start);
					if ((string) $today == (string) $startdate) {
						if (strtolower($unit->disable_openhouse) != "yes") {
							$openhouse = 'Yes';
						}
					}
				}
			}
			$audit->openhouse = $openhouse;
			$audit->status = $unit->status;
			$audit->rentcafe_status = $rentcafe_status;
			$audit->user = $user;
			$audit_mapper->save($audit);
		} catch (Exception $e) {
			return;
		}
	}

	static function goldfarb()
	{
		$output = [];

		$buildings_mapper = new Custom_Model_Mapper_GfBuilding();
		$openhouses_mapper = new Custom_Model_Mapper_GfBuildingOpenHouse();
		$unit_mapper = new Custom_Model_Mapper_GfUnit();

		$buildings = $buildings_mapper->getAllResidentialRentals();
		if (!$buildings) {
			return $output;
		}
		foreach ($buildings as $building) {
			$openhouses = $openhouses_mapper->fetchByBuildingId($building->id);
			$units = $unit_mapper->fetchUnitsByBuildingId($building->id);
			foreach ($units as $unit) {
				$line = [];
				foreach (self::$headerNames as $header) {
					switch ($header) {
						case 'unit_id':
							$line[$header] = $unit->id;
							break;
						case 'type':
							$line[$header] = 'goldfarb';
							break;
						case 'status':
							$line[$header] = $unit->status;
							break;
						case 'rentcafe_status':
							$line[$header] = 'Not Applicable';
							break;
						case 'price':
							$line[$header] = money_format('$%i', $unit->price);
							break;
						case 'net_effective':
							$line[$header] = money_format('$%i', $unit_mapper->getUnitNetEffectivePrice($unit));
							break;
						case 'concession':
							$line[$header] = ($unit->concession_language)
								? ucwords(substr_replace($unit->concession_language, ' ', 3, 0))
								: 'Not Set';
							break;
						case 'openhouse':
							$today = date("Y-m-d");
							$openhouse = 'No';
							if (!empty($openhouses)) {
								foreach ($openhouses as $open) {
									$start = strtotime($open->start_time);
									$end = strtotime($open->end_time);
									$startdate = date("Y-m-d", $start);
									$starttime = date("H:i", $start);
									$endtime = date("H:i", $end);
									if((string) $today == (string) $startdate) {
										if(strtolower($unit->disable_openhouse) != "yes") {
											$openhouse = 'Yes';
										}
									}
								}
							}
							$line[$header] = $openhouse;
							break;
						default:
							if (isset($detail->$header)) {
								$line[$header] = $unit->$header;
							} else {
								self::$lastErrors[] = "Property {$header} does not exists in the 'units'. [".__LINE__."]";
								continue 2;
							}
					}
				}
				if (!empty($line)) {
					$output[] = $line;
				}
			}
		}
		//endregion
		return $output;
	}
}
