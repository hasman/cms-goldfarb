<?php
/**
 * @OA\Info(
 *     title="Logic CMS API",
 *     version="0.1",
 *     @OA\Contact(
 *         email="support@cyber-ny.com"
 *     )
 * )
 */
class Api_Bootstrap extends Zend_Application_Module_Bootstrap
{
	protected function _initApiAutoload()
	{
		$autoloader = new Zend_Application_Module_Autoloader(
			array(
				'namespace' => 'Api_',
				'basePath'  => dirname(__FILE__),
			)
		);
		return $autoloader;
	}
}
