<?php
class Api_Model_Mapper_ApiKey extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Api_Model_DbTable_ApiKey');
		$this->setEntity('Api_Model_ApiKey');
	}

	public function validateKey($key, $permission = null)
	{
		$select = $this->getDbTable()->select()
			->where("api_key = ?", $key)
			->where("status = ?", "Enabled")
		;
		$result = $this->getDbTable()->fetchRow($select);

		if (!$result) return false;

		if ($permission) {
			$permissions = json_decode($result->permissions);

			if (!in_array($permission, $permissions)) return false;
		}

		return true;
	}
}
