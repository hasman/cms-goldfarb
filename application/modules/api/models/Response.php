<?php
class Api_Model_Response
{
	const STATUS_OK = 'success';
	const STATUS_ERROR = 'error';

	/** @var string error|success are the only valid valid */
	var $status;
	/** @var string Response String */
	var $message;
	/** @var mixed Data to return to the caller */
	var $data;

	/**
	 * Api_Model_Response constructor.
	 * @param array $options
	 * @throws Exception
	 */
	public function __construct($options)
	{
		if (is_array($options)) {
			if (!empty($options['status'])) {
				$this->setStatus($options['status']);
			}
			if (!empty($options['message'])) {
				$this->setMessage($options['message']);
			}
			if (!empty($options['data'])) {
				$this->setData($options['data']);
			}
		}
	}


	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param int $status
	 * @return Api_Model_Response
	 * @throws Exception
	 */
	public function setStatus($status)
	{
		if (!in_array($status, [self::STATUS_OK, self::STATUS_ERROR])) {
			throw new Exception("Incorrect status [{$status}]", 500);
		}
		$this->status = $status;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param string $message
	 * @return Api_Model_Response
	 */
	public function setMessage($message)
	{
		$this->message = $message;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @param mixed $data
	 * @return Api_Model_Response
	 */
	public function setData($data)
	{
		$this->data = $data;
		return $this;
	}

	/**
	 * @return false|string
	 */
	public function __toString()
	{
		return json_encode(get_object_vars($this));
	}
}
