<?php
class Api_VideosController extends Api_Controller
{
	var $_s3;

	/**
	 * @OA\Get(
	 *     path="/api/videos",
	 *     summary="Returns a list of videos",
	 *     @OA\Response(
	 *         response="200",
	 *         description="A JSON array of videos"
	 *     ),
	 *     @OA\Response(
	 *         response="404",
	 *         description="Videos not found"
	 *     ),
	 *     @OA\Response(
	 *         response="422",
	 *         description="Method not allowed"
	 *     ),
	 * )
	 */
	public function indexAction()
	{
		if( $this->getRequest()->isGet() ) {
			$per_page = $this->getRequest()->getParam('per_page',10);
			$page = $this->getRequest()->getParam('page',1);

			$id = $this->getRequest()->getParam('id','');
			$q = $this->getRequest()->getParam('q','');
			$sort_by = $this->getRequest()->getParam('sort_by','id');
			$sort_order = $this->getRequest()->getParam('sort_order','ASC');

			$search_keys = array(
				'id' => $id,
				'keyword' => $q,
			);
			$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
			$results = $mapper->listSearch($sort_by, $sort_order, $search_keys);

			$paginator = Zend_Paginator::factory($results);
			$paginator->setCurrentPageNumber($page);
			$paginator->setItemCountPerPage($per_page);

			if ($paginator) {
				$video_data = [];
				foreach ($paginator as $result) {
					$video_data[] = $this->getVideoData($result['id']);
				}
				$this->_result = new Api_Model_Response([
					'status' => Api_Model_Response::STATUS_OK,
					'data' => [
						'response'=> $video_data,
						'pagination' => [
							'current' => $page,
							'per_page' => $per_page,
							'pages' => (ceil($paginator->getTotalItemCount() / $per_page)),
							'total_results' => $paginator->getTotalItemCount()
						],
					],
				]);
			}else {
				$this->_header_code = 404;
				$this->_result = new Api_Model_Response([
					'status' => Api_Model_Response::STATUS_ERROR,
					'message' => 'Video not allowed',
				]);
			}
		}else {
			$this->_header_code = 422;
			$this->_result = new Api_Model_Response([
				'status' => Api_Model_Response::STATUS_ERROR,
				'message' => 'Method not allowed',
			]);
		}
	}

	// Handle GET and return a single video
	/**
	 * @OA\Get(
	 *     path="/api/videos/view/{id}",
	 *     @OA\Parameter(
	 *         name="id",
	 *         in="path",
	 *         required=true,
	 *         description="Application identifier for the video resource",
     *     ),
	 *     @OA\Response(
	 *         response="200",
	 *         description="Returns video data"
	 *     ),
	 *     @OA\Response(
	 *         response="404",
	 *         description="Video not found"
	 *     ),
	 *     @OA\Response(
	 *         response="422",
	 *         description="ID not present"
	 *     ),
	 * )
	 */
	public function viewAction()
	{
		if( $this->getRequest()->isGet() ) {
			if ($id = $this->getRequest()->getParam('id',0)) {
				$mapper = new Application_Model_Mapper_VideoPlatformVideo();
				$video = $mapper->find($id);

				if ($video) {
					$this->_result = new Api_Model_Response([
						'status' => Api_Model_Response::STATUS_OK,
						'data' => $this->getVideoData($video),
					]);
				}else {
					$this->_header_code = 404;
					$this->_result = new Api_Model_Response([
						'status' => Api_Model_Response::STATUS_ERROR,
						'message' => 'Video not found',
					]);
				}
			}else {
				$this->_header_code = 422;
				$this->_result = new Api_Model_Response([
					'status' => Api_Model_Response::STATUS_ERROR,
					'message' => 'ID not present',
				]);
			}
		}else {
			$this->_header_code = 422;
			$this->_result = new Api_Model_Response([
				'status' => Api_Model_Response::STATUS_ERROR,
				'message' => 'Method not allowed',
			]);
		}
	}

	// Handle PUT and update a video
	/**
	 * @OA\Put(
	 *     path="/api/videos/update",
	 *     @OA\Response(
	 *         response="200",
	 *         description="Updates video data"
	 *     ),
	 *     @OA\Response(
	 *         response="422",
	 *         description="Method not allowed"
	 *     ),
	 * )
	 */
	public function updateAction()
	{
		if( $this->getRequest()->isPut() ) {

		}else {
			$this->_header_code = 422;
			$this->_result = new Api_Model_Response([
				'status' => Api_Model_Response::STATUS_ERROR,
				'message' => 'Method not allowed',
			]);
		}
	}

	// Handle DELETE and delete a video
	/**
	 * @OA\Delete(
	 *     path="/api/videos/delete",
	 *     @OA\Response(
	 *         response="200",
	 *         description="Delete video data"
	 *     ),
	 *     @OA\Response(
	 *         response="422",
	 *         description="Method not allowed"
	 *     ),
	 * )
	 */
	public function deleteAction()
	{
		if( $this->getRequest()->isDelete() ) {

		}else {
			$this->_header_code = 422;
			$this->_result = new Api_Model_Response([
				'status' => Api_Model_Response::STATUS_ERROR,
				'message' => 'Method not allowed',
			]);
		}
	}

	// Handle POST and create a video
	/**
	 * @OA\Post(
	 *     path="/api/videos/create",
	 *     @OA\Response(
	 *         response="200",
	 *         description="Creates video data"
	 *     ),
	 *     @OA\Response(
	 *         response="422",
	 *         description="Method not allowed"
	 *     ),
	 * )
	 */
	public function createAction()
	{
		if( $this->getRequest()->isPost() ) {

		}else {
			$this->_header_code = 422;
			$this->_result = new Api_Model_Response([
				'status' => Api_Model_Response::STATUS_ERROR,
				'message' => 'Method not allowed',
			]);
		}
	}

	private function getVideoData($video)
	{
		if (is_numeric($video)) {
			$mapper = new Application_Model_Mapper_VideoPlatformVideo();
			$video = $mapper->find($video);
		}

		//monetization settings
		$mapper = new Application_Model_Mapper_VideoPlatformVideoMonetization();
		$monetization = $mapper->fetchByVideoId($video->id);

		//image gallery
		$mapper = new Application_Model_Mapper_VideoPlatformVideoImage();
		$items = $mapper->fetchByVideoId($video->id);
		$images = array();
		foreach ($items as $item) {
			if ($item->image_src) {
				$images[] = array(
					'title' => $item->title,
					'caption' => $item->caption,
					'image_url' => $this->_full_url . $item->image_src,
					'sort_order' => $item->sort_order,
				);
			}
		}

		//trailer
		$trailer_url = '';
		$mapper = new Application_Model_Mapper_VideoPlatformVideoTrailer();
		$trailer = $mapper->fetchByVideoId($video->id);

		if ($trailer) {
			//get streams (transcoded files)
			$mapper = new Application_Model_Mapper_VideoPlatformVideoTrailerStream();
			$streams = $mapper->fetchByTrailerId($trailer->id);

			if ($streams) {
				$this->_s3 = new Application_Model_AmazonS3(); //only load where needed because the library has a high overhead
				//how do we know which stream we want to use? For now hard coding a priority list
				foreach ($streams as $stream) {
					if ($stream->format_name == 'HLS') {
						$trailer_url = $this->_s3->getVideoTrailerStreamURL($stream->id, $trailer->id);
					}
				}
			}
		}

		//cast & crew
		$mapper = new Application_Model_Mapper_VideoPlatformVideoCast();
		$items = $mapper->fetchByVideoId($video->id, 'director');
		$directors = array();
		foreach ($items as $item) {
			$directors[] = array(
				'name' => $item->name,
				'role' => $item->role,
				'sort_order' => $item->sort_order,
			);
		}
		$items = $mapper->fetchByVideoId($video->id, 'cast');
		$cast = array();
		foreach ($items as $item) {
			$cast[] = array(
				'name' => $item->name,
				'role' => $item->role,
				'sort_order' => $item->sort_order,
			);
		}
		$items = $mapper->fetchByVideoId($video->id, 'crew');
		$crew = array();
		foreach ($items as $item) {
			$crew[] = array(
				'name' => $item->name,
				'role' => $item->role,
				'sort_order' => $item->sort_order,
			);
		}

		//news
		$mapper = new Application_Model_Mapper_VideoPlatformVideoNews();
		$items = $mapper->fetchByVideoId($video->id, 'news');
		$news = array();
		foreach ($items as $item) {
			$news[] = array(
				'name' => $item->name,
				'content' => $item->description,
				'sort_order' => $item->sort_order,
				'created' => $item->created
			);
		}
		//reviews
		$items = $mapper->fetchByVideoId($video->id, 'reviews');
		$reviews = array();
		foreach ($items as $item) {
			$reviews[] = array(
				'name' => $item->name,
				'content' => $item->description,
				'sort_order' => $item->sort_order,
				'created' => $item->created
			);
		}

		//profile/attributes
		$mapper = new Application_Model_Mapper_VideoPlatformVideoProfile();
		$items = $mapper->getProfileByVideoId($video->id);
		$attributes = array();
		foreach ($items as $key => $item) {
			$attributes[$key] = array(
				'title' => $item['title'],
				'value' => $item['value']
			);
		}


		return array(
			//basic video details
			'id' => $video->id,
			'code' => $video->code,
			//'hash' => $video->hash,
			'title' => $video->title,
			'summary' => $video->summary,
			'description' => $video->description,
			'meta_keywords' => $video->meta_keywords,
			'meta_description' => $video->meta_description,
			'transcode_status' => $video->transcode_status,
			'status' => $video->status,
			'thumbnail_url' => ($video->thumbnail_src)?"{$this->_full_url}{$video->thumbnail_src}":null,
			'poster_url' => ($video->poster_src)?"{$this->_full_url}{$video->poster_src}":null,
			'poster_type' => $video->poster_type,
			'trailer_url' => $trailer_url,
			'created' => $video->created,
			'modified' => $video->modified,

			//monetization details
			'purchase_required' => $monetization->purchase_required,
			'purchase_price' => $monetization->purchase_price,
			'rental_required' => $monetization->rental_required,
			'rental_price' => $monetization->rental_price,
			'rental_duration_days' => $monetization->rental_duration_days,

			//image gallery
			'gallery_images' => $images,

			//directors
			'directors' => $directors,

			//cast
			'cast' => $cast,

			//crew
			'crew' => $crew,

			//news
			'news' => $news,

			//reviews
			'reviews' => $reviews,

			//profile attributes
			'attributes' => $attributes,
		);
	}
}
