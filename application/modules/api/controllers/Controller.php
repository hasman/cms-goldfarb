<?php
class Api_Controller extends Zend_Controller_Action
{
	use ZendRenderer;
	use ZendOptions;

	/** @var Zend_Db_Adapter_Mysqli Zend DB Adapter */
	var $_db;
	/** @var string ie: https://example.com */
	var $_full_url;
	/** @var Api_Model_Response Result of the API call */
	var $_result;
	/** @var int HTTP Response Code */
	var $_header_code;

	public function init()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
		$this->disableRenderers();

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$options = $this->getOptions();

		$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
		$this->_full_url = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];

		$api_key = $this->getRequest()->getParam('api_key','');

		$mapper = new Api_Model_Mapper_ApiKey();
		if (!$mapper->validateKey($api_key)) {
			$this->forward('noauth');
		}
	}

	public function postDispatch()
	{
		switch ($this->_header_code) {
			case 401:
				header("HTTP/1.0 401 Unauthorized");
				break;
			case 404:
				header("HTTP/1.0 404 Not Found");
				break;
			case 422:
				header("HTTP/1.0 422 Unprocessable Entity");
				break;
			case 500:
				header("HTTP/1.0 500 Internal Error");
				break;
			case 200:
			default:
				header("HTTP/1.0 200 OK");
		}

		header("Content-type: application/json");

		echo (string)$this->_result;
	}

	public function noauthAction()
	{
		$this->_header_code = 401;
		$this->_result = new Api_Model_Response([
			'status' => Api_Model_Response::STATUS_ERROR,
			'message' => 'API Key missing or invalid',
		]);
	}
}
