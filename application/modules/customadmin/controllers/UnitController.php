<?php
use Cocur\Slugify\Slugify;

class Customadmin_UnitController extends Zend_Controller_Action {

	/** @var Zend_Db_Adapter_Mysqli */
	public $_db;

	public function indexAction()
	{
		$this->_redirect('/admin/unit/list');
	}

	public function listAction() {
        $this->view->placeholder('page_title')->set("Units");
        $breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Units</li>';
        $this->view->placeholder('breadcrumb')->set($breadcrumb);

		$mapper = new Customadmin_Model_Mapper_GfBuilding();
		$this->view->buildings = $mapper->fetchPairs();

        $mapper = new Customadmin_Model_Mapper_GfRegion();
        $this->view->regions = $mapper->fetchPairs();

		$mapper = new Customadmin_Model_Mapper_GfNeighborhood();
		$this->view->neighborhoods = $mapper->fetchPairs();

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','status');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->building = $building = trim($this->_getParam('building',''));
		$this->view->neighborhood = $neighborhood = trim($this->_getParam('neighborhood',''));
		$this->view->region = $region = trim($this->_getParam('region',''));
		$this->view->status = $status = trim($this->_getParam('status',''));
		$this->view->unit_type = $unit_type = trim($this->_getParam('unit_type',''));

		$search_keys = array(
			'keyword' => $mask,
			'building' => $building,
			'neighborhood' => $neighborhood,
			'region' => $region,
			'status' => $status,
			'unit_type' => $unit_type,
		);
		$mapper = new Customadmin_Model_Mapper_GfUnit();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function addAction() {
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'units', 'add')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Units");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/unit/list">Units</a></li><li class="active">Add Unit</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Customadmin_Form_Unit();
		$form->getElement('cmd')->setValue('add');
		$this->view->form = $form;
		$this->view->cmd = 'add';

		$this->render('form');
	}

	public function editAction() {
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'units', 'edit')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Units");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/unit/list">Units</a></li><li class="active">Edit Unit</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->view->unit_id = $id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {
			$form = new Customadmin_Form_Unit();

			$mapper = new Customadmin_Model_Mapper_GfUnit();
			$this->view->unit = $entity = $mapper->find($id);
			$data = $entity->toArray();

			//building
			$mapper = new Customadmin_Model_Mapper_GfBuilding();
			$this->view->building = $mapper->find($entity->building_id);

			//features
			$mapper = new Customadmin_Model_Mapper_GfFeature();
			$data['features'] = $mapper->fetchArrayByUnitId($id);

			//incentives
			$mapper = new Customadmin_Model_Mapper_GfIncentive();
			$data['incentives'] = $mapper->fetchArrayByUnitId($id);

			//convert commercial and residential to Type
			$data['type'] = '';
			if (stripos($data['commercial'], 'Office') !== false) {
				$data['type'] = 'commercial_office';
			}elseif (stripos($data['commercial'], 'Retail') !== false) {
				$data['type'] = 'commercial_retail';
			}elseif (stripos($data['residential'], 'Rentals') !== false) {
				$data['type'] = 'residential_rentals';
			}

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('form');
		} else {
			$this->getResponse()->setRedirect('/admin/unit/list');
		}
	}

	public function saveAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'units', 'edit')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Units");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/unit/list">Units</a></li><li class="active">Edit Unit</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Customadmin_Form_Unit();
		$mapper = new Customadmin_Model_Mapper_GfUnit();

		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		//if ($data['id']) {
		if( $form->isValid($data) ){
			$commercial = $residential = [];
			//convert commercial and residential flags into array
			if ($data['type'] == 'commercial_office') $commercial[] = 'Office';
			if ($data['type'] == 'commercial_retail') $commercial[] = 'Retail';
			if ($data['type'] == 'residential_rentals') $residential[] = 'Rentals';

			$data['commercial'] = json_encode($commercial);
			$data['residential'] = json_encode($residential);

			$entity = $mapper->find($data['id']);
			if ($entity) {
				$data = array_merge($entity->toArray(), $data);
			}

			if (!$data['code']) {
				$data['code'] = strtolower($data['number']);
			}

			$entity = new Custom_Model_GfUnit($data);
			$id = $mapper->save($entity);

			Custom_Service_Audit::recordUnitActivity($data['id'], $this->view->admin_user->email);

			//features
			$tc_mapper = new Customadmin_Model_Mapper_GfUnitFeature();
			$tc_mapper->deleteByUnitId($id);
			foreach ($data['features'] as $k => $tag_id) {
				$tc_mapper->save(new Custom_Model_GfUnitFeature(array('unit_id' => $id, 'feature_id' => $tag_id,)));
			}

			//incentives
			$tc_mapper = new Customadmin_Model_Mapper_GfUnitIncentive();
			$tc_mapper->deleteByUnitId($id);
			foreach ($data['incentives'] as $k => $tag_id) {
				$tc_mapper->save(new Custom_Model_GfUnitIncentive(array('unit_id' => $id, 'incentive_id' => $tag_id,)));
			}

			$this->getResponse()->setRedirect('/admin/unit/list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('form');
		}
	}

	public function imageViewAction()
	{
		$this->view->unit_id = $unit_id = $this->getRequest()->getParam('unit_id', 0);
		if(!empty($unit_id) && is_numeric($unit_id)){
			$mapper = new Customadmin_Model_Mapper_GfUnitImage();
			$this->view->images = $mapper->fetchOrderedByUnitId($unit_id);
		}
	}

	/* //this would be a simple method. We are using an Image Library concept here instead
	public function imageAddAction()
	{
		$unit_id = $this->getRequest()->getParam('unit_id', 0);
		if(!empty($unit_id) && is_numeric($unit_id)){

			$form = new Customadmin_Form_UnitImage();

			$data = array(
				'unit_id' => $unit_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('image-form');
		}
	}
*/
	public function imageEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Customadmin_Form_UnitImage();
			$mapper = new Customadmin_Model_Mapper_GfUnitImage();
			$item = $mapper->find($id);

			$mapper = new Customadmin_Model_Mapper_GfImage();
			$image = $mapper->find($item->image_id);

			$alt_text = $image->alt_text == '' ? $item->alt_text : $image->alt_text;

			$data = array(
				'id' => $item->id,
				'unit_id' => $item->unit_id,
				'sort_order' => $item->sort_order,
				'image_id' => $image->id,
				'title' => $image->title,
				'alt_text' => $alt_text,
				'image' => $image->image
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('image-form');
		}
	}

	public function imageSaveAction()
	{
		$form = new Customadmin_Form_UnitImage();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$items = array(
				'id' => $data['id'],
				'unit_id' => $data['unit_id'],
				'image_id' => $data['image_id'],
				'sort_order' => $data['sort_order'],
				'alt_text' => $data['alt_text']
			);
			$mapper = new Customadmin_Model_Mapper_GfUnitImage();
			$mapper->save(new Custom_Model_GfUnitImage($items));

			//ALTER TABLE gf_unit_images ADD alt_text varchar(255) null AFTER image_id;
;

			$this->getResponse()->setRedirect("/admin/unit/image-view/unit_id/{$data['unit_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('image-form');
		}
	}


	public function imageAddAction()
	{
		$this->view->unit_id = $unit_id = $this->getRequest()->getParam('unit_id', 0);
		if(!empty($unit_id) && is_numeric($unit_id)) {
			$mapper = new Customadmin_Model_Mapper_GfTag();
			$this->view->tags = $mapper->fetchPairs();

			$mapper = new Customadmin_Model_Mapper_GfUnit();
			$unit = $mapper->find($unit_id);

			//building
			$mapper = new Customadmin_Model_Mapper_GfBuilding();
			$building = $mapper->find($unit->building_id);

			$tag = [];
			if ($unit->number) {
				$tag[] = $unit->number;
			}
			if ($building->voyager_code)  {
				$tag[] = $building->voyager_code;
			}

			$page = $this->getRequest()->getParam('page', 1);
			$this->view->dir = $dir = $this->_getParam('dir', 'DESC');
			$this->view->sort = $sort = $this->_getParam('sort', 'created');

			$this->view->mask = $mask = trim($this->_getParam('mask', ''));
			$this->view->tag_id = $tag_id = $this->_getParam('tag_id', '');
			//$this->view->tag = $tag = trim($this->_getParam('tag', ''));

			if ($mask || $tag_id) {
				$tag = null;
			}

			$search_keys = array(
				'keyword' => $mask,
				'tag_id' => $tag_id,
				'tag' => $tag,
			);
			$mapper = new Customadmin_Model_Mapper_GfImage();
			$results = $mapper->listSearch($sort, $dir, $search_keys);

			$paginator = Zend_Paginator::factory($results);
			$paginator->setCurrentPageNumber($page);
			$paginator->setItemCountPerPage(24);
			$this->view->list = $paginator;
		}
	}

	public function imageSelectAction()
	{
		$this->view->unit_id = $unit_id = $this->getRequest()->getParam('unit_id', 0);
		$image_id = $this->getRequest()->getParam('id', 0);
		if(!empty($unit_id) && is_numeric($unit_id) && !empty($image_id) && is_numeric($image_id)) {
			$data = array(
				'unit_id' => $unit_id,
				'image_id' => $image_id,
				'sort_order' => 9999,
			);

			$mapper = new Customadmin_Model_Mapper_GfUnitImage();
			$mapper->save(new Custom_Model_GfUnitImage($data));
		}

		$this->getResponse()->setRedirect("/admin/unit/image-view/unit_id/{$unit_id}");
	}

	public function imageDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$unit_id = $this->getRequest()->getParam('unit_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Customadmin_Model_Mapper_GfUnitImage();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/unit/image-view/unit_id/{$unit_id}");
	}

	public function imageSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Customadmin_Model_Mapper_GfUnitImage();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function imageLibraryAddAction() {
		$this->view->unit_id = $unit_id = $this->getRequest()->getParam('unit_id', 0);

		if(!empty($unit_id) && is_numeric($unit_id)) {
			$form = new Customadmin_Form_ImageLibraryImage();
			$form->getElement('cmd')->setValue('add');

			$mapper = new Customadmin_Model_Mapper_GfUnit();
			$unit = $mapper->find($unit_id);

			//building
			$mapper = new Customadmin_Model_Mapper_GfBuilding();
			$building = $mapper->find($unit->building_id);

			$tag = [];
			if ($unit->number) {
				$tag[] = $unit->number;
			}
			if ($building->voyager_code)  {
				$tag[] = $building->voyager_code;
			}
			$form->getElement('tags')->setValue(implode("|",$tag));
			$form->setAction('/admin/unit/image-library-save/unit_id/'.$unit->id);

			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('image-library-form');
		}
	}

	public function imageLibrarySaveAction()
	{
		$this->view->unit_id = $unit_id = $this->getRequest()->getParam('unit_id', 0);

		if(!empty($unit_id) && is_numeric($unit_id)) {
			$form = new Customadmin_Form_ImageLibraryImage();
			$mapper = new Customadmin_Model_Mapper_GfImage();

			$form->isValid($this->getRequest()->getParams());
			$data = $form->getValues();
			$this->view->errors = array();

			//if ($data['id']) {
			if ($form->isValid($data)) {

				$entity = $mapper->find($data['id']);
				if ($entity) {
					$data = array_merge($entity->toArray(), $data);
				}

				$entity = new Custom_Model_GfImage($data);
				$id = $mapper->save($entity);

				//tags
				$t_mapper = new Customadmin_Model_Mapper_GfTag();
				$tc_mapper = new Customadmin_Model_Mapper_GfImageTag();

				$tc_mapper->deleteByImageId($id);

				$tags = explode("|", $data['tags']);
				$sort = 0;
				foreach ($tags as $k => $v) {
					if ($v && trim($v) != '' && $v != 'NULL') {
						$entity = $t_mapper->doesTagExist($v);
						if (!$entity) {
							$tag_id = $t_mapper->save(new Custom_Model_GfTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v)));
						} else {
							$tag_id = $entity->id;
						}

						$tc_mapper->save(new Custom_Model_GfImageTag(array('image_id' => $id, 'tag_id' => $tag_id)));

						$sort++;
					}
				}

				//add the image to the unit
				$data = array(
					'unit_id' => $unit_id,
					'image_id' => $id,
					'sort_order' => 9999,
				);

				$mapper = new Customadmin_Model_Mapper_GfUnitImage();
				$mapper->save(new Custom_Model_GfUnitImage($data));

				$this->getResponse()->setRedirect("/admin/unit/image-view/unit_id/{$unit_id}");
			} else {
				$data = $form->getValues();
				$this->view->cmd = $data['cmd'];
				$form->getElement('cmd')->setValue($data['cmd']);
				$form->populate($data);
				$this->view->form = $form;
				$this->render('image-library-form');
			}
		}
	}

	public function ajaxListAction()
	{
		$this->view->layout()->disableLayout();

		$search_keys = array(
			'keyword' => $this->_getParam('q','')
		);
		$mapper = new Customadmin_Model_Mapper_GfUnit();
		$results = $mapper->listSearch('code', 'ASC', $search_keys);

		$stmt = $this->_db->query($results->__toString());
		$items = $stmt->fetchAll();

		$array = array();
		foreach ($items as $item) {
			$array['results'][] = array("id"=>$item['id'],"text"=>"{$item['building_name']} - {$item['code']}");
		}

		echo json_encode($array);exit;
	}

	public function ajaxViewAction()
	{
		$this->view->layout()->disableLayout();

		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Customadmin_Model_Mapper_GfUnit();
			$entity = $mapper->find($id);

			$item = $entity->toArray();
			$array = array("id"=>$item['id'],"text"=>"{$item['building_name']} - {$item['code']}");

			echo json_encode($array);exit;
		}
	}

	/**
	 * @throws Zend_Paginator_Exception
	 * @throws Exception
	 */
	public function historyViewAction()
	{
		$this->view->unit_id = $unit_id = $this->getRequest()->getParam('unit_id', 0);
		if(!empty($unit_id) && is_numeric($unit_id)){
			$mapper = new Customadmin_Model_Mapper_GfUnit();
			$entity = $mapper->find($unit_id);
			if ($entity) {
				$page = $this->getRequest()->getParam('page',1);
				$this->view->dir = $dir = $this->_getParam('dir','DESC');
				$this->sort = $sort = $this->getRequest()->getParam('sort', 'created');

				$search_keys = array(
					'unit_id' => $unit_id,
				);
				$mapper = new Customadmin_Model_Mapper_GfAudit();
				$results = $mapper->listSearch($sort, $dir, $search_keys);

				$paginator = Zend_Paginator::factory($results);
				$paginator->setCurrentPageNumber($page);
				$paginator->setItemCountPerPage(50);
				$this->view->list = $paginator;
			}
		}
	}

	public function init() {
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'units', 'view');
		if ($acl_status) {
			$this->view->admin_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->admin_user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap');

		$options = $bootstrap->getOptions();
		$this->_auth_key = $options['auth']['key'];
		$this->_auth_vector = $options['auth']['vector'];

		$this->_statuses = array(
			'Available' => 'Available',
			'Unavailable' => 'Unavailable',
			'Occupied' => 'Occupied',
			'all_vacant' => 'All Vacant'
		);
		$this->view->statuses = $this->_statuses;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
