<?php
use Cocur\Slugify\Slugify;

class Customadmin_BuildingController extends Zend_Controller_Action {

	public function indexAction()
	{
		$this->_redirect('/admin/building/list');
	}

	public function listAction() {
        $this->view->placeholder('page_title')->set("Buildings");
        $breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Buildings</li>';
        $this->view->placeholder('breadcrumb')->set($breadcrumb);

        $mapper = new Customadmin_Model_Mapper_GfRegion();
        $this->view->regions = $mapper->fetchPairs();

		$mapper = new Customadmin_Model_Mapper_GfNeighborhood();
		$this->view->neighborhoods = $mapper->fetchPairs();

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','created');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->neighborhood = $neighborhood = trim($this->_getParam('neighborhood',''));
		$this->view->region = $region = trim($this->_getParam('region',''));

		$search_keys = array(
			'keyword' => $mask,
			'neighborhood' => $neighborhood,
			'region' => $region,
		);
		$mapper = new Customadmin_Model_Mapper_GfBuilding();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function editAction() {
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'building', 'edit')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Buildings");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/building/list">Buildings</a></li><li class="active">Edit Building</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->view->building_id = $id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {
			$form = new Customadmin_Form_Building();

			$mapper = new Customadmin_Model_Mapper_GfBuilding();
			$entity = $mapper->find($id);
			$data = $entity->toArray();

			//amenities
			$mapper = new Customadmin_Model_Mapper_GfAmenity();
			$data['amenities'] = implode("|",$mapper->fetchAmenityNamesByBuildingId($id));

			//convert commercial to array
			$data['commercial'] = ($data['commercial'])?json_decode($data['commercial']):array();

			//convert residential to array
			$data['residential'] = ($data['residential'])?json_decode($data['residential']):array();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('form');
		} else {
			$this->getResponse()->setRedirect('/admin/building/list');
		}
	}

	public function saveAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'building', 'edit')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Buildings");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/building/list">Buildings</a></li><li class="active">Edit Building</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Customadmin_Form_Building();
		$mapper = new Customadmin_Model_Mapper_GfBuilding();

		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if ($data['id']) {
		//if( $form->isValid($data) ){
			//convert commercial to json
			$data['commercial'] = ($data['commercial']) ? json_encode($data['commercial']) : '';

			//convert residential to array
			$data['residential'] = ($data['residential']) ? json_encode($data['residential']) : '';

			$entity = $mapper->find($data['id']);
			$data = array_merge($entity->toArray(),$data);

			$entity = new Custom_Model_GfBuilding($data);
			$id = $mapper->save($entity);

			//amenities
			$t_mapper = new Customadmin_Model_Mapper_GfAmenity();
			$tc_mapper = new Customadmin_Model_Mapper_GfBuildingAmenity();

			$tc_mapper->deleteByBuildingId($id);

			$tags = explode("|",$data['amenities']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesAmenityExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Custom_Model_GfAmenity(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'name' => $v, 'status' => 'Enabled')));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Custom_Model_GfBuildingAmenity(array('building_id' => $id, 'amenity_id' => $tag_id,)));//'sort_order' => $sort

					$sort++;
				}
			}

			$this->getResponse()->setRedirect('/admin/building/list');
			/*
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('form');
		}
			*/
		}
	}

	public function openhouseViewAction()
	{
		$this->view->building = $building_id = $this->getRequest()->getParam('building_id', 0);
		if(!empty($building_id) && is_numeric($building_id)){
			$mapper = new Customadmin_Model_Mapper_GfBuildingOpenHouse();
			$this->view->open_houses = $mapper->fetchByBuildingId($building_id);
		}
	}

	public function openhouseAddAction()
	{
		$building_id = $this->getRequest()->getParam('building_id', 0);
		if(!empty($building_id) && is_numeric($building_id)){

			$form = new Customadmin_Form_OpenHouse();

			$data = array(
				'building_id' => $building_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('openhouse-form');
		}
	}

	public function openhouseEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Customadmin_Form_OpenHouse();
			$mapper = new Customadmin_Model_Mapper_GfBuildingOpenHouse();
			$item = $mapper->find($id);

			$data = $item->toArray();
			$data['date'] = date("m/d/Y", strtotime($data['start_time']));
			$data['start'] = date("g:i a", strtotime($data['start_time']));
			$data['end'] = date("g:i a", strtotime($data['end_time']));

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('openhouse-form');
		}
	}

	public function openhouseSaveAction()
	{
		$form = new Customadmin_Form_OpenHouse();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['start_time'] = date("Y-m-d H:i", strtotime($data['date'].' '.str_replace(' ','',$data['start'])));
			$data['end_time'] = date("Y-m-d H:i", strtotime($data['date'].' '.str_replace(' ','',$data['end'])));

			$mapper = new Customadmin_Model_Mapper_GfBuildingOpenHouse();
			$item = new Custom_Model_GfBuildingOpenHouse($data);
			$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/building/openhouse-view/building_id/{$data['building_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('openhouse-form');
		}
	}


	public function openhouseDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$building_id = $this->getRequest()->getParam('building_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Customadmin_Model_Mapper_GfBuildingOpenHouse();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/building/openhouse-view/building_id/{$building_id}");
	}

	public function imageViewAction()
	{
		$this->view->building_id = $building_id = $this->getRequest()->getParam('building_id', 0);
		if(!empty($building_id) && is_numeric($building_id)){
			$mapper = new Customadmin_Model_Mapper_GfBuildingImage();
			$this->view->images = $mapper->fetchOrderedByBuildingId($building_id);
		}
	}

	/* //this would be a simple method. We are using an Image Library concept here instead
	public function imageAddAction()
	{
		$building_id = $this->getRequest()->getParam('building_id', 0);
		if(!empty($building_id) && is_numeric($building_id)){

			$form = new Customadmin_Form_BuildingImage();

			$data = array(
				'building_id' => $building_id
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('image-form');
		}
	}

	public function imageEditAction()
	{
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){

			$form = new Customadmin_Form_BuildingImage();
			$mapper = new Customadmin_Model_Mapper_GfBuildingImage();
			$item = $mapper->find($id);

			$mapper = new Customadmin_Model_Mapper_GfImage();
			$image = $mapper->find($item->image_id);

			$data = array(
				'id' => $item->id,
				'building_id' => $item->building_id,
				'sort_order' => $item->sort_order,
				'image_id' => $image->id,
				'title' => $image->title,
				'alt_text' => $image->alt_text,
				'image' => $image->image
			);

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('image-form');
		}
	}

	public function imageSaveAction()
	{
		$form = new Customadmin_Form_BuildingImage();
		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($this->getRequest()->getParams())) {
			$data['sort_order'] = ($data['sort_order'])?$data['sort_order']:"9999";

			$mapper = new Customadmin_Model_Mapper_GfBuildingImage();
			$item = new Custom_Model_GfBuildingImage($data);
			//$id = $mapper->save($item);

			$this->getResponse()->setRedirect("/admin/building/image-view/building_id/{$data['building_id']}");
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('image-form');
		}
	}
	*/

	public function imageAddAction()
	{
		$this->view->building_id = $building_id = $this->getRequest()->getParam('building_id', 0);
		if(!empty($building_id) && is_numeric($building_id)) {
			$mapper = new Customadmin_Model_Mapper_GfTag();
			$this->view->tags = $mapper->fetchPairs();

			$mapper = new Customadmin_Model_Mapper_GfBuilding();
			$building = $mapper->find($building_id);

			$tag = [];
			if ($building->voyager_code)  {
				$tag[] = $building->voyager_code;
			}

			$page = $this->getRequest()->getParam('page', 1);
			$this->view->dir = $dir = $this->_getParam('dir', 'DESC');
			$this->view->sort = $sort = $this->_getParam('sort', 'created');

			$this->view->mask = $mask = trim($this->_getParam('mask', ''));
			$this->view->tag_id = $tag_id = $this->_getParam('tag_id', '');
			//$this->view->tag = $tag = trim($this->_getParam('tag', ''));

			$search_keys = array(
				'keyword' => $mask,
				'tag_id' => $tag_id,
				'tag' => $tag,
			);
			$mapper = new Customadmin_Model_Mapper_GfImage();
			$results = $mapper->listSearch($sort, $dir, $search_keys);

			$paginator = Zend_Paginator::factory($results);
			$paginator->setCurrentPageNumber($page);
			$paginator->setItemCountPerPage(24);
			$this->view->list = $paginator;
		}
	}

	public function imageSelectAction()
	{
		$this->view->building_id = $building_id = $this->getRequest()->getParam('building_id', 0);
		$image_id = $this->getRequest()->getParam('id', 0);
		if(!empty($building_id) && is_numeric($building_id) && !empty($image_id) && is_numeric($image_id)) {
			$data = array(
				'building_id' => $building_id,
				'image_id' => $image_id,
				'sort_order' => 9999,
			);

			$mapper = new Customadmin_Model_Mapper_GfBuildingImage();
			$mapper->save(new Custom_Model_GfBuildingImage($data));
		}

		$this->getResponse()->setRedirect("/admin/building/image-view/building_id/{$building_id}");
	}

	public function imageDeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);

		$building_id = $this->getRequest()->getParam('building_id', 0);
		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Customadmin_Model_Mapper_GfBuildingImage();
			$item = $mapper->find($id);
			$mapper->delete($item);
		}

		$this->getResponse()->setRedirect("/admin/building/image-view/building_id/{$building_id}");
	}

	public function imageSortAction(){
		$this->_helper->viewRenderer->setNoRender(true);

		$values = $this->getRequest()->getParam('sort');
		$sort = 1;
		foreach($values AS $val){
			if( !empty($val) ){
				$mapper = new Customadmin_Model_Mapper_GfBuildingImage();
				$mapper->setSortOrder($val,$sort);
				$sort++;
			}
		}
	}

	public function imageLibraryAddAction() {
		$this->view->building_id = $building_id = $this->getRequest()->getParam('building_id', 0);

		if(!empty($building_id) && is_numeric($building_id)) {
			$form = new Customadmin_Form_ImageLibraryImage();
			$form->getElement('cmd')->setValue('add');

			$mapper = new Customadmin_Model_Mapper_GfBuilding();
			$building = $mapper->find($building_id);

			$tag = [];
			if ($building->voyager_code)  {
				$tag[] = $building->voyager_code;
			}
			$form->getElement('tags')->setValue(implode("|",$tag));

			$this->view->form = $form;
			$this->view->cmd = 'add';

			$this->render('image-library-form');
		}
	}

	public function imageLibrarySaveAction()
	{
		$this->view->building_id = $building_id = $this->getRequest()->getParam('building_id', 0);

		if(!empty($building_id) && is_numeric($building_id)) {
			$form = new Customadmin_Form_ImageLibraryImage();
			$mapper = new Customadmin_Model_Mapper_GfImage();

			$form->isValid($this->getRequest()->getParams());
			$data = $form->getValues();
			$this->view->errors = array();

			//if ($data['id']) {
			if ($form->isValid($data)) {

				$entity = $mapper->find($data['id']);
				$data = array_merge($entity->toArray(),$data);

				$entity = new Custom_Model_GfImage($data);
				$id = $mapper->save($entity);

				//tags
				$t_mapper = new Customadmin_Model_Mapper_GfTag();
				$tc_mapper = new Customadmin_Model_Mapper_GfImageTag();

				$tc_mapper->deleteByImageId($id);

				$tags = explode("|", $data['tags']);
				$sort = 0;
				foreach ($tags as $k => $v) {
					if ($v && trim($v) != '' && $v != 'NULL') {
						$entity = $t_mapper->doesTagExist($v);
						if (!$entity) {
							$tag_id = $t_mapper->save(new Custom_Model_GfTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v)));
						} else {
							$tag_id = $entity->id;
						}

						$tc_mapper->save(new Custom_Model_GfImageTag(array('image_id' => $id, 'tag_id' => $tag_id)));

						$sort++;
					}
				}

				//add the image to the building
				$data = array(
					'building_id' => $building_id,
					'image_id' => $id,
					'sort_order' => 9999,
				);

				$mapper = new Customadmin_Model_Mapper_GfBuildingImage();
				$mapper->save(new Custom_Model_GfBuildingImage($data));

				$this->getResponse()->setRedirect("/admin/building/image-view/building_id/{$building_id}");
			} else {
				$data = $form->getValues();
				$this->view->cmd = $data['cmd'];
				$form->getElement('cmd')->setValue($data['cmd']);
				$form->populate($data);
				$this->view->form = $form;
				$this->render('image-library-form');
			}
		}
	}

	public function init() {
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'building', 'view');
		if ($acl_status) {
			$this->view->admin_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->admin_user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap');

		$options = $bootstrap->getOptions();
		$this->_auth_key = $options['auth']['key'];
		$this->_auth_vector = $options['auth']['vector'];

		$this->_statuses = array(
			'enabled' => 'Enabled',
			'disabled' => 'Disabled',
		);
		$this->view->statuses = $this->_statuses;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
