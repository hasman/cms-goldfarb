<?php
class Customadmin_IndexController extends Zend_Controller_Action
{
	/** @var  Zend_Db_Adapter_Abstract */
	protected $_db;
	protected $decimal_point = 2;
	protected $date_format = 'Y-m-d';
	protected $time_format = 'H:i:s';
	protected $field_data = [
		'ga:date' => ['label' => 'Date', 'type' => 'date', 'icon' => 'fas fa-globe', 'bgcolor' => '#FB9678'],
		'ga:sessions' => ['label' => 'User Sessions', 'type' => 'int', 'icon' => 'fas fa-signal', 'bgcolor' => '#9575CD'],
		'ga:users' => ['label' => 'Users', 'type' => 'int', 'icon' => 'fas fa-users', 'bgcolor' => '#fb9678'],
		'ga:pageviews' => ['label' => 'Page Views', 'type' => 'int', 'icon' => 'fas fa-laptop', 'bgcolor' => '#9675ce'],
		'ga:pageviewsPerSession' => ['label' => 'Page Views Per Sessions', 'type' => 'decimal', 'icon' => 'fas fa-eye', 'bgcolor' => '#fec107'],
		'ga:avgSessionDuration' => ['label' => 'Avg. Session Duration', 'type' => 'time', 'icon' => 'fas fa-clock-o', 'bgcolor' => '#7be462'],
		'ga:bounceRate' => ['label' => 'Bounce Rate', 'type' => 'percent', 'icon' => 'fas fa-bar-chart', 'bgcolor' => '#abacab'],
		'ga:percentNewSessions' => ['label' => '% New Sessions', 'type' => 'percent', 'icon' => 'far fa-user-circle', 'bgcolor' => '#01c0c8']
	];

	public function indexAction()
	{
		$this->view->messages = $this->_flashMessenger->getMessages();
		$this->view->placeholder('page_title')->set("Dashboard");
		$startdate = $this->getRequest()->getParam('startdate', date('Y-m-d', strtotime('-7 days')));
		$enddate = $this->getRequest()->getParam('enddate', date('Y-m-d', strtotime('today')));

		$this->view->startdate = $startdate;
		$this->view->enddate = $enddate;

		$site_settings = new Application_Model_Mapper_SiteSettingValue();
		$default_view_id = $site_settings->getValueByCode('dashboard_view_id'); //161290111
		$this->view->has_analytics = empty($default_view_id)?false:true;

		$this->view->reports = [
			'ga:users', 'ga:pageviews', 'ga:pageviewsPerSession',
			'ga:avgSessionDuration', 'ga:bounceRate', 'ga:percentNewSessions'
		];
		$this->view->dashboard_widgets = [];
		if ($this->view->has_analytics) {
			try {
				$svc = new Application_Service_GoogleAnalytics();

				$result = $svc->report($startdate, $enddate, [], $this->view->reports, [], [], []);

				if ($result) {
					$data = $result->getDataTable()->toSimpleObject();
					$cols = $data->cols;
					if ($result->totalResults > 0) {
						$rows = null;
						if (isset($data->rows)) {
							$rows = $data->rows[0];
						}
						foreach ($cols as $index => $col) {
							if (isset($this->field_data[$col->id])) {
								$tmp = $this->field_data[$col->id];
								if (!empty($rows)) {
									$tmp['data']['count'] = $this->formatResult($rows->c[$index]->v, $tmp['type']);
								} else {
									$tmp['data']['count'] = $this->formatResult(0, $tmp['type']);
								}
							}
							$this->view->dashboard_widgets[$index] = $tmp;
						}
					}
				}
			}catch (Exception $e) {
				//echo 'Caught exception: ',  $e->getMessage(), "\n";

				$this->view->has_analytics = false;
			}
		}

		//ecommerce sales data
		$this->view->ecomm_status = $ecomm_status = $site_settings->getValueByCode('ecomm_status');
		if ($ecomm_status == 'Enabled') {
			//sales per day - month to date
			$sql = $this->_db->select()->from(array('o'=>$this->_table_prefix.'orders'),'')
				->joinLeft(array('opay'=>$this->_table_prefix.'order_payments'),'o.id = opay.order_id','')
				->where('o.created LIKE "'.date("Y-m-").'%" ')
				->where('o.status <> "Canceled"')
				->columns('DATE_FORMAT(o.created,"%Y-%m-%d") AS period','o')
				->columns('(SUM(opay.total_amount)) AS amount','opay')
				->columns('(COUNT(*)) AS orders', 'o')
				->group('DATE_FORMAT(o.created,"%Y%m%d")')
			;
			$this->view->sales_mtd = $sales_mtd = $this->_db->fetchAll($sql);
			$this->view->sales_mtd_total = 0;
			foreach ($sales_mtd as $sale) {
				$this->view->sales_mtd_total += $sale['amount'];
			}

			//sales per month past 12 months
			$sql = $this->_db->select()->from(array('o'=>$this->_table_prefix.'orders'),'')
				->joinLeft(array('opay'=>$this->_table_prefix.'order_payments'),'o.id = opay.order_id','')
				->where('o.created >= DATE_SUB(DATE(NOW()), INTERVAL 12 MONTH) ')
				->where('o.status <> "Canceled"')
				->columns('DATE_FORMAT(o.created,"%Y-%m") AS period','o')
				->columns('(SUM(opay.total_amount)) AS amount','opay')
				->columns('(COUNT(*)) AS orders', 'o')
				->group('DATE_FORMAT(o.created,"%Y%m")')
			;
			$this->view->sales_year = $sales_year = $this->_db->fetchAll($sql);
			$this->view->sales_year_total = 0;
			foreach ($sales_year as $sale) {
				$this->view->sales_year_total += $sale['amount'];
			}

			//top products ordered past month
			$sql = $this->_db->select()->from(array('o'=>$this->_table_prefix.'orders'),'')
				->joinLeft(array('oi'=>$this->_table_prefix.'order_items'),'o.id = oi.order_id',array('warehouse_sku','title'))
				->where('o.created >= DATE_SUB(DATE(NOW()), INTERVAL 1 MONTH) ')
				->where('sku IS NOT NULL')
				->where('oi.line_total > 0')
				->where('o.status <> "Canceled"')
				->columns('(SUM(oi.line_total)) AS amount','oi')
				->columns('(SUM(oi.qty)) AS orders', 'o')
				->group('oi.warehouse_sku')
				->order('orders DESC')
				->limit(20)
			;
			$this->view->products_month = $this->_db->fetchAll($sql);

			//top products ordered all time
			$sql = $this->_db->select()->from(array('o'=>$this->_table_prefix.'orders'),'')
				->joinLeft(array('oi'=>$this->_table_prefix.'order_items'),'o.id = oi.order_id',array('warehouse_sku','title'))
				->where('sku IS NOT NULL')
				->where('oi.line_total > 0')
				->where('o.status <> "Canceled"')
				->columns('(SUM(oi.line_total)) AS amount','oi')
				->columns('(SUM(oi.qty)) AS orders', 'o')
				->group('oi.warehouse_sku')
				->order('orders DESC')
				->limit(20)
			;
			$this->view->products_year = $this->_db->fetchAll($sql);

			//recent orders
			$sql = $this->_db->select()->from(array('o'=>$this->_table_prefix.'orders'),'*')
				->joinLeft(array('oab'=>$this->_table_prefix.'order_addresses'),'o.id = oab.order_id AND oab.type="billing"',array('first_name','last_name','company'))
				->joinLeft(array('opay'=>$this->_table_prefix.'order_payments'),'o.id = opay.order_id',array('type','total_amount'))
				->order('o.created DESC')
				->limit(10)
			;
			$this->view->orders = $this->_db->fetchAll($sql);
		}
	}

	function formatResult($data, $type)
	{
		$output = $data;
		if (!empty($data)) {
			switch ($type) {
				case 'date':
					$output = date($this->date_format, strtotime($data));
					break;
				case 'time':
					$output = gmdate($this->time_format, $data);
					break;
				case 'datetime':
					$output = date($this->date_format.' '.$this->time_format, strtotime($data));
					break;
				case 'decimal':
					$output = number_format($data, $this->decimal_point);
					break;
				case 'percent':
					$output = number_format($data, 0).'%';
					break;
			}
		}
		return $output;
	}

	function init()
	{
		$this->view->placeholder('section')->set("home");

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}else{
			$this->view->user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		}

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();
		$this->view->sitename = $options['site']['name'];
		$this->view->service_account = $options['google']['service_account'];

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
