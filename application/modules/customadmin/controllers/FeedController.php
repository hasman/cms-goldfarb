<?php
class Customadmin_FeedController extends Zend_Controller_Action {

	public function indexAction()
	{
		$this->getResponse()->setRedirect('/admin/feed/list');
	}

	public function listAction() {
        $this->view->placeholder('page_title')->set("Feed Audits");
        $breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Feed Audits</li>';
        $this->view->placeholder('breadcrumb')->set($breadcrumb);

		$results = Custom_Service_Building::fetchNestedBuildingData();
		$buildings = [];
		$buildings_list = [""  => "Select A Building"];
		foreach ($results as $building) {
			$buildings_list[$building->code] = $building->name;
			$buildings[$building->code] = [
				'name' => $building->name,
				'neighborhood' => $building->neighborhood->code,
				'region' => $building->region->code,
			];
		}
		$this->view->buildings = $buildings;
		$this->view->buildings_list = $buildings_list;
	}


	public function init() {
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'building', 'view');
		if ($acl_status) {
			$this->view->admin_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->admin_user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap');

		$options = $bootstrap->getOptions();
		$this->_auth_key = $options['auth']['key'];
		$this->_auth_vector = $options['auth']['vector'];

		$this->_statuses = array(
			'enabled' => 'Enabled',
			'disabled' => 'Disabled',
		);
		$this->view->statuses = $this->_statuses;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
