<?php
use Cocur\Slugify\Slugify;

class Customadmin_ImageLibraryController extends Zend_Controller_Action {

	public function indexAction()
	{
		$this->view->placeholder('page_title')->set("Image Library");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Image Library</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$mapper = new Customadmin_Model_Mapper_GfTag();
		$this->view->tags = $mapper->fetchPairs();

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','created');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->tag_id = $tag_id = $this->_getParam('tag_id','');
		$this->view->tag = $tag = trim($this->_getParam('tag',''));

		$search_keys = array(
			'keyword' => $mask,
			'tag_id' => $tag_id,
			'tag' => $tag,
		);
		$mapper = new Customadmin_Model_Mapper_GfImage();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(48);
		$this->view->list = $paginator;
	}



	public function addAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'units', 'add')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Image Library");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Image Library</li><li class="active">Add Image</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Customadmin_Form_ImageLibraryImage();
		$form->getElement('cmd')->setValue('add');
		$this->view->form = $form;
		$this->view->cmd = 'add';

		$this->render('form');
	}

	public function editAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'units', 'add')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Image Library");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Image Library</li><li class="active">Edit Image</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->view->floorplan_id = $id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {
			$form = new Customadmin_Form_ImageLibraryImage();

			$mapper = new Customadmin_Model_Mapper_GfImage();
			$this->view->image = $entity = $mapper->find($id);
			$data = $entity->toArray();

			//tags
			$t_mapper = new Customadmin_Model_Mapper_GfImageTag();
			$data['tags'] = implode("|",$t_mapper->fetchTagNamesByImageId($id));

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('form');
		} else {
			$this->getResponse()->setRedirect('/admin/image-library');
		}
	}

	public function saveAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'units', 'add')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Image Library");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Image Library</li><li class="active">Edit Image</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Customadmin_Form_ImageLibraryImage();
		$mapper = new Customadmin_Model_Mapper_GfImage();

		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		//if ($data['id']) {
		if( $form->isValid($data) ){

			$entity = $mapper->find($data['id']);
			if ($entity) {
				$data = array_merge($entity->toArray(), $data);
			}

			$entity = new Custom_Model_GfImage($data);
			$id = $mapper->save($entity);

			//tags
			$t_mapper = new Customadmin_Model_Mapper_GfTag();
			$tc_mapper = new Customadmin_Model_Mapper_GfImageTag();

			$tc_mapper->deleteByImageId($id);

			$tags = explode("|",$data['tags']);
			$sort = 0;
			foreach ($tags as $k=>$v) {
				if ($v && trim($v) != '' && $v != 'NULL') {
					$entity = $t_mapper->doesTagExist($v);
					if (!$entity) {
						$tag_id = $t_mapper->save(new Custom_Model_GfTag(array('code' => strtolower(str_ireplace(" ", "-", $v)), 'title' => $v)));
					} else {
						$tag_id = $entity->id;
					}

					$tc_mapper->save(new Custom_Model_GfImageTag(array('image_id' => $id, 'tag_id' => $tag_id)));

					$sort++;
				}
			}

			$this->getResponse()->setRedirect('/admin/image-library');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('form');
		}
	}

	public function deleteAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'units', 'add')) {
			$this->redirect("/admin");
		}

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Custom_Model_Mapper_GfImage();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/image-library');
	}

	public function init() {
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'units', 'view');
		if ($acl_status) {
			$this->view->admin_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->admin_user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap');

		$options = $bootstrap->getOptions();
		$this->_auth_key = $options['auth']['key'];
		$this->_auth_vector = $options['auth']['vector'];

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
