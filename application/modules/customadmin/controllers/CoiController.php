<?php
use Cocur\Slugify\Slugify;

class Customadmin_CoiController extends Zend_Controller_Action {

	public function indexAction()
	{
		$this->_redirect('/admin/coi/list');
	}
	public function listAction() {

        $this->view->placeholder('page_title')->set("COI Document Manager");
        $breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">COI</li>';
        $this->view->placeholder('breadcrumb')->set($breadcrumb);		
		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','created');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));

		$search_keys = array(
			'keyword' => $mask
		);
		$mapper = new Customadmin_Model_Mapper_GfCoiTrades();
		$results = $mapper->listSearch($sort, $dir, $search_keys);
		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
		
		$mapper = new Custom_Model_Mapper_GfCoi();
		$results = $mapper->getAllCoi();
		$this->view->coi = $results;
		
		$mapper = new Custom_Model_Mapper_GfCoiDocs();
		$results = $mapper->getAllCoiDocs();
		$this->view->docs = $results;	
	}
	public function doceditAction(){
		
	}
	public function editAction() {
        $this->view->placeholder('page_title')->set("COI Document Edit");
        $breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active"><a href="/admin/coi/list">COI</a></li><li class="active">Edit</li>';
        $this->view->placeholder('breadcrumb')->set($breadcrumb);		
		$this->view->id = $id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {
			$form = new Customadmin_Form_Coi();

			$mapper = new Customadmin_Model_Mapper_GfCoiTrades();
			$entity = $mapper->find($id);
			$data = $entity->toArray();
			$this->view->item = $data;
			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';
			$this->render('edit');
		} else {
			$this->getResponse()->setRedirect('/admin/coi/list');
		}

	}
	public function addAction() {
        $this->view->placeholder('page_title')->set("COI Document Add");
        $breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active"><a href="/admin/coi/list">COI</a></li><li class="active">Add</li>';
        $this->view->placeholder('breadcrumb')->set($breadcrumb);		

			$form = new Customadmin_Form_Coi();
			$form->getElement('cmd')->setValue('add');
			$this->view->form = $form;
			$this->view->cmd = 'add';
			$this->render('edit');


	}
	public function deleteAction(){
		$id = $this->getRequest()->getParam('id', 0);
		//echo $id;
		if(!empty($id) && is_numeric($id)){
		$where = array('id = ?' =>  $id);
    	$DB = Zend_Db_Table_Abstract::getDefaultAdapter();
		$DB->delete('gf_coi_trades', $where);
		}
		$this->getResponse()->setRedirect('/admin/coi/list');
	}
	public function deletedocAction(){
	
		
		$postData = $this->getRequest()->getPost();
	
		$target_dir = WEB_PATH.'/userFiles/uploads/coi/';
		$deleteFile = $target_dir . $postData['docDelete'];
		$fileType = strtolower(pathinfo($postData['docDelete'],PATHINFO_EXTENSION));
		$docFiles = [];
		if(!empty($postData['docDocs'])){
			$docFiles = json_decode(htmlspecialchars_decode($postData['docDocs']),true);
		}		
		$docId = $postData['docId'];
		if(file_exists($deleteFile) && $fileType == 'pdf'){
			unlink($deleteFile);
			$i = 0;
			foreach($docFiles as $doc){
				if($doc['file'] != $postData['docDelete']){
					$newDocFiles[$i]['file']=$doc['file'];
					$newDocFiles[$i]['name']=$doc['name'];
					$i++;
				}
			}
			$docFilesJson = json_encode($newDocFiles);
    		$data = ['docs'=> $docFilesJson];
    		$where = array('id = ?' =>  $postData['docId']);
    		$DB = Zend_Db_Table_Abstract::getDefaultAdapter();
			$DB->update('gf_coi_trades',$data, $where);		
		
			echo "SUCCESS: ".$docFilesJson;
		}
		else{
			echo "ERROR: file not found";
		}
		
	exit;
	}
	public function saveAction(){
		$id = $this->getRequest()->getPost('id');
		$name = $this->getRequest()->getPost('name');
		$description = $this->getRequest()->getPost('description');
		$limit = $this->getRequest()->getPost('limit');
		$DB = Zend_Db_Table_Abstract::getDefaultAdapter();
		if(!empty($id) && is_numeric($id)){
			$data = ['name'=> $name,'description'=>$description,'limit'=>$limit];
		    $where = array('id = ?' =>  $id);
			$DB->update('gf_coi_trades',$data, $where);	
		}
		else{
			$data = ['name'=> $name,'description'=>$description,'limit'=>$limit];
			$DB->insert('gf_coi_trades',$data);		
		}
	
	$this->getResponse()->setRedirect('/admin/coi/list');
	}
		public function savesummaryAction(){
		$id = $this->getRequest()->getPost('id');
		$name = $this->getRequest()->getPost('title');
		$description = $this->getRequest()->getPost('summary');
		$instructions = $this->getRequest()->getPost('instructions');
		$DB = Zend_Db_Table_Abstract::getDefaultAdapter();
		if(!empty($id) && is_numeric($id)){
			$data = ['title'=> $name,'summary'=>$description,'instructions'=>$instructions];
		    $where = array('id = ?' =>  $id);
			$DB->update('gf_coi',$data, $where);	
		}	
	$this->getResponse()->setRedirect('/admin/coi/list');
	}
	public function uploaderAction() {
	
		$target_dir = WEB_PATH.'/userFiles/uploads/coi/';
		$target_file = $target_dir . basename($_FILES["file"]["name"]);
		$uploadOk = 1;
		$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		$postData = $this->getRequest()->getPost();
		$docFiles = [];
		$link =  $_FILES["file"]["name"];
/*		
		if(!empty($postData['docDocs'])){
		$docFiles = json_decode(htmlspecialchars_decode($postData['docDocs']),true);
		}
*/		
		if($fileType == 'pdf') {
    		$uploadOk = 1;
  		} else {
    		$error = "ERROR: Not a PDF";
    		$uploadOk = 0;
  		}
		if (file_exists($target_file)) {
  			//$error = "ERROR: file already exists";
  			$link =  rand(0,999999999)."-".$_FILES["file"]["name"];
  			//$uploadOk = 0;
		}
		if ($uploadOk == 0) {
  			echo $error;
		} else {
  			if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {

    			//$link =  $_FILES["file"]["name"];

    			$data = ['link'=> "/userFiles/uploads/coi/".$link];
    			$where = array('id = ?' =>  $postData['id']);
    			$DB = Zend_Db_Table_Abstract::getDefaultAdapter();
				$DB->update('gf_coi_docs',$data, $where);
    			echo "SUCCESS: ".$link;
  			} else {
    			echo "ERROR: error uploading your file.";
  			}
		}

	
	//print_r($this->getRequest());
			exit;
	}
	public function init() {
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'coi', 'view');
		if ($acl_status) {
			$this->view->admin_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->admin_user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap');

		$options = $bootstrap->getOptions();
		$this->_auth_key = $options['auth']['key'];
		$this->_auth_vector = $options['auth']['vector'];

		$this->_statuses = array(
			'enabled' => 'Enabled',
			'disabled' => 'Disabled',
		);
		$this->view->statuses = $this->_statuses;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}	
}