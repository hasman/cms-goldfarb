<?php
use Cocur\Slugify\Slugify;

class Customadmin_PoiController extends Zend_Controller_Action {

	public function indexAction()
	{
		$this->_redirect('/admin/poi/list');
	}

	public function listAction() {
        $this->view->placeholder('page_title')->set("Points of Interest");
        $breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Points of Interest</li>';
        $this->view->placeholder('breadcrumb')->set($breadcrumb);

		$mapper = new Customadmin_Model_Mapper_GfPoiCategory();
		$this->view->poi_categories = $mapper->fetchPairs();

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','name');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));
		$this->view->category_id = $category_id = trim($this->_getParam('category_id',''));

		$search_keys = array(
			'keyword' => $mask,
			'category_id' => $category_id
		);
		$mapper = new Customadmin_Model_Mapper_GfPoi();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function addAction() {
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'region', 'add')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Points of Interest");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/poi/list">Points of Interest</a></li><li class="active">Add Point of Interest</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Customadmin_Form_Poi();
		$form->getElement('cmd')->setValue('add');
		$this->view->form = $form;
		$this->view->cmd = 'add';

		$this->render('form');
	}

	public function editAction() {
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'region', 'edit')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Points of Interest");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/poi/list">Points of Interest</a></li><li class="active">Edit Point of Interest</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->view->poi_id = $id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {
			$form = new Customadmin_Form_Poi();

			$mapper = new Customadmin_Model_Mapper_GfPoi();
			$entity = $mapper->find($id);
			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('form');
		} else {
			$this->getResponse()->setRedirect('/admin/poi/list');
		}
	}

	public function saveAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'region', 'edit')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Points of Interest");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/poi/list">Points of Interest</a></li><li class="active">Edit Point of Interest</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Customadmin_Form_Poi();
		$mapper = new Customadmin_Model_Mapper_GfPoi();

		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($data) ){
			$entity = new Custom_Model_GfPoi($data);
			$id = $mapper->save($entity);

			$this->getResponse()->setRedirect('/admin/poi/list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('form');
		}
	}

	public function deleteAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'region', 'edit')) {
			$this->redirect("/admin");
		}

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Customadmin_Model_Mapper_GfPoi();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/poi/list');
	}

	public function categoryListAction()
	{
		$this->view->placeholder('page_title')->set("Point of Interest Categories");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li class="active">Point of Interest Categories</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','name');

		$this->view->mask = $mask = trim($this->_getParam('mask',''));

		$search_keys = array(
			'keyword' => $mask,
		);
		$mapper = new Customadmin_Model_Mapper_GfPoiCategory();
		$results = $mapper->listSearch($sort, $dir, $search_keys);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(100);
		$this->view->list = $paginator;
	}

	public function categoryAddAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'region', 'add')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Point of Interest Category");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/poi/category-list">Point of Interest Categories</a></li><li class="active">Add Point of Interest Category</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Customadmin_Form_PoiCategory();
		$form->getElement('cmd')->setValue('add');
		$this->view->form = $form;
		$this->view->cmd = 'add';

		$this->render('category-form');
	}

	public function categoryEditAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'region', 'edit')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Point of Interest Category");
		$breadcrumb = '<li><a href="/admin/">Home</a></li><li><a href="/admin/poi/category-list">Point of Interest Categories</a></li><li class="active">Edit Point of Interest Category</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$this->view->poi_id = $id = $this->getRequest()->getParam('id', 0);
		if (!empty($id) && is_numeric($id)) {
			$form = new Customadmin_Form_PoiCategory();

			$mapper = new Customadmin_Model_Mapper_GfPoiCategory();
			$entity = $mapper->find($id);
			$data = $entity->toArray();

			$form->populate($data);
			$form->getElement('cmd')->setValue('edit');
			$this->view->form = $form;
			$this->view->cmd = 'edit';

			$this->render('category-form');
		} else {
			$this->getResponse()->setRedirect('/admin/poi/category-list');
		}
	}

	public function categorySaveAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'region', 'edit')) {
			$this->redirect("/admin");
		}

		$this->view->placeholder('page_title')->set("Point of Interest Categories");
		$breadcrumb = '<li><a href="/">Home</a></li><li><a href="/admin/poi/category-list">Point of Interest Categories</a></li><li class="active">Edit Point of Interest Category</li>';
		$this->view->placeholder('breadcrumb')->set($breadcrumb);

		$form = new Customadmin_Form_PoiCategory();
		$mapper = new Customadmin_Model_Mapper_GfPoiCategory();

		$form->isValid($this->getRequest()->getParams());
		$data = $form->getValues();
		$this->view->errors = array();

		if( $form->isValid($data) ){
			$entity = new Custom_Model_GfPoiCategory($data);
			$id = $mapper->save($entity);

			$this->getResponse()->setRedirect('/admin/poi/category-list');
		}else{
			$data = $form->getValues();
			$this->view->cmd = $data['cmd'];
			$form->getElement('cmd')->setValue($data['cmd']);
			$form->populate($data);
			$this->view->form = $form;
			$this->render('category-form');
		}
	}

	public function categoryDeleteAction()
	{
		if (!Zend_Registry::get('admin_acl')->isAllowed($this->view->admin_user->role, 'region', 'edit')) {
			$this->redirect("/admin");
		}

		$id = $this->getRequest()->getParam('id', 0);
		if(!empty($id) && is_numeric($id)){
			$mapper = new Customadmin_Model_Mapper_GfPoiCategory();
			$entity = $mapper->find($id);
			$mapper->delete($entity);
		}

		$this->getResponse()->setRedirect('/admin/poi/category-list');
	}

	public function init() {
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$acl_status = Zend_Registry::get('admin_acl')->isAllowed($auth->getIdentity()->role, 'region', 'view');
		if ($acl_status) {
			$this->view->admin_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			//check if they have accepted latest licenses
			$mapper = new Admin_Model_Mapper_AdminUserLicense();
			if (!$mapper->checkUpToDate($this->view->admin_user->id)) {
				$this->getResponse()->setRedirect("/admin/license");
			}
		} else {
			$auth->clearIdentity();
			Cny_Auth::storeRequestAndAuthenticateUser();
		}

		$bootstrap = $this->getInvokeArg('bootstrap');

		$options = $bootstrap->getOptions();
		$this->_auth_key = $options['auth']['key'];
		$this->_auth_vector = $options['auth']['vector'];

		$this->_statuses = array(
			'enabled' => 'Enabled',
			'disabled' => 'Disabled',
		);
		$this->view->statuses = $this->_statuses;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->view->site_timezone = $settings_mapper->getValueByCode('site_timezone', 'America/New_York');
	}
}
