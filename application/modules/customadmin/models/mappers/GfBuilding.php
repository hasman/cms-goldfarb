<?php
class Customadmin_Model_Mapper_GfBuilding extends Custom_Model_Mapper_GfBuilding
{
	public function listSearch($sort = '', $dir = 'ASC', $search = '') {
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('b'=>'gf_buildings'),'*')
			->joinLeft(array('n'=>$this->_table_prefix.'gf_neighborhoods'), 'b.neighborhood_id = n.id', array('neighborhood_name'=>'name'))
			->joinLeft(array('r'=>$this->_table_prefix.'gf_regions'), 'n.region_id = r.id', array('region_name'=>'name'))
		;
//print_r($search);
		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					if (strpos($val, '"') !== false) {
						$match = '= "' . str_replace('"', '', $val) . '"';
					} else {
						$match = 'LIKE "%' . addslashes($val) . '%"';
					}

					switch ($key) {
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('b.name '.$match);
							break;
						case 'building_type':
							if($val == 'residential'){
							$select->where('b.residential <> "[]"');
							}
							if($val == 'commercial'){
							$select->where('b.commercial <> "[]"');
							}
							break;
						case 'neighborhood':
							$select->where('b.neighborhood_id = ?', $val);
							break;
						case 'region':
							$select->where('n.region_id = ?', $val);
							break;
						case 'status':
							$select->where('status = ?', $val);
							break;
						default:
							$select->where('id ' . $match . '');
							break;
					}
				}
			}
		}

		$select->order(array($sort.' '.$dir));

//echo $select->__toString();exit;
		return $select;
	}

	public function fetchPairs($use_code = false)
	{
		$select = $this->getDbTable()->select()->order('name ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$key = $row->id;
			if ($use_code) {
				$key = $row->code;
			}
			$array[$key] = "[".strtoupper($row->voyager_code)."] ".$row->name;
		}

		return $array;
	}
}
