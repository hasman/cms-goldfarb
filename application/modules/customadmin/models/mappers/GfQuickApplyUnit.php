<?php
class Customadmin_Model_Mapper_GfQuickApplyUnit extends Custom_Model_Mapper_GfQuickApplyUnit
{
	public function listUnits() {
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('qau' => $this->_table_prefix . 'gf_quick_apply_units'),array('id','bedrooms'))
			->joinLeft(array('u' => $this->_table_prefix . 'gf_units'), 'qau.unit_id = u.id', array('yardi_code','number'))
			->joinLeft(array('b' => $this->_table_prefix . 'gf_buildings'), 'u.building_id = b.id', array('building_name'=>'name'))
			->joinLeft(array('n' => $this->_table_prefix . 'gf_neighborhoods'), 'b.neighborhood_id = n.id', '')
			->joinLeft(array('r' => $this->_table_prefix . 'gf_regions'), 'n.region_id = r.id', array('region_name'=>'name'))
			->order(array('r.sort_order','qau.bedrooms'))
		;

		return $this->getDbTable()->fetchAll($select);
	}

	public function listSearch($sort = '', $dir = 'ASC', $search = '') {
		$select = $this->getDbTable()->select()
		;

		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					if (strpos($val, '"') !== false) {
						$match = '= "' . str_replace('"', '', $val) . '"';
					} else {
						$match = 'LIKE "%' . addslashes($val) . '%"';
					}

					switch ($key) {
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('name '.$match);
							break;
						case 'status':
							$select->where('status = ?', $val);
							break;
						default:
							$select->where('id ' . $match . '');
							break;
					}
				}
			}
		}

		$select->order(array($sort.' '.$dir));

//echo $select->__toString();exit;
		return $select;
	}

}
