<?php
class Customadmin_Model_Mapper_GfBuildingOpenHouse extends Custom_Model_Mapper_GfBuildingOpenHouse
{
	public function fetchByBuildingId($building_id, $future_only = true)
	{
		$select = $this->getDbTable()->select()
			->where('building_id = ?', $building_id)
			->order(array('start_time ASC'));

		if ($future_only) {
			$select->where('end_time > NOW()');
		}

		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
