<?php
class Customadmin_Model_Mapper_GfBuildingImage extends Custom_Model_Mapper_GfBuildingImage
{
	public function fetchOrderedByBuildingId($building_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('bi'=>$this->_table_prefix.'gf_building_images'),array('id','sort_order'))
			->joinLeft(array('i'=>$this->_table_prefix.'gf_images'),'bi.image_id = i.id',array('title','image','alt_text'))
			->where('bi.building_id = ?', $building_id)
			->order('bi.sort_order');
		$result = $this->getDbTable()->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
