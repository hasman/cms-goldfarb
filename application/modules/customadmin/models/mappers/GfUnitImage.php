<?php
class Customadmin_Model_Mapper_GfUnitImage extends Custom_Model_Mapper_GfUnitImage
{
	public function fetchOrderedByUnitId($unit_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('ui'=>$this->_table_prefix.'gf_unit_images'),array('id','sort_order'))
			->joinLeft(array('i'=>$this->_table_prefix.'gf_images'),'ui.image_id = i.id',array('title','image','alt_text'))
			->columns(array('alt_text'=>new Zend_Db_Expr('IF (ui.alt_text = "" OR ui.alt_text IS NULL, i.alt_text, ui.alt_text)')))
			->where('ui.unit_id = ?', $unit_id)
			->order('ui.sort_order');
		$result = $this->getDbTable()->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}

}
