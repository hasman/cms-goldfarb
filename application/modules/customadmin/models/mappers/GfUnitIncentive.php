<?php
class Customadmin_Model_Mapper_GfUnitIncentive extends Custom_Model_Mapper_GfUnitIncentive
{
	public function deleteByUnitId($unit_id)
	{
		return $this->getDbTable()->delete(array('unit_id = ?' => $unit_id));
	}
}
