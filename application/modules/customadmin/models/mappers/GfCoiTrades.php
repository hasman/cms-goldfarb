<?php
class Customadmin_Model_Mapper_GfCoiTrades extends Custom_Model_Mapper_GfCoiTrades
{
	public function listSearch($sort = '', $dir = 'ASC', $search = '') {
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('b'=>'gf_coi_trades'),'*')	
			;	
		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					if (strpos($val, '"') !== false) {
						$match = '= "' . str_replace('"', '', $val) . '"';
					} else {
						$match = 'LIKE "%' . addslashes($val) . '%"';
					}
					switch ($key) {
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('name '.$match);
							break;
						default:
							$select->where('id ' . $match . '');
							break;
					}
				}
			}
		}
			
		$select->order(array($sort.' '.$dir));

//echo $select->__toString();exit;
		return $select;
	}

}