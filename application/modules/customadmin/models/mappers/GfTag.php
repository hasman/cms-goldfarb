<?php
class Customadmin_Model_Mapper_GfTag extends Custom_Model_Mapper_GfTag
{
	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}

	public function fetchByImageId($image_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('it'=>$this->_table_prefix.'gf_image_tags'),'*')
			->where("it.image_id = ?", $image_id)
			->joinLeft(array('t'=>$this->_table_prefix.'gf_tags'),'it.tag_id = t.id','')
			->order(array('t.title ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function doesTagExist($tag_title)
	{
		$select = $this->getDbTable()->select()
			->where("title = ?", $tag_title)
		;

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function fetchStringList($image_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('t'=>$this->_table_prefix.'gf_tags'),'*')
			->order(array('t.title ASC'));
		if ($image_id) {
			$select->joinLeft(array('it'=>$this->_table_prefix.'gf_image_tags'),'it.tag_id = t.id','')
				->where("it.image_id =?",$image_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.$item->title.'"';
		}

		return implode(",",$all);
	}
}
