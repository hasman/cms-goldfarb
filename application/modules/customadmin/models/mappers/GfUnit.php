<?php
class Customadmin_Model_Mapper_GfUnit extends Custom_Model_Mapper_GfUnit
{
	public function listSearch($sort = '', $dir = 'ASC', $search = '') {
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('u'=>'gf_units'),'*')
			->joinLeft(array('b'=>$this->_table_prefix.'gf_buildings'), 'u.building_id = b.id', array('building_name'=>'name'))
			->joinLeft(array('n'=>$this->_table_prefix.'gf_neighborhoods'), 'b.neighborhood_id = n.id', array('neighborhood_name'=>'name'))
			->joinLeft(array('r'=>$this->_table_prefix.'gf_regions'), 'n.region_id = r.id', array('region_name'=>'name'))
		;

		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					if (strpos($val, '"') !== false) {
						$match = '= "' . str_replace('"', '', $val) . '"';
					} else {
						$match = 'LIKE "%' . addslashes($val) . '%"';
					}

					switch ($key) {
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('b.name '.$match. ' OR
								u.code '.$match);
							break;
						case 'building':
							$select->where('u.building_id = ?', $val);
							break;
						case 'neighborhood':
							$select->where('b.neighborhood_id = ?', $val);
							break;
						case 'region':
							$select->where('n.region_id = ?', $val);
							break;
						case 'status':
							if ($val == 'all_vacant') {
								$select->where('u.status IN ("Available","Unavailable")');
							}else {
								$select->where('u.status = ?', $val);
							}
							break;
						case 'unit_type':
							if ($val == 'residential') {
								$select->where('u.residential <> "[]"');
							}elseif ($val == 'commercial') {
								$select->where('u.commercial <> "[]"');
							}elseif ($val == 'commercial_office') {
								$select->where('u.commercial LIKE "%\"Office\"%"');
							}elseif ($val == 'commercial_retail') {
								$select->where('u.commercial LIKE "%\"Retail\"%"');
							}elseif ($val == 'residential_rentals') {
								$select->where('u.residential LIKE "%\"Rentals\"%"');
							}
							break;
						default:
							$select->where('u.id ' . $match . '');
							break;
					}
				}
			}
		}

		$select->order(array($sort.' '.$dir));

//echo $select->__toString();exit;
		return $select;
	}

	public function fetchPairs($region_id = null, $bedrooms = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('u'=>'gf_units'),'*')
			->joinLeft(array('b'=>$this->_table_prefix.'gf_buildings'), 'u.building_id = b.id', array('building_name'=>'name'))
			->joinLeft(array('n'=>$this->_table_prefix.'gf_neighborhoods'), 'b.neighborhood_id = n.id', array('neighborhood_name'=>'name'))
			->joinLeft(array('r'=>$this->_table_prefix.'gf_regions'), 'n.region_id = r.id', array('region_name'=>'name'))
			->order(array('b.name ASC','u.number ASC'));

		if ($region_id) {
			$select->where('r.id = ?', $region_id);
		}

		if (!is_null($bedrooms)) {
			if ($bedrooms >= 3) {
				$select->where('u.bedrooms >= ?', $bedrooms);
			} else {
				$select->where('u.bedrooms = ?', $bedrooms);
			}
		}

		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->building_name.' - '.$row->number;
		}

		return $array;
	}
}
