<?php
class Customadmin_Model_Mapper_GfImage extends Custom_Model_Mapper_GfImage
{
	public function listSearch($sort = '', $dir = 'ASC', $search = '') {
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('i'=>'gf_images'),'*')
			->joinLeft(array('it'=>$this->_table_prefix.'gf_image_tags'), 'it.image_id = i.id', '')
			->joinLeft(array('t'=>$this->_table_prefix.'gf_tags'), 'it.tag_id = t.id', '')
			//->columns(array('tags'=>new Zend_Db_Expr('(GROUP_CONCAT(t.title))')))
			->group('i.id')
		;

		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					if (!is_array($val)) {
						if (strpos($val, '"') !== false) {
							$match = '= "' . str_replace('"', '', $val) . '"';
						} else {
							$match = 'LIKE "%' . addslashes($val) . '%"';
						}
					}

					switch ($key) {
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('i.title '.$match);
							break;
						case 'tag_id':
							if (is_array($val)) {
								$select->where('it.tag_id IN ('.implode(',',$val).')');
								//$select->having(new Zend_Db_Expr('COUNT(DISTINCT it.tag_id) = '.count($val)));
								$sort_array[] = 'COUNT(DISTINCT it.tag_id) DESC';
							}else {
								$select->where('it.tag_id = ?', $val);
							}
							break;
						case 'tag':
							if (is_array($val)) {
								$pieces = [];
								foreach ($val as $v) {
									$pieces[] = "'".$v."'";
								}

								$select->where('t.code IN ('.implode(',',$pieces).')');
								//$select->having(new Zend_Db_Expr('COUNT(DISTINCT it.tag_id) = '.count($val)));
								$sort_array[] = 'COUNT(DISTINCT it.tag_id) DESC';
							}else {
								$select->where('t.code = ?', $val);
							}

							break;
						default:
							$select->where('i.id ' . $match . '');
							break;
					}
				}
			}
		}

		$sort_array[] = $sort.' '.$dir;
		$select->order($sort_array);

//echo $select->__toString();exit;
		return $select;
	}
}
