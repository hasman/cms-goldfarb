<?php
class Customadmin_Model_Mapper_GfRegion extends Custom_Model_Mapper_GfRegion
{
	public function listSearch($sort = '', $dir = 'ASC', $search = '') {
		$select = $this->getDbTable()->select()
		;

		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					if (strpos($val, '"') !== false) {
						$match = '= "' . str_replace('"', '', $val) . '"';
					} else {
						$match = 'LIKE "%' . addslashes($val) . '%"';
					}

					switch ($key) {
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('name '.$match);
							break;
						case 'status':
							$select->where('status = ?', $val);
							break;
						default:
							$select->where('id ' . $match . '');
							break;
					}
				}
			}
		}

		$select->order(array($sort.' '.$dir));

//echo $select->__toString();exit;
		return $select;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('name ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->name;
		}

		return $array;
	}
}
