<?php
class Customadmin_Model_Mapper_GfFloorplan extends Custom_Model_Mapper_GfFloorplan
{
	public function listSearch($sort = '', $dir = 'ASC', $search = '') {
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('f'=>'gf_floorplans'),'*')
			->joinLeft(array('b'=>$this->_table_prefix.'gf_buildings'), 'f.building_id = b.id', array('building_name'=>'name'))
		;

		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					if (strpos($val, '"') !== false) {
						$match = '= "' . str_replace('"', '', $val) . '"';
					} else {
						$match = 'LIKE "%' . addslashes($val) . '%"';
					}

					switch ($key) {
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('f.title '.$match);
							break;
						case 'building':
							$select->where('f.building_id = ?', $val);
							break;
						case 'status':
							$select->where('f.status = ?', $val);
							break;
						default:
							$select->where('f.id ' . $match . '');
							break;
					}
				}
			}
		}

		$select->order(array($sort.' '.$dir));

//echo $select->__toString();exit;
		return $select;
	}

	public function fetchPairs($building_id = null)
	{
		$select = $this->getDbTable()->select()->order('title ASC');

		if ($building_id) {
			$select->where('building_id = ?', $building_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}
}
