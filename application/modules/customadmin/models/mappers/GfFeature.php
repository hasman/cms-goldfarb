<?php
class Customadmin_Model_Mapper_GfFeature extends Custom_Model_Mapper_GfFeature
{
	public function listSearch($sort = '', $dir = 'ASC', $search = '') {
		$select = $this->getDbTable()->select()
		;

		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					if (strpos($val, '"') !== false) {
						$match = '= "' . str_replace('"', '', $val) . '"';
					} else {
						$match = 'LIKE "%' . addslashes($val) . '%"';
					}

					switch ($key) {
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('name '.$match);
							break;
						case 'status':
							$select->where('status = ?', $val);
							break;
						default:
							$select->where('id ' . $match . '');
							break;
					}
				}
			}
		}

		$select->order(array($sort.' '.$dir));

//echo $select->__toString();exit;
		return $select;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('name ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->name;
		}

		return $array;
	}

	public function fetchByUnitId($unit_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('ba'=>$this->_table_prefix.'gf_unit_features'),'*')
			->where("ba.unit_id = ?", $unit_id)
			->joinLeft(array('a'=>$this->_table_prefix.'gf_features'),'ba.feature_id = a.id','')
			->order(array('a.name ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function doesFeatureExist($feature)
	{
		$select = $this->getDbTable()->select()
			->where("name = ?", $feature)
		;

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function fetchFeatureNamesByUnitId($unit_id, $tag_override = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('ba'=>$this->_table_prefix.'gf_unit_features'))
			->where("ba.unit_id = ?", $unit_id)
			->joinLeft(array('a'=>$this->_table_prefix.'gf_features'),'ba.feature_id = a.id','name')
			->order(array('a.name ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->name;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchArrayByUnitId($unit_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('ba'=>$this->_table_prefix.'gf_unit_features'),'*')
			->where("ba.unit_id = ?", $unit_id)
			->joinLeft(array('a'=>$this->_table_prefix.'gf_features'),'ba.feature_id = a.id','')
			->order(array('a.name ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			foreach ($result as $row) {
				$collection[] = $row->feature_id;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchStringList($unit_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('a'=>$this->_table_prefix.'gf_features'),'*')
			->order(array('a.name ASC'));
		if ($unit_id) {
			$select->joinLeft(array('ba'=>$this->_table_prefix.'gf_unit_features'),'ba.amernity_id = a.id','')
				->where("ba.unit_id =?",$unit_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.$item->name.'"';
		}

		return implode(",",$all);
	}
}
