<?php
class Customadmin_Model_Mapper_GfAudit extends Custom_Model_Mapper_GfAudit
{
	/**
	 * @param  string  $sort
	 * @param  string  $dir
	 * @param  array  $search
	 * @return Zend_Db_Select
	 * @throws Exception
	 */
	public function listSearch($sort = '', $dir = 'DESC', $search = []) {
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('a'=>'gf_audits'),'*')
		;

		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					switch ($key) {
						case 'price':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('a.price '.$match);
							break;
						case 'type':
							$select->where('a.type = ?', $val);
							break;
						case 'openhouse':
							$select->where('a.openhouse = ?', $val);
							break;
						case 'status':
							$select->where('a.status = ?', $val);
							break;
						case 'rentcafe_status':
							$select->where('a.rentcafe_status = ?', $val);
							break;
						case 'unit_id':
							$select->where('a.unit_id = ?', $val);
							break;
					}
				}
			}
		}
		$select->order(array($sort.' '.$dir));
		return $select;
	}

}
