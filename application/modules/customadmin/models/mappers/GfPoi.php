<?php
class Customadmin_Model_Mapper_GfPoi extends Custom_Model_Mapper_GfPoi
{
	public function listSearch($sort = '', $dir = 'ASC', $search = '') {
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('poi'=>'gf_pois'),'*')
			->joinLeft(array('poic'=>$this->_table_prefix.'gf_poi_categories'), 'poi.poi_category_id = poic.id', array('category_name'=>'name'))
		;

		if (!empty($search)) {
			foreach ($search AS $key => $val) {
				if (!empty($val)) {
					if (strpos($val, '"') !== false) {
						$match = '= "' . str_replace('"', '', $val) . '"';
					} else {
						$match = 'LIKE "%' . addslashes($val) . '%"';
					}

					switch ($key) {
						case 'keyword':
							if (strpos($val,'"') !== false) {
								$match = '= "'.str_replace('"','',$val).'"';
							}else {
								$match = 'LIKE "%'.$val.'%"';
							}
							$select->where('name '.$match);
							break;
						case 'category_id':
							$select->where('poi_category_id = ?', $val);
							break;
						case 'status':
							$select->where('status = ?', $val);
							break;
						default:
							$select->where('id ' . $match . '');
							break;
					}
				}
			}
		}

		$select->order(array($sort.' '.$dir));

//echo $select->__toString();exit;
		return $select;
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()->order('name ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->name;
		}

		return $array;
	}

	public function fetchByNeighborhoodId($neighborhood_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('np'=>$this->_table_prefix.'gf_neighborhood_pois'),'*')
			->where("np.neighborhood_id = ?", $neighborhood_id)
			->joinLeft(array('p'=>$this->_table_prefix.'gf_pois'),'np.poi_id = p.id','')
			->order(array('p.name ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function doesPoiExist($poi)
	{
		$select = $this->getDbTable()->select()
			->where("name = ?", $poi)
		;

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function fetchPoiNamesByNeighborhoodId($neighborhood_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('np'=>$this->_table_prefix.'gf_neighborhood_pois'))
			->where("np.neighborhood_id = ?", $neighborhood_id)
			->joinLeft(array('p'=>$this->_table_prefix.'gf_pois'),'np.poi_id = p.id','name')
			->order(array('p.name ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->name;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchArrayByNeighborhoodId($neighborhood_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('np'=>$this->_table_prefix.'gf_neighborhood_pois'),'*')
			->where("np.neighborhood_id = ?", $neighborhood_id)
			->joinLeft(array('p'=>$this->_table_prefix.'gf_pois'),'np.poi_id = p.id','')
			->order(array('p.name ASC'));
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			foreach ($result as $row) {
				$collection[] = $row->poi_id;
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchStringList($neighborhood_id = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('p'=>$this->_table_prefix.'gf_pois'),'*')
			->order(array('p.name ASC'));
		if ($neighborhood_id) {
			$select->joinLeft(array('np'=>$this->_table_prefix.'gf_neighborhood_pois'),'np.amernity_id = p.id','')
				->where("np.neighborhood_id =?",$neighborhood_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		$all = array();
		foreach ($result as $item) {
			$all[] = '"'.$item->name.'"';
		}

		return implode(",",$all);
	}
}
