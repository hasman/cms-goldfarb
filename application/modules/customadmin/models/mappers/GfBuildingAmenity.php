<?php
class Customadmin_Model_Mapper_GfBuildingAmenity extends Custom_Model_Mapper_GfBuildingAmenity
{
	public function deleteByBuildingId($building_id)
	{
		return $this->getDbTable()->delete(array('building_id = ?' => $building_id));
	}
}
