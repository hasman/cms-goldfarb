<?php
class Customadmin_Model_Mapper_GfImageTag extends Custom_Model_Mapper_GfImageTag
{
	public function deleteByImageId($blog)
	{
		return $this->getDbTable()->delete(array('image_id = ?' => $blog));
	}

	public function fetchTagNamesByImageId($image_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('it'=>$this->_table_prefix.'gf_image_tags'))
			->where("it.image_id = ?", $image_id)
			->joinLeft(array('t'=>$this->_table_prefix.'gf_tags'),'it.tag_id = t.id','title')
			->order(array('t.title ASC'));

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			foreach ($result as $row) {
				$collection[] = $row->title;
			}
			return $collection;
		} else {
			return false;
		}
	}
}
