<?php
class Customadmin_Model_Mapper_GfNeighborhoodPoi extends Custom_Model_Mapper_GfNeighborhoodPoi
{
	public function deleteByNeighborhoodId($neighborhood_id)
	{
		return $this->getDbTable()->delete(array('neighborhood_id = ?' => $neighborhood_id));
	}
}
