<?php
class Customadmin_Model_Mapper_GfUnitFeature extends Custom_Model_Mapper_GfUnitFeature
{
	public function deleteByUnitId($unit_id)
	{
		return $this->getDbTable()->delete(array('unit_id = ?' => $unit_id));
	}
}
