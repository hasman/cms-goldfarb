<?php
class Customadmin_Bootstrap extends Zend_Application_Module_Bootstrap
{
	protected function _initCustomAdminAutoload()
	{
		$autoloader = new Zend_Application_Module_Autoloader(
			array(
				'namespace' => 'Customadmin_',
				'basePath'  => dirname(__FILE__),
			)
		);
		return $autoloader;
	}

	protected function _initPagination()
	{
		Zend_Paginator::setDefaultScrollingStyle('Sliding');
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('pagination_control.phtml');
		Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH . '/helpers');
	}
}
