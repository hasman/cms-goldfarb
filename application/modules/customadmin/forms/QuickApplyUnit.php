<?php
class Customadmin_Form_QuickApplyUnit extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/default.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/quick-apply-unit/save' );
		$this->setAttrib( 'id', 'quickApplyUnitForm' );

		//Options
		$mapper = new Customadmin_Model_Mapper_GfRegion();
		$regions = $mapper->fetchPairs();

		$mapper = new Customadmin_Model_Mapper_GfUnit();
		$units = $mapper->fetchPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['region_id'] = $this->createElement( 'select', 'region_id' )
			->setLabel('Region')
			->setRequired(true)
			->setMultiOptions($regions)
			->setAttribs(array("class"=>"form-control","disabled"=>"disabled"))
			->setDecorators( $decor_element );
		$elements['bedrooms'] = $this->createElement( 'select', 'bedrooms' )
			->setLabel('Bedrooms')
			->setRequired(true)
			->setMultiOptions(array('0'=>'0','1'=>'1','2'=>'2','3'=>'3+'))
			->setAttribs(array("class"=>"form-control","disabled"=>"disabled"))
			->setDecorators( $decor_element );
		$elements['unit_id'] = $this->createElement( 'select', 'unit_id' )
			->setLabel('Unit')
			->setRequired(true)
			->setMultiOptions($units)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
