<?php
class Customadmin_Form_Coi extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/coi.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/coi/save' );
		$this->setAttrib( 'id', 'coiForm' );

		//Options
		$limits = array('2'=>'$2 Million','3'=>'$3 Million', '5'=>'$5 Million', '10'=>'$10 Million');


		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['code'] = $this->createElement( 'hidden' , 'code' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['name'] = $this->createElement( 'text', 'name' )
			->setLabel('Name')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['description'] = $this->createElement( 'textarea', 'description' )
			->setLabel('Description')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>"8"))
			->setDecorators( $decor_element );
		$elements['limit'] = $this->createElement( 'select', 'limit' )
			->setLabel('Limit')
			->setRequired(true)
			->setMultiOptions($limits)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
