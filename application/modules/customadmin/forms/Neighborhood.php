<?php
class Customadmin_Form_Neighborhood extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/default.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/neighborhood/save' );
		$this->setAttrib( 'id', 'neighborhoodForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');

		$mapper = new Customadmin_Model_Mapper_GfRegion();
		$regions = $mapper->fetchPairs();

		$mapper = new Customadmin_Model_Mapper_GfPoi();
		$pois = $mapper->fetchPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['code'] = $this->createElement( 'hidden' , 'code' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['sort_order'] = $this->createElement( 'hidden' , 'sort_order' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['name'] = $this->createElement( 'text', 'name' )
			->setLabel('Name')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['description'] = $this->createElement( 'textarea', 'description' )
			->setLabel('Description')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control","rows"=>"8"))
			->setDecorators( $decor_element );
		$elements['region_id'] = $this->createElement( 'select', 'region_id' )
			->setLabel('Region')
			->setMultiOptions($regions)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['pois'] = $this->createElement( 'select', 'pois' )
			->setLabel('Points of Interest')
			->setRequired(false)
			->setMultiOptions($pois)
			->setAttribs(array("class"=>"form-control","multiple"=>"multiple"))
			->setRegisterInArrayValidator(false)
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
