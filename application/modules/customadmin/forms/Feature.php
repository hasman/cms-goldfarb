<?php
class Customadmin_Form_Feature extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript'  => 'forms/default.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/feature/save' );
		$this->setAttrib( 'id', 'featureForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');
		$streeteasy_tags = array(''=>'',
			"doorman"=>"Doorman",
			"gym"=>"Gym",
			"pool"=>"Pool",
			"elevator"=>"Elevator",
			"garage"=>"Garage",
			"parking"=>"Parking",
			"balcony"=>"Balcony",
			"storage"=>"Storage",
			"patio"=>"Patio",
			"fireplace"=>"Fireplace",
			"washerDryer"=>"Washer Dryer",
			"dishwasher"=>"Dishwasher",
			"furnished"=>"Furnished",
			"pets"=>"Pets",
		);
		$zillow_tags = array('' => '',
			"NoPets" => "Pets Allowed",
			"Cats" => "Cats Allowed",
			"SmallDogs" => "Small Dogs Allowed",
			"LargeDogs" => "Large Dogs Allowed",
			"DisabledAccess" => "Disabled Access",
			"Doorman" => "Doorman",
			"Elevator" => "Elevator",
			"Pool" => "Pool",
			"Waterfront" => "Waterfront",
			"FitnessCenter" => "Fitness Center",
			"OnsiteLaundry" => "Onsite Laundry",
			"Gas" => "Gas Included",
			"Internet" => "Internet Included",
			"Cable" => "Cable TV Included",
			"SatTV" => "Sat TV Included",
			"NearTransportation" => "Near Transportation",
			"Patio" => "Patio",
			"Fireplace" => "Fireplace",
			"Garden" => "Garden",
		);
		$apartmentcom_tags = array('' => '',
			"AdditionalStorage" => "AdditionalStorage",
			"AirConditioner" => "AirConditioner",
			"Alarm" => "Alarm",
			"Availability24Hours" => "Availability24Hours",
			"Balcony" => "Balcony",
			"BasketballCourt" => "BasketballCourt",
			"Bilingual" => "Bilingual",
			"BoatDocks" => "BoatDocks",
			"BusinessCenter" => "BusinessCenter",
			"Cable" => "Cable",
			"Carpet" => "Carpet",
			"Carport" => "Carport",
			"CarWashArea" => "CarWashArea",
			"CeilingFan" => "CeilingFan",
			"ChildCare" => "ChildCare",
			"ClubDiscount" => "ClubDiscount",
			"ClubHouse" => "ClubHouse",
			"Concierge" => "Concierge",
			"ConferenceRoom" => "ConferenceRoom",
			"ControlledAccess" => "ControlledAccess",
			"Courtyard" => "Courtyard",
			"CoverPark" => "CoverPark",
			"DishWasher" => "DishWasher",
			"Disposal" => "Disposal",
			"DoorAttendant" => "DoorAttendant",
			"DoubleSinkVanity" => "DoubleSinkVanity",
			"Dryer" => "Dryer",
			"Elevator" => "Elevator",
			"Fireplace" => "Fireplace",
			"FitnessCenter" => "FitnessCenter",
			"FramedMirrors" => "FramedMirrors",
			"FreeWeights" => "FreeWeights",
			"Furnished" => "Furnished",
			"FurnishedAvailable" => "FurnishedAvailable",
			"GamingStations" => "GamingStations",
			"Garage" => "Garage",
			"Gate" => "Gate",
			"GroceryService" => "GroceryService",
			"GroupExcercise" => "GroupExcercise",
			"GuestRoom" => "GuestRoom",
			"Handrails" => "Handrails",
			"HardwoodFlooring" => "HardwoodFlooring",
			"Heat" => "Heat",
			"HighSpeed" => "HighSpeed",
			"Housekeeping" => "Housekeeping",
			"HouseSitting" => "HouseSitting",
			"IslandKitchen" => "IslandKitchen",
			"JoggingWalkingTrails" => "JoggingWalkingTrails",
			"LakeAccess" => "LakeAccess",
			"LakeFront" => "LakeFront",
			"LargeClosets" => "LargeClosets",
			"Laundry" => "Laundry",
			"Library" => "Library",
			"LinenCloset" => "LinenCloset",
			"MealService" => "MealService",
			"MediaRoom" => "MediaRoom",
			"Microwave" => "Microwave",
			"MultiUseRoom" => "MultiUseRoom",
			"NightPatrol" => "NightPatrol",
			"OnSiteMaintenance" => "OnSiteMaintenance",
			"OnSiteManagement" => "OnSiteManagement",
			"Other" => "Other",
			"PackageReceiving" => "PackageReceiving",
			"Pantry" => "Pantry",
			"Patio" => "Patio",
			"PerDiemAccepted" => "PerDiemAccepted",
			"Playground" => "Playground",
			"Pool" => "Pool",
			"PrivateBalcony" => "PrivateBalcony",
			"PrivatePatio" => "PrivatePatio",
			"Racquetball" => "Racquetball",
			"Range" => "Range",
			"RecRoom" => "RecRoom",
			"Recycling" => "Recycling",
			"Refrigerator" => "Refrigerator",
			"Satellite" => "Satellite",
			"Sauna" => "Sauna",
			"ShortTermLease" => "ShortTermLease",
			"Skylight" => "Skylight",
			"SmokeFree" => "SmokeFree",
			"Spa" => "Spa",
			"StorageSpace" => "StorageSpace",
			"Sundeck" => "Sundeck",
			"TennisCourt" => "TennisCourt",
			"TileFlooring" => "TileFlooring",
			"TVLounge" => "TVLounge",
			"ValetTrash" => "ValetTrash",
			"VaultedCeiling" => "VaultedCeiling",
			"View" => "View",
			"Vintage" => "Vintage",
			"VinylFlooring" => "VinylFlooring",
			"VolleyballCourt" => "VolleyballCourt",
			"Washer" => "Washer",
			"WD_Hookup" => "WD_Hookup",
			"WheelChair" => "WheelChair",
			"WindowCoverings" => "WindowCoverings",
			"WirelessInternet" => "WirelessInternet",
		);

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['name'] = $this->createElement( 'text', 'name' )
			->setLabel('Name')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['streeteasy_tag'] = $this->createElement( 'select', 'streeteasy_tag' )
			->setLabel('Streeteasy Tag')
			->setMultiOptions($streeteasy_tags)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['zillow_tag'] = $this->createElement( 'select', 'zillow_tag' )
			->setLabel('Zillow Tag')
			->setMultiOptions($zillow_tags)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['apartmentscom_tag'] = $this->createElement( 'select', 'apartmentscom_tag' )
			->setLabel('Apartments.com Tag')
			->setMultiOptions($apartmentcom_tags)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		/*
		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		*/

		$this->addElements( $elements );
	}

}
