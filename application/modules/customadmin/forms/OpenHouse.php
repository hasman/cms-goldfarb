<?php
class Customadmin_Form_OpenHouse extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/default-ajax.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/building/openhouse-save' );
		$this->setAttrib( 'id', 'openhouseForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['building_id'] = $this->createElement( 'hidden', 'building_id' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['date'] = $this->createElement( 'text', 'date' )
			->setLabel('Date')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control datepicker"))
			->setDecorators( $decor_element );
		$elements['start'] = $this->createElement( 'text', 'start' )
			->setLabel('Start Time')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control timepicker"))
			->setDecorators( $decor_element );
		$elements['end'] = $this->createElement( 'text', 'end' )
			->setLabel('End Time')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control timepicker"))
			->setDecorators( $decor_element );
		$elements['details'] = $this->createElement( 'text', 'details' )
			->setLabel('Details')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );


		/*
		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		*/

		$this->addElements( $elements );
	}

}
