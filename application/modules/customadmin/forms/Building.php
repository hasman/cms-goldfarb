<?php
class Customadmin_Form_Building extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/building.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/building/save' );
		$this->setAttrib( 'id', 'buildingForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');

		$mapper = new Customadmin_Model_Mapper_GfNeighborhood();
		$neighborhoods = $mapper->fetchPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['code'] = $this->createElement( 'hidden' , 'code' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['name'] = $this->createElement( 'text', 'name' )
			->setLabel('Name')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['neighborhood_id'] = $this->createElement( 'select', 'neighborhood_id' )
			->setLabel('Neighborhood')
			->setRequired(false)
			->setMultiOptions($neighborhoods)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['commercial'] = $this->createElement( 'multiCheckbox', 'commercial' )
			->setLabel('Commercial')
			->setRequired(false)
			->setMultiOptions(array("Retail"=>"Retail","Office"=>"Office"))
			->setAttribs(array("class"=>"form-control-checkbox"))
			->setDecorators( $decor_element );
		$elements['residential'] = $this->createElement( 'multiCheckbox', 'residential' )
			->setLabel('Residential')
			->setRequired(false)
			->setMultiOptions(array("Rentals"=>"Rentals"))
			->setAttribs(array("class"=>"form-control-checkbox"))
			->setDecorators( $decor_element );
		$elements['description'] = $this->createElement( 'textarea', 'description' )
			->setLabel('Description')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control","rows"=>"8"))
			->setDecorators( $decor_element );
		$elements['voyager_code'] = $this->createElement( 'text', 'voyager_code' )
			->setLabel('Voyager Code')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['yardi_code'] = $this->createElement( 'text', 'yardi_code' )
			->setLabel('RentCafe Code')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['active_streeteasy'] = $this->createElement( 'select', 'active_streeteasy' )
			->setLabel('Street Easy Posts')
			->setRequired(false)
			->setMultiOptions(array("Yes"=>"Yes","No"=>"No"))
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['elise_text'] = $this->createElement( 'textarea', 'elise_text' )
			->setLabel('Elise Text')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>"8",'placeholder'=>'Elise text {{walkin_key}} wrapped'))
			->setDecorators( $decor_element );
		$elements['address_display_name'] = $this->createElement( 'text', 'address_display_name' )
			->setLabel('Display Name')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['cross_street'] = $this->createElement( 'text', 'cross_street' )
			->setLabel('Cross Street')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['street1'] = $this->createElement( 'text', 'street1' )
			->setLabel('Street 1')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['street2'] = $this->createElement( 'text', 'street2' )
			->setLabel('Street 2')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['city'] = $this->createElement( 'text', 'city' )
			->setLabel('City')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['state'] = $this->createElement( 'text', 'state' )
			->setLabel('State')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['zip'] = $this->createElement( 'text', 'zip' )
			->setLabel('Zip Code')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['meta_title'] = $this->createElement( 'text', 'meta_title' )
			->setLabel('Page Title')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['meta_description'] = $this->createElement( 'textarea', 'meta_description' )
			->setLabel('Meta Data Description')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>"4"))
			->setDecorators( $decor_element );

		$elements['amenities'] = $this->createElement( 'text', 'amenities' )
			->setLabel('Amenities')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		/*
		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		*/

		$this->addElements( $elements );
	}

}
