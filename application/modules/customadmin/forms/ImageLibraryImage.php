<?php
class Customadmin_Form_ImageLibraryImage extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/image-library-image.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/image-library/save' );
		$this->setAttrib( 'id', 'imageLibraryImageForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Title')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['alt_text'] = $this->createElement( 'text', 'alt_text' )
			->setLabel('Alt Text')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['image'] = $this->createElement( 'hidden', 'image' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');

		$elements['tags'] = $this->createElement( 'text', 'tags' )
			->setLabel('Tags')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		/*
		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		*/

		$this->addElements( $elements );
	}

}
