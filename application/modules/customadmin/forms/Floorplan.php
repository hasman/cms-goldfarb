<?php
class Customadmin_Form_Floorplan extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/floorplan.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/floorplan/save' );
		$this->setAttrib( 'id', 'floorplanForm' );

		//Options
		$statuses = array('Enabled'=>'Enabled', 'Disabled'=>'Disabled');

		$mapper = new Customadmin_Model_Mapper_GfBuilding();
		$buildings = $mapper->fetchPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['title'] = $this->createElement( 'text', 'title' )
			->setLabel('Layout Name')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['image'] = $this->createElement( 'hidden', 'image' )
			->removeDecorator('label')
			->removeDecorator('HtmlTag');

		$elements['building_id'] = $this->createElement( 'select', 'building_id' )
			->setLabel('Building')
			->setRequired(false)
			->setMultiOptions($buildings)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		/*
		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		*/

		$this->addElements( $elements );
	}

}
