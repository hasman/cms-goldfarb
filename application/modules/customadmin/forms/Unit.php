<?php
class Customadmin_Form_Unit extends Zend_Form
{

	public function init()
	{
		//Decorators
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('AdminDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'forms/unit.phtml' ) ) ) );

		//Validators

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/admin/unit/save' );
		$this->setAttrib( 'id', 'unitForm' );

		//Options
		$statuses = array('Available'=>'Available', 'Unavailable'=>'Unavailable', 'Occupied'=>'Occupied');
		$yn = array('Yes'=>'Yes','No'=>'No');
		$ny = array('No'=>'No','Yes'=>'Yes');
		$enum = array('Enabled'=>'Enabled','Disabled'=>'Disabled');

		$concession_languages = array(
			'' => 'None',
			'A' => "A - 1 month free or landlord pays 1 month broker's fee on a 12 month lease* Gross rent is $%s net effective rent is $%s. (promotions apply to new residents and immediate move in only)",
			'B' => "B - Your rent is $%s per month. You will receive the 2nd month free, making your net effective rent $%s (promotions apply to new residents and immediate move in only)",
			'C' => "C - %s month(s) free on a %s month lease rent is $%s net effective rent is $%s. (promotions apply to new residents and immediate move in only)",
			'D' => "D - '$%s reflects a concession of %s months(s) free. Monthly rent is $%s for the lease term. (promotions apply to new residents and immediate move in only)",
		);

		$mapper = new Customadmin_Model_Mapper_GfBuilding();
		$buildings = $mapper->fetchPairs();

		$lease_terms = [];
		for ($i=6; $i<=36; $i++) {
			$lease_terms[$i] = $i." Months";
		}

		$concession_months = [''=>'None','0.5'=>'.5 (1/2) Month'];
		for ($i=1; $i<=4; $i++) {
			$s = 's'; if ($i == 1) $s = '';
			$concession_months[$i] = $i." Month".$s;
		}

		$mapper = new Customadmin_Model_Mapper_GfFeature();
		$features = $mapper->fetchPairs();

		$mapper = new Customadmin_Model_Mapper_GfIncentive();
		$incentives = $mapper->fetchPairs();

		$mapper = new Customadmin_Model_Mapper_GfFloorplan();
		$floorplans = $mapper->fetchPairs();

		//Form fields
		$elements['id'] = $this->createElement( 'hidden', 'id' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['cmd'] = $this->createElement( 'hidden' , 'cmd' )->removeDecorator('label')->removeDecorator('HtmlTag');
		$elements['code'] = $this->createElement( 'hidden' , 'code' )->removeDecorator('label')->removeDecorator('HtmlTag');

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Status')
			->setMultiOptions($statuses)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['featured'] = $this->createElement( 'select', 'featured' )
			->setLabel('Featured')
			->setMultiOptions($ny)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['floor'] = $this->createElement( 'text', 'floor' )
			->setLabel('Floor')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['square_footage'] = $this->createElement( 'text', 'square_footage' )
			->setLabel('Square Footage')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['square_footage_status'] = $this->createElement( 'select', 'square_footage_status' )
			->setLabel('Sq. Footage Syndication')
			->setMultiOptions($enum)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['floorplan_id'] = $this->createElement( 'select', 'floorplan_id' )
			->setLabel('Floorplan')
			->setMultiOptions($floorplans)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['virtual_tour_id'] = $this->createElement( 'text', 'virtual_tour_id' )
			->setLabel('Matterport Tour ID')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['description'] = $this->createElement( 'textarea', 'description' )
			->setLabel('Description')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control","rows"=>"8"))
			->setDecorators( $decor_element );
		$elements['walkin_key'] = $this->createElement( 'text', 'walkin_key' )
			->setLabel('Walk In Key')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['number'] = $this->createElement( 'text', 'number' )
			->setLabel('Number')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['feed_number_override'] = $this->createElement( 'text', 'feed_number_override' )
			->setLabel('Number Override')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['show_number'] = $this->createElement( 'select', 'show_number' )
			->setLabel('Show Number')
			->setMultiOptions($ny)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['building_id'] = $this->createElement( 'select', 'building_id' )
			->setLabel('Building')
			->setRequired(false)
			->setMultiOptions($buildings)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['type'] = $this->createElement( 'select', 'type' )
			->setLabel('Unit Type')
			->setRequired(false)
			->setMultiOptions(array("residential_rentals"=>"Residential - Rentals","commercial_retail"=>"Commercial - Retail","commercial_office"=>"Commercial - Office"))
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['price'] = $this->createElement( 'text', 'price' )
			->setLabel('Price')
			->setRequired(true)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['concession_language'] = $this->createElement( 'select', 'concession_language' )
			->setLabel('Concession Language')
			->setRequired(false)
			->setMultiOptions($concession_languages)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['lease_term'] = $this->createElement( 'select', 'lease_term' )
			->setLabel('Lease Term')
			->setRequired(true)
			->setMultiOptions($lease_terms)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['concession_months'] = $this->createElement( 'select', 'concession_months' )
			->setLabel('Concession Months')
			->setRequired(false)
			->setMultiOptions($concession_months)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['concession_expiration'] = $this->createElement( 'text', 'concession_expiration' )
			->setLabel('Concession Expiration')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control datepicker"))
			->setDecorators( $decor_element );
		$elements['streeteasy_status'] = $this->createElement( 'select', 'streeteasy_status' )
			->setLabel('Post to StreetEasy?')
			->setRequired(false)
			->setMultiOptions($enum)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['disable_openhouse'] = $this->createElement( 'select', 'disable_openhouse' )
			->setLabel('Disable Open House?')
			->setRequired(false)
			->setMultiOptions($ny)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['carousel_vimeo_url'] = $this->createElement( 'text', 'carousel_vimeo_url' )
			->setLabel('Unit Carousel Vimeo Video URL (Empty for Disabled)')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );

		$elements['meta_title'] = $this->createElement( 'text', 'meta_title' )
			->setLabel('Page Title')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control"))
			->setDecorators( $decor_element );
		$elements['meta_description'] = $this->createElement( 'textarea', 'meta_description' )
			->setLabel('Meta Data Description')
			->setRequired(false)
			->setAttribs(array("class"=>"form-control","rows"=>"4"))
			->setDecorators( $decor_element );

		$elements['features'] = $this->createElement( 'select', 'features' )
			->setLabel('Features')
			->removeDecorator('label')
			->setRequired(false)
			->setMultiOptions($features)
			->setAttribs(array("class"=>"form-control","multiple"=>"multiple"))
			->setRegisterInArrayValidator(false)
			->setDecorators( $decor_element );

		$elements['incentives'] = $this->createElement( 'select', 'incentives' )
			->setLabel('Incentives (First Incentive Displays)')
			->removeDecorator('label')
			->setRequired(false)
			->setMultiOptions($incentives)
			->setAttribs(array("class"=>"form-control","multiple"=>"multiple"))
			->setRegisterInArrayValidator(false)
			->setDecorators( $decor_element );


		$this->addElements( $elements );
	}

}
