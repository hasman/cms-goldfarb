<?php

class SearchController extends Zend_Controller_Action
{
	/** @var Zend_Db_Adapter_Mysqli $_db */
	protected $_db;

	function init()
	{
		$this->view->placeholder('body_class')->set("search-page");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$options = $bootstrap->getOptions();
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->view->addsearch_domain = $domain = $_SERVER['HTTP_HOST'];
		if (isset($options['site']['domain'])) {
			$domain = $options['site']['domain'];
		}
	}

    public function indexAction()
    {
	    $generator = new Cny_Model_HtmlBuild();
	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();
	    $this->view->headLink()->appendStylesheet('/index/css/?v='.$generator->cssStamp(0));

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    $announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
	    if ($announcement_bar_status == 'enable' && $announcement_bar_page == '') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

	    //promotional pop-up
	    $promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
	    $promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
	    if ($promo_popup_status == 'Enabled' && ($promo_popup_page == '')) {
		    $promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
		    $promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
		    $promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
		    $extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

		    $this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$promo_popup_modal_code.'", "'.$promo_popup_delay.'", "'.$promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
	    }

	    $mapper = new Admin_Model_Mapper_SiteSettingValue();
	    if ($mapper->getValueByCode('addsearch_status') == 'Enabled') {
		    $this->view->addsearch_key = $mapper->getValueByCode('addsearch_site_key');

		    $this->render("addsearch");
	    }else {
		    $mapper = new Application_Model_Mapper_SearchIndex();
		    $this->view->q = $q = $this->getParam('q', null);
		    $this->view->page = $page = $this->getRequest()->getParam('page', 1);
		    $this->view->per_page = $per_page = 10;

		    $data = array();
		    if (!is_null($q)) {
			    if ($results = $mapper->query($q)) {
				    foreach ($results as $row) {
					    $data[] = array(
						    'id' => $row['id'],
						    'title' => $row['title'],
						    'description' => $row['description'],
						    'url' => $row['url'],
						    '_score' => $row['_score'],
					    );
				    }
			    }
		    }
		    //$this->view->data = $data;

		    $paginator = Zend_Paginator::factory($data);
		    $paginator->setCurrentPageNumber($page);
		    $paginator->setItemCountPerPage($per_page);
		    $this->view->data = $paginator;
	    }
    }

    public function updateIndexAction()
    {
    	echo "<pre>\n";

    	// Pages
    	$mapper = new Admin_Model_Mapper_Page();
		$result = $mapper->fetchAll();
		foreach ($result as $entity) {
			$mapper->save($entity);
			echo '.';
		}

    	// Blog
    	$mapper_bp = new Admin_Model_Mapper_BlogPost();
		$result = $mapper_bp->fetchAll();
		foreach ($result as $entity) {
			$mapper_bp->save($entity);
			echo '.';
		}

		echo "\ndone!\n";
	    die;
    }

    public function testAction()
    {
    	Zend_Debug::dump(APPLICATION_ENV);
    	die;
    }
}

