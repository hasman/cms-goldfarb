<?php

class CartController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db = Zend_Db_Table::getDefaultAdapter();

	    $options = Zend_Registry::get('configuration')->toArray();
	    $this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();
	    $this->view->free_shipping_status = $this->free_shipping_status = $settings_mapper->getValueByCode('shipping_free_standard_shipping_status');
	    $this->view->free_shipping_threshold = $this->free_shipping_threshold = $settings_mapper->getValueByCode('shipping_free_standard_shipping_threshold');

        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
        if( !$auth->hasIdentity() ){
            $this->view->customer = $this->_customer = null;
        }else {
            $this->view->customer = $this->_customer = $auth->getIdentity();

            //Cny_Commerce_Cart::mergeCart($this->_customer->id);
        }
        $post_auth_redirect = new Zend_Session_Namespace('post_auth_redirect');
        $post_auth_redirect->url = "/cart";

        $this->_cart = new Zend_Session_Namespace('cart');
        if (!is_array($this->_cart->items)) $this->_cart->items = array();

	    //if checkout disabled redirect to home (shouldn't be here)
	    if ($settings_mapper->getValueByCode('checkout_status') == 'Disabled') {
		    if ($this->getRequest()->isXmlHttpRequest()) {
		    	echo "";exit;
		    }else {
			    $this->redirect("/");
		    }
	    }
    }

    public function indexAction()
    {
        $this->view->placeholder('body_class')->append('cart-page');

        unset($this->_checkout->error);

	    $this->view->products = array(); $sub_total = 0;

	    $this->view->promo_code = $promo_code = $this->_cart->promo_code = ($this->_cart->promo_code)?$this->_cart->promo_code:$this->_getParam('promo_code','');
	    if ($this->_getParam('promo_code','')) {
		    $this->view->promo_code = $promo_code = $this->_cart->promo_code = $this->_getParam('promo_code','');
	    }

	    $this->setupCart($promo_code);
    }

    public function addAction()
    {
        $output = [];
        $product_ids = $this->_getParam("product_id",'');
	    $variant_ids = $this->_getParam("variant_id",'');
        $qtys = $this->_getParam("qty",1);

        if (!is_array($product_ids)) {
	        $product_ids = array($product_ids);
        }
	    if (!is_array($variant_ids)) {
		    $variant_ids = array($variant_ids);
	    }
	    if (!is_array($qtys)) {
		    $qtys = array($qtys);
	    }

	    $subtotal = 0;

        $mapper = new Application_Model_Mapper_Product();
        foreach ($product_ids as $k => $id) {
        	$qty = (!is_array($qtys) || !array_key_exists($k,$qtys))?1:$qtys[$k];

	        $prod = $mapper->find($id);
	        if (!$prod || $prod->status == 'Disabled' || $prod->status == 'Unavailable') continue;

	        if ($id && $qty) {
	        	$found = false;
	        	foreach ($this->_cart->items as $k => $row) {
			        if ($id == $row['product_id']) {
				        $old_qty = $this->_cart->items[$k]['qty'];
				        $this->_cart->items[$k]['qty'] = $old_qty+$qty;
				        $found = true;
			        }
		        }
		        if (!$found) {
			        $this->_cart->items[] = array('product_id' => $id, 'qty' => $qty);
		        }
	        }
        }

	    $variant_mapper = new Application_Model_Mapper_ProductVariant();
	    foreach ($variant_ids as $k => $id) {
		    $qty = (!is_array($qtys) || !array_key_exists($k,$qtys))?1:$qtys[$k];

		    $variant = $variant_mapper->find($id);
		    if ($variant) {
			    $prod = $mapper->find($variant->parent_product_id);
		    }
			if (!$variant || $variant->status == 'Disabled' || $variant->status == 'Unavailable' || $prod->status == 'Disabled' || $prod->status == 'Unavailable') continue;

		    if ($id && $qty) {
			    $found = false;
			    foreach ($this->_cart->items as $k => $row) {
				    if ($id == $row['variant_id']) {
					    $old_qty = $this->_cart->items[$k]['qty'];
					    $this->_cart->items[$k]['qty'] = $old_qty+$qty;
					    $found = true;
				    }
			    }
			    if (!$found)  {
				    $this->_cart->items[] = array('product_id' => $prod->id, 'variant_id' => $id, 'qty' => $qty);
			    }
		    }
	    }

	    $this->storeCart();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $output['action'] = 'add';
            //$output['count'] =  Cny_Commerce_Cart::count();
            //$output['total'] =  number_format(Cny_Commerce_Cart::total(), 2);
            //$output['cart_items'] = Cny_Commerce_Cart::getItemsAsArray();

	        $promo_code = $this->_cart->promo_code = ($this->_cart->promo_code)?$this->_cart->promo_code:'';

	        $this->setupCart($promo_code);

	        $output['items'] = $this->view->products;

	        //free shipping data
	        $settings_mapper = new Application_Model_Mapper_SiteSettingValue();
	        $free_ship_threshold = $settings_mapper->getValueByCode('shipping_free_standard_shipping_threshold');
	        $output['free_shipping'] = array(
	        	'status' => $settings_mapper->getValueByCode('shipping_free_standard_shipping_status'),
	        	'needed' => ($free_ship_threshold - $this->_cart->sub_total > 0)?number_format($free_ship_threshold - $this->_cart->sub_total,2):0
	        );

            echo json_encode($output);
        } else {
            $this->_redirect("/cart");
        }
        exit;
    }

    public function removeAction()
    {
        $output = [];
        $product_id = $this->_getParam("product_id",'');
	    $variant_id = $this->_getParam("variant_id",'');

        if( $product_id ){
        	foreach ($this->_cart->items as $k => $row) {
        		if ($product_id == $row['product_id']) {
			        unset($this->_cart->items[$k]);

			        break;
		        }
	        }
        }
	    if( $variant_id ){
		    foreach ($this->_cart->items as $k => $row) {
			    if ($variant_id == $row['variant_id']) {
				    unset($this->_cart->items[$k]);

				    break;
			    }
		    }
	    }

        $this->storeCart();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $output['action'] = 'remove';
            //$output['count'] =  Cny_Commerce_Cart::count();
            //$output['total'] =  number_format(Cny_Commerce_Cart::total(), 2);
            //$output['cart_items'] = Cny_Commerce_Cart::getItemsAsArray();
            echo json_encode($output);
        } else {
            $this->_redirect("/cart");
        }
        exit;
    }

    public function updateAction()
    {
        if( $this->getRequest()->isPost() ){
	        $products = $this->_getParam("products",array());
	        $variants = $this->_getParam("variants",array());

	        foreach ($products as $product_id => $qty) {
		        foreach ($this->_cart->items as $k => $row) {
			        if ($product_id == $row['product_id']) {
				        if ($qty == 0) unset($this->_cart->items[$k]);
				        else $this->_cart->items[$k]['qty'] = $qty;

				        break;
			        }
		        }
	        }
	        foreach ($variants as $variant_id => $qty) {
		        foreach ($this->_cart->items as $k => $row) {
			        if ($variant_id == $row['variant_id']) {
				        if ($qty == 0) unset($this->_cart->items[$k]);
				        else $this->_cart->items[$k]['qty'] = $qty;

				        break;
			        }
		        }
	        }
        }

        $this->storeCart();

        $this->_redirect("/cart");
        exit;
    }

	public function itemCountAction()
	{
		$product_mapper = new Application_Model_Mapper_Product();
		$variant_mapper = new Application_Model_Mapper_ProductVariant();

		$total_qty = 0;

		foreach ($this->_cart->items as $k => $item) {
			$product = $product_mapper->find($item['product_id']);

			if ($product->status == 'Disabled' || $product->status == 'Unavailable') {
				break;
			}

			if (isset($item['variant_id']) && $item['variant_id']) {
				$variant = $variant_mapper->find($item['variant_id']);

				if ($variant->status == 'Disabled' || $variant->status == 'Unavailable') {
					break;
				}
			}

			$total_qty += $item['qty'];
		}

		echo $total_qty;
		exit;
	}

    public function emptyAction()
    {
        if ($this->_customer->id) {
            //$this->_db->delete($this->_table_prefix."customer_cart","customer_id={$this->_customer->id}");
        }

        $this->_cart->setExpirationSeconds(1);
        $this->_redirect("/cart");
    }

    private function storeCart()
    {
        if (isset($this->_customer->id)) {
            //Cny_Commerce_Cart::save($this->_customer->id);
        }
    }

	public function promoRemoveAction()
	{
		$this->_cart->promo_code = '';

		$this->redirect("/cart");
	}

	private function setupCart($promo_code)
	{
		$this->view->products = array(); $sub_total = 0;
		$product_mapper = new Application_Model_Mapper_Product();
		$variant_mapper = new Application_Model_Mapper_ProductVariant();
		$product_image_mapper = new Application_Model_Mapper_ProductImage();
		$variant_type_mapper = new Application_Model_Mapper_ProductVariantType();

		//get base subtotal
		$base_sub_total = 0;
		foreach ($this->_cart->items as $k => $item) {
			$prod['product'] = $product_mapper->find($item['product_id']);

			if ($prod['product']->status == 'Disabled' || $prod['product']->status == 'Unavailable') {
				break;
			}

			$price = $prod['product']->price;

			if (isset($item['variant_id']) && $item['variant_id']) {
				$prod['variant'] = $variant_mapper->find($item['variant_id']);

				if ($prod['variant']->status == 'Disabled' || $prod['variant']->status == 'Unavailable') {
					break;
				}

				$price = $prod['variant']->price;
			}

			if ($price < 0) $price = 0;

			$base_sub_total += ($price * $item['qty']);
		}

		//if there is a promo code
		$promo_products = array(); $this->view->promo_message = $this->view->promo = $promo = '';
		if ($promo_code) {
			$mapper = new Application_Model_Mapper_Promo();
			$this->view->promo_message = $mapper->validateCode($promo_code, $base_sub_total);
			if (!$this->view->promo_message) {
				$this->view->promo = $promo = $mapper->fetchByCode($promo_code, $base_sub_total);

				if ($promo) {
					if ($promo->sitewide == 'No') {
						//get prodoucts
						$mapper = new Application_Model_Mapper_PromoThresholdProduct();
						$promo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id);
					}
					$this->_cart->valid_promo_code = $promo_code;

					//BOGO
					/*
					if ($promo->type == 'bogo') {
						$mapper = new Application_Model_Mapper_PromoThresholdProduct();
						$bogo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id,'bogo');

						if (count($bogo_products) == 1) {
							$this->_cart->items[$bogo_products[0]] = array('qty' => 1, 'lic' => 1);
						}else {
							$this->view->bogo_products = array();
							$product_manager = new Application_Model_Mapper_Product();
							$prod = array();

							foreach ($bogo_products as $bogo_product_id) {
								$prod['price'] = 0;
								$prod['product'] = $product_manager->find($bogo_product_id);

								$this->view->bogo_products[] = $prod;
							}
						}
					}
					*/
				} else {
					$this->_cart->promo_code = "";
					if ($this->_cart->valid_promo_code) {
						$this->_cart->promo_code = $this->_cart->valid_promo_code;
						$this->_cart->setExpirationSeconds(1, 'valid_promo_code');
						$this->_cart->setExpirationHops(1, 'valid_promo_code');
						$this->redirect("/cart");
					}
				}
			}else {
				if ($this->_cart->valid_promo_code) {
					$this->_cart->promo_code = $this->_cart->valid_promo_code;
					$this->_cart->setExpirationSeconds(1, 'valid_promo_code');
					$this->_cart->setExpirationHops(1, 'valid_promo_code');
					$this->redirect("/cart");
				}
			}
		}
		//check for auto promo
		if (!$promo_code || !$promo) {
			$this->view->promo_message = ''; //reset promo message in case of invalid promo entered
			$this->_cart->promo_code = '';

			//if no promo code entered check for automatic promos
			$mapper = new Application_Model_Mapper_Promo();
			$this->view->promo = $promo = $mapper->fetchAutomaticPromo($base_sub_total);

			if ($promo) {
				if ($promo->sitewide == 'No') {
					//get products
					$mapper = new Application_Model_Mapper_PromoThresholdProduct();
					$promo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id);
				}

				//BOGO
				/*
				if ($promo->type == 'bogo') {
					$mapper = new Application_Model_Mapper_PromoThresholdProduct();
					$bogo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id,'bogo');

					if (count($bogo_products) == 1) {
						$this->_cart->items[$bogo_products[0]] = array('qty' => 1, 'lic' => 1);
					}else {
						$this->view->bogo_products = array();
						$product_manager = new Application_Model_Mapper_Product();
						$prod = array();

						foreach ($bogo_products as $bogo_product_id) {
							$prod['price'] = 0;
							$prod['product'] = $product_manager->find($bogo_product_id);

							$this->view->bogo_products[] = $prod;
						}
					}
				}
				*/
			}
		}

		foreach ($this->_cart->items as $k => $item) {
			$prod = array();

			$prod['product'] = $product_mapper->find($item['product_id']);

			if ($prod['product']->status == 'Disabled' || $prod['product']->status == 'Unavailable') {
				unset($this->_cart->items[$k]);
				break;
			}

			$prod['qty'] = $item['qty'];
			$prod['price'] = $prod['product']->price;
			$prod['msrp'] = $prod['product']->msrp;

			$primary_image = $product_image_mapper->fetchPrimaryByProductId($prod['product']->id);
			$prod['primary_image'] = ($primary_image && $primary_image->image_src)?$primary_image->image_src:"/custom_assets/images/fm_default_poster.jpg";

			if (isset($item['variant_id']) && $item['variant_id']) {
				$prod['variant'] = $variant_mapper->find($item['variant_id']);

				if ($prod['variant']->status == 'Disabled' || $prod['variant']->status == 'Unavailable') {
					unset($this->_cart->items[$k]);
					break;
				}

				$prod['price'] = $prod['variant']->price;
				$prod['msrp'] = $prod['variant']->msrp;

				$variant_type = $variant_type_mapper->find($prod['variant']->product_variant_type_id);
				$prod['variant_type'] = ($variant_type)?$variant_type->title:'';
			}

			if ($promo && ($promo->sitewide == 'Yes' || ((isset($item['variant_id']) && $item['variant_id'] && in_array($item['variant_id'],$promo_products['variants'])) || ($item['product_id'] && in_array($item['product_id'],$promo_products['products']))) )) {
				//get excluded products
				$mapper = new Application_Model_Mapper_PromoThresholdProduct();
				$exclude_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id,'exclude');
				//$bogo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id,'bogo');
				if (($item['product_id'] && in_array($item['product_id'],$exclude_products['products'])) || (isset($item['variant_id']) && $item['variant_id'] && in_array($item['variant_id'],$exclude_products['variants'])) ) {
					$prod['price'] = $prod['price'];
				}else {
					//if (stripos($promo->licenses,"|{$item['lic']}|") === false) continue;
					if ($promo->type == "fixed") {
						$prod['price'] = $prod['price'] - $promo->amount;
					} elseif ($promo->type == "percent") {
						$prod['price'] = round($prod['price'] - ($prod['price'] * ($promo->amount / 100)), 2);
					} elseif ($promo->type == "percent_msrp") {
						$new_price = round($prod['msrp'] - ($prod['msrp'] * ($promo->amount / 100)), 2);
						$prod['price'] = ($new_price < $prod['price']) ? $new_price : $prod['price'];
					} elseif ($promo->type == "price") {
						$prod['price'] = $promo->amount;
					}
					/*
					elseif ($promo->type == "bogo") {
						foreach ($bogo_products as $bogo_product_id) {
							if ($id == $bogo_product_id) {
								if ($bogo_prod_count < 1) {
									$prod['price'] = 0;
								}else {
									$prod['price'] = $prod['price'];
								}
								$bogo_prod_count++;
							}
						}
					}
					*/
				}
			}else {
				$prod['price'] = $prod['price'];
			}

			if ($prod['price'] < 0) $prod['price'] = 0;

			$prod['line_total'] = number_format($prod['price'] * $prod['qty'], 2);

			$sub_total += ($prod['price'] * $prod['qty']);
			$this->_cart->sub_total = $sub_total;

			$this->view->products[] = $prod;

			//save price in cart session so we can use it in shipping calc
			$this->_cart->items[$k]['price'] = $prod['price'];
		}
	}
}
