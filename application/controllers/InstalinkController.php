<?php

class InstalinkController extends Zend_Controller_Action
{

	function init()
	{
		//$this->view->placeholder('section')->set("home");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->instalinks_url = $instalinks_url = ($settings_mapper->getValueByCode('instalinks_url'))?$settings_mapper->getValueByCode('instalinks_url'):'instagram-links';
		$instalinks_status = ($settings_mapper->getValueByCode('instalinks_status'))?$settings_mapper->getValueByCode('instalinks_status'):'Disabled';
		if ($instalinks_status != 'Enabled') {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}

		//remove site header/footer
		$this->view->placeholder('_header')->set('');
		$this->view->placeholder('_footer')->set('');
	}

    public function indexAction()
    {
	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    if ($announcement_bar_status == 'enable') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

        $this->view->placeholder('body_class')->append('instalink-page');

        $mapper = new Application_Model_Mapper_InstalinksLink();
	    $this->view->posts = $mapper->fetchActive();

	    //Meta Data
	    $this->view->headTitle($settings_mapper->getValueByCode('site_name'), "SET");
	    $this->view->headTitle()->prepend($settings_mapper->getValueByCode('instalinks_page_title'));

	    //Open Graph tags
	    $this->view->doctype(Zend_View_Helper_Doctype::XHTML1_RDFA);
	    $this->view->headMeta()->setProperty('og:title', $settings_mapper->getValueByCode('instalinks_page_title') .' :: '.$site_name);
	    if ($header_image = $settings_mapper->getValueByCode('instalinks_image')) {
		    $this->view->headMeta()->setProperty('og:image', $this->_full_domain . str_replace("userFiles/uploads", "userThumbs",$header_image));
	    }
    }
}

