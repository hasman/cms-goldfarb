<?php
class CheckoutVideoController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db = Zend_Db_Table::getDefaultAdapter();

        $options = Zend_Registry::get('configuration')->toArray();

	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    $this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

	    //if video platform disabled redirect to home (shouldn't be here)
	    if ($settings_mapper->getValueByCode('video_platform_status') == 'Disabled') {
		    $this->redirect("/");
	    }

        $this->view->shipping_countries = $this->view->billing_countries =  $all_countries = Zend_Registry::get('countries')->toArray();
        $this->view->usstates = $this->_states = $this->_provinces = array();

        if ($settings_mapper->getValueByCode('ecommerce_shipping_countries')) {
        	$selected = json_decode($settings_mapper->getValueByCode('ecommerce_shipping_countries'));
	        $this->view->shipping_countries = array();
	        foreach ($selected as $k => $country_code) {
		        $this->view->shipping_countries[$country_code] = $all_countries[$country_code];
	        }
        }
	    if ($settings_mapper->getValueByCode('ecommerce_billing_countries')) {
		    $selected = json_decode($settings_mapper->getValueByCode('ecommerce_billing_countries'));
		    $this->view->billing_countries = array();
		    foreach ($selected as $k => $country_code) {
			    $this->view->billing_countries[$country_code] = $all_countries[$country_code];
		    }
	    }

		$this->_taxStates = (isset($options['tax']))?$options['tax']:array();

		/*
		$this->_goole_api_key = $options['google']['api']['key'];
		$this->_captcha_privkey = $options['google']['recaptcha']['invis']['secret'];
		$this->view->captcha_key = $options['google']['recaptcha']['invis']['key'];
		*/

        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
        if( !$auth->hasIdentity() ){
	        $post_auth_redirect = new Zend_Session_Namespace('post_auth_redirect');
	        $post_auth_redirect->url = $_SERVER['REQUEST_URI'];

            //$this->view->customer = $this->_customer = null;
	        $this->redirect("/login");
        }else {
            $this->view->customer = $this->_customer = $auth->getIdentity();

            //Cny_Commerce_Cart::mergeCart($this->_customer->id);
        }
        $post_auth_redirect = new Zend_Session_Namespace('post_auth_redirect');
        $post_auth_redirect->url = $_SERVER['REQUEST_URI'];

        $this->_billing = array(
            'first_name' => '',
            'last_name' => '',
            'company' => '',
            'email' => '',
            'phone' => '',
            'address' => '',
            'address2' => '',
            'city' => '',
            'region' => '',
            'postcode' => '',
            'country' => ''
        );
        $this->_payment = array(
            'name' => '',
            'cc_num' => '',
            'ccv' => '',
            'exp_month' => '',
            'exp_year' => '',
        );

        $this->_checkout = new Zend_Session_Namespace('lvp_checkout');
        if (!is_array($this->_checkout->billing)) $this->_checkout->billing = $this->_billing;
        if (!is_array($this->_checkout->payment)) $this->_checkout->payment = $this->_payment;
        if (!$this->_checkout->error) $this->_checkout->error = array();

	    //payment method options
	    $this->_payment_methods = array();
	    $this->_default_payment_method = '';
	    if ($settings_mapper->getValueByCode('ecommerce_paypal_status') == 'Enabled') {
	    	$this->_payment_methods['paypal'] = 'paypal';
		    $this->_default_payment_method = 'paypal';
	    }
	    if ($settings_mapper->getValueByCode('ecommerce_amazonpay_status') == 'Enabled') {
		    $this->_payment_methods['amazonpay'] = 'amazon';
		    $this->_default_payment_method = 'amazonpay';

		    /* Amazon Pay Code - Start */
		    $this->view->amazonpay = array();
		    $this->view->amazonpay['seller_id'] = $settings_mapper->getValueByCode('ecommerce_amazonpay_seller_id');

		    $this->view->HeadScript()->appendScript(
<<<amzscript
    var orderReferenceId = null;
    var authRequest = null;
	window.onAmazonLoginReady = function() {
		amazon.Login.setClientId("{$settings_mapper->getValueByCode('ecommerce_amazonpay_client_id')}");
	};
	window.onAmazonPaymentsReady = function() {
		if (typeof showAmazonPayButton === "function") {
			showAmazonPayButton();
		}
	};

amzscript
		    );
		    $this->view->HeadScript()->setAllowArbitraryAttributes(true);
		    $this->view->HeadScript()->appendFile(
			    (APPLICATION_ENV == 'production')?"https://static-na.payments-amazon.com/OffAmazonPayments/us/js/Widgets.js":"https://static-na.payments-amazon.com/OffAmazonPayments/us/sandbox/js/Widgets.js", 'text/javascript', ['defer'=>'defer']
		    );
		    /* Amazon Pay Code - End */
	    }
	    if ($settings_mapper->getValueByCode('ecommerce_payrix_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'payrix';
		    $this->_default_payment_method = 'cc';
	    }elseif ($settings_mapper->getValueByCode('ecommerce_authnet_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'authorize.net';
		    $this->_default_payment_method = 'cc';
	    }elseif ($settings_mapper->getValueByCode('ecommerce_mes_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'mes';
		    $this->_default_payment_method = 'cc';
	    }elseif ($settings_mapper->getValueByCode('ecommerce_stripe_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'stripe';
		    $this->_default_payment_method = 'cc';
	    }elseif ($settings_mapper->getValueByCode('ecommerce_payflowpro_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'payflowpro';
		    $this->_default_payment_method = 'cc';
	    }
	    $this->view->payment_methods = $this->_payment_methods;
    }

    public function indexAction()
    {
	    $this->view->placeholder('body_class')->append('checkout-page');

	    $this->view->method = $payment_method = $this->getRequest()->getParam("method", $this->getRequest()->getParam("payment_method", $this->_default_payment_method));
	    $this->view->copyInfo = 'y';

	    $this->view->error = $this->_checkout->error;
	    $this->_checkout->error = array();

	    //Amazon Pay redirect work around
	    $amz_workaround = false;
	    if ($this->view->method == 'amazonpay') {
		    $this->_amz_workaround = new Zend_Session_Namespace('lvp_amz_workaround');
		    $this->view->billing = $this->_amz_workaround->billing;
		    $this->view->shipping = $this->_amz_workaround->shipping;
		    $this->view->payment = $this->_amz_workaround->payment;

		    //having an issue with some payment data being set and then causing CC to be selected
		    $this->view->payment['name'] = '';
		    $this->view->payment['cc_num'] = '';

		    $this->_amz_workaround->setExpirationSeconds(1);

		    $amz_workaround = true;
	    }
	    $this->view->amz_redir = $this->getRequest()->getParam('amz_redir', 'n');//for mobile to open the billing accordian


	    $video_mapper = new Application_Model_Mapper_VideoPlatformVideo();
	    $monetization_mapper = new Application_Model_Mapper_VideoPlatformVideoMonetization();

	    $this->view->products = array();
	    $sub_total = 0;
	    $discount = 0;

	    //get video id and purchase type
	    $video_id = $this->getRequest()->getParam('id',0);
	    $entitlement_type = $this->getRequest()->getParam('type','purchase');

	    $video = $video_mapper->find($video_id);
	    $monetization = $monetization_mapper->fetchByVideoId($video->id);

	    $price = ($entitlement_type == 'rent')?$monetization->rental_price:$monetization->purchase_price;

	    $this->view->products[] = array(
	    	'id' => $video->id,
	    	'title' => $video->title,
		    'entitlement_type' => $entitlement_type,
		    'price' => $price,
		    'qty' => 1
	    );
	    $sub_total += $price;

	    $this->_checkout->products = $this->view->products;

	    $this->view->order_total = $this->_checkout->sub_total = $sub_total;

	    $this->view->discount = $discount;

	    $customer_mapper = new Application_Model_Mapper_Customer();
	    $customer_address_mapper = new Application_Model_Mapper_CustomerAddress();

	    $customer_id = (is_object($this->_customer) && $this->_customer->id) ? $this->_customer->id : 0;
	    $customer = $customer_mapper->find($customer_id);
	    $customer_billing = $customer_address_mapper->findByCustomerId($customer_id, 'billing', 'Yes');

	    if (!$amz_workaround) {
		    $this->view->billing = array(
			    'first_name' => ($customer_billing->first_name) ? $customer_billing->first_name : (is_object($customer) && $customer->first_name) ? $customer->first_name : '',
			    'last_name' => ($customer_billing->last_name) ? $customer_billing->last_name : (is_object($customer) && $customer->last_name) ? $customer->last_name : '',
			    'email' => (is_object($customer) && $customer->email) ? $customer->email : '',
			    'phone' => ($customer_billing->phone) ? $customer_billing->phone : '',
			    'company' => ($customer_billing->company) ? $customer_billing->company : '',
			    'address' => ($customer_billing->address1) ? $customer_billing->address1 : '',
			    'address2' => ($customer_billing->address2) ? $customer_billing->address2 : '',
			    'city' => ($customer_billing->city) ? $customer_billing->city : '',
			    'region' => ($customer_billing->region) ? $customer_billing->region : '',
			    'postcode' => ($customer_billing->postcode) ? $customer_billing->postcode : '',
			    'country' => ($customer_billing->country) ? $customer_billing->country : 'US'
		    );
	    }

	    $regions = Zend_Registry::get('regions')->toArray();
	    $this->view->billing_states = (array_key_exists(strtolower($this->view->billing['country']), $regions))?$regions[strtolower($this->view->billing['country'])]['region']:null;

        $form = new Application_Form_CheckoutVideo();
        if ($this->getRequest()->isPost()) {
	        $payment = $this->getRequest()->getParam("payment", $this->view->payment);
	        $this->view->payment = $this->_checkout->payment = $payment;

	        $billing = $this->getRequest()->getParam("billing", $this->view->billing);

	        $this->view->billing_states = (array_key_exists(strtolower($billing['country']), $regions))?$regions[strtolower($billing['country'])]['region']:null;
	        if (!$this->view->billing_states) {
		        $billing['region'] = $billing['region_alt'];
		        $form->getSubForm('billing')->getElement('region')->setValue($billing['region']);
		        $form->getSubForm('billing')->getElement('region')->setRequired(false);
	        }
	        $this->view->billing = $this->_checkout->billing = $billing;

	        $this->view->billing_states = (array_key_exists(strtolower($this->view->billing['country']), $regions))?$regions[strtolower($this->view->billing['country'])]['region']:null;

	        //can ignore for non-cc orders. Those only have to worry about shipping info
	        if ($payment_method == 'cc') {
		        if ($billing['country'] == 'US') {
			        $cleaned = preg_replace('/[^\d]/i', '', $billing['postcode']);
			        if (strlen($cleaned) <> 5 && strlen($cleaned) <> 9) {
				        $b_postcode_error = true;
			        }
		        } elseif ($billing['country'] == 'CA') {
			        $cleaned = preg_replace('/[^\da-z]/i', '', $billing['postcode']);
			        if (strlen($cleaned) <> 6) {
				        $b_postcode_error = true;
			        } else {
				        $billing['postcode'] = substr($billing['postcode'], 0, 3) . " " . substr($billing['postcode'], 4, 3);
			        }
		        }
	        }
	        $this->view->billing = $this->_checkout->billing = $billing;

	        //temp since no reCAPTCHA
	        $form->getSubForm('payment')->removeElement('captcha');

	        if ($payment_method == "paypal" || $payment_method == "amazonpay") {
		        $form->getSubForm('billing')->removeElement('first_name');
		        $form->getSubForm('billing')->removeElement('last_name');
		        $form->getSubForm('billing')->removeElement('address');
		        $form->getSubForm('billing')->removeElement('city');
		        $form->getSubForm('billing')->removeElement('region');
		        $form->getSubForm('billing')->removeElement('postcode');
		        $form->getSubForm('billing')->removeElement('country');
		        $form->getSubForm('billing')->removeElement('email');

		        $form->getSubForm('payment')->removeElement('name');
		        $form->getSubForm('payment')->removeElement('cc_num');
		        $form->getSubForm('payment')->removeElement('ccv');
		        $form->getSubForm('payment')->removeElement('exp_month');
		        $form->getSubForm('payment')->removeElement('exp_year');
		        $form->getSubForm('payment')->removeElement('captcha');
	        }

	        if ($form->isValid($this->getRequest()->getParams())) {
		        //tax (no tax on digital items)
		        $tax = 0;

		        //sub total
		        $sub_total += $discount;//sub_total has the discount in it already but we need to show the breakdown.

		        //total
		        $cost = $sub_total + $tax;

		        $this->_checkout->totals = array(
			        'tax' => $tax,
			        'discount' => $discount,
			        'sub_total' => $sub_total,
			        'total' => round($cost - $discount, 2)
		        );

		        if ($payment_method == "paypal" && array_key_exists('paypal',$this->_payment_methods)) {
			        $paypal = new Application_Model_Paypal();
			        $response = $paypal->createTransaction($this->view->products, $this->_checkout->totals, 'sale', '/checkout-video/paypal', '/checkout-video/?id='.$video_id.'&type='.$entitlement_type.'&method=paypal');

			        if ($response) {
				        $this->view->error[] = $response;
			        }
		        } elseif ($payment_method == "cc" && array_key_exists('cc',$this->_payment_methods)) {
			        switch (substr($payment['cc_num'], 0, 1)) {
				        case "3":
					        $cc_type = "Amex";
					        break;
				        case "4":
					        $cc_type = "Visa";
					        break;
				        case "5":
					        $cc_type = "MasterCard";
					        break;
				        case "6":
					        $cc_type = "Discover";
					        break;
			        }
			        $payment['cc_type'] = $cc_type;

			        //normalize to 5 digit zip for US billing zipcode
			        if ($billing['country'] == 'US' && strlen($billing['postcode']) > 5) {
				        $billing['postcode'] = substr(preg_replace("/[^0-9]/", "", $billing['postcode']), 0, 5);
				        $this->_checkout->billing = $billing;
			        }

			        if ($this->_payment_methods['cc'] == 'payrix') {
				        $payrix = new Application_Model_Payrix();
				        $response = $payrix->createTransaction($payment, $this->_checkout->totals, $billing, [], $this->view->products);

				        if (isset($response['response']->id) && $response['response']->id) { //approved
					        $payment_profile_id = $customer_profile_id = new Zend_Db_Expr("NULL");

					        $order_mapper = new Application_Model_Mapper_VideoPlatformOrder();

					        $order_data = array(
						        'customer_id' => ($this->_customer) ? $this->_customer->id : null,
						        'bill_method' => 'cco',
						        'subtotal' => $sub_total,
						        'discount' => $discount,
						        'tax' => $tax,
						        'total' => $cost,
						        'confirmation_sent' => 'No',
					        );
					        $order = new Application_Model_VideoPlatformOrder($order_data);
					        $order_id = $order_mapper->save($order);

					        $order_payment_mapper = new Application_Model_Mapper_VideoPlatformOrderPayment();
					        $order_payment_data = array(
						        'video_platform_order_id' => $order_id,
						        'type' => 'cc',
						        'transaction_id' => $response['response']->id,
						        'identifier' => $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]",
						        'card_token' => $payment_profile_id,
						        'total_amount' => round($cost - $discount, 2),
						        'discount_amount' => $discount,
						        'tax_amount' => $tax,
						        'response' => json_encode($response['response']),
						        'status' => 'approved',
					        );
					        $order_payment = new Application_Model_VideoPlatformOrderPayment($order_payment_data);
					        $order_payment_mapper->save($order_payment);


					        $this->processOrder($order_id, $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]", $this->_checkout, $this->_cart, $this->_customer);
				        }else {
					        $this->view->error[] = $response['error_message'];
				        }
			        }elseif ($this->_payment_methods['cc'] == 'payflowpro') {
				        $payflow = new Application_Model_PayflowPro();
				        $response = $payflow->sale_transaction($payment, $this->_checkout->totals, $billing, [], $this->view->products);

				        if (isset($response['response']['RESULT']) && $response['response']['RESULT'] == 0) {
					        $order_mapper = new Application_Model_Mapper_VideoPlatformOrder();

					        $order_data = array(
						        'customer_id' => ($this->_customer) ? $this->_customer->id : null,
						        'bill_method' => 'cco',
						        'subtotal' => $sub_total,
						        'discount' => $discount,
						        'tax' => $tax,
						        'total' => $cost,
						        'confirmation_sent' => 'No',
					        );
					        $order = new Application_Model_VideoPlatformOrder($order_data);
					        $order_id = $order_mapper->save($order);

					        $order_payment_mapper = new Application_Model_Mapper_VideoPlatformOrderPayment();
					        $order_payment_data = array(
						        'video_platform_order_id' => $order_id,
						        'type' => 'cc',
						        'transaction_id' => $response['response']['PNREF'],
						        'identifier' => $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]",
						        'card_token' => $response['response']['PNREF'],
						        'total_amount' => round($cost - $discount, 2),
						        'discount_amount' => $discount,
						        'tax_amount' => $tax,
						        'response' => serialize($response['response']),
						        'status' => 'approved',
					        );
					        $order_payment = new Application_Model_VideoPlatformOrderPayment($order_payment_data);
					        $order_payment_mapper->save($order_payment);


					        $this->processOrder($order_id, $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]", $this->_checkout, $this->_cart, $this->_customer);
				        } else {
					        $this->view->error[] = $response['response']['RESPMSG'];
				        }
			        }elseif ($this->_payment_methods['cc'] == 'authorize.net') {
				        //authorize.net
				        $authnet = new Application_Model_AuthorizeNet();
				        $response = $authnet->createTransaction($payment, $this->_checkout->totals, $billing, [], $this->view->products);

				        if (($response['response'] != null) && ($response['response']->getResponseCode() == "1")) { //approved
					        $payment_profile_id = $customer_profile_id = new Zend_Db_Expr("NULL");
					        if (($response['profile_response'] != null) && ($response['profile_response']->getMessages()->getResultCode() == "Ok")) {
						        $customer_profile_id = $response['profile_response']->getCustomerProfileId();
						        $payment_profile_id = current($response['profile_response']->getCustomerPaymentProfileIdList());
					        } else {
						        //echo "ERROR Response : " . $response['profile_response']->getMessages()->getMessage()[0]->getCode() . "  " .$response['profile_response']->getMessages()->getMessage()[0]->getText() . "\n";
					        }

					        $order_mapper = new Application_Model_Mapper_VideoPlatformOrder();

					        $order_data = array(
						        'customer_id' => ($this->_customer) ? $this->_customer->id : null,
						        'bill_method' => 'cco',
						        'subtotal' => $sub_total,
						        'discount' => $discount,
						        'tax' => $tax,
						        'total' => $cost,
						        'confirmation_sent' => 'No',
					        );
					        $order = new Application_Model_VideoPlatformOrder($order_data);
					        $order_id = $order_mapper->save($order);

					        $order_payment_mapper = new Application_Model_Mapper_VideoPlatformOrderPayment();
					        $order_payment_data = array(
						        'video_platform_order_id' => $order_id,
						        'type' => 'cc',
						        'transaction_id' => $response['response']->getTransId(),
						        'identifier' => $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]",
						        'card_token' => $payment_profile_id,
						        'total_amount' => round($cost - $discount, 2),
						        'discount_amount' => $discount,
						        'tax_amount' => $tax,
						        'response' => json_encode($response['response']),
						        'status' => 'approved',
					        );
					        $order_payment = new Application_Model_VideoPlatformOrderPayment($order_payment_data);
					        $order_payment_mapper->save($order_payment);


					        $this->processOrder($order_id, $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]", $this->_checkout, $this->_cart, $this->_customer);
				        } else {
					        if (isset($response['error_message']) && $response['error_message']) {
						        return $response['error_message'];
					        }elseif (isset($response['response']) && $response['response']->getErrors() != null) {
						        $this->view->error[] = $response['response']->getErrors()[0]->getErrorText(); //$response['response']->getErrors()[0]->getErrorCode()
					        } else {
						        if ($response['profile_response']->getMessages() != null) {
							        $this->view->error[] = $response['profile_response']->getMessages()->getMessage()[0]->getText(); //$response['profile_response']->getErrors()[0]->getErrorCode()
						        }
					        }
				        }
			        }
		        } elseif ($payment_method == "amazonpay" && array_key_exists('amazonpay',$this->_payment_methods)) {
			        $amazon_pay = new Application_Model_AmazonPay();
			        $response = $amazon_pay->authorize($payment, $this->_checkout->totals, $this->getRequest()->getParam('amazon_order_reference_id', ''));

			        if (isset($response['response']->AuthorizeResult->AuthorizationDetails->AuthorizationStatus->State) && $response['response']->AuthorizeResult->AuthorizationDetails->AuthorizationStatus->State != "Declined") {
				        $order_mapper = new Application_Model_Mapper_VideoPlatformOrder();

				        $order_data = array(
					        'customer_id' => $customer_id,
					        'bill_method' => 'amazonpay',
					        'subtotal' => $sub_total,
					        'discount' => $discount,
					        'tax' => $tax,
					        'total' => $cost,
					        'confirmation_sent' => 'No',
				        );
				        $order = new Application_Model_VideoPlatformOrder($order_data);
				        $order_id = $order_mapper->save($order);

				        $order_payment_mapper = new Application_Model_Mapper_VideoPlatformOrderPayment();
				        $order_payment_data = array(
					        'video_platform_order_id' => $order_id,
					        'type' => 'amazonpay',
					        'transaction_id' => $this->getRequest()->getParam('amazon_order_reference_id', ''),
					        'identifier' => "",
					        'mes_card_token' => '',
					        'total_amount' => round($cost - $discount, 2),
					        'discount_amount' => $discount,
					        'tax_amount' => $tax,
					        'response' => json_encode($response['response']),
					        'status' => 'approved',
				        );
				        $order_payment = new Application_Model_VideoPlatformOrderPayment($order_payment_data);
				        $order_payment_mapper->save($order_payment);

				        //capture funds
				        $response = $amazon_pay->capture($response['response']->AuthorizeResult->AuthorizationDetails->AmazonAuthorizationId, $cost - $discount, $order_id."-".time());

				        //'transaction_id' => $response['response']->CaptureResult->CaptureDetails->AmazonCaptureId,
				        //'response' => json_encode($response['response']),

				        $this->processOrder($order_id, "Amazon Pay", $this->_checkout, $this->_cart, $this->_customer);
			        }elseif (isset($responseObject->Error)) {
				        $this->view->error[] = $response['response']->Error->Message;
			        } else {
				        $this->view->error[] = "Declined by Amazon Pay";
			        }

		        }
	        } else {
		        if ($this->getRequest()->getParam('promo_btn', '') != '1') {
			        foreach ($form->getMessages() as $subform => $errors) {
				        foreach ($errors as $k => $v) {
					        $key = $subform . "_" . $k;

					        if (array_key_exists(0, $v))
						        $this->view->error[$key] = $v[0];
					        if (array_key_exists('isEmpty', $v))
						        $this->view->error[$key] = $v['isEmpty'];
				        }
			        }
			        //print_r($form->getMessages());
			        //$this->view->error = "Please fill in all fields";
		        }
	        }
        }

    }

	public function paypalAction()
	{
		$this->view->error = array();
		$billing = $this->_checkout->billing;
		$payment = $this->_checkout->payment;

		$checkout = $this->_checkout;
		$cart = $this->_cart;
		$customer = $this->_customer;

		$paypal = new Application_Model_Paypal();

		if (isset($_GET['paymentId']) ) {
			$paymentId = $_GET['paymentId'];
			$result = $paypal->executeTransaction($paymentId);

			if (isset($result['error'])) {
				$this->_checkout->error[] = $result['error'];

				$this->_redirect("/checkout-video");
				exit;
			}

			/*
			//check if customer in the block list. If so just refresh the page
			$block_address = $shipping; //doing this so we can manipulate the data without effecting info saved
			$block_address['email'] = $payment->getPayer()->getPayerInfo()->getEmail();
			$block_mapper = new Application_Model_Mapper_CustomerBlock();
			$blocked = $block_mapper->verify($block_address, $_SERVER['REMOTE_ADDR']);

			if ($blocked) {
				$this->_redirect("/checkout");exit;
			}
			*/

			if ($result['payment']->getState() == 'approved') {
				//fix for possible duplicate orders
				$sql = $this->_db->quoteInto("SELECT order_id FROM order_payments WHERE transaction_id = ?", $result['payment']->getId());
				$duplicate = $this->_db->fetchOne($sql);
				if ($duplicate) {
					$this->_cart->setExpirationHops(1);
					$this->_cart->setExpirationSeconds(1);
					$this->_checkout->setExpirationHops(1);
					$this->_checkout->setExpirationSeconds(1);

					//Cny_Commerce_Cart::removeAll();

					$this->_order_info = new Zend_Session_Namespace('lvp_order_info');
					$this->_order_info->order_id = $duplicate;

					//unset($_SESSION['payer_id']);
					//unset($_SESSION['token']);

					$this->_redirect("/checkout-video/confirmation");
					exit;
				}

				$order_mapper = new Application_Model_Mapper_VideoPlatformOrder();

				$customer_id = (is_object($customer) && $customer->id) ? $customer->id : 0;
				$order_data = array(
					'customer_id' => $customer_id,
					'bill_method' => 'paypal',
					'subtotal' =>  $result['payment']->getTransactions()[0]->getAmount()->getTotal() - $result['payment']->getTransactions()[0]->getAmount()->getDetails()->getTax(),
					'discount' => 0,//$discount,
					'tax' => $result['payment']->getTransactions()[0]->getAmount()->getDetails()->getTax(),
					'total' => $result['payment']->getTransactions()[0]->getAmount()->getTotal(),
					'confirmation_sent' => 'No',
				);
				$order = new Application_Model_VideoPlatformOrder($order_data);
				$order_id = $order_mapper->save($order);

				$order_payment_mapper = new Application_Model_Mapper_VideoPlatformOrderPayment();
				$order_payment_data = array(
					'video_platform_order_id' => $order_id,
					'type' => 'paypal',
					'transaction_id' => $result['payment']->getId(),
					'sale_id' => $result['result']->getTransactions()[0]->getRelatedResources()[0]->getSale()->getId(),
					'identifier' => "Paypal - " . $result['payment']->getPayer()->getPayerInfo()->getPayerId(),
					'total_amount' => $result['payment']->getTransactions()[0]->getAmount()->getTotal(),
					//'discount_amount' => $payment->getTransactions()[0]->getAmount()->getDetails()->get,
					'tax_amount' => $result['payment']->getTransactions()[0]->getAmount()->getDetails()->getTax(),
					'response' => json_encode($result['payment']),
					'status' => 'approved',
				);
				$order_payment = new Application_Model_VideoPlatformOrderPayment($order_payment_data);
				$order_payment_mapper->save($order_payment);

				$billing['email'] = $result['payment']->getPayer()->getPayerInfo()->getEmail();
				$billing['firstname'] = $result['payment']->getPayer()->getPayerInfo()->getFirstName();
				$billing['lastname'] = $result['payment']->getPayer()->getPayerInfo()->getLastName();

				$checkout->billing = $this->_checkout->billing = $billing;

				$this->processOrder($order_id, "Paypal - " . $result['payment']->getPayer()->getPayerInfo()->getPayerId(), $checkout, $cart, $customer);
			}else {
				$this->_checkout->error[] = "The payment has been denied by Paypal";

				/*
				if ($response['L_ERRORCODE0'] == 10416) {
					unset($_SESSION['payer_id']);
					unset($_SESSION['token']);
				}
				*/

				$this->_redirect("/checkout-video");
			}
		} else {
			$this->_redirect("/checkout-video");
		}

	}

	public function amazonReturnAction()
	{
		$access_token = $this->getRequest()->getParam('access_token');
		if (is_null($access_token)) {
			// Throw a 404 error
			$this->getResponse()->setHttpResponseCode(404);
			return;
		}
		$_SESSION['access_token'] = $access_token;
		$this->getResponse()->setRedirect('/checkout-video?method=amazonpay&amz_redir='.$this->getRequest()->getParam('amz_reload','y'));
		return;
	}

	public function amazonGetAddressAction()
	{
		$amazon_pay = new Application_Model_AmazonPay();
		$response = $amazon_pay->getAddress($this->getRequest()->getParam('amazonOrderReferenceId',''), $_SESSION['access_token']);

		echo $response->toJson();

		exit;
	}

	public function amzWorkaroundAction()
	{
		$this->_amz_workaround = new Zend_Session_Namespace('lvp_amz_workaround');

		$this->_amz_workaround->billing = $this->getRequest()->getParam('billing', $this->_billing);
		$this->_amz_workaround->shipping = $this->getRequest()->getParam('shipping', $this->_shipping);
		$this->_amz_workaround->payment = $this->getRequest()->getParam('payment', $this->_payment);

		echo "done";
		exit;
	}

    private function processOrder($order_id, $identifier, $_checkout, $_cart, $_customer)
    {
        $this->view->payment = $payment = $_checkout->payment;
        $this->view->billing = $billing = $_checkout->billing;
        $customer_id = (is_object($_customer) && $_customer->id)?$_customer->id:0;
        $this->view->totals = $totals = $_checkout->totals;
        $this->view->products = $products = $_checkout->products;
        $this->view->payment_identifier = $identifier;

	    $video_mapper = new Application_Model_Mapper_VideoPlatformVideo();
	    $monetization_mapper = new Application_Model_Mapper_VideoPlatformVideoMonetization();
	    $entitlement_mapper = new Application_Model_Mapper_VideoPlatformEntitlement();

        $order_product_mapper = new Application_Model_Mapper_VideoPlatformOrderItem();
        foreach ($products as $k => $product) {
            $order_item_data = array(
                'video_platform_order_id' => $order_id,
                'video_platform_video_id' => $product['id'],
	            'title' => $product['title'],
                'price' => $product['price'],
                'discount' => null,
                'tax' => (isset($_checkout->tax_details))?number_format(($product['price']) * $_checkout->tax_details->taxSales,2):null,
                'total' => $product['price'],
	            'purchase_type' => $product['entitlement_type']
            );

            $order_product = new Application_Model_VideoPlatformOrderItem($order_item_data);
            $order_product_mapper->save($order_product);

            $expiration = null;
            if ($product['entitlement_type'] == 'rent') {
	            $monetization = $monetization_mapper->fetchByVideoId($product['id']);

	            $duration = ($monetization->rental_duration_days)?$monetization->rental_duration_days:1;

	            $expiration = new Zend_Db_Expr("DATE_ADD(NOW(), INTERVAL {$duration} DAY)");
            }

            //save entitlement
	        $entitlement_mapper->save(new Application_Model_VideoPlatformEntitlement(array(
		        'customer_id' => $customer_id,
		        'video_platform_video_id' => $product['id'],
		        'entitlement_type' => $product['entitlement_type'],
		        'expiration' => $expiration
	        )));
        }

        //customer info
        $mapper = new Application_Model_Mapper_VideoPlatformOrderAddress();
        $customer_address_data = array(
            'video_platform_order_id' => $order_id,
            'type' => 'billing',
            'first_name' => $billing['first_name'],
            'last_name' => $billing['last_name'],
            //'company' => $billing['company'],
            'address' => $billing['address'],
            'address2' => $billing['address2'],
            'city' => $billing['city'],
            'region' => $billing['region'],
            'postcode' => $billing['postcode'],
            'country' => $billing['country'],
            'phone' => $billing['phone']
        );
        $customer_address_info = new Application_Model_VideoPlatformOrderAddress($customer_address_data);
        $mapper->save($customer_address_info);


        if (is_object($_customer) && $_customer->id) {
            //check if they have a default shipping and billing address
            $customer_address_mapper = new Application_Model_Mapper_CustomerAddress();
            $customer_billing = $customer_address_mapper->findByCustomerId($_customer->id, 'billing', 'Yes');

            if (!$customer_billing->id) {
                $customer_address_mapper->save(new Application_Model_CustomerAddress(
                    array(
                        'customer_id' => $_customer->id,
                        'address_type' => 'billing',
                        'first_name' => $billing['first_name'],
                        'last_name' => $billing['last_name'],
                        //'company' => $billing['company'],
                        'address' => $billing['address'],
                        'address2' => $billing['address2'],
                        'city' => $billing['city'],
                        'region' => $billing['region'],
                        'postcode' => $billing['postcode'],
                        'country' => $billing['country'],
                        'phone_day' => $billing['phone'],
                        'default_address' => 'Yes'
                    )
                ));
            }

        }

        $this->_checkout->setExpirationHops(1);
        $this->_checkout->setExpirationSeconds(1);

        $this->_order_info = new Zend_Session_Namespace('lvp_order_info');
        $this->_order_info->order_id = $order_id;

        //unset($_SESSION['payer_id']);
        //unset($_SESSION['token']);

        $this->redirect("/checkout-video/confirmation");
    }

    public function confirmationAction()
    {
        $this->view->placeholder('body_class')->append('checkout-page confirmation-page');

        $this->_order_info = new Zend_Session_Namespace('lvp_order_info');
        $this->view->order_id = $order_id = $this->_order_info->order_id;
        $this->_order_info->setExpirationHops(1);

        if ($order_id) {
            $mapper = new Application_Model_Mapper_VideoPlatformOrder();
            $this->view->order = $mapper->find($order_id);

            $mapper = new Application_Model_Mapper_VideoPlatformOrderAddress();
            $this->view->billing_address = $mapper->findByOrderId($order_id, 'billing');

            $mapper = new Application_Model_Mapper_VideoPlatformOrderItem();
            $this->view->products = $mapper->findByOrderId($order_id);

            $mapper = new Application_Model_Mapper_VideoPlatformOrderPayment();
            $this->view->payment = $mapper->findByOrderId($order_id);

	        //send email confirmation
	        if ($this->view->order->confirmation_sent == 'No') {
		        try {
			        $emails = new Application_Model_Emails();
			        $emails->orderVideoConfirmation($order_id);

			        $mapper = new Application_Model_Mapper_VideoPlatformOrder();
			        $order = $mapper->find($order_id);
			        $order->confirmation_sent = 'Yes';
			        $mapper->save($order);
		        } catch (Exception $ex) {

		        }
	        }

	        $lvp_post_purchase_redirect = new Zend_Session_Namespace('lvp_post_purchase_redirect');
	        $this->redirect(($lvp_post_purchase_redirect->url)?$lvp_post_purchase_redirect->url:"/");
        }else {
            $this->redirect("/");
        }
    }

    public function getTaxAction()
    {
        $this->view->layout()->disableLayout();

        $address = $this->getRequest()->getParam("address","");
        $city = $this->getRequest()->getParam("city","");
        $region = $this->getRequest()->getParam("region","");
        $postcode = $this->getRequest()->getParam("postcode","");
        $country = $this->getRequest()->getParam("country","");
        $sub_total = $this->getRequest()->getParam("sub_total","");

        $tax = 0.00;

        if (array_key_exists($region,$this->_taxStates) && $region && $postcode) {
        	/*
            $ziptax = new Cny_Model_ZipTax();
            $tax_response = $ziptax->taxRate(array(
                'address' => $address,
                'city' => $city,
                'region' => $region,
                'postcode' => $postcode,
                'country' => $country
            ));

            if (isset($tax_response->taxSales)) {
                $tax = number_format(($sub_total * $tax_response->taxSales), 2);
           }
        	*/

	        $tax = number_format(($sub_total * $this->_taxStates[$region]/100), 2);
        }

        echo $tax;
        exit;
    }

	public function getStatesAction()
	{
		$this->view->layout()->disableLayout();

		$region = $this->getRequest()->getParam("region","");
		$country = $this->getRequest()->getParam("country","");

		$regions = Zend_Registry::get('regions')->toArray();
		if (array_key_exists(strtolower($country), $regions)) {
			$states = array(""=>"Select State/Province/Region") + $regions[strtolower($country)]['region'];

			echo json_encode($states);
		}else {
			echo "0";
		}

		exit;
	}

	public function promoRemoveAction()
	{
		$this->_cart->promo_code = '';

		$this->redirect("/checkout-video");
	}
}
