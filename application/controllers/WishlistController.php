<?php
class WishlistController extends Zend_Controller_Action
{
	function init()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
		//$this->_wishlist_encryption_key = $options['encryption']['key']['wishlist'];

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
		if( $auth->hasIdentity() ){
			$this->_customer = $auth->getIdentity();
		}

		$this->_cart = new Zend_Session_Namespace('cart');
		if (!is_array($this->_cart->items)) $this->_cart->items = array();
	}

	public function addAction()
	{
		$redirect = $this->getRequest()->getParam("redir","/account/wishlist");

		$output = [];
		$product_id = $this->_getParam("product_id",'');
		$variant_id = $this->_getParam("variant_id",null);
		$price = null;

		if (isset($this->_customer) && $this->_customer->id) {
			$mapper = new Application_Model_Mapper_Product();
			$product = $mapper->find($product_id);
			if ($product->status == 'Disabled') exit;
			if ($product->price) $price = $product->price;

			if ($variant_id) {
				$mapper = new Application_Model_Mapper_ProductVariant();
				$variant = $mapper->find($variant_id);
				if ($variant->status == 'Disabled') exit;
				if ($variant->price) $price = $variant->price;
			}

			$mapper = new Application_Model_Mapper_CustomerWishlist();
			if ($product_id) {
				$mapper->addToWishlist($this->_customer->id, $product_id, $variant_id, $price);
			}
		}

		if ($this->getRequest()->isXmlHttpRequest()) {
			$output['action'] = 'add';
			$output['item']['product_id'] =  $product_id;
			$output['item']['variant_id'] =  $variant_id;

			if (!isset($this->_customer) || !$this->_customer->id) {
				$output['action'] = 'error';
				$output['message'] = 'You must be logged into your account to add an item to your wishlist.<br/><br/>
				<div class="text-center">
					<a href="/login" class="btn btn--red">Login</a>
					<a href="/register" class="btn btn--red">Create Account</a>
					<br/><br/>
					<button aria-label="Close" type="button" class="btn btn-sm btn--gray" onclick="$(\'.alert-message\').remove();">Maybe Later</button>
				</div>';
			}

			echo json_encode($output);
		} else {
			$this->redirect(urldecode($redirect));
		}
		exit;
	}

	public function removeAction()
	{
		$redirect = $this->getRequest()->getParam("redir","/account/wishlist");

		$output = [];
		$product_id = $this->_getParam("product_id",'');
		$variant_id = $this->_getParam("variant_id",null);

		$mapper = new Application_Model_Mapper_CustomerWishlist();
		if(isset($this->_customer) && $this->_customer->id ){
			$mapper->removeFromWishlist($this->_customer->id, $product_id, $variant_id);
		}

		if ($this->getRequest()->isXmlHttpRequest()) {
			$output['action'] = 'remove';
			$output['item']['product_id'] =  $product_id;
			$output['item']['variant_id'] =  $variant_id;

			if (!isset($this->_customer) || !$this->_customer->id) {
				$output['action'] = 'error';
				$output['message'] = 'You must be logged into your account to remove an item from your wishlist.';
			}

			echo json_encode($output);
		} else {
			$this->redirect(urldecode($redirect));
		}
		exit;
	}

	public function cartAddAction()
	{
		$product_id = $this->_getParam("product_id",'');
		$variant_id = $this->_getParam("variant_id",null);

		$mapper = new Application_Model_Mapper_CustomerWishlist();
		if(isset($this->_customer) && $this->_customer->id ){
			$mapper->removeFromWishlist($this->_customer->id, $product_id, $variant_id);
		}

		$this->redirect("/cart/add/product_id/$product_id/variant_id/$variant_id");
	}

	/*
	public function viewAction()
	{
		//decode customer ID
		$wishlist_id = str_ireplace(array(" "), array("+"),$this->getRequest()->getParam('id',''));

		$encryption = new Cny_View_Helper_Encryption();
		$customer_id = $encryption->decrypt($wishlist_id, $this->_wishlist_encryption_key);

		if(empty($customer_id) || !is_numeric($customer_id)) {
			$this->getResponse()->setRedirect('/');
		}

		$mapper = new Application_Model_Mapper_Customer();
		$this->view->friend = $mapper->find($customer_id);

		$this->_cart = new Zend_Session_Namespace('cart');
		if (!is_array($this->_cart->items)) $this->_cart->items = array();

		$mapper = new Application_Model_Mapper_CustomerWishlist();
		$wishlist = $mapper->findByCustomerId($customer_id);
		$items = ($wishlist)?json_decode($wishlist->wishlist):array();

		$this->view->promo_code = $promo_code = $this->_cart->promo_code = ($this->_cart->promo_code)?$this->_cart->promo_code:$this->_getParam('promo_code','');

		//get base subtotal
		$base_sub_total = 0;
		$price_mapper = new Application_Model_Mapper_ProductPrice();
		foreach ($this->_cart->items as $id => $item) {
			$price = $price_mapper->fetchPriceDataByProductId($id);

			$base_sub_total += ($price['price'] * $item['qty']);
		}

		//if there is a promo code
		$promo_products = array(); $this->view->promo_message = $this->view->promo = $promo = '';
		if ($promo_code) {
			$mapper = new Application_Model_Mapper_Promo();
			$this->view->promo_message = $mapper->validateCode($promo_code, $base_sub_total);
			if (!$this->view->promo_message) {
				$this->view->promo = $promo = $mapper->fetchByCode($promo_code, $base_sub_total);

				if ($promo) {
					if ($promo->sitewide == 'No') {
						//get prodoucts
						$mapper = new Application_Model_Mapper_PromoProduct();
						$promo_products = $mapper->fetchProductArrayByPromoId($promo->id);
					}
					$this->_cart->valid_promo_code = $promo_code;
				} else {
					$this->_cart->promo_code = "";
					if ($this->_cart->valid_promo_code) {
						$this->_cart->promo_code = $this->_cart->valid_promo_code;
						$this->_cart->setExpirationSeconds(1, 'valid_promo_code');
						$this->_cart->setExpirationHops(1, 'valid_promo_code');
						$this->redirect("/checkout");
					}
				}
			}else {
				if ($this->_cart->valid_promo_code) {
					$this->_cart->promo_code = $this->_cart->valid_promo_code;
					$this->_cart->setExpirationSeconds(1, 'valid_promo_code');
					$this->_cart->setExpirationHops(1, 'valid_promo_code');
					$this->redirect("/checkout");
				}
			}
		}else {
			//if no promo code entered check for automatic promos
			$mapper = new Application_Model_Mapper_Promo();
			$this->view->promo = $promo = $mapper->fetchAutomaticPromo($base_sub_total);

			if ($promo) {
				if ($promo->sitewide == 'No') {
					//get products
					$mapper = new Application_Model_Mapper_PromoProduct();
					$promo_products = $mapper->fetchProductArrayByPromoId($promo->id);
				}
			}
		}

		$this->view->products = array(); $sub_total = 0;
		$product_manager = new Application_Model_Mapper_Product();
		$price_mapper = new Application_Model_Mapper_ProductPrice();
		foreach ($items as $k => $id) {
			//defaults because I don't have these extra items being stored at the moment
			$item  = array('id'=>$id,'qty'=>1,'lic'=>1);

			$qty = $item['qty'];
			$prod = array();
			$price = $price_mapper->fetchPriceDataByProductId($id);

			$prod['qty'] = $qty;
			$prod['price'] = $price['price'];

			if ($promo && (in_array($id,$promo_products) || $promo->sitewide == 'Yes')) {
				//if (stripos($promo->licenses,"|{$item['lic']}|") === false) continue;
				if ($promo->type == "fixed") {
					$prod['price'] = $prod['price']-$promo->amount;
				}elseif($promo->type == "percent") {
					$prod['price'] = round($prod['price'] - ($prod['price'] * ($promo->amount/100)),2);
				}elseif($promo->type == "percent_msrp") {
					$new_price = round($price['msrp_price'] - ($price['msrp_price'] * ($promo->amount/100)),2);
					$prod['price'] = ($new_price < $prod['price']) ? $new_price : $prod['price'];
				}elseif($promo->type == "price") {
					$prod['price'] = $promo->amount;
				}
			}else {
				$prod['price'] = $prod['price'];
			}
			$sub_total += ($prod['price'] * $qty);

			$prod['product'] = $product_manager->find($id, false);

			if (!$prod['product'] || $prod['product']->status == 'Disabled') {
				continue;
			}

			$this->view->products[] = $prod;
		}
	}
	*/
}
