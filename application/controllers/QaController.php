<?php
class QaController extends Zend_Controller_Action
{
	function init()
	{
		//$this->view->placeholder('body_class')->set("product-page");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$options = $bootstrap->getOptions();
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix']) ? $options['resources']['multidb']['db']['table_prefix'] : '';

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
	}

	public function productAction()
	{
		$ret_url = $this->getRequest()->getParam('ret_url', '/');

		if( $this->getRequest()->isPost() ) {
			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

			$valid = false;
			if ($settings_mapper->getValueByCode('product_qas_captcha_status') == 'Enabled') {
				$recaptcha_secret_key = $settings_mapper->getValueByCode('recaptcha_secret_key');

				if ($recaptcha_secret_key) {
					$recaptcha = new \ReCaptcha\ReCaptcha($recaptcha_secret_key);
					$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
					if ($resp->isSuccess()) {
						$valid = true;
					} else {
						$errors = $resp->getErrorCodes();

						$this->_flashMessenger->addMessage('CAPTCHA Failed');
					}
				}
			}else {
				$valid = true;
			}

			$data = $this->getRequest()->getParam('data', array());
			$data['remote_addr'] = $_SERVER['REMOTE_ADDR'];
			$data['referrer'] = $_SERVER['HTTP_REFERER'];
			$data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
			$data['status'] = 'Pending';

			if ($valid) {
				$data['is_spam'] = 'No';
				if ($settings_mapper->getValueByCode('akismet_api_key')) {
					$akismet = new Application_Model_Akismet();

					$verify_data = array(
						'user_ip' => $data['user_ip'],
						'referrer' => $data['referrer'],
						'comment_type' => 'question',
						'comment_author' => $data['name'],
						'comment_author_email' => (isset($data['email'])) ? $data['email'] : null,
						'comment_content' => $data['question'],
						'comment_date_gmt' => date("c")
					);

					if ($akismet->commentCheck($verify_data)) { //akismet returns "false" if not spam
						$data['is_spam'] = 'Yes';
						$data['status'] = 'Pending';
					}
				}

				$mapper = new Application_Model_Mapper_ProductQa();
				$mapper->save(new Application_Model_ProductQa($data));

				$response_message = ($settings_mapper->getValueByCode('product_qas_response_message'))?$settings_mapper->getValueByCode('product_qas_response_message'):"Your question has been received";
				$this->_flashMessenger->addMessage($response_message);
			}
		}

		$this->redirect($ret_url);
	}
}
