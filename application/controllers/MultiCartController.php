<?php
class MultiCartController extends Zend_Controller_Action
{

	function init()
	{

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$options = $bootstrap->getOptions();
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		if (!$this->getRequest()->isXmlHttpRequest()) {
			$generator = new Cny_Model_HtmlBuild();
			$this->view->header = $generator->buildHeader();
			$this->view->footer = $generator->buildFooter();

			//set body classes based on settings
			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
			$header_type = $settings_mapper->getValueByCode('header_type');

			if ($header_type == '') {
				$this->view->placeholder('body_class')->append('header-fixed-space-default');
			}
			if ($settings_mapper->getValueByCode('nav_fixed_position') != 'navbar-fixed-bottom' && $settings_mapper->getValueByCode('nav_fixed_position') != 'navbar-static-top') {
				$this->view->placeholder('body_class')->append('header-fixed');
			}

			//footer and global CSS
			$mapper = new Application_Model_Mapper_Page();
			$footer = $mapper->doesExists(['code'=>'footer']);
			$this->view->headLink()->appendStylesheet('/index/css/page_id/'.$footer->id.'/?v='.$generator->cssStamp($footer->id));
		}
	}

	public function indexAction()
	{

	}

	public function loginAction()
	{

		$this->view->placeholder('body_class')->append('account-auth-page login-page');
	}

	public function informationAction()
	{
		$this->view->placeholder('body_class')->append('account-auth-page information-page');


	}

	public function shippingAction()
	{
		$this->view->placeholder('body_class')->append('account-auth-page shipping-page');


	}

	public function paymentAction()
	{
		$this->view->placeholder('body_class')->append('account-auth-page payment-page');


	}

	public function confirmationAction()
	{
		$this->view->placeholder('body_class')->append('account-auth-page confirmation-page');


	}
}
