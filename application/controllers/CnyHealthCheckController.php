<?php
//turn on error reporting but don't display
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', false);

//register a fatal error handler so we can return info
register_shutdown_function( "fatal_handler" );

function fatal_handler() {
	$errfile = "unknown file";
	$errstr  = "shutdown";
	$errno   = E_CORE_ERROR;
	$errline = 0;

	$error = error_get_last();

	if($error !== NULL) {
		$errno   = $error["type"];
		$errfile = $error["file"];
		$errline = $error["line"];
		$errstr  = $error["message"];

		$response['Fatal Exception'] = [
			'status' => 'Error',
			'message' => $error["message"],
		];

		echo json_encode($response);
		exit;
	}
}

class CnyHealthCheckController extends Zend_Controller_Action
{
    public function indexAction()
    {
    	$this->view->layout()->disableLayout();

    	$response = [];
	    $start_time = microtime(true);

    	try {
		    $bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		    $options = $bootstrap->getOptions();
		    $resource = $bootstrap->getPluginResource('multidb'); //multi db support
		    $db = $resource->getDefaultDb();

		    $this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix']) ? $options['resources']['multidb']['db']['table_prefix'] : '';

		    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		    //grab something directly from the DB to make sure it works
		    $random_row = $db->fetchRow("SELECT * FROM {$this->_table_prefix}.pages ORDER BY RAND() LIMIT 1");
		    $response['Random DB Read'] = [
		    	'status' => ($random_row)?"Success":"Error",
			    'message' => ($random_row)?"Read {$random_row['title']} successfully":"No pages in DB should be impossible"
		    ];

		    //use the Site Settings class to get a value and confirm homepage is set
		    $home_page_code = $settings_mapper->getValueByCode('home_page',null);
		    $response['Home Page Set'] = [
			    'status' => ($home_page_code)?"Success":"Error",
			    'message' => ($home_page_code)?"A homepage is set":"No homepage set. Using splash page"
		    ];

		    //generate a page (this is conditional because a new install won't have any pages)
		    $random_page = $db->fetchRow("SELECT * FROM {$this->_table_prefix}.pages WHERE block_type='page' AND status='Enabled' ORDER BY RAND() LIMIT 1");
		    if ($random_page) {
			    $generator = new Cny_Model_HtmlBuild();
			    $content = $generator->buildPage($random_page['id'], false);
			    $response['Random Page Generation'] = [
				    'status' => ($random_page && $content) ? "Success" : "Error",
				    'message' => ($content) ? "Generated page {$random_page['title']} successfully" : "Unable to generate page {$random_page['title']}"
			    ];
		    }else {
			    $response['Random Page Generation'] = [
				    'status' => "Error",
				    'message' => "No pages available to test page generation"
				];
		    }
	    }catch(Exception $e) {
		    http_response_code(490);

    		$response['Exception Thrown'] = [
    			'status' => 'Error',
    			'code' => $e->getCode(),
			    'line' => $e->getLine(),
			    'message' => $e->getMessage(),
			    'trace' => $e->getTraceAsString()
		    ];
	    }

	    $end_time = microtime(true);

    	$response['Info'] = [
    		'start_time' => $start_time,
		    'end_time' => $end_time,
		    'total_time_taken' => $end_time - $start_time
	    ];

	    echo json_encode($response);
	    exit;
    }
}

