<?php
class FaqController extends Zend_Controller_Action
{
	function init()
	{
		//$this->view->placeholder('body_class')->set("product-page");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$options = $bootstrap->getOptions();
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix']) ? $options['resources']['multidb']['db']['table_prefix'] : '';

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$faq_status = ($settings_mapper->getValueByCode('faq_status'))?$settings_mapper->getValueByCode('faq_status'):'Disabled';
		if ($faq_status != 'Enabled') {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}
		$this->view->faq_url = $faq_url = ($settings_mapper->getValueByCode('faq_url'))?$settings_mapper->getValueByCode('faq_url'):'faq';

	}

	public function indexAction()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$faq_page_status = ($settings_mapper->getValueByCode('faq_page_status'))?$settings_mapper->getValueByCode('faq_page_status'):'Enabled';
		if ($faq_page_status != 'Enabled') {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}

		if ($template_code = $settings_mapper->getValueByCode('faq_template')) {
			$generator = new Cny_Model_HtmlBuild();
			$mapper = new Application_Model_Mapper_Template();
			$template = $mapper->findByCode($template_code);

			if ($template) {
				$mapper = new Application_Model_Mapper_TemplateBlock();
				$blocks = $mapper->findByTemplateId($template->id);

				foreach ($blocks as $position_code => $block) {
					$this->view->placeholder('template_position-'.$position_code)->append($generator->buildPage($block['block_id'], false, null, true));
					$this->view->placeholder('template_width-'.$position_code)->append($block['width']);
				}
			}
		}

		//announcement bar
		$announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
		$announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
		if ($announcement_bar_status == 'enable' && $announcement_bar_page == '') {
			$this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
		}

		//promotional pop-up
		$promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
		$promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
		if ($promo_popup_status == 'Enabled' && ($promo_popup_page == '')) {
			$promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
			$promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
			$promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
			$extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

			$this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$promo_popup_modal_code.'", "'.$promo_popup_delay.'", "'.$promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
		}

		//start actual page functionality
		$mapper = new Application_Model_Mapper_FaqSection();
		$sections = $mapper->fetchOrdered();
		$this->view->sections = $mapper->fetchPairs();

		$mapper = new Application_Model_Mapper_FaqItem();
		$this->view->tree = array();
		foreach ($sections as $section) {
			$items = $mapper->fetchBySectionId($section->id);

			$this->view->tree[$section->id] = array(
				'section' => $section,
				'items' => $items
			);
		}

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->faq_submission_status = $settings_mapper->getValueByCode('faq_submission_status');
		$this->view->faq_captcha_status = $settings_mapper->getValueByCode('faq_captcha_status');
		$this->view->recaptcha_site_key = $settings_mapper->getValueByCode('recaptcha_site_key');
		$this->view->recaptcha_secret_key = $settings_mapper->getValueByCode('recaptcha_secret_key');
	}

	public function submitAction()
	{
		if( $this->getRequest()->isPost() ) {
			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

			$valid = false;
			if ($settings_mapper->getValueByCode('faq_captcha_status') == 'Enabled') {
				$recaptcha_secret_key = $settings_mapper->getValueByCode('recaptcha_secret_key');

				if ($recaptcha_secret_key) {
					$recaptcha = new \ReCaptcha\ReCaptcha($recaptcha_secret_key);
					$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
					if ($resp->isSuccess()) {
						$valid = true;
					} else {
						$errors = $resp->getErrorCodes();

						$this->_flashMessenger->addMessage('CAPTCHA Failed');
					}
				}
			}else {
				$valid = true;
			}

			$data = $this->getRequest()->getParam('data', array());
			$data['remote_addr'] = $_SERVER['REMOTE_ADDR'];
			$data['referrer'] = $_SERVER['HTTP_REFERER'];
			$data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
			$data['status'] = 'Pending';

			if ($valid) {
				$data['is_spam'] = 'No';
				if ($settings_mapper->getValueByCode('akismet_api_key')) {
					$akismet = new Application_Model_Akismet();

					$verify_data = array(
						'user_ip' => $data['user_ip'],
						'referrer' => $data['referrer'],
						'comment_type' => 'question',
						'comment_author' => $data['name'],
						'comment_author_email' => (isset($data['email'])) ? $data['email'] : null,
						'comment_content' => $data['question'],
						'comment_date_gmt' => date("c")
					);

					if ($akismet->commentCheck($verify_data)) { //akismet returns "false" if not spam
						$data['is_spam'] = 'Yes';
						//$data['status'] = 'Pending';
					}
				}

				$mapper = new Application_Model_Mapper_FaqSubmission();
				$mapper->save(new Application_Model_FaqSubmission($data));

				$this->_flashMessenger->addMessage('<h5>Thank you for submitting your question</h5><p>We\'ll reply as soon as possible.</p>');
			}
		}

		$this->redirect("/{$this->view->faq_url}");
	}
}
