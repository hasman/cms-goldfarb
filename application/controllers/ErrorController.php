<?php
class ErrorController extends Zend_Controller_Action
{
	public function errorAction()
	{
		$errors = $this->_getParam('error_handler');

		switch ($errors->type) {
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
				// 404 error -- controller or action not found
				$priority = Zend_Log::NOTICE;
				$code = 404;
				$message = "Page not found";

				break;
			default:
				// application error
				$priority = Zend_Log::CRIT;
				$code = 500;
				$message = "Application error";
				break;
		}
		//ZF sends codes outside of HTTP response codes mixed in with HTTP response codes so checking if < 600 to try prevent unexpected browser errors
		$this->getResponse()->setHttpResponseCode(($errors->exception->getCode() && $errors->exception->getCode() > 400 && $errors->exception->getCode() < 600)?$errors->exception->getCode():$code);
		$this->view->message = ($errors->exception->getMessage() && $errors->exception->getCode() && $errors->exception->getCode() > 400 && $errors->exception->getCode() < 600)?$errors->exception->getMessage():$message;

		// Log exception, if logger available
		if ($log = $this->getLog()) {
			//$log->crit($this->view->message, $errors->exception);

			$log->log($this->view->message, $priority, $errors->exception);
			$log->log('Request Parameters', $priority);
			$log->log(var_export($errors->request->getParams(), true), $priority);
			$log->log($errors->exception->getMessage(), $priority);
			$log->log($errors->exception->getTraceAsString(), $priority);
			$log->log("---------------", $priority);
		}

		// conditionally display exceptions
		if ($this->getInvokeArg('displayExceptions') == true) {
			$this->view->exception = $errors->exception;
		}

		$this->view->request   = $errors->request;

		// CUSTOM error content
		$this->view->placeholder('body_class')->append('error-page');

		$mapper = new Application_Model_Mapper_Page();
		$page = $mapper->doesExists(['code'=>'custom_error']);

		if ($page->template && $page->template != 'layout') {
			$this->view->layout()->setLayout($page->template);
		}

		$generator = new Cny_Model_HtmlBuild();
		$this->view->content = $generator->buildPage($page->id, true, null, true);

		/*
		$this->view->headTitle()->prepend(($page->meta_title)?$page->meta_title:$page->title);
		$this->view->headMeta()->appendName('keywords', $page->meta_keywords);
		$this->view->headMeta()->appendName('description', $page->meta_description);
		*/

		$this->view->headLink()->appendStylesheet('/index/css/?v='.$generator->cssStamp($page->id));

		//set body classes based on settings
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$header_type = $settings_mapper->getValueByCode('header_type');

		if ($header_type == '') {
			$this->view->placeholder('body_class')->append('header-fixed-space-default');
		}
		if ($settings_mapper->getValueByCode('nav_fixed_position') != 'navbar-fixed-bottom' && $settings_mapper->getValueByCode('nav_fixed_position') != 'navbar-static-top') {
			$this->view->placeholder('body_class')->append('header-fixed');
		}

		$this->view->placeholder('_header')->set('');
		$this->view->placeholder('_footer')->set('');

		//navigation setup/options/etc.
		if ($page->nav_display == 'Enabled') {
			$this->view->placeholder('_header')->set('');

			if (!$page->header_template || $page->header_template == 'default') {
				$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
				$header_type = $settings_mapper->getValueByCode('header_type');

				$page->header_template = $header_type;
			}

			if ($page->header_template && $page->header_template == 'default') {
				$this->view->placeholder('_header')->prepend($generator->buildHeader());
			}elseif ($page->header_template && stripos($page->header_template,"block-") !== false) {
				$header_block_id = str_ireplace("block-","",$page->header_template);
				$this->view->placeholder('_header')->prepend($generator->buildCustomHeader($header_block_id));
			}else {
				$this->view->placeholder('_header')->prepend($generator->buildHeader($page->header_template));
			}
		}
		//footer
		if ($page->footer_display == 'Enabled') {
			$this->view->placeholder('_footer')->set('');

			$this->view->placeholder('_footer')->append($generator->buildFooter());
		}
	}

	public function cliAction()
	{
		$errors = $this->_getParam('error_handler');

		switch ($errors->type) {
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

				$this->view->message = 'Page not found';
				break;
			default:
				$this->view->message = 'Application error';
				break;
		}

		// Log exception, if logger available
		if ($log = $this->getLog()) {
			$log->crit($this->view->message, $errors->exception);
		}

		// conditionally display exceptions
		if ($this->getInvokeArg('displayExceptions') == true) {
			$this->view->exception = $errors->exception;
		}

		$this->view->request   = $errors->request;
	}

	public function getLog()
	{
		$bootstrap = $this->getInvokeArg('bootstrap');
		if (!$bootstrap->hasPluginResource('Log')) {
			return false;
		}
		$log = $bootstrap->getResource('Log');
		return $log;
	}
}
