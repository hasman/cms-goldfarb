<?php
class CheckoutController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_db = Zend_Db_Table::getDefaultAdapter();

        $options = Zend_Registry::get('configuration')->toArray();

	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    $this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

	    //if checkout disabled redirect to home (shouldn't be here)
	    if ($settings_mapper->getValueByCode('checkout_status') == 'Disabled') {
		    $this->redirect("/");
	    }

        $this->view->shipping_countries = $this->view->billing_countries =  $all_countries = Zend_Registry::get('countries')->toArray();

        if ($settings_mapper->getValueByCode('ecommerce_shipping_countries')) {
        	$selected = json_decode($settings_mapper->getValueByCode('ecommerce_shipping_countries'));
	        $this->view->shipping_countries = array();
	        foreach ($selected as $k => $country_code) {
		        $this->view->shipping_countries[$country_code] = $all_countries[$country_code];
	        }
        }
	    if ($settings_mapper->getValueByCode('ecommerce_billing_countries')) {
		    $selected = json_decode($settings_mapper->getValueByCode('ecommerce_billing_countries'));
		    $this->view->billing_countries = array();
		    foreach ($selected as $k => $country_code) {
			    $this->view->billing_countries[$country_code] = $all_countries[$country_code];
		    }
	    }

		$this->_taxStates = (isset($options['tax']))?$options['tax']:array();

		/*
		$this->_goole_api_key = $options['google']['api']['key'];
		$this->_captcha_privkey = $options['google']['recaptcha']['invis']['secret'];
		$this->view->captcha_key = $options['google']['recaptcha']['invis']['key'];
		*/

        $this->_cart = new Zend_Session_Namespace('cart');
        if (!is_array($this->_cart->items)) $this->_cart->items = array();

        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
        if( !$auth->hasIdentity() ){
            $this->view->customer = $this->_customer = null;
        }else {
            $this->view->customer = $this->_customer = $auth->getIdentity();

            //Cny_Commerce_Cart::mergeCart($this->_customer->id);
        }
        $post_auth_redirect = new Zend_Session_Namespace('post_auth_redirect');
        $post_auth_redirect->url = "/checkout";

        $this->_shipping = $this->_billing = array(
            'first_name' => '',
            'last_name' => '',
            'company' => '',
            'email' => '',
            'phone' => '',
            'address' => '',
            'address2' => '',
            'city' => '',
            'region' => '',
            'postcode' => '',
            'country' => ''
        );
        $this->_payment = array(
            'name' => '',
            'cc_num' => '',
            'ccv' => '',
            'exp_month' => '',
            'exp_year' => '',
            'shipping_method' => ''
        );

        $this->_checkout = new Zend_Session_Namespace('checkout');
        if (!is_array($this->_checkout->billing)) $this->_checkout->billing = $this->_billing;
        if (!is_array($this->_checkout->shipping)) $this->_checkout->shipping = $this->_shipping;
        if (!is_array($this->_checkout->payment)) $this->_checkout->payment = $this->_payment;
        if (!$this->_checkout->error) $this->_checkout->error = array();

	    //payment method options
	    $this->_payment_methods = array();
	    $this->_default_payment_method = '';
	    if ($settings_mapper->getValueByCode('ecommerce_paypal_status') == 'Enabled') {
	    	$this->_payment_methods['paypal'] = 'paypal';
		    $this->_default_payment_method = 'paypal';
	    }
	    if ($settings_mapper->getValueByCode('ecommerce_amazonpay_status') == 'Enabled') {
		    $this->_payment_methods['amazonpay'] = 'amazon';
		    $this->_default_payment_method = 'amazonpay';

		    /* Amazon Pay Code - Start */
		    $this->view->amazonpay = array();
		    $this->view->amazonpay['seller_id'] = $settings_mapper->getValueByCode('ecommerce_amazonpay_seller_id');

		    $this->view->HeadScript()->appendScript(
<<<amzscript
    var orderReferenceId = null;
    var authRequest = null;
	window.onAmazonLoginReady = function() {
		amazon.Login.setClientId("{$settings_mapper->getValueByCode('ecommerce_amazonpay_client_id')}");
	};
	window.onAmazonPaymentsReady = function() {
		if (typeof showAmazonPayButton === "function") {
			showAmazonPayButton();
		}
	};

amzscript
		    );
		    $this->view->HeadScript()->setAllowArbitraryAttributes(true);
		    $this->view->HeadScript()->appendFile(
			    (APPLICATION_ENV == 'production')?"https://static-na.payments-amazon.com/OffAmazonPayments/us/js/Widgets.js":"https://static-na.payments-amazon.com/OffAmazonPayments/us/sandbox/js/Widgets.js", 'text/javascript', ['defer'=>'defer']
		    );
		    /* Amazon Pay Code - End */
	    }
	    if ($settings_mapper->getValueByCode('ecommerce_payrix_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'payrix';
		    $this->_default_payment_method = 'cc';
	    }elseif ($settings_mapper->getValueByCode('ecommerce_authnet_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'authorize.net';
		    $this->_default_payment_method = 'cc';
	    }elseif ($settings_mapper->getValueByCode('ecommerce_mes_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'mes';
		    $this->_default_payment_method = 'cc';
	    }elseif ($settings_mapper->getValueByCode('ecommerce_stripe_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'stripe';
		    $this->_default_payment_method = 'cc';
	    }elseif ($settings_mapper->getValueByCode('ecommerce_payflowpro_status') == 'Enabled') {
		    $this->_payment_methods['cc'] = 'payflowpro';
		    $this->_default_payment_method = 'cc';
	    }
	    $this->view->payment_methods = $this->_payment_methods;

	    //custom questions/items
	    $this->view->custom_items = array();
	    if ($settings_mapper->getValueByCode('checkout_item_1_status') == 'Enabled') {
		    $this->view->custom_items[1] = array(
		    	'label' => $settings_mapper->getValueByCode('checkout_item_1_label'),
			    'type' => $settings_mapper->getValueByCode('checkout_item_1_type'),
			    'required' => $settings_mapper->getValueByCode('checkout_item_1_required'),
		    );
	    }
	    if ($settings_mapper->getValueByCode('checkout_item_2_status') == 'Enabled') {
		    $this->view->custom_items[2] = array(
			    'label' => $settings_mapper->getValueByCode('checkout_item_2_label'),
			    'type' => $settings_mapper->getValueByCode('checkout_item_2_type'),
			    'required' => $settings_mapper->getValueByCode('checkout_item_2_required'),
		    );
	    }
	    if ($settings_mapper->getValueByCode('checkout_item_3_status') == 'Enabled') {
		    $this->view->custom_items[3] = array(
			    'label' => $settings_mapper->getValueByCode('checkout_item_3_label'),
			    'type' => $settings_mapper->getValueByCode('checkout_item_3_type'),
			    'required' => $settings_mapper->getValueByCode('checkout_item_3_required'),
		    );
	    }
	    if ($settings_mapper->getValueByCode('checkout_item_4_status') == 'Enabled') {
		    $this->view->custom_items[4] = array(
			    'label' => $settings_mapper->getValueByCode('checkout_item_4_label'),
			    'type' => $settings_mapper->getValueByCode('checkout_item_4_type'),
			    'required' => $settings_mapper->getValueByCode('checkout_item_4_required'),
		    );
	    }
	    if ($settings_mapper->getValueByCode('checkout_item_5_status') == 'Enabled') {
		    $this->view->custom_items[5] = array(
			    'label' => $settings_mapper->getValueByCode('checkout_item_5_label'),
			    'type' => $settings_mapper->getValueByCode('checkout_item_5_type'),
			    'required' => $settings_mapper->getValueByCode('checkout_item_5_required'),
		    );
	    }
	    if ($settings_mapper->getValueByCode('checkout_item_6_status') == 'Enabled') {
		    $this->view->custom_items[6] = array(
			    'label' => $settings_mapper->getValueByCode('checkout_item_6_label'),
			    'type' => $settings_mapper->getValueByCode('checkout_item_6_type'),
			    'required' => $settings_mapper->getValueByCode('checkout_item_6_required'),
		    );
	    }
	    $this->_custom_items = $this->view->custom_items;
    }

    public function indexAction()
    {
    	if (!$this->_cart->items) {
            $this->_redirect("/cart");
        }

	    $this->view->placeholder('body_class')->append('checkout-page');

	    $this->view->method = $payment_method = $this->getRequest()->getParam("method", $this->getRequest()->getParam("payment_method", $this->_default_payment_method));
	    $this->view->copyInfo = 'y';

	    $this->view->error = $this->_checkout->error;
	    $this->_checkout->error = array();

	    //Amazon Pay redirect work around
	    $amz_workaround = false;
	    if ($this->view->method == 'amazonpay') {
		    $this->_amz_workaround = new Zend_Session_Namespace('amz_workaround');
		    $this->view->billing = $this->_amz_workaround->billing;
		    $this->view->shipping = $this->_amz_workaround->shipping;
		    $this->view->payment = $this->_amz_workaround->payment;

		    //having an issue with some payment data being set and then causing CC to be selected
		    $this->view->payment['name'] = '';
		    $this->view->payment['cc_num'] = '';

		    $this->_amz_workaround->setExpirationSeconds(1);

		    $amz_workaround = true;
	    }
	    $this->view->amz_redir = $this->getRequest()->getParam('amz_redir', 'n');//for mobile to open the billing accordian


	    $product_mapper = new Application_Model_Mapper_Product();
	    $variant_mapper = new Application_Model_Mapper_ProductVariant();

	    $this->view->promo_code = $promo_code = $this->_cart->promo_code = ($this->_cart->promo_code)?$this->_cart->promo_code:$this->_getParam('promo_code','');
	    if ($this->_getParam('promo_code','')) {
		    $this->view->promo_code = $promo_code = $this->_cart->promo_code = $this->_getParam('promo_code','');
	    }

	    //get base subtotal
	    $base_sub_total = 0;
	    foreach ($this->_cart->items as $k => $item) {
		    $prod['product'] = $product_mapper->find($item['product_id']);

		    if ($prod['product']->status == 'Disabled' || $prod['product']->status == 'Unavailable') {
			    break;
		    }

		    $price = $prod['product']->price;

		    if (isset($item['variant_id']) && $item['variant_id']) {
			    $prod['variant'] = $variant_mapper->find($item['variant_id']);

			    if ($prod['variant']->status == 'Disabled' || $prod['variant']->status == 'Unavailable') {
				    break;
			    }

			    $price = $prod['variant']->price;
		    }

		    if ($price < 0) $price = 0;

		    $base_sub_total += ($price * $item['qty']);
	    }

	    //if there is a promo code
	    $promo_products = array(); $this->view->promo_message = $this->view->promo = $promo = '';
	    if ($promo_code) {
		    $mapper = new Application_Model_Mapper_Promo();
		    $this->view->promo_message = $mapper->validateCode($promo_code, $base_sub_total);
		    if (!$this->view->promo_message) {
			    $this->view->promo = $promo = $mapper->fetchByCode($promo_code, $base_sub_total);

			    if ($promo) {
				    if ($promo->sitewide == 'No') {
					    //get prodoucts
					    $mapper = new Application_Model_Mapper_PromoThresholdProduct();
					    $promo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id);
				    }
				    $this->_cart->valid_promo_code = $promo_code;

				    //BOGO
				    /*
				    if ($promo->type == 'bogo') {
					    $mapper = new Application_Model_Mapper_PromoThresholdProduct();
					    $bogo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id,'bogo');

					    if (count($bogo_products) == 1) {
						    $this->_cart->items[$bogo_products[0]] = array('qty' => 1, 'lic' => 1);
					    }else {
						    $this->view->bogo_products = array();
						    $product_manager = new Application_Model_Mapper_Product();
						    $prod = array();

						    foreach ($bogo_products as $bogo_product_id) {
							    $prod['price'] = 0;
							    $prod['product'] = $product_manager->find($bogo_product_id);

							    $this->view->bogo_products[] = $prod;
						    }
					    }
				    }
				    */
			    } else {
				    $this->_cart->promo_code = "";
				    if ($this->_cart->valid_promo_code) {
					    $this->_cart->promo_code = $this->_cart->valid_promo_code;
					    $this->_cart->setExpirationSeconds(1, 'valid_promo_code');
					    $this->_cart->setExpirationHops(1, 'valid_promo_code');
					    $this->redirect("/cart");
				    }
			    }
		    }else {
			    if ($this->_cart->valid_promo_code) {
				    $this->_cart->promo_code = $this->_cart->valid_promo_code;
				    $this->_cart->setExpirationSeconds(1, 'valid_promo_code');
				    $this->_cart->setExpirationHops(1, 'valid_promo_code');
				    $this->redirect("/cart");
			    }
		    }
	    }
	    //check for auto promo
	    if (!$promo_code || !$promo) {
		    $this->view->promo_message = ''; //reset promo message in case of invalid promo entered
		    $this->_cart->promo_code = '';

		    //if no promo code entered check for automatic promos
		    $mapper = new Application_Model_Mapper_Promo();
		    $this->view->promo = $promo = $mapper->fetchAutomaticPromo($base_sub_total);

		    if ($promo) {
			    if ($promo->sitewide == 'No') {
				    //get products
				    $mapper = new Application_Model_Mapper_PromoThresholdProduct();
				    $promo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id);
			    }

			    //BOGO
			    /*
			    if ($promo->type == 'bogo') {
				    $mapper = new Application_Model_Mapper_PromoThresholdProduct();
				    $bogo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id,'bogo');

				    if (count($bogo_products) == 1) {
					    $this->_cart->items[$bogo_products[0]] = array('qty' => 1, 'lic' => 1);
				    }else {
					    $this->view->bogo_products = array();
					    $product_manager = new Application_Model_Mapper_Product();
					    $prod = array();

					    foreach ($bogo_products as $bogo_product_id) {
						    $prod['price'] = 0;
						    $prod['product'] = $product_manager->find($bogo_product_id);

						    $this->view->bogo_products[] = $prod;
					    }
				    }
			    }
			    */
		    }
	    }

	    $this->view->products = array();
	    $sub_total = 0;
	    $world_restricted = $ca_restricted = array();
	    $discount = 0;
	    $po_box = false;

	    foreach ($this->_cart->items as $k => $item) {
		    $prod = array();

		    $prod['product'] = $product_mapper->find($item['product_id']);

		    if ($prod['product']->status == 'Disabled' || $prod['product']->status == 'Unavailable') {
			    unset($this->_cart->items[$k]);
			    break;
		    }

		    $prod['qty'] = $item['qty'];
		    $prod['base_price'] = $prod['price'] = $prod['product']->price;
		    $prod['msrp'] = $prod['product']->msrp;

		    if (isset($item['variant_id']) && $item['variant_id']) {
			    $prod['variant'] = $variant_mapper->find($item['variant_id']);

			    if ($prod['variant']->status == 'Disabled' || $prod['variant']->status == 'Unavailable') {
				    unset($this->_cart->items[$k]);
				    break;
			    }

			    $prod['price'] = $prod['variant']->price;
			    $prod['msrp'] = $prod['variant']->msrp;
		    }

		    if ($promo && ($promo->sitewide == 'Yes' || ((isset($item['variant_id']) && $item['variant_id'] && in_array($item['variant_id'],$promo_products['variants'])) || ($item['product_id'] && in_array($item['product_id'],$promo_products['products']))) )) {
			    //get excluded products
			    $mapper = new Application_Model_Mapper_PromoThresholdProduct();
			    $exclude_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id,'exclude');
			    //$bogo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id,'bogo');
			    if (($item['product_id'] && in_array($item['product_id'],$exclude_products['products'])) || (isset($item['variant_id']) && $item['variant_id'] && in_array($item['variant_id'],$exclude_products['variants'])) ) {
				    $prod['price'] = $prod['price'];
			    }else {
				    $price_discount = 0;
				    if ($promo->type == "fixed") {
					    $price_discount = $promo->amount;
				    } elseif ($promo->type == "percent") {
					    $price_discount = round($prod['price'] * ($promo->amount / 100), 2);
				    } elseif ($promo->type == "percent_msrp") {
					    $new_price = round($prod['msrp'] - ($prod['msrp'] * ($promo->amount / 100)), 2);
					    $price_discount = $prod['price'] - (($new_price < $prod['price']) ? $new_price : $prod['price']);
				    } elseif ($promo->type == "price") {
					    $price_discount = abs($promo->amount - $prod['price']);
				    }
				    /*
					elseif ($promo->type == "bogo") {
						foreach ($bogo_products as $bogo_product_id) {
							if ($id == $bogo_product_id) {
								if ($bogo_prod_count < 1) {
									$prod['price'] = 0;
								}else {
									$prod['price'] = $prod['price'];
								}
								$bogo_prod_count++;
							}
						}
					}
					*/

				    $prod['price'] = $prod['price'] - $price_discount;

				    $discount += $price_discount;
			    }
		    }else {
			    $prod['price'] = $prod['price'];
		    }

		    if ($prod['price'] < 0) $prod['price'] = 0;

		    $sub_total += ($prod['price'] * $prod['qty']);
		    $this->_cart->sub_total = $sub_total;

		    $this->view->products[] = $prod;

		    //save price in cart session so we can use it in shipping calc
		    $this->_cart->items[$k]['price'] = $prod['price'];
	    }

	    //second check of items in cart in case products went out of stock
	    if (!$this->_cart->items) {
		    $this->_redirect("/cart");
	    }

	    $this->_checkout->products = $this->view->products;

	    //entire order promo
	    if ($promo && $promo->type == "fixed_order") {
		    $this->view->discount = $discount = ($promo->amount > $sub_total) ? $sub_total : $promo->amount;
		    $sub_total = $sub_total - $discount;
	    }

	    $this->view->order_total = $this->_checkout->sub_total = $sub_total;

	    $this->view->discount = $discount;

	    $customer_mapper = new Application_Model_Mapper_Customer();
	    $customer_address_mapper = new Application_Model_Mapper_CustomerAddress();

	    $customer_id = (is_object($this->_customer) && $this->_customer->id) ? $this->_customer->id : 0;
	    $customer = $customer_mapper->find($customer_id);
	    $customer_billing = $customer_address_mapper->findByCustomerId($customer_id, 'billing', 'Yes');
	    $customer_shipping = $customer_address_mapper->findByCustomerId($customer_id, 'shipping', 'Yes');

	    if (!$amz_workaround) {
		    $this->view->billing = array(
			    'first_name' => ($customer_billing->first_name) ? $customer_billing->first_name : (is_object($customer) && $customer->first_name) ? $customer->first_name : '',
			    'last_name' => ($customer_billing->last_name) ? $customer_billing->last_name : (is_object($customer) && $customer->last_name) ? $customer->last_name : '',
			    'email' => (is_object($customer) && $customer->email) ? $customer->email : '',
			    'phone' => ($customer_billing->phone) ? $customer_billing->phone : '',
			    'company' => ($customer_billing->company) ? $customer_billing->company : '',
			    'address' => ($customer_billing->address1) ? $customer_billing->address1 : '',
			    'address2' => ($customer_billing->address2) ? $customer_billing->address2 : '',
			    'city' => ($customer_billing->city) ? $customer_billing->city : '',
			    'region' => ($customer_billing->region) ? $customer_billing->region : '',
			    'postcode' => ($customer_billing->postcode) ? $customer_billing->postcode : '',
			    'country' => ($customer_billing->country) ? $customer_billing->country : ''
		    );
		    $this->view->shipping = array(
			    'first_name' => ($customer_shipping->first_name) ? $customer_shipping->first_name : (is_object($customer) && $customer->first_name) ? $customer->first_name : '',
			    'last_name' => ($customer_shipping->last_name) ? $customer_shipping->last_name : (is_object($customer) && $customer->last_name) ? $customer->last_name : '',
			    'email' => (is_object($customer) && $customer->email) ? $customer->email : '',
			    'phone' => ($customer_shipping->phone) ? $customer_shipping->phone : '',
			    'company' => ($customer_shipping->company) ? $customer_shipping->company : '',
			    'address' => ($customer_shipping->address1) ? $customer_shipping->address1 : '',
			    'address2' => ($customer_shipping->address2) ? $customer_shipping->address2 : '',
			    'city' => ($customer_shipping->city) ? $customer_shipping->city : '',
			    'region' => ($customer_shipping->region) ? $customer_shipping->region : '',
			    'postcode' => ($customer_shipping->postcode) ? $customer_shipping->postcode : '',
			    'country' => ($customer_shipping->country) ? $customer_shipping->country : '',
		    );
	    }

	    $regions = Zend_Registry::get('regions')->toArray();
	    $this->view->billing_states = (array_key_exists(strtolower($this->view->billing['country']), $regions))?$regions[strtolower($this->view->billing['country'])]['region']:null;
	    $this->view->shipping_states = (array_key_exists(strtolower($this->view->shipping['country']), $regions))?$regions[strtolower($this->view->shipping['country'])]['region']:null;

        $form = new Application_Form_Checkout();
        if ($this->getRequest()->isPost()) {
	        $payment = $this->getRequest()->getParam("payment", $this->view->payment);
	        $this->view->payment = $this->_checkout->payment = $payment;

	        $billing = $this->getRequest()->getParam("billing", $this->view->billing);
	        $shipping = $this->getRequest()->getParam("shipping", $this->view->shipping);

	        //if Copy to Billing is checked copy from shipping info
	        $this->view->copyInfo = $copyInfo = $this->getRequest()->getParam("copyInfo", "n");
	        if ($copyInfo == "y") {
		        $form->getSubForm('billing')->getElement('first_name')->setValue($shipping['first_name']);
		        $_POST['billing']['first_name'] = $billing['first_name'] = $shipping['first_name'];

		        $form->getSubForm('billing')->getElement('last_name')->setValue($shipping['last_name']);
		        $_POST['billing']['last_name'] = $billing['last_name'] = $shipping['last_name'];

		        /*
				if ($billing['phone'] == '') {
					$form->getSubForm('billing')->getElement('phone')->setValue($shipping['phone']);
					$_POST['billing']['phone'] = $billing['phone'] = $shipping['phone'];
				}
				*/

		        $form->getSubForm('billing')->getElement('email')->setValue($shipping['email']);
		        $_POST['billing']['email'] = $billing['email'] = $shipping['email'];

		        $form->getSubForm('billing')->getElement('address')->setValue($shipping['address']);
		        $_POST['billing']['address'] = $billing['address'] = $shipping['address'];

		        $form->getSubForm('billing')->getElement('address2')->setValue($shipping['address2']);
		        $_POST['billing']['address2'] = $billing['address2'] = $shipping['address2'];

		        $form->getSubForm('billing')->getElement('city')->setValue($shipping['city']);
		        $_POST['billing']['city'] = $billing['city'] = $shipping['city'];

		        if (isset($shipping['region'])) {
			        $form->getSubForm('billing')->getElement('region')->setValue($shipping['region']);
			        $_POST['billing']['region'] = $billing['region'] = $shipping['region'];
		        }

		        $form->getSubForm('billing')->getElement('postcode')->setValue($shipping['postcode']);
		        $_POST['billing']['postcode'] = $billing['postcode'] = $shipping['postcode'];

		        $form->getSubForm('billing')->getElement('country')->setValue($shipping['country']);
		        $_POST['billing']['country'] = $billing['country'] = $shipping['country'];
	        }

	        $regions = Zend_Registry::get('regions')->toArray();
	        if (array_key_exists(strtolower($billing['country']), $regions)) {
		        $this->view->billing_states = $regions[strtolower($billing['country'])]['region'];
	        }else {
		        $this->view->billing_states = null;

		        $billing['region'] = $billing['region_alt'];
		        $form->getSubForm('billing')->getElement('region')->setValue($billing['region']);
		        $form->getSubForm('billing')->getElement('region')->setRequired(false);
	        }

	        if (array_key_exists(strtolower($shipping['country']), $regions)) {
		        $this->view->shipping_states = $regions[strtolower($shipping['country'])]['region'];
	        }else {
		        $this->view->shipping_states = null;

		        $shipping['region'] = $shipping['region_alt'];
		        $form->getSubForm('shipping')->getElement('region')->setValue($shipping['region']);
		        $form->getSubForm('shipping')->getElement('region')->setRequired(false);
	        }

	        $this->view->billing = $this->_checkout->billing = $billing;
	        $this->view->shipping = $this->_checkout->shipping = $shipping;


	        /*
	        //check if customer in the block list. If so just refresh the page
	        $block_mapper = new Application_Model_Mapper_CustomerBlock();
	        $blocked = $block_mapper->verify($shipping, $_SERVER['REMOTE_ADDR']);

	        if ($blocked) {
		        $this->_redirect("/checkout");
		        exit;
	        }
	        */

	        //handle zip/postal code formatting
	        $s_postcode_error = $b_postcode_error = false;
	        if ($shipping['country'] == 'US') {
		        $cleaned = preg_replace('/[^\d]/i', '', $shipping['postcode']);
		        if (strlen($cleaned) <> 5 && strlen($cleaned) <> 9) {
			        $s_postcode_error = true;
		        }
	        } elseif ($shipping['country'] == 'CA') {
		        $cleaned = preg_replace('/[^\da-z]/i', '', $shipping['postcode']);
		        if (strlen($cleaned) <> 6) {
			        $s_postcode_error = true;
		        } else {
			        $shipping['postcode'] = substr($cleaned, 0, 3) . " " . substr($cleaned, 3, 3);
		        }
	        }
	        $this->view->shipping = $this->_checkout->shipping = $shipping;

	        //can ignore for non-cc orders. Those only have to worry about shipping info
	        if ($payment_method == 'cc') {
		        if ($billing['country'] == 'US') {
			        $cleaned = preg_replace('/[^\d]/i', '', $billing['postcode']);
			        if (strlen($cleaned) <> 5 && strlen($cleaned) <> 9) {
				        $b_postcode_error = true;
			        }
		        } elseif ($billing['country'] == 'CA') {
			        $cleaned = preg_replace('/[^\da-z]/i', '', $billing['postcode']);
			        if (strlen($cleaned) <> 6) {
				        $b_postcode_error = true;
			        } else {
				        $billing['postcode'] = substr($billing['postcode'], 0, 3) . " " . substr($billing['postcode'], 4, 3);
			        }
		        }
	        }
	        $this->view->billing = $this->_checkout->billing = $billing;

	        //can't ship to Canada PO Boxes
	        if ($shipping['country'] == 'CA') {
		        if (stripos($shipping['address'], 'po box') !== false || stripos($shipping['address'], 'p.o. box') !== false ||
			        stripos($shipping['address2'], 'po box') !== false || stripos($shipping['address2'], 'p.o. box') !== false) {
			        $po_box = true;
		        }
	        }

	        //temp since no reCAPTCHA
	        $form->getSubForm('payment')->removeElement('captcha');

	        if ($payment_method == "paypal" || $payment_method == "amazonpay") {
		        $form->getSubForm('billing')->removeElement('first_name');
		        $form->getSubForm('billing')->removeElement('last_name');
		        $form->getSubForm('billing')->removeElement('address');
		        $form->getSubForm('billing')->removeElement('city');
		        $form->getSubForm('billing')->removeElement('region');
		        $form->getSubForm('billing')->removeElement('postcode');
		        $form->getSubForm('billing')->removeElement('country');
		        $form->getSubForm('billing')->removeElement('email');

		        $form->getSubForm('payment')->removeElement('name');
		        $form->getSubForm('payment')->removeElement('cc_num');
		        $form->getSubForm('payment')->removeElement('ccv');
		        $form->getSubForm('payment')->removeElement('exp_month');
		        $form->getSubForm('payment')->removeElement('exp_year');
		        $form->getSubForm('payment')->removeElement('captcha');
	        }

	        $custom_valid = true;
	        //handle validation of custom items
	        if ($this->getRequest()->getParam('promo_btn', '') != '1') { //can ignore if promo button pressed
		        $this->view->profile = $profile = $this->_getParam('profile', array());
		        foreach ($this->_custom_items as $k => $custom_item) {
			        if ($custom_item['required'] == 'Yes' && (!isset($profile['custom_item_' . $k . '_value']) || !$profile['custom_item_' . $k . '_value'])) {
				        $custom_valid = false;
				        $this->view->error[] = $custom_item['label'] . " required";
			        }
		        }
	        }

	        if ($form->isValid($this->getRequest()->getParams())
		        && $this->getRequest()->getParam('promo_btn', '') != '1'
		        && $custom_valid
		    //    && $this->view->restriction_warning === false && $s_postcode_error === false && $b_postcode_error === false
		    //    && $po_box === false
	        ) {
		        $this->_checkout->profile = $profile;

		        $tax = $tax_rate = 0;
		        //find tax if in set states
		        if (array_key_exists($shipping['region'], $this->_taxStates)) {
		        	$tax_rate = $this->_taxStates[$shipping['region']];

		        	/*
			        $ziptax = new Cny_Model_ZipTax();
			        $tax_response = $ziptax->taxRate($shipping);
			        //need to save the county for shipping address for tax report
			        $this->_checkout->shipping['county'] = $tax_response->geoCounty;
			        //$this->_checkout->tax_rate = $tax_response->taxSales;
			        $this->_checkout->tax_details = $tax_response;

		        	$tax_rate = $tax_response->taxSales;
			        */
		        }

		        //promo/discount
		        $this->view->discount = $discount;

		        //tax
		        //$this->view->tax = $tax = round(($sub_total * $tax_rate),2);
		        //introducing rounding issue so better do it per product the same we need/use is later
		        if ($tax_rate) {
			        foreach ($this->view->products as $product) {
				        $tax += number_format(($product['price'] * $product['qty']) * ($tax_rate/100), 2);
			        }
		        }
		        $this->view->tax = $tax;


		        //sub total
		        $sub_total += $discount;//sub_total has the discount in it already but we need to show the breakdown.

		        //shipping cost
		        $shipping_model = new Application_Model_Shipping();
		        $shipping_options = $shipping_model->getShippingOptions($shipping, $this->view->products);
		        $shipping_cost = $shipping_options[$payment['shipping_method']]['rate'];

		        //total
		        $cost = $sub_total + $tax + $shipping_cost;

		        $this->_checkout->totals = array(
			        'shipping' => $shipping_cost,
			        'tax' => $tax,
			        'discount' => $discount,
			        'sub_total' => $sub_total,
			        'total' => round($cost - $discount, 2)
		        );

		        if ($cost - $discount <= 0) { //free order doesn't need to be processed by payment processor
			        $order_mapper = new Application_Model_Mapper_Order();

			        $order_data = array(
				        'customer_id' => ($this->_customer) ? $this->_customer->id : null,
				        'email' => $billing['email'],
				        'bill_method' => 'free',
				        'status' => 'Order Taken',
				        'tax_rate' => ($tax_rate) ? $tax_rate : null
			        );
			        $order = new Application_Model_Order($order_data);
			        $order_id = $order_mapper->save($order);

			        $mapper = new Application_Model_Mapper_PaymentHistory();
			        $payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
				        'order_id' => $order_id,
				        'type' => 'free',
				        'cc_type' => null,
				        'amount' => $cost - $discount,
				        'amount_remaining' => $cost - $discount,
				        'transaction_id' =>null,
				        'response' => null,
				        'status' => 'sale'
			        )));
			        //saving payment history products
			        $mapper = new Application_Model_Mapper_PaymentHistoryProduct();
			        foreach ($this->view->products as $product) {
				        $mapper->save(new Application_Model_PaymentHistoryProduct(array(
					        'payment_history_id' => $payment_history_id,
					        'product_id' => $product['product']->id
				        )));
			        }

			        $order_payment_mapper = new Application_Model_Mapper_OrderPayment();
			        $order_payment_data = array(
				        'order_id' => $order_id,
				        'type' => 'free',
				        'transaction_id' => null,
				        'identifier' => null,
				        'card_token' => null,
				        'total_amount' => round($cost - $discount, 2),
				        'discount_amount' => $discount,
				        'shipping_amount' => $shipping_cost,
				        'tax_amount' => $tax,
				        'request' => '',
				        'response' => null,
				        'status' => 'approved',
			        );
			        $order_payment = new Application_Model_OrderPayment($order_payment_data);
			        $order_payment_mapper->save($order_payment);

			        $this->processOrder($order_id, "FREE", $this->_checkout, $this->_cart, $this->_customer);
		        }else {
			        if ($payment_method == "paypal" && array_key_exists('paypal', $this->_payment_methods)) {
				        $paypal = new Application_Model_Paypal();
				        $response = $paypal->createTransaction($this->view->products, $this->_checkout->totals);

				        if ($response) {
					        $this->view->error[] = $response;
				        }
			        } elseif ($payment_method == "cc" && array_key_exists('cc', $this->_payment_methods)) {
				        switch (substr($payment['cc_num'], 0, 1)) {
					        case "3":
						        $cc_type = "Amex";
						        break;
					        case "4":
						        $cc_type = "Visa";
						        break;
					        case "5":
						        $cc_type = "MasterCard";
						        break;
					        case "6":
						        $cc_type = "Discover";
						        break;
				        }
				        $payment['cc_type'] = $cc_type;

				        //normalize to 5 digit zip for US billing zipcode
				        if ($billing['country'] == 'US' && strlen($billing['postcode']) > 5) {
					        $billing['postcode'] = substr(preg_replace("/[^0-9]/", "", $billing['postcode']), 0, 5);
					        $this->_checkout->billing = $billing;
				        }

				        if ($this->_payment_methods['cc'] == 'payrix') {
					        $payrix = new Application_Model_Payrix();
					        $response = $payrix->createTransaction($payment, $this->_checkout->totals, $billing, $shipping, $this->view->products);

					        if (isset($response['response']->id) && $response['response']->id) { //approved
						        $payment_profile_id = $customer_profile_id = new Zend_Db_Expr("NULL");

						        $order_mapper = new Application_Model_Mapper_Order();

						        $order_data = array(
							        'customer_id' => ($this->_customer) ? $this->_customer->id : null,
							        'email' => $billing['email'],
							        'bill_method' => 'cco',
							        'status' => 'Order Taken',
							        'tax_rate' => ($tax_rate) ? $tax_rate : null
						        );
						        $order = new Application_Model_Order($order_data);
						        $order_id = $order_mapper->save($order);

						        $mapper = new Application_Model_Mapper_PaymentHistory();
						        $payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
							        'order_id' => $order_id,
							        'type' => 'cc',
							        'cc_type' => $cc_type,
							        'amount' => $cost - $discount,
							        'amount_remaining' => $cost - $discount,
							        'transaction_id' => $response['response']->id,
							        'response' => json_encode($response['response']),
							        'status' => 'sale'
						        )));
						        //saving payment history products
						        $mapper = new Application_Model_Mapper_PaymentHistoryProduct();
						        foreach ($this->view->products as $product) {
							        $mapper->save(new Application_Model_PaymentHistoryProduct(array(
								        'payment_history_id' => $payment_history_id,
								        'product_id' => $product['product']->id
							        )));
						        }

						        $order_payment_mapper = new Application_Model_Mapper_OrderPayment();
						        $order_payment_data = array(
							        'order_id' => $order_id,
							        'type' => 'cc',
							        'transaction_id' => $response['response']->id,
							        'identifier' => $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]",
							        'card_token' => $payment_profile_id,
							        'total_amount' => round($cost - $discount, 2),
							        'discount_amount' => $discount,
							        'shipping_amount' => $shipping_cost,
							        'tax_amount' => $tax,
							        'request' => '',
							        'response' => json_encode($response['response']),
							        'status' => 'approved',
						        );
						        $order_payment = new Application_Model_OrderPayment($order_payment_data);
						        $order_payment_mapper->save($order_payment);


						        $this->processOrder($order_id, $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]", $this->_checkout, $this->_cart, $this->_customer);
					        } else {
						        $this->view->error[] = $response['error_message'];
					        }
				        } elseif ($this->_payment_methods['cc'] == 'payflowpro') {
					        $payflow = new Application_Model_PayflowPro();
					        $response = $payflow->sale_transaction($payment, $this->_checkout->totals, $billing, $shipping, $this->view->products);

					        if (isset($response['response']['RESULT']) && $response['response']['RESULT'] == 0) {
						        $order_mapper = new Application_Model_Mapper_Order();

						        $order_data = array(
							        'customer_id' => ($this->_customer) ? $this->_customer->id : null,
							        'email' => $billing['email'],
							        'bill_method' => 'cco',
							        'status' => 'Order Taken',
							        'tax_rate' => ($tax_rate) ? $tax_rate : null
						        );
						        $order = new Application_Model_Order($order_data);
						        $order_id = $order_mapper->save($order);

						        $mapper = new Application_Model_Mapper_PaymentHistory();
						        $payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
							        'order_id' => $order_id,
							        'type' => 'cc',
							        'cc_type' => $cc_type,
							        'amount' => $cost - $discount,
							        'amount_remaining' => $cost - $discount,
							        'transaction_id' => $response['response']['PNREF'],
							        'response' => serialize($response['response']),
							        'status' => 'sale'
						        )));
						        //saving payment history products
						        $mapper = new Application_Model_Mapper_PaymentHistoryProduct();
						        foreach ($this->view->products as $product) {
							        $mapper->save(new Application_Model_PaymentHistoryProduct(array(
								        'payment_history_id' => $payment_history_id,
								        'product_id' => $product['product']->id
							        )));
						        }

						        $order_payment_mapper = new Application_Model_Mapper_OrderPayment();
						        $order_payment_data = array(
							        'order_id' => $order_id,
							        'type' => 'cc',
							        'transaction_id' => $response['response']['PNREF'],
							        'identifier' => $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]",
							        'card_token' => $response['response']['PNREF'],
							        'total_amount' => round($cost - $discount, 2),
							        'discount_amount' => $discount,
							        'shipping_amount' => $shipping_cost,
							        'tax_amount' => $tax,
							        'request' => '',
							        'response' => serialize($response['response']),
							        'status' => 'approved',
						        );
						        $order_payment = new Application_Model_OrderPayment($order_payment_data);
						        $order_payment_mapper->save($order_payment);


						        $this->processOrder($order_id, $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]", $this->_checkout, $this->_cart, $this->_customer);
					        } else {
						        $this->view->error[] = $response['response']['RESPMSG'];
					        }
				        } elseif ($this->_payment_methods['cc'] == 'authorize.net') {
					        //authorize.net
					        $authnet = new Application_Model_AuthorizeNet();
					        $response = $authnet->createTransaction($payment, $this->_checkout->totals, $billing, $shipping, $this->view->products);

					        if (($response['response'] != null) && ($response['response']->getResponseCode() == "1")) { //approved
						        $payment_profile_id = $customer_profile_id = new Zend_Db_Expr("NULL");
						        if (($response['profile_response'] != null) && ($response['profile_response']->getMessages()->getResultCode() == "Ok")) {
							        $customer_profile_id = $response['profile_response']->getCustomerProfileId();
							        $payment_profile_id = current($response['profile_response']->getCustomerPaymentProfileIdList());
						        } else {
							        //echo "ERROR Response : " . $response['profile_response']->getMessages()->getMessage()[0]->getCode() . "  " .$response['profile_response']->getMessages()->getMessage()[0]->getText() . "\n";
						        }

						        $order_mapper = new Application_Model_Mapper_Order();

						        $order_data = array(
							        'customer_id' => ($this->_customer) ? $this->_customer->id : null,
							        'email' => $billing['email'],
							        'bill_method' => 'cco',
							        'status' => 'Order Taken',
							        'tax_rate' => ($tax_rate) ? $tax_rate : null
						        );
						        $order = new Application_Model_Order($order_data);
						        $order_id = $order_mapper->save($order);

						        $mapper = new Application_Model_Mapper_PaymentHistory();
						        $payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
							        'order_id' => $order_id,
							        'type' => 'cc',
							        'cc_type' => $cc_type,
							        'amount' => $cost - $discount,
							        'amount_remaining' => $cost - $discount,
							        'transaction_id' => $response['response']->getTransId(),
							        'response' => json_encode($response['response']),
							        'status' => 'sale'
						        )));
						        //saving payment history products
						        $mapper = new Application_Model_Mapper_PaymentHistoryProduct();
						        foreach ($this->view->products as $product) {
							        $mapper->save(new Application_Model_PaymentHistoryProduct(array(
								        'payment_history_id' => $payment_history_id,
								        'product_id' => $product['product']->id
							        )));
						        }

						        $order_payment_mapper = new Application_Model_Mapper_OrderPayment();
						        $order_payment_data = array(
							        'order_id' => $order_id,
							        'type' => 'cc',
							        'transaction_id' => $response['response']->getTransId(),
							        'identifier' => $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]",
							        'card_token' => $payment_profile_id,
							        'total_amount' => round($cost - $discount, 2),
							        'discount_amount' => $discount,
							        'shipping_amount' => $shipping_cost,
							        'tax_amount' => $tax,
							        'request' => '',
							        'response' => json_encode($response['response']),
							        'status' => 'approved',
						        );
						        $order_payment = new Application_Model_OrderPayment($order_payment_data);
						        $order_payment_mapper->save($order_payment);


						        $this->processOrder($order_id, $cc_type . " [" . substr($payment['cc_num'], -4, 4) . "]", $this->_checkout, $this->_cart, $this->_customer);
					        } else {
						        if (isset($response['error_message']) && $response['error_message']) {
							        return $response['error_message'];
						        }elseif (isset($response['response']) && $response['response']->getErrors() != null) {
							        $this->view->error[] = $response['response']->getErrors()[0]->getErrorText(); //$response['response']->getErrors()[0]->getErrorCode()
						        } else {
							        if ($response['profile_response']->getMessages() != null) {
								        $this->view->error[] = $response['profile_response']->getMessages()->getMessage()[0]->getText(); //$response['profile_response']->getErrors()[0]->getErrorCode()
							        }
						        }
					        }
				        }
			        } elseif ($payment_method == "amazonpay" && array_key_exists('amazonpay', $this->_payment_methods)) {
				        $amazon_pay = new Application_Model_AmazonPay();
				        $response = $amazon_pay->authorize($payment, $this->_checkout->totals, $this->getRequest()->getParam('amazon_order_reference_id', ''));

				        if (isset($response['response']->AuthorizeResult->AuthorizationDetails->AuthorizationStatus->State) && $response['response']->AuthorizeResult->AuthorizationDetails->AuthorizationStatus->State != "Declined") {
					        $order_mapper = new Application_Model_Mapper_Order();

					        $order_data = array(
						        'customer_id' => $customer_id,
						        'email' => $billing['email'],
						        'bill_method' => 'amazonpay',
						        'status' => 'Order Taken',
						        'tax_rate' => ($tax_rate) ? $tax_rate : null
					        );
					        $order = new Application_Model_Order($order_data);
					        $order_id = $order_mapper->save($order);

					        $mapper = new Application_Model_Mapper_PaymentHistory();
					        $payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
						        'order_id' => $order_id,
						        'type' => 'amazonpay',
						        'cc_type' => 'Amazon Pay',
						        'amount' => $cost - $discount,
						        'amount_remaining' => $cost - $discount,
						        'transaction_id' => $response['response']->AuthorizeResult->AuthorizationDetails->AmazonAuthorizationId,
						        'response' => json_encode($response['response']),
						        'status' => 'preauth'
					        )));
					        //saving payment history products
					        $mapper = new Application_Model_Mapper_PaymentHistoryProduct();
					        foreach ($this->view->products as $product) {
						        $mapper->save(new Application_Model_PaymentHistoryProduct(array(
							        'payment_history_id' => $payment_history_id,
							        'product_id' => $product['product']->id
						        )));
					        }

					        $order_payment_mapper = new Application_Model_Mapper_OrderPayment();
					        $order_payment_data = array(
						        'order_id' => $order_id,
						        'type' => 'amazonpay',
						        'transaction_id' => $this->getRequest()->getParam('amazon_order_reference_id', ''),
						        'identifier' => "",
						        'mes_card_token' => '',
						        'total_amount' => round($cost - $discount, 2),
						        'discount_amount' => $discount,
						        'shipping_amount' => $shipping_cost,
						        'tax_amount' => $tax,
						        'request' => '',
						        'response' => json_encode($response['response']),
						        'status' => 'approved',
					        );
					        $order_payment = new Application_Model_OrderPayment($order_payment_data);
					        $order_payment_mapper->save($order_payment);

					        //capture funds
					        $response = $amazon_pay->capture($response['response']->AuthorizeResult->AuthorizationDetails->AmazonAuthorizationId, $cost - $discount, $order_id . "-" . time());

					        $mapper = new Application_Model_Mapper_PaymentHistory();
					        $payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
						        'order_id' => $order_id,
						        'type' => 'amazonpay',
						        'cc_type' => 'Amazon Pay',
						        'amount' => $cost - $discount,
						        'amount_remaining' => $cost - $discount,
						        'transaction_id' => $response['response']->CaptureResult->CaptureDetails->AmazonCaptureId,
						        'response' => json_encode($response['response']),
						        'status' => 'sale'
					        )));

					        $this->processOrder($order_id, "Amazon Pay", $this->_checkout, $this->_cart, $this->_customer);
				        } elseif (isset($responseObject->Error)) {
					        $this->view->error[] = $response['response']->Error->Message;
				        } else {
					        $this->view->error[] = "Declined by Amazon Pay";
				        }

			        }
		        }
	        } else {
		        if ($this->getRequest()->getParam('promo_btn', '') != '1') {
			        foreach ($form->getMessages() as $subform => $errors) {
				        foreach ($errors as $k => $v) {
					        $key = $subform . "_" . $k;

					        if (array_key_exists(0, $v))
						        $this->view->error[$key] = $v[0];
					        if (array_key_exists('isEmpty', $v))
						        $this->view->error[$key] = $v['isEmpty'];
				        }
			        }
			        //print_r($form->getMessages());
			        //$this->view->error = "Please fill in all fields";

			        if ($this->view->restriction_warning === true) {
				        $this->view->error['shipping_restriction'] = "There are items in your cart that can not be shipped
                                to your country. Please go back to the cart and remove them.";
			        }

			        /*
			        if ($s_postcode_error === true) {
				        $this->view->error['shipping_postcode'] = "There is a problem with the shipping zip/postcode.";
			        }
			        if ($b_postcode_error === true) {
				        $this->view->error['billing_postcode'] = "There is a problem with the billing zip/postcode.";
			        }
			        */

			        if ($po_box === true) {
				        $this->view->error['po_box'] = "We cannot ship to PO Boxes in Canada.";
				        $this->view->error['shipping_address'] = "";
				        $this->view->error['shipping_address2'] = "";
			        }
		        }else {
		        	//if they applied a promo and a shipping method was selected let's reselect it
			        $shipping_model = new Application_Model_Shipping();
			        $this->view->shipping_options = $shipping_model->getShippingOptions($shipping, $this->view->products);
		        }
	        }
        }

    }

	public function paypalAction()
	{
		$this->view->error = array();
		$billing = $this->_checkout->billing;
		$shipping = $this->_checkout->shipping;
		$payment = $this->_checkout->payment;

		$checkout = $this->_checkout;
		$cart = $this->_cart;
		$customer = $this->_customer;

		$paypal = new Application_Model_Paypal();

		if (isset($_GET['paymentId']) ) {
			if (!$shipping || !$cart || !isset($checkout->products) || count($checkout->products) == 0) {
				$this->_checkout->error[] = "There was a problem with the cart";
				$this->_redirect("/cart");
				exit;
			}

			$paymentId = $_GET['paymentId'];
			$result = $paypal->executeTransaction($paymentId);

			if (isset($result['error'])) {
				$this->_checkout->error[] = $result['error'];

				$this->_redirect("/checkout");
				exit;
			}

			/*
			//check if customer in the block list. If so just refresh the page
			$block_address = $shipping; //doing this so we can manipulate the data without effecting info saved
			$block_address['email'] = $payment->getPayer()->getPayerInfo()->getEmail();
			$block_mapper = new Application_Model_Mapper_CustomerBlock();
			$blocked = $block_mapper->verify($block_address, $_SERVER['REMOTE_ADDR']);

			if ($blocked) {
				$this->_redirect("/checkout");exit;
			}
			*/

			if ($result['payment']->getState() == 'approved') {
				//fix for possible duplicate orders
				$sql = $this->_db->quoteInto("SELECT order_id FROM order_payments WHERE transaction_id = ?", $result['payment']->getId());
				$duplicate = $this->_db->fetchOne($sql);
				if ($duplicate) {
					$this->_cart->setExpirationHops(1);
					$this->_cart->setExpirationSeconds(1);
					$this->_checkout->setExpirationHops(1);
					$this->_checkout->setExpirationSeconds(1);

					//Cny_Commerce_Cart::removeAll();

					$this->_order_info = new Zend_Session_Namespace('order_info');
					$this->_order_info->order_id = $duplicate;

					//unset($_SESSION['payer_id']);
					//unset($_SESSION['token']);

					$this->_redirect("/checkout/confirmation");
					exit;
				}

				$order_mapper = new Application_Model_Mapper_Order();

				$customer_id = (is_object($customer) && $customer->id) ? $customer->id : 0;
				$order_data = array(
					'customer_id' => $customer_id,
					'bill_method' => 'paypal',
					'email' => $result['payment']->getPayer()->getPayerInfo()->getEmail(),
					'status' => 'Order Taken',
					'tax_rate' => (isset($checkout->tax_details))?$checkout->tax_details->taxSales:null
				);
				$order = new Application_Model_Order($order_data);
				$order_id = $order_mapper->save($order);

				$mapper = new Application_Model_Mapper_PaymentHistory();
				$payment_history_id = $mapper->save(new Application_Model_PaymentHistory(array(
					'order_id' => $order_id,
					'type' => 'paypal',
					'cc_type' => 'PayPal',
					'amount' => $result['payment']->getTransactions()[0]->getAmount()->getTotal(),
					'transaction_id' => $result['payment']->getId(),
					'response' => json_encode($result['payment']),
					'status' => 'sale'
				)));
				//saving payment history products
				$mapper = new Application_Model_Mapper_PaymentHistoryProduct();
				foreach ($cart->items as $id => $product) {
					$mapper->save(new Application_Model_PaymentHistoryProduct(array(
						'payment_history_id' => $payment_history_id,
						'product_id' => $id
					)));
				}

				$order_payment_mapper = new Application_Model_Mapper_OrderPayment();
				$order_payment_data = array(
					'order_id' => $order_id,
					'type' => 'paypal',
					'transaction_id' => $result['payment']->getId(),
					'sale_id' => $result['result']->getTransactions()[0]->getRelatedResources()[0]->getSale()->getId(),
					'identifier' => "Paypal - " . $result['payment']->getPayer()->getPayerInfo()->getPayerId(),
					'total_amount' => $result['payment']->getTransactions()[0]->getAmount()->getTotal(),
					//'discount_amount' => $payment->getTransactions()[0]->getAmount()->getDetails()->get,
					'shipping_amount' => $result['payment']->getTransactions()[0]->getAmount()->getDetails()->getShipping(),
					'tax_amount' => $result['payment']->getTransactions()[0]->getAmount()->getDetails()->getTax(),
					//'request' => '',
					'response' => json_encode($result['payment']),
					'status' => 'approved',
				);
				$order_payment = new Application_Model_OrderPayment($order_payment_data);
				$order_payment_mapper->save($order_payment);

				$billing['email'] = $result['payment']->getPayer()->getPayerInfo()->getEmail();
				$billing['firstname'] = $result['payment']->getPayer()->getPayerInfo()->getFirstName();
				$billing['lastname'] = $result['payment']->getPayer()->getPayerInfo()->getLastName();

				$checkout->billing = $this->_checkout->billing = $billing;

				$this->processOrder($order_id, "Paypal - " . $result['payment']->getPayer()->getPayerInfo()->getPayerId(), $checkout, $cart, $customer);
			}else {
				$this->_checkout->error[] = "The payment has been denied by Paypal";

				/*
				if ($response['L_ERRORCODE0'] == 10416) {
					unset($_SESSION['payer_id']);
					unset($_SESSION['token']);
				}
				*/

				$this->_redirect("/checkout");
			}
		} else {
			$this->_redirect("/checkout");
		}

	}

	public function amazonReturnAction()
	{
		$access_token = $this->getRequest()->getParam('access_token');
		if (is_null($access_token)) {
			// Throw a 404 error
			$this->getResponse()->setHttpResponseCode(404);
			return;
		}
		$_SESSION['access_token'] = $access_token;
		$this->getResponse()->setRedirect('/checkout?method=amazonpay&amz_redir='.$this->getRequest()->getParam('amz_reload','y'));
		return;
	}

	public function amazonGetAddressAction()
	{
		$amazon_pay = new Application_Model_AmazonPay();
		$response = $amazon_pay->getAddress($this->getRequest()->getParam('amazonOrderReferenceId',''), $_SESSION['access_token']);

		echo $response->toJson();

		exit;
	}

	public function amzWorkaroundAction()
	{
		$this->_amz_workaround = new Zend_Session_Namespace('amz_workaround');

		$this->_amz_workaround->billing = $this->getRequest()->getParam('billing', $this->_billing);
		$this->_amz_workaround->shipping = $this->getRequest()->getParam('shipping', $this->_shipping);
		$this->_amz_workaround->payment = $this->getRequest()->getParam('payment', $this->_payment);

		echo "done";
		exit;
	}

    private function processOrder($order_id, $identifier, $_checkout, $_cart, $_customer)
    {
        $this->view->payment = $payment = $_checkout->payment;
        $this->view->billing = $billing = $_checkout->billing;
        $this->view->shipping = $shipping = $_checkout->shipping;
        $customer_id = (is_object($_customer) && $_customer->id)?$_customer->id:0;
        $this->view->totals = $totals = $_checkout->totals;
        $this->view->products = $products = $_checkout->products;
        $this->view->payment_identifier = $identifier;
	    $this->view->profile = $profile = $this->_checkout->profile;

        $order_profile_mapper = new Application_Model_Mapper_OrderProfile();
        $order_profile_data = array(
            'order_id' => $order_id,
            'promo_code' => ($_cart->promo_code)?$_cart->promo_code:$_cart->promo_id,
            //'affiliate_code' => (isset($_SESSION['affiliate']))?$_SESSION['affiliate']:'',
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'remote_addr' => $_SERVER['REMOTE_ADDR'],
            'site' => 'Main'
        );
        //custom items
	    foreach ($this->_custom_items as $k => $custom_item) {
		    $order_profile_data['custom_item_'.$k] = $custom_item['label'];
		    $order_profile_data['custom_item_'.$k.'_value'] = $profile['custom_item_'.$k.'_value'];
	    }

        $order_profile = new Application_Model_OrderProfile($order_profile_data);
        $order_profile_mapper->save($order_profile);

        //determine shipping class
        if ($shipping['country'] == 'CA') {
            $shipping_class = "First Class Mail International";
        }elseif($shipping['country'] == 'US') {
            if ($payment['shipping_method'] == 'Standard') {
                $shipping_class = "FIRST CLASS";
            }elseif ($payment['shipping_method'] == 'Expedited') {
                $shipping_class = "PRIORITY MAIL";
            }
        }
        $this->view->shipping_service = 'USPS';
        $this->view->shipping_class = $shipping_class;

        $order_shipping_mapper = new Application_Model_Mapper_OrderShipping();
        $order_shipping_data = array(
            'order_id' => $order_id,
            'code' => '',
            //'service' => $this->_shipping_services[$payment['shipping_method']]['service'],
            'service' => 'USPS',
            //'class' => $this->_shipping_services[$payment['shipping_method']]['class'],
            'class' => $shipping_class,
            'method' => $payment['shipping_method'],
            'tracking_code' => ''
        );
        $order_shipping = new Application_Model_OrderShipping($order_shipping_data);
        $order_shipping_mapper->save($order_shipping);

	    $variant_type_mapper = new Application_Model_Mapper_ProductVariantType();
        $order_product_mapper = new Application_Model_Mapper_OrderItem();
        foreach ($products as $k => $product) {
	        $variant_type =  isset($product['variant']->product_variant_type_id)?$variant_type_mapper->find($product['variant']->product_variant_type_id):null;
	        if (is_object($variant_type)) $variant_type = $variant_type->title;
            $warehouse_sku = isset($product['variant'])?$product['variant']->sku:$product['product']->sku;

            if (isset($product['license']) && $product['license']) {
	            $variant_type .= ' '.$product['license'];
            }

            $order_item_data = array(
                'order_id' => $order_id,
                'product_id' => $product['product']->id,
	            'variant_id' => isset($product['variant'])?$product['variant']->id:null,
                'sku' => isset($product['variant'])?$product['variant']->sku:$product['product']->sku,
                'warehouse_sku' => $warehouse_sku,
                'warehouse' => isset($product['variant'])?$product['variant']->warehouse:$product['product']->warehouse,
                'title' => isset($product['variant'])?$product['variant']->title:$product['product']->title,
                'type' => ($variant_type)?$variant_type:null,
                'qty' => $product['qty'],
                'base_price' => $product['base_price'],
                'paid_price' => $product['price'],
                'line_total' => $product['price'] * $product['qty'],
                'tax_amount' => (isset($_checkout->tax_details))?number_format(($product['price'] * $product['qty']) * $_checkout->tax_details->taxSales,2):null,
                'state_tax' => (isset($_checkout->tax_details))?number_format(($product['price'] * $product['qty']) * $_checkout->tax_details->stateSalesTax,2):null,
                'county_tax' => (isset($_checkout->tax_details))?number_format(($product['price'] * $product['qty']) * $_checkout->tax_details->countySalesTax,2):null,
                'city_tax' => (isset($_checkout->tax_details))?number_format(($product['price'] * $product['qty']) * $_checkout->tax_details->citySalesTax,2):null,
                'district_tax' => (isset($_checkout->tax_details))?number_format(($product['price'] * $product['qty']) * $_checkout->tax_details->districtSalesTax,2):null,
                'status' => ($totals['total'] > 1000)?'Hold':'Order Taken'
            );

            $order_product = new Application_Model_OrderItem($order_item_data);
            $oi_id = $order_product_mapper->save($order_product);

	        //data for NetSuite
	        $products[$k]['order_item_id'] = $oi_id;
        }

        //customer info
        $mapper = new Application_Model_Mapper_OrderAddress();
        $customer_address_data = array(
            'order_id' => $order_id,
            'type' => 'billing',
            'first_name' => $billing['first_name'],
            'last_name' => $billing['last_name'],
            //'company' => $billing['company'],
            'address' => $billing['address'],
            'address2' => $billing['address2'],
            'city' => $billing['city'],
            'region' => $billing['region'],
            'postcode' => $billing['postcode'],
            'country' => $billing['country'],
            'phone' => $billing['phone']
        );
        $customer_address_info = new Application_Model_OrderAddress($customer_address_data);
        $mapper->save($customer_address_info);

        $customer_address_data = array(
            'order_id' => $order_id,
            'type' => 'shipping',
            'first_name' => $shipping['first_name'],
            'last_name' => $shipping['last_name'],
            //'company' => $shipping['company'],
            'address' => $shipping['address'],
            'address2' => $shipping['address2'],
            'city' => $shipping['city'],
            'county' => (isset($shipping['county']))?$shipping['county']:'',
            'region' => $shipping['region'],
            'postcode' => $shipping['postcode'],
            'country' => $shipping['country'],
            'phone' => $shipping['phone']
        );
        $customer_address_info = new Application_Model_OrderAddress($customer_address_data);
        $mapper->save($customer_address_info);

        $order_total_mapper = new Application_Model_Mapper_OrderTotal();
        //sub total
        $order_total_data = array(
            'order_id' => $order_id,
            'code' => 'subtotal',
            'title' => 'Sub-Total',
            'amount' => (isset($totals['sub_total']))?$totals['sub_total']:0,
            'sort_order' => 1
        );
        $order_total = new Application_Model_OrderTotal($order_total_data);
        $order_total_mapper->save($order_total);
        //tax
        $order_total_data = array(
            'order_id' => $order_id,
            'code' => 'tax',
            'title' => 'Tax',
            'amount' => (isset($totals['tax']))?$totals['tax']:0,
            'sort_order' => 4
        );
        $order_total = new Application_Model_OrderTotal($order_total_data);
        $order_total_mapper->save($order_total);
        //shipping
        $order_total_data = array(
            'order_id' => $order_id,
            'code' => 'shipping',
            //'title' => 'Shipping ['.$this->_shipping_services[$payment['shipping_method']]['service'].']',
            'title' => 'Shipping ['.$payment['shipping_method'].']',
            'amount' => (isset($totals['shipping']))?$totals['shipping']:0,
            'sort_order' => 3
        );
        $order_total = new Application_Model_OrderTotal($order_total_data);
        $order_total_mapper->save($order_total);
        //promo
        $order_total_data = array(
            'order_id' => $order_id,
            'code' => 'discount',
            'title' => 'Discount',
            'amount' => (isset($totals['discount']))?$totals['discount']:0,
            'sort_order' => 2
        );
        $order_total = new Application_Model_OrderTotal($order_total_data);
        $order_total_mapper->save($order_total);
        //grand total
        $order_total_data = array(
            'order_id' => $order_id,
            'code' => 'total',
            'title' => 'Total',
            'amount' => (isset($totals['total']))?$totals['total']:0,
            'sort_order' => 5
        );
        $order_total = new Application_Model_OrderTotal($order_total_data);
        $order_total_mapper->save($order_total);

        //mark promo code used (if single use)
        if ($_cart->promo_code) {
            $mapper = new Application_Model_Mapper_PromoSingleCode();
            $mapper->markUsed($_cart->promo_code);
        }

        if (is_object($_customer) && $_customer->id) {
            //$this->_db->delete($this->_table_prefix."customer_cart","customer_id={$_customer->id}");

            //check if they have a default shipping and billing address
            $customer_address_mapper = new Application_Model_Mapper_CustomerAddress();
            $customer_billing = $customer_address_mapper->findByCustomerId($_customer->id, 'billing', 'Yes');
            $customer_shipping = $customer_address_mapper->findByCustomerId($_customer->id, 'shipping', 'Yes');

            if (!$customer_billing->id) {
                $customer_address_mapper->save(new Application_Model_CustomerAddress(
                    array(
                        'customer_id' => $_customer->id,
                        'address_type' => 'billing',
                        'first_name' => $billing['first_name'],
                        'last_name' => $billing['last_name'],
                        //'company' => $billing['company'],
                        'address' => $billing['address'],
                        'address2' => $billing['address2'],
                        'city' => $billing['city'],
                        'region' => $billing['region'],
                        'postcode' => $billing['postcode'],
                        'country' => $billing['country'],
                        'phone_day' => $billing['phone'],
                        'default_address' => 'Yes'
                    )
                ));
            }
            if (!$customer_shipping->id) {
                $customer_address_mapper->save(new Application_Model_CustomerAddress(
                    array(
                        'customer_id' => $_customer->id,
                        'address_type' => 'shipping',
                        'first_name' => $shipping['first_name'],
                        'last_name' => $shipping['last_name'],
                        //'company' => $shipping['company'],
                        'address' => $shipping['address'],
                        'address2' => $shipping['address2'],
                        'city' => $shipping['city'],
                        'region' => $shipping['region'],
                        'postcode' => $shipping['postcode'],
                        'country' => $shipping['country'],
                        'phone_day' => $shipping['phone'],
                        'default_address' => 'Yes'
                    )
                ));
            }
        }

        $this->_cart->setExpirationHops(1);
        $this->_cart->setExpirationSeconds(1);
        $this->_checkout->setExpirationHops(1);
        $this->_checkout->setExpirationSeconds(1);

        //Cny_Commerce_Cart::removeAll();

        $this->_order_info = new Zend_Session_Namespace('order_info');
        $this->_order_info->order_id = $order_id;

        //unset($_SESSION['payer_id']);
        //unset($_SESSION['token']);

        $this->_redirect("/checkout/confirmation");
    }

    public function confirmationAction()
    {
        $this->view->placeholder('body_class')->append('checkout-page confirmation-page');

        $this->_order_info = new Zend_Session_Namespace('order_info');
        $this->view->order_id = $order_id = $this->_order_info->order_id;
        $this->_order_info->setExpirationHops(1);

        //save order ID in session for a while in case they decide to register as a customer
        $this->view->register_link = false;
        if (!is_object($this->_customer) || !$this->_customer->id) {
            $this->_last_order = new Zend_Session_Namespace('last_order');
            $this->_last_order->order_id = $order_id;
            $this->_last_order->setExpirationSeconds(1800);
            $this->view->register_link = true;
        }

        if ($order_id) {
            $mapper = new Application_Model_Mapper_Order();
            $this->view->order = $mapper->find($order_id);

            $mapper = new Application_Model_Mapper_OrderAddress();
            $this->view->billing_address = $mapper->findByOrderId($order_id, 'billing');
            $this->view->shipping_address = $mapper->findByOrderId($order_id, 'shipping');

            $mapper = new Application_Model_Mapper_OrderItem();
            $this->view->products = $mapper->findByOrderId($order_id);

            $mapper = new Application_Model_Mapper_OrderTotal();
            $this->view->totals = $mapper->findByOrderId($order_id);

            $mapper = new Application_Model_Mapper_OrderPayment();
            $this->view->payment = $mapper->findByOrderId($order_id);

            $mapper = new Application_Model_Mapper_OrderShipping();
            $this->view->shipping = $mapper->findByOrderId($order_id);

	        $mapper = new Application_Model_Mapper_OrderProfile();
	        $this->view->order_profile = $mapper->findByOrderId($order_id);

	        //send email confirmation
	        if ($this->view->order->confirmation_sent == 'No') {
		        try {
			        $emails = new Application_Model_Emails();
			        $emails->orderConfirmation($order_id);

			        $mapper = new Application_Model_Mapper_Order();
			        $order = $mapper->find($order_id);
			        $order->confirmation_sent = 'Yes';
			        $mapper->save($order);
		        } catch (Exception $ex) {

		        }
	        }

        }else {
            $this->_redirect("/cart");
        }
    }

    public function shippingOptionsAction()
    {
        $this->view->layout()->disableLayout();

        $shipping_model = new Application_Model_Shipping();

        $region = $this->getRequest()->getParam("region","");
        $country = $this->getRequest()->getParam("country","");
        $postcode = $this->getRequest()->getParam("postcode","");

        if (!$country && $postcode) {
            if (preg_match('/^([a-zA-Z]\d[a-zA-Z])\ {0,1}(\d[a-zA-Z]\d)$/',$postcode)) {
                $country = "CA";
            }else {
                $country = "US";
            }
        }

        //if no region (state) we need to determine based on postcode
	    if (!$region) {
        	/*
		    $ziptax = new Cny_Model_ZipTax();
		    $tax_response = $ziptax->taxRate(array(
			    'address' => '',
			    'city' => '',
			    'region' => '',
			    'postcode' => $postcode,
			    'country' => $country
		    ));

		    $region = $tax_response->geoState;
		    */
	    }

	    $address = array(
		    'address1' => $this->getRequest()->getParam("address1",""),
		    'city' => $this->getRequest()->getParam("city",""),
		    'region' => $region,
		    'postcode' => $postcode,
		    'country' => $country,
	    );

	    $products = $this->_checkout->products;
	    if ($this->getRequest()->getParam('s','') == 'cart') {
	    	$products = $this->_cart->items;
	    }

	    echo json_encode($shipping_model->getShippingOptions($address,$products));

        exit;
    }

    public function getTaxAction()
    {
        $this->view->layout()->disableLayout();

        $address = $this->getRequest()->getParam("address","");
        $city = $this->getRequest()->getParam("city","");
        $region = $this->getRequest()->getParam("region","");
        $postcode = $this->getRequest()->getParam("postcode","");
        $country = $this->getRequest()->getParam("country","");
        $sub_total = $this->getRequest()->getParam("sub_total","");

        $tax = 0.00;

        if (array_key_exists($region,$this->_taxStates) && $region && $postcode) {
        	/*
            $ziptax = new Cny_Model_ZipTax();
            $tax_response = $ziptax->taxRate(array(
                'address' => $address,
                'city' => $city,
                'region' => $region,
                'postcode' => $postcode,
                'country' => $country
            ));

            if (isset($tax_response->taxSales)) {
                $tax = number_format(($sub_total * $tax_response->taxSales), 2);
           }
        	*/

	        $tax = number_format(($sub_total * $this->_taxStates[$region]/100), 2);
        }

        echo $tax;
        exit;
    }

	public function getStatesAction()
	{
		$this->view->layout()->disableLayout();

		$region = $this->getRequest()->getParam("region","");
		$country = $this->getRequest()->getParam("country","");

		$regions = Zend_Registry::get('regions')->toArray();
		if (array_key_exists(strtolower($country), $regions)) {
			$states = array(""=>"Select State/Province/Region") + $regions[strtolower($country)]['region'];

			echo json_encode($states);
		}else {
			echo "0";
		}

		exit;
	}

	public function promoRemoveAction()
	{
		$this->_cart->promo_code = '';

		$this->redirect("/checkout");
	}
}
