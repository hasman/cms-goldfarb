<?php

class VideoController extends Zend_Controller_Action
{

	function init()
	{
		$this->view->placeholder('section')->set("home");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->video_url = $video_url = ($settings_mapper->getValueByCode('video_url'))?$settings_mapper->getValueByCode('video_url'):'video';
		$video_status = ($settings_mapper->getValueByCode('video_status'))?$settings_mapper->getValueByCode('video_status'):'Enabled';
		if ($video_status != 'Enabled') {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}

		$this->_video_sort_order = ($settings_mapper->getValueByCode('video_sort_order'))?$settings_mapper->getValueByCode('video_sort_order'):'';

		if ($template_code = $settings_mapper->getValueByCode('video_template')) {
			$generator = new Cny_Model_HtmlBuild();
			$mapper = new Application_Model_Mapper_Template();
			$template = $mapper->findByCode($template_code);

			if ($template) {
				$mapper = new Application_Model_Mapper_TemplateBlock();
				$blocks = $mapper->findByTemplateId($template->id);

				foreach ($blocks as $position_code => $block) {
					$this->view->placeholder('template_position-'.$position_code)->append($generator->buildPage($block['block_id'], false, null, true));
					$this->view->placeholder('template_width-'.$position_code)->append($block['width']);
				}
			}
		}
	}

    public function indexAction()
    {
	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    if ($announcement_bar_status == 'enable') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

        $this->view->placeholder('body_class')->append('video_library-page');

	    $page = $this->getRequest()->getParam('page', 1);
	    $per_page = 6;

        $mapper = new Application_Model_Mapper_Video();
	    $posts = $mapper->fetchActive($this->_video_sort_order);

	    $paginator = Zend_Paginator::factory($posts);
	    $paginator->setCurrentPageNumber($page);
	    $paginator->setItemCountPerPage($per_page);
	    $this->view->videos = $paginator;

	    //$tag_mapper = new Application_Model_Mapper_VideoTag();
	    //$this->view->tags = $tag_mapper->fetchAllActive();
    }

	public function categoryAction()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		//announcement bar
		$announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
		if ($announcement_bar_status == 'enable') {
			$this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
		}

		$this->view->placeholder('body_class')->append('video_library-page');

		$page = $this->getRequest()->getParam('page', 1);
		$per_page = 6;

		$code = $this->getRequest()->getParam('code','');

		$mapper = new Application_Model_Mapper_VideoCategory();
		if ($code) {
			$this->view->category = $category = $mapper->doesExists(array('code'=>$code));
		}

		if ($category && $category->status == 'Enabled') {
			//$mapper = new Application_Model_Mapper_Video();
			$posts = $mapper->fetchVideosByCategoryId($category->id, $this->_video_sort_order);

			$paginator = Zend_Paginator::factory($posts);
			$paginator->setCurrentPageNumber($page);
			$paginator->setItemCountPerPage($per_page);
			$this->view->videos = $paginator;

		}else {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}
	}

    public function viewAction()
    {
	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    if ($announcement_bar_status == 'enable') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

        $this->view->doctype('XHTML1_RDFA');

        $id = $this->getRequest()->getParam('id');
        if (!is_null($id)) {
            $service = new Application_Model_Mapper_Video();
            $video = $service->find($id);
            if ($video) {
                $this->view->placeholder('body_class')->append('video-page video-detail-page');
                $this->view->video = $video;
                $this->view->headTitle()->prepend($video->title);

                $mapper = new Application_Model_Mapper_VideoTag();
                $this->view->tags = $mapper->fetchByVideoId($video->id);
            } else {
	            throw new Zend_Controller_Action_Exception('This page does not exist', 404);
            }
        } else {
	        throw new Zend_Controller_Action_Exception('This page does not exist', 404);
        }
    }

    public function tagAction()
    {
	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    $announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
	    if ($announcement_bar_status == 'enable' && $announcement_bar_page == '') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

	    //promotional pop-up
	    $promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
	    $promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
	    if ($promo_popup_status == 'Enabled' && ($promo_popup_page == '')) {
		    $promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
		    $promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
		    $promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
		    $extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

		    $this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$promo_popup_modal_code.'", "'.$promo_popup_delay.'", "'.$promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
	    }

	    $this->view->placeholder('body_class')->append('video-page video-tag-page');

	    $page = $this->getRequest()->getParam('page', 1);
	    $row_count = $this->getRequest()->getParam('rowcount', 6);
	    $code = $this->getRequest()->getParam('tag','');

	    $this->view->headTitle()->set("Video Tag: $code");

        $mapper = new Application_Model_Mapper_VideoTags();
        $this->view->tag = $tag = $mapper->findByCode($code);

        if ($tag) {
            $mapper = new Application_Model_Mapper_VideoTag();
            $posts = $mapper->fetchVideosByTagId($tag->id, $this->_video_sort_order);

            $paginator = Zend_Paginator::factory($posts);
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($row_count);
            $this->view->videos = $paginator;
        }else {
	        throw new Zend_Controller_Action_Exception('This page does not exist', 404);
        }
    }

}

