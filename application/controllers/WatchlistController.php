<?php

class WatchlistController extends Zend_Controller_Action
{

	function init()
	{
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->view->layout()->disableLayout();
		}

		$bootstrap = $this->getInvokeArg('bootstrap');
		$resource = $bootstrap->getPluginResource('multidb');
		$this->_db = $resource->getDefaultDb();
		//$this->_db = $resource->getDb('kino');

		$options = Zend_Registry::get('configuration')->toArray();

		$this->view->placeholder('body_class')->append('account-page');

		$this->_db = Zend_Db_Table::getDefaultAdapter();
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';


		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
		if( $auth->hasIdentity() ){
			$this->_customer = $auth->getIdentity();
		}

		$this->_cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array('automatic_serialization' => true, 'lifetime' => 0);
		$this->_backendOptions = array('cache_dir' => $this->_cache_dir);
	}

	public function restrictedAction()
	{
		//
	}

	public function addAction()
	{
		$redirect = $this->getRequest()->getParam("redir","/account/watchlist");

		$output = [];
		$id = $this->_getParam("id",$this->_getParam("id",''));

		if (isset($this->_customer) && $this->_customer->id) {
			$mapper = new Application_Model_Mapper_CustomerWatchlist();
			if ($id) {
				$mapper->addVideo($this->_customer->id, $id);
			}
		}

		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('customer_' . $this->_customer->id)
		);

		if ($this->getRequest()->isXmlHttpRequest()) {
			$output['action'] = 'add';
			$output['item']['video_id'] =  $id;

			if (!isset($this->_customer) || !$this->_customer->id) {
				$output['action'] = 'error';
				$output['message'] = 'You must be logged into your account to add an item to your watchlist.<br/><br/>
				<div class="text-center">
					<a href="/login" class="btn btn--red">Login</a>
					<a href="/register" class="btn btn--red">Create Account</a>
					<br/><br/>
					<button type="button" class="btn btn-sm btn--gray" onclick="$(\'.alert-message\').remove();">Maybe Later</button>
				</div>';
			}

			echo json_encode($output);
		} else {
			$this->_redirect(urldecode($redirect));
		}
		exit;
	}

	public function removeAction()
	{
		$redirect = $this->getRequest()->getParam("redir","/account/watchlist");

		$output = [];
		$id = $this->_getParam("id",'');

		$mapper = new Application_Model_Mapper_CustomerWatchlist();
		if( $id && isset($this->_customer) && $this->_customer->id ){
			$mapper->removeVideo($this->_customer->id, $id);
		}

		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);
		$cache->clean(
			Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
			array('customer_' . $this->_customer->id)
		);

		if ($this->getRequest()->isXmlHttpRequest()) {
			$output['action'] = 'remove';
			$output['item']['video_id'] =  $id;

			if (!isset($this->_customer) || !$this->_customer->id) {
				$output['action'] = 'error';
				$output['message'] = 'You must be logged into your account to remove an item from your watchlist.';
			}

			echo json_encode($output);
		} else {
			$this->_redirect(urldecode($redirect));
		}
		exit;
	}
}
