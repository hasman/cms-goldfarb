<?php

class LvpGenreController extends Zend_Controller_Action
{

	function init()
	{
		$this->view->placeholder('section')->set("lvp");
		$this->view->placeholder('body_class')->append('lvp-page');

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->_full_domain = (isset($options['site']['full_secure']))?$options['site']['full_secure']:null;
		if (isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
			$this->_full_domain = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
		}

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->base_video_url = $base_video_url = ($settings_mapper->getValueByCode('video_platform_url'))?$settings_mapper->getValueByCode('video_platform_url'):'videos';
		$video_status = ($settings_mapper->getValueByCode('video_platform_status'))?$settings_mapper->getValueByCode('video_platform_status'):'Enabled';
		if ($video_status != 'Enabled') {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}

		$this->view->standard_attributes = $this->_standard_attributes = $options['lvp']['standard_attributes'];

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
		if( !$auth->hasIdentity() ){
			$this->view->customer = $this->_customer = null;
		}else {
			$this->view->customer = $this->_customer = $auth->getIdentity();
		}

		$this->_video_sort_order = 'a-z';
	}

	public function viewAction()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$lvp_status = ($settings_mapper->getValueByCode('video_platform_status'))?$settings_mapper->getValueByCode('video_platform_status'):'Disabled';
		$this->view->lvp_base_url = $lvp_base_url = ($settings_mapper->getValueByCode('video_platform_url'))?$settings_mapper->getValueByCode('video_platform_url'):'videos';

		if ($lvp_status == 'Enabled') {

			//announcement bar
			$announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
			if ($announcement_bar_status == 'enable') {
				$this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
			}

			//$this->view->placeholder('body_class')->append('video_library-page');
			$this->view->placeholder('body_class')->append('lvp-category-page');

			$page = $this->getRequest()->getParam('page', 1);
			$per_page = $settings_mapper->getValueByCode('lvp_cat_grid_item_page_count');

			$code = $this->getRequest()->getParam('code', '');

			$mapper = new Application_Model_Mapper_VideoPlatformGenre();
			if ($code) {
				$this->view->genre = $genre = $mapper->doesExists(array('code' => $code));
			}

			if ($genre && $genre->status == 'Enabled') {
				$mapper = new Application_Model_Mapper_VideoPlatformVideoGenre();
				$posts = $mapper->fetchVideosByGenreId($genre->id, $this->_video_sort_order);

				$paginator = Zend_Paginator::factory($posts);
				$paginator->setCurrentPageNumber($page);
				$paginator->setItemCountPerPage($per_page);
				$this->view->videos = $paginator;

			} else {
				throw new Zend_Controller_Action_Exception('This page does not exist', 404);
			}
		}else {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}
	}
}

