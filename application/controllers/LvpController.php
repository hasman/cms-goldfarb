<?php
use Cocur\Slugify\Slugify;

class LvpController extends Zend_Controller_Action
{

	function init()
	{
		$this->view->placeholder('section')->set("lvp");
		$this->view->placeholder('body_class')->append('lvp-page');

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->_full_domain = (isset($options['site']['full_secure']))?$options['site']['full_secure']:null;
		if (isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
			$this->_full_domain = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
		}

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->base_video_url = $base_video_url = ($settings_mapper->getValueByCode('video_platform_url'))?$settings_mapper->getValueByCode('video_platform_url'):'videos';
		$video_status = ($settings_mapper->getValueByCode('video_platform_status'))?$settings_mapper->getValueByCode('video_platform_status'):'Enabled';
		if ($video_status != 'Enabled') {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}

		$this->view->standard_attributes = $this->_standard_attributes = $options['lvp']['standard_attributes'];

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
		if( !$auth->hasIdentity() ){
			$this->view->customer = $this->_customer = null;
		}else {
			$this->view->customer = $this->_customer = $auth->getIdentity();
		}

		//default poster
		$this->view->default_poster = $settings_mapper->getValueByCode('lvp_default_poster','');
	}

    public function viewAction()
    {
	    $lvp_post_purchase_redirect = new Zend_Session_Namespace('lvp_post_purchase_redirect');
	    $lvp_post_purchase_redirect->url = $_SERVER['REQUEST_URI'];


	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    $this->view->placeholder('body_class')->append('lvp-detail-page');

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    if ($announcement_bar_status == 'enable') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

        //$this->view->placeholder('body_class')->append('video_library-page');
	    $this->view->doctype('XHTML1_RDFA');

	    $code = $this->getRequest()->getParam('code');
	    if (!is_null($code)) {
		    $mapper = new Application_Model_Mapper_VideoPlatformVideo();
		    $this->view->video = $video = $mapper->findByCode($code);

		    if ($video) {
			    $s3 = new Application_Model_AmazonS3();

			    //TODO - currently only custom thumbnail exists
			    $this->view->thumbnail_url = $thumbnail_url = ($video->thumbnail_src)?$video->thumbnail_src:(($video->video_platform_video_thumbnail_id)?'':null);

			    $this->view->headTitle()->prepend($video->title);
			    $this->view->headMeta()->setProperty("og:title",$video->title);

			    if ($video->meta_keywords) {
				    $this->view->headMeta()->setName("keywords",$video->meta_keywords);
			    }
			    if ($video->meta_description) {
				    $this->view->headMeta()->setName("description",$video->meta_description);
				    $this->view->headMeta()->setProperty("og:description",$video->meta_description);
			    }

			    if ($thumbnail_url) {
				    $this->view->headMeta()->setProperty("og:image",$this->_full_domain.$thumbnail_url);
			    }
			    $this->view->headMeta()->setProperty("og:type","video");

			    //monetization settings
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoMonetization();
			    $this->view->monetization = $monetization = $mapper->fetchByVideoId($video->id);

			    //image gallery
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoImage();
			    $this->view->images = $mapper->fetchByVideoId($video->id);

			    //cast & crew
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoCast();
			    $this->view->directors = $mapper->fetchByVideoId($video->id, 'director');
			    $this->view->cast = $mapper->fetchByVideoId($video->id, 'cast');
			    $this->view->crew = $mapper->fetchByVideoId($video->id, 'crew');

			    //News & Reviews
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoNews();
			    $this->view->news = $mapper->fetchByVideoId($video->id, 'news');
			    $this->view->reviews = $mapper->fetchByVideoId($video->id, 'reviews');

			    //profile/attributes
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoProfile();
			    $this->view->profile = $mapper->getProfileByVideoId($video->id);

			    //geo blocking
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoGeoblock();
			    $this->view->geo_blocked = $geo_blocked = $mapper->checkVideoGeoBlock($video->id);

			    //check for entitlement
			    $entitled = false;
			    if ($this->_customer) {
				    $mapper = new Application_Model_Mapper_VideoPlatformEntitlement();
				    $entitled = $mapper->checkEntitlement($this->_customer->id, $video->id);

				    if ($entitled) {
					    $this->view->entitlement = $mapper->fetchEntitlement($this->_customer->id, $video->id);
				    }
			    }
			    $this->view->entitled = $entitled;

			    if ( (((isset($monetization->purchase_required) && $monetization->purchase_required == 'Yes') ||
					    (isset($monetization->rental_required) && $monetization->rental_required == 'Yes'))
				    && !$entitled) || $geo_blocked
			    ) {
				    //trailer
				    $mapper = new Application_Model_Mapper_VideoPlatformVideoTrailer();
				    $this->view->trailer = $trailer = $mapper->fetchByVideoId($video->id);

				    if ($trailer) {
					    //get streams (transcoded files)
					    $mapper = new Application_Model_Mapper_VideoPlatformVideoTrailerStream();
					    $streams = $mapper->fetchByTrailerId($trailer->id);
				    }

				    if ($streams) {
					    //how do we know which stream we want to use? For now hard coding a priority list
					    foreach ($streams as $stream) {
						    if ($stream->format_name == 'HLS') {
							    $this->view->video_url = $s3->getVideoTrailerStreamURL($stream->id, $trailer->id);
						    }
					    }
				    }

				    //fallback to source file
				    if (!$this->view->video_url && $trailer) {
					    $this->view->video_url = $s3->getVideoTrailerURL($trailer->id);
				    }
			    } else {
				    //temp - replace with HLS manifest
				    //$this->view->video_url = $s3->getVideoURL($video->id);

				    //get streams (transcoded files)
				    $mapper = new Application_Model_Mapper_VideoPlatformVideoStream();
				    $streams = $mapper->fetchByVideoId($video->id);

				    if ($streams) {
					    //how do we know which stream we want to use? For now hard coding a priority list
					    foreach ($streams as $stream) {
						    if ($stream->format_name == 'HLS') {
							    $this->view->video_url = $s3->getVideoStreamURL($stream->id, $video->id);
						    }
					    }
				    }

				    $mapper = new Application_Model_Mapper_VideoPlatformVideoTrack();
				    $this->view->tracks = $mapper->fetchByVideoId($video->id);

				    //extras / bonus features
				    $mapper = new Application_Model_Mapper_VideoPlatformVideoExtra();
				    $this->view->extras = $mapper->fetchByVideoId($video->id);
			    }

			    //monetization links
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoMonetizationLink();
			    $this->view->monetization_links = $mapper->fetchByVideoId($video->id);

			    //create hash and save to analytics table
			    $this->view->analytics_hash = $analytics_hash = md5(uniqid((isset($this->_customer->id))?$this->_customer->id:rand(), true));
				$mapper = new Application_Model_Mapper_VideoPlatformVideoAnalytic();
			    $mapper->save(new Application_Model_VideoPlatformVideoAnalytic(array(
				    'customer_id' => (isset($this->_customer->id))?$this->_customer->id:0,
				    'hash' => $analytics_hash,
				    'video_platform_video_id' => $video->id,
				    'played' => 'No',
				    'seconds_played' => 0,
			    )));

			    //watchlist check
			    $mapper = new Application_Model_Mapper_CustomerWatchlist();
			    $watchlist = $mapper->findByCustomerId($this->_customer->id);
			    $watchlist = ($watchlist)?json_decode($watchlist->list):array();
			    $this->view->in_watchlist = (in_array($video->id, $watchlist))?true:false;
		    }else {
			    throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		    }
	    }else {
		    throw new Zend_Controller_Action_Exception('This page does not exist', 404);
	    }
    }

	public function extraModalAction()
	{
		//$this->view->placeholder('body_class')->append('lvp-detail-page');

		$id = $this->getRequest()->getParam('id');
		if (!is_null($id)) {
			$mapper = new Application_Model_Mapper_VideoPlatformVideoExtra();
			$this->view->video = $video = $mapper->find($id);

			if ($video) {
				$s3 = new Application_Model_AmazonS3();

				//TODO - currently only custom thumbnail exists
				$this->view->thumbnail_url = $thumbnail_url = ($video->thumbnail_src)?$video->thumbnail_src:(($video->video_platform_video_thumbnail_id)?'':null);

				//get streams (transcoded files)
				$mapper = new Application_Model_Mapper_VideoPlatformVideoExtrasStream();
				$streams = $mapper->fetchByExtrasId($video->id);

				if ($streams) {
					//how do we know which stream we want to use? For now hard coding a priority list
					foreach ($streams as $stream) {
						if ($stream->format_name == 'HLS') {
							$this->view->video_url = $s3->getVideoExtrasStreamURL($stream->id, $video->id);
						}
					}
				}
			}else {
				throw new Zend_Controller_Action_Exception('This page does not exist', 404);
			}
		}else {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}
	}

    public function embedAction()
    {
    	$this->view->layout()->setLayout('video-embed');

	    $lvp_post_purchase_redirect = new Zend_Session_Namespace('lvp_post_purchase_redirect');
	    $lvp_post_purchase_redirect->url = $_SERVER['REQUEST_URI'];

	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    $hash = $this->getRequest()->getParam('hash', '');

	    if (!is_null($hash)) {
		    $mapper = new Application_Model_Mapper_VideoPlatformVideo();
		    $this->view->video = $video = $mapper->findByVideoHash($hash);

		    if ($video) {
			    $s3 = new Application_Model_AmazonS3();

			    //TODO - currently only custom thumbnail exists
			    $this->view->thumbnail_url = $thumbnail_url = ($video->thumbnail_src)?$video->thumbnail_src:(($video->video_platform_video_thumbnail_id)?'':null);

			    //monetization settings
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoMonetization();
			    $this->view->monetization = $monetization = $mapper->fetchByVideoId($video->id);

			    //geo blocking
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoGeoblock();
			    $this->view->geo_blocked = $geo_blocked = $mapper->checkVideoGeoBlock($video->id);

			    //check for entitlement
			    $entitled = false;
			    if ($this->_customer) {
				    $mapper = new Application_Model_Mapper_VideoPlatformEntitlement();
				    $entitled = $mapper->checkEntitlement($this->_customer->id, $video->id);

				    if ($entitled) {
					    $this->view->entitlement = $mapper->fetchEntitlement($this->_customer->id, $video->id);
				    }
			    }
			    $this->view->entitled = $entitled;

			    if ( (((isset($monetization->purchase_required) && $monetization->purchase_required == 'Yes') ||
						    (isset($monetization->rental_required) && $monetization->rental_required == 'Yes'))
					    && !$entitled) || $geo_blocked
			    ) {


			    	/*
				    //trailer
				    $mapper = new Application_Model_Mapper_VideoPlatformVideoTrailer();
				    $this->view->trailer = $trailer = $mapper->fetchByVideoId($video->id);

				    if ($trailer) {
					    //get streams (transcoded files)
					    $mapper = new Application_Model_Mapper_VideoPlatformVideoTrailerStream();
					    $streams = $mapper->fetchByTrailerId($trailer->id);
				    }

				    if ($streams) {
					    //how do we know which stream we want to use? For now hard coding a priority list
					    foreach ($streams as $stream) {
						    if ($stream->format_name == 'HLS') {
							    $this->view->video_url = $s3->getVideoTrailerStreamURL($stream->id, $trailer->id);
						    }
					    }
				    }

				    //fallback to source file
				    if (!$this->view->video_url && $trailer) {
					    $this->view->video_url = $s3->getVideoTrailerURL($trailer->id);
				    }
				    */
			    } else {
				    //temp - replace with HLS manifest
				    //$this->view->video_url = $s3->getVideoURL($video->id);

				    //get streams (transcoded files)
				    $mapper = new Application_Model_Mapper_VideoPlatformVideoStream();
				    $streams = $mapper->fetchByVideoId($video->id);

				    if ($streams) {
					    //how do we know which stream we want to use? For now hard coding a priority list
					    foreach ($streams as $stream) {
						    if ($stream->format_name == 'HLS') {
							    $this->view->video_url = $s3->getVideoStreamURL($stream->id, $video->id);
						    }
					    }
				    }

				    $mapper = new Application_Model_Mapper_VideoPlatformVideoTrack();
				    $this->view->tracks = $mapper->fetchByVideoId($video->id);

				    //extras / bonus features
				    $mapper = new Application_Model_Mapper_VideoPlatformVideoExtra();
				    $this->view->extras = $mapper->fetchByVideoId($video->id);
			    }

			    //monetization links
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoMonetizationLink();
			    $this->view->monetization_links = $mapper->fetchByVideoId($video->id);

			    //create hash and save to analytics table
			    $this->view->analytics_hash = $analytics_hash = md5(uniqid((isset($this->_customer->id))?$this->_customer->id:rand(), true));
			    $mapper = new Application_Model_Mapper_VideoPlatformVideoAnalytic();
			    $mapper->save(new Application_Model_VideoPlatformVideoAnalytic(array(
				    'customer_id' => (isset($this->_customer->id))?$this->_customer->id:0,
				    'hash' => $analytics_hash,
				    'video_platform_video_id' => $video->id,
				    'played' => 'No',
				    'seconds_played' => 0,
			    )));
		    }else {
			    throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		    }
	    }else {
		    throw new Zend_Controller_Action_Exception('This page does not exist', 404);
	    }
    }

	public function analyticsAction()
	{
		$hash = $this->_getParam("hash",'');
		$played = $this->_getParam("play",'Yes');
		$seconds_plus = $this->_getParam("scplus",1);

			$mapper = new Application_Model_Mapper_VideoPlatformVideoAnalytic();
			if ($hash) {
				$mapper->updateAnalytic((isset($this->_customer->id))?$this->_customer->id:null, $hash, $played, $seconds_plus);
			}


		exit;
	}

	public function zypeAction()
	{
		$lvp_post_purchase_redirect = new Zend_Session_Namespace('lvp_post_purchase_redirect');
		$lvp_post_purchase_redirect->url = $_SERVER['REQUEST_URI'];


		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$slugify = new Slugify();

		$lvp_url = ($settings_mapper->getValueByCode('video_platform_url'))?$settings_mapper->getValueByCode('video_platform_url'):'videos';

		$this->view->placeholder('body_class')->append('lvp-detail-page');

		//announcement bar
		$announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
		if ($announcement_bar_status == 'enable') {
			$this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
		}

		//$this->view->placeholder('body_class')->append('video_library-page');
		$this->view->doctype('XHTML1_RDFA');

		$zype = new Application_Model_Zype();
		$geocode = new Application_Model_GoogleGeocode();

		$zype_id = $this->getRequest()->getParam('zype_id', null);

		$this->view->video = $video = $zype->getVideo($zype_id);

		if (is_null($video) || $video->active != 1) {
			// 404 Film Not Found
			$this->getResponse()->setHttpResponseCode(404);
			$this->render('notfound');
			return;
		}

		//get customer data from Zype
		if (isset($this->_customer->zype) && $this->_customer->zype->expiration < time()) {
			$this->_customer->zype = $zype->refreshAccount();
		}

		//check entitlement
		$this->view->entitled = false;
		if ($video->purchase_required != '1' && $video->rental_required != '1' && $video->subscription_required != '1' && $video->registration_required != '1' ) {
			$this->view->entitled = true;
		}

		if ($this->_customer && isset($this->_customer->zype->_id) && $this->_customer->zype->_id) {
			$entitlement = $zype->checkVideoEntitlement($zype_id, $this->_customer->zype->access_token);

			if ($entitlement && isset($entitlement->message) && $entitlement->message == 'entitled') {
				$this->view->entitled = true;

				//BEGIN WORKAROUND - Zype doesn't give detail about the entitlement
				$purchase = false;
				$page = 1; $per_page = 100;
				$entitlement_details = (object)['expires_at'=>''];
				$response = $zype->getVideoEntitlements($this->_customer->zype_id, array('page'=>$page,'per_page'=>$per_page));
				if ($response && $response->response) {
					$all_entitlements = [];
					$all_entitlements = array_merge($all_entitlements, $response->response);
					for ($i = 2; $i <= $response->pagination->pages; $i++) {
						$response2 = $zype->getVideoEntitlements($this->_customer->zype_id, array('page'=>$i,'per_page'=>$per_page));
						$all_entitlements = array_merge($all_entitlements, $response2->response);
					}
					foreach ($all_entitlements as $entitlement) {
						if ($entitlement->video_id == $zype_id && $entitlement->transaction_type == 'purchase') {
							$purchase = true;
						}

						if ($entitlement->video_id == $zype_id && (is_null($entitlement->expires_at) || strtotime($entitlement->expires_at) > strtotime($entitlement_details->expires_at))) {
							$entitlement_details = $entitlement;
						}
					}
				}
				$this->view->entitlement = $entitlement_details;
				//END WORKAROUND
			}

			//watchlist check
			$mapper = new Application_Model_Mapper_CustomerWatchlist();
			$watchlist = $mapper->findByCustomerId($this->_customer->id);
			$watchlist = ($watchlist)?json_decode($watchlist->list):array();
			$this->view->in_watchlist = (in_array($video->_id, $watchlist))?true:false;
		}

		//check content rules (region restriction)
		//$geoip = $zype->geoIP($_SERVER['REMOTE_ADDR']);
		$this->view->region_restricted = $zype->testVideoContentRules($video->_id,(isset($geoip->response->country_code2))?$geoip->response->country_code2:null);

		//check if video has ad timings (free ad supported)
		$this->view->free_with_ads = false;
		$ads = $zype->listAdTimings($video->_id);
		if (isset($ads->response[0]->ad_timings)) {
			if (count($ads->response[0]->ad_timings) > 1) {
				$this->view->free_with_ads = true;
			}
		}

		//subscription option(s)
		// TODO - For now hard coding to first one but may want to redirect to a page in the future with multiple options
		$this->view->subscription_plans = array();
		$subscription_plans = $zype->listSubscriptionPlans();
		foreach ($subscription_plans as $plans) {
			$plan = current($plans);
			if (isset($plan->active) && $plan->active == '1') {
				$this->view->subscription_plans[] = $plan;
			}
		}

		$this->view->headTitle($video->title,"PREPEND");
		$this->view->headMeta()->setName('description', strip_tags($video->short_description));
		$this->view->doctype(Zend_View_Helper_Doctype::XHTML1_RDFA);
		$this->view->headMeta('summary_large_image', 'twitter:card', 'name', [], 'SET');
		$this->view->headMeta()->setProperty('og:title', $video->title);
		$this->view->headMeta()->setProperty('og:type', 'website');
		if ($video->short_description) {
			$this->view->headMeta()->setProperty('og:description', strip_tags($video->short_description));
		}
		// Zend_Debug::dump($this->view->film_images);die;

		$friendly_title = ($video->friendly_title)?$video->friendly_title:$slugify->slugify($video->title);

		$this->view->headMeta()->setProperty('og:url', 'https://'.$_SERVER['SERVER_NAME'].'/'.$lvp_url.'/'.$friendly_title.'/'.$video->_id);
		//$this->view->headMeta()->setName('GOOGLEBOT','NOARCHIVE');
		$this->view->headLink(array('rel' => 'canonical', 'href' => 'https://'.$_SERVER['SERVER_NAME'].'/'.$lvp_url.'/'.$friendly_title.'/'.$video->_id),'APPEND');
	}
}

