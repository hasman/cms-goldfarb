<?php
use Cocur\Slugify\Slugify;

class IndexController extends Zend_Controller_Action
{
	var $_db;
	var $_full_domain;
	var $_invalid_mimetypes;
	var $_cache_dir;
	var $_cache_dir_temp;

	function init()
	{
		$this->view->placeholder('section')->set("home");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->_cache_dir = $options['dir']['cache'];
		$this->_cache_dir_temp = $this->_cache_dir.'/temp';

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$site_name = $settings_mapper->getValueByCode('site_name');

		$this->view->headTitle($site_name, "SET");

		$this->_full_domain = (isset($options['site']['full_secure']))?$options['site']['full_secure']:null;

		if (isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
			$this->_full_domain = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
		}

		$this->_invalid_mimetypes = array(
			'application/x-msdownload', //exe dll com bat msi
			'text/php', //php
			'text/x-php', //php
			'application/php', //php
			'application/x-php', //php
			'application/x-httpd-php', //php
			'application/x-httpd-php-source', //php
			'application/javascript', //js
		);

		$api_key = $settings_mapper->getValueByCode('google_api_key');
		if ($api_key) {
			$this->view->headScript()->appendFile("https://maps.googleapis.com/maps/api/js?v=3&key={$api_key}");
		}
	}

	public function indexAction()
	{
		$this->forward("page");
	}

	public function previewPageAction()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$generator = new Cny_Model_HtmlBuild();

		$id = $this->getRequest()->getParam('id',0);

		$mapper = new Application_Model_Mapper_Page();
		/** @var Application_Model_Page $page */
		$page = $mapper->find($id);

		$page_settings = json_decode($page->json_settings);

		$this->view->placeholder('body_id')->append('page-'.$page->id);
		$this->view->placeholder('body_class')->append($page->class);

		$this->view->doctype(Zend_View_Helper_Doctype::XHTML1_RDFA);
		if ($page->meta_title) {
			$this->view->headTitle()->set($page->meta_title);
			$this->view->headMeta()->setProperty('og:title', $page->meta_title);
		}else {
			$this->view->headTitle()->prepend($page->title);
			$this->view->headMeta()->setProperty('og:title', $page->title);
		}
		if ($page->meta_keywords) {
			$this->view->headMeta()->setName('keywords', $page->meta_keywords);
		}
		if ($page->meta_description) {
			$this->view->headMeta()->setName('description', $page->meta_description);
			$this->view->headMeta()->setProperty('og:description', $page->meta_description);
		}elseif ($meta_description = $settings_mapper->getValueByCode('meta_description')) {
			//$this->view->headMeta()->setName('description', $page->meta_description);
			$this->view->headMeta()->setProperty('og:description', $meta_description);
		}
		if (isset($page->meta_canonical_url) && $page->meta_canonical_url) {
			$this->view->headLink(array('rel' => 'canonical', 'href' => $page->meta_canonical_url));
			$this->view->headMeta()->setProperty('og:url', $page->meta_canonical_url);
		}
		$og_image_default = $settings_mapper->getValueByCode('open_graph_image');
		if (isset($page->open_graph_image) && $page->open_graph_image) {
			$this->view->headMeta()->setProperty('og:image', $this->_full_domain . $page->open_graph_image);
		}elseif ($og_image_default) {
			$this->view->headMeta()->setProperty('og:image', $this->_full_domain . $og_image_default);
		}

		$this->view->placeholder('_meta_head')->append($page->meta_head_custom);

		//Schema.org JSON-LD
		$json_ld = '';
		if ($page->schema_custom_json) {
			$json_ld = $page->schema_custom_json;
		}else {
			$json_ld = json_encode(array(
				"@context" => "http://schema.org",
				"@type" => ($page->schema_page_type)?$page->schema_page_type:"WebPage",
				"name" => ($page->meta_title)?$page->meta_title:$page->title,
				"url" => ($page->meta_canonical_url)?$page->meta_canonical_url:"{$this->_full_domain}/{$page->code}",
				"description" => ($page->meta_description)?$page->meta_description:""
			));
		}
		if ($json_ld) {
			$this->view->placeholder('_meta_head')->append('<script type="application/ld+json">'.$json_ld.'</script>');
		}

		$template_header = $template_footer = $template_footer_mobile = '';
		if ($page->template && $page->template != 'layout') {
			$mapper = new Application_Model_Mapper_Template();
			$template = $mapper->findByCode($page->template);

			if ($template) {
				$mapper = new Application_Model_Mapper_TemplateBlock();
				$blocks = $mapper->findByTemplateId($template->id);

				foreach ($blocks as $position_code => $block) {
					switch ($position_code) {
						case 'header':
							$template_header = $block;
							break;
						case 'footer':
							$template_footer = $block;
							break;
						case 'footer_mobile':
							$template_footer_mobile = $block;
							break;
						default:
							$tb_setting = json_decode($block['json_settings']);

							$this->view->placeholder('template_position-' . $position_code)->append($generator->buildPage($block['block_id'], false, $page->id, true));
							$this->view->placeholder('template_width-' . $position_code)->append(isset($tb_setting->desktop_width)?$tb_setting->desktop_width:null);
							break;
					}
				}
			}
		}

		if( $this->getRequest()->isPost() ){
			$module_id = $this->getRequest()->getParam('module_id',0);

			$this->formSubmit($module_id, $this->getRequest()->getPost(), $_FILES);

			$mapper = new Application_Model_Mapper_PageSectionRowColumnModule();
			$module = $mapper->find($module_id);

			$module_content = json_decode($module->json_content);
			if (isset($module_content->confirmation_type) && $module_content->confirmation_type == 'select_page' && isset($module_content->confirmation_page_code)) {
				$this->redirect("/{$module_content->confirmation_page_code}");
			}elseif (isset($module_content->confirmation_type) && $module_content->confirmation_type == 'url' && isset($module_content->confirmation_url) && $module_content->confirmation_url) {
				$this->redirect($module_content->confirmation_url);
			}else {
				$this->redirect("/index/preview-page/id/$id");
			}
		}

		$this->view->content = $generator->buildPage($id, false);

		$this->view->headLink()->appendStylesheet('/index/css/?v='.time().'&cache=false&page_id='.$id);

		//set body classes based on settings
		$header_type = $settings_mapper->getValueByCode('header_type');

		if ($header_type == '') {
			$this->view->placeholder('body_class')->append('header-fixed-space-default');
		}

		//navigation setup/options/etc.
		if ($page->nav_display == 'Enabled') {
			if (!$page->header_template || $page->header_template == 'default') {
				$header_type = $settings_mapper->getValueByCode('header_type');

				$page->header_template = $header_type;
			}

			//get nav header visiblity options
			$class = '';
			$settings = json_decode($page->json_settings);
			if (isset($settings->nav_desktop_status) && $settings->nav_desktop_status == 'Disabled') {
				$class .= ' hidden-lg';
			}
			if (isset($settings->nav_tablet_status) && $settings->nav_tablet_status == 'Disabled') {
				$class .= ' hidden-md hidden-sm';
			}
			if (isset($settings->nav_mobile_status) && $settings->nav_mobile_status == 'Disabled') {
				$class .= ' hidden-xs';
			}


			$this->view->placeholder('_header')->prepend('<div class="'.$class.'">');
			if ($template_header) {
				$this->view->placeholder('_header')->append($generator->buildCustomHeader($template_header['block_id']));
			}elseif ($page->header_template && $page->header_template == 'default') {
				$this->view->placeholder('_header')->append($generator->buildHeader());
			}elseif ($page->header_template && stripos($page->header_template,"block-") !== false) {
				$header_block_id = str_ireplace("block-","",$page->header_template);
				$this->view->placeholder('_header')->append($generator->buildCustomHeader($header_block_id));
			}else {
				$this->view->placeholder('_header')->append($generator->buildHeader($page->header_template));
			}
			$this->view->placeholder('_header')->append('</div>');
		}
		//footer
		if ($page->footer_display == 'Enabled') {
			//get footer visiblity options
			$class = '';
			$settings = json_decode($page->json_settings);
			if (isset($settings->footer_desktop_status) && $settings->footer_desktop_status == 'Disabled') {
				$class .= ' hidden-lg';
			}
			if (isset($settings->footer_tablet_status) && $settings->footer_tablet_status == 'Disabled') {
				$class .= ' hidden-md hidden-sm';
			}
			if (isset($settings->footer_mobile_status) && $settings->footer_mobile_status == 'Disabled') {
				$class .= ' hidden-xs';
			}

			$this->view->placeholder('_footer')->prepend('<div class="'.$class.'">');

			if ($template_footer || $template_footer_mobile) {
				if ($template_footer) {
					$this->view->placeholder('_footer')->append('<div class="hidden-xs">');
					$this->view->placeholder('_footer')->append($generator->buildPage($template_footer['block_id'], false, $page->id, true));
					$this->view->placeholder('_footer')->append('</div>');
				}
				if ($template_footer_mobile) {
					$this->view->placeholder('_footer')->append('<div class="visible-xs">');
					$this->view->placeholder('_footer')->append($generator->buildPage($template_footer_mobile['block_id'], false, $page->id, true));
					$this->view->placeholder('_footer')->append('</div>');
				}
			}else {
				$this->view->placeholder('_footer')->append($generator->buildFooter());
			}

			$this->view->placeholder('_footer')->append('</div>');
		}

		//announcement bar
		$announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
		$announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
		if ($announcement_bar_status == 'enable' && ($announcement_bar_page == '' || ($announcement_bar_page == 'home' && $page->code == $settings_mapper->getValueByCode('home_page')))) {
			$this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
		}

		//promotional pop-up
		$promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
		$promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
		if ($promo_popup_status == 'Enabled' && (!isset($page_settings->promo_popup_force_disable) || $page_settings->promo_popup_force_disable == 'Disabled')
			&& ($promo_popup_page == '' || ($promo_popup_page == 'home' && $page->code == $settings_mapper->getValueByCode('home_page')))) {

			$promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
			$promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
			$promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
			$extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

			$this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$promo_popup_modal_code.'", "'.$promo_popup_delay.'", "'.$promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
		}
		//page specific promotional pop-up
		if (isset($page_settings->promo_popup_status) && $page_settings->promo_popup_status == 'Enabled') {
			$extra_classes = $page_settings->promo_popup_size;

			$this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$page_settings->promo_popup_modal_code.'", "'.$page_settings->promo_popup_delay.'", "'.$page_settings->promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
		}

		//aggregated module styles
		$this->view->placeholder('_module_style_block')->prepend('<style>');
		$this->view->placeholder('_module_style_block')->append($generator->getModuleStyleBlock());
		$this->view->placeholder('_module_style_block')->append('</style>');

		$this->render('page');
	}

	public function pageAction()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$generator = new Cny_Model_HtmlBuild();
		$cache_status = true;

		$code = $this->getRequest()->getParam('code','');

		$mapper = new Application_Model_Mapper_Page();
		$ssv_mapper = new Application_Model_Mapper_SiteSettingValue();

		if (!$code) {
			$code = $ssv_mapper->getValueByCode('home_page');

			if (!$code || !$mapper->doesExists(['code'=>$code])) {
				$this->render("index");
				return;
			}
		}

		$page = $mapper->doesExists(['code'=>$code]);

		if (!$page || $page->status != 'Enabled') {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}

		$page_settings = json_decode($page->json_settings);

		if (isset($page_settings->page_cache) && $page_settings->page_cache == 'Disabled') {
			$cache_status = false;
		}

		$template_header = $template_footer = $template_footer_mobile = '';
		if ($page->template && $page->template != 'layout') {
			$mapper = new Application_Model_Mapper_Template();
			$template = $mapper->findByCode($page->template);

			if ($template) {
				$mapper = new Application_Model_Mapper_TemplateBlock();
				$blocks = $mapper->findByTemplateId($template->id);

				foreach ($blocks as $position_code => $block) {
					switch ($position_code) {
						case 'header':
							$template_header = $block;
							break;
						case 'footer':
							$template_footer = $block;
							break;
						case 'footer_mobile':
							$template_footer_mobile = $block;
							break;
						default:
							$tb_setting = json_decode($block['json_settings']);

							$this->view->placeholder('template_position-' . $position_code)->append($generator->buildPage($block['block_id'], false, $page->id, true));
							$this->view->placeholder('template_width-' . $position_code)->append(isset($tb_setting->desktop_width)?$tb_setting->desktop_width:null);
							break;
					}
				}
			}
		}

		$this->view->placeholder('body_id')->append('page-'.$page->id);
		$this->view->placeholder('body_class')->append($page->class);

		if( $this->getRequest()->isPost() ){
			$module_id = $this->getRequest()->getParam('module_id',0);

			$this->formSubmit($module_id, $this->getRequest()->getPost(), $_FILES);

			$mapper = new Application_Model_Mapper_PageSectionRowColumnModule();
			$module = $mapper->find($module_id);

			$module_content = json_decode($module->json_content);
			if (isset($module_content->confirmation_type) && $module_content->confirmation_type == 'select_page' && isset($module_content->confirmation_page_code)) {
				$this->redirect("/{$module_content->confirmation_page_code}");
			}elseif (isset($module_content->confirmation_type) && $module_content->confirmation_type == 'url' && isset($module_content->confirmation_url) && $module_content->confirmation_url) {
				$this->redirect($module_content->confirmation_url);
			}else {
				$this->redirect("/$code");
			}
		}

		//check if page password protected and redirect to login page
		if ($page->password_protect == 'Enabled' && $page->password) {
			$page_password = new Zend_Session_Namespace('page_password');

			if ($page_password->$code != "valid") {
				$this->forward("protected");
			}
		}

		$this->view->content = $generator->buildPage($page->id, $cache_status);

		if ($ssv_mapper->getValueByCode('global_basic_lazy_load_status') == 'Enabled') {
			$regex = '#<img([^>]*) src="([^"/]*/?[^".]*\.[^"]*)"([^>]*)>#';
			$this->view->content = preg_replace_callback($regex, function ($matches) {
				foreach ($matches as $match) {
					if (stripos($match, "rev-slidebg") !== false) {
						return $matches[0];
					}
				}
				return '<img'.$matches[1].' src="/assets/images/blank.gif" data-src="'.$matches[2].'"'.$matches[3].'>';
			}, $this->view->content);

			$this->view->headScript()->appendScript('
document.addEventListener("DOMContentLoaded", function() {
  var lazyImages = [].slice.call(document.querySelectorAll("img"));

  if ("IntersectionObserver" in window) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          if (typeof lazyImage.dataset.src != "undefined") {
            lazyImage.src = lazyImage.dataset.src;
          }
          if (typeof lazyImage.dataset.srcset != "undefined") {
            lazyImage.srcset = lazyImage.dataset.srcset;
          }          
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  }
});
');
		}


		$this->view->doctype(Zend_View_Helper_Doctype::XHTML1_RDFA);
		if ($page->meta_title) {
			$this->view->headTitle()->set($page->meta_title);
			$this->view->headMeta()->setProperty('og:title', $page->meta_title);
		}else {
			$this->view->headTitle()->prepend($page->title);
			$this->view->headMeta()->setProperty('og:title', $page->title);
		}
		if ($page->meta_keywords) {
			$this->view->headMeta()->setName('keywords', $page->meta_keywords);
		}
		if ($page->meta_description) {
			$this->view->headMeta()->setName('description', $page->meta_description);
			$this->view->headMeta()->setProperty('og:description', $page->meta_description);
		}elseif ($meta_description = $settings_mapper->getValueByCode('meta_description')) {
			//$this->view->headMeta()->setName('description', $page->meta_description);
			$this->view->headMeta()->setProperty('og:description', $meta_description);
		}
		if (isset($page->meta_canonical_url) && $page->meta_canonical_url) {
			$this->view->headLink(array('rel' => 'canonical', 'href' => $page->meta_canonical_url));
			$this->view->headMeta()->setProperty('og:url', $page->meta_canonical_url);
		}
		$og_image_default = $settings_mapper->getValueByCode('open_graph_image');
		if (isset($page->open_graph_image) && $page->open_graph_image) {
			$this->view->headMeta()->setProperty('og:image', $this->_full_domain . $page->open_graph_image);
		}elseif ($og_image_default) {
			$this->view->headMeta()->setProperty('og:image', $this->_full_domain . $og_image_default);
		}

		$this->view->placeholder('_meta_head')->append($page->meta_head_custom);

		$this->view->headLink()->appendStylesheet('/index/css/?v='.$generator->cssStamp($page->id));
		//$this->view->headLink(array('media'=>'all' ,'href'=>'/index/css/?v='.$generator->cssStamp($page->id), 'onload'=>'this.media="all"', 'rel'=>'stylesheet' ));
//		$this->view->placeholder('_meta_head')->append('<link href="/index/css/?v='.$generator->cssStamp($page->id).'" rel="stylesheet" media="print" onload="this.media=\'all\'">');

		//Schema.org JSON-LD
		$json_ld = '';
		if ($page->schema_custom_json) {
			$json_ld = $page->schema_custom_json;
		}else {
			$json_ld = json_encode(array(
				"@context" => "http://schema.org",
				"@type" => ($page->schema_page_type)?$page->schema_page_type:"WebPage",
				"name" => ($page->meta_title)?$page->meta_title:$page->title,
				"url" => ($page->meta_canonical_url)?$page->meta_canonical_url:"{$this->_full_domain}/{$page->code}",
				"description" => ($page->meta_description)?$page->meta_description:""
			));
		}
		if ($json_ld) {
			$this->view->placeholder('_meta_head')->append('<script type="application/ld+json">'.$json_ld.'</script>');
		}

		//set body classes based on settings
		$header_type = $settings_mapper->getValueByCode('header_type');

		if ($header_type == '') {
			$this->view->placeholder('body_class')->append('header-fixed-space-default');
		}

		//navigation setup/options/etc.
		if ($page->nav_display == 'Enabled') {
			if (!$page->header_template || $page->header_template == 'default') {
				$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
				$header_type = $settings_mapper->getValueByCode('header_type');

				$page->header_template = $header_type;
			}

			//get nav header visiblity options
			$class = '';
			if (isset($page_settings->nav_desktop_status) && $page_settings->nav_desktop_status == 'Disabled') {
				$class .= ' hidden-lg';
			}
			if (isset($page_settings->nav_tablet_status) && $page_settings->nav_tablet_status == 'Disabled') {
				$class .= ' hidden-md hidden-sm';
			}
			if (isset($page_settings->nav_mobile_status) && $page_settings->nav_mobile_status == 'Disabled') {
				$class .= ' hidden-xs';
			}


			$this->view->placeholder('_header')->prepend('<div class="'.$class.'">');
			if ($template_header) {
				$this->view->placeholder('_header')->append($generator->buildCustomHeader($template_header['block_id']));
			}elseif ($page->header_template && $page->header_template == 'default') {
				$this->view->placeholder('_header')->prepend($generator->buildHeader());
			}elseif ($page->header_template && stripos($page->header_template,"block-") !== false) {
				$header_block_id = str_ireplace("block-","",$page->header_template);
				$this->view->placeholder('_header')->prepend($generator->buildCustomHeader($header_block_id));
			}else {
				$this->view->placeholder('_header')->prepend($generator->buildHeader($page->header_template));
			}
			$this->view->placeholder('_header')->append('</div>');
		}
		//footer
		if ($page->footer_display == 'Enabled') {
			//get footer visiblity options
			$class = '';
			if (isset($page_settings->footer_desktop_status) && $page_settings->footer_desktop_status == 'Disabled') {
				$class .= ' hidden-lg';
			}
			if (isset($page_settings->footer_tablet_status) && $page_settings->footer_tablet_status == 'Disabled') {
				$class .= ' hidden-md hidden-sm';
			}
			if (isset($page_settings->footer_mobile_status) && $page_settings->footer_mobile_status == 'Disabled') {
				$class .= ' hidden-xs';
			}

			$this->view->placeholder('_footer')->prepend('<div class="'.$class.'">');

			if ($template_footer || $template_footer_mobile) {
				if ($template_footer) {
					$this->view->placeholder('_footer')->append('<div class="hidden-xs">');
					$this->view->placeholder('_footer')->append($generator->buildPage($template_footer['block_id'], false, $page->id, true));
					$this->view->placeholder('_footer')->append('</div>');
				}
				if ($template_footer_mobile) {
					$this->view->placeholder('_footer')->append('<div class="visible-xs">');
					$this->view->placeholder('_footer')->append($generator->buildPage($template_footer_mobile['block_id'], false, $page->id, true));
					$this->view->placeholder('_footer')->append('</div>');
				}
			}else {
				$this->view->placeholder('_footer')->append($generator->buildFooter());
			}

			$this->view->placeholder('_footer')->append('</div>');
		}

		//announcement bar
		$announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
		$announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
		if ($announcement_bar_status == 'enable' && ($announcement_bar_page == '' || ($announcement_bar_page == 'home' && $code == $settings_mapper->getValueByCode('home_page')))) {
			$this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
		}

		//promotional pop-up
		$promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
		$promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
		if ($promo_popup_status == 'Enabled' && (!isset($page_settings->promo_popup_force_disable) || $page_settings->promo_popup_force_disable == 'Disabled')
			&& ($promo_popup_page == '' || ($promo_popup_page == 'home' && $code == $settings_mapper->getValueByCode('home_page')))) {

			$promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
			$promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
			$promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
			$extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

			$this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$promo_popup_modal_code.'", "'.$promo_popup_delay.'", "'.$promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
		}
		//page specific promotional pop-up
		if (isset($page_settings->promo_popup_status) && $page_settings->promo_popup_status == 'Enabled') {
			$extra_classes = $page_settings->promo_popup_size;

			$this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$page_settings->promo_popup_modal_code.'", "'.$page_settings->promo_popup_delay.'", "'.$page_settings->promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
		}

		//aggregated module styles
		$this->view->placeholder('_module_style_block')->prepend('<style>');
		$this->view->placeholder('_module_style_block')->append($generator->getModuleStyleBlock());
		$this->view->placeholder('_module_style_block')->append('</style>');
	}

	public function protectedAction()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$generator = new Cny_Model_HtmlBuild();
		$cache_status = true;

		$code = $this->getRequest()->getParam('code','');
		$this->view->error = "";

		$mapper = new Application_Model_Mapper_Page();
		/** @var Application_Model_Page $page */
		$this->view->page = $page = $mapper->doesExists(['code'=>$code]);

		$template_header = $template_footer = $template_footer_mobile = '';
		if ($page->template && $page->template != 'layout') {
			$mapper = new Application_Model_Mapper_Template();
			$template = $mapper->findByCode($page->template);

			if ($template) {
				$mapper = new Application_Model_Mapper_TemplateBlock();
				$blocks = $mapper->findByTemplateId($template->id);

				foreach ($blocks as $position_code => $block) {
					switch ($position_code) {
						case 'header':
							$template_header = $block;
							break;
						case 'footer':
							$template_footer = $block;
							break;
						case 'footer_mobile':
							$template_footer_mobile = $block;
							break;
						default:
							$tb_setting = json_decode($block['json_settings']);

							$this->view->placeholder('template_position-' . $position_code)->append($generator->buildPage($block['block_id'], false, $page->id, true));
							$this->view->placeholder('template_width-' . $position_code)->append($tb_setting->desktop_width);
							break;
					}
				}
			}
		}

		$this->view->placeholder('body_id')->append('page-'.$page->id);
		$this->view->placeholder('body_class')->append($page->class);

		if( $this->getRequest()->isPost() ){
			$password = $this->getRequest()->getParam('password','');

			$authAdapter = $this->_getAuthAdapter(array('code'=>$code,'password'=>$password));
			$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('page_password'));
			$result = $auth->authenticate($authAdapter);

			if( $result->isValid() ){
				$page_password = new Zend_Session_Namespace('page_password');
				$page_password->$code = "valid";
				$page_password->setExpirationSeconds(3600);

				$this->redirect("/$code");
			}else {
				$this->view->error = "Password incorrect";

				$this->view->doctype(Zend_View_Helper_Doctype::XHTML1_RDFA);
				if ($page->meta_title) {
					$this->view->headTitle()->set($page->meta_title);
					$this->view->headMeta()->setProperty('og:title', $page->meta_title);
				}else {
					$this->view->headTitle()->prepend($page->title);
					$this->view->headMeta()->setProperty('og:title', $page->title);
				}
				if ($page->meta_keywords) {
					$this->view->headMeta()->setName('keywords', $page->meta_keywords);
				}
				if ($page->meta_description) {
					$this->view->headMeta()->setName('description', $page->meta_description);
					$this->view->headMeta()->setProperty('og:description', $page->meta_description);
				}elseif ($meta_description = $settings_mapper->getValueByCode('meta_description')) {
					//$this->view->headMeta()->setName('description', $page->meta_description);
					$this->view->headMeta()->setProperty('og:description', $meta_description);
				}
				if (isset($page->meta_canonical_url) && $page->meta_canonical_url) {
					$this->view->headLink(array('rel' => 'canonical', 'href' => $page->meta_canonical_url));
					$this->view->headMeta()->setProperty('og:url', $page->meta_canonical_url);
				}
				$og_image_default = $settings_mapper->getValueByCode('open_graph_image');
				if (isset($page->open_graph_image) && $page->open_graph_image) {
					$this->view->headMeta()->setProperty('og:image', $this->_full_domain . $page->open_graph_image);
				}elseif ($og_image_default) {
					$this->view->headMeta()->setProperty('og:image', $this->_full_domain . $og_image_default);
				}

				$this->view->placeholder('_meta_head')->append($page->meta_head_custom);

				$this->view->headLink()->appendStylesheet('/index/css/?v='.$generator->cssStamp($page->id));

				//Schema.org JSON-LD
				$json_ld = '';
				if ($page->schema_custom_json) {
					$json_ld = $page->schema_custom_json;
				}else {
					$json_ld = json_encode(array(
						"@context" => "http://schema.org",
						"@type" => ($page->schema_page_type)?$page->schema_page_type:"WebPage",
						"name" => ($page->meta_title)?$page->meta_title:$page->title,
						"url" => ($page->meta_canonical_url)?$page->meta_canonical_url:"{$this->_full_domain}/{$page->code}",
						"description" => ($page->meta_description)?$page->meta_description:""
					));
				}
				if ($json_ld) {
					$this->view->placeholder('_meta_head')->append('<script type="application/ld+json">'.$json_ld.'</script>');
				}

				//set body classes based on settings
				$header_type = $settings_mapper->getValueByCode('header_type');

				if ($header_type == '') {
					$this->view->placeholder('body_class')->append('header-fixed-space-default');
				}

				//navigation setup/options/etc.
				if ($page->nav_display == 'Enabled') {
					if (!$page->header_template || $page->header_template == 'default') {
						$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
						$header_type = $settings_mapper->getValueByCode('header_type');

						$page->header_template = $header_type;
					}

					//get nav header visiblity options
					$class = '';
					$settings = json_decode($page->json_settings);
					if (isset($settings->nav_desktop_status) && $settings->nav_desktop_status == 'Disabled') {
						$class .= ' hidden-lg';
					}
					if (isset($settings->nav_tablet_status) && $settings->nav_tablet_status == 'Disabled') {
						$class .= ' hidden-md hidden-sm';
					}
					if (isset($settings->nav_mobile_status) && $settings->nav_mobile_status == 'Disabled') {
						$class .= ' hidden-xs';
					}


					$this->view->placeholder('_header')->prepend('<div class="'.$class.'">');
					if ($template_header) {
						$this->view->placeholder('_header')->append($generator->buildCustomHeader($template_header['block_id']));
					}elseif ($page->header_template && $page->header_template == 'default') {
						$this->view->placeholder('_header')->prepend($generator->buildHeader());
					}elseif ($page->header_template && stripos($page->header_template,"block-") !== false) {
						$header_block_id = str_ireplace("block-","",$page->header_template);
						$this->view->placeholder('_header')->prepend($generator->buildCustomHeader($header_block_id));
					}else {
						$this->view->placeholder('_header')->prepend($generator->buildHeader($page->header_template));
					}
					$this->view->placeholder('_header')->append('</div>');
				}
				//footer
				if ($page->footer_display == 'Enabled') {
					//get footer visiblity options
					$class = '';
					$settings = json_decode($page->json_settings);
					if (isset($settings->footer_desktop_status) && $settings->footer_desktop_status == 'Disabled') {
						$class .= ' hidden-lg';
					}
					if (isset($settings->footer_tablet_status) && $settings->footer_tablet_status == 'Disabled') {
						$class .= ' hidden-md hidden-sm';
					}
					if (isset($settings->footer_mobile_status) && $settings->footer_mobile_status == 'Disabled') {
						$class .= ' hidden-xs';
					}

					$this->view->placeholder('_footer')->prepend('<div class="'.$class.'">');

					if ($template_footer || $template_footer_mobile) {
						if ($template_footer) {
							$this->view->placeholder('_footer')->append('<div class="hidden-xs">');
							$this->view->placeholder('_footer')->append($generator->buildPage($template_footer['block_id'], false, $page->id, true));
							$this->view->placeholder('_footer')->append('</div>');
						}
						if ($template_footer_mobile) {
							$this->view->placeholder('_footer')->append('<div class="visible-xs">');
							$this->view->placeholder('_footer')->append($generator->buildPage($template_footer_mobile['block_id'], false, $page->id, true));
							$this->view->placeholder('_footer')->append('</div>');
						}
					}else {
						$this->view->placeholder('_footer')->append($generator->buildFooter());
					}

					$this->view->placeholder('_footer')->append('</div>');
				}

				//aggregated module styles
				$this->view->placeholder('_module_style_block')->prepend('<style>');
				$this->view->placeholder('_module_style_block')->append($generator->getModuleStyleBlock());
				$this->view->placeholder('_module_style_block')->append('</style>');
			}
		}

	}

	public function cssAction()
	{
		$cached = ($this->getRequest()->getParam('cache',true) === 'false'?false:true);
		$page_id = $this->getRequest()->getParam('page_id',null);

		$generator = new Cny_Model_HtmlBuild();
		$css = $generator->buildSiteCSS($cached, $page_id);

		header_remove('Pragma');
		header("Content-type: text/css");
		header("Cache-Control: max-age=".(60 * 60 * 24 * 365).", public");
		header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (60 * 60 * 24 * 365))); // 1 year
		echo $css;
		exit;
	}

	public function buildCssAction()
	{
		$this->view->layout()->disableLayout();

		$page_id = $this->getRequest()->getParam('page_id',null);

		$cache_manager = new Application_Model_CacheManager();
		$cache_manager->buildCSS($page_id);

		exit;
	}

	public function backgroundBuildCssAction()
	{
		$cache_manager = new Application_Model_CacheManager();

		$page_id = $this->getRequest()->getParam('page_id',null);
		if ($page_id === true) $page_id = null;

		$cache_id = 'css_site'.$page_id;
		$timestamp = time();
		$timestamp_file = $this->_cache_dir_temp.'/'.$cache_id.'.timestamp';

		//if another build in progress wait
		if ($cache_manager->inProgressCheck($timestamp_file, $timestamp) == 'run') {
			$cache_manager->cacheCSS($page_id, $timestamp_file, $timestamp);
		}else {
			exit;
		}

		exit;
	}

	private function formSubmit($module_id, $data, $files)
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$recaptcha_secret_key = $settings_mapper->getValueByCode('recaptcha_secret_key');

		$mapper = new Application_Model_Mapper_PageSectionRowColumnModule();
		$module = $mapper->find($module_id);

		if (!$module) return null;

		$content = json_decode($module->json_content);
		//$settings = json_decode($module->json_settings);

		$slugify = new Slugify();
		$codes = array();
		$panels = array();
		if ($module->type == 'newsletter_form') {
			if (isset($content->form_type) && $content->form_type == 'name_email') {
				$panels[] = (object)array('field_type'=>'text','label'=>'Name','field_required'=>'yes','sort_order'=>1);
				$panels[] = (object)array('field_type'=>'email','label'=>'Email','field_required'=>'yes','sort_order'=>2);
			}elseif (isset($content->form_type) && $content->form_type == 'first_last_email') {
				$panels[] = (object)array('field_type'=>'text','label'=>'First Name','field_required'=>'yes','sort_order'=>1);
				$panels[] = (object)array('field_type'=>'text','label'=>'Last Name','field_required'=>'yes','sort_order'=>2);
				$panels[] = (object)array('field_type'=>'email','label'=>'Email','field_required'=>'yes','sort_order'=>3);
			}else {
				$panels[] = (object)array('field_type'=>'email','label'=>'Email','field_required'=>'yes','sort_order'=>1);
			}
		}else {
			foreach ($content->panels as $panel) {
				if (isset($panel->status) && $panel->status == 'Disabled') continue;

				$panels[$panel->sort_order] = $panel;
			}
			ksort($panels);
		}

		$g_recaptcha = false;
		$g_recatpcha_secret = '';

		foreach ($panels as $panel) {
			$code = $orig_code = $slugify->slugify($panel->label);
			$counter = 1;
			while (in_array($code, $codes)) {
				$code = $orig_code . "_" . $counter;
				$counter++;
			}
			$codes[$code] = $panel;

			if ($panel->field_type == 'recaptcha') {
				if ((isset($panel->recaptcha_secret_key) && $panel->recaptcha_secret_key) || $recaptcha_secret_key) {
					$g_recaptcha = true;
					if ($recaptcha_secret_key) {
						$g_recatpcha_secret = $recaptcha_secret_key;
					}elseif (isset($panel->recaptcha_secret_key) && $panel->recaptcha_secret_key) {
						$g_recatpcha_secret = $panel->recaptcha_secret_key;
					}
				}
			}
		}

		$valid = false;
		if ($g_recaptcha) {
			$recaptcha = new \ReCaptcha\ReCaptcha($g_recatpcha_secret);
			$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
			if ($resp->isSuccess()) {
				$valid = true;
			} else {
				$errors = $resp->getErrorCodes();
			}
		}else {
			$valid = true;
		}

		if ($valid) {
			$body = []; $json_data = [];
			$email_address = '';

			foreach ($data as $key => $value) {
				if (array_key_exists($key, $codes)) {
					$item = $codes[$key];

					if (is_array($value)) {
						$value = implode(", ", $value);
					}

					$body[$item->sort_order] = "<p>".$item->label . ": " . $value . "</p>";

					$json_data[$key] = array(
						'title' => $item->label,
						'field_type' => $item->field_type,
						'field_required' => $item->field_required,
						'sort_order' => $item->sort_order,
						'value' => $value
					);

					if (strtolower($item->field_type) == 'email') {
						$email_address = $value;
					}
				}
			}
			$file_mapper = new Application_Model_Mapper_FormFile();
			foreach ($files as $key => $file) {
				if (array_key_exists($key, $codes)) {
					$item = $codes[$key];

					if (in_array(mime_content_type($file['tmp_name']), $this->_invalid_mimetypes)) {
						unlink($file['tmp_name']);
						continue;
					}

					$dest = realpath($_SERVER['DOCUMENT_ROOT']."/../form_files");
					$filename_hash = sha1_file($file['tmp_name']);
					$mimetype = mime_content_type($file['tmp_name']);
					if (move_uploaded_file($file['tmp_name'], $dest."/".$filename_hash)) {
						$file_mapper->save(new Application_Model_FormFile(array(
							'hash' => $filename_hash,
							'filename' => $file['name'],
							'mimetype' => $mimetype
						)));
					}

					$body[$item->sort_order] = $item->label . ": <a href='{$this->_full_domain}/index/file-download/hash/{$filename_hash}'>" . $file['name'] . "</a><br/>";

					$json_data[$key] = array(
						'title' => $item->label,
						'field_type' => $item->field_type,
						'field_required' => $item->field_required,
						'sort_order' => $item->sort_order,
						'value' => $filename_hash
					);
				}
			}
			ksort($body);

			$mapper = new Application_Model_Mapper_FormSubmission();
			$mapper->save(new Application_Model_FormSubmission(array(
				'module_id' => $module_id,
				'form_title' => (isset($content->form_name) && $content->form_name)?$content->form_name:"Form {$module_id}",
				'json_data' => json_encode($json_data)
			)));

			if ($content->email_address) {
				$mailbody = "<h3>Form: {$content->form_name}</h3><br/><br/>";
				$mailbody .= implode("",$body);

				//reply-to override
				$replyto = null;
				if (isset($content->reply_to_customer_status) && $content->reply_to_customer_status == 'Enabled') {
					$replyto = $email_address;
				}

				$emails = new Application_Model_Emails();
				$emails->generic($mailbody, $content->email_address, $content->form_name . ' Form Submission', isset($content->from_email_address) ? $content->from_email_address : $email_address, null, $replyto);
			}

			//SharpSpring submit
			if (isset($content->sharpspring_lead_add) && $content->sharpspring_lead_add == 'Yes' && $settings_mapper->getValueByCode('sharpspring_account_id') && $settings_mapper->getValueByCode('sharpspring_secret_key')) {
				$ss_map = (array)$content->sharpspring_mappings;

				$field_map = array();
				foreach ($ss_map as $label => $field) {
					if (isset($json_data[$label]['value'])) {
						$field_map[$field] = $json_data[$label]['value'];
					}
				}

				$sharpspring = new Application_Model_SharpSpring();
				$sharpspring->createLead($field_map);

				if (isset($content->sharpspring_list_id) && $content->sharpspring_list_id) {
					$sharpspring->addLeadToListByEmail($content->sharpspring_list_id, $field_map['emailAddress']);
				}
			}

			//MailChimp submit
			if (isset($content->mailchimp_lead_add) && $content->mailchimp_lead_add == 'Yes' && isset($content->mailchimp_list_id) && $content->mailchimp_list_id && $settings_mapper->getValueByCode('mailchimp_secret_key')) {
				$mc_map = (array)$content->mailchimp_mappings;
				$email = ''; $field_map = array();
				foreach ($mc_map as $label => $field) {
					if (isset($json_data[$label]['value'])) {
						if (strtolower($field) == 'email') {
							$email = $json_data[$label]['value'];
						} else {
							$field_map[$field] = $json_data[$label]['value'];
						}
					}
				}

				$mailchimp = new Application_Model_MailChimp();
				$mailchimp->listSubscribe($content->mailchimp_list_id, $email, $field_map);
			}

			//send response email if enabled
			if (isset($content->response_email_status) && $content->response_email_status && isset($content->response_email_text) && $content->response_email_text && $email_address) {
				$mailbody = $content->response_email_text;

				$emails = new Application_Model_Emails();
				$emails->generic($mailbody, $email_address, $content->form_name, isset($content->from_email_address) ? $content->from_email_address : '');
			}
		}
	}

	protected function _getAuthAdapter($formData)
	{
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		$authAdapter = new Jr_Auth_Adapter_DbTableBcrypt( $dbAdapter );
		$authAdapter->setTableName('pages')
			->setIdentityColumn('code')
			->setCredentialColumn('password')
			->setCredentialTreatment('AND status = "Enabled"');

		$authAdapter->setIdentity($formData['code']);
		$authAdapter->setCredential($formData['password']);
		return $authAdapter;
	}

	public function getVideoHtmlAction()
	{
		$this->view->layout()->disableLayout();

		$this->view->video_url = $url = $this->getRequest()->getParam('url','');

		$vp = new Cny_Model_VideoParser();
		$this->view->url = $vp->getEmbedUrl($url);

	}

	public function modalAction()
	{
		$code = $this->getRequest()->getParam('code','');

		$mapper = new Application_Model_Mapper_Page();

		$page = $mapper->doesExists(['code'=>$code]);

		if (!$page || $page->status != 'Enabled') {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}

		$generator = new Cny_Model_HtmlBuild();
		$this->view->content = $generator->buildPage($page->id, true, null, true);

		$this->view->layout()->disableLayout();
		$this->render("page");
	}

	public function ajaxFormSubmitAction()
	{
		if( $this->getRequest()->isPost() ){
			$module_id = $this->getRequest()->getParam('module_id',0);

			$this->formSubmit($module_id, $this->getRequest()->getPost(), $_FILES);

			$mapper = new Application_Model_Mapper_PageSectionRowColumnModule();
			$module = $mapper->find($module_id);

			echo $module->json_content;

			exit;
		}
	}

	public function paymentReviewAction()
	{
		$this->view->placeholder('body_class')->append('payment-method-page');

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$generator = new Cny_Model_HtmlBuild();
		$cache_status = true;

		$this->view->headLink()->appendStylesheet('/index/css/?v='.$generator->cssStamp(0));

		//set body classes based on settings
		$header_type = $settings_mapper->getValueByCode('header_type');

		if ($header_type == '') {
			$this->view->placeholder('body_class')->append('header-fixed-space-default');
		}

		//navigation setup/options/etc.
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$header_type = $settings_mapper->getValueByCode('header_type');

		$this->view->placeholder('_header')->prepend('<div class="">');
		if ($header_type && $header_type == 'default') {
			$this->view->placeholder('_header')->prepend($generator->buildHeader());
		}elseif ($header_type && stripos($header_type,"block-") !== false) {
			$header_block_id = str_ireplace("block-","",$header_type);
			$this->view->placeholder('_header')->prepend($generator->buildCustomHeader($header_block_id));
		}else {
			$this->view->placeholder('_header')->prepend($generator->buildHeader($header_type));
		}
		$this->view->placeholder('_header')->append('</div>');

		//footer
		$this->view->placeholder('_footer')->prepend('<div class="">');
		$this->view->placeholder('_footer')->append($generator->buildFooter());
		$this->view->placeholder('_footer')->append('</div>');

		//aggregated module styles
		$this->view->placeholder('_module_style_block')->prepend('<style>');
		$this->view->placeholder('_module_style_block')->append($generator->getModuleStyleBlock());
		$this->view->placeholder('_module_style_block')->append('</style>');

		$module_id = $this->getRequest()->getParam('module_id',0);

		if($module_id && $this->getRequest()->isPost() ){
			$mapper = new Application_Model_Mapper_PageSectionRowColumnModule();
			$this->view->module = $module = $mapper->find($module_id);
			$this->view->content = json_decode($module->json_content);

			$this->view->data = $this->getRequest()->getPost();
		}else {
			$this->redirect("/");
		}
	}

	public function paymentSubmitAction()
	{
		$module_id = $this->getRequest()->getParam('module_id',0);

		if($module_id && $this->getRequest()->isPost() ){
			$data = $this->getRequest()->getPost();
			$errors = $this->paymentSubmit($module_id, $data);

			$mapper = new Application_Model_Mapper_PageSectionRowColumnModule();
			$module = $mapper->find($module_id);

			if (!$errors) {
				$module_content = json_decode($module->json_content);

				$email_addresses = array();
				$panels = array();
				if (isset($module_content->panels) && $module_content->panels) {
					$slugify = new Slugify();
					$codes = array();
					foreach ($module_content->panels as $panel) {
						$code = $orig_code = $slugify->slugify($panel->label);
						// "payment" is a reserved code value so replacing
						if ($code == 'payment') {
							$code = $orig_code = 'payment_amount';
						}

						$counter = 1;
						while (in_array($code, $codes)) {
							$code = $orig_code . "_" . $counter;
							$counter++;
						}
						$codes[] = $code;
						if (isset($panel->send_confirmation_email) && $panel->send_confirmation_email == 'yes' && $panel->field_type == 'email') {
							$email_addresses[] = $data[$code];
						}

						if (isset($panel->is_payment_amount) && $panel->is_payment_amount == 'yes') {
							$data[$code] = "$".number_format(preg_replace("/[^0-9.]/", "",$data[$code]),2);
						}
						$panels[$panel->sort_order] = array(
							'panel' => $panel,
							'value' => $data[$code]
						);
					}
					ksort($panels);
				}

				//send email
				if ($email_addresses) {
					$emails = new Application_Model_Emails();
					$emails->paymentConfirmation($module_content, $email_addresses, $panels);
				}

				if (isset($module_content->confirmation_type)) {
					if ($module_content->confirmation_type == 'select_page' && isset($module_content->confirmation_page_code)) {
						$this->redirect("/{$module_content->confirmation_page_code}");
					}elseif ($module_content->confirmation_type == 'replace_form') {
						$this->_order_info = new Zend_Session_Namespace('payment_order');
						$this->_order_info->module_content = $module_content;
						$this->_order_info->panels = $panels;

						$this->redirect("/payment-confirmation");
					}else {
						$this->redirect("/");
					}
				} else {
					$this->redirect("/");
				}
			}else {
				$this->view->data = $this->getRequest()->getPost();
				$this->view->payment = $this->view->data['payment'];
				unset($this->view->payment['cc_num']);
				unset($this->view->payment['ccv']);
				$this->view->errors = $errors;
			}

			$this->forward("payment-review");
		}else {
			$this->redirect("/");
		}
	}

	public function paymentConfirmationAction()
	{
		$this->view->placeholder('body_class')->append('payment-method-page');

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$generator = new Cny_Model_HtmlBuild();
		$cache_status = true;

		$this->view->headLink()->appendStylesheet('/index/css/?v='.$generator->cssStamp(0));

		//set body classes based on settings
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$header_type = $settings_mapper->getValueByCode('header_type');

		if ($header_type == '') {
			$this->view->placeholder('body_class')->append('header-fixed-space-default');
		}

		//navigation setup/options/etc.
		$header_type = $settings_mapper->getValueByCode('header_type');

		$this->view->placeholder('_header')->prepend('<div class="">');
		if ($header_type && $header_type == 'default') {
			$this->view->placeholder('_header')->prepend($generator->buildHeader());
		}elseif ($header_type && stripos($header_type,"block-") !== false) {
			$header_block_id = str_ireplace("block-","",$header_type);
			$this->view->placeholder('_header')->prepend($generator->buildCustomHeader($header_block_id));
		}else {
			$this->view->placeholder('_header')->prepend($generator->buildHeader($header_type));
		}
		$this->view->placeholder('_header')->append('</div>');

		//footer
		$this->view->placeholder('_footer')->prepend('<div class="">');
		$this->view->placeholder('_footer')->append($generator->buildFooter());
		$this->view->placeholder('_footer')->append('</div>');

		//aggregated module styles
		$this->view->placeholder('_module_style_block')->prepend('<style>');
		$this->view->placeholder('_module_style_block')->append($generator->getModuleStyleBlock());
		$this->view->placeholder('_module_style_block')->append('</style>');

		//page data
		$this->_order_info = new Zend_Session_Namespace('payment_order');
		$this->view->content = $this->_order_info->module_content;
		$this->view->panels = $this->_order_info->panels;

		$this->_order_info->setExpirationHops(1);
	}

	private function paymentSubmit($module_id, $data)
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$recaptcha_secret_key = $settings_mapper->getValueByCode('recaptcha_secret_key');

		$errors = array();

		$mapper = new Application_Model_Mapper_PageSectionRowColumnModule();
		$module = $mapper->find($module_id);

		if (!$module) return null;

		$content = json_decode($module->json_content);
		//$settings = json_decode($module->json_settings);

		$slugify = new Slugify();
		$codes = array();
		$panels = array();
		foreach ($content->panels as $panel) {
			if (isset($panel->status) && $panel->status == 'Disabled') continue;

			$panels[$panel->sort_order] = $panel;
		}
		ksort($panels);

		$g_recaptcha = false;
		$g_recatpcha_secret = '';
		$payment_amount_field = '';
		$tips = array();
		$recurring = $recurring_plan_id = '';
		$min_amount = 0;

		foreach ($panels as $panel) {
			$code = $orig_code = $slugify->slugify($panel->label);
			// "payment" is a reserved code value so replacing
			if ($code == 'payment') {
				$code = $orig_code = 'payment_amount';
			}

			$counter = 1;
			while (in_array($code, $codes)) {
				$code = $orig_code . "_" . $counter;
				$counter++;
			}
			$codes[$code] = $panel;

			if ($panel->field_type == 'recaptcha') {
				if ((isset($panel->recaptcha_secret_key) && $panel->recaptcha_secret_key) || $recaptcha_secret_key) {
					$g_recaptcha = true;
					if ($recaptcha_secret_key) {
						$g_recatpcha_secret = $recaptcha_secret_key;
					}elseif (isset($panel->recaptcha_secret_key) && $panel->recaptcha_secret_key) {
						$g_recatpcha_secret = $panel->recaptcha_secret_key;
					}
				}
			}elseif ($panel->field_type == 'tip') {
				if ($panel->tip_percent && isset($data[$code]) && $data[$code]) {
					$tips[] = preg_replace("/[^0-9.]/", "", $panel->tip_percent);
				}elseif (!$panel->tip_percent && isset($data[$code]) && $data[$code]) {
					$tips[] = preg_replace("/[^0-9.]/", "", $data[$code]);
				}
				/*
				if ($panel->tip_percent && isset($data[$code]) && $data[$code]) {
					$tips[] = preg_replace("/[^0-9.]/", "", $panel->tip_percent);
				}elseif(!$panel->tip_percent && isset($data[$code]) && $data[$code]) {
					$tips[] = preg_replace("/[^0-9.]/", "", $data[$code]);
				}
				*/
			}elseif ($panel->field_type == 'recurring') {
				if ($panel->recurring_plan_id) {
					$recurring = $panel->recurring_options;
					$recurring_plan_id = $panel->recurring_plan_id;
				}else {
					$recurring = $data[$code];
				}
			}

			if ($panel->is_payment_amount == 'yes') {
				$amount = preg_replace("/[^0-9.]/", "", $data[$code]);

				if (isset($panel->min_amount) && $panel->min_amount) {
					$min_amount = preg_replace("/[^0-9.]/", "", $panel->min_amount);
				}
			}
		}

		//adjust amount outside of loop otherwise amount may not be set before tip field
		$total_tip = 0;
		if ($tips) {
			foreach ($tips as $k => $percent) {
				$total_tip += round($amount * ($percent/100),2);
			}
		}
		$amount += $total_tip;

		$captcha_valid = $valid = false;
		if ($g_recaptcha) {
			$recaptcha = new \ReCaptcha\ReCaptcha($g_recatpcha_secret);
			$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
			if ($resp->isSuccess()) {
				$captcha_valid = $valid = true;
			} else {
				//$errors = $resp->getErrorCodes();
				$errors[] = "reCAPTCHA response missing";
			}
		}else {
			$captcha_valid= $valid = true;
		}

		if ($captcha_valid) {
			if (!$min_amount || $amount >= $min_amount) {
				$transaction_id = '';
				if ($content->payment_gateway == 'stripe') {
					$stripe = new Application_Model_Stripe();

					if ($recurring && $recurring_plan_id) {
						switch ($recurring) {
							case "Monthly":
								$period = "month";
								break;
							case "Annually":
								$period = "year";
								break;
							case "Weekly":
								$period = "week";
								break;
							case "Daily":
								$period = "day";
								break;
						}

						$recurring_profile_id = $stripe->createSubscription($_POST['stripeToken'], $_POST['stripeEmail'], $recurring_plan_id);
					} else {
						$transaction_id = $stripe->charge($_POST['stripeToken'], $amount, $_POST['stripeEmail']);
					}

				} elseif ($content->payment_gateway == 'payrix') {
					//Payrix
					$payrix = new Application_Model_Payrix();
					$response = $payrix->createTransaction($data['payment'], array('total' => $amount), $data['payment'], null, null);

					if (isset($response['response']->id) && $response['response']->id) { //approved
						$payment_profile_id = $customer_profile_id = new Zend_Db_Expr("NULL");

						$transaction_id = $response['response']->id;

						if ($recurring && $recurring_plan_id) {
							switch ($recurring) {
								case "Monthly":
									$period = "month";
									break;
								case "Annually":
									$period = "year";
									break;
								case "Weekly":
									$period = "week";
									break;
								case "Daily":
									$period = "day";
									break;
							}

							$recurring_response = $payrix->createRecurringBilling($data['payment'], $data['payment'], $recurring_plan_id, date("Ymd", strtotime("today + 1 $period")));
							if (isset($recurring_response['response']->id) && $recurring_response['response']->id) {
								$recurring_profile_id = $recurring_response['response']->id;
							}
						}
					} else {
						$valid = false;

						$errors[] = $response['error_message'];
					}
				} elseif ($content->payment_gateway == 'authorize_net') {
					//authorize.net
					$authnet = new Application_Model_AuthorizeNet();
					$response = $authnet->createTransaction($data['payment'], array('total' => $amount), $data['payment'], null, null);

					if (!$response['error_message'] && ($response['response'] != null) && ($response['response']->getResponseCode() == "1")) { //approved
						$payment_profile_id = $customer_profile_id = new Zend_Db_Expr("NULL");
						if (($response['profile_response'] != null) && ($response['profile_response']->getMessages()->getResultCode() == "Ok")) {
							$customer_profile_id = $response['profile_response']->getCustomerProfileId();
							$payment_profile_id = current($response['profile_response']->getCustomerPaymentProfileIdList());
						} else {
							//echo "ERROR Response : " . $response['profile_response']->getMessages()->getMessage()[0]->getCode() . "  " .$response['profile_response']->getMessages()->getMessage()[0]->getText() . "\n";
						}

						if ($recurring) {
							switch ($recurring) {
								case "Monthly":
									$period = "months";
									$frequency = 1;
									break;
								case "Annually":
									$period = "months";
									$frequency = 12;
									break;
								case "Weekly":
									$period = "days";
									$frequency = 7;
									break;
								case "Daily":
									$period = "days";
									$frequency = 1;
									break;
							}

							$recurring_response = $authnet->createRecurringBilling($data['payment'], $amount, $data['payment'], date("Y-m-d", strtotime("today + $frequency $period")), $period, $frequency);
							if (isset($recurring_response['response']['subscription_id']) && $recurring_response['response']['subscription_id']) {
								$recurring_profile_id = $recurring_response['response']['subscription_id'];
							}
						}

						$transaction_id = $response['response']->getTransId();
					} else {
						$valid = false;

						if (isset($response['error_message']) && $response['error_message']) {
							return $response['error_message'];
						}elseif (isset($response['response']) && $response['response']->getErrors() != null) {
							$errors[] = $response['response']->getErrors()[0]->getErrorText(); //$response['response']->getErrors()[0]->getErrorCode()
						} else {
							if ($response['profile_response']->getMessages() != null) {
								$this->view->error[] = $response['profile_response']->getMessages()->getMessage()[0]->getText(); //$response['profile_response']->getErrors()[0]->getErrorCode()
							}
						}
					}
				} elseif ($content->payment_gateway == 'payflowpro') {
					if (isset($data['payment']['name']) && !isset($data['payment']['first_name'])) {
						$name = explode(" ", $data['payment']['name']);
						$data['payment']['first_name'] = (array_key_exists(0, $name)) ? $name[0] : "";
						$data['payment']['last_name'] = (array_key_exists(1, $name)) ? $name[1] : "";
					}

					$payflow = new Application_Model_PayflowPro();
					$response = $payflow->sale_transaction($data['payment'], array('total' => $amount), $data['payment'], null, null);

					if (isset($response['response']['RESULT']) && $response['response']['RESULT'] == 0) {
						$transaction_id = $response['response']['PNREF'];

						if ($recurring) {
							switch ($recurring) {
								case "Monthly":
									$period = "MONT";
									break;
								case "Annually":
									$period = "YEAR";
									break;
								case "Weekly":
									$period = "WEEK";
									break;
								case "Daily":
									$period = "DAYS";
									break;
							}

							$recurring_response = $payflow->create_recurring_billing($data['payment'], $amount, $transaction_id, date("mdY"), $period);
							if (isset($recurring_response['response']['RESULT']) && $recurring_response['response']['RESULT'] == 0) {
								$recurring_profile_id = $recurring_response['response']['PROFILEID'];
							}
						}
					} else {
						$valid = false;

						$errors[] = $response['response']['RESPMSG'];
					}
				}
			}else {
				$valid = false;

				$errors[] = "Payment amount is less then the minimum amount";
			}
		}

		if ($valid) {
			$body = ''; $json_data = []; $tips = []; $amount = 0;

			$body .= "<h3>Payment Received</h3>
				<p>Payment has been received from form: {$content->form_name}</p><br/><br/>
			";

			foreach ($data as $key => $value) {
				if (array_key_exists($key, $codes)) {
					$item = $codes[$key];

					if (is_array($value)) {
						$value = implode(", ", $value);
					}

					$body .= "<p>".$item->label . ": " . $value . "</p>";

					$json_data[$key] = array(
						'title' => $item->label,
						'field_type' => $item->field_type,
						'field_required' => $item->field_required,
						'sort_order' => $item->sort_order,
						'value' => $value
					);

					if ($item->is_payment_amount == 'yes') {
						$amount = preg_replace("/[^0-9.]/", "", $value);
					}elseif ($item->field_type == 'tip') {
						if ($item->tip_percent && $value) {
							$tips[] = preg_replace("/[^0-9.]/", "", $item->tip_percent);
						}elseif (!$item->tip_percent && $value) {
							$tips[] = preg_replace("/[^0-9.]/", "", $value);
						}
					}
				}
			}

			//adjust amount outside of loop otherwise amount may not be set before tip field
			$total_tip = 0;
			if ($tips) {
				foreach ($tips as $k => $percent) {
					$total_tip += round($amount * ($percent/100),2);
				}
			}
			$amount += $total_tip;
			if ($amount) {
				$body .= "<p><b style='color:#ff0000;'>Total</b>: $" . number_format($amount,2) . "</p>";

				$json_data['|payment_total|'] = array(
					'title' => 'Total',
					'field_type' => 'text',
					'field_required' => 'No',
					'sort_order' => 999998,
					'value' => "$".number_format($amount,2)
				);
			}

			//store transaction id
			$json_data['transaction_id'] = array(
				'title' => 'Transaction ID',
				'field_type' => 'text',
				'field_required' => 'No',
				'sort_order' => 999999,
				'value' => $transaction_id
			);

			if ($recurring) {
				$json_data['recurring_profile'] = array(
					'title' => 'Recurring Profile ID',
					'field_type' => 'text',
					'field_required' => 'No',
					'sort_order' => 999999,
					'value' => $recurring_profile_id
				);
			}

			$mapper = new Application_Model_Mapper_FormSubmission();
			$mapper->save(new Application_Model_FormSubmission(array(
				'module_id' => $module_id,
				'form_title' => (isset($content->form_name) && $content->form_name)?$content->form_name:"Form {$module_id}",
				'json_data' => json_encode($json_data)
			)));

			if ($content->email_address) {
				$emails = new Application_Model_Emails();
				$emails->generic($body, $content->email_address, $content->form_name . ' Payment Form Submission', isset($content->from_email_address) ? $content->from_email_address : '');
			}

			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

			//SharpSpring submit
			if (isset($content->sharpspring_lead_add) && $content->sharpspring_lead_add == 'Yes' && $settings_mapper->getValueByCode('sharpspring_account_id') && $settings_mapper->getValueByCode('sharpspring_secret_key')) {
				$ss_map = (array)$content->sharpspring_mappings;

				$field_map = array();
				foreach ($ss_map as $label => $field) {
					if (isset($json_data[$label]['value'])) {
						$field_map[$field] = $json_data[$label]['value'];
					}
				}

				$sharpspring = new Application_Model_SharpSpring();
				$sharpspring->createLead($field_map);

				if (isset($content->sharpspring_list_id) && $content->sharpspring_list_id) {
					$sharpspring->addLeadToListByEmail($content->sharpspring_list_id, $field_map['emailAddress']);
				}
			}

			//MailChimp submit
			if (isset($content->mailchimp_lead_add) && $content->mailchimp_lead_add == 'Yes' && isset($content->mailchimp_list_id) && $content->mailchimp_list_id && $settings_mapper->getValueByCode('mailchimp_secret_key')) {
				$mc_map = (array)$content->mailchimp_mappings;
				$email = ''; $field_map = array();
				foreach ($mc_map as $label => $field) {
					if (isset($json_data[$label]['value'])) {
						if (strtolower($field) == 'email') {
							$email = $json_data[$label]['value'];
						} else {
							$field_map[$field] = $json_data[$label]['value'];
						}
					}
				}

				$mailchimp = new Application_Model_MailChimp();
				$mailchimp->listSubscribe($content->mailchimp_list_id, $email, $field_map);
			}

			return;
		}else {
			return $errors;
		}
	}

	public function fileDownloadAction()
	{
		$hash = $this->getRequest()->getParam("hash","");

		$mapper = new Application_Model_Mapper_FormFile();
		$file = $mapper->findByHash($hash);

		$dest = realpath($_SERVER['DOCUMENT_ROOT']."/../form_files");

		if(empty($hash)) {
			header("HTTP/1.0 404 Not Found");
			return;
		}

		$size = filesize($dest."/".$hash);
		$fp   = fopen($dest."/".$hash, "rb");
		if (!($size && $fp)) {
			// Error.
			return;
		}

		header("Content-type: " . $file->mimetype);
		header("Content-Length: " . $size);
		header("Content-Disposition: attachment; filename=" . $file->filename);
		header('Content-Transfer-Encoding: binary');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		fpassthru($fp);

		exit;
	}

	public function phpTestAction()
	{//having an issue with testing for Plesk PHP path so doing a simple test from CacheManager
		echo "pass";exit;
	}
}
