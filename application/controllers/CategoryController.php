<?php
class CategoryController extends Zend_Controller_Action
{
	function init()
	{
		$this->view->placeholder('body_class')->set("category-page");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$options = $bootstrap->getOptions();
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function listAction()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		//announcement bar
		$announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
		$announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
		if ($announcement_bar_status == 'enable' && $announcement_bar_page == '') {
			$this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
		}

		//promotional pop-up
		$promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
		$promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
		if ($promo_popup_status == 'Enabled' && ($promo_popup_page == '')) {
			$promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
			$promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
			$promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
			$extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

			$this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("' . $promo_popup_modal_code . '", "' . $promo_popup_delay . '", "' . $promo_popup_recurrence . '", "' . $extra_classes . '");
});
');
		}

		$mapper = new Application_Model_Mapper_Category();
		$this->view->categories = $mapper->fetchParents();

		if (!$this->view->categories) { //let's show all products
			$this->view->sort_options = array("a-z"=>"Alpha A-Z","z-a"=>"Alpha Z-A");
			if ($settings_mapper->getValueByCode('checkout_status') != 'Disabled') {
				$this->view->sort_options['low-high'] = 'Price Lowest to Highest';
				$this->view->sort_options['high-low'] = 'Price Highest to Lowest';
			}

			//filters & options
			$this->view->filters = array();
			$this->view->filters['page'] = $page = $this->getRequest()->getParam('page', 1);
			$this->view->filters['per_page'] = $per_page = $this->getRequest()->getParam('per_page', 10);
			$this->view->filters['list_type'] = $this->getRequest()->getParam('list_type', 'grid');
			$this->view->filters['sort_type'] = $sort_type = $this->getRequest()->getParam('sort_type', 'a-z');

			//fetch products
			$mapper = new Application_Model_Mapper_Product();
			$results = $mapper->fetchActive($sort_type, $per_page);

			$total_count = count($results);
			$from_count = 1 + (($page * $per_page) - $per_page);
			if ($total_count && $from_count > $total_count) {
				$this->redirect("/category?" . $_SERVER['QUERY_STRING']);
			}
			$to_count = ($per_page * $page);
			if ($to_count > $total_count) $to_count = $total_count;
			$this->view->display_count = "Displaying " . $from_count . " to " . $to_count . " of " . $total_count . " items";

			$paginator = Zend_Paginator::factory($results);
			$paginator->setCurrentPageNumber($page);
			$paginator->setItemCountPerPage($per_page);
			$this->view->products = $paginator;

			$this->render("view");
		}
	}

    public function viewAction()
    {
        $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    $announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
	    if ($announcement_bar_status == 'enable' && $announcement_bar_page == '') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

	    //promotional pop-up
	    $promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
	    $promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
	    if ($promo_popup_status == 'Enabled' && ($promo_popup_page == '')) {
		    $promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
		    $promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
		    $promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
		    $extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

		    $this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$promo_popup_modal_code.'", "'.$promo_popup_delay.'", "'.$promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
	    }

	    $category = null;
	    $code = $this->getRequest()->getParam('code','');
	    $parent_code = $this->getRequest()->getParam('parent_code','');
	    $id = $this->getRequest()->getParam('id',$this->getRequest()->getParam('category_id',0));

	    $mapper = new Application_Model_Mapper_Category();
	    if ($code) {
		    $category = $mapper->doesExists(array('code'=>$code));
	    }elseif ($id) {
		    $category = $mapper->find($id);
	    }

	    if ($parent_code) {
		    $this->view->parent_category = $parent = $mapper->doesExists(array('code'=>$parent_code));

		    if ($category->parent_id != $parent->id) {
			    throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		    }
	    }

	    if ($category && $category->status == 'Enabled') {
		    $this->view->category = $category;

		    //parent category
		    if (!$category->parent_id) {
			    //check if has children and show sub-category view page
			    $this->view->children = $mapper->fetchByParentId($category->id);

			    if ($this->view->children) {
				    $this->render('sub-category');
			    }
		    }

		    if (!$this->view->children) {
		    	$this->view->sort_options = array("a-z"=>"Alpha A-Z","z-a"=>"Alpha Z-A");
		    	if ($settings_mapper->getValueByCode('checkout_status') != 'Disabled') {
				    $this->view->sort_options['low-high'] = 'Price Lowest to Highest';
				    $this->view->sort_options['high-low'] = 'Price Highest to Lowest';
			    }

			    //filters & options
			    $this->view->filters = array();
			    $this->view->filters['page'] = $page = $this->getRequest()->getParam('page', 1);
			    $this->view->filters['per_page'] = $per_page = $this->getRequest()->getParam('per_page', 10);
			    $this->view->filters['list_type'] = $this->getRequest()->getParam('list_type', 'grid');
			    $this->view->filters['sort_type'] = $sort_type = $this->getRequest()->getParam('sort_type', 'a-z');

			    //fetch category products
			    $mapper = new Application_Model_Mapper_ProductCategory();
			    $results = $mapper->fetchProductsByCategoryId($category->id,$sort_type);

			    $total_count = count($results);
			    $from_count = 1 + (($page * $per_page) - $per_page);
			    if ($total_count && $from_count > $total_count) {
				    $this->redirect("/category/{$category->code}/?" . $_SERVER['QUERY_STRING']);
			    }
			    $to_count = ($per_page * $page);
			    if ($to_count > $total_count) $to_count = $total_count;
			    $this->view->display_count = "Displaying " . $from_count . " to " . $to_count . " of " . $total_count . " items";

			    $paginator = Zend_Paginator::factory($results);
			    $paginator->setCurrentPageNumber($page);
			    $paginator->setItemCountPerPage($per_page);
			    $this->view->products = $paginator;
		    }



		    //meta data
		    $site_name = $settings_mapper->getValueByCode('site_name');
		    $this->view->headTitle($site_name, "SET");
		    $this->view->headTitle()->prepend($category->name);
		    if ($category->meta_keywords) {
			    $this->view->headMeta()->setName('keywords', $category->meta_keywords);
		    }
		    if ($category->meta_description) {
			    $this->view->headMeta()->setName('description', $category->meta_description);
		    }
	    } else {
		    throw new Zend_Controller_Action_Exception('This page does not exist', 404);
	    }

    }


}
