<?php
class AuthController extends Zend_Controller_Action
{
	protected $_redirectUrl = '/';
	protected $_db;
	protected $_dbTable;

	function init()
	{
		$this->view->placeholder('body_class')->append('auth-page');

		$this->_db = Zend_Db_Table::getDefaultAdapter();
		$this->_dbTable = 'customers';

		$bootstrap = $this->getInvokeArg('bootstrap');
		$options = $bootstrap->getOptions();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->_from = ($options['auth']['from_name'])?$options['auth']['from_name']:"Password Recovery";
		$this->_email = ($settings_mapper->getValueByCode('email_from_address'))?$settings_mapper->getValueByCode('email_from_address'):$options['auth']['from_email'];
		if (!$this->_email && isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$this->_email = "do-not-reply@{$_SERVER["SERVER_NAME"]}";
		}
		$this->_site = ($settings_mapper->getValueByCode('site_name'))?$settings_mapper->getValueByCode('site_name'):$options['site']['name'];
		if (!$this->_site && isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$this->_site = $_SERVER["SERVER_NAME"];
		}

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
		if( $auth->hasIdentity() && $_SERVER['REQUEST_URI'] != "/auth/logout" && $_SERVER['REQUEST_URI'] != "/auth/check"){
			$this->redirect('/account');
		}

		$this->_form_data = new Zend_Session_Namespace('form_data');
		if (!is_array($this->_form_data->data)) $this->_form_data->data = array();
	}

	public function indexAction()
	{
		$this->redirect('/login');
	}

	public function loginAction()
	{
		$this->view->placeholder('body_class')->append('auth-page login-page');

		$this->view->remember = "";
		$form = new Application_Form_Login();
		$form->populate($this->_form_data->data);

		if (isset($_COOKIE['CustomerRememberMe'])) {
			$form->getElement('email')->setValue($_COOKIE['CustomerRememberMe']);
			$this->view->remember = "checked";
		}

		$this->view->loginform = $form;

		if( $this->getRequest()->isPost() ){
			$this->forward('identify');
		}
	}

	public function logoutAction()
	{
		//manually destroy session if handler is memcache due to PHP7 memcache bug @see https://github.com/php-memcached-dev/php-memcached/issues/199
		if(session_status() !== PHP_SESSION_DISABLED && ini_get('session.save_handler') === 'memcache') {
			//surpress warnings just in case
			@session_destroy();
		}

		//Zend_Session::namespaceUnset('customer');

		Zend_Session :: forgetMe();

		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		$this->redirect('/login');
	}

	public function identifyAction()
	{
		if( $this->getRequest()->isPost() ){
			$this->_form_data->data = $formData = $this->getRequest()->getPost();
			if( empty($formData['email']) || empty($formData['password']) ){
				$this->_flashMessenger->addMessage('Empty email or password');
			}else{
				if ($this->getRequest()->getParam('remember',0) == '1') {
					setcookie("CustomerRememberMe", $formData['email'], time()+(3600 * 365 * 20));
				}else {
					setcookie("CustomerRememberMe", '', time()-3600);
				}

				// do the authentication
				$valid = false;
				$authAdapter = $this->_getAuthAdapter($formData);
				$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
				$result = $auth->authenticate($authAdapter);
				if( !$result->isValid() ){
					$mapper = new Application_Model_Mapper_Customer();
					$user = $mapper->doesExists(array('email'=>$formData['email']));

					$max_attempts = 3;
					if ($user) {
						$user->failed_attempts = $user->failed_attempts + 1;

						$left = $max_attempts - ($user->failed_attempts);

						$this->_flashMessenger->addMessage('Login failed. You have '.$left.' attempt(s) before your account will automatically be locked');

						if ($left <= 0) {
							$user->password(new Zend_Db_Expr("NULL"));

							$this->_flashMessenger->addMessage('Login failed. Your account has been locked. Contact customer service or use the forgot password to reset your password');
						}

						$mapper->save($user);
					}else {
						$this->_flashMessenger->addMessage('Email address or password incorrect');
					}
				}else{
					$valid = true;

					$data = $authAdapter->getResultRowObject(null,'password');
					$auth->getStorage()->write($data);

					//zype integration
					$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
					if ($settings_mapper->getValueByCode('zype_api_key_admin') != '' || $settings_mapper->getValueByCode('zype_api_key_read_only') != '') {
						$this->_customer = $auth->getIdentity();

						$zype = new Application_Model_Zype();
						$zype_consumer = $zype->getConsumerByEmail($formData['email']);
						//Cny_Debug::prettyPrint($zype_consumer,true);
						$this->_customer->zype = $zype_consumer;

						$token = $zype->getConsumerAccessToken(array('email' => $formData['email'], 'password' => $formData['password']));
						//Cny_Debug::prettyPrint($token,true);

						if ($token && !$token->error) {
							$this->_customer->zype->access_token = $token->access_token;
							$this->_customer->zype->refresh_token = $token->refresh_token;
							$this->_customer->zype->expiration = $token->created_at + $token->expires_in;
						}else {
							$valid = false;

							//manually destroy session if handler is memcache due to PHP7 memcache bug @see https://github.com/php-memcached-dev/php-memcached/issues/199
							if(session_status() !== PHP_SESSION_DISABLED && ini_get('session.save_handler') === 'memcache') {
								//surpress warnings just in case
								@session_destroy();
							}
							//Zend_Session::namespaceUnset('customer');
							Zend_Session :: forgetMe();

							$auth = Zend_Auth::getInstance();
							$auth->clearIdentity();

							$this->_flashMessenger->addMessage('There was a problem logging in. If this persists please use the forgot password link to reset your password.');
						}
					}

					if ($valid) {
						$this->_form_data->setExpirationHops(1);
						$this->_form_data->setExpirationSeconds(1);

						$mapper = new Application_Model_Mapper_Customer();
						$mapper->setLastLogin($data->id);

						$lvp_post_purchase_redirect = new Zend_Session_Namespace('lvp_post_purchase_redirect');
						if ($lvp_post_purchase_redirect->url) {
							$url_redirect = $lvp_post_purchase_redirect->url;
						} else {
							$userAuth = new Cny_UserAuth();
							$this->_redirectUrl = $userAuth->fetchPreviousRequestUrl();
							$url_redirect = ($this->_redirectUrl) ? $this->_redirectUrl : '/account';
						}

						$this->redirect($url_redirect);
						return;
					}
				}
			}
		}
		$this->redirect('/login');
	}

	public function forgotAction()
	{

		if( $this->getRequest()->isPost() ){
			$email = $this->_getParam("email","");

			$mapper = new Application_Model_Mapper_Customer();

			$user = $mapper->doesExists(array('email'=>$email));

			if (!$user || !$email) {
				$this->_flashMessenger->addMessage('Invalid Email');
			}else {
				$code = $mapper->setResetCode($user->id);

				$pageURL = 'http';
				if (array_key_exists("HTTPS",$_SERVER) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
				$pageURL .= "://";
				$pageURL .= $_SERVER["SERVER_NAME"];

				$mailbody = '
<p>Click on the link below to reset your password:</p>
<p><a href="'.$pageURL."/auth/reset/c/".$code.'">'.$pageURL."/auth/reset/c/".$code.'</a></p>
<p>If you did not request for your password to be reset you don\'t need to take any action, just ignore this email.</p>
<p>The link to reset your password will expire in one(1) hour.</p>
				';

				$emails = new Application_Model_Emails();
				$emails->generic($mailbody, $user->email, 'Password reset for '.$this->_site,'');

				$this->_flashMessenger->addMessage('Password reset email sent');

				$this->redirect('/login');
			}
		}
	}

	public function resetAction()
	{
		$this->view->code = $code = $this->_getParam("c","");

		$mapper = new Application_Model_Mapper_Customer();

		$user = $mapper->validateCode($code);

		if (!$user || !$code) {
			$this->_flashMessenger->addMessage('Email Address not found or link expired');
		}else {
			if( $this->getRequest()->isPost() ){
				$password = $this->_getParam("password","");
				$v_password = $this->_getParam("v_password","");

				if( $password && $v_password == $password ){
					$bcrypt = new Jr_Crypt_Password_Bcrypt();

					$user->password = $bcrypt->create($password);
					$mapper->save($user);

					$mapper->clearResetCode($user->id);

					//zype integration
					$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
					if ($settings_mapper->getValueByCode('zype_api_key_admin') != '' || $settings_mapper->getValueByCode('zype_api_key_read_only') != '') {
						$zype = new Application_Model_Zype();

						$response = $zype->getConsumerByEmail($user->email);
						$response = $zype->updateConsumer($response->_id, array(
							'password' => $password,
							'password_confirmation' => $v_password
						));
					}

					$this->_flashMessenger->addMessage('Password successfully saved');

					$this->redirect('/login');
				}

				$this->_flashMessenger->addMessage('Password doesn\'t match');
			}
		}
	}

	protected function _getAuthAdapter($formData)
	{
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		$authAdapter = new Jr_Auth_Adapter_DbTableBcrypt( $dbAdapter );
		$authAdapter->setTableName($this->_table_prefix.'customers')
			->setIdentityColumn('email')
			->setCredentialColumn('password')
			->setCredentialTreatment('AND status = "enabled"');

		$authAdapter->setIdentity($formData['email']);
		$authAdapter->setCredential($formData['password']);

		return $authAdapter;
	}

	public function checkAction()
	{
		$this->view->layout()->disableLayout();

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
		echo json_encode(array(
				'isLoggedIn' => ($auth->hasIdentity())?true:false,
				'loginHTML' => '',
				'registerHTML' => '',
			)
		);

		exit;
	}

	public function registerAction()
	{
		$this->view->placeholder('body_class')->append('register-page');

		$form = new Application_Form_Register();
		$form->populate($this->_form_data->data);

		$this->view->form = $form;

		if( $this->getRequest()->isPost() ){
			$this->forward('create');
		}
	}

	public function createAction()
	{
		$form = new Application_Form_Register();

		if( $this->getRequest()->isPost() ){
			$this->_form_data->data = $formData = $this->getRequest()->getPost();
			if( $form->isValid($formData)) {
				$mapper = new Application_Model_Mapper_Customer();

				if ($mapper->doesExists(array('email'=>$formData['email']))) {
					$this->_flashMessenger->addMessage('Account already exists. Please login or use a new email address.');
				}elseif ($formData['v_password'] != $formData['password']) {
					$this->_flashMessenger->addMessage('Password doesn\'t match.');
				}else {
					$this->_form_data->setExpirationHops(1);
					$this->_form_data->setExpirationSeconds(1);

					$bcrypt = new Jr_Crypt_Password_Bcrypt();
					$password = $bcrypt->create($formData['password']);

					$mapper->save(new Application_Model_Customer(array(
						'first_name' => $formData['first_name'],
						'last_name' => $formData['last_name'],
						'email' => $formData['email'],
						'password' => $password,
						'status' => 'Enabled'
					)));

					//MailChimp submit
					$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
					if ($settings_mapper->getValueByCode('mailchimp_registration_status') == 'Enabled' && $list_id = $settings_mapper->getValueByCode('mailchimp_registration_list_id')) {
						$mailchimp = new Application_Model_MailChimp();
						$mailchimp->listSubscribe($list_id, $formData['email'], array('FNAME'=>$formData['first_name'],'LNAME'=>$formData['last_name']));
					}

					//zype integration
					if ($settings_mapper->getValueByCode('zype_api_key_admin') != '' || $settings_mapper->getValueByCode('zype_api_key_read_only') != '') {
						$zype = new Application_Model_Zype();
						$result = $zype->createConsumer($formData);
					}

					$this->forward('identify'); //not sure why forwarding doesn't work here.

					$this->_flashMessenger->addMessage('Your account was successfully created. Please login to continue.');
					$this->redirect('/login');
				}

				$this->redirect('/register');
			}else {
				$this->_flashMessenger->addMessage('Please complete all fields');

				$this->redirect('/register');
			}
		}else {
			$this->redirect('/register');
		}
	}

	public function loginAsAction()
	{
		$admin_auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		$id = $this->getRequest()->getParam('id', 0);

		if( $admin_auth->hasIdentity() && $id ){
			// do the authentication
			$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));

			$mapper = new Application_Model_Mapper_Customer();
			$data = $mapper->find($id);
			unset($data->password);

			$auth->getStorage()->write($data);
		}

		$this->redirect('/account');
	}
}
