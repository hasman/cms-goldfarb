<?php
class AccountController extends Zend_Controller_Action
{

    public function init()
    {
	    if (!$this->getRequest()->isXmlHttpRequest()) {
		    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		    //announcement bar
		    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
		    $announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
		    if ($announcement_bar_status == 'enable' && ($announcement_bar_page == '' || $announcement_bar_page == 'account')) {
			    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
		    }

		    //promotional pop-up
		    $promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
		    $promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
		    if ($promo_popup_status == 'Enabled' && ($promo_popup_page == '' || $promo_popup_page == 'account')) {
			    $promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
			    $promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
			    $promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
			    $extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

			    $this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$promo_popup_modal_code.'", "'.$promo_popup_delay.'", "'.$promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
		    }
	    }

        /* Initialize action controller here */
        $this->view->placeholder('body_class')->append('account-page');

        $this->_db = Zend_Db_Table::getDefaultAdapter();
	    $options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
	    $this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
        if( !$auth->hasIdentity() ){
	        if( $auth->hasIdentity() && $_SERVER['REQUEST_URI'] != "/account/restricted") {
		        $userAuth = new Cny_UserAuth();
		        $userAuth->storeRequestAndAuthenticateUser();
		        $this->redirect('/login');
	        }
        }else {
            $this->_customer = $auth->getIdentity();
        }

        $this->view->shipping_countries = Zend_Registry::get('countries')->toArray();
        $this->view->billing_countries = Zend_Registry::get('countries')->toArray();
        $this->_states = Zend_Registry::get('regions')->us->region->toArray();
        $this->view->usstates = $this->_states;
	    $this->view->settings = $this->_customer;
    }

    public function indexAction()
    {
        $this->getResponse()->setRedirect('/account/addresses');
    }

    public function addressesAction()
    {
        $mapper = new Application_Model_Mapper_CustomerAddress();
        $this->view->billing_default = $mapper->findByCustomerId($this->_customer->id, 'billing', 'Yes', true);
        $this->view->shipping_default = $mapper->findByCustomerId($this->_customer->id, 'shipping', 'Yes', true);
        $this->view->billing = $mapper->findByCustomerId($this->_customer->id, 'billing', 'No', true);
        $this->view->shipping = $mapper->findByCustomerId($this->_customer->id, 'shipping', 'No', true);
    }

    public function addressDeleteAction()
    {
        $id = $this->getRequest()->getParam('id', 0);
        if(!empty($id) && is_numeric($id)){
            $mapper = new Application_Model_Mapper_CustomerAddress();
            $entity = $mapper->find($id);

	        if ($entity->customer_id != $this->_customer->id) {
		        $this->getResponse()->setRedirect('/account/addresses');
	        }

            if ($entity->customer_id == $this->_customer->id) {
                $mapper->delete($entity);
            }
        }

        $this->getResponse()->setRedirect('/account/addresses');
    }

    public function addressDefaultAction()
    {
        $id = $this->getRequest()->getParam('id', 0);
        if(!empty($id) && is_numeric($id)){
            $mapper = new Application_Model_Mapper_CustomerAddress();
            $entity = $mapper->find($id);

	        if ($entity->customer_id != $this->_customer->id) {
		        $this->getResponse()->setRedirect('/account/addresses');
	        }

            if ($entity->customer_id == $this->_customer->id) {
                $mapper->makeDefault($this->_customer->id, $id, $entity->address_type);
            }
        }

        $this->getResponse()->setRedirect('/account/addresses');
    }

    public function addressAddAction()
    {
        $type = $this->getRequest()->getParam('type', 'shipping');

        $form = new Application_Form_Address();
        $form->getElement('cmd')->setValue('add');
        $form->getElement('address_type')->setValue($type);
        $form->getElement('country')->setValue('US');

        $this->view->cmd = 'add';
        $this->view->form = $form;
        $this->render('address-form');
    }

    public function addressEditAction()
    {
        $this->view->film_id = $id = $this->getRequest()->getParam('id', 0);
        if(!empty($id) && is_numeric($id)){

            $form = new Application_Form_Address();
            $mapper = new Application_Model_Mapper_CustomerAddress();
            $entity = $mapper->find($id);

	        if ($entity->customer_id != $this->_customer->id) {
		        $this->getResponse()->setRedirect('/account/addresses');
	        }

            $data = $entity->toArray();

            $form->populate($data);
            $form->getElement('cmd')->setValue('edit');
            $this->view->form = $form;
            $this->view->cmd = 'edit';

            $this->render('address-form');
        }else{
            $this->getResponse()->setRedirect('/account/addresses');
        }
    }

    public function addressSaveAction()
    {
        $form = new Application_Form_Address();
        $form->isValid($this->getRequest()->getParams());
        $data = $form->getValues();
        $this->view->errors = array();

        $mapper = new Application_Model_Mapper_CustomerAddress();

        if( $form->isValid($this->getRequest()->getParams())){
            $data['customer_id'] = $this->_customer->id;
            if (!$data['default_address'])
                $data['default_address'] = 'No';

            if ($data['country'] == 'US' || $data['country'] == 'CA') {
                $data['region'] = $this->getRequest()->getParam('region');
            }else {
                $data['region'] = $this->getRequest()->getParam('region_alt');
            }

            $entity = new Application_Model_CustomerAddress($data);
            $id = $mapper->save($entity);

            if ($this->getRequest()->getParam('default','') == 'y') {
                if ($entity->customer_id == $this->_customer->id) {
                    $mapper->makeDefault($this->_customer->id, $id, $data['address_type']);
                }
            }

            if ($this->getRequest()->getParam('copy','') == 'y') {
                if ($data['address_type'] == 'billing') {
                    $data['address_type'] = 'shipping';
                }else {
                    $data['address_type'] = 'billing';
                }

                $entity = new Application_Model_CustomerAddress($data);
                $id = $mapper->save($entity);

                if ($this->getRequest()->getParam('default','') == 'y') {
                    if ($entity->customer_id == $this->_customer->id) {
                        $mapper->makeDefault($this->_customer->id, $id, $data['address_type']);
                    }
                }
            }

            $this->getResponse()->setRedirect('/account/addresses');
        }else{
            $data = $form->getValues();
            $this->view->cmd = $data['cmd'];
            $form->getElement('cmd')->setValue($data['cmd']);

            $this->view->address_id = $data['id'];

            $form->populate($data);
            $this->view->form = $form;

            $this->render('address-form');
        }
    }

    public function settingsAction()
    {
        $this->view->settings = $this->_customer;
    }

    public function settingsEditAction()
    {
        $form = new Application_Form_Settings();

        $data = (array)$this->_customer;

        $form->populate($data);
        $this->view->form = $form;

        $this->render('settings-form');
    }

    public function settingsSaveAction()
    {
        $form = new Application_Form_Settings();
        $form->isValid($this->getRequest()->getParams());
        $data = $form->getValues();
        $errors = array();

        if( $this->getRequest()->isPost() ) {
            $valid = true;
            $mapper = new Application_Model_Mapper_Customer();

            if ($mapper->doesExists(array('email'=>$data['email']), $this->_customer->id)) {
                $valid = false;
                $errors[] = "Account already exists";
            }
            if ($data['password'] != $data['v_password']) {
                $valid = false;
                $errors[] = "Password doesn't match";
            }

            if ($form->isValid($data) && $valid) {
                $entity = $mapper->find($this->_customer->id);

                if ($data['password'] && $data['password'] == $data['v_password']) {
	                //zype integration
	                $settings_mapper = new Application_Model_Mapper_SiteSettingValue();
	                if ($settings_mapper->getValueByCode('zype_api_key_admin') != '' || $settings_mapper->getValueByCode('zype_api_key_read_only') != '') {
		                $zype = new Application_Model_Zype();

		                $response = $zype->getConsumerByEmail($data['email']);
		                if ($response && $response->_id) {
			                $response = $zype->updateConsumer($response->_id, array(
				                'password' => $data['password'],
				                'password_confirmation' => $data['v_password']
			                ));
		                }
	                }

                    $bcrypt = new Jr_Crypt_Password_Bcrypt();
                    $data['password'] = $entity->password = $bcrypt->create($data['password']);
                }
                unset($data['v_password']);

	            $entity_data = array_merge($entity->toArray(), $data);
	            $mapper->save(new Application_Model_Customer($entity_data));

                if ($data['status'] == 'Disabled') {
                    $this->getResponse()->setRedirect('/auth/logout');
                }

                //store new info in session so welcome changes etc.
                $new_data = $mapper->find($this->_customer->id);

                unset($new_data->password);
                $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
                $auth->getStorage()->write($new_data);

                $this->getResponse()->setRedirect('/account/settings');
            } else {
                $this->view->error = implode("<br/>", $errors);

                $data = $form->getValues();

                $form->populate($data);
                $this->view->form = $form;

                $this->render('settings-form');
            }
        }
    }

	public function ordersAction()
	{
		$page = $this->getRequest()->getParam('page',1);

		$mapper = new Application_Model_Mapper_Order();
		$results = $mapper->findByCustomerId($this->_customer->id);

		$paginator = Zend_Paginator::factory($results);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(20);
		$this->view->list = $paginator;
	}

	public function orderViewAction()
	{
		$id = $this->getRequest()->getParam('id');
		if(!empty($id) && is_numeric($id)){
			$mapper = new Application_Model_Mapper_Order();
			$this->view->order = $mapper->find($id);

			if ($this->view->order->customer_id != $this->_customer->id) {
				$this->getResponse()->setRedirect('/account/orders');
			}

			$mapper = new Application_Model_Mapper_OrderAddress();
			$this->view->billing_address = $mapper->findByOrderId($id,'billing');
			$this->view->shipping_address = $mapper->findByOrderId($id,'shipping');
			if (!$this->view->shipping_address) $this->view->shipping_address = $this->view->billing_address;

			$mapper = new Application_Model_Mapper_OrderNote();
			$this->view->note = $mapper->findByOrderId($id);

			$mapper = new Application_Model_Mapper_OrderPayment();
			$this->view->payment = $mapper->findByOrderId($id);

			$mapper = new Application_Model_Mapper_OrderProfile();
			$this->view->profile = $mapper->findByOrderId($id);

			$mapper = new Application_Model_Mapper_OrderShipping();
			$this->view->shipping = $mapper->findByOrderId($id);

			$mapper = new Application_Model_Mapper_OrderItem();
			$this->view->items = $mapper->findByOrderId($id);

			$mapper = new Application_Model_Mapper_OrderTotal();
			$this->view->totals = $mapper->findByOrderId($id);
		}else{
			$this->getResponse()->setRedirect('/account/orders');
		}
	}

	public function wishlistAction()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		if ($settings_mapper->getValueByCode('product_wishlist_status') != 'Enabled') {
			$this->redirect("/account");
		}

		$mapper = new Application_Model_Mapper_CustomerWishlist();
		$this->view->list = $mapper->fetchByCustomerId($this->_customer->id);
	}

	public function restrictedAction()
	{
		//
	}

	public function watchlistAction()
	{
		$mapper = new Application_Model_Mapper_CustomerWatchlist();
		$watchlist = $mapper->findByCustomerId($this->_customer->id);
		$this->view->list = ($watchlist)?json_decode($watchlist->list):array();
	}
}

