<?php
class ReviewController extends Zend_Controller_Action
{
	function init()
	{
		//$this->view->placeholder('body_class')->set("product-page");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$options = $bootstrap->getOptions();
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();
		$this->valid_mimetypes = array(
			'image/png', //Portable Network Graphics
			'image/svg+xml', //Scalable Vector Graphics
			'image/webp', //Web Picture format
			'image/jpeg', //Joint Photographic Expert Group image
			'image/gif', //Graphics Interchange Format
		);

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix']) ? $options['resources']['multidb']['db']['table_prefix'] : '';
	}

	public function productAction()
	{
		$ret_url = $this->getRequest()->getParam('ret_url', '/');

		if( $this->getRequest()->isPost() ) {
			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

			$valid = false;
			if ($settings_mapper->getValueByCode('product_reviews_captcha_status') == 'Enabled') {
				$recaptcha_secret_key = $settings_mapper->getValueByCode('recaptcha_secret_key');

				if ($recaptcha_secret_key) {
					$recaptcha = new \ReCaptcha\ReCaptcha($recaptcha_secret_key);
					$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
					if ($resp->isSuccess()) {
						$valid = true;
					} else {
						$errors = $resp->getErrorCodes();
					}
				}
			}else {
				$valid = true;
			}

			$data = $this->getRequest()->getParam('data', array());
			$data['remote_addr'] = $_SERVER['REMOTE_ADDR'];
			$data['referrer'] = $_SERVER['HTTP_REFERER'];
			$data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
			$data['status'] = 'Pending';

			if ($valid) {
				if ($settings_mapper->getValueByCode('product_reviews_auto_approval') == 'Enabled') {
					$data['status'] = 'Approved';
				}

				$data['is_spam'] = 'No';
				if ($settings_mapper->getValueByCode('akismet_api_key')) {
					$akismet = new Application_Model_Akismet();

					$verify_data = array(
						'user_ip' => $data['user_ip'],
						'referrer' => $data['referrer'],
						'comment_type' => 'review',
						'comment_author' => $data['name'],
						'comment_author_email' => (isset($data['email'])) ? $data['email'] : null,
						'comment_content' => $data['review'],
						'comment_date_gmt' => date("c")
					);

					if ($akismet->commentCheck($verify_data)) { //akismet returns "false" if not spam
						$data['is_spam'] = 'Yes';
						$data['status'] = 'Pending';
					}
				}

				$mapper = new Application_Model_Mapper_ProductReview();
				$ReviewID = $mapper->save(new Application_Model_ProductReview($data));

				if(count($_FILES['filename']['name']) > 0){
					//create folder if doesn't exist
					if (!is_dir(realpath($_SERVER['DOCUMENT_ROOT'] . "/..")."/review_files")) {
						mkdir(realpath($_SERVER['DOCUMENT_ROOT'] . "/..")."/review_files");
						chmod(realpath($_SERVER['DOCUMENT_ROOT'] . "/..")."/review_files", 0777);
					}

					//Loop through each file
					for($i=0; $i<count($_FILES['filename']['name']); $i++) {
						//Get the temp file path
						$tmpFilePath = $_FILES['filename']['tmp_name'][$i];

						//Make sure we have a filepath
						if($tmpFilePath != "") {
							//save the filename
							$shortname = $_FILES['filename']['name'][$i];
							//Check the mime_type
							if (in_array(mime_content_type($_FILES['filename']['tmp_name'][$i]), $this->valid_mimetypes)) {
								$filename_hash = sha1_file($_FILES['filename']['tmp_name'][$i]);
								$mimetype = mime_content_type($_FILES['filename']['tmp_name'][$i]);
								//save the url and the file
								$filePath = realpath($_SERVER['DOCUMENT_ROOT'] . "/../review_files") . "/" . $filename_hash;

								//Upload the file into the temp dir
								if (move_uploaded_file($tmpFilePath, $filePath)) {
									try{

										//$files[] = $shortname;
										//insert into db
										//use $shortname for the filename
										//use $filePath for the relative url to the file
										$file_mapper = new Application_Model_Mapper_ProductReviewFile();
										$file_mapper->save(new Application_Model_ProductReviewFile(array(
											'product_review_id' => $ReviewID,
											'hash' => $filename_hash,
											'filename' => $shortname,
											'mimetype' => $mimetype,
										)));
									}catch (Exception $e) {
										//echo "An exception has occurred: ".$e;
								}
							} else {
									//delete that file
									unlink($_FILES['filename']['tmp_name'][$i]);
								}
							}
						}
					}
				}

				//end upload
			}
		}
;
		$this->redirect($ret_url);
	}

	public function imageAction()
	{
		$hash = $this->getRequest()->getParam('hash');
		//Get the records from the review_files table
		$mapper = new Application_Model_Mapper_ProductReviewFile();
		$records = $mapper->findByHash($hash);

		if(!empty($hash)){
			$file = realpath($_SERVER['DOCUMENT_ROOT'] . "/../review_files") . "/" . $hash;

			// Quick check to verify that the file exists
			if (file_exists($file)) {
				header("Content-type: " . $records['mimetype']);
				header('Expires: 0');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file));
				//readfile($file);
				readfile($file);
				exit;
			} else{
				echo "Sorry, file does not exist.";
				exit;
			}
		}
	}

	public function modalImageAction()
	{
		$this->view->layout()->disableLayout();

		$hash = $this->getRequest()->getParam('hash');

		echo '<img src="/review/image/hash/'.$hash.'" class="img-responsive center-block">';

		exit;
	}
}
