<?php

class BlogController extends Zend_Controller_Action
{

	function init()
	{
		$this->view->placeholder('section')->set("home");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->_full_domain = (isset($options['site']['full_secure']))?$options['site']['full_secure']:null;
		if (isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
			$this->_full_domain = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
		}

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->view->blog_url = $blog_url = ($settings_mapper->getValueByCode('blog_url'))?$settings_mapper->getValueByCode('blog_url'):'blog';
		$blog_status = ($settings_mapper->getValueByCode('blog_status'))?$settings_mapper->getValueByCode('blog_status'):'Enabled';
		if ($blog_status != 'Enabled') {
			throw new Zend_Controller_Action_Exception('This page does not exist', 404);
		}
		$this->_blog_meta_title = ($settings_mapper->getValueByCode('blog_meta_title'))?$settings_mapper->getValueByCode('blog_meta_title'):'Blog';
		$this->_blog_meta_keywords = ($settings_mapper->getValueByCode('blog_meta_keywords'))?$settings_mapper->getValueByCode('blog_meta_keywords'):'';
		$this->_blog_meta_description = ($settings_mapper->getValueByCode('blog_meta_description'))?$settings_mapper->getValueByCode('blog_meta_description'):'';
	}

    public function indexAction()
    {
	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    $this->view->headTitle()->set("{$this->_blog_meta_title} - Home");
	    if ($this->_blog_meta_keywords) {
		    $this->view->headMeta()->appendName('keywords', $this->_blog_meta_keywords);
	    }
	    if ($this->_blog_meta_description) {
		    $this->view->headMeta()->appendName('description', $this->_blog_meta_description);
	    }

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    if ($announcement_bar_status == 'enable') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

        $this->view->placeholder('body_class')->append('blog-page');

	    $page = $this->getRequest()->getParam('page', 1);
	    $per_page = 20;

        $mapper = new Application_Model_Mapper_BlogPost();
	    $posts = $mapper->fetchAllActiveArticles();

	    $paginator = Zend_Paginator::factory($posts);
	    $paginator->setCurrentPageNumber($page);
	    $paginator->setItemCountPerPage($per_page);
	    $this->view->posts = $paginator;

	    $this->view->latest = $mapper->fetchLatestArticles(4);

	    $tag_mapper = new Application_Model_Mapper_BlogPostTag();
	    $this->view->tags = $tag_mapper->fetchAllActive();
    }

    public function articleAction()
    {
	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    if ($announcement_bar_status == 'enable') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

        $this->view->doctype('XHTML1_RDFA');

        $code = $this->getRequest()->getParam('code');
        if (!is_null($code)) {
            $service = new Application_Model_Mapper_BlogPost();
            $article = $service->findByCode($code);
            if ($article) {
                $this->view->placeholder('body_class')->append('blog-page blog-detail-page');
                $this->view->article = $article;
                $this->view->headTitle()->prepend($article['title']);
	            $this->view->headMeta()->setProperty("og:title",$article['title']);

                if ($article['meta_keywords']) {
                    $this->view->headMeta()->setName("keywords",$article['meta_keywords']);
                }
                if ($article['meta_description']) {
                    $this->view->headMeta()->setName("description",$article['meta_description']);
                    $this->view->headMeta()->setProperty("og:description",$article['meta_description']);
                }

                if ($article['thumbnail']) {
	                $this->view->headMeta()->setProperty("og:image",$this->_full_domain.$article['thumbnail']);
                }elseif($article['image']) {
	                $this->view->headMeta()->setProperty("og:image",$this->_full_domain.$article['image']);
                }
                $this->view->headMeta()->setProperty("og:type","article");

                $mapper = new Application_Model_Mapper_BlogPostTag();
                $this->view->tags = $mapper->fetchByBlogId($article['id']);

	            $this->view->latest = $this->view->related = $service->fetchLatestArticles(4, $article['id']);

	            $related_type = $settings_mapper->getValueByCode('blog_related_type');
                if ($related_type == 'tag') {
	                $mapper = new Application_Model_Mapper_BlogPost();
	                $this->view->related = $mapper->fetchRelatedArticlesBytags($article['id'], 4);
                }elseif ($related_type == 'category') {
                	//
                }

                //get next and previous posts
	            $this->view->next_post = $service->findNextPrev($article['id'], 'next');
	            $this->view->prev_post = $service->findNextPrev($article['id'], 'prev');
            } else {
	            throw new Zend_Controller_Action_Exception('This page does not exist', 404);
            }
        } else {
	        throw new Zend_Controller_Action_Exception('This page does not exist', 404);
        }
    }

    public function tagAction()
    {
	    $generator = new Cny_Model_HtmlBuild();
	    $this->view->header = $generator->buildHeader();
	    $this->view->footer = $generator->buildFooter();

	    //set body classes based on settings
	    $settings_mapper = new Application_Model_Mapper_SiteSettingValue();
	    $header_type = $settings_mapper->getValueByCode('header_type');

	    if ($header_type == '') {
		    $this->view->placeholder('body_class')->append('header-fixed-space-default');
	    }
	    if ($settings_mapper->getValueByCode('nav_fixed_position') != 'navbar-fixed-bottom' && $settings_mapper->getValueByCode('nav_fixed_position') != 'navbar-static-top') {
		    $this->view->placeholder('body_class')->append('header-fixed');
	    }

	    //footer and global CSS
	    $mapper = new Application_Model_Mapper_Page();
	    $footer = $mapper->doesExists(['code'=>'footer']);
	    $this->view->headLink()->appendStylesheet('/index/css/page_id/'.$footer->id.'/?v='.$generator->cssStamp($footer->id));

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    $announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
	    if ($announcement_bar_status == 'enable' && $announcement_bar_page == '') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

	    //promotional pop-up
	    $promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
	    $promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
	    if ($promo_popup_status == 'Enabled' && ($promo_popup_page == '')) {
		    $promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
		    $promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
		    $promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
		    $extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

		    $this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$promo_popup_modal_code.'", "'.$promo_popup_delay.'", "'.$promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
	    }

	    $this->view->placeholder('body_class')->append('blog-page blog-tag-page');

	    $page = $this->getRequest()->getParam('page', 1);
	    $row_count = $this->getRequest()->getParam('rowcount', 6);
	    $code = $this->getRequest()->getParam('tag','');

	    $this->view->headTitle()->set("{$this->_blog_meta_title} Tag: $code");
	    if ($this->_blog_meta_keywords) {
		    $this->view->headMeta()->appendName('keywords', $this->_blog_meta_keywords);
	    }
	    if ($this->_blog_meta_description) {
		    $this->view->headMeta()->appendName('description', $this->_blog_meta_description);
	    }

        $mapper = new Application_Model_Mapper_BlogTag();
        $this->view->tag = $tag = $mapper->findByCode($code);

        if ($tag) {
            $mapper = new Application_Model_Mapper_BlogPostTag();
            $posts = $mapper->fetchBlogsByTagId($tag->id);

            $paginator = Zend_Paginator::factory($posts);
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($row_count);
            $this->view->posts = $paginator;
        }else {
	        throw new Zend_Controller_Action_Exception('This page does not exist', 404);
        }
    }

    public function rssAction()
    {
        $this->view->layout()->disableLayout();

        $feed = new Zend_Feed_Writer_Feed;
        $feed->setTitle($this->_blog_meta_title);
        $feed->setDescription('News');
        $feed->setLink('https://'.$this->_domain.'/'.$this->view->blog_url);
        $feed->setFeedLink('https://'.$this->_domain.'/'.$this->view->blog_url.'/rss', 'rss');
        $feed->addAuthor(array(
            'name'  => $this->_blog_meta_title,
            'email' => 'rss@'.$this->_domain,
            'uri'   => 'https://'.$this->_domain
        ));
        $feed->setDateModified(time());

        $mapper = new Application_Model_Mapper_BlogPost();
        $articles = $mapper->fetchAllActiveArticles();

        foreach ($articles as $article) {
            $entry = $feed->createEntry();
            $entry->setTitle($article->title);
            $entry->setLink('https://'.$this->_domain.'/'.$this->view->blog_url.'/'.$article->code);
            if ($article->byline) {
                $entry->addAuthor(array(
                    'name' => $article->byline
                ));
            }

            //since not all articles have correct dates
            $created_time = ($article->created != '0000-00-00 00:00:00')?strtotime($article->created):strtotime($article->posted_date);
            $modified_time = ($article->modified)?strtotime($article->modified):$created_time;

            $entry->setDateModified($modified_time);
            $entry->setDateCreated($created_time);
            $entry->setDescription(($article->summary)?$article->summary:$article->title);
            $entry->setContent($article->content);
            $feed->addEntry($entry);
        }

        $out = $feed->export('rss');

        echo $out;
        exit;
    }
}

