<?php
class ProductController extends Zend_Controller_Action
{
	function init()
	{
		$this->view->placeholder('body_class')->set("product-page");

		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$options = $bootstrap->getOptions();
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->_full_domain = (isset($options['site']['full_secure']))?$options['site']['full_secure']:null;
		if (isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
			$this->_full_domain = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
		}

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
		if( !$auth->hasIdentity() ){
			$userAuth = new Cny_UserAuth();
			$userAuth->storePreviousRequestUrl($_SERVER['REQUEST_URI']);

			$this->_customer = null;
		}else {
			$this->_customer = $auth->getIdentity();
		}

		$this->view->flashMessenger = $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
	}

    public function viewAction()
    {
        $settings_mapper = new Application_Model_Mapper_SiteSettingValue();

	    //announcement bar
	    $announcement_bar_status = $settings_mapper->getValueByCode('announcement_bar_status');
	    $announcement_bar_page = $settings_mapper->getValueByCode('announcement_bar_page');
	    if ($announcement_bar_status == 'enable' && $announcement_bar_page == '') {
		    $this->view->placeholder('_pre_header')->append($this->view->render("/html-build/partials/announcement_bar.phtml"));
	    }

	    //promotional pop-up
	    $promo_popup_status = $settings_mapper->getValueByCode('promo_popup_status');
	    $promo_popup_page = $settings_mapper->getValueByCode('promo_popup_page');
	    if ($promo_popup_status == 'Enabled' && ($promo_popup_page == '')) {
		    $promo_popup_modal_code = $settings_mapper->getValueByCode('promo_popup_code');
		    $promo_popup_delay = $settings_mapper->getValueByCode('promo_popup_delay');
		    $promo_popup_recurrence = $settings_mapper->getValueByCode('promo_popup_recurrence');
		    $extra_classes = $settings_mapper->getValueByCode('promo_popup_size');

		    $this->view->headScript()->appendScript('
$(document).ready(function(){
	window.cny.promoPopup("'.$promo_popup_modal_code.'", "'.$promo_popup_delay.'", "'.$promo_popup_recurrence.'", "'.$extra_classes.'");
});
');
	    }

        $product = null;
	    $code = $this->getRequest()->getParam('code','');
        $id = $this->getRequest()->getParam('id',$this->getRequest()->getParam('product_id',0));
	    $variant_id = $this->getRequest()->getParam('variant_id',0);

	    $mapper = new Application_Model_Mapper_Product();
	    if ($code) {
		    $this->view->product = $product = $mapper->findEnabledByCode($code);
	    }elseif ($id) {
		    $this->view->product = $product = $mapper->findEnabled($id);
	    }

        if ($product) {
            $mapper2 = new Application_Model_Mapper_ProductImage();
	        $this->view->primary_image = $mapper2->fetchPrimaryByProductId($product->id);
            if ($this->view->primary_image){
	            $this->view->primary_image->thumbnail_src = str_replace("userFiles/uploads", "userThumbs", $this->view->primary_image->image_src);
            }

	        $this->view->secondary_image = $mapper2->fetchSecondaryByProductId($product->id);
	        if ($this->view->secondary_image){
		        $this->view->secondary_image->thumbnail_src = str_replace("userFiles/uploads", "userThumbs", $this->view->secondary_image->image_src);
	        }

	        $this->view->nonpri_images = array();
            $nonpri_images = $mapper2->fetchNonPrimaryByProductId($product->id);
            if (!(empty($nonpri_images))) {
                foreach ($nonpri_images as $nonpri_image) {
                	$nonpri_image->thumbnail_src = str_replace("userFiles/uploads", "userThumbs", $nonpri_image->image_src);
	                $this->view->nonpri_images[] = $nonpri_image;
                }
            }

            //videos
	        $this->view->videos = array();
            $mapper = new Application_Model_Mapper_ProductVideo();
            $this->view->videos = $mapper->fetchOrderedByProductId($product->id);

	        //product profile
	        $mapper = new Application_Model_Mapper_ProductProfile();
	        $this->view->product_profile = $mapper->getProfileByProductId($product->id);

            //variant types
	        $mapper = new Application_Model_Mapper_ProductVariantType();
	        $this->view->variant_types = $mapper->fetchByProductId($product->id);

            $mapper3 = new Application_Model_Mapper_ProductVariant();
	        $this->view->variants = $variants = $mapper3->getVariantsByProductId($product->id);

	        if ($variant_id) {
	        	$this->view->active_variant = $active_variant = $mapper3->find($variant_id);

	        	if (!$active_variant || $active_variant->status != 'Enabled') {
	        		$this->redirect("/product/{$product->code}");
		        }

		        $mapper4 = new Application_Model_Mapper_ProductVariantImage();
		        $variant_primary_image = $mapper4->fetchPrimaryByProductVariantId($variant_id);

		        if ($variant_primary_image){
			        $variant_primary_image->thumbnail_src = str_replace("userFiles/uploads", "userThumbs", $variant_primary_image->image_src);
			        //make variant primary image the primary and move "product primary" to list of non-primary
			        array_unshift($this->view->nonpri_images, $this->view->primary_image);
			        $this->view->primary_image = $variant_primary_image;
		        }

		        $variant_secondary_image = $mapper4->fetchSecondaryByProductVariantId($variant_id);

		        if ($variant_secondary_image){
			        $variant_secondary_image->thumbnail_src = str_replace("userFiles/uploads", "userThumbs", $variant_secondary_image->image_src);
			        //make variant secondary image the secondary and move "product secondary" to list of non-primary
			        array_unshift($this->view->nonpri_images, $this->view->secondary_image);
			        $this->view->secondary_image = $variant_secondary_image;
		        }

		        //variant profile
		        $mapper = new Application_Model_Mapper_ProductVariantProfile();
		        $this->view->variant_profile = $mapper->getProfileByProductVariantId($variant_id);
	        }

            $mapper4 = new Application_Model_Mapper_ProductVariantImage();
            foreach($variants as $variant) {
                $variant_image = $mapper4->fetchPrimaryByProductVariantId($variant->id);

                if (!(empty($variant_image))) {
                	if ($variant_image->id != $this->view->primary_image->id) {
		                $variant_image->thumbnail_src = str_replace("userFiles/uploads", "userThumbs", $variant_image->image_src);
		                $this->view->nonpri_images[] = $variant_image;
	                }
                }
	            $nonpri_images = $mapper4->fetchNonPrimaryByProductVariantId($variant->id);
	            if (!(empty($nonpri_images))) {
		            foreach ($nonpri_images as $nonpri_image) {
			            $nonpri_image->thumbnail_src = str_replace("userFiles/uploads", "userThumbs", $nonpri_image->image_src);
			            $this->view->nonpri_images[] = $nonpri_image;
		            }
	            }

	            //videos
	            $mapper = new Application_Model_Mapper_ProductVariantVideo();
	            if (!(empty($mapper->fetchOrderedByProductVariantId($variant->id)))) {
		            foreach ($mapper->fetchOrderedByProductVariantId($variant->id) as $video) {
			            $this->view->videos[] = $video;
		            }
	            }
            }

            //category breadcrumb
	        $mapper = new Application_Model_Mapper_ProductCategory();
	        $this->view->parent_category = $mapper->fetchProductPrimaryCategory($product->id);

            //related products
	        $mapper = new Application_Model_Mapper_ProductRelated();
            $this->view->related = $mapper->fetchByProductId($product->id);

	        //recently viewed
	        $recently_viewed = json_decode((isset($_COOKIE['recently_viewed']) ? $_COOKIE['recently_viewed'] : ''));
	        $recently_viewed = (array) $recently_viewed;

	        if (!$recently_viewed) {
		        $recently_viewed = array();
	        }

	        array_unshift($recently_viewed, $product->id);
	        $recently_viewed = array_unique($recently_viewed);
	        array_slice($recently_viewed, 0, 13); //limit to 12 + current product (since being removed on display)
	        setcookie('recently_viewed', json_encode($recently_viewed), time() + (3600 * 24 * 365), '/');
	        //remove current product from display
	        $key = array_search($product->id, $recently_viewed);
	        if ($key !== false) {
		        unset($recently_viewed[$key]);
	        }
	        $this->view->recently_viewed = array();
	        $mapper = new Application_Model_Mapper_Product();
	        foreach ($recently_viewed as $k => $recent_id) {
		        $this->view->recently_viewed[] = $mapper->find($recent_id);
	        }


            $settings_mapper = new Application_Model_Mapper_SiteSettingValue();
            $site_name = $settings_mapper->getValueByCode('site_name');
            $this->view->headTitle($site_name, "SET");
            $this->view->headTitle()->prepend($product->title);
            if ($product->meta_keywords) {
	            $this->view->headMeta()->setName('keywords', $product->meta_keywords);
            }
            if ($product->meta_description) {
	            $this->view->headMeta()->setName('description', $product->meta_description);
            }

            //Open Graph tags
	        $this->view->doctype(Zend_View_Helper_Doctype::XHTML1_RDFA);
	        $this->view->headMeta()->setProperty('og:title', $product->title.' :: '.$site_name);
	        if ($product->meta_description) {
		        $this->view->headMeta()->setProperty('og:description', $product->meta_description);
	        }
	        if ($this->view->primary_image && $this->view->primary_image->image_src) {
		        $this->view->headMeta()->setProperty('og:image', $this->_full_domain . $this->view->primary_image->image_src);
	        }
	        //$this->view->headMeta()->setProperty('og:url', $this->_full_domain.'/product/');

            $this->view->placeholder('body_class')->append('dark');

            //reviews/ratings
	        if ($this->view->reviews_status = $settings_mapper->getValueByCode('product_reviews_status') == 'Enabled') {
	        	$this->view->reviews_star_color = $settings_mapper->getValueByCode('product_reviews_star_color');
	        	$this->view->reviews_location = $settings_mapper->getValueByCode('product_reviews_display_location');
		        $this->view->reviews_email_required = $settings_mapper->getValueByCode('product_reviews_email_required');
		        $this->view->reviews_captcha_status = $settings_mapper->getValueByCode('product_reviews_captcha_status');
		        $this->view->recaptcha_site_key = $settings_mapper->getValueByCode('recaptcha_site_key');
		        $this->view->recaptcha_secret_key = $settings_mapper->getValueByCode('recaptcha_secret_key');

		        if ($this->view->reviews_system = $settings_mapper->getValueByCode('product_reviews_system') == 'logic') { //the LOGIC (Cyber-NY) reviews system
		        	$mapper = new Application_Model_Mapper_ProductReview();


		        	if ($variant_id) {
		        		$this->view->rating = $mapper->fetchAggregateByProductVariantId($variant_id);
		        		$this->view->reviews = $reviews = $mapper->fetchReviewsByProductVariantId($variant_id);
			        }else {
				        $this->view->rating = $mapper->fetchAggregateByProductId($product->id);
				        $this->view->reviews = $reviews = $mapper->fetchReviewsByProductId($product->id);
			        }

		        }
	        }

	        //question/answers
	        if ($this->view->qa_status = $settings_mapper->getValueByCode('product_qas_status') == 'Enabled') {
		        $this->view->qa_location = $settings_mapper->getValueByCode('product_qas_display_location');
		        $this->view->qa_email_required = $settings_mapper->getValueByCode('product_qas_email_required');
		        $this->view->qa_captcha_status = $settings_mapper->getValueByCode('product_qas_captcha_status');
		        $this->view->recaptcha_site_key = $settings_mapper->getValueByCode('recaptcha_site_key');
		        $this->view->recaptcha_secret_key = $settings_mapper->getValueByCode('recaptcha_secret_key');

		        if ($this->view->qa_system = $settings_mapper->getValueByCode('product_qas_system') == 'logic') { //the LOGIC (Cyber-NY) reviews system
			        $mapper = new Application_Model_Mapper_ProductQa();
			        if ($variant_id) {
				        $this->view->qas = $mapper->fetchByProductVariantId($variant_id);
			        }else {
				        $this->view->qas = $mapper->fetchByProductId($product->id);
			        }
		        }
	        }

	        //check if in wishlist
	        if ($this->_customer && $this->_customer->id) {
	        	$mapper = new Application_Model_Mapper_CustomerWishlist();
	        	$this->view->in_wishlist = $mapper->inWishlist($this->_customer->id, $product->id, $variant_id);
	        }
        } else {
	        throw new Zend_Controller_Action_Exception('This page does not exist', 404);
        }



    }


}
