<?php
class ShipStationController extends Zend_Controller_Action
{
	public function init()
	{
		ini_set('memory_limit','256M');
		ini_set('max_execution_time', 600);

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$this->_helper->viewRenderer->setNoRender(true);
		$this->view->layout()->disableLayout();

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->_cfg_shipstation = [
			'username' => $settings_mapper->getValueByCode('shipstation_connect_username'),
			'password' => $settings_mapper->getValueByCode('shipstation_connect_password')
		];

		$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
		$this->_full_url = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];

		$this->_xml = new DOMDocument('1.0', 'utf-8');

		$this->_dropship = "0 DAY";//$options['preorder']['drop_date'];
	}

	public function indexAction()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$per_page = 100;

		$username = $this->getRequest()->getParam('SS-UserName','');
		$password = $this->getRequest()->getParam('SS-Password','');
		$action = $_GET['action'];//$this->getRequest()->getParam('action','');//ZF uses action internally so must use PHP GET

		/*
		//debug
		$mail_body = "
	Username: {$username} <br/>
	Password: {$password} <br/>
	Action: {$action} <br/>
	<hr/>
	Start Date: ".$this->getRequest()->getParam('start_date','')."<br/>
	End Date: ".$this->getRequest()->getParam('end_date','')."<br/>
	Page: ".$this->getRequest()->getParam('page','')."<br/>
	<hr/>
	Order ID: ".$this->getRequest()->getParam('order_number','')."<br/>
	Carrier: ".$this->getRequest()->getParam('carrier','')."<br/>
	Service: ".$this->getRequest()->getParam('service','')."<br/>
	Tracking Number: ".$this->getRequest()->getParam('tracking_number','')."<br/>
	<hr/><br/><br/>
	URL: {$_SERVER['REQUEST_URI']}<br/>
	";

		$emails = new Application_Model_Emails();
		$emails->generic($mail_body, "craig@cyber-ny.com", 'ShipStation Debug');

		if ($log = $this->getLog()) {
			$priority = Zend_Log::NOTICE;

			$log->log('ShipStation Debug', $priority);
			$log->log($mail_body, $priority);
			$log->log("---------------", $priority);
		}
		//end debug
		*/

		//validate info
		if ($username != $this->_cfg_shipstation['username'] || $password != $this->_cfg_shipstation['password']) {
			//exit;
		}

		if ($action == 'export') { // get orders
			$start_date = $this->getRequest()->getParam('start_date','');
			$end_date = $this->getRequest()->getParam('end_date','');
			$page = $this->getRequest()->getParam('page',1);

			//get orders
			$sql_start_date = $this->_db->quote(date("Y-m-d H:i:s", strtotime($start_date)));
			$sql_end_date = $this->_db->quote(date("Y-m-d H:i:s", strtotime($end_date)));
			$limit = ($per_page * $page) - $per_page;

			$xml_orders = $this->_xml->createElement('Orders');

			if ($page == 1) {
				$sql = "SELECT COUNT(DISTINCT(o.id)) 
FROM orders AS o 
LEFT JOIN order_items AS oi ON oi.order_id = o.id 
LEFT JOIN products AS p ON oi.product_id = p.id
LEFT JOIN product_variants AS pv ON oi.variant_id = pv.id  
WHERE o.created <= $sql_end_date AND oi.status = 'Order Taken' 
AND (IF (oi.variant_id IS NOT NULL, pv.inventory, p.inventory) > 0 OR IF (oi.variant_id IS NOT NULL, pv.inventory, p.inventory) IS NULL) 
AND (IF (oi.variant_id IS NOT NULL, pv.available_date, p.available_date) <= DATE_ADD(NOW(), INTERVAL $this->_dropship) OR IF (oi.variant_id IS NOT NULL, pv.available_date, p.available_date) IS NULL) 
AND IF (oi.variant_id IS NOT NULL, pv.warehouse, p.warehouse) = 'shipstation'"; //o.created >= $sql_start_date AND

				$total = $this->_db->fetchOne($sql);
				$total_pages = ceil($total / $per_page);

				$xml_orders->setAttribute('pages', $total_pages);
			}

			$sql = "SELECT o.* 
FROM orders AS o 
LEFT JOIN order_items AS oi ON oi.order_id = o.id 
LEFT JOIN products AS p ON oi.product_id = p.id
LEFT JOIN product_variants AS pv ON oi.variant_id = pv.id 
WHERE o.created <= $sql_end_date AND oi.status = 'Order Taken' 
AND (IF (oi.variant_id IS NOT NULL, pv.inventory, p.inventory) > 0 OR IF (oi.variant_id IS NOT NULL, pv.inventory, p.inventory) IS NULL) 
AND (IF (oi.variant_id IS NOT NULL, pv.available_date, p.available_date) <= DATE_ADD(NOW(), INTERVAL $this->_dropship) OR IF (oi.variant_id IS NOT NULL, pv.available_date, p.available_date) IS NULL) 
AND IF (oi.variant_id IS NOT NULL, pv.warehouse, p.warehouse) = 'shipstation'
GROUP BY o.id 
ORDER BY o.id LIMIT $limit, $per_page"; //o.created >= $sql_start_date AND

			$results = $this->_db->fetchAll($sql);

			foreach ($results as $row) {
				$xml_order = $this->orderXML($row['id']);

				if ($xml_order) {
					$xml_orders->appendChild($xml_order);
				}
			}

			$this->_xml->appendChild($xml_orders);

			if (!is_dir(realpath($_SERVER['DOCUMENT_ROOT'] . "/..")."/logs")) {
				mkdir(realpath($_SERVER['DOCUMENT_ROOT'] . "/..")."/logs");
				chmod(realpath($_SERVER['DOCUMENT_ROOT'] . "/..")."/logs", 0777);
			}
			if (!is_dir(realpath($_SERVER['DOCUMENT_ROOT'] . "/..")."/logs/warehouse")) {
				mkdir(realpath($_SERVER['DOCUMENT_ROOT'] . "/..")."/logs/warehouse");
				chmod(realpath($_SERVER['DOCUMENT_ROOT'] . "/..")."/logs/warehouse", 0777);
			}

			$xml_path = realpath($_SERVER['DOCUMENT_ROOT'] . "/../logs/warehouse");
			$this->_xml->save( $xml_path . '/shipstation-' . strtotime($start_date) . "-" . strtotime($end_date) . "-" . $page . ".xml");

			header("Content-type: application/xml; charset=utf-8");
			echo $this->_xml->saveXML();
		}elseif ($action == 'shipnotify') {
			$order_id = $this->getRequest()->getParam('order_number',0);
			$carrier = $this->getRequest()->getParam('carrier','');
			$service = $this->getRequest()->getParam('service','');
			$tracking_number = $this->getRequest()->getParam('tracking_number','');

			/*
<?xml version="1.0" encoding="utf-8"?>
<ShipNotice>
  <OrderNumber>ABC123</OrderNumber>
  <OrderID>123456</OrderID>
  <CustomerCode>customer@mystore.com</CustomerCode>
  <CustomerNotes></CustomerNotes>
  <InternalNotes></InternalNotes>
  <NotesToCustomer></NotesToCustomer>
  <NotifyCustomer></NotifyCustomer>
  <LabelCreateDate>12/8/2011 12:56</LabelCreateDate>
  <ShipDate>12/8/2011</ShipDate>
  <Carrier>USPS</Carrier>
  <Service>Priority Mail</Service>
  <TrackingNumber>1Z909084330298430820</TrackingNumber>
  <ShippingCost>4.95</ShippingCost>
  <CustomField1></CustomField1>
  <CustomField2></CustomField2>
  <CustomField3></CustomField3>
  <Recipient>
    <Name>The President</Name>
    <Company>US Govt</Company>
    <Address1>1600 Pennsylvania Ave</Address1>
    <Address2></Address2>
    <City>Washington</City>
    <State>DC</State>
    <PostalCode>20500</PostalCode>
    <Country>US</Country>
  </Recipient>
  <Items>
    <Item>
      <SKU>FD88821</SKU>
      <Name>My Product Name</Name>
      <Quantity>2</Quantity>
      <LineItemID>25590</LineItemID>
    </Item>
  </Items>
</ShipNotice>
		 */

			$dataPOST = trim(file_get_contents('php://input'));
			$xmlData = new SimpleXMLElement($dataPOST);

			$shipped_date = date("Y-m-d",strtotime($xmlData->ShipDate));

			$parts = explode("-",$xmlData->OrderID);
			if (count($parts) <= 2) {
				$order_id = $parts[0];
			}else {
				echo "error: Order ID invalid ({$xmlData->OrderID})";
				exit;
			}

			$products = array();
			$mapper = new Application_Model_Mapper_OrderItem();

			foreach ($xmlData->Items as $row) {
				foreach ($row->Item as $item) {
					$valid = $this->_db->update("order_items", array(
							'status' => 'Shipped',
							'tracking_code' => $xmlData->TrackingNumber,
							'shipped_date' => $shipped_date,
							'modified' => new Zend_Db_Expr("NOW()")
						),
						"order_id = '{$order_id}' AND id = '{$item->LineItemID}'"
					);

					if ($valid) {
						$products[] = $mapper->find((int)$item->LineItemID);
					}
				}
			}

			$this->_db->query("UPDATE orders SET STATUS = 'Shipped' WHERE (SELECT COUNT(*) FROM order_items AS oi WHERE oi.order_id = orders.id) = (SELECT COUNT(*) FROM order_items AS oi2 WHERE oi2.order_id = orders.id AND oi2.status = 'Shipped') AND orders.id = '{$order_id}' ");

			$xml_path = realpath($_SERVER['DOCUMENT_ROOT'] . "/../logs");
			$xmlData->asXml($xml_path .'/'. $xmlData->OrderID.'.xml');

			if ($settings_mapper->getValueByCode('shipping_notification_email_status') == 'Enabled') {
				$emails = new Application_Model_Emails();
				$emails->shippedNotification($order_id, $products);
			}

			echo "";
		}

		exit;
	}

	private function orderXML($order_id)
	{
		//order details
		$order_mapper = new Application_Model_Mapper_Order();
		$order = $order_mapper->find($order_id);

		//billing address
		$mapper = new Application_Model_Mapper_OrderAddress();
		$billing = $mapper->findByOrderId($order_id, 'billing');
		//shipping address
		$shipping = $mapper->findByOrderId($order_id, 'shipping');

		//items
		$item_mapper = new Application_Model_Mapper_OrderItem();
		$items = $item_mapper->findByOrderId($order_id);

		//shipping
		$mapper = new Application_Model_Mapper_OrderShipping();
		$shipping_method = $mapper->findByOrderId($order_id);

		//totals
		$mapper = new Application_Model_Mapper_OrderTotal();
		$db_totals = $mapper->findByOrderId($order_id);
		$totals = array();
		foreach ($db_totals as $row) {
			$totals[$row->code] = $row->amount;
		}

		$variant_mapper = new Application_Model_Mapper_ProductVariant();
		$product_mapper = new Application_Model_Mapper_Product();

		//check to see if any products to send to warehouse otherwise end so no record is created
		$item_count = 0;
		foreach ($items as $item) {
			if ($item->status != 'Order Taken') {
				continue;
			}
			if ($item->variant_id) {
				$product = $variant_mapper->find($item->variant_id);

			}else {
				$product = $product_mapper->find($item->product_id);
			}

			if (($product->available_date && strtotime($product->available_date) > strtotime("today + $this->_dropship")) || $product->inventory <= 0) {
				continue;
			}
			if (strtolower($product->warehouse) != 'shipstation') {
				continue;
			}

			$item_count++;
		}
		if ($item_count === 0) {
			return null;
		}

		//check for required fields and ignore if missing so no batch error and we can be notified
		if (
			(!$billing->first_name && !$billing->last_name) ||
			(!$shipping->first_name && !$shipping->last_name) ||
			!$shipping->address || !$shipping->city || !$shipping->region || !$shipping->postcode || !$shipping->country
		) {
			$mailbody = "<h4>Error with address: Order #$order_id</h4>
				<p>
				Billing First Name: {$billing->first_name}<br/>
				Billing Last Name: {$billing->last_name}<br/><br/>
				Shipping First Name: {$shipping->first_name}<br/>
				Shipping Last Name: {$shipping->last_name}<br/>
				Shipping Address: {$shipping->address}<br/>
				Shipping City: {$shipping->city}<br/>
				Shipping Region: {$shipping->region}<br/>
				Shipping Postcode: {$shipping->postcode}<br/>
				Shipping Country: {$shipping->country}<br/>
				</p>
			";

			$emails = new Application_Model_Emails();
			$emails->generic($mailbody, "support@cyber-ny.com", 'ShipStation Order Error', '');

			return null;
		}


		$xml_order = $this->_xml->createElement('Order');
		$xml_order->appendChild($this->_xml->createElement('OrderID'))->appendChild($this->_xml->createCDATASection($order->id.'-'.$order->counter));
		$xml_order->appendChild($this->_xml->createElement('OrderNumber'))->appendChild($this->_xml->createCDATASection($order->id));
		$xml_order->appendChild($this->_xml->createElement('OrderDate', date("m/d/Y H:i", strtotime($order->created)))); //time in UTC time. Format: MM/dd/yyyy HH:mm
		$xml_order->appendChild($this->_xml->createElement('OrderStatus'))->appendChild($this->_xml->createCDATASection($order->status));
		$xml_order->appendChild($this->_xml->createElement('LastModified', ($order->modified)?date("m/d/Y H:i", strtotime($order->modified)):date("m/d/Y H:i", strtotime($order->created)))); //time in UTC time. Format: MM/dd/yyyy HH:mm
		$xml_order->appendChild($this->_xml->createElement('ShippingMethod'))->appendChild($this->_xml->createCDATASection($shipping_method->method));
		//$xml_order->appendChild($this->_xml->createElement('PaymentMethod'))->appendChild($this->_xml->createCDATASection(''));
		$xml_order->appendChild($this->_xml->createElement('OrderTotal', $totals['total']));
		$xml_order->appendChild($this->_xml->createElement('TaxAmount', $totals['tax']));
		$xml_order->appendChild($this->_xml->createElement('ShippingAmount', $totals['shipping']));
		//$xml_order->appendChild($this->_xml->createElement('CustomerNotes'))->appendChild($this->_xml->createCDATASection(''));
		//$xml_order->appendChild($this->_xml->createElement('InternalNotes'))->appendChild($this->_xml->createCDATASection(''));
		//$xml_order->appendChild($this->_xml->createElement('Gift', ''));
		//$xml_order->appendChild($this->_xml->createElement('GiftMessage'))->appendChild($this->_xml->createCDATASection(''));
		//$xml_order->appendChild($this->_xml->createElement('CustomField1'))->appendChild($this->_xml->createCDATASection(''));
		//$xml_order->appendChild($this->_xml->createElement('CustomField2'))->appendChild($this->_xml->createCDATASection(''));
		//$xml_order->appendChild($this->_xml->createElement('CustomField3'))->appendChild($this->_xml->createCDATASection(''));
		//$xml_order->appendChild($this->_xml->createElement('RequestedWarehouse'))->appendChild($this->_xml->createCDATASection(''));
		//$xml_order->appendChild($this->_xml->createElement('Source'))->appendChild($this->_xml->createCDATASection(''));

		//customer data
		$xml_customer = $this->_xml->createElement('Customer');
		$xml_customer->appendChild($this->_xml->createElement('CustomerCode'))->appendChild($this->_xml->createCDATASection($order->email)); //customer email address
		$xml_order->appendChild($xml_customer);
		//billing info
		$xml_billing = $this->_xml->createElement('BillTo');
		$xml_billing->appendChild($this->_xml->createElement('Name'))->appendChild($this->_xml->createCDATASection($billing->first_name.' '.$billing->last_name)); //full name
		$xml_billing->appendChild($this->_xml->createElement('Company'))->appendChild($this->_xml->createCDATASection($billing->company));
		$xml_billing->appendChild($this->_xml->createElement('Phone'))->appendChild($this->_xml->createCDATASection($billing->phone));
		$xml_billing->appendChild($this->_xml->createElement('Email'))->appendChild($this->_xml->createCDATASection($order->email));
		$xml_customer->appendChild($xml_billing);
		//shipping
		$xml_shipping = $this->_xml->createElement('ShipTo');
		$xml_shipping->appendChild($this->_xml->createElement('Name'))->appendChild($this->_xml->createCDATASection($shipping->first_name.' '.$shipping->last_name)); //full name
		$xml_shipping->appendChild($this->_xml->createElement('Company'))->appendChild($this->_xml->createCDATASection($shipping->company));
		$xml_shipping->appendChild($this->_xml->createElement('Address1'))->appendChild($this->_xml->createCDATASection($shipping->address));
		$xml_shipping->appendChild($this->_xml->createElement('Address2'))->appendChild($this->_xml->createCDATASection($shipping->address2));
		$xml_shipping->appendChild($this->_xml->createElement('City'))->appendChild($this->_xml->createCDATASection($shipping->city));
		$xml_shipping->appendChild($this->_xml->createElement('State'))->appendChild($this->_xml->createCDATASection($shipping->region));
		$xml_shipping->appendChild($this->_xml->createElement('PostalCode'))->appendChild($this->_xml->createCDATASection($shipping->postcode));
		$xml_shipping->appendChild($this->_xml->createElement('Country'))->appendChild($this->_xml->createCDATASection($shipping->country));
		$xml_shipping->appendChild($this->_xml->createElement('Phone'))->appendChild($this->_xml->createCDATASection($shipping->phone));
		$xml_customer->appendChild($xml_shipping);
		//items
		$xml_items = $this->_xml->createElement('Items');
		//loop through items in order
		foreach ($items as $item) {
			if ($item->status != 'Order Taken') {
				continue;
			}

			if ($item->variant_id) {
				$product = $variant_mapper->find($item->variant_id);

			}else {
				$product = $product_mapper->find($item->product_id);
			}

			if (($product->available_date && strtotime($product->available_date) > strtotime("today + $this->_dropship")) || $product->inventory <= 0) {
				continue;
			}
			if (strtolower($product->warehouse) != 'shipstation') {
				continue;
			}

			$xml_item = $this->_xml->createElement('Item');
			$xml_item->appendChild($this->_xml->createElement('LineItemID'))->appendChild($this->_xml->createCDATASection($item->id)); //order item table id
			$xml_item->appendChild($this->_xml->createElement('SKU'))->appendChild($this->_xml->createCDATASection(($product->warehouse_sku)?$product->warehouse_sku:$product->sku));
			$xml_item->appendChild($this->_xml->createElement('Name'))->appendChild($this->_xml->createCDATASection($product->title));
			//$xml_item->appendChild($this->_xml->createElement('ImageUrl'))->appendChild($this->_xml->createCDATASection(''));
			$xml_item->appendChild($this->_xml->createElement('Weight', '5'));
			$xml_item->appendChild($this->_xml->createElement('WeightUnits', 'oz'));
			$xml_item->appendChild($this->_xml->createElement('Quantity', $item->qty));
			$xml_item->appendChild($this->_xml->createElement('UnitPrice', $item->paid_price)); //price of single unit

			/* //can add options
			$xml_options = $this->_xml->createElement('Options');
			$xml_option = $xml_options->createElement('Option'); //can loop option tag if multiple options
			$xml_option->appendChild($this->_xml->createElement('Name'))->appendChild($this->_xml->createCDATASection(''));
			$xml_option->appendChild($this->_xml->createElement('Value'))->appendChild($this->_xml->createCDATASection(''));
			$xml_option->appendChild($this->_xml->createElement('Weight', '')); //weight of option if adds to item weight
			$xml_options->appendChild($xml_option);
			$xml_item->appendChild($xml_options);
			*/

			$xml_items->appendChild($xml_item);

			$item_entity = $item_mapper->find($item->id);
			$item_entity->status = 'Processing';
			$item_mapper->save($item_entity);
		}
		//TODO - need to add discount as an item
		// but may not be needed since ShipStation isn't handling accounting...

		$xml_order->appendChild($xml_items);

		$order->counter++;
		$order_mapper->save($order);

		return $xml_order;
	}

	public function getLog()
	{
		$bootstrap = $this->getInvokeArg('bootstrap');
		if (!$bootstrap->hasPluginResource('Log')) {
			return false;
		}
		$log = $bootstrap->getResource('Log');
		return $log;
	}
}

