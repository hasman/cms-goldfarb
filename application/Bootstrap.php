<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initCacheMgr()
	{
		$this->bootstrap('cachemanager');
		$cachemanager = $this->getResource('cachemanager');
		Zend_Registry::set('cachemanager', $cachemanager);
	}

	protected function _initAutoload()
	{
		$autoloader = new Zend_Application_Module_Autoloader(
			array(
				'namespace' => 'Cny_',
				'basePath'  => dirname(__FILE__),
			)
		);
		return $autoloader;
	}

	protected function _initPagination()
	{
		Zend_Paginator::setDefaultScrollingStyle('Sliding');
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('pagination_control.phtml');
		Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH . '/helpers');
	}

/*
	protected function _initCustomRouter()
	{
		$this->bootstrap('FrontController');
		$this->bootstrap('cachemanager');
		$this->bootstrap('multidb');
		// @var Zend_Controller_Front $front //
		$front = $this->getResource('FrontController');
		// @var Zend_Controller_Router_Rewrite $router //
		$router = $front->getRouter();

		$svc = new Application_Service_RewriteRules();
		$rules = $svc->fetch();
		$route = new Cny_Controller_Router_Cms(['_rules' => $rules]);
		$router->addRoute('cms', $route);
	}
*/
	protected function _initWebServices()
	{
		putenv('GOOGLE_APPLICATION_CREDENTIALS='.APPLICATION_PATH.'/data/google_service_account.json');
		define('GOOGLE_APPLICATION_CREDENTIALS', getenv('GOOGLE_APPLICATION_CREDENTIALS'));
	}

	protected function _initDatabase()
	{
		$resource = $this->getPluginResource('multidb');
		Zend_Registry::set("multidb", $resource);
	}

	protected function _initConfig()
	{
		$options = $this->getOptions();
		$config = new Zend_Config($options);
		Zend_Registry::set('configuration', $config);
		return $config;
	}

	protected function _initViewHelpers()
	{
		$this->bootstrap('multidb');

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$site_name = $settings_mapper->getValueByCode('site_name');
		$meta_title = $settings_mapper->getValueByCode('meta_title');
		$meta_description = $settings_mapper->getValueByCode('meta_description');
		$meta_keywords = $settings_mapper->getValueByCode('meta_keywords');


		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();

		$view->headMeta()->appendName("keywords", ($meta_keywords)?$meta_keywords:'');
		$view->headMeta()->appendName("description", ($meta_description)?$meta_description:'');
		$view->headTitle($meta_title?$meta_title:$site_name);
		$view->headTitle()->setSeparator(' :: ');

		$view->headMeta()->appendName("platform", "LOGIC Business Cloud by Cyber-NY https://cyber-ny.com");

		$view->headScript()->appendFile("https://www.google.com/recaptcha/api.js",null,array("async"=>"true","defer"=>"true"));
	}

	protected function _initData()
	{
		$path = APPLICATION_PATH.'/configs/data.ini';
		if( file_exists($path) ){
			$options = $this->getOptions();
			$cache_dir = $cache_dir = $options['dir']['cache'];
			$frontendOptions = array('automatic_serialization' => true, 'lifetime' => 3600);
			$backendOptions = array('cache_dir' => $cache_dir);

			$cache_id = 'data_ini';
			$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

			if (!($cache->test($cache_id))) {
				$data = new Zend_Config_Ini($path);

				$cache->save($data, $cache_id, array('ini'));
			}else {
				$data = $cache->load($cache_id);
			}

			$regions = $data->get('region');
			$countries = $data->get('country');
			$languages = $data->get('language');

			Zend_Registry::set( 'regions', $regions );
			Zend_Registry::set( 'countries', $countries );
			Zend_Registry::set( 'languages', $languages );
		}
	}

	/*
	protected function _initS3()
	{
		$options = $this->getOptions();
		$config = new Zend_Config($options);

		$s3 = new Zend_Service_Amazon_S3($config->s3->key, $config->s3->secret);
		$s3->registerStreamWrapper("s3");
		Zend_Registry::set('s3', $s3);
	}

	protected function _initMediaSystem()
	{
		$options = $this->getOption('cny');
		if (!array_key_exists('media', $options)) {
			$options['media'] = array();
		}
		Cny_Media::setConfig($options['media']);
	}
*/

	protected function _initModuleList()
	{
		//CMS modules
		$path = APPLICATION_PATH.'/configs/modules.ini';
		if( file_exists($path) ){
			$data = new Zend_Config_Ini($path, 'modules', array('allowModifications'=>true));
			$modules = $data->get('cms_module');
		}
		Zend_Registry::set( 'cms_modules', $modules );
	}
	protected function _initNavigationList()
	{
		//CMS Navigation Templates
		$path = APPLICATION_PATH.'/configs/navigations.ini';
		if( file_exists($path) ){
			$data = new Zend_Config_Ini($path, 'navigations', array('allowModifications'=>true));
			$navs = $data->get('cms_navigation');
		}
		Zend_Registry::set( 'cms_navigations', $navs );
	}
	protected function _initFontList()
	{
		//CMS Font Options
		$path = APPLICATION_PATH.'/configs/fonts.ini';
		if( file_exists($path) ){
			$data = new Zend_Config_Ini($path, 'fonts', array('allowModifications'=>true));
			$fonts = $data->get('cms_font');
		}
		Zend_Registry::set( 'cms_fonts', $fonts );
	}
	protected function _initLayouts()
	{
		//CMS Layouts/templates
		$path = APPLICATION_PATH.'/configs/layouts.ini';
		if( file_exists($path) ){
			$data = new Zend_Config_Ini($path, 'layouts', array('allowModifications'=>true));
			$layouts = $data->get('cms_layout');
		}
		Zend_Registry::set( 'cms_layouts', $layouts );
	}
	protected function _initCronTasks()
	{
		//CMS Font Options
		$path = APPLICATION_PATH.'/configs/cron_tasks.ini';
		if( file_exists($path) ){
			$data = new Zend_Config_Ini($path, 'cron_tasks', array('allowModifications'=>true));
			$cron_tasks = $data->get('cron_task');
		}
		Zend_Registry::set( 'cron_tasks', $cron_tasks );
	}

	protected function _initRouter()
	{
		$this->bootstrap('multidb');
		$this->bootstrap('FrontController');
		$front = $this->getResource('FrontController');

		if (php_sapi_name() == 'cli') {
			$front->setParam('disableOutputBuffering', true);
			$front->setRouter(new Cny_Controller_Router_Cli());
			$front->setRequest(new Zend_Controller_Request_Simple());
		} else {
			$router = $front->getRouter();

			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$sql = "SELECT code FROM pages";
			$static_pages = $db->fetchAll($sql);

			foreach ($static_pages as $k => $v) {
				$code = $v['code'];
				$route = new Zend_Controller_Router_Route_Static('/'.$code, [
					'controller' => 'index', 'action' => 'page', 'code' => $code
				]);
				$router->addRoute($code, $route);
			}

			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
			$blog_status = ($settings_mapper->getValueByCode('blog_status'))?$settings_mapper->getValueByCode('blog_status'):'Enabled';
			$blog_url = ($settings_mapper->getValueByCode('blog_url'))?$settings_mapper->getValueByCode('blog_url'):'blog';

			if ($blog_status == 'Enabled') {
				/*
				 * Blog Main Paqe
				 */
				$route = new Zend_Controller_Router_Route(
					$blog_url,
					['module' => 'default', 'controller' => 'blog', 'action' => 'index']
				);
				$router->addRoute('blog_main_page', $route);

				/*
				 * Blog Tags
				 */
				$route = new Zend_Controller_Router_Route(
					$blog_url . '/tag/:tag',
					['module' => 'default', 'controller' => 'blog', 'action' => 'tag']
				);
				$router->addRoute('blog_tag', $route);

				/*
				 * Blog Articles
				 */
				$route = new Zend_Controller_Router_Route(
					$blog_url . '/:code',
					['module' => 'default', 'controller' => 'blog', 'action' => 'article']
				);
				$router->addRoute('blog_code', $route);

				/*
				 * Blog Search
				 */
				$route = new Zend_Controller_Router_Route_Static(
					$blog_url . '/search', ['module' => 'default', 'controller' => 'blog', 'action' => 'search']
				);
				$router->addRoute('blog_search', $route);

				/*
				* Blog RSS Feed
				*/
				$route = new Zend_Controller_Router_Route_Static(
					$blog_url . '/rss', ['module' => 'default', 'controller' => 'blog', 'action' => 'rss']
				);
				$router->addRoute('blog_rss_feed', $route);
			}

			$ecommerce_status = ($settings_mapper->getValueByCode('ecommerce_shop_page_status'))?$settings_mapper->getValueByCode('ecommerce_shop_page_status'):'Disabled';
			$ecommerce_url = ($settings_mapper->getValueByCode('ecommerce_url'))?$settings_mapper->getValueByCode('ecommerce_url'):'shop';
			if ($ecommerce_status == 'Enabled') {
				/*
				 * Commerce Main Paqe
				 */
				$route = new Zend_Controller_Router_Route(
					$ecommerce_url,
					['module' => 'default', 'controller' => 'category', 'action' => 'list']
				);
				$router->addRoute('ecommerce_main_page', $route);
			}

			$faq_status = ($settings_mapper->getValueByCode('faq_status'))?$settings_mapper->getValueByCode('faq_status'):'Disabled';
			$faq_url = ($settings_mapper->getValueByCode('faq_url'))?$settings_mapper->getValueByCode('faq_url'):'faq';
			if ($faq_status == 'Enabled') {
				/*
				 * FAQ Main Paqe
				 */
				$route = new Zend_Controller_Router_Route(
					$faq_url,
					['module' => 'default', 'controller' => 'faq', 'action' => 'index']
				);
				$router->addRoute('faq_main_page', $route);
			}

			$videos_status = ($settings_mapper->getValueByCode('videos_status'))?$settings_mapper->getValueByCode('videos_status'):'Disabled';
			$video_url = ($settings_mapper->getValueByCode('video_url'))?$settings_mapper->getValueByCode('video_url'):'video';
			if ($videos_status == 'Enabled') {
				/*
				 * Video Main Paqe
				 */
				$route = new Zend_Controller_Router_Route(
					$video_url,
					['module' => 'default', 'controller' => 'video', 'action' => 'index']
				);
				$router->addRoute('video_main_page', $route);

				/*
				 * Video Categories
				 */
				$route = new Zend_Controller_Router_Route(
					$video_url.'/category/:code',
					['module' => 'default', 'controller' => 'video', 'action' => 'category']
				);
				$router->addRoute('video_category', $route);

				/*
				 * Video Tags
				 */
				$route = new Zend_Controller_Router_Route(
					$video_url.'/tag/:tag',
					['module' => 'default', 'controller' => 'video', 'action' => 'tag']
				);
				$router->addRoute('video_tag', $route);
			}

			$instalinks_status = ($settings_mapper->getValueByCode('instalinks_status'))?$settings_mapper->getValueByCode('instalinks_status'):'Disabled';
			$instalinks_url = ($settings_mapper->getValueByCode('instalinks_url'))?$settings_mapper->getValueByCode('instalinks_url'):'instagram-links';
			if ($instalinks_status == 'Enabled') {
				/*
				 * Instalinks Main Paqe
				 */
				$route = new Zend_Controller_Router_Route(
					$instalinks_url,
					['module' => 'default', 'controller' => 'instalink', 'action' => 'index']
				);
				$router->addRoute('instalinks_main_page', $route);
			}

			$lvp_status = ($settings_mapper->getValueByCode('video_platform_status'))?$settings_mapper->getValueByCode('video_platform_status'):'Disabled';
			$lvp_url = $video_url = ($settings_mapper->getValueByCode('video_platform_url'))?$settings_mapper->getValueByCode('video_platform_url'):'videos';
			if ($lvp_status == 'Enabled') {
				/*
				 * LVP Video Paqe
				 */
				$route = new Zend_Controller_Router_Route(
					$lvp_url.'/:code',
					['module' => 'default', 'controller' => 'lvp', 'action' => 'view']
				);
				$router->addRoute('lvp_main_page', $route);

				if ($settings_mapper->getValueByCode('zype_api_key_admin') != '' || $settings_mapper->getValueByCode('zype_api_key_read_only') != '') {
					/*
					 * LVP Zype Video Paqe
					 */
					$route = new Zend_Controller_Router_Route(
						$lvp_url . '/:friendly_title/:zype_id',
						['module' => 'default', 'controller' => 'lvp', 'action' => 'zype']
					);
					$router->addRoute('lvp_zype_page', $route);

					/*
					 * LVP Zype Video Categories
					 */
					$route = new Zend_Controller_Router_Route(
						$lvp_url.'/category/:friendly_title/:zype_id',
						['module' => 'default', 'controller' => 'lvp-category', 'action' => 'zype']
					);
					$router->addRoute('lvp_zype_category', $route);
				}

				/*
				 * All Video
				 */
				$route = new Zend_Controller_Router_Route(
					$lvp_url.'/all-videos',
					['module' => 'default', 'controller' => 'lvp-category', 'action' => 'all']
				);
				$router->addRoute('lvp_all_videos', $route);

				/*
				 * Video Categories
				 */
				$route = new Zend_Controller_Router_Route(
					$lvp_url.'/category/:code',
					['module' => 'default', 'controller' => 'lvp-category', 'action' => 'view']
				);
				$router->addRoute('lvp_category', $route);

				/*
				 * Video Tags
				 */
				$route = new Zend_Controller_Router_Route(
					$lvp_url.'/tag/:code',
					['module' => 'default', 'controller' => 'lvp-tag', 'action' => 'view']
				);
				$router->addRoute('lvp_tag', $route);

				/*
				 * Video Genre
				 */
				$route = new Zend_Controller_Router_Route(
					$lvp_url.'/genre/:code',
					['module' => 'default', 'controller' => 'lvp-genre', 'action' => 'view']
				);
				$router->addRoute('lvp_genre', $route);
			}

			/*
			* Login
			*/
			$route = new Zend_Controller_Router_Route_Static(
				'login', ['module' => 'default', 'controller' => 'auth', 'action' => 'login']
			);
			$router->addRoute('login', $route);

			/*
			* Register
			*/
			$route = new Zend_Controller_Router_Route_Static(
				'register', ['module' => 'default', 'controller' => 'auth', 'action' => 'register']
			);
			$router->addRoute('register', $route);

			/*
			 * Product Detail Page
			 */
			$route = new Zend_Controller_Router_Route(
				'product/:code',
				['module' => 'default', 'controller' => 'product', 'action' => 'view']
			);
			$router->addRoute('product_detail', $route);

			/*
			 * Category Detail Page
			 */
			$route = new Zend_Controller_Router_Route(
				'category/:code',
				['module' => 'default', 'controller' => 'category', 'action' => 'view']
			);
			$router->addRoute('categorydetail', $route);

			/*
			 * Category Detail Page
			 */
			$route = new Zend_Controller_Router_Route(
				'category/:parent_code/:code',
				['module' => 'default', 'controller' => 'category', 'action' => 'view']
			);
			$router->addRoute('subcategorydetail', $route);

			/*
			 * Payment Form Review Page
			 */
			$route = new Zend_Controller_Router_Route(
				'payment-review',
				['module' => 'default', 'controller' => 'index', 'action' => 'payment-review']
			);
			$router->addRoute('paymentreview', $route);

			/*
			 * Payment Form Review Page
			 */
			$route = new Zend_Controller_Router_Route(
				'payment-confirmation',
				['module' => 'default', 'controller' => 'index', 'action' => 'payment-confirmation']
			);
			$router->addRoute('paymentconfirmation', $route);

			/*
			 * Collection Detail Page
			 */
			$route = new Zend_Controller_Router_Route(
				'collection/:code',
				['module' => 'default', 'controller' => 'collection', 'action' => 'view']
			);
			$router->addRoute('collectiondetail', $route);
		}
	}

	protected function _initEvents()
	{
		$this->bootstrap('log');
		$log = $this->getResource('log');
		$log->info("Bootstraped");
		$events = Zend_EventManager_StaticEventManager::getInstance();

		$callback = array('Application_Service_SearchIndex', 'pageChanged');
		$events->attach(
			'Admin_Model_Mapper_Page',
			'save.post',
			$callback
		);

		$callback = array('Application_Service_SearchIndex', 'blogChanged');
		$events->attach(
			'Application_Model_Mapper_BlogPost',
			'save.post',
			$callback
		);

		$callback = array('Application_Service_SearchIndex', 'productChanged');
		$events->attach(
			'Admin_Model_Mapper_Product',
			'save.post',
			$callback
		);
	}

	//This should go at the end of file so it overrides other ini sets?
	protected function _initCustom()
	{
		$path = APPLICATION_PATH.'/configs/custom.ini';
		if( file_exists($path) ){
			$full_data = new Zend_Config_Ini($path);

			Zend_Registry::set( 'custom_configuration', $full_data );

			//modules
			if (isset($full_data->modules)) {
				$modules = $full_data->modules->cms_module;

				if ($modules) {
					$cms_modules = Zend_Registry::get('cms_modules');
					Zend_Registry::set('cms_modules', array_merge_recursive($cms_modules->toArray(), $modules->toArray()));
				}
			}

			//navigation templates
			if (isset($full_data->navigations)) {
				$navs = $full_data->navigations->cms_navigation;

				if ($navs) {
					$cms_navs = Zend_Registry::get('cms_navigations');
					Zend_Registry::set('cms_navigations', array_merge_recursive($cms_navs->toArray(), $navs->toArray()));
				}
			}

			//fonts
			if (isset($full_data->fonts)) {
				$fonts = $full_data->fonts->cms_font;

				if ($fonts) {
					$cms_fonts = Zend_Registry::get('cms_fonts');
					Zend_Registry::set('cms_fonts', array_merge_recursive($cms_fonts->toArray(), $fonts->toArray()));
				}
			}

			//cron tasks
			if (isset($full_data->cron_tasks)) {
				$cron_tasks = $full_data->cron_tasks->cron_task;

				if ($cron_tasks) {
					$cms_cron_tasks = Zend_Registry::get('cron_tasks');
					Zend_Registry::set('cron_tasks', array_merge_recursive($cms_cron_tasks->toArray(), $cron_tasks->toArray()));
				}
			}

			//layouts/templates
			if (isset($full_data->layouts)) {
				$layouts = $full_data->layouts->cms_layout;

				if ($layouts) {
					$cms_layouts = Zend_Registry::get('cms_layouts');
					Zend_Registry::set('cms_layouts', array_merge_recursive($cms_layouts->toArray(), $layouts->toArray()));
				}
			}

			if (php_sapi_name() != 'cli') {
				//custom routers
				$config = new Zend_Config_Ini($path, 'production');
				if ($config->get('routes')) {
					$front = $this->getResource('FrontController');
					$router = $front->getRouter();
					$router->addConfig($config, 'routes');
				}
			}

			return;
		}else {
			Zend_Registry::set( 'custom_configuration', array() );
		}
	}

	protected function _initSMTP()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		if ($settings_mapper->getValueByCode('smtp_relay_status', 'Disabled') == 'Enabled') {
			$tr = new Zend_Mail_Transport_Smtp($settings_mapper->getValueByCode('smtp_relay_host'),
				[
					'auth' => $settings_mapper->getValueByCode('smtp_relay_auth'),
					'ssl' => $settings_mapper->getValueByCode('smtp_relay_ssl'),
					'port' => $settings_mapper->getValueByCode('smtp_relay_port'),
					'username' => $settings_mapper->getValueByCode('smtp_relay_username'),
					'password' => $settings_mapper->getValueByCode('smtp_relay_password'),
				]
			);
			Zend_Mail::setDefaultTransport($tr);
		}
	}
}
