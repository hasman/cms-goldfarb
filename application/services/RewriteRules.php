<?php

use Cocur\Slugify\Slugify;

class Application_Service_RewriteRules
{
	/** @var Zend_Cache_Core */
	private $cache;
	/** @var Cocur\Slugify\Slugify */
	private $slugify;

	public function fetch()
	{
		$output = [];
		if (!Zend_Registry::isRegistered('cachemanager')) {
			return $output;
		}
		$manager = Zend_Registry::get('cachemanager');
		if ($manager->hasCache('config')) {
			$this->cache = $manager->getCache('config');
		} else {
			$this->cache = false;
		}

		$this->slugify = new Slugify(['separator' => '_']);
		$cache_id = $this->slugify->slugify('cms_rewrite_rules');
		if (APPLICATION_ENV == 'development') {
			$this->cache->remove($cache_id);
		}
		if (!$this->cache->test($cache_id)) {
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select();
			$select
				->from(['rr' => 'rewrite_rules'], ['path', 'module', 'controller', 'action', 'type', '_param', ])
				->where('status = ?', 'Enabled')
				->order('sort_order')
				->order('created')
			;
			$output = $db->fetchAssoc($select);
			$this->cache->save($output, $cache_id);
		} else {
			$output = $this->cache->load($cache_id);
		}
		return $output;
	}
}