<?php

class Application_Service_GoogleAnalytics
{
	/** @var Zend_Cache_Core */
	private $cache;
	/** @var Google_Client */
	private $client;
	/** @var Google_Service_Analytics */
	private $analytics;
	/** @var array */
	private $options;
	private $default_view_id;

	public function __construct()
	{
		$manager = Zend_Registry::get('cachemanager');
		if ($manager->hasCache('google_analytics')) {
			$this->cache = $manager->getCache('google_analytics');
		} else {
			$this->cache = false;
		}

		$site_settings = new Application_Model_Mapper_SiteSettingValue();
		$default_view_id = $site_settings->getValueByCode('dashboard_view_id');
		$this->default_view_id = !empty($default_view_id)?'ga:'.$default_view_id:'';

		$this->client = new Google_Client();
		$this->client->setApplicationName("CNY Site Manager Analytics Reporting");
		$this->client->setAuthConfig(GOOGLE_APPLICATION_CREDENTIALS);
		$this->client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
		$this->analytics = new Google_Service_Analytics($this->client);
	}

	public function report($startdate, $enddate, $dimensions = [], $metrics = [], $filters = [], $sorts = [], $opts = [], $max_results = 365)
	{
		$output = $options = $signature = [];

		if (isset($opts['view_id'])) {
			$ga_view_id = $opts['view_id'];
		} else {
			$ga_view_id = $this->default_view_id;
		}

		$signature['ga_view_id'] = $ga_view_id;

		$signature['start_date'] = $startdate;
		$signature['end_date'] = $enddate;

		$dimensions = implode( ",", $dimensions );
		$signature['dimensions'] = $dimensions;

		$metrics = implode( ",", $metrics );
		$signature['metrics'] = $metrics;

		if (count($filters) > 0) {
			$signature['filters'] = implode('&', $filters);
		}
		$signature['sorts'] = implode(',', $sorts);
		$signature['max-results'] = $max_results;
		$signature['samplingLevel'] = 'HIGHER_PRECISION';

		try{
			if (!empty($dimensions)) {
				$options['dimensions'] = $dimensions;
			}
			if (count($filters) > 0) {
				$options['filters'] = implode('&', $filters);
			}
			if (!empty($sorts)) {
				$options['sort'] = implode(',', $sorts);
			}
			$options['max-results'] = $max_results;
			$options['samplingLevel'] = 'HIGHER_PRECISION';
			$options['output'] = 'dataTable';

			ksort($signature);
			$cache_id = sha1(http_build_query($signature));

			if (($data = $this->cache->load($cache_id)) === false) {
				$data = $this->analytics->data_ga->get(
					$ga_view_id, $startdate, $enddate,
					$metrics, $options
				);
				$this->cache->save($data, $cache_id, ['stats']);
			}

			$output = $data;
		} catch( Google_Service_Exception $ex ) {
//			print_r($ex);
		}
		return $output;
	}
}