<?php
class Application_Service_SearchIndex
{
	static function pageChanged(Zend_EventManager_Event $e)
	{
		$mapper = new Application_Model_Mapper_SearchIndex();
		if ($e->getName() == 'save.post') {
			$params = $e->getParams();
			$url = '/'.$params['entity']->code;

			/** @var Application_Model_SearchIndex $entity */
			$entity = $mapper->doesUrlExists($url);

			if ($params['entity']->status == 'Disabled') {
				if ($entity) {
					$entity = $mapper->delete($entity);
				}
			} else {
				$html_build = new Cny_Model_HtmlBuild();

				if (!$entity) {
					$entity = $mapper->blank(['url' => $url]);
				}
				$title = $params['entity']->meta_title;
				if (empty($title)) {
					$title = $params['entity']->title;
					if (empty($title)) {
						$title = '[UNTITLED]';
					}
				}
				$entity->title = $title;

				$keywords = $params['entity']->meta_keywords;
				if (empty($keywords)) {
					$keywords = '';
				}
				$entity->keywords = $keywords;

				$description = $params['entity']->meta_description;
				if (empty($description)) {
					$description = '';
				}
				$entity->description = $description;

				// Get the content
				$text = $html_build->buildPage($params['output'], false);
				$text = trim(strip_tags($text));
				$entity->content = preg_replace('!\s+!', ' ', $text);
				try {
					$mapper->save($entity);
				} catch(Exception $e) {
				}
			}
		}
	}

	static function blogChanged(Zend_EventManager_Event $e)
	{
		$mapper = new Application_Model_Mapper_SearchIndex();
		if ($e->getName() == 'save.post') {
			$params = $e->getParams();
			$url = '/blog/'.$params['entity']->code;

			/** @var Application_Model_SearchIndex $entity */
			$entity = $mapper->doesUrlExists($url);

			if ($params['entity']->status == 'Disabled') {
				if ($entity) {
					$mapper->delete($entity);
				}
			} else {
				if (!$entity) {
					$entity = $mapper->blank(['url' => $url]);
				}
				$title = $params['entity']->title;
				if (empty($title)) {
					$title = '[UNTITLED]';
				}
				$entity->title = $title;

				$keywords = $params['entity']->meta_keywords;
				if (empty($keywords)) {
					$keywords = '';
				}
				$entity->keywords = $keywords;

				$description = $params['entity']->meta_description;
				if (empty($description)) {
					$description = strip_tags($params['entity']->summary);
					if (empty($description)) {
						$description = '';
					}
				}
				$entity->description = $description;

				// Get the content
				$text = $params['entity']->content;
				$text = trim(strip_tags($text));
				$entity->content = preg_replace('!\s+!', ' ', $text);
				try {
					$mapper->save($entity);
				} catch(Exception $e) {
				}
			}
		}
	}

	static function productChanged(Zend_EventManager_Event $e)
	{
		$mapper = new Application_Model_Mapper_SearchIndex();
		if ($e->getName() == 'save.post') {
			$params = $e->getParams();
			$url = '/product/'.$params['entity']->code;

			/** @var Application_Model_SearchIndex $entity */
			$entity = $mapper->doesUrlExists($url);

			if ($params['entity']->status == 'Disabled') {
				if ($entity) {
					$mapper->delete($entity);
				}
			} else {
				if (!$entity) {
					$entity = $mapper->blank(['url' => $url]);
				}
				$title = $params['entity']->title;
				if (empty($title)) {
					$title = '[UNTITLED]';
				}
				$entity->title = $title;

				$keywords = $params['entity']->meta_keywords;
				if (empty($keywords)) {
					$keywords = '';
				}
				$entity->keywords = $keywords;

				$description = $params['entity']->meta_description;
				if (empty($description)) {
					$description = strip_tags($params['entity']->summary);
					if (empty($description)) {
						$description = '';
					}
				}
				$entity->description = $description;

				// Get the content
				$text = $params['entity']->description;
				$text = trim(strip_tags($text));
				$entity->content = preg_replace('!\s+!', ' ', $text);
				if (empty($entity->content)) {
					$entity->content = '';
				}
				try {
					$mapper->save($entity);
				} catch(Exception $e) {
				}
			}
		}
	}
}
