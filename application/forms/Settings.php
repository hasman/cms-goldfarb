<?php
class Application_Form_Settings extends Zend_Form
{

	public function init()
	{
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('SiteDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'account/forms/settings.phtml' ) ) ) );

		//Form
		$this->setMethod( 'post' );
		$this->setAction( '/account/settings-save' );
		$this->setAttrib( 'id', 'settingsForm' );

		//Form fields
		$elements['first_name'] = $this->createElement( 'text', 'first_name' )
			->setLabel('First Name')
			->setRequired(true)
			->setAttribs(array('class' => 'form-control','placeholder'=>'First Name'))
			->setDecorators( $decor_element );
		$elements['last_name'] = $this->createElement( 'text', 'last_name' )
			->setLabel('Last Name')
			->setRequired(true)
			->setAttribs(array('class' => 'form-control','placeholder'=>'Last Name'))
			->setDecorators( $decor_element );
		$elements['email'] = $this->createElement( 'text', 'email' )
			->setLabel('Email')
			->setRequired(true)
			->addValidator('EmailAddress')
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Email'))
			->setDecorators( $decor_element );
		$elements['password'] = $this->createElement( 'password', 'password' )
			->setLabel('New Password')
			->setRequired(false)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'New Password'))
			->setDecorators( $decor_element );
		$elements['v_password'] = $this->createElement( 'password', 'v_password' )
			->setLabel('Confirm New Password')
			->setRequired(false)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Confirm New Password'))
			->setDecorators( $decor_element );

		$elements['status'] = $this->createElement( 'select', 'status' )
			->setLabel('Account Status')
			->setRequired(true)
			->setMultiOptions(
				array(
					'Enabled' => 'Account Active',
					'Disabled' => "Disable Account"
				)
			)
			->setAttribs(array('class'=> 'form-control'))
			->setDecorators( $decor_element );

		$elements['submit'] = $this->createElement( 'submit', 'submit')
			->setAttribs(array('class' => 'btn btn-orange btn-checkout'))
			->setValue('Submit')
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}

}
