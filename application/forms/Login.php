<?php
/**
* Login Zend Form
*/
class Application_Form_Login extends Zend_Form
{

	public function init()
	{
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('SiteDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'auth/login.phtml' ) ) ) );

		/* Generate the Form */
		$this->setName('login')
			->setAction('/auth/identify')
			->setMethod('post')
			->setAttrib('id', 'login');

		//Form Fields
		$elements['email'] = $this->createElement( 'text', 'email' )
			->setRequired(true)
			->addValidator('EmailAddress')
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Email'))
			->setDecorators( $decor_element );
		$elements['password'] = $this->createElement( 'password', 'password' )
			->setRequired(true)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Password'))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}
}
