<?php
class Application_Form_Checkout extends Zend_Form
{

	public function init()
	{
		$billing = new Zend_Form_SubForm();
		$billing->addElements(array(
			new Zend_Form_Element_Text('first_name', array(
				'required'   => true,
				'label'      => 'First Name',
				'ErrorMessages'     => array('Please enter billing first name')
			)),
			new Zend_Form_Element_Text('last_name', array(
				'required'   => true,
				'label'      => 'Last Name',
				'ErrorMessages'     => array('Please enter billing last name')
			)),
			new Zend_Form_Element_Text('address', array(
				'required'   => true,
				'label'      => 'Address',
				'ErrorMessages'     => array('Please enter billing address')
			)),
			new Zend_Form_Element_Text('address2', array(
				'required'   => false,
				'label'      => 'Address',
				'ErrorMessages'     => array('Please enter billing address')
			)),
			new Zend_Form_Element_Text('city', array(
				'required'   => true,
				'label'      => 'City',
				'ErrorMessages'     => array('Please enter billing city')
			)),
			new Zend_Form_Element_Text('region', array(
				'required'   => true,
				'label'      => 'State',
				'ErrorMessages'     => array('Please select a billing state or enter a billing region')
			)),
			new Zend_Form_Element_Text('postcode', array(
				'required'   => true,
				'label'      => 'Zip',
				//'validators' => array('PostCode'),
				'ErrorMessages'     => array('Please enter billing zip code')
			)),
			new Zend_Form_Element_Text('country', array(
				'required'   => true,
				'label'      => 'Country',
				'ErrorMessages'     => array('Please select a billing country')
			)),
			new Zend_Form_Element_Text('email', array(
				'required'   => true,
				'label'      => 'Email',
				'validators' => array('EmailAddress'),
				'ErrorMessages'     => array('Please enter a valid billing email address')
			)),
			new Zend_Form_Element_Text('phone', array(
				'required'   => false,
				'label'      => 'Phone',
				'ErrorMessages'     => array('Please enter a valid billing phone number')
			)),
		));

		$shipping = new Zend_Form_SubForm();
		$shipping->addElements(array(
			new Zend_Form_Element_Text('first_name', array(
				'required'   => true,
				'label'      => 'First Name',
				'ErrorMessages'     => array('Please enter shipping first name')
			)),
			new Zend_Form_Element_Text('last_name', array(
				'required'   => true,
				'label'      => 'Last Name',
				'ErrorMessages'     => array('Please enter shipping last name')
			)),
			new Zend_Form_Element_Text('address', array(
				'required'   => true,
				'label'      => 'Address',
				'ErrorMessages'     => array('Please enter shipping address')
			)),
			new Zend_Form_Element_Text('address2', array(
				'required'   => false,
				'label'      => 'Address',
				'ErrorMessages'     => array('Please enter shipping address')
			)),
			new Zend_Form_Element_Text('city', array(
				'required'   => true,
				'label'      => 'City',
				'ErrorMessages'     => array('Please enter shipping city')
			)),
			new Zend_Form_Element_Text('region', array(
				'required'   => true,
				'label'      => 'State',
				'ErrorMessages'     => array('Please select a shipping state or enter a shipping region')
			)),
			new Zend_Form_Element_Text('postcode', array(
				'required'   => true,
				'label'      => 'Zip',
				//'validators' => array('PostCode'),
				'ErrorMessages'     => array('Please enter shipping zip code')
			)),
			new Zend_Form_Element_Text('country', array(
				'required'   => true,
				'label'      => 'Country',
				'ErrorMessages'     => array('Please select a shipping country')
			)),
			new Zend_Form_Element_Text('phone', array(
				'required'   => false,
				'label'      => 'Phone',
				'ErrorMessages'     => array('Please enter a valid shipping phone number')
			)),
		));

		$payment = new Zend_Form_SubForm();
		$payment->addElements(array(
			new Zend_Form_Element_Text('shipping_method', array(
				'required'   => true,
				'label'      => 'Shipping Method',
				'ErrorMessages'     => array('Please select a shipping method')
			)),
			new Zend_Form_Element_Text('name', array(
				'required'   => true,
				'label'      => 'Name',
				'ErrorMessages'     => array('Please enter a name')
			)),
			new Zend_Form_Element_Text('cc_num', array(
				'required'   => true,
				'label'      => 'Credit Card Number',
				'validators' => array('CreditCard'),
				'ErrorMessages'     => array('Credit Card number missing or invalid')
			)),
			new Zend_Form_Element_Text('ccv', array(
				'required'   => true,
				'label'      => 'CCV',
				'validators' => array('Digits'),
				'ErrorMessages'     => array('CCV code missing or invalid')
			)),
			new Zend_Form_Element_Text('exp_month', array(
				'required'   => true,
				'label'      => 'Expiration Month',
				'validators' => array('Digits'),
				'ErrorMessages'     => array('Credit Card expiration required')
			)),
			new Zend_Form_Element_Text('exp_year', array(
				'required'   => true,
				'label'      => 'Expiration Year',
				'validators' => array('Digits'),
				'ErrorMessages'     => array('Credit Card expiration required')
			)),
			//BEGIN: Added for ReCAPTCHA on Checkout
			new Zend_Form_Element_Text('captcha', array(
				'required'   => true,
				'label'      => 'No-CAPTCHA reCAPTCHA',
				'ErrorMessages'     => array('reCAPTCHA required')
			)),
			//END: Added for ReCAPTCHA on Checkout
		));


		// Attach sub forms to main form
		$this->addSubForms(array(
			'billing'  => $billing,
			'shipping' => $shipping,
			'payment' => $payment
		));
	}

}
