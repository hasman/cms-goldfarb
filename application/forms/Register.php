<?php
/**
* Register Zend Form
*/
class Application_Form_Register extends Zend_Form
{

	public function  init()
	{
		$this->addElementPrefixPath('Cny_Decorator','Cny/Decorator/','decorator');
		$decor_element = array('SiteDecorator');
		$this->setDecorators ( array (array ('ViewScript', array ('viewScript' => 'auth/register.phtml' ) ) ) );

		//Form
		$this->setName('register')
			->setAction('/auth/create')
			->setMethod('post')
			->setAttrib('id', 'register');

		//Form Fields
		$elements['first_name'] = $this->createElement( 'text', 'first_name' )
			->setRequired(true)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'First Name'))
			->setDecorators( $decor_element );
		$elements['last_name'] = $this->createElement( 'text', 'last_name' )
			->setRequired(true)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Last Name'))
			->setDecorators( $decor_element );
		$elements['email'] = $this->createElement( 'text', 'email' )
			->setRequired(true)
			->addValidator('EmailAddress')
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Email'))
			->setDecorators( $decor_element );
		$elements['password'] = $this->createElement( 'password', 'password' )
			->setRequired(true)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Password'))
			->setDecorators( $decor_element );
		$elements['v_password'] = $this->createElement( 'password', 'v_password' )
			->setRequired(true)
			->setAttribs(array('class'=> 'form-control','placeholder'=>'Confirm Password'))
			->setDecorators( $decor_element );

		$this->addElements( $elements );
	}
}
