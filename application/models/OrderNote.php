<?php
class Application_Model_OrderNote extends Application_Model_Abstract
{
	public $order_id;
    public $type;
    public $note;
    public $creator;
    public $creator_email;
    public $emailed_to;
    public $sent_to_customer;
    public $display_to_customer;
}
