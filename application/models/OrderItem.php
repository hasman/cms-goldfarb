<?php
class Application_Model_OrderItem extends Application_Model_Abstract
{
	public $order_id;
    public $product_id;
	public $variant_id;
    public $sku;
    public $warehouse_sku;
	public $warehouse;
    public $title;
    public $type;
    public $qty;
    public $base_price;
    public $paid_price;
    public $line_total;
    public $tax_amount;
    public $state_tax;
    public $county_tax;
    public $city_tax;
    public $district_tax;
	public $status;
    public $tracking_code;
    public $shipped_date;
}
