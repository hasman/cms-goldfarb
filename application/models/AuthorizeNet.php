<?php
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class Application_Model_AuthorizeNet
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->cfg = (isset($options['authorizenet']))?$options['authorizenet']:null;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($value = $settings_mapper->getValueByCode('ecommerce_authnet_login_id')) {
			$this->cfg['login_id'] = $value;
		}
		if ($value = $settings_mapper->getValueByCode('ecommerce_authnet_trx_key')) {
			$this->cfg['key'] = $value;
		}

	}

	/*
	 * intent: (authCaptureTransaction, authOnlyTransaction)
	 * products: array of products
	 * totals: array of costs array('shipping','tax','discount','sub_total','total')
	 */
	public function createTransaction($payment, $totals = array(), $billing, $shipping, $products, $comments = null, $intent = 'authCaptureTransaction')
	{
		$tresponse = $profile_response = null;

		$name = explode(" ", $payment['name']);

		// Common setup for API credentials
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName($this->cfg['login_id']);
		$merchantAuthentication->setTransactionKey($this->cfg['key']);

		// Create the payment data for a credit card
		$exp_month = str_pad($payment['exp_month'], "2", "0", STR_PAD_LEFT);

		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($payment['cc_num']);
		$creditCard->setCardCode($payment['ccv']);
		$creditCard->setExpirationDate("{$payment['exp_year']}-{$exp_month}");
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);

		// Create a transaction
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType($intent);
		$transactionRequestType->setAmount($totals['total']);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setCustomerIP($_SERVER['REMOTE_ADDR']);

		if ($comments) {
			// Create order information
			$order = new AnetAPI\OrderType();
			//$order->setInvoiceNumber("10101");
			$order->setDescription($comments);
		}

		// Billing address
		$billAddress = new AnetAPI\CustomerAddressType();
		$billAddress->setFirstName($name[0]);
		$billAddress->setLastName($name[1]);
		$billAddress->setCompany(isset($billing['company'])?$billing['company']:null);
		$billAddress->setAddress($billing['address'] . ' ' . $billing['address2']);
		$billAddress->setCity($billing['city']);
		$billAddress->setState($billing['region']);
		$billAddress->setZip($billing['postcode']);
		$billAddress->setCountry($billing['country']);

		$transactionRequestType->setBillTo($billAddress);

		// Shipping address
		if ($shipping) {
			$shipAddress = new AnetAPI\CustomerAddressType();
			$shipAddress->setFirstName($shipping['first_name']);
			$shipAddress->setLastName($shipping['last_name']);
			$shipAddress->setCompany(isset($shipping['company']) ? $shipping['company'] : null);
			$shipAddress->setAddress($shipping['address'] . ' ' . $shipping['address2']);
			$shipAddress->setCity($shipping['city']);
			$shipAddress->setState($shipping['region']);
			$shipAddress->setZip($shipping['postcode']);
			$shipAddress->setCountry($shipping['country']);

			$transactionRequestType->setShipTo($shipAddress);
		}

		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setTransactionRequest($transactionRequestType);
		$controller = new AnetController\CreateTransactionController($request);

		if (APPLICATION_ENV == 'production') {
			$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		} else {
			$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
		}

		$tresponse = $response->getTransactionResponse();

		$error = '';
		if (($tresponse != null) && ($tresponse->getResponseCode() == "1")) {
			//store profile ID for later capture
			$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
			$merchantAuthentication->setName($this->cfg['login_id']);
			$merchantAuthentication->setTransactionKey($this->cfg['key']);

			$request = new AnetAPI\CreateCustomerProfileFromTransactionRequest();
			$request->setMerchantAuthentication($merchantAuthentication);
			$request->setTransId($tresponse->getTransId());

			if (isset($billing['email']) && $billing['email']) {
				$customerProfile = new AnetAPI\CustomerProfileBaseType();
				$customerProfile->setEmail($billing['email']);

				$request->setCustomer($customerProfile);
				// You can just provide the customer Profile ID instead of setCustomer() if you save CustomerProfileId
				//$request->setCustomerProfileId("123343");
			}

			$controller = new AnetController\CreateCustomerProfileFromTransactionController($request);

			if (APPLICATION_ENV == 'production') {
				$profile_response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
			} else {
				$profile_response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
			}
		}else {
			if (isset($response) && $response->getMessages()->getResultCode() == 'Error') {
				$error = "There was a problem with your credit card. Please check and try again.";//$response->getMessages()
			}
		}

		return array('response' => $tresponse, 'profile_response' => $profile_response, 'error_message' => $error);
	}

	/*
	 *
	 *
	 *
	 */
	public function refundTransaction($transactionId, $amount)
	{
		$tresponse = $profile_response = $error = null;

		// Common setup for API credentials
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName($this->cfg['login_id']);
		$merchantAuthentication->setTransactionKey($this->cfg['key']);

		// Set the transaction's refId
		$refId = 'ref' . time();

		//getTransactionDetails :: need to get the last 4 of the CC
		$request = new AnetAPI\GetTransactionDetailsRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setTransId($transactionId);

		$controller = new AnetController\GetTransactionDetailsController($request);

		if (APPLICATION_ENV == 'production') {
			$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		} else {
			$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
		}

		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
			//echo "SUCCESS: Transaction Status:" . $response->getTransaction()->getTransactionStatus() . "\n";
			//echo "                Auth Amount:" . $response->getTransaction()->getAuthAmount() . "\n";
			//echo "                   Trans ID:" . $response->getTransaction()->getTransId() . "\n";

			$cc_number = $response->getTransaction()->getPayment()->getCreditCard()->getCardnumber();

			// Create the payment data for a credit card
			$creditCard = new AnetAPI\CreditCardType();
			$creditCard->setCardNumber($cc_number); // either a full card number and expiration date, or the original transaction ID (transId) and last 4 digits of the card number.
			$creditCard->setExpirationDate("XXXX");
			$paymentOne = new AnetAPI\PaymentType();
			$paymentOne->setCreditCard($creditCard);
			//create a transaction
			$transactionRequest = new AnetAPI\TransactionRequestType();
			$transactionRequest->setTransactionType( "refundTransaction");
			$transactionRequest->setAmount($amount);
			$transactionRequest->setPayment($paymentOne);
			$transactionRequest->setRefTransId($transactionId);


			$request = new AnetAPI\CreateTransactionRequest();
			$request->setMerchantAuthentication($merchantAuthentication);
			$request->setRefId($refId);
			$request->setTransactionRequest( $transactionRequest);
			$controller = new AnetController\CreateTransactionController($request);

			if (APPLICATION_ENV == 'production') {
				$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
			} else {
				$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
			}

			if ($response != null) {
				if($response->getMessages()->getResultCode() == "Ok") {
					$tresponse = $response->getTransactionResponse();

					if ($tresponse != null && $tresponse->getMessages() != null) {
						//echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
						//echo "Refund SUCCESS: " . $tresponse->getTransId() . "\n";
						//echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n";
						//echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
					}else {
						if($tresponse->getErrors() != null) {
							//echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
							$error = $tresponse->getErrors()[0]->getErrorText();
						}
					}
				}else {
					$tresponse = $response->getTransactionResponse();
					if($tresponse != null && $tresponse->getErrors() != null) {
						//echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode();
						$error = $tresponse->getErrors()[0]->getErrorText();
					}else {
						//echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode();
						$error = $response->getMessages()->getMessage()[0]->getText();
					}
				}
			}else {
				$error = "There was a problem with Authorize.net Please try again later.";
			}
		}else {
			//echo "Response : " . $response->getMessages()->getMessage()[0]->getCode() . "  " .$response->getMessages()->getMessage()[0]->getText() . "\n";

			$error = $response->getMessages()->getMessage()[0]->getText();
		}

		return array('response' => $tresponse, 'error_message' => $error);
	}

	public function createRecurringBilling($payment, $amount, $billing, $start_date, $period, $frequency = 1)
	{
		$tresponse = $profile_response = null;

		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName($this->cfg['login_id']);
		$merchantAuthentication->setTransactionKey($this->cfg['key']);

		$name = explode(" ", $payment['name']);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Subscription Type Info
		$subscription = new AnetAPI\ARBSubscriptionType();
		$subscription->setName("Subscription");

		$interval = new AnetAPI\PaymentScheduleType\IntervalAType();
		$interval->setLength($frequency);
		$interval->setUnit($period);

		$paymentSchedule = new AnetAPI\PaymentScheduleType();
		$paymentSchedule->setInterval($interval);
		$paymentSchedule->setStartDate(new DateTime($start_date));
		$paymentSchedule->setTotalOccurrences("9999");

		$subscription->setPaymentSchedule($paymentSchedule);
		$subscription->setAmount($amount);

		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($payment['cc_num']);
		$creditCard->setCardCode($payment['ccv']);
		$creditCard->setExpirationDate("{$payment['exp_year']}-{$payment['exp_month']}");

		$payment = new AnetAPI\PaymentType();
		$payment->setCreditCard($creditCard);
		$subscription->setPayment($payment);

		$billAddress = new AnetAPI\NameAndAddressType();
		$billAddress->setFirstName($name[0]);
		$billAddress->setLastName($name[1]);
		$billAddress->setCompany(isset($billing['company'])?$billing['company']:null);
		$billAddress->setAddress($billing['address'] . ' ' . $billing['address2']);
		$billAddress->setCity($billing['city']);
		$billAddress->setState($billing['region']);
		$billAddress->setZip($billing['postcode']);
		$billAddress->setCountry($billing['country']);

		$subscription->setBillTo($billAddress);

		$request = new AnetAPI\ARBCreateSubscriptionRequest();
		$request->setmerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setSubscription($subscription);

		$controller = new AnetController\ARBCreateSubscriptionController($request);

		if (APPLICATION_ENV == 'production') {
			$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		} else {
			$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
		}

		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") ) {
			$tresponse['subscription_id'] = $response->getSubscriptionId();
		}else {
			//echo "Response : " . $response->getMessages()->getMessage()[0]->getCode() . "  " .$response->getMessages()->getMessage()[0]->getText() . "\n";

			$error = $response->getMessages()->getMessage()[0]->getText();
		}

		return array('response' => $tresponse, 'error_message' => $error);
	}
}
