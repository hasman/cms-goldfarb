<?php
class Application_Model_Font extends Application_Model_Abstract
{
	public $id;
	public $code;
	public $title;
	public $type;
	public $url;
	public $status;
	public $created;
	public $modified;
}
