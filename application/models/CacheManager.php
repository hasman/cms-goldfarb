<?php
class Application_Model_CacheManager
{
	var $_php_path;
	var $_cache_dir;
	var $_cache_dir_temp;
	var $_cache_lifetime;
	var $_cache_lifetime_css;
	var $_frontendOptions;
	var $_frontendOptionsCSS;
	var $_backendOptions;
	var $_backendOptions_temp;
	var $_fallback;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->_fallback = false;
		$this->_cache_dir = $options['dir']['cache'];
		$this->_cache_dir_temp = $this->_cache_dir.'/temp';
		$this->_cache_lifetime = $settings_mapper->getValueByCode('page_cache_lifetime');
		$this->_cache_lifetime_css = $settings_mapper->getValueByCode('css_cache_lifetime');
		$this->_frontendOptions = array('automatic_serialization' => true, 'lifetime' => $this->_cache_lifetime);
		$this->_frontendOptionsCSS = array('automatic_serialization' => true, 'lifetime' => $this->_cache_lifetime_css);
		$this->_backendOptions = array('cache_dir' => $this->_cache_dir);
		$this->_backendOptions_temp = array('cache_dir' => $this->_cache_dir_temp);

		//create temporary folder
		if (!is_dir($this->_cache_dir.'/temp')) {
			mkdir($this->_cache_dir.'/temp', 0777);
		}

		if (!$this->_php_path) {
			if (PHP_MAJOR_VERSION && PHP_MINOR_VERSION) {
				$php_version = PHP_MAJOR_VERSION . "." . PHP_MINOR_VERSION;
			} else {
				$parts = explode(".", phpversion());
				$php_version = $parts[0] . "." . $parts[1];
			}

			/*
			//having an issue here where I'm getting a response that the directory doesn't exist even when it does
			//tested on Kino Now production site and it said 7.0 directory didn't exist and then it ran saying 7.2 did exist (but that isn't setup properly)
			//Why is it saying the directory doesn't exist when it does? And then why did it run and say 7.2 was the PHP version?
			if (is_dir("/opt/plesk/php/{$php_version}/bin/")) {
				$this->_php_path = "/opt/plesk/php/{$php_version}/bin/";
			}
			*/
			if (self::phpPathTest("/opt/plesk/php/{$php_version}/bin/")) {
				$this->_php_path = "/opt/plesk/php/{$php_version}/bin/";
			}elseif (self::phpPathTest()) {
				//I guess plain "php" works?
			}else {
				$this->_fallback = true;
			}

		}

	}

	//having an issue with testing for Plesk PHP path so doing a simple test
	private function phpPathTest($path = '')
	{
		$result = shell_exec($path.'php '.APPLICATION_PATH.'/../scripts/process_runner.php -m default -c index -a php-test'."  2>&1");

		if ($result == 'pass') {
			return true;
		}

		return false;
	}

	public function checkCache($cache_id)
	{
		$lifetime = (stripos($cache_id, 'css_') !== false)?$this->_cache_lifetime_css:$this->_cache_lifetime;

		$stamp = @filemtime($this->_cache_dir.'/zend_cache---'.$cache_id);
		if (!$stamp) $stamp = time();

		if (time() - $stamp > $lifetime) {
			return false;
		}

		return true;
	}

	public function inProgressCheck($timestamp_file, $timestamp, $attempt = 0)
	{
		//failsafe so no infinite runs
		if ($attempt == 3) {
			return 'die';
		}

		//if another build in progress wait
		if (is_file($timestamp_file) && file_get_contents($timestamp_file) > $timestamp) {
			return 'die';
		}elseif (is_file($timestamp_file) && file_get_contents($timestamp_file) < $timestamp) {
			sleep(30);//wait and try again
			$this->inProgressCheck($timestamp_file, $timestamp, $attempt++);
		}else {
			return 'run';
		}
	}

	public function refreshCSS($page_id = null)
	{
		if ($this->_fallback) {
			$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptionsCSS, $this->_backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$page_id);
		}else {
			exec($this->_php_path.'php '.APPLICATION_PATH.'/../scripts/process_runner.php -m default -c index -a background-build-css page_id='.$page_id." > /dev/null &");

			//$result = shell_exec($this->_php_path.'php '.APPLICATION_PATH.'/../scripts/process_runner.php -m default -c index -a background-build-css --page_id '.$page_id."  2>&1");
			//print_r($result);exit;
		}

		/*
		$cache_id = 'css_site'.$page_id;
		$timestamp = time();
		$timestamp_file = $this->_cache_dir_temp.'/'.$cache_id.'.timestamp';

		//if another build in progress wait
		if ($this->inProgressCheck($timestamp_file, $timestamp) == 'run') {
			$this->cacheCSS($page_id, $timestamp_file, $timestamp);
		}else {
			exit;
		}
		*/
	}

	public function cacheCSS($page_id = null, $timestamp_file, $timestamp)
	{
		//create timestamp file
		file_put_contents($timestamp_file, $timestamp);

		$result = shell_exec($this->_php_path.'php '.APPLICATION_PATH.'/../scripts/process_runner.php -m default -c index -a build-css'."  2>&1");

		if ($result) { //there should be no output so if there is there was an error. Clear cache which will cause CSS refresh like old method
			$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptionsCSS, $this->_backendOptions);
			$cache->remove('css_site');
			$cache->remove('css_site'.$page_id);
		}

		//delete timestmp file after completion
		unlink($timestamp_file);
	}

	public function buildCSS($page_id = null)
	{
		//$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptionsCSS, $this->_backendOptions);
		$temp_cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptionsCSS, $this->_backendOptions_temp);

		$generator = new Cny_Model_HtmlBuild();

		//page specific CSS cache file
		if ($page_id) {
			$cache_id = 'css_site'.$page_id;

			$css = $generator->buildCSS($page_id);

			$tags = array();

			/*
			$mapper = new Application_Model_Mapper_Page();
			$pages = $mapper->fetchAll();

			foreach ($pages as $page) {
				$tags[] = 'page_'.$page->id;
			}
			*/

			$temp_cache->save($css, $cache_id, $tags);

			//move cache files from temp location
			rename($this->_cache_dir_temp . '/zend_cache---' . $cache_id, $this->_cache_dir . '/zend_cache---' . $cache_id);
			rename($this->_cache_dir_temp . '/zend_cache---internal-metadatas---' . $cache_id, $this->_cache_dir . '/zend_cache---internal-metadatas---' . $cache_id);
		}

		//full site CSS file
		$cache_id = 'css_site';
		$css = $generator->buildCSS();

		$tags = array();

		$temp_cache->save($css, $cache_id, $tags);

		//move cache files from temp location
		rename($this->_cache_dir_temp . '/zend_cache---' . $cache_id, $this->_cache_dir . '/zend_cache---' . $cache_id);
		rename($this->_cache_dir_temp . '/zend_cache---internal-metadatas---' . $cache_id, $this->_cache_dir . '/zend_cache---internal-metadatas---' . $cache_id);
	}
}
