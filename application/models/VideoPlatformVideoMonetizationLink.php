<?php
class Application_Model_VideoPlatformVideoMonetizationLink extends Application_Model_Abstract
{
	public $video_platform_video_id;
	public $title;
	public $url;
	public $image_src;
	public $sort_order = '9999';
	public $status = 'Enabled';
}
