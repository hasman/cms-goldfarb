<?php
class Application_Model_HistoryModule extends Application_Model_Abstract
{
	public $id;
	public $module_id;
	public $user_id;
	public $table_field_json;
	public $json_content;
	public $created;
	public $modified;
}
