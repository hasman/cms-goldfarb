<?php
class Application_Model_FaqSubmission extends Application_Model_Abstract
{
	public $faq_section_id;
	public $name;
	public $email;
	public $question;
	public $answer;
	public $status;
	public $is_spam;
	public $remote_addr;
	public $user_agent;
	public $referrer;
}
