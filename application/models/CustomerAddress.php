<?php
class Application_Model_CustomerAddress extends Application_Model_Abstract
{
	public $customer_id;
	public $address_type;
	public $address_name;
	public $first_name;
	public $last_name;
	public $company;
	public $address1;
	public $address2;
	public $city;
	public $region;
	public $postcode;
	public $country;
	public $phone;
	public $default_address;
}
