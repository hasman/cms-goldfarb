<?php
class Application_Model_Page extends Application_Model_Abstract
{
	public $id;
	public $title;
	public $code;
	public $summary;
	public $content;
	public $class;
	public $json_styles;
	public $json_settings;
	public $author;
	public $template;
	public $meta_title;
	public $meta_keywords;
	public $meta_description;
	public $meta_canonical_url;
	public $open_graph_image;
	public $meta_head_custom;
	public $tracking_code;
	public $nav_display = 'Enabled';
	public $footer_display = 'Enabled';
	public $created_by_user_id;
	public $last_modified_by_user_id;
	public $editable = 'Yes';
	//public $main_nav_status = 'Disabled';
	//public $main_nav_parent_page_id;
	//public $main_nav_sort_order;
	public $created;
	public $modified;
	public $status;
	public $password_protect = 'Disabled';
	public $password;
	public $block_type = 'page';
	public $header_template;
	public $system_block = 'No';
	public $custom_var_1;
	public $custom_var_2;
	public $custom_var_3;
	public $custom_var_4;
	public $custom_var_5;
	public $custom_var_6;
	public $schema_page_type;
	public $schema_custom_json;
}
