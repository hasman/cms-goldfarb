<?php

class Application_Model_PayflowPro
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->cfg = (isset($options['payflow_pro'])) ? $options['payflow_pro'] : null;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($value = $settings_mapper->getValueByCode('ecommerce_payflowpro_username')) {
			$this->cfg['username'] = $value;
		}
		if ($value = $settings_mapper->getValueByCode('ecommerce_payflowpro_vendor')) {
			$this->cfg['vendor'] = $value;
		}
		if ($value = $settings_mapper->getValueByCode('ecommerce_payflowpro_partner')) {
			$this->cfg['partner'] = $value;
		}
		if ($value = $settings_mapper->getValueByCode('ecommerce_payflowpro_password')) {
			$this->cfg['password'] = $value;
		}

		if (APPLICATION_ENV == 'production') {
			$this->cfg['api_endpoint'] = 'https://payflowpro.paypal.com';
		} else {
			$this->cfg['api_endpoint'] = 'https://pilot-payflowpro.paypal.com';
		}
	}

	//authorization request
	function authorization($payment, $totals = array(), $billing, $shipping, $products)
	{
		$currencyCode="USD";
		$request_id = md5($payment['cc_num'] . $totals['total'] . date('YmdGis') . "1");

		// body
		$parray = array(
			'TENDER=' . 'C', // C = credit card, P = PayPal
			'TRXTYPE=' . 'A', // S = Sale transaction, A = Authorisation, C = Credit, D = Delayed Capture, V = Void
			'ACCT=' . $payment['cc_num'],
			'EXPDATE=' . date("my",strtotime($payment['exp_year']."-".str_pad($payment['exp_month'],2,'0',STR_PAD_LEFT)."-01")),
			'CVV2=' . $payment['ccv'],
			'NAME=' . $payment['name'],
			'AMT=' . $totals['total'],
			'CURRENCY=' . $currencyCode
		);

		$resArray = $this->hash_call($parray, $request_id);

		return array('response' => $resArray, 'profile_response' => null);
	}

	//direct sale transaction
	function sale_transaction($payment, $totals = array(), $billing, $shipping, $products, $comment = null)
	{
		$currencyCode="USD";
		$request_id = md5($payment['cc_num'] . $totals['total'] . date('YmdGis') . "1");

		// body
		$parray = array(
			'TENDER=' . 'C', // C = credit card, P = PayPal
			'TRXTYPE=' . 'S', // S = Sale transaction, A = Authorisation, C = Credit, D = Delayed Capture, V = Void
			'ACCT=' . $payment['cc_num'],
			'EXPDATE=' . date("my",strtotime($payment['exp_year']."-".str_pad($payment['exp_month'],2,'0',STR_PAD_LEFT)."-01")),
			'CVV2=' . $payment['ccv'],
			'NAME=' . $payment['name'],
			'AMT=' . $totals['total'],
			'CURRENCY=' . $currencyCode,
			'FIRSTNAME=' . $billing['first_name'],
			'LASTNAME=' . $billing['last_name'],
			'STREET=' . $billing['address'],
			'CITY=' . $billing['city'],
			'STATE=' . $billing['region'],
			'ZIP=' . $billing['postcode'],
			'COUNTRY=' . $billing['country'],
		);
		if ($comment) {
			if (strlen($comment) > 128) {
				$parray[] = 'COMMENT1=' . substr($comment, 0, 128);
				$parray[] = 'COMMENT2=' . substr($comment, 128, 128);
			}else {
				$parray[] = 'COMMENT1=' . $comment;
			}
		}

		$resArray = $this->hash_call($parray, $request_id);

		return array('response' => $resArray, 'profile_response' => null);
	}

	//recurring billing profile
	function create_recurring_billing($payment, $amount, $origid = null, $start_date, $period, $frequency = 1)
	{
		$currencyCode="USD";
		$request_id = md5($payment['cc_num'] . $amount . date('YmdGis') . "4");

		if (strlen(preg_replace("/[^0-9]/","",$start_date)) != 8) {
			$start = date("mdY", strtotime($start_date));
		}else {
			$start = $start_date;
		}

		// body
		$parray = array(
			'TENDER=' . 'C', // C = credit card, P = PayPal
			'TRXTYPE=' . 'R',
			'ACTION=' . 'A', //A = add, M = modify, C = cancel, R = reactivate, I = inquiry, P = payment
			'PROFILENAME=' . 'Subscription', //Non-unique identifying text name
			'ACCT=' . $payment['cc_num'],
			'AMT=' . $amount,
			'CURRENCY=' . $currencyCode,
			'START=' . $start,
			'TERM=' . '0',
			'PAYPERIOD=' . $period,
			'FREQUENCY=' . $frequency,
			'MAXFAILPAYMENTS=' . '1',
			'RETRYNUMDAYS=' . '1',
			'EMAIL=' . (isset($payment['email']))?$payment['email']:''
		);

		$resArray = $this->hash_call($parray, $request_id);

		return array('response' => $resArray, 'profile_response' => null);
	}

	//cancel recurring billing profile
	function cancel_recurring_billing($profile_id)
	{
		$request_id = md5($profile_id . date('YmdGis') . "4");

		// body
		$parray = array(
			'TENDER=' . 'C', // C = credit card, P = PayPal
			'TRXTYPE=' . 'R',
			'ACTION=' . 'C', //A = add, M = modify, C = cancel, R = reactivate, I = inquiry, P = payment
			'ORIGPROFILEID=' . $profile_id,
		);

		$resArray = $this->hash_call($parray, $request_id);

		return array('response' => $resArray, 'profile_response' => null);
	}

	//recurring billing profile
	function modify_recurring_billing($profile_id, $payment = null, $amount = null, $origid = null, $start_date = null, $period = null, $frequency = null)
	{
		$request_id = md5($profile_id . date('YmdGis') . "5");

		// body
		$parray = array(
			'TENDER=' . 'C', // C = credit card, P = PayPal
			'TRXTYPE=' . 'R',
			'ACTION=' . 'M', //A = add, M = modify, C = cancel, R = reactivate, I = inquiry, P = payment
			'ORIGPROFILEID=' . $profile_id
		);

		if ($start_date) {
			if (strlen(preg_replace("/[^0-9]/", "", $start_date)) != 8) {
				$start = date("mdY", strtotime($start_date));
			} else {
				$start = $start_date;
			}
			$parray[] = 'START=' . $start;
		}
		if ($payment && isset($payment['cc_num'])) {
			$parray[] = 'ACCT=' . $payment['cc_num'];
		}
		if ($amount) {
			$parray[] = 'AMT=' . $amount;
		}
		if ($period) {
			$parray[] = 'PAYPERIOD=' . $period;
		}
		if ($frequency) {
			$parray[] = 'FREQUENCY=' . $frequency;
		}
		if (isset($payment['email'])) {
			$parray[] = 'EMAIL=' . $payment['email'];
		}
		if ($origid) {
			$parray[] = 'ORIGID=' . $origid;
		}

		$resArray = $this->hash_call($parray, $request_id);

		return array('response' => $resArray, 'profile_response' => null);
	}

	//recurring billing profile information
	function inquire_recurring_billing($profile_id)
	{
		$request_id = md5($profile_id . date('YmdGis') . "4");

		// body
		$parray = array(
			'TENDER=' . 'C', // C = credit card, P = PayPal
			'TRXTYPE=' . 'R',
			'ACTION=' . 'I', //A = add, M = modify, C = cancel, R = reactivate, I = inquiry, P = payment
			'ORIGPROFILEID=' . $profile_id,
		);

		$resArray = $this->hash_call($parray, $request_id);

		return array('response' => $resArray, 'profile_response' => null);
	}

	//delayed capture transaction
	function delayed_capture($origid, $amount = '')
	{
		if (strlen($origid) < 3) {
			return 'OrigID not valid';
		}

		// build hash
		$request_id = md5($origid . $amount . date('YmdGis') . "2");

		// body
		$parray = array(
			'TENDER=' . 'C', // C = credit card, P = PayPal
			'TRXTYPE=' . 'D', // S = Sale transaction, A = Authorisation, C = Credit, D = Delayed Capture, V = Void
			'ORIGID=' . $origid,
			'AMT=' . $amount
		);

		$resArray = $this->hash_call($parray, $request_id);

		return array('response' => $resArray, 'profile_response' => null);
	}

	// Credit Transaction
	function credit_transaction($origid, $amount)
	{
		if (strlen($origid) < 3) {
			return 'OrigID not valid';
		}

		// build hash
		$request_id = md5($origid . $amount . date('YmdGis') . "2");

		// body
		$parray = array(
			'TENDER=' . 'C', // C = credit card, P = PayPal
			'TRXTYPE=' . 'C', // S = Sale transaction, A = Authorisation, C = Credit, D = Delayed Capture, V = Void
			'ORIGID=' . $origid,
			'AMT=' . $amount
		);

		$resArray = $this->hash_call($parray, $request_id);

		return array('response' => $resArray, 'profile_response' => null);
	}

	// Void Transaction
	function void_transaction($origid)
	{
		if (strlen($origid) < 3) {
			return 'OrigID not valid';
		}

		// build hash
		$request_id = md5($origid . date('YmdGis') . "3");

		// body
		$parray = array(
			'TENDER=' . 'C', // C = credit card, P = PayPal
			'TRXTYPE=' . 'V', // S = Sale transaction, A = Authorisation, C = Credit, D = Delayed Capture, V = Void
			'ORIGID=' . $origid
		);

		$resArray = $this->hash_call($parray, $request_id);

		return array('response' => $resArray, 'profile_response' => null);
	}

	private function hash_call($parray, $request_id)
	{
		$array = array(
			'USER=' . $this->cfg['username'],
			'VENDOR=' . $this->cfg['vendor'],
			'PARTNER=' . $this->cfg['partner'],
			'PWD=' . $this->cfg['password'],
			'CLIENTIP=' . $_SERVER['REMOTE_ADDR'],
			'VERBOSITY=MEDIUM'
		);
		$plist = implode("&", array_merge($array, $parray));

		$headers = $this->get_curl_headers();
		$headers[] = "X-VPS-Request-ID: " . $request_id; //unique so PayPal can check for duplicate transactions

		$user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)"; // play as Mozilla
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->cfg['api_endpoint']);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 1); // tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 45); // times out after 45 secs
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // this line makes it work under https
		curl_setopt($ch, CURLOPT_POSTFIELDS, $plist); //adding POST data
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2); //verifies ssl certificate
		curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE); //forces closure of connection when done
		curl_setopt($ch, CURLOPT_POST, 1); //data sent as POST

		$result = curl_exec($ch);
		$headers = curl_getinfo($ch);
		curl_close($ch);

		$pfpro = $this->get_curl_result($result); //result arrray

		return $pfpro;

		/*
		if (isset($pfpro['RESULT']) && $pfpro['RESULT'] == 0) {
			return $pfpro;
		} else {
			$this->set_errors($pfpro['RESPMSG'] . ' ['. $pfpro['RESULT'] . ']');
			return false;
		}
		*/
	}

	// Curl custom headers; adjust appropriately for your setup:
	private function get_curl_headers() {
		$headers = array();

		$headers[] = "Content-Type: text/name value"; //or maybe text/xml
		$headers[] = "X-VPS-Timeout: 30";
		$headers[] = "X-VPS-VIT-OS-Name: Linux";  // Name of your OS
		$headers[] = "X-VPS-VIT-OS-Version: RHEL 4";  // OS Version
		$headers[] = "X-VPS-VIT-Client-Type: PHP/cURL";  // What you are using
		$headers[] = "X-VPS-VIT-Client-Version: 0.01";  // For your info
		$headers[] = "X-VPS-VIT-Client-Architecture: x64";  // For your info
		//$headers[] = "X-VPS-VIT-Client-Certification-Id: " . $ClientCertificationId . ""; // get this from payflowintegrator@paypal.com
		$headers[] = "X-VPS-VIT-Integration-Product: " . $this->cfg['vendor'];  // For your info, would populate with application name
		$headers[] = "X-VPS-VIT-Integration-Version: 0.01"; // Application version

		return $headers;
	}

	// parse result and return an array
	private function get_curl_result($result) {
		if (empty($result)) return;

		$pfpro = array();
		$result = strstr($result, 'RESULT');
		$valArray = explode('&', $result);
		foreach($valArray as $val) {
			$valArray2 = explode('=', $val);
			$pfpro[$valArray2[0]] = $valArray2[1];
		}
		return $pfpro;
	}
}
