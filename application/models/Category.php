<?php
class Application_Model_Category extends Application_Model_Abstract
{
	public $name;
	public $code;
	public $parent_id;
	public $block_id;
	public $main_image;
	public $main_image_alt_tag;
	public $banner_image;
	public $banner_image_alt_tag;
	public $banner_mobile_image;
	public $banner_mobile_image_alt_tag;
	public $banner_alignment;
	public $banner_force_fullwidth;
	public $description;
	public $description_position;
	public $title_display_status;
	public $title_display_position;
	public $breadcrumb_display_status;
	public $breadcrumb_display_position;
	public $banner_display_status;
	public $meta_title;
	public $meta_keywords;
	public $meta_description;
	public $nav_display;
	public $sort_order;
	public $status;
}
