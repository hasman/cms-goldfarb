<?php
class Application_Model_SharpSpring
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

		$this->cfg['api_url'] = "http://api.sharpspring.com/pubapi/v1/";

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($value = $settings_mapper->getValueByCode('sharpspring_account_id')) {
			$this->cfg['account_id'] = $value;
		}
		if ($value = $settings_mapper->getValueByCode('sharpspring_secret_key')) {
			$this->cfg['secret_key'] = $value;
		}
	}

	public function getFields()
	{
		$data = array(
			'method' => 'getFields',
			'id' => session_id(),
			'params' => array(
				'where' => ''
			)
		);

		$queryString = http_build_query(array('accountID' => $this->cfg['account_id'], 'secretKey' => $this->cfg['secret_key']));
		$url = $this->cfg['api_url']."?$queryString";

		$data = json_encode($data);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data),
			'Expect: '
		));

		$result = curl_exec($ch);
		curl_close($ch);

		$json = json_decode($result);

		$result = $json->result->field;

		return $result;
	}

	public function getLists()
	{
		$data = array(
			'method' => 'getActiveLists',
			'id' => session_id(),
			'params' => array(
				'where' => ''
			)
		);

		$queryString = http_build_query(array('accountID' => $this->cfg['account_id'], 'secretKey' => $this->cfg['secret_key']));
		$url = $this->cfg['api_url']."?$queryString";

		$data = json_encode($data);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data),
			'Expect: '
		));

		$result = curl_exec($ch);
		curl_close($ch);

		$json = json_decode($result);

		$result = $json->result->activeList;

		return $result;
	}

	public function createLead($mapping = array())
	{
		$data = array(
			'method' => 'createLeads',
			'id' => session_id(),
			'params' => array(
				'objects' => array($mapping)
			)
		);

		$queryString = http_build_query(array('accountID' => $this->cfg['account_id'], 'secretKey' => $this->cfg['secret_key']));
		$url = $this->cfg['api_url']."?$queryString";

		$data = json_encode($data);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data),
			'Expect: '
		));

		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}

	public function addLeadToListByEmail($list_id, $email)
	{
		$data = array(
			'method' => 'addListMemberEmailAddress',
			'id' => session_id(),
			'params' => array(
				'listID' => $list_id,
				'emailAddress' => $email
			)
		);

		$queryString = http_build_query(array('accountID' => $this->cfg['account_id'], 'secretKey' => $this->cfg['secret_key']));
		$url = $this->cfg['api_url']."?$queryString";

		$data = json_encode($data);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data),
			'Expect: '
		));

		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}

	public function getLeadByEmail($email)
	{
		$data = array(
			'method' => 'getLeads',
			'id' => session_id(),
			'params' => array(
				'where' => array(
					'emailAddress' => $email
				)
			)
		);

		$queryString = http_build_query(array('accountID' => $this->cfg['account_id'], 'secretKey' => $this->cfg['secret_key']));
		$url = $this->cfg['api_url']."?$queryString";

		$data = json_encode($data);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data),
			'Expect: '
		));

		$result = curl_exec($ch);
		curl_close($ch);

		$json = json_decode($result);

		$result = current($json->result->lead);

		return $result;
	}
}
