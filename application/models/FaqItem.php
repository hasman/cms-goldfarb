<?php
class Application_Model_FaqItem extends Application_Model_Abstract
{
	public $faq_section_id;
	public $question;
	public $answer;
	public $sort_order = 9999;
	public $status;
}
