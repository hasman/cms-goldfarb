<?php
class Application_Model_VideoPlatformOrderPayment extends Application_Model_Abstract
{
	public $video_platform_order_id;
	public $transaction_id;
	public $sale_id;
	public $identifier;
	public $card_token;
	public $total_amount;
	public $discount_amount;
	public $tax_amount;
	public $response;
}
