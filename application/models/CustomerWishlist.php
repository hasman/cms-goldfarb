<?php
class Application_Model_CustomerWishlist extends Application_Model_Abstract
{
	public $customer_id;
	public $product_id;
	public $variant_id;
	public $wishlist_price;
	public $additional_data;
}
