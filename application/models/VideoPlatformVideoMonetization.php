<?php
class Application_Model_VideoPlatformVideoMonetization extends Application_Model_Abstract
{
	public $video_platform_video_id;
	public $purchase_required = 'No';
	public $purchase_price;
	public $rental_required = 'No';
	public $rental_price;
	public $rental_duration_days;
}
