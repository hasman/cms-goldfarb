<?php
class Application_Model_VideoPlatformVideoTrack extends Application_Model_Abstract
{
	public $video_platform_video_id;
	public $filename;
	public $public_url;
	public $label;
	public $srclang;
	public $kind;
	public $default_track;
}
