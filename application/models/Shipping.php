<?php

class Application_Model_Shipping
{
	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

		//shipping name/info
		$this->_shipping_services = array(
			'INTERNATIONAL_PRIORITY' => array('provider'=>'fedex','service'=>'FedEx International Priority','class'=>'priority', 'hint'=>''),
			'INTERNATIONAL_ECONOMY' => array('provider'=>'fedex','service'=>'FedEx International Economy','class'=>'standard', 'hint'=>''),

			//'FIRST_OVERNIGHT' => array('provider'=>'fedex','service'=>'FedEx Overnight AM','class'=>'1day', 'hint'=>''),
			//'PRIORITY_OVERNIGHT' => array('provider'=>'fedex','service'=>'FedEx Priority Overnight','class'=>'1day', 'hint'=>''),
			'STANDARD_OVERNIGHT' => array('provider'=>'fedex','service'=>'FedEx Overnight','class'=>'1day', 'hint'=>''),
			//'FEDEX_2_DAY_AM' => array('provider'=>'fedex','service'=>'FedEx 2 Day AM','class'=>'2day', 'hint'=>''),
			'FEDEX_2_DAY' => array('provider'=>'fedex','service'=>'FedEx 2 Day','class'=>'2day', 'hint'=>''),
			//'FEDEX_EXPRESS_SAVER' => array('provider'=>'fedex','service'=>'FedEx Express Saver','class'=>'standard', 'hint'=>''),
			'FEDEX_GROUND' => array('provider'=>'fedex','service'=>'FedEx Ground','class'=>'standard', 'hint'=>''),
			'SMART_POST' => array('provider'=>'fedex','service'=>'FedEx Smartpost','class'=>'standard', 'hint'=>'')
		);
	}

	public function services()
	{
		return $this->_shipping_services;
	}

	public function getShippingOptions($address, $products = null)
	{
		$this->_cart = new Zend_Session_Namespace('cart');

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$shipping_options = array();
		$surcharge = ($surcharge = preg_replace("/[^0-9.]/","",$settings_mapper->getValueByCode('shipping_surcharge')))?$surcharge:0; //surcharge added to rate
		$items = 0;
		$sub_total = 0;
		$weight = 0;

		if ($products) {
			foreach ($products as $product) {
				$items += $product['qty'];
				$sub_total += ($product['price'] * $product['qty']);

				$weight += ($product['product']->weight * $product['qty']);
			}
		}

		if ($flat_rate_table = $settings_mapper->getValueByCode('shipping_flat_rate')) {
			$rate = 0;
			$tmp_table = [];
			$table_rows = ($flat_rate_table) ? json_decode($flat_rate_table) : array();
			if ($table_rows) {
				foreach ($table_rows as $row) {
					if (!isset($row->country) || !$row->country || in_array($address['country'],$row->country)) {
						$tmp_table[$row->subtotal] = $row->shipping_rate;
					}
				}
				krsort($tmp_table);

				$found = false;
				foreach ($tmp_table as $row_subtotal => $row_rate) {
					if ($row_subtotal <= $sub_total && !$found) {
						$rate = $row_rate;

						$found = true;
					}
				}

				if ($found) {
					$shipping_options['Standard'] = array('name' => 'Standard Rate', 'rate' => number_format($rate, 2), 'description' => '');
				}
			}
		}

		//FedEx
		if ($settings_mapper->getValueByCode('fedex_status') == 'Enabled') {
			if (!$weight) $weight = 1;

			$fedex = new Application_Model_FedExRate();
			$rates = $fedex->getAvailableRates($address, $weight);

			$fedex_services = array();
			foreach ($this->_shipping_services as $code => $service) {
				if ($service['provider'] == 'fedex')
					$fedex_services[$code] = $service['service'];
			}

			foreach ($rates as $rate) {
				if (is_object($rate)) {
					if (array_key_exists($rate->ServiceType, $fedex_services)) {
						$cost = $rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;

						if ($cost) {
							$shipping_options[$rate->ServiceType] = array('name' => $fedex_services[$rate->ServiceType], 'rate' => number_format($cost + $surcharge, 2), 'hint' => $this->_shipping_services[$rate->ServiceType]['hint'], 'description' => '');
						}
					}
				}
			}
		}

		//check promos for free shipping
		$mapper = new Application_Model_Mapper_Promo();
		if ($this->_cart->promo_code) {
			$promo = $mapper->fetchByCode($this->_cart->promo_code, $sub_total);
		}else {
			$promo = $mapper->fetchAutomaticPromo($sub_total);
		}

		if ($promo) {
			if ($promo->threshold && $promo->threshold > $sub_total) {
				//subtotal hasn't reached required threshold
			}else {
				if ($promo->shipping_restriction && $promo->shipping_restriction != $address['country']) {
					//shipping discount only for specific country
				} else {
					$shipping_amount = $promo->shipping_amount;

					if ($promo->shipping_qty_threshold) {
						//need to get a count of how many products in the cart are in the promo
						$mapper = new Application_Model_Mapper_PromoProduct();
						$promo_products = $mapper->fetchProductArrayByPromoId($promo->id);

						$cnt = 0;
						foreach ($this->_cart->items as $item_id => $item) {
							if (in_array($item_id,$promo_products) || $promo->sitewide == 'Yes') {
								$cnt += $item['qty'];
							}
						}

						if ($cnt >= $promo->shipping_qty_threshold) {
							$shipping_amount = $promo->shipping_amount_over_threshold;
						}
					}

					if ($promo->shipping_type == 'fixed') {
						$shipping_options['Standard'] = array(
							'name' => 'Standard Shipping',
							'rate' => number_format($shipping_options['Standard']['rate'] - $shipping_amount, 2),
							'description' => '',
							'hint' => ''
						);
					} elseif ($promo->shipping_type == 'price') {
						$shipping_options['Standard'] = array(
							'name' => 'Standard Shipping',
							'rate' => number_format($shipping_amount, 2),
							'description' => '',
							'hint' => ''
						);
					}
				}
			}
		}

		//fallback to return something
		if (!$shipping_options && ($address['country'] == 'US' || $address['country'] == 'CA')) {
			$shipping_options['Standard'] = array('name'=>'Standard Rate','rate'=>number_format(5 + $items/2,2), 'description' => '');
		}

		//sort by rate
		$counter = 0;
		$new_order = [];
		foreach ($shipping_options as $key => $option) {
			$new_order[$option['rate']."".$counter] = array($key=>$option);
			$counter++;
		}
		ksort($new_order);

		$return = [];
		foreach ($new_order as $k => $item) {
			foreach ($item as $key => $option) {
				$return[$key] = $option;
			}
		}

		return $return;
	}

}
