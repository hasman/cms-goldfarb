<?php
class Application_Model_Promo extends Application_Model_Abstract
{
	public $id;
	public $title;
	public $internal_name;
	public $code;
	public $trigger;
	public $status;
	public $start_date;
	public $start_time;
	public $end_date;
	public $end_time;
	public $utc_start_datetime;
	public $utc_end_datetime;
	public $notes;
	public $created;
	public $modified;
}
