<?php
class Cny_Model_VideoParser
{
	public function __construct()
	{
		$this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$options = Zend_Registry::get('configuration')->toArray();

		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'master_file' => $cache_dir, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );
	}

	public function getMediaDetails($url)
	{
		$data = array();

		// YouTube url?
		if(preg_match("/youtu.be\/([a-z1-9.-_-])+/", $url)) {
			preg_match("/youtu.be\/([a-z1-9.-_-]+)/", $url, $matches);
			if(isset($matches[1])) {
				$data['type'] = 'youtube';
				$data['embed_url'] = 'https://www.youtube.com/embed/'.$matches[1];
				$data['video_id'] = $matches[1];
				$data['thumbnail'] = 'https://img.youtube.com/vi/'.$matches[1].'/0.jpg';
			}
		}
		elseif(preg_match("/youtube.com(.+)v=([^&]+)/", $url)) {
			preg_match("/v=([^&]+)/", $url, $matches);
			if(isset($matches[1])) {
				$data['type'] = 'youtube';
				$data['embed_url'] = 'https://www.youtube.com/embed/'.$matches[1];
				$data['video_id'] = $matches[1];
				$data['thumbnail'] = 'https://img.youtube.com/vi/'.$matches[1].'/0.jpg';
			}
		}
		elseif(preg_match("/youtube.com\/embed\/([^&]+)/", $url)) {
			preg_match("/embed\/([^&]+)/", $url, $matches);
			if(isset($matches[1])) {
				$data['type'] = 'youtube';
				$data['embed_url'] = 'https://www.youtube.com/embed/'.$matches[1];
				$data['video_id'] = $matches[1];
				$data['thumbnail'] = 'https://img.youtube.com/vi/'.$matches[1].'/0.jpg';
			}
		}
		// Google video
		elseif(preg_match("/video.google.com(.+)docid=([^&]+)/", $url)) {
			preg_match("/docid=([^&]+)/", $url, $matches);
			if(isset($matches[1])) {
				$data['type'] = 'google';
				$data['embed_url'] = 'https://video.google.com/googleplayer.swf?docId='.$matches[1].'&hl=en';
				$data['video_id'] = $matches[1];
				$data['thumbnail'] = '';
			}
		}
		// Vimeo video
		elseif(preg_match("/vimeo.com\/[1-9.-_]+/", $url)) {
			preg_match("/vimeo.com\/([1-9.-_]+)/", $url, $matches);
			if(isset($matches[1])) {
				$data['type'] = 'vimeo';
				$data['embed_url'] = 'https://player.vimeo.com/video/'.$matches[1];
				$data['video_id'] = $matches[1];
				$data['thumbnail'] = '';
			}
		}

		//doesn't match so treat as generic/direct video URL
		else {
			$data['type'] = 'generic';
			$data['embed_url'] = $url;
			$data['video_id'] = '';
			$data['thumbnail'] = '';
		}

		return $data;
	}

	public function getEmbedUrl($url)
	{
		$details = $this->getMediaDetails($url);

		if ($details['type'] == 'generic') {
			return false;
		}

		return $details['embed_url'];
	}

	public function getVideoId($url)
	{
		$details = $this->getMediaDetails($url);

		return $details['video_id'];
	}
}
