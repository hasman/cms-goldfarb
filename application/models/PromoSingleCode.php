<?php
class Application_Model_PromoSingleCode extends Application_Model_Abstract
{
	public $id;
	public $promo_id;
	public $code;
	public $amount;
	public $used_timestamp;
	public $expiration;
}
