<?php
class Cny_Model_Layout
{
	var $generator;
	static $_defaultView;
	protected $_view;

	protected function getDefaultView()
	{
		if (self::$_defaultView === null) {
			self::$_defaultView = new Zend_View();
			self::$_defaultView->setScriptPath(APPLICATION_PATH.'/views/scripts');
			self::$_defaultView->setHelperPath(APPLICATION_PATH.'/views/helpers');
		}
		return self::$_defaultView;
	}

	public function __construct()
	{
		$this->_view = self::getDefaultView();

		$this->generator = new Cny_Model_HtmlBuild();

		$this->_view->headLink()->appendStylesheet('/index/css/page_id/0/?v='.$this->generator->cssStamp(0));

		//set body classes based on settings
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$header_type = $settings_mapper->getValueByCode('header_type');

		if ($header_type == '') {
			$this->_view->placeholder('body_class')->append('header-fixed-space-default');
		}
		if ($settings_mapper->getValueByCode('nav_fixed_position') != 'navbar-fixed-bottom' && $settings_mapper->getValueByCode('nav_fixed_position') != 'navbar-static-top') {
			$this->_view->placeholder('body_class')->append('header-fixed');
		}
	}

	public function nav()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$header_type = $settings_mapper->getValueByCode('header_type');

		if ($header_type && $header_type == 'default') {
			return $this->generator->buildHeader();
		}elseif ($header_type && stripos($header_type,"block-") !== false) {
			$header_block_id = str_ireplace("block-","",$header_type);

			return $this->generator->buildCustomHeader($header_block_id);
		}else {
			return $this->generator->buildHeader($header_type);
		}
	}

	public function footer()
	{
		return $this->generator->buildFooter();
	}

	public function moduleStyleBlock()
	{
		return $this->generator->getModuleStyleBlock();
	}
}
