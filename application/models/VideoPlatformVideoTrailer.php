<?php
class Application_Model_VideoPlatformVideoTrailer extends Application_Model_Abstract
{
	public $hash;
	public $video_platform_video_id;
	public $trailer_video_platform_video_id;
	public $filename;
	public $filename_internal;
	public $filesize;
	public $mimetype;
	public $etag;
	public $transcode_status;
	public $public_url;
	public $notify_email;
	public $embed_url;
	public $embed_code;
}
