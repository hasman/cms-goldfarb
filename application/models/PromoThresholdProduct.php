<?php
class Application_Model_PromoThresholdProduct extends Application_Model_Abstract
{
	public $id;
	public $promo_threshold_id;
	public $product_id;
	public $variant_id;
	public $type = 'include';
	public $created;
	public $modified;
}
