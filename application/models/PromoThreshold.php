<?php
class Application_Model_PromoThreshold extends Application_Model_Abstract
{
	public $id;
	public $promo_id;
	public $threshold;
	public $amount;
	public $type;
	public $shipping_amount;
	public $shipping_type;
	public $shipping_restriction;
	public $shipping_qty_threshold;
	public $shipping_amount_over_threshold;
	public $email_coupon_code_count;
	public $email_coupon_code_amount;
	public $email_coupon_code_expiration;
	public $email_coupon_code_expiration_utc;
	public $sitewide = 'No';
	public $banner;
	public $created;
	public $modified;
}
