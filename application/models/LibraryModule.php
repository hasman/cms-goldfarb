<?php
class Application_Model_LibraryModule extends Application_Model_Abstract
{
	public $id;
	public $label;
	public $type;
	public $class;
	public $json_styles;
	public $json_settings;
	public $desktop_status = 'Enabled';
	public $tablet_status = 'Enabled';
	public $mobile_status = 'Enabled';
	public $json_content;
	public $wireframe_json;
	public $created;
	public $modified;
}
