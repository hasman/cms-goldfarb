<?php
class Application_Model_MaxMind
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($value = $settings_mapper->getValueByCode('maxmind_account_id')) {
			$this->cfg['maxmind_account_id'] = $value;
		}
		if ($value = $settings_mapper->getValueByCode('maxmind_license_key')) {
			$this->cfg['maxmind_license_key'] = $value;
		}
		if ($value = $settings_mapper->getValueByCode('maxmind_precision')) {
			$this->cfg['maxmind_precision'] = $value;
		}

		$this->cfg['api_url'] = "https://geoip.maxmind.com/geoip/v2.1/";
	}

	public function getInfoFromIP($ip = null)
	{
		if (!$ip) {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		$url = $this->cfg['api_url']."{$this->cfg['maxmind_precision']}/{$ip}";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, $this->cfg['maxmind_account_id'] . ":" . $this->cfg['maxmind_license_key']);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}

}
