<?php
class Application_Model_ProductVideo extends Application_Model_Abstract
{
	public $product_id;
	public $video_src;
	public $image_src;
	public $alt_text;
	public $sort_order;
}
