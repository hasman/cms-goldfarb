<?php
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\Authorization;
use PayPal\Api\Capture;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Sale;
use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;

class Application_Model_Paypal
{
	protected $cfg;
	protected $apiContext;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->cfg = (isset($options['paypal']))?$options['paypal']:array();

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($value = $settings_mapper->getValueByCode('ecommerce_paypal_client_id')) {
			$this->cfg['client']['id'] = $value;
		}
		if ($value = $settings_mapper->getValueByCode('ecommerce_paypal_client_secret')) {
			$this->cfg['client']['secret'] = $value;
		}

		$this->apiContext = new \PayPal\Rest\ApiContext(
			new \PayPal\Auth\OAuthTokenCredential(
				$this->cfg['client']['id'],
				$this->cfg['client']['secret']
			)
		);
		$this->apiContext->setConfig(
			array(
				'mode' => (APPLICATION_ENV == 'production')?'live':'sandbox',
				/*
				'log.LogEnabled' => true,
				'log.FileName' => '../PayPal.log',
				'log.LogLevel' => 'DEBUG', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
				*/
				//'cache.enabled' => true, //cache folder needs write permission
			)
		);
	}

	/*
	 * intent: (sale, authorize)
	 * products: array of products
	 * totals: array of costs array('shipping','tax','discount','sub_total','total')
	 */
	public function createTransaction($products, $totals = array(), $intent = 'sale', $returnUrl = '/checkout/paypal', $cancelUrl = '/checkout/?method=paypal')
	{


		$payer = new Payer();
		$payer->setPaymentMethod("paypal");

		$items = array();
		foreach ($products as $product) {
			$item = new Item();
			$item->setName($product['product']->title)
				->setCurrency('USD')
				->setQuantity($product['qty'])
				->setSku($product['product']->sku)
				->setPrice($product['price']);
			$items[] = $item;
		}

		$itemList = new ItemList();
		$itemList->setItems($items);

		$details = new Details();
		$details->setShipping($totals['shipping'])
			->setTax($totals['tax'])
			->setSubtotal($totals['sub_total'] - $totals['discount']); //I HATE that PayPal has no proper support for discounts

		$amount = new Amount();
		$amount->setCurrency("USD")
			->setTotal($totals['total'])
			->setDetails($details);

		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setItemList($itemList)
			->setDescription("Payment description")
			->setInvoiceNumber(uniqid());

		//$baseUrl = getBaseUrl();
		$baseUrl = sprintf(
			"%s://%s",
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME']
		);
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl($baseUrl . $returnUrl)
			->setCancelUrl($baseUrl . $cancelUrl);

		$payment = new Payment();
		$payment
			->setIntent($intent)
			->setPayer($payer)
			->setRedirectUrls($redirectUrls)
			->setTransactions(array($transaction));

		try {
			$payment->create($this->apiContext);
		} catch (Exception $ex) {
			echo $ex->getData();
			exit;
		}

		if ($payment->getState() == 'success' || $payment->getState() == 'created') {
			header("location: " . $payment->getApprovalLink());
			exit;
		} else {
			if (isset($payment) && $payment->getFailureReason()) {
				return $payment->getFailureReason() . " <br/>";
			} else {
				return "There was an error communicating with PayPal. Please try again shortly. <br/>";
			}
		}
	}

	public function getPayment($paymentId)
	{
		return Payment::get($paymentId, $this->apiContext);
	}

	public function executeTransaction($paymentId)
	{
		$payment = Payment::get($paymentId, $this->apiContext);

		$execution = new PaymentExecution();
		$execution->setPayerId($_GET['PayerID']);

		try {
			$result = $payment->execute($execution, $this->apiContext);

			if ($result->getState() == 'approved') {
				try {
					return array('result' => $result, 'payment' => Payment::get($paymentId, $this->apiContext));
				} catch (Exception $ex) {
					return array('error' => json_decode($ex->getData())->message);
				}
			}else {
				return array('error' => "The payment has been denied by Paypal");

			}
		} catch (Exception $ex) {
			return array('error' => json_decode($ex->getData())->message);
		}
	}

	public function refundTransaction($transactionId, $amount)
	{
		try {
			$amt = new Amount();
			$amt->setCurrency('USD')
				->setTotal($amount);

			$refundRequest = new RefundRequest();
			$refundRequest->setAmount($amt);

			$sale = new Sale();
			$sale->setId($transactionId);

			$result = $sale->refundSale($refundRequest, $this->apiContext);

			if ($result->getState() == 'approved') {
				try {
					return array('response' => $result, 'error' => null);
				} catch (Exception $ex) {
					return array('error' => json_decode($ex->getData())->message);
				}
			}else {
				return array('error' => "The refund has been denied by Paypal");

			}

			//$capture = Capture::get($transactionId, $this->apiContext);
			//$captureRefund = $capture->refundCapturedPayment($refundRequest, $this->apiContext);
		} catch (Exception $ex) {
			return array('error' => json_decode($ex->getData())->message);
		}
	}
}
