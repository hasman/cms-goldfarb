<?php
class Application_Model_PageSectionRowColumnModule extends Application_Model_Abstract
{
	public $id;
	public $page_section_row_column_id;
	public $type;
	public $class;
	public $class_special;
	public $label;
	public $json_styles;
	public $json_settings;
	public $background_image_path;
	public $background_color;
	public $padding;
	public $desktop_status = 'Enabled';
	public $tablet_status = 'Enabled';
	public $mobile_status = 'Enabled';
	public $json_content;
	public $wireframe_json;
	public $sort_order = 9999;
	public $created;
	public $modified;
}
