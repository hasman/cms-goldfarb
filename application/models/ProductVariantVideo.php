<?php
class Application_Model_ProductVariantVideo extends Application_Model_Abstract
{
	public $product_variant_id;
	public $video_src;
	public $image_src;
	public $alt_text;
	public $sort_order;
}
