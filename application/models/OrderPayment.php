<?php
class Application_Model_OrderPayment extends Application_Model_Abstract
{
	public $order_id;
	public $type;
    public $transaction_id;
    public $sale_id = '';
    public $identifier;
    public $card_token;
    public $total_amount;
    public $discount_amount;
    public $shipping_amount;
    public $tax_amount;
    public $request;
    public $response;
    public $status;
}
