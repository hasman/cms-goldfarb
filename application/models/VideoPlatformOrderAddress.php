<?php
class Application_Model_VideoPlatformOrderAddress extends Application_Model_Abstract
{
	public $video_platform_order_id;
	public $type;
	public $first_name;
	public $last_name;
	public $company;
	public $address;
	public $address2;
	public $city;
	public $county = '';
	public $region;
	public $postcode;
	public $country;
	public $phone;
}
