<?php
class Cny_Model_TwitterFeed
{
	public function __construct()
	{
		//
	}

	public function fetchFeed($limit = null)
	{
		$cache_dir = APPLICATION_PATH.'/../tmp/cache';
		$frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 60 );
		$backendOptions  = array( 'cache_dir' => $cache_dir );


		$cache_id = 'twitter_'.$limit;
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

		//var_dump($cache->test($cache_id));exit;
		if(!($cache->test($cache_id))){
			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

			$token = new Zend_Oauth_Token_Access();
			$token->setToken($settings_mapper->getValueByCode('twitter_token'));
			$token->setTokenSecret($settings_mapper->getValueByCode('twitter_token_secret'));

			$twitter = new Zend_Service_Twitter(array(
				'username' => $settings_mapper->getValueByCode('twitter_username'),
				'accessToken' => $token,
				'oauthOptions' => array(
					'consumerKey' => $settings_mapper->getValueByCode('twitter_consumer_key'),
					'consumerSecret' => $settings_mapper->getValueByCode('twitter_consumer_secret')
				)
			));

			$tweets = array();
			$results = $twitter->statusesUserTimeline(array("screen_name"=>$settings_mapper->getValueByCode('twitter_username'),"exclude_replies"=>true));
			//$results = $twitter->statusesUserTimeline(array("screen_name"=>$settings_mapper->getValueByCode('twitter_username'),"count"=>$limit,"exclude_replies"=>true));
			$l_tweets = $results->toValue();

			foreach ($l_tweets as $tweet) {
				//create custom "time ago" timestamp
				$time = time() - strtotime($tweet->created_at);
				if ($time > 24*3600) $time_ago = date("M j",strtotime($tweet->created_at));
				elseif ($time < 3600) $time_ago = ceil($time/60)."m";
				else $time_ago = round($time/3600)."hrs";

				$tweet->cny_time_ago = $time_ago;

				//create custom description field with links
				$description = $tweet->text;
				$description = preg_replace_callback(
					"#(^|[\n \!\?\.\,\:])@([^ \,\.\:\!\?\"\t\n\r<]*)#is",
					function($m) { return "'{$m[1]}<a href=\"https://twitter.com/{$m[2]}\" target=\"_blank\">@{$m[2]}</a>'"; },
					$description
				);
				$description = preg_replace_callback(
					"#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t<]*)#is",
					function($m) { return "'{$m[1]}<a href=\"{$m[2]}\" target=\"_blank\">{$m[2]}</a>'"; },
					$description
				);
				$description = preg_replace_callback(
					"#(^|[\n ])((www|ftp)\.[^ \"\t\n\r<]*)#is",
					function($m) { return "'{$m[1]}<a href=\"http://{$m[2]}\" target=\"_blank\">{$m[2]}</a>'"; },
					$description
				);

				$tweet->cny_description = $description;

				$tweets[strtotime($tweet->created_at)] = $tweet;
			}

			krsort($tweets);
			$tweets = array_splice($tweets, 0,$limit);

			$cache->save($tweets, $cache_id, array('twitter','count_'.$limit));
		}else{
			$tweets = $cache->load($cache_id);
		}

		return $tweets;
	}
}
