<?php
class Application_Model_PaymentHistoryProduct extends Application_Model_Abstract
{
	public $payment_history_id;
	public $product_id;
	public $variant_id;
	public $sku;
	public $warehouse_sku;
}
