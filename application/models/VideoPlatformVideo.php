<?php
class Application_Model_VideoPlatformVideo extends Application_Model_Abstract
{
	public $code;
	public $hash;
	public $filename;
	public $filename_internal;
	public $title;
	public $summary;
	public $description;
	public $meta_keywords;
	public $meta_description;
	public $filesize;
	public $mimetype;
	public $etag;
	public $transcode_status;
	public $public_url;
	public $status;
	public $video_platform_video_thumbnail_id;
	public $thumbnail_src;
	public $poster_src;
	public $poster_type;
	public $notify_email;
	public $purchase_support_text;
	public $admin_user_id;
	public $embed_url;
	public $embed_code;
	public $video_hash;
}
