<?php
class Application_Model_Video extends Application_Model_Abstract
{
	public $title;
	public $video_url;
	public $embed_code;
	public $caption;
	public $description;
	public $video_category_id;
	public $posted_date;
	public $sort_order;
	public $status;
}
