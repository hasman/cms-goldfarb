<?php
class Application_Model_SearchIndex extends Application_Model_Abstract
{
	public $title;
	public $keywords;
	public $description;
	public $url;
	public $content;
	public $created;
	public $modified;
}
