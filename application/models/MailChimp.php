<?php
class Application_Model_MailChimp
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($value = $settings_mapper->getValueByCode('mailchimp_secret_key')) {
			$this->cfg['secret_key'] = $value;
		}

		//mailchimp API URL is partly based on the datacenter set in the API key
		$explode = explode("-", $this->cfg['secret_key']);
		$dc = end($explode);

		$this->cfg['api_url'] = "https://{$dc}.api.mailchimp.com/3.0";
	}

	private function execute($endpoint = '', $method = 'GET', $data = array(), $query_params = array())
	{
		$qs_array = [];
		foreach ($query_params as $k => $param) {
			$qs_array[$k] = $param;
		}

		$queryString = http_build_query($qs_array);

		/*
		$queryString = '';
		if ($key_type) $queryString .= "&api_key=" .$this->cfg['api_keys'][$key_type];
		foreach ($query_params as $k => $param) {
			if (is_array($param)) {
				foreach ($param as $v) {
					$queryString .= "&".urlencode($k)."=".urlencode($v);
				}
			}else {
				$queryString .= "&".urlencode($k)."=".urlencode($param);
			}
		}
		*/

		$url = $this->cfg['api_url'] . $endpoint . "?$queryString";

		$data = json_encode($data);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, "username" . ":" . $this->cfg['secret_key']);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data),
			'Expect: '
		));

		$result = curl_exec($ch);
		curl_close($ch);

		$json = json_decode($result);

		return $json;
	}

	public function listSubscribe($list_id, $email, $mapping = array()) {
		$data = [];
		$data['email_address'] = $email;
		$data['status'] = 'subscribed'; //subscribed, unsubscribed, cleaned, pending, transactional
		$data['merge_fields'] = (object)$mapping;

		return $this->execute("/lists/{$list_id}/members", 'POST', $data);
	}

	public function listUnsubscribe($list_id, $email) {
		$data = [];
		$data['email_address'] = $email;
		$data['status'] = 'unsubscribed'; //subscribed, unsubscribed, cleaned, pending, transactional

		$hash = md5($email);

		return $this->execute("/lists/{$list_id}/members/{$hash}", 'PUT', $data);
	}
}
