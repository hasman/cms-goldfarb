<?php
class Application_Model_PageSection extends Application_Model_Abstract
{
	public $id;
	public $page_id;
	public $type = 'standard';
	public $class;
	public $json_styles;
	public $json_settings;
	public $desktop_status = 'Enabled';
	public $tablet_status = 'Enabled';
	public $mobile_status = 'Enabled';
	public $sort_order = 9999;
	public $created;
	public $modified;
}
