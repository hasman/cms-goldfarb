<?php
class Application_Model_ProductVariant extends Application_Model_Abstract
{
	public $parent_product_id;
	public $product_variant_type_id;
	public $code;
	public $title;
	public $title_sort;
	public $description;
	public $sku;
	public $barcode;
	public $inventory;
	public $price;
	public $msrp;
	public $weight;
	public $status;
	public $warehouse;
	public $available_date;
	public $meta_title;
	public $meta_description;
	public $meta_keywords;
	public $sort_order;
}
