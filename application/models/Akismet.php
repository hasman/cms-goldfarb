<?php
class Application_Model_Akismet
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

		$this->cfg['site_url'] = (isset($options['site']['full_secure']))?$options['site']['full_secure']:null;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($value = $settings_mapper->getValueByCode('akismet_api_key')) {
			$this->cfg['akismet_api_key'] = $value;
		}

		$this->cfg['api_url'] = "https://{$this->cfg['akismet_api_key']}.rest.akismet.com/1.1/";

		if (!$this->cfg['site_url'] && isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
			$this->cfg['site_url'] = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
		}
	}

	/*
	 * Verify Akismet Key
	 *
	 * key: Akismet key (required)
	 * blog: fully qualified site URL (required)
	 */
	public function verifyKey()
	{
		$data = http_build_query(array('key' => $this->cfg['akismet_api_key'], 'blog' => $this->cfg['site_url']));

		$url = $this->cfg['api_url']."verify-key";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result; //"valid" if verified otherwise "invalid"
	}

	/*
	 * Check comment/post is spam or not
	 *
	 * blog: fully qualified site URL (required)
	 * user_ip: IP address of commenter (required)
	 * user_agent: User Agent string (required)
	 * referrer: HTTP_REFERER header (required)
	 * permalink: The full permanent URL of the entry the comment was submitted to.
	 * comment_type: (comment, form-post, etc.) can be anything
	 * comment_author: name of person who posted
	 * comment_author_email: email of person who posted
	 * comment_author_url:
	 * comment_content: The content of the post
	 * comment_date_gmt: GMT Date of the post
	 * comment_post_modified_gmt: GMT Date if modified
	 * blog_lang: ISO 639-1 format, comma-separated
	 * blog_charset:
	 * user_role: If you set it to "administrator", Akismet will always return false.
	 * is_test:
	 *
	 */
	public function commentCheck($post_data = array())
	{
		$post_data['blog'] = $this->cfg['site_url'];
		$post_data['user_agent'] = "LOGIC/1.0 | Akismet/1.0";

		$data = http_build_query($post_data);

		$url = $this->cfg['api_url']."comment-check";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result; //"true" if spam, "false" if not
	}

	/*
	 * Tell Akismet a comment/post is spam
	 *
	 * blog: fully qualified site URL (required)
	 * user_ip: IP address of commenter (required)
	 * user_agent: User Agent string (required)
	 * referrer: HTTP_REFERER header (required)
	 * permalink: The full permanent URL of the entry the comment was submitted to.
	 * comment_type: (comment, form-post, etc.) can be anything
	 * comment_author: name of person who posted
	 * comment_author_email: email of person who posted
	 * comment_author_url:
	 * comment_content: The content of the post
	 * comment_date_gmt: GMT Date of the post
	 * comment_post_modified_gmt: GMT Date if modified
	 * blog_lang: ISO 639-1 format, comma-separated
	 * blog_charset:
	 * user_role: If you set it to "administrator", Akismet will always return false.
	 * is_test:
	 *
	 */
	public function submitSpam($post_data = array())
	{
		$post_data['blog'] = $this->cfg['site_url'];
		$post_data['user_agent'] = "LOGIC/1.0 | Akismet/1.0";

		$data = http_build_query($post_data);

		$url = $this->cfg['api_url']."submit-spam";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		return true; //Akismet doesn't send a useful response
	}

	/*
	 * Tell Akismet comment/post is NOT spam
	 *
	 * blog: fully qualified site URL (required)
	 * user_ip: IP address of commenter (required)
	 * user_agent: User Agent string (required)
	 * referrer: HTTP_REFERER header (required)
	 * permalink: The full permanent URL of the entry the comment was submitted to.
	 * comment_type: (comment, form-post, etc.) can be anything
	 * comment_author: name of person who posted
	 * comment_author_email: email of person who posted
	 * comment_author_url:
	 * comment_content: The content of the post
	 * comment_date_gmt: GMT Date of the post
	 * comment_post_modified_gmt: GMT Date if modified
	 * blog_lang: ISO 639-1 format, comma-separated
	 * blog_charset:
	 * user_role: If you set it to "administrator", Akismet will always return false.
	 * is_test:
	 *
	 */
	public function submitHam($post_data = array())
	{
		$post_data['blog'] = $this->cfg['site_url'];
		$post_data['user_agent'] = "LOGIC/1.0 | Akismet/1.0";

		$data = http_build_query($post_data);

		$url = $this->cfg['api_url']."submit-ham";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		return true; //Akismet doesn't send a useful response
	}
}
