<?php
class Application_Model_Sitemap
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

		$this->cfg['site_url'] = (isset($options['site']['full_secure']))?$options['site']['full_secure']:null;

		if (!$this->cfg['site_url'] && isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
			$this->cfg['site_url'] = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
		}
	}

	public function generate()
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->cfg['home_page'] = '';
		if ($value = $settings_mapper->getValueByCode('home_page')) {
			$this->cfg['home_page'] = $value;
		}

		$xmlString = '<?xml version="1.0" encoding="UTF-8"?>';
		$xmlString .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

		//CMS pages
		$mapper = new Application_Model_Mapper_Page();
		$pages = $mapper->fetchPages();

		foreach ($pages as $page) {
			$code = ($this->cfg['home_page'] == $page->code)?"":$page->code;

			$xmlString .= '<url>';
			$xmlString .= '<loc>'.$this->cfg['site_url'].'/'.$code.'</loc>';
			$xmlString .= '<lastmod>' . date(DATE_ATOM, time()) . '</lastmod>';
			$xmlString .= '<changefreq>daily</changefreq>';
			$xmlString .= '<priority>1.0</priority>';
			$xmlString .= '</url>';
		}

		//Category pages
		$mapper = new Admin_Model_Mapper_Category();
		$categories = $mapper->nestedArray();

		foreach ($categories as $page) {
			$xmlString .= '<url>';
			$xmlString .= '<loc>'.$this->cfg['site_url'].'/category/'.$page['code'].'</loc>';
			$xmlString .= '<lastmod>' . date(DATE_ATOM, time()) . '</lastmod>';
			$xmlString .= '<changefreq>daily</changefreq>';
			$xmlString .= '<priority>1.0</priority>';
			$xmlString .= '</url>';

			if (isset($page['children']) && $page['children']) {
				foreach ($page['children'] as $child) {
					$xmlString .= '<url>';
					$xmlString .= '<loc>'.$this->cfg['site_url'].'/category/'.$page['code'].'/'.$child['code'].'</loc>';
					$xmlString .= '<lastmod>' . date(DATE_ATOM, time()) . '</lastmod>';
					$xmlString .= '<changefreq>daily</changefreq>';
					$xmlString .= '<priority>1.0</priority>';
					$xmlString .= '</url>';
				}
			}
		}

		//Product pages
		$mapper = new Application_Model_Mapper_Product();
		$products = $mapper->fetchActive();

		foreach ($products as $page) {
			$xmlString .= '<url>';
			$xmlString .= '<loc>'.$this->cfg['site_url'].'/product/'.$page->code.'</loc>';
			$xmlString .= '<lastmod>' . date(DATE_ATOM, time()) . '</lastmod>';
			$xmlString .= '<changefreq>daily</changefreq>';
			$xmlString .= '<priority>1.0</priority>';
			$xmlString .= '</url>';
		}

		//Blog pages
		$blog_status = ($settings_mapper->getValueByCode('blog_status'))?$settings_mapper->getValueByCode('blog_status'):'Enabled';
		if ($blog_status != 'Enabled') {
			$blog_url = ($settings_mapper->getValueByCode('blog_url'))?$settings_mapper->getValueByCode('blog_url'):'blog';

			$xmlString .= '<url>';
			$xmlString .= '<loc>'.$this->cfg['site_url'].'/'.$blog_url.'</loc>';
			$xmlString .= '<lastmod>' . date(DATE_ATOM, time()) . '</lastmod>';
			$xmlString .= '<changefreq>daily</changefreq>';
			$xmlString .= '<priority>1.0</priority>';
			$xmlString .= '</url>';

			//Blog articles
			$mapper = new Application_Model_Mapper_BlogPost();
			$posts = $mapper->fetchAllActiveArticles();

			foreach ($posts as $page) {
				$xmlString .= '<url>';
				$xmlString .= '<loc>'.$this->cfg['site_url'].'/'.$blog_url.'/'.$page->code.'</loc>';
				$xmlString .= '<lastmod>' . date(DATE_ATOM, time()) . '</lastmod>';
				$xmlString .= '<changefreq>daily</changefreq>';
				$xmlString .= '<priority>1.0</priority>';
				$xmlString .= '</url>';
			}
		}

		//FAQ page
		$faq_status = ($settings_mapper->getValueByCode('faq_status'))?$settings_mapper->getValueByCode('faq_status'):'Disabled';
		if ($faq_status == 'Enabled') {
			$faq_url = ($settings_mapper->getValueByCode('faq_url'))?$settings_mapper->getValueByCode('faq_url'):'faq';

			$xmlString .= '<url>';
			$xmlString .= '<loc>'.$this->cfg['site_url'].'/'.$faq_url.'</loc>';
			$xmlString .= '<lastmod>' . date(DATE_ATOM, time()) . '</lastmod>';
			$xmlString .= '<changefreq>daily</changefreq>';
			$xmlString .= '<priority>1.0</priority>';
			$xmlString .= '</url>';
		}

		//Video page
		$videos_status = ($settings_mapper->getValueByCode('videos_status'))?$settings_mapper->getValueByCode('videos_status'):'Disabled';
		if ($videos_status == 'Enabled') {
			$video_url = ($settings_mapper->getValueByCode('video_url'))?$settings_mapper->getValueByCode('video_url'):'video';

			$xmlString .= '<url>';
			$xmlString .= '<loc>'.$this->cfg['site_url'].'/'.$video_url.'</loc>';
			$xmlString .= '<lastmod>' . date(DATE_ATOM, time()) . '</lastmod>';
			$xmlString .= '<changefreq>daily</changefreq>';
			$xmlString .= '<priority>1.0</priority>';
			$xmlString .= '</url>';

			//video categories
			$mapper = new Application_Model_Mapper_VideoCategory();
			$categories = $mapper->fetchCategories();

			foreach ($categories as $page) {
				$xmlString .= '<url>';
				$xmlString .= '<loc>'.$this->cfg['site_url'].'/'.$video_url.'/'.$page->code.'</loc>';
				$xmlString .= '<lastmod>' . date(DATE_ATOM, time()) . '</lastmod>';
				$xmlString .= '<changefreq>daily</changefreq>';
				$xmlString .= '<priority>1.0</priority>';
				$xmlString .= '</url>';
			}
		}

		//custom integrations
		$path = APPLICATION_PATH.'/configs/custom.ini';
		if( file_exists($path) ){
			$xmlString .= '<sitemap>';
			$xmlString .= '<loc>'.$this->cfg['site_url'].'/sitemap/custom?f=.xml</loc>';
			$xmlString .= '</sitemap>';
		}

		$xmlString .= '</urlset>';

		$dom = new DOMDocument;
		$dom->preserveWhiteSpace = FALSE;
		$dom->loadXML($xmlString);

		$dom->save($_SERVER['DOCUMENT_ROOT'].'/sitemap.xml');
	}
}
