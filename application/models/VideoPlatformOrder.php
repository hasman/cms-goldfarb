<?php
class Application_Model_VideoPlatformOrder extends Application_Model_Abstract
{
	public $customer_id;
	public $bill_method;
	public $subtotal;
	public $discount;
	public $tax;
	public $total;
	public $confirmation_sent;
}
