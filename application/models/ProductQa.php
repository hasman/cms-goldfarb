<?php
class Application_Model_ProductQa extends Application_Model_Abstract
{
	public $product_id;
	public $product_variant_id;
	public $name;
	public $email;
	public $question;
	public $answer;
	public $status;
	public $is_spam;
	public $remote_addr;
	public $user_agent;
	public $referrer;
}
