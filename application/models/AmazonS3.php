<?php
class Application_Model_AmazonS3
{
	private $_cfg;
	private $_valid_mimetypes;
	private $_transcode_formats;
	private $_s3;
	private $_admin_user;

	public function __construct()
	{
		$this->_valid_mimetypes = ['video/3gp','video/3gpp','video/aac','video/asf','video/avi','video/x-msvideo','video/divx','video/flv','video/x-flv','video/m4a','video/mkv','video/mov','video/quicktime','video/mp4','video/mpeg','video/mpeg-ps','video/mpeg-ts','video/mxf','video/ogg','video/vob','video/webm','video/x-m4v','video/ms-asf','video/x-ms-wmv','video/x-msvideo'];
		$this->_transcode_formats = [
			'1080-hls' => ['preset_id'=>'1590612171522-hce0rw','extension'=>'mp4','format_name'=>'High','resolution'=>'1080'],
			'720-hls' => ['preset_id'=>'1590618579523-xsptom','extension'=>'mp4','format_name'=>'Medium','resolution'=>'720'],
			'480-hls' => ['preset_id'=>'1590760389511-z75yo3','extension'=>'mp4','format_name'=>'Low','resolution'=>'480'],
			'360-hls' => ['preset_id'=>'1590697717741-2d1s64','extension'=>'mp4','format_name'=>'Very low','resolution'=>'360'],
			//'roku-dash' => ['preset_id'=>'1351620000001-100060','extension'=>'mp4','format_name'=>'HLS (medium)','resolution'=>'1080'],
		];

		if (php_sapi_name() != 'cli') {
			$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

			$cache_dir = $options['dir']['cache'];
			$this->_frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
			$this->_backendOptions  = array( 'cache_dir' => $cache_dir );

			$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
			$this->_admin_user = $auth->getIdentity();
		}
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->_cfg['site_url'] = (isset($options['site']['full_secure'])) ? $options['site']['full_secure'] : null;

		if ((!isset($this->cfg['site_url']) || !$this->cfg['site_url']) && isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
			$this->_cfg['site_url'] = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
		}

		$this->_cfg['site_name'] = ($settings_mapper->getValueByCode('site_name'))?$settings_mapper->getValueByCode('site_name'):$options['site']['name'];
		if ((!isset($this->cfg['site_name']) || !$this->_cfg['site_name']) && isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$this->_cfg['site_name'] = $_SERVER["SERVER_NAME"];
		}

		$this->_cfg['amazon_access_key'] = ($settings_mapper->getValueByCode('amazon_access_key'))?$settings_mapper->getValueByCode('amazon_access_key'):"AKIA2GDALP73SHHYCIPP";
		$this->_cfg['amazon_secret_access_key'] = ($settings_mapper->getValueByCode('amazon_secret_access_key'))?$settings_mapper->getValueByCode('amazon_secret_access_key'):"XV3y9qbnjPIzgKgN60HbYiVq/0ned/uCaVp/dmLO";
		$this->_cfg['amazon_region'] = ($settings_mapper->getValueByCode('amazon_region'))?$settings_mapper->getValueByCode('amazon_region'):"us-east-1";
		$this->_cfg['video_folder'] = 'uploads';
		$this->_cfg['video_output_folder'] = 'videos';
		$this->_cfg['chunk_folder'] = 'chunks';
		$this->_cfg['pipeline'] = ($settings_mapper->getValueByCode('amazon_transcode_pipeline'))?$settings_mapper->getValueByCode('amazon_transcode_pipeline'):'1590613566605-7wdktp';

		//get cached bucket info
		//can't get options (and hence cache from CLI?
		if (php_sapi_name() != 'cli') {
			$cache_id = 'aws_bucket_info';
			$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);

			if(($cache->test($cache_id))){
				$cached_cfg = $cache->load($cache_id);

				$this->_cfg['input_bucket'] = $cached_cfg['input_bucket'];
				$this->_cfg['output_bucket'] = $cached_cfg['output_bucket'];
				$this->_cfg['thumbnail_bucket'] = $cached_cfg['thumbnail_bucket'];
			}
		}

		//need to get buckets from pipeline
		if (!isset($this->_cfg['input_bucket']) || !$this->_cfg['input_bucket'] || !isset($this->_cfg['output_bucket']) || !$this->_cfg['output_bucket']) {
			$cmd = 'aws elastictranscoder read-pipeline --id '.$this->_cfg['pipeline'];

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);

			/*
			echo "<pre>";
			print_r(json_decode($result));
			echo "</pre>";
			*/

			$data = json_decode($result);

			if ($data && isset($data->Pipeline->InputBucket)) {
				$this->_cfg['input_bucket'] = $data->Pipeline->InputBucket;
			}else {
				$this->_cfg['input_bucket'] = $data->Pipeline->ContentConfig->Bucket;
			}
			if ($data && isset($data->Pipeline->OutputBucket)) {
				$this->_cfg['output_bucket'] = $data->Pipeline->OutputBucket;
			}else {
				$this->_cfg['output_bucket'] = $data->Pipeline->ContentConfig->Bucket;
			}
			if ($data && isset($data->Pipeline->ThumbnailConfig->Bucket)) {
				$this->_cfg['thumbnail_bucket'] = $data->Pipeline->ThumbnailConfig->Bucket;
			}else {
				$this->_cfg['thumbnail_bucket'] = $data->Pipeline->ContentConfig->Bucket;
			}

			if (!$this->_cfg['input_bucket'] || !$this->_cfg['output_bucket']) {
				echo "Pipeline invalid or region not correct";
				die;
			}

			if (php_sapi_name() != 'cli') {
				$cached_cfg = $this->_cfg;

				$cache->save($cached_cfg, $cache_id, array('aws'));
			}
		}
		$this->_cfg['root_path'] = realpath(APPLICATION_PATH.'/../');

		//create directories if they don't exist
		if (!is_dir($this->_cfg['root_path'] . '/aws')) mkdir($this->_cfg['root_path'] . '/aws', 0777, true);
		if (!is_dir($this->_cfg['root_path'] . '/aws/input')) mkdir($this->_cfg['root_path'] . '/aws/input', 0777, true);
		if (!is_dir($this->_cfg['root_path'] . '/aws/output')) mkdir($this->_cfg['root_path'] . '/aws/output', 0777, true);
		if (!is_dir($this->_cfg['root_path'] . '/aws/playlist')) mkdir($this->_cfg['root_path'] . '/aws/playlist', 0777, true);
		if (!is_dir($this->_cfg['root_path'] . '/aws/meta')) mkdir($this->_cfg['root_path'] . '/aws/meta', 0777, true);
		if (!is_dir("{$this->_cfg['root_path']}/{$this->_cfg['chunk_folder']}")) mkdir("{$this->_cfg['root_path']}/{$this->_cfg['chunk_folder']}");

		//get and/or set lifecycle policy
		//can't get options (and hence cache from CLI?
		$result = null;
		if (php_sapi_name() != 'cli') {
			$cache_id = 'aws_bucket_lifecycle';
			$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);

			if(($cache->test($cache_id))){
				$result = $cache->load($cache_id);
			}
		}

		if (php_sapi_name() == 'cli' || !$result) {
			$cmd = 'aws s3api get-bucket-lifecycle --bucket ' . $this->_cfg['input_bucket'];
			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=" . $this->_cfg['amazon_access_key'] . "; \n" .
				"export AWS_SECRET_ACCESS_KEY=" . $this->_cfg['amazon_secret_access_key'] . "; \n" .
				"export AWS_DEFAULT_REGION=" . $this->_cfg['amazon_region'] . "; \n" .
				"export AWS_DEFAULT_OUTPUT=json; \n" .
				$cmd . "  2>&1"
			);

			if (php_sapi_name() != 'cli') {
				$cache->save($result, $cache_id, array('aws'));
			}
		}
		if (!$result) {
			$meta = '{"Rules":[{"ID":"Video Mulitpart File Rule","Status": "Enabled","Prefix": "lvp-","AbortIncompleteMultipartUpload": {"DaysAfterInitiation": 3}}]}';
			$metajson = $this->_cfg['root_path'].'/aws/meta/lifecycle.json';
			file_put_contents($metajson,$meta);

			$cmd = 'aws s3api put-bucket-lifecycle --bucket '.$this->_cfg['input_bucket'].' --lifecycle-configuration file://'.$metajson;

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);
		}

		$this->_s3 = new Zend_Service_Amazon_S3($this->_cfg['amazon_access_key'], $this->_cfg['amazon_secret_access_key']);
		//$this->_s3->registerStreamWrapper("s3");
	}

	public function checkMimeType($mime_type)
	{
		return in_array($mime_type,$this->_valid_mimetypes);
	}

	public function initiateMultipartFile($file_data)
	{
		if (!$this->checkMimeType($file_data['mime_type'])) {
			return array("status"=>"error","info"=>"Invalid file type: {$file_data['mime_type']}");
		}

		$path_parts = pathinfo($file_data['file_name']);

		if (isset($file_data['total_size']) && $file_data['total_size']) {
			$rnd = $file_data['total_size'];
		}else {
			$rnd = uniqid();
		}

		$hash = sha1($this->_cfg['site_url'].$rnd.time());

		$cmd = "aws s3api create-multipart-upload --bucket {$this->_cfg['input_bucket']} --key '{$this->_cfg['video_folder']}/{$hash}.{$path_parts['extension']}' --acl public-read --content-type '{$file_data['mime_type']}'";

		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);

		$json = json_decode($result);

		return array('status'=>'success','hash'=>$hash,'upload_id'=>$json->UploadId,'key'=>"{$this->_cfg['video_folder']}/{$hash}.{$path_parts['extension']}");
	}

	public function uploadChunk($chunk_data)
	{
		$path_parts = pathinfo($chunk_data['file_name']);

		$filename = "{$chunk_data['file_id']}-{$chunk_data['chunk_index']}.{$path_parts['extension']}";

		move_uploaded_file($chunk_data['file_tmp_name'], "{$this->_cfg['root_path']}/{$this->_cfg['chunk_folder']}/{$filename}");

		$cmd = 'aws s3api upload-part --bucket '.$this->_cfg['input_bucket'].' --key "'.$chunk_data['key'].'" --part-number '.$chunk_data['chunk_index'].' --body "'.$this->_cfg['root_path'].'/'.$this->_cfg['chunk_folder'].'/'.$filename.'" --upload-id  "'.$chunk_data['multipart_upload_id'].'"';

		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);

		unlink("{$this->_cfg['root_path']}/{$this->_cfg['chunk_folder']}/{$filename}");

		return array(
			'status' => 'success',
			'info' => "",
		);
	}

	public function chunkConcatenate($file_data, $id = null, $stream_association = 'video', $video_platform_video_id = null)
	{
		//$path_parts = pathinfo($file_data['file_name']);
		$path_parts = pathinfo($file_data['key']);
		$hash = $path_parts['filename'];

		//get parts
		$cmd = 'aws s3api list-parts --bucket '.$this->_cfg['input_bucket'].' --key "'.$file_data['key'].'" --upload-id "'.$file_data['multipart_upload_id'].'"';

		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);
		$json = json_decode($result);

		$meta = [];
		foreach ($json->Parts as $part) {
			$meta['Parts'][] = ['PartNumber'=>$part->PartNumber, 'ETag'=>$part->ETag];
		}

		$partsjson = $this->_cfg['root_path'].'/aws/meta/'.$file_data['file_name'].'-parts.json';
		file_put_contents($partsjson,json_encode($meta));

		$cmd = 'aws s3api complete-multipart-upload --multipart-upload "file://'.$partsjson.'" --bucket '.$this->_cfg['input_bucket'].' --key "'.$file_data['key'].'" --upload-id "'.$file_data['multipart_upload_id'].'"';

		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);

		$fileinfo = $this->_s3->getInfo($this->_cfg['input_bucket'] . '/' . $file_data['key']);

		/*
		if (!$this->checkMimeType($file_data['mime_type'])) {
			$this->_s3->removeObject($this->_cfg['input_bucket'] . '/' . $file_data['key']);

			return array("status"=>"error","info"=>"Invalid file type: {$file_data['mime_type']}");
		}
		*/

		if(!in_array($fileinfo['type'],$this->_valid_mimetypes)){
			$this->_s3->removeObject($this->_cfg['input_bucket'] . '/' . $file_data['key']);

			return array(
				'status' => 'error',
				'info' => "invalid file format: {$fileinfo['type']}",
			);
		}

		if ($stream_association == 'trailer') { //trailer
			$stream_mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();

			if ($id) {
				$videos = $stream_mapper->fetchByTrailerId($id);

				foreach ($videos as $stream) {
					$this->deleteTrailerStream($stream->id);

					$stream_mapper->delete($stream);
				}

				$old_video = $mapper->find($id);

				$this->_s3->removeObject($this->_cfg['input_bucket'] . '/' . $this->_cfg['video_folder'] . '/' . $old_video->filename_internal);
			}

			$data = array(
				'id' => $id,
				'hash' => $hash,
				'video_platform_video_id' => $video_platform_video_id,
				'filename' => $file_data['file_name'],
				'filename_internal' => $path_parts['filename'] . "." . $path_parts['extension'],
				'filesize' => (isset($fileinfo['size'])) ? $fileinfo['size'] : null,
				'mimetype' => (isset($fileinfo['type'])) ? $fileinfo['type'] : null,
				'etag' => (isset($fileinfo['etag'])) ? str_replace('"', '', $fileinfo['etag']) : null,
				'notify_email' => (isset($this->_admin_user)) ? $this->_admin_user->email : null,
				'transcode_status' => 'Processing',
				'status' => 'Enabled',
				'public_url' => ''
			);
			if ($old_video) {
				$data = array_merge($old_video->toArray(), $data);
			}

			$video_id = $mapper->save(new Application_Model_VideoPlatformVideoTrailer($data));

			if ($id) {
				$this->transcode($id, null, 'trailer');
			}
		}elseif ($stream_association == 'extra') {
			$stream_mapper = new Admin_Model_Mapper_VideoPlatformVideoExtrasStream();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();

			if ($id) {
				$videos = $stream_mapper->fetchByExtrasId($id);

				foreach ($videos as $stream) {
					$this->deleteExtrasStream($stream->id);

					$stream_mapper->delete($stream);
				}

				$old_video = $mapper->find($id);

				$this->_s3->removeObject($this->_cfg['input_bucket'] . '/' . $this->_cfg['video_folder'] . '/' . $old_video->filename_internal);
			}

			$data = array(
				'id' => $id,
				'hash' => $hash,
				'video_platform_video_id' => $video_platform_video_id,
				'filename' => $file_data['file_name'],
				'filename_internal' => $path_parts['filename'] . "." . $path_parts['extension'],
				'filesize' => (isset($fileinfo['size'])) ? $fileinfo['size'] : null,
				'mimetype' => (isset($fileinfo['type'])) ? $fileinfo['type'] : null,
				'etag' => (isset($fileinfo['etag'])) ? str_replace('"', '', $fileinfo['etag']) : null,
				'notify_email' => (isset($this->_admin_user)) ? $this->_admin_user->email : null,
				'transcode_status' => 'Processing',
				'status' => 'Enabled',
				'public_url' => ''
			);
			if ($old_video) {
				$data = array_merge($old_video->toArray(), $data);
			}

			$video_id = $mapper->save(new Application_Model_VideoPlatformVideoExtra($data));

			if ($id) {
				$this->transcode($id, null, 'extra');
			}
		}else { //main video
			$stream_mapper = new Admin_Model_Mapper_VideoPlatformVideoStream();
			$mapper = new Admin_Model_Mapper_VideoPlatformVideo();

			if ($id) {
				$videos = $stream_mapper->fetchByVideoId($id);

				foreach ($videos as $stream) {
					$this->deleteStream($stream->id);

					$stream_mapper->delete($stream);
				}

				$old_video = $mapper->find($id);

				$this->_s3->removeObject($this->_cfg['input_bucket'] . '/' . $this->_cfg['video_folder'] . '/' . $old_video->filename_internal);
			}

			$data = array(
				'id' => $id,
				'hash' => $hash,
				'filename' => $file_data['file_name'],
				'filename_internal' => $path_parts['filename'].".".$path_parts['extension'],
				'filesize' => (isset($fileinfo['size'])) ? $fileinfo['size'] : null,
				'mimetype' => (isset($fileinfo['type'])) ? $fileinfo['type'] : null,
				'etag' => (isset($fileinfo['etag'])) ? str_replace('"', '', $fileinfo['etag']) : null,
				'notify_email' => (isset($this->_admin_user))?$this->_admin_user->email:null,
				'transcode_status' => 'Processing',
				'status' => 'Enabled',
				'public_url' => ''
			);
			if ($old_video) {
				$data = array_merge($old_video->toArray(), $data);
			}

			$video_id = $mapper->save(new Application_Model_VideoPlatformVideo($data));

			if ($id) {
				$this->transcode($id);
			}
		}

		return array(
			'status' => 'success',
			'info' => "",
			'video_id' => $video_id
		);
	}

	public function uploadFileToS3($local_file, $remote_folder)
	{
		$path_parts = pathinfo($local_file);
		$mimetype = mime_content_type($local_file);

		/*
		if(!in_array($mimetype,$this->_valid_mimetypes)){
			echo "invalid file format: {$mimetype}";
			return false;
		}
		*/

		$dest_file_name = "/".$this->_cfg['video_output_folder']."/".$remote_folder."/".$path_parts['filename'].".".$path_parts['extension'];

		$fp = fopen($local_file, "rb");//read as stream to reduce memory usage

		$result = $this->_s3->putObject($this->_cfg['output_bucket'] . $dest_file_name, $fp,
			array(Zend_Service_Amazon_S3::S3_CONTENT_TYPE_HEADER => $mimetype,
				Zend_Service_Amazon_S3::S3_ACL_HEADER => Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ)
		);

		if ($result) {
			unlink($local_file);

			return $public_url = "https://".$this->_cfg['output_bucket'].".s3.".$this->_cfg['amazon_region'].".amazonaws.com/".$this->_cfg['video_output_folder']."/".$remote_folder."/".$path_parts['filename'].".".$path_parts['extension'];
		}else {
			return false;
		}

	}

	public function transcode($video_id, $format = null, $type = 'video')
	{
		if ($type == 'trailer') {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
		}elseif ($type == 'extra') {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
		}else {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
		}
		$video = $mapper->find($video_id);
		//set transcode status to processing
		$video->transcode_status = 'Processing';
		$mapper->save($video);

		$filename = $video->filename_internal;
		$path_parts = pathinfo($filename);
		$hash = $path_parts['filename'];

		$format_name = 'HLS';
		$resolution = 'Adaptive';
		$outputfile = $hash.'.m3u8';

		//create the hash subfolder on AWS so we can write to it
		$cmd = 'aws s3api put-object --bucket '.$this->_cfg['input_bucket'].' --key '.$this->_cfg['video_output_folder'].'/'.$hash.'/';
		//echo $cmd."<br/>";

		//	return json for submit as $exec
		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);


		$input = '[{"Key":"'.$this->_cfg['video_folder'].'/'.$filename.'","FrameRate":"auto","Resolution":"auto","AspectRatio":"auto","Interlaced":"auto","Container":"mp4"}]';
		$inputjson = $this->_cfg['root_path'].'/aws/input/'.$filename.'.json';
		file_put_contents($inputjson,$input);

		$output = []; $output_keys = []; $first_output = true;
		foreach ($this->_transcode_formats as $key => $format_info) {
			$output_data = array(
				'Key' => $hash.'_'.$key,
				'Rotate' => 'auto',
				'PresetId' => $format_info['preset_id'],
				'SegmentDuration' => '5.0'
			);
			if ($first_output) {
				$output_data['ThumbnailPattern'] = $hash.'_thumb-{count}';
			}

			$output[] = $output_data;

			$first_output = false;

			$output_keys[] = '"'.$path_parts['filename'].'_'.$key.'"';
		}
		$outputjson = $this->_cfg['root_path'].'/aws/output/'.$filename.'-'.$format_name.'.json';
		file_put_contents($outputjson,json_encode($output));

		$playlist = '[{"Name":"'.$path_parts['filename'].'","Format":"HLSv3","OutputKeys":['.implode(",",$output_keys).'],"HlsContentProtection":{"Method":"aes-128","KeyStoragePolicy":"WithVariantPlaylists"}}]';
		$playlistjson = $this->_cfg['root_path'].'/aws/playlist/'.$filename.'-'.$format['preset_id'].'.json';
		file_put_contents($playlistjson,$playlist);

		$meta = '{"copyright":"'.$this->_cfg['site_name'].'"}'; //,"title":"'.str_ireplace(array('(',')','@',',',';',':','\\','"','/','[',']','?','=','{','}','&'),"",$video->title).'" //TODO - find better string replacement function to make AWS not throw errors
		$metajson = $this->_cfg['root_path'].'/aws/meta/'.$filename.'.json';
		file_put_contents($metajson,$meta);

		$cmd = 'aws elastictranscoder create-job --pipeline-id '.$this->_cfg['pipeline'].' --inputs file://'.$inputjson.' --outputs file://'.$outputjson.' --user-metadata file://'.$metajson.' --playlists file://'.$playlistjson.' --output-key-prefix "'.$this->_cfg['video_output_folder'].'/'.$hash.'/"';
		//echo $cmd."<br/>";

		//	return json for submit as $exec
		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);

		//Cny_Debug::prettyPrint($result, true, true);

		$data = json_decode($result);

		if ($type == 'trailer') {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
			$mapper->save(new Application_Model_VideoPlatformVideoTrailerStream(array(
				'video_platform_trailer_id' => $video_id,
				'filename' => $outputfile,
				'format_name' => $format_name,
				'resolution' => $resolution,
				'transcode_status' => 'Processing',
				'aws_job_id' => $data->Job->Id
			)));
		}elseif ($type == 'extra') {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtrasStream();
			$mapper->save(new Application_Model_VideoPlatformVideoExtrasStream(array(
				'video_platform_extras_id' => $video_id,
				'filename' => $outputfile,
				'format_name' => $format_name,
				'resolution' => $resolution,
				'transcode_status' => 'Processing',
				'aws_job_id' => $data->Job->Id
			)));
		}else {
			$mapper = new Admin_Model_Mapper_VideoPlatformVideoStream();
			$mapper->save(new Application_Model_VideoPlatformVideoStream(array(
				'video_platform_video_id' => $video_id,
				'filename' => $outputfile,
				'format_name' => $format_name,
				'resolution' => $resolution,
				'transcode_status' => 'Processing',
				'aws_job_id' => $data->Job->Id
			)));
		}

		return true;
	}

	public function jobStatus($job_id)
	{
		//aws elastictranscoder read-job --id 1533838012294-example

		$cmd = 'aws elastictranscoder read-job --id '.$job_id;
		//echo $cmd."<br/>";


		//	return json for submit as $exec
		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);

		/*
		echo "<pre>";
		print_r(json_decode($result));
		echo "</pre>";
		*/

		return json_decode($result);

		exit;
	}

	public function getVideoURL($video_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
		$video = $mapper->find($video_id);

		if ($video->public_url) {
			return $video->public_url;
		}elseif ($video->filename_internal) {
			$public_url = "https://".$this->_cfg['input_bucket'].".s3.".$this->_cfg['amazon_region'].".amazonaws.com/".$this->_cfg['video_folder']."/".$video->filename_internal;

			$video->public_url = $public_url;
			$mapper->save($video);

			return $public_url;
		}else {
			return null;
		}
	}

	public function getExpiringVideoURL($video_id, $expiration_time = '+5 days')
	{ //Expiration limit too strict - not going to be useful
		$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
		$video = $mapper->find($video_id);

		$expires_in_seconds = 3600;

		//aws s3 presign s3://awsexamplebucket/test2.txt --expires-in 604800

		$cmd = 'aws s3 presign s3://'.$this->_cfg['input_bucket'].'/'.$this->_cfg['video_folder'].'/'.$video->filename_internal.' --expires-in '.$expires_in_seconds;
		echo $cmd."<br/>";


		//	return json for submit as $exec
		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);

		echo $result."<br/><br/>";


		/*
		$cmd = $s3Client->getCommand('GetObject', [
			'Bucket' => 'my-bucket',
			'Key' => 'testKey'
		]);

		$request = $s3Client->createPresignedRequest($cmd, '+20 minutes');
		//$presignedUrl = (string)$request->getUri();


		$cmd = $this->_s3->getCommand('GetObject', ['Bucket'=>$this->_cfg['output_bucket'],'Key'=>$this->_cfg['video_output_folder'].'/'.$video->filename_internal]);

		$result = $this->_s3->createPresignedRequest($cmd, $expiration_time);

		echo "<pre>";
		print_r(json_decode($result));
		echo "</pre>";
		*/

		exit;
	}

	public function getVideoStreamURL($stream_id, $video_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoStream();
		$video = $mapper->findValidated($stream_id, $video_id);

		if ($video->public_url) {
			return $video->public_url;
		}else {
			$source_mapper = new Admin_Model_Mapper_VideoPlatformVideo();
			$source = $source_mapper->find($video_id);

			/*
			//set permissions so the file can be read publicly
			$cmd = "aws s3api put-object-acl --bucket ".$this->_cfg['output_bucket']." --key ".$this->_cfg['video_output_folder']." --acl public-read";

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);
			//Cny_Debug::prettyPrint($result,true);

			//not getting the response in the docs for put-object-acl so checking here again UGH ;(
			*/

			//set cache-control to none to avoid caching issue on m3u8 file
			$cmd = "aws s3 cp s3://".$this->_cfg['output_bucket']."/".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename." s3://".$this->_cfg['output_bucket']."/".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename." --cache-control no-cache --acl public-read";

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);

			$cmd = "aws s3api get-object-acl --bucket ".$this->_cfg['output_bucket']." --key ".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename;

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);
			//Cny_Debug::prettyPrint($result,true);

			$data = json_decode($result);
			//check if Grants -> Grantee -> URI http://acs.amazonaws.com/groups/global/AllUsers has Grants -> Grantee -> Permission READ
			if ($data) {
				foreach ($data->Grants as $grant) {
					if (stripos($grant->Grantee->URI,'groups/global/AllUsers') !== false) {
						if (stripos($grant->Permission, 'READ') !== false || stripos($grant->Permission, 'FULL_CONTROL') !== false) {
							$public_url = "https://".$this->_cfg['output_bucket'].".s3.".$this->_cfg['amazon_region'].".amazonaws.com/".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename;

							$video->public_url = $public_url;
							$mapper->save($video);

							return $public_url;
						}
					}
				}
			}

			return null;
		}
	}

	public function getVideoTrailerURL($video_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
		$video = $mapper->find($video_id);

		if ($video->trailer_video_platform_video_id) {
			return $this->getVideoURL($video->trailer_video_platform_video_id);
		}

		if ($video->public_url) {
			return $video->public_url;
		}elseif ($video->filename_internal) {
			$public_url = "https://".$this->_cfg['input_bucket'].".s3.".$this->_cfg['amazon_region'].".amazonaws.com/".$this->_cfg['video_folder']."/".$video->filename_internal;

			$video->public_url = $public_url;
			$mapper->save($video);

			return $public_url;
		}else {
			return null;
		}
	}

	public function getVideoTrailerStreamURL($stream_id, $video_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
		$video = $mapper->findValidated($stream_id, $video_id);

		if ($video->public_url) {
			return $video->public_url;
		}else {
			$source_mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
			$source = $source_mapper->find($video_id);

			/*
			//set permissions so the file can be read publicly
			$cmd = "aws s3api put-object-acl --bucket ".$this->_cfg['output_bucket']." --key ".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename." --acl public-read";

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);
			//Cny_Debug::prettyPrint($result,true);

			//not getting the response in the docs for put-object-acl so checking here again UGH ;(
			*/

			//set cache-control to none to avoid caching issue on m3u8 file
			$cmd = "aws s3 cp s3://".$this->_cfg['output_bucket']."/".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename." s3://".$this->_cfg['output_bucket']."/".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename." --cache-control no-cache --acl public-read";

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);

			$cmd = "aws s3api get-object-acl --bucket ".$this->_cfg['output_bucket']." --key ".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename;

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);
			//Cny_Debug::prettyPrint($result,true);

			$data = json_decode($result);
			//check if Grants -> Grantee -> URI http://acs.amazonaws.com/groups/global/AllUsers has Grants -> Grantee -> Permission READ
			if ($data) {
				foreach ($data->Grants as $grant) {
					if (stripos($grant->Grantee->URI,'groups/global/AllUsers') !== false) {
						if (stripos($grant->Permission, 'READ') !== false || stripos($grant->Permission, 'FULL_CONTROL') !== false) {
							$public_url = "https://".$this->_cfg['output_bucket'].".s3.".$this->_cfg['amazon_region'].".amazonaws.com/".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename;

							$video->public_url = $public_url;
							$mapper->save($video);

							return $public_url;
						}
					}
				}
			}

			return null;
		}
	}

	public function deleteVideo($video_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideo();
		$video = $mapper->find($video_id);

		$this->_s3->removeObject($this->_cfg['input_bucket'] . '/' . $this->_cfg['video_folder'] . '/' . $video->filename_internal);

		$mapper = new Admin_Model_Mapper_VideoPlatformVideoStream();
		$videos = $mapper->fetchByVideoId($video_id);

		foreach ($videos as $stream) {
			$this->deleteStream($stream->id);
		}

		//trailer
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
		$trailer = $mapper->fetchByVideoId($video_id);

		if ($trailer) {
			$this->deleteTrailer($trailer->id);
		}

		$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
		$videos = $mapper->fetchByTrailerId($video_id);

		foreach ($videos as $stream) {
			$this->deleteTrailerStream($stream->id);
		}

		//extras
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
		$stream_mapper = new Admin_Model_Mapper_VideoPlatformVideoExtrasStream();
		$extras = $mapper->fetchByVideoId($video_id);

		if ($extras) {
			foreach ($extras as $extra) {
				$this->deleteExtra($extra->id);

				$videos = $stream_mapper->fetchByExtrasId($video_id);

				foreach ($videos as $stream) {
					$this->deleteExtrasStream($stream->id);
				}
			}
		}
	}

	public function deleteStream($stream_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoStream();
		$video = $mapper->find($stream_id);

		$source_mapper = new Admin_Model_Mapper_VideoPlatformVideo();
		$source = $source_mapper->find($video->video_platform_video_id);

		//$this->_s3->removeObject($this->_cfg['output_bucket'] . '/' . $this->_cfg['video_output_folder'] . '/' . $video->filename);

		$path_parts = pathinfo($video->filename);

		$cmd = "aws s3 rm s3://".$this->_cfg['output_bucket'] . '/' . $this->_cfg['video_output_folder'] .'/'. $source->hash . '/ --recursive --exclude "*" --include "' . $path_parts['filename'] .'*" ';
		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);
		//Cny_Debug::prettyPrint($result,true);
	}

	public function deleteTrailer($video_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
		$video = $mapper->find($video_id);

		$this->_s3->removeObject($this->_cfg['input_bucket'] . '/' . $this->_cfg['video_folder'] . '/' . $video->filename_internal);

		$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
		$videos = $mapper->fetchByTrailerId($video_id);

		foreach ($videos as $stream) {
			$this->deleteTrailerStream($stream->id);
		}
	}

	public function deleteTrailerStream($stream_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailerStream();
		$video = $mapper->find($stream_id);

		$source_mapper = new Admin_Model_Mapper_VideoPlatformVideoTrailer();
		$source = $source_mapper->find($video->video_platform_trailer_id);

		//$this->_s3->removeObject($this->_cfg['output_bucket'] . '/' . $this->_cfg['video_output_folder'] . '/' . $video->filename);

		$path_parts = pathinfo($video->filename);

		$cmd = "aws s3 rm s3://".$this->_cfg['output_bucket'] . '/' . $this->_cfg['video_output_folder'] . '/' . $source->hash . '/ --recursive --exclude "*" --include "' . $path_parts['filename'] .'*" ';
		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);
		//Cny_Debug::prettyPrint($result,true);
	}

	public function getVideoExtrasURL($video_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
		$video = $mapper->find($video_id);

		if ($video->public_url) {
			return $video->public_url;
		}else {
			$public_url = "https://".$this->_cfg['input_bucket'].".s3.".$this->_cfg['amazon_region'].".amazonaws.com/".$this->_cfg['video_folder']."/".$video->filename_internal;

			$video->public_url = $public_url;
			$mapper->save($video);

			return $public_url;
		}
	}

	public function getVideoExtrasStreamURL($stream_id, $video_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtrasStream();
		$video = $mapper->findValidated($stream_id, $video_id);

		if ($video->public_url) {
			return $video->public_url;
		}else {
			$source_mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
			$source = $source_mapper->find($video_id);

			/*
			//set permissions so the file can be read publicly
			$cmd = "aws s3api put-object-acl --bucket ".$this->_cfg['output_bucket']." --key ".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename." --acl public-read";

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);
			//Cny_Debug::prettyPrint($result,true);

			//not getting the response in the docs for put-object-acl so checking here again UGH ;(
			*/

			//set cache-control to none to avoid caching issue on m3u8 file
			$cmd = "aws s3 cp s3://".$this->_cfg['output_bucket']."/".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename." s3://".$this->_cfg['output_bucket']."/".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename." --cache-control no-cache --acl public-read";

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);

			$cmd = "aws s3api get-object-acl --bucket ".$this->_cfg['output_bucket']." --key ".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename;

			$result = shell_exec(
				"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
				"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
				"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
				"export AWS_DEFAULT_OUTPUT=json; \n".
				$cmd."  2>&1"
			);
			//Cny_Debug::prettyPrint($result,true);

			$data = json_decode($result);
			//check if Grants -> Grantee -> URI http://acs.amazonaws.com/groups/global/AllUsers has Grants -> Grantee -> Permission READ
			if ($data) {
				foreach ($data->Grants as $grant) {
					if (stripos($grant->Grantee->URI,'groups/global/AllUsers') !== false) {
						if (stripos($grant->Permission, 'READ') !== false || stripos($grant->Permission, 'FULL_CONTROL') !== false) {
							$public_url = "https://".$this->_cfg['output_bucket'].".s3.".$this->_cfg['amazon_region'].".amazonaws.com/".$this->_cfg['video_output_folder']."/".$source->hash."/".$video->filename;

							$video->public_url = $public_url;
							$mapper->save($video);

							return $public_url;
						}
					}
				}
			}

			return null;
		}
	}

	public function deleteExtra($video_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
		$video = $mapper->find($video_id);

		$this->_s3->removeObject($this->_cfg['input_bucket'] . '/' . $this->_cfg['video_folder'] . '/' . $video->filename_internal);

		$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtrasStream();
		$videos = $mapper->fetchByExtrasId($video_id);

		foreach ($videos as $stream) {
			$this->deleteExtrasStream($stream->id);
		}
	}

	public function deleteExtrasStream($stream_id)
	{
		$mapper = new Admin_Model_Mapper_VideoPlatformVideoExtrasStream();
		$video = $mapper->find($stream_id);

		$source_mapper = new Admin_Model_Mapper_VideoPlatformVideoExtra();
		$source = $source_mapper->find($video->video_platform_extras_id);

		//$this->_s3->removeObject($this->_cfg['output_bucket'] . '/' . $this->_cfg['video_output_folder'] . '/' . $video->filename);

		$path_parts = pathinfo($video->filename);

		$cmd = "aws s3 rm s3://".$this->_cfg['output_bucket'] . '/' . $this->_cfg['video_output_folder'] . '/' . $source->hash . '/ --recursive --exclude "*" --include "' . $path_parts['filename'] .'*" ';
		$result = shell_exec(
			"export AWS_ACCESS_KEY_ID=".$this->_cfg['amazon_access_key']."; \n".
			"export AWS_SECRET_ACCESS_KEY=".$this->_cfg['amazon_secret_access_key']."; \n".
			"export AWS_DEFAULT_REGION=".$this->_cfg['amazon_region']."; \n".
			"export AWS_DEFAULT_OUTPUT=json; \n".
			$cmd."  2>&1"
		);
		//Cny_Debug::prettyPrint($result,true);
	}
}
