<?php
class Application_Model_VideoPlatformOrderItem extends Application_Model_Abstract
{
	public $video_platform_order_id;
	public $video_platform_video_id;
	public $title;
	public $price;
	public $discount;
	public $tax;
	public $total;
	public $purchase_type;
}
