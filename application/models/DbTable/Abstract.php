<?php
class Application_Model_DbTable_Abstract extends Zend_Db_Table_Abstract
{
	public function __construct()
	{
		$options = Zend_Registry::get('configuration')->toArray();
		$table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';

		$this->_name = $table_prefix.$this->_name;

		return parent::__construct();
	}
}
?>
