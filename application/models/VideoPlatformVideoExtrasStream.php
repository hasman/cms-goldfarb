<?php
class Application_Model_VideoPlatformVideoExtrasStream extends Application_Model_Abstract
{
	public $video_platform_extras_id;
	public $filename;
	public $format_name;
	public $resolution;
	public $transcode_status;
	public $aws_job_id;
	public $public_url;
}
