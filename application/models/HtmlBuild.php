<?php
use Cocur\Slugify\Slugify;

class Cny_Model_HtmlBuild
{
	var $css;
	var $font_families = array();
	static $_defaultView;
	protected $_view;
	var $_wireframe_mode;
	var $module_style_block;


	protected function getDefaultView()
	{
		if (self::$_defaultView === null) {
			self::$_defaultView = new Zend_View();
			self::$_defaultView->setScriptPath(APPLICATION_PATH.'/views/scripts');
			self::$_defaultView->setHelperPath(APPLICATION_PATH.'/views/helpers');
		}
		return self::$_defaultView;
	}

	public function __construct($wireframe_mode = false)
	{
		$this->_view = self::getDefaultView();

		$this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$options = Zend_Registry::get('configuration')->toArray();
		//$this->domain = $options['site']['full_secure'];

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array('automatic_serialization' => true, 'lifetime' => $settings_mapper->getValueByCode('page_cache_lifetime'));
		$this->frontendOptionsCSS = array('automatic_serialization' => true, 'lifetime' => $settings_mapper->getValueByCode('css_cache_lifetime'));
		$this->backendOptions = array('cache_dir' => $cache_dir);

		$this->_video_settings_yt = ",startAt:0,showControls:false,quality:'highres',mute:false,autoPlay:true,loop:true,opacity:1,realfullscreen:true,optimizeDisplay:true";
		$this->_video_settings_generic = ' data-bgvideo="true" data-bgvideo-show-pause-play="false" data-bgvideo-pause-after="0"  data-bgvideo-fade-in="0" ';

		$this->_wireframe_mode = $wireframe_mode;

		$this->module_style_block = '';
	}

	private function isempty($var) {
		if (isset($var)) {
			if (!empty($var) || $var === '0' || $var === 0) {
				return false;
			}
		}

		return true;
	}

	public function buildModule($id, $row_settings = null, $wireframe_override = false, $page_id = null)
	{
		//no reason to cache modules since doing full page cache for now
/*
		$cache_id = 'module_'.$id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);

		if(!($cache->test($cache_id)) || $this->_wireframe_mode) {
*/
			$mapper = new Application_Model_Mapper_PageSectionRowColumnModule();
			$module = $mapper->find($id);

			$content = json_decode($module->json_content);
			$styles = json_decode($module->json_styles);
			$settings = json_decode($module->json_settings);
			$div_class = '';
			$data_attributes = '';

			//if (!$content) return '';
			if ($module->desktop_status == 'Disabled' && $module->tablet_status == 'Disabled' && $module->mobile_status == 'Disabled') {
				return '';
			}

			//hide for specific sizes
			if ($module->desktop_status == 'Disabled') {
				$div_class .= ' hidden-lg';
			}
			if ($module->tablet_status == 'Disabled') {
				$div_class .= ' hidden-sm hidden-md';
			}
			if ($module->mobile_status == 'Disabled') {
				$div_class .= ' hidden-xs';
			}

			//custom classes to append
			if ($module->class) {
				$div_class .= ' '.$module->class;
			}
			//special classes to append for split column modules
			if ($module->class_special) {
				$div_class .= ' '.$module->class_special;
			}else {
				$div_class .= ' split-none';
			}
			//set equal height for modules if set on row and on module
			if (isset($row_settings->equal_height) && $row_settings->equal_height == "Enabled"
				&& (!isset($settings->equal_height) || (isset($settings->equal_height) && $settings->equal_height == "Enabled")))
			{

				$div_class .= " js-equal-height";
			}

			//animation settings
		if (isset($settings->animation)  &&  $settings->animation == "animate"
			&& (!isset($settings->animation_scroll) || (isset($settings->animation_scroll) && $settings->animation_scroll == ""))) {
				$div_class .= ' '.$settings->animation;

				if (isset($settings->animation_effect)) {
					$data_attributes .= ' data-animation-effect="'.$settings->animation_effect.'"';
				}
				if (isset($settings->animation_duration)) {
					$div_class .= ' '.$settings->animation_duration;
				}
			}

		//animation scroll settings
		if (isset($settings->animation) && $settings->animation == 'animate' && (isset($settings->animation_scroll) && $settings->animation_scroll == 'on') ) {
			$div_class .= ' section-motion-scroll';

			if (isset($settings->animation_scroll)) {
				$data_attributes .= ' data-animation-tween="'.$settings->animation_scroll_effect.'"';
			}
		}
			//background hover zoom settings
			if (isset($settings->background_hover_zoom_status) && $settings->background_hover_zoom_status == 'on') {
				if (isset($settings->background_hover_zoom_percent)) {
					$data_attributes .= ' data-background-hover-zoom-percent="'.$settings->background_hover_zoom_percent.'"';
				}
			}

			//parallax paroller
			if (isset($settings->parallax_factor) && $settings->parallax_factor) {
				$data_attributes .= ' data-paroller-factor="'.$settings->parallax_factor.'"';

				if (isset($settings->parallax_factor_md)) {
					$data_attributes .= ' data-paroller-factor-md="'.$settings->parallax_factor_md.'"';
				}
				if (isset($settings->parallax_factor_xs)) {
					$data_attributes .= ' data-paroller-factor-xs="'.$settings->parallax_factor_xs.'"';
				}
			}

			//apply shade
			if (isset($settings->apply_shade) && $settings->apply_shade == 'on') {
				$div_class .= ' apply-shade-module';
				if (isset($settings->shade_color) && $settings->shade_color) {
					$data_attributes .= ' data-shade-color="'.$settings->shade_color.'"';
				}
			}

			$this->_view->config = array(
				'div_class' => $div_class,
				'data_attributes' => $data_attributes
			);
			$this->_view->content = $content;
			$this->_view->styles = $styles;
			$this->_view->settings = $settings;
			$this->_view->module = $module;

			if (!$page_id) {
				$sql = $this->_db->quoteInto("SELECT ps.page_id 
					FROM page_section_row_column_modules AS psrcm 
					LEFT JOIN page_section_row_columns AS psrc ON psrc.id = psrcm.page_section_row_column_id
					LEFT JOIN page_section_rows AS psr ON psr.id = psrc.page_section_row_id
					LEFT JOIN page_sections AS ps ON psr.page_section_id = ps.id
					WHERE psrcm.id = ?", $id);
				$page_id = $this->_db->fetchOne($sql);
			}
			$this->_view->page_id = $page_id;

			if ($this->_wireframe_mode && !$wireframe_override) {
				//$this->_view->wireframe_settings = json_decode($module->wireframe_json);
				//$html = $this->_view->render("/html-build/wireframe/{$module->type}.phtml");
				//wireframe mode removed - just in case going to generate normal page
				$html = $this->_view->render("/html-build/modules/{$module->type}.phtml");
			}else {
				$html = $this->_view->render("/html-build/modules/{$module->type}.phtml");
			}

/*
			if (!$this->_wireframe_mode) {
				$cache->save($html, $cache_id, array('module_' . $id, 'page_' . $page_id));
			}
		}else {
			$html = $cache->load($cache_id);
		}
*/
		return $html;
	}

	public function buildRow($id, $fullwidth = false, $wireframe_override = false, $page_id = null)
	{
		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$row = $mapper->find($id);
		$row_settings = json_decode($row->json_settings);

		$mapper = new Admin_Model_Mapper_PageSectionRowColumn();
		$columns = $mapper->getPageSectionRowColumns($id);

		$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();

		$html = '';
		if ($columns) {
			$vp = new Cny_Model_VideoParser();

			foreach ($columns as $column) {
				$modules = $mapper->getPageSectionRowColumnModules($column->id);

				$class = $column->class;
				if (!$fullwidth) $class .= ' col-sm-'.$column->col_class;


				$html .= '<div id="column-block-'.$column->id.'" class="module-block' . $class . '">';

				if ($modules) {
					foreach ($modules as $module) {
						$class = '';
						$styles = json_decode($module->json_styles);
						//new fullwidth row setting
						if (isset($row_settings->full_width) && $row_settings->full_width == 'Enabled') $class .= ' p-r-0 p-l-0';

						$html .= '<div id="module-block-'.$module->id.'" class="module-block ' . $class .'">';

						if ($styles && array_key_exists('video_url', $styles) && $styles->video_url) {
							$video_details = $vp->getMediaDetails(($styles->video_url));
							$video_poster_url =  (isset($styles->video_poster)) ? $styles->video_poster : "";

							if ($video_details['type'] == 'generic') {
								$html .= '<video class="jquery-background-video" '.$this->_video_settings_generic.' loop autoplay muted playsinline poster="'.$video_poster_url.'">
											<source src="'.$video_details['embed_url'].'" type="video/mp4">
										</video>';
							}else {
								$html .= '<div class="yt_player" data-property="{videoURL:\'' . $video_details['video_id'] . '\',containment:\'#module-block-' . $module->id . '\''.$this->_video_settings_yt.'}"></div>';
							}
						}

						$html .= $this->buildModule($module->id, $row_settings, $wireframe_override, $page_id);

						$html .= '</div>';
					}
				}

				$html .= '</div>';
			}
		}

		return $html;
	}

	public function buildSection($id, $fullwidth = false, $margin = true, $wireframe_override = false, $page_id = null)
	{
		$orig_fullwidth = $fullwidth;

		$mapper = new Admin_Model_Mapper_PageSectionRow();
		$rows = $mapper->getPageSectionRows($id);

		$margin_class = '';
		if ($margin) {
			$margin_class = 'margin-bottom-30';
		}

		$html = '';
		if ($rows) {
			$vp = new Cny_Model_VideoParser();

			foreach ($rows as $row) {
				$fullwidth = $orig_fullwidth;

				$styles = json_decode($row->json_styles);
				$settings = json_decode($row->json_settings);
				$row_class = $row->class;

				if ($row->desktop_status == 'Disabled' && $row->tablet_status == 'Disabled' && $row->mobile_status == 'Disabled') {
					continue;
				}

				//need to see if there is a module set as "full width" or not
				if (!$mapper->rowContainsFullwidthModule($row->id)) {
					//$row_class .= ' container';
				}
				if (!$fullwidth) {
					$row_class .= ' row';
				}elseif ($fullwidth && ($mapper->getRowModuleCount($row->id) > 1 && !(isset($settings->full_width) && $settings->full_width == 'Enabled')) || (isset($settings->fixed_full_width) && $settings->fixed_full_width == 'Enabled')) {
					//if it's fullwidth AND has multiple columns then we need a new class and override
					$row_class = $row->class .= ' container';
					$fullwidth = false; //on module need to unset fullwidth otherwise multi columns are set to full width
				}

				//new full width setting on the row
				if (isset($settings->full_width) && $settings->full_width == 'Enabled') {
					$row_class =  $row->class . ' fullwidth';
					$fullwidth = false;
				}

				$div_class = '';
				//hide for specific sizes
				if ($row->desktop_status == 'Disabled') {
					$div_class .= ' hidden-lg';
				}
				if ($row->tablet_status == 'Disabled') {
					$div_class .= ' hidden-sm hidden-md';
				}
				if ($row->mobile_status == 'Disabled') {
					$div_class .= ' hidden-xs';
				}

				//animation settings
				$data_attributes = '';
				if (isset($settings->animation)  &&  $settings->animation == "animate"
					&& (!isset($settings->animation_scroll) || (isset($settings->animation_scroll) && $settings->animation_scroll == ""))) {
					$row_class .= ' '.$settings->animation;

					if (isset($settings->animation_effect)) {
						$data_attributes .= ' data-animation-effect="'.$settings->animation_effect.'"';
					}
					if (isset($settings->animation_duration)) {
						$row_class .= ' '.$settings->animation_duration;
					}
				}

				//animation scroll settings
				if (isset($settings->animation) && (isset($settings->animation_scroll) && $settings->animation_scroll == 'on') ) {
					$div_class .= ' section-motion-scroll';

					if (isset($settings->animation_scroll)) {
						$data_attributes .= ' data-animation-tween="'.$settings->animation_scroll_effect.'"';
					}
				}
				//background hover zoom settings
				if (isset($settings->background_hover_zoom_status) && $settings->background_hover_zoom_status == 'on') {
					if (isset($settings->background_hover_zoom_percent)) {
						$data_attributes .= ' data-background-hover-zoom-percent="'.$settings->background_hover_zoom_percent.'"';
					}
				}

				//parallax paroller
				if (isset($settings->parallax_factor) && $settings->parallax_factor) {
					$data_attributes .= ' data-paroller-factor="'.$settings->parallax_factor.'"';

					if (isset($settings->parallax_factor_md)) {
						$data_attributes .= ' data-paroller-factor-md="'.$settings->parallax_factor_md.'"';
					}
					if (isset($settings->parallax_factor_xs)) {
						$data_attributes .= ' data-paroller-factor-xs="'.$settings->parallax_factor_xs.'"';
					}
				}

				//set equal height init for modules
				if (isset($settings->equal_height) && $settings->equal_height == "Enabled") {
					$div_class .= " equal-height-init";
				}

				//column spacing overide
				if (isset($settings->column_spacing) && $settings->column_spacing) {
					$data_attributes .= ' data-column-spacing="'.$settings->column_spacing.'"';

				}

				//apply shade
				if (isset($settings->apply_shade) && $settings->apply_shade == 'on') {
					$div_class .= ' apply-shade';
					if (isset($settings->shade_color) && $settings->shade_color) {
						$data_attributes .= ' data-shade-color="'.$settings->shade_color.'"';
					}
				}

				//vertical alignment
				if (isset($settings->row_vert_align) && ($settings->row_vert_align == "flex-align-center" || $settings->row_vert_align == "flex-align-end") ) {
					$row_class .= " row-height flex-row flex-wrap $settings->row_vert_align";
				}

				$html .= '<div id="row-block-'.$row->id.'" class="row-block '.$row_class.' '.$margin_class.' '.$div_class.'" '.$data_attributes.'>';

				if ($styles && array_key_exists('video_url', $styles) && $styles->video_url) {
					$video_details = $vp->getMediaDetails(($styles->video_url));
					$video_poster_url =  (isset($styles->video_poster)) ? $styles->video_poster : "";

					if ($video_details['type'] == 'generic') {
						$html .= '<video class="jquery-background-video" '.$this->_video_settings_generic.' loop autoplay muted playsinline poster="'.$video_poster_url.'">
											<source src="'.$video_details['embed_url'].'" type="video/mp4">
										</video>';
					}else {
						$html .= '<div class="yt_player" data-property="{videoURL:\'' . $video_details['video_id'] . '\',containment:\'#row-block-' . $row->id . '\'' . $this->_video_settings_yt . '}"></div>';
					}
				}

				$html .= $this->buildRow($row->id, $fullwidth, $wireframe_override, $page_id);

				$html .= '</div>';
			}
		}

		return $html;
	}

	public function buildPage($id, $cache_page = true, $parent_page_id = null, $block = false)
	{
		$cache_id = 'page_'.$id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);

		if(!($cache->test($cache_id)) || $cache_page === false || $this->_wireframe_mode) {
			$mapper = new Application_Model_Mapper_Page();
			$page = $mapper->find($id);

			if (!$page) return;

			$html = '';

			$mapper = new Application_Model_Mapper_PageSection();
			$sections = $mapper->getPageSections($id);

			if ($sections) {
				$slugify = new Slugify();
				$vp = new Cny_Model_VideoParser();

				foreach ($sections as $section) {
					$columns = $mapper->countSectionColumns($section->id);
					$fullwidth = false;

					if ($section->desktop_status == 'Disabled' && $section->tablet_status == 'Disabled' && $section->mobile_status == 'Disabled') {
						continue;
					}

					$div_class = '';
					//hide for specific sizes
					if ($section->desktop_status == 'Disabled') {
						$div_class .= ' hidden-lg';
					}
					if ($section->tablet_status == 'Disabled') {
						$div_class .= ' hidden-sm hidden-md';
					}
					if ($section->mobile_status == 'Disabled') {
						$div_class .= ' hidden-xs';
					}

					$styles = json_decode($section->json_styles);
					$settings = json_decode($section->json_settings);

					//animation settings
					$data_attributes = '';
					if (isset($settings->animation)  &&  $settings->animation == "animate"
						&& (!isset($settings->animation_scroll) || (isset($settings->animation_scroll) && $settings->animation_scroll == ""))) {
						$div_class .= ' '.$settings->animation;

						if (isset($settings->animation_effect)) {
							$data_attributes .= ' data-animation-effect="'.$settings->animation_effect.'"';
						}
						if (isset($settings->animation_duration)) {
							$div_class .= ' '.$settings->animation_duration;
						}

					}

					//animation scroll settings
					if (isset($settings->animation) && $settings->animation == 'animate' && (isset($settings->animation_scroll) && $settings->animation_scroll == 'on') ) {
						$div_class .= ' section-motion-scroll';

						if (isset($settings->animation_scroll)) {
							$data_attributes .= ' data-animation-tween="'.$settings->animation_scroll_effect.'"';
						}
					}

					//section animation settings
					if (isset($settings->section_animation) && $settings->section_animation == 'animate') {
						$div_class .= ' section-swipe';
					}

					//background hover zoom settings
					if (isset($settings->background_hover_zoom_status) && $settings->background_hover_zoom_status == 'on') {
						if (isset($settings->background_hover_zoom_percent)) {
							$data_attributes .= ' data-background-hover-zoom-percent="'.$settings->background_hover_zoom_percent.'"';
						}
					}

					//parallax paroller
					if (isset($settings->parallax_factor) && $settings->parallax_factor) {
						$data_attributes .= ' data-paroller-factor="'.$settings->parallax_factor.'"';

						if (isset($settings->parallax_factor_md)) {
							$data_attributes .= ' data-paroller-factor-md="'.$settings->parallax_factor_md.'"';
						}
						if (isset($settings->parallax_factor_xs)) {
							$data_attributes .= ' data-paroller-factor-xs="'.$settings->parallax_factor_xs.'"';
						}
					}

					//force full height then center content
					if (isset($settings->section_full_height) && $settings->section_full_height == 'on') {
						$div_class .= ' section-full-height';

						//vertical alignment
						if (isset($settings->section_vert_align) && $settings->section_vert_align ) {
							$div_class .= " $settings->section_vert_align";
						}


					}

					//anchor tags
					$anchor_tag = (isset($settings->label) && $settings->label)?$slugify->slugify($settings->label):"section-{$section->id}";
					$html .= '<a name="'.$anchor_tag.'"></a>';

					//Ken Burns Effect
					if (isset($settings->kb_animation) && $settings->kb_animation == 'kb_animate') {
						$scale_class = ' '.$settings->kb_effect;
					}

					//apply shade
					if (isset($settings->apply_shade) && $settings->apply_shade == 'on') {
						$div_class .= ' apply-shade';
						if (isset($settings->shade_color) && $settings->shade_color) {
							$data_attributes .= ' data-shade-color="'.$settings->shade_color.'"';
						}
					}

					switch ($section->type) {
						case 'standard':
							if (!$block) $div_class .= ' container';

							$html .= '<div id="section-block-'.$section->id.'" class="section-block '.$section->class.' '.$div_class.'" '.$data_attributes.'>';

							if (isset($settings->kb_animation) && $settings->kb_animation == 'kb_animate') {
								$html .= '<div class="background-scale-wrap">
											<div class="background-scale background-scale--kb'.$scale_class.'"><img src="'.$styles->background_image.'" alt=""></div>
										  </div>';
							}

							if ($styles && array_key_exists('video_url', $styles) && $styles->video_url) {
								$video_details = $vp->getMediaDetails(($styles->video_url));
								$video_poster_url =  (isset($styles->video_poster)) ? $styles->video_poster : "";

								if ($video_details['type'] == 'generic') {
									$html .= '<video class="jquery-background-video" '.$this->_video_settings_generic.' loop autoplay muted playsinline poster="'.$video_poster_url.'">
											<source src="'.$video_details['embed_url'].'" type="video/mp4">
										</video>';
								}else {
									$html .= '<div class="yt_player" data-property="{videoURL:\'' . $video_details['video_id'] . '\',containment:\'#section-block-' . $section->id . '\'' . $this->_video_settings_yt . '}"></div>';
								}
							}

							break;
						case 'fullwidth':
							$fullwidth = true;

							$html .= '<div id="section-block-'.$section->id.'" class="section-block '.$section->class.' '.$div_class.' margin-bottom-30" '.$data_attributes.'>';

							if (isset($settings->kb_animation) && $settings->kb_animation == 'kb_animate') {
								$html .= '<div class="background-scale-wrap">
											<div class="background-scale background-scale--kb'.$scale_class.'"><img src="'.$styles->background_image.'" alt=""></div>
										  </div>';
							}

							if ($styles && array_key_exists('video_url', $styles) && $styles->video_url) {
								$video_details = $vp->getMediaDetails(($styles->video_url));
								$video_poster_url =  (isset($styles->video_poster)) ? $styles->video_poster : "";

								if ($video_details['type'] == 'generic') {
									$html .= '<video class="jquery-background-video" '.$this->_video_settings_generic.' loop autoplay muted playsinline poster="'.$video_poster_url.'">
										<source src="'.$video_details['embed_url'].'" type="video/mp4">
									</video>';
								}else {
									$html .= '<div class="yt_player" data-property="{videoURL:\'' . $video_details['video_id'] . '\',containment:\'#section-block-' . $section->id . '\'' . $this->_video_settings_yt . '}"></div>';
								}
							}
							break;
						case 'custom':

							$html .= '<div id="section-block-'.$section->id.'" class="custom_section section-block '.$section->class.' '.$div_class.'" '.$data_attributes.'>';

							if (isset($settings->kb_animation) && $settings->kb_animation == 'kb_animate') {
								$html .= '<div class="background-scale-wrap">
											<div class="background-scale background-scale--kb'.$scale_class.'"><img src="'.$styles->background_image.'" alt=""></div>
										  </div>';
							}

							if ($styles && array_key_exists('video_url', $styles) && $styles->video_url) {
								$video_details = $vp->getMediaDetails(($styles->video_url));
								$video_poster_url =  (isset($styles->video_poster)) ? $styles->video_poster : "";

								if ($video_details['type'] == 'generic') {
									$html .= '<video class="jquery-background-video" '.$this->_video_settings_generic.' loop autoplay muted playsinline poster="'.$video_poster_url.'">
											<source src="'.$video_details['embed_url'].'" type="video/mp4">
										</video>';
								}else {
									$html .= '<div class="yt_player" data-property="{videoURL:\'' . $video_details['video_id'] . '\',containment:\'#section-block-' . $section->id . '\'' . $this->_video_settings_yt . '}"></div>';
								}
							}
							break;
						default:
							break;
					}

					$html .= $this->buildSection($section->id, $fullwidth, true, false, ($parent_page_id)?$parent_page_id:$id);

					$html .= '</div>';
				}
			}

			if ($cache_page) {
				if (!$this->_wireframe_mode) {
					$cache->save($html, $cache_id, array('pages', 'page_' . $id));
				}
			}
		}else {
			$html = $cache->load($cache_id);
		}

		if (!$block) { //can only aggregate module style blocks if not a "page block" otherwise there is no way to get those CSS rules into the page
			preg_match_all('/<style.*?>(.+?)<\/style>/si', $html, $matches);
			if (isset($matches[1])) {
				foreach ($matches[1] as $style_block) {
					$this->module_style_block .= $style_block;
				}
			}

			return preg_replace("/<style.*?>.+?<\/style>/si","",$html);
		}else {
			return $html;
		}
	}

	//deprecated
	//TODO - remove references to this function in custom integrations
	public function buildPageCSS($page_id, $cache_page = true)
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$css = '';

		$cache_id = 'css_page_'.$page_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);

		if(!($cache->test($cache_id)) || $cache_page === false || $this->_wireframe_mode) {
			$this->pageCSS($page_id);

			//footer CSS
			$mapper = new Application_Model_Mapper_Page();
			$footer = $mapper->doesExists(['code'=>'footer']);
			$this->pageCSS($footer->id);
			//mobile footer CSS
			$mobile_footer = $mapper->doesExists(['code'=>'mobile_footer']);
			$this->pageCSS($mobile_footer->id);

			$this->css .= $settings_mapper->getValueByCode('custom_css');

			if ($cache_page === true) {
				if (!$this->_wireframe_mode) {
					$cache->save($this->css, $cache_id, array('page_' . $page_id));
				}
			}

			$css = $this->css;
		}else {
			$css = $cache->load($cache_id);
		}

		echo $css;
	}

	public function buildSiteCSS($cache_page = true, $page_id = null)
	{
		//minor issue where $page_id is set to 0 but that never gets cleared/refreshed without a full cache flush
		//set 0 to null to fix
		if ($page_id == 0) $page_id = null;

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$css = '';

		$cache_id = 'css_site'.$page_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptionsCSS, $this->backendOptions);

		if(!($cache->test($cache_id)) || $cache_page === false || $this->_wireframe_mode) {
			$css = $this->buildCSS($page_id);

			if ($cache_page === true) {
				if (!$this->_wireframe_mode) {
					$tags = array();

					/*
					$mapper = new Application_Model_Mapper_Page();
					$pages = $mapper->fetchAll();

					foreach ($pages as $page) {
						$tags[] = 'page_'.$page->id;
					}
					*/

					$cache->save($css, $cache_id, $tags);
				}
			}
		}else {
			$css = $cache->load($cache_id);
		}

		echo $css;
	}

	public function buildCSS($page_id = null)
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->pageCSS($page_id);

		$this->css .= $settings_mapper->getValueByCode('custom_css');

		return $this->css;
	}

	public function pageCSS($page_id = null)
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		//general site/page CSS
		$rules = $mobile_rules = array();
		//button element
		$rules['.module-block .btn, .auth-page .login-form form .btn.btn--cms-default']['custom_font'] = $settings_mapper->getValueByCode('button_font');
		//global button settings
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['background-color'] = $settings_mapper->getValueByCode('global_button_color');
		$rules['.module-block .btn--cms-default:hover, .auth-page .login-form form .btn.btn--cms-default:hover']['background-color'] = $settings_mapper->getValueByCode('global_button_hover_color');
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['border-width'] = $settings_mapper->getValueByCode('global_button_border_size');
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['border-color'] = $settings_mapper->getValueByCode('global_button_border_color');
		$rules['.module-block .btn--cms-default:hover, .auth-page .login-form form .btn.btn--cms-default']['border-color'] = $settings_mapper->getValueByCode('global_button_border_hover_color');
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['custom_font'] = $settings_mapper->getValueByCode('global_button_font');
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['font-size'] = $settings_mapper->getValueByCode('global_button_text_size');
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['font-weight'] = $settings_mapper->getValueByCode('global_button_text_weight');
		$rules['.module-block .btn--cms-default, .module-block .btn--cms-default:visited, .auth-page .login-form form .btn.btn--cms-default, .auth-page .login-form form .btn.btn--cms-default:visited']['color'] = $settings_mapper->getValueByCode('global_button_text_color');
		$rules['.module-block.btn--cms-default:hover, .module-block .btn--cms-default:hover:visited, .auth-page .login-form form .btn.btn--cms-default, .auth-page .login-form form .btn.btn--cms-default:hover:visited']['color'] = $settings_mapper->getValueByCode('global_button_text_hover_color');
		if ($settings_mapper->getValueByCode('global_button_width')) {
			$rules['.module-block .btn--cms-default']['max-width'] = $settings_mapper->getValueByCode('global_button_width');
			$rules['.module-block .btn--cms-default']['width'] = '100%';
		}
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['padding-top'] = $settings_mapper->getValueByCode('global_button_v_padding');
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['padding-bottom'] = $settings_mapper->getValueByCode('global_button_v_padding');
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['padding-left'] = $settings_mapper->getValueByCode('global_button_h_padding');
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['padding-right'] = $settings_mapper->getValueByCode('global_button_h_padding');
		$rules['.module-block .btn--cms-default, .auth-page .login-form form .btn.btn--cms-default']['margin-top'] = $settings_mapper->getValueByCode('global_button_top_margin');

		//global font settings
		$rules['body']['custom_font'] = $settings_mapper->getValueByCode('global_font');
		$rules['body']['color'] = $settings_mapper->getValueByCode('global_text_color');
		$rules['body']['font-weight'] = $settings_mapper->getValueByCode('global_font_weight');
		$rules['body']['font-size'] = $settings_mapper->getValueByCode('global_font_size');
		$rules['body']['line-height'] = $settings_mapper->getValueByCode('global_font_lineheight');
		//link settings
		$rules['a, a:active, a:visited, a:focus']['color'] = $settings_mapper->getValueByCode('global_link_color');
		$rules['a:visited, a:focus']['color'] = $settings_mapper->getValueByCode('global_link_visited_color');
		$rules['a:hover']['color'] = $settings_mapper->getValueByCode('global_link_hover_color');
		$rules['a, a:visited, a:active, a:focus']['text-decoration'] = $settings_mapper->getValueByCode('global_link_decoration');
		$rules['a:hover']['text-decoration'] = $settings_mapper->getValueByCode('global_link_hover_decoration');
		//breadcrumb settings
		$rules['.breadcrumb li, .breadcrumb li a']['color'] = $settings_mapper->getValueByCode('global_link_color');
		$rules['.breadcrumb li.active, .breadcrumb li a:hover, .breadcrumb li a']['color'] = $settings_mapper->getValueByCode('global_link_hover_color');
		//H1-6 tag settings
		$rules['h1, .h1']['custom_font'] = $settings_mapper->getValueByCode('h1_font');
		$rules['h1, .h1']['font-weight'] = $settings_mapper->getValueByCode('h1_font_weight');
		$rules['h1, .h1']['font-size'] = $settings_mapper->getValueByCode('h1_font_size');
		$rules['h1, .h1']['line-height'] = $settings_mapper->getValueByCode('h1_font_lineheight');
		$rules['h1, .h1, .header .h1']['color'] = $settings_mapper->getValueByCode('h1_text_color');
		$rules['h2']['custom_font'] = $settings_mapper->getValueByCode('h2_font');
		$rules['h2']['font-weight'] = $settings_mapper->getValueByCode('h2_font_weight');
		$rules['h2']['font-size'] = $settings_mapper->getValueByCode('h2_font_size');
		$rules['h2']['line-height'] = $settings_mapper->getValueByCode('h2_font_lineheight');
		$rules['h2']['color'] = $settings_mapper->getValueByCode('h2_text_color');
		$rules['h3']['custom_font'] = $settings_mapper->getValueByCode('h3_font');
		$rules['h3']['font-weight'] = $settings_mapper->getValueByCode('h3_font_weight');
		$rules['h3']['font-size'] = $settings_mapper->getValueByCode('h3_font_size');
		$rules['h3']['line-height'] = $settings_mapper->getValueByCode('h3_font_lineheight');
		$rules['h3']['color'] = $settings_mapper->getValueByCode('h3_text_color');
		$rules['h4']['custom_font'] = $settings_mapper->getValueByCode('h4_font');
		$rules['h4']['font-weight'] = $settings_mapper->getValueByCode('h4_font_weight');
		$rules['h4']['font-size'] = $settings_mapper->getValueByCode('h4_font_size');
		$rules['h4']['line-height'] = $settings_mapper->getValueByCode('h4_font_lineheight');
		$rules['h4']['color'] = $settings_mapper->getValueByCode('h4_text_color');
		$rules['h5']['custom_font'] = $settings_mapper->getValueByCode('h5_font');
		$rules['h5']['font-weight'] = $settings_mapper->getValueByCode('h5_font_weight');
		$rules['h5']['font-size'] = $settings_mapper->getValueByCode('h5_font_size');
		$rules['h5']['line-height'] = $settings_mapper->getValueByCode('h5_font_lineheight');
		$rules['h5']['color'] = $settings_mapper->getValueByCode('h5_text_color');
		$rules['h6']['custom_font'] = $settings_mapper->getValueByCode('h6_font');
		$rules['h6']['font-weight'] = $settings_mapper->getValueByCode('h6_font_weight');
		$rules['h6']['font-size'] = $settings_mapper->getValueByCode('h6_font_size');
		$rules['h6']['line-height'] = $settings_mapper->getValueByCode('h6_font_lineheight');
		$rules['h6']['color'] = $settings_mapper->getValueByCode('h6_text_color');

		//page background settings
		$rules['body']['background-color'] = $settings_mapper->getValueByCode('body_background_color');
		$rules['body']['background-image'] = $settings_mapper->getValueByCode('body_background_image');
		if ($settings_mapper->getValueByCode('body_background_image')) {
			$rules['body']['background-size'] = 'cover';
			$rules['body']['background-position'] = 'center';
		}

		//site identity settings
		$rules['.header .navbar-brand']['font-size'] = $settings_mapper->getValueByCode('site_name_font_size');
		$rules['.header .navbar-brand']['line-height'] = $settings_mapper->getValueByCode('site_name_font_height');
		$rules['.header .navbar-brand']['custom_font'] = $settings_mapper->getValueByCode('site_name_font');
		$rules['.header .navbar-brand, .header .navbar-brand:hover, .header .navbar-brand:visited, .header .navbar-brand:focus, .header .navbar-brand:active']['color'] = $settings_mapper->getValueByCode('site_name_color');
		$rules['.header .navbar-brand']['font-weight'] = $settings_mapper->getValueByCode('site_name_font_weight');
		//navigation settings
		$rules['.header .nav li']['custom_font'] = $settings_mapper->getValueByCode('nav_font');
		$rules['.header']['font-weight'] = $settings_mapper->getValueByCode('nav_font_weight');

		$rules['.header .navbar-nav>li>a, .header--custom .navbar--flex .navbar-nav>li>a, .navbar-controls .search-btn']['color'] = $settings_mapper->getValueByCode('nav_text_color');

		$rules['.header .navbar-nav>li:hover>a, .header.header--custom .navbar--flex .navbar-nav>li:hover>a, .header .navbar-nav>li:focus>a, .header.header--custom .navbar--flex .navbar-nav>li:focus>a, .header .navbar-nav>li.current>a, .header.header--custom .navbar--flex .navbar-nav>li.current>a, .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover, .navbar-controls .search-btn:hover']['color'] = $settings_mapper->getValueByCode('nav_hover_text_color');

		$rules['.header .navbar-nav>li:hover>a, .header.header--custom .navbar--flex .navbar-nav>li:hover>a, .header .navbar-nav>li:focus>a, .header.header--custom .navbar--flex .navbar-nav>li:focus>a, .header .navbar-nav>li.current>a, .header.header--custom .navbar--flex .navbar-nav>li.current>a']['border-bottom-color'] = $settings_mapper->getValueByCode('nav_hover_text_color');

		$rules['.header .navbar-nav>li:hover, .header.header--custom .navbar--flex .navbar-nav>li:hover, .header .navbar-nav>li:focus, .header.header--custom .navbar--flex .navbar-nav>li:focus, .header .navbar-nav>li.current, .header.header--custom .navbar--flex .navbar-nav>li.current']['border-bottom-color'] = $settings_mapper->getValueByCode('nav_hover_text_color');

		$rules['.header .dropdown-menu']['border-top-color'] = $settings_mapper->getValueByCode('nav_hover_text_color');

		$rules['.header .navbar-nav>li:hover, .header .navbar-nav>li>a:hover, .header .navbar-nav>li:focus, .header .navbar-nav>li>a:focus, .header .navbar-nav>li.current, .header .navbar-nav>li.current>a']['background-color'] = $settings_mapper->getValueByCode('nav_hover_background_color');

		$rules['.header .navbar-nav>li:active, .header .navbar-nav>li.current>a, .header.header-fixed-shrink .navbar-nav>li.current>a']['color'] = $settings_mapper->getValueByCode('nav_active_text_color');

		$rules['.header .navbar-toggle, .header .navbar-toggle:focus, .header .navbar-toggle:hover']['background-color'] = $settings_mapper->getValueByCode('nav_hover_background_color').'!important';
		$rules['.header .navbar-toggle span, .header .navbar-toggle .fa']['color'] = $settings_mapper->getValueByCode('nav_hover_text_color');
		$rules['.header .navbar-default .navbar-toggle .icon-bar']['background-color'] = $settings_mapper->getValueByCode('nav_hover_text_color');
		$rules['.header .navbar-nav>li>a, .dropdown .dropdown-submenu>a']['text-transform'] = $settings_mapper->getValueByCode('nav_text_case');
		$rules['.header .navbar-bottom']['background-color'] = $settings_mapper->getValueByCode('nav_bottom_link_bg_color');
		$rules['.header .nav>li>a, .header--custom .navbar--flex .navbar-header-wrap .nav>li>a, .header--custom .navbar--flex .navbar-header-right .navbar-account .nav.nav-links>li>a']['font-size'] = $settings_mapper->getValueByCode('nav_font_size');
		$rules['.header .nav>li>a, .header--custom .navbar--flex .navbar-header-wrap .nav>li>a, .header--custom .navbar--flex .navbar-header-right .navbar-account .nav.nav-links>li>a']['line-height'] = $settings_mapper->getValueByCode('nav_font_lineheight');
		$rules['.header .cart-count-badge']['color'] = $settings_mapper->getValueByCode('header_cart_badge_font_color');
		$rules['.header .cart-count-badge']['background-color'] = $settings_mapper->getValueByCode('header_cart_badge_color');


		//navigation background settings
		$rules['.header, .header.header--custom .navbar--flex']['background-color'] = $settings_mapper->getValueByCode('nav_background_color');
		$rules['.header, .header.header--custom .navbar--flex']['background-image'] = $settings_mapper->getValueByCode('nav_background_image');
		if ($settings_mapper->getValueByCode('nav_background_image')) {
			$rules['.header .navbar']['background-color'] = 'transparent !important';
		}
		$background_image_style = $settings_mapper->getValueByCode('nav_background_image_style');

		switch($background_image_style) {
			case "cover":
				$rules['.header, .header.header--custom .navbar--flex']['background-size'] = 'cover';
				$rules['.header, .header.header--custom .navbar--flex']['background-position'] = 'center';
				break;

			case "contain":
				$rules['.header, .header.header--custom .navbar--flex']['background-size'] = 'contain';
				$rules['.header, .header.header--custom .navbar--flex']['background-position'] = 'center';
				break;
			case "repeat":
				$rules['.header, .header.header--custom .navbar--flex']['background-repeat'] = 'repeat';
				$rules['.header, .header.header--custom .navbar--flex']['background-position'] = 'center';
				break;
			case "no-repeat":
				$rules['.header, .header.header--custom .navbar--flex']['background-repeat'] = 'no-repeat';
				$rules['.header, .header.header--custom .navbar--flex']['background-position'] = 'center';
				break;
			default:
		}

		//nav social icons navbar-social social-media-list
		$rules['.header .navbar-social a .fa, .header .topbar .loginbar.social-media-list>li .fa, .header ul.loginbar.social-media-list>li .fa']['font-size'] = $settings_mapper->getValueByCode('nav_social_font_size');
		$rules['.header .navbar-social a .fa, .header .topbar .loginbar.social-media-list>li .fa, .header ul.loginbar.social-media-list>li .fa']['color'] = $settings_mapper->getValueByCode('nav_social_color');
		$rules['.header .navbar-social a:hover .fa, .header .topbar .loginbar.social-media-list>li:hover .fa, .header ul.loginbar.social-media-list>li:hover .fa']['color'] = $settings_mapper->getValueByCode('nav_social_hover_color');
		$rules['.header .navbar-social a .fa, .header .topbar .loginbar.social-media-list>li .fa, .header ul.loginbar.social-media-list>li .fa, .header .topbar-v3']['background-color'] = $settings_mapper->getValueByCode('nav_social_background_color');
		$rules['.header .navbar-social a:hover .fa, .header .topbar .loginbar.social-media-list>li:hover .fa, .header ul.loginbar.social-media-list>li:hover .fa']['background-color'] = $settings_mapper->getValueByCode('nav_social_hover_background_color');
		$rules['.header .topbar .loginbar.social-media-list>li+li, .header--custom .navbar--flex .navbar-header-right .navbar-social .nav>li+li']['margin-left'] = $settings_mapper->getValueByCode('nav_social_spacing');

		//secondary nav
		$rules['.header .navbar-toggle']['border-color'] = $settings_mapper->getValueByCode('nav_hover_text_color');
		$rules['.header .dropdown-menu li a']['color'] = $settings_mapper->getValueByCode('nav_secondary_text_color');
		$rules['.header .dropdown-menu li a']['background-color'] = $settings_mapper->getValueByCode('nav_secondary_background_color');
		$rules['.header .dropdown-menu li a:hover']['color'] = $settings_mapper->getValueByCode('nav_secondary_hover_text_color');
		$rules['.header .dropdown-menu li a:hover']['background-color'] = $settings_mapper->getValueByCode('nav_secondary_hover_background_color');
		if ($option = $settings_mapper->getValueByCode('nav_border_bottom') == 'Disable') {
			$rules['.header']['border-bottom'] = '0!important;';
		}
		$rules['.header .dropdown-menu li a']['custom_font'] = $settings_mapper->getValueByCode('nav_secondary_font');
		$rules['.header .dropdown-menu li a']['font-weight'] = $settings_mapper->getValueByCode('nav_secondary_font_weight');
		$rules['.header .dropdown-menu li a']['font-size'] = $settings_mapper->getValueByCode('nav_secondary_font_size');
		$rules['.header .dropdown-menu li a']['line-height'] = $settings_mapper->getValueByCode('nav_secondary_font_lineheight');

		//sticky nav settings
		$rules['.header.header-fixed-shrink .nav li, .header.header--custom.header-fixed-shrink .navbar--flex .nav li']['custom_font'] = $settings_mapper->getValueByCode('nav_sticky_font');
		$rules['.header.header-fixed-shrink, .header.header--custom.header-fixed-shrink .navbar--flex']['font-weight'] = $settings_mapper->getValueByCode('nav_sticky_font_weight');
		$rules['.header.header-fixed-shrink .nav>li>a, .header--custom.header-fixed-shrink .navbar--flex .navbar-header-wrap .nav>li>a, .header--custom.header-fixed-shrink .navbar--flex .navbar-header-right .navbar-account .nav.nav-links>li>a']['line-height'] = $settings_mapper->getValueByCode('nav_sticky_font_lineheight');
		$rules['.header.header-fixed-shrink .nav>li>a, .header--custom.header-fixed-shrink .navbar--flex .navbar-header-wrap .nav>li>a, .header--custom.header-fixed-shrink .navbar--flex .navbar-header-right .navbar-account .nav.nav-links>li>a']['font-size'] = $settings_mapper->getValueByCode('nav_sticky_font_size');
		$rules['.header.header-fixed-shrink, .header.header--custom.header-fixed-shrink .navbar--flex, .header--centered.header-fixed-shrink']['background-color'] = $settings_mapper->getValueByCode('nav_sticky_background_color').'!important';
		$rules['.header.header-fixed-shrink .navbar-toggle, .header.header--custom.header-fixed-shrink .navbar--flex .navbar-toggle, .header.header-fixed-shrink .navbar-toggle:focus, .header.header--custom.header-fixed-shrink .navbar--flex .navbar-toggle:focus, .header.header-fixed-shrink .navbar-toggle:hover, .header.header--custom.header-fixed-shrink .navbar--flex .navbar-toggle:hover']['background-color'] = $settings_mapper->getValueByCode('nav_sticky_background_color');
		$rules['.header.header-fixed-shrink, .header.header--custom.header-fixed-shrink .navbar--flex']['background-color'] = $settings_mapper->getValueByCode('nav_sticky_hover_background_color');
		$rules['.header.header-fixed-shrink .navbar-nav>li>a, .header.header--custom.header-fixed-shrink .navbar--flex .navbar-nav>li>a, .navbar-controls .search-btn']['color'] = $settings_mapper->getValueByCode('nav_sticky_text_color');
		$rules['.header.header-fixed-shrink .navbar-nav>li:hover, .header.header--custom.header-fixed-shrink .navbar--flex .navbar-nav>li:hover']['color'] = $settings_mapper->getValueByCode('nav_sticky_hover_text_color');
		$rules['.header.header-fixed-shrink .navbar-nav>li:active, .header.header--custom.header-fixed-shrink .navbar--flex .navbar-nav>li:active']['color'] = $settings_mapper->getValueByCode('nav_sticky_active_text_color');

		//special rule for Navbar padding for Fixed position options
		if ($settings_mapper->getValueByCode('nav_fixed_padding') && $settings_mapper->getValueByCode('nav_fixed_position') != '') {
			if ($settings_mapper->getValueByCode('nav_fixed_position') == 'navbar-fixed-bottom') {
				$rules['body']['padding-bottom'] = $settings_mapper->getValueByCode('nav_fixed_padding');
			}else {
				$rules['body']['padding-top'] = $settings_mapper->getValueByCode('nav_fixed_padding');
			}
		}

		//header search window in nav styling
		$rules['.header .search-open']['background-color'] = $settings_mapper->getValueByCode('header_search_window_background_color');
		$rules['.header .search-open .input-group-btn button']['color'] = $settings_mapper->getValueByCode('header_search_btn_font_color');
		$rules['.header .search-open .input-group-btn button:hover']['color'] = $settings_mapper->getValueByCode('header_search_btn_font_hover_font_color');
		$rules['.header .search-open .input-group-btn button']['background-color'] = $settings_mapper->getValueByCode('header_search_btn_background_color');
		$rules['.header .search-open .input-group-btn button:hover']['background-color'] = $settings_mapper->getValueByCode('header_search_btn_hover_background_color');
		$rules['.header .search-open--simple']['border'] = "0!important;";

		//mobile nav rules
		$rules['.header .navbar .navbar-toggle .icon-bar']['background-color'] = $settings_mapper->getValueByCode('nav_burger_color');
		$border_color = ($settings_mapper->getValueByCode('nav_burger_border_color'))?$settings_mapper->getValueByCode('nav_burger_border_color'):$settings_mapper->getValueByCode('nav_burger_color');
		$rules['.header .navbar .navbar-toggle']['border'] = "1px solid ".$border_color;
		$mobile_rules['.header .navbar .navbar-collapse']['background-color'] = $settings_mapper->getValueByCode('nav_mobile_canvas_color');
		$mobile_rules['.header .navbar .navbar-collapse .navbar-nav li a, .header .navbar .navbar-collapse .js-close-navbar, .header .navbar .navbar-nav .open .dropdown-menu>li>a, .navbar-controls .search-btn']['color'] = $settings_mapper->getValueByCode('nav_mobile_font_color');
		$mobile_rules['.header .navbar .navbar-collapse .navbar-nav li a']['font-size'] = $settings_mapper->getValueByCode('nav_mobile_font_size');
		$mobile_rules['.header .navbar .navbar-collapse .navbar-nav li a']['line-height'] = $settings_mapper->getValueByCode('nav_mobile_font_lineheight');
		$mobile_rules['.header .navbar .navbar-collapse .navbar-nav li a:hover']['color'] = $settings_mapper->getValueByCode('nav_mobile_hover_text_color');
		$mobile_rules['.header .navbar .navbar-collapse .navbar-nav li a:hover']['background-color'] = $settings_mapper->getValueByCode('nav_mobile_hover_background_color');
		$rules['.header .navbar .navbar-toggle, .header--custom .navbar--flex .navbar-toggle']['background-color'] = $settings_mapper->getValueByCode('nav_burger_background_color').'!important';

		//header hamburger nav rules
		$rules['.header--nav-burger .navbar .navbar-collapse']['background-color'] = $settings_mapper->getValueByCode('nav_mobile_canvas_color');
		$rules['.header--nav-burger .navbar .navbar-collapse .navbar-nav li a, .header--nav-burger .navbar .navbar-collapse .js-close-navbar, .header--nav-burger .navbar .navbar-nav .open .dropdown-menu>li>a']['color'] = $settings_mapper->getValueByCode('nav_mobile_font_color');
		$rules['.header--nav-burger .navbar .navbar-collapse .navbar-nav li a']['font-size'] = $settings_mapper->getValueByCode('nav_mobile_font_size');
		$rules['.header--nav-burger .navbar .navbar-collapse .navbar-nav li a']['line-height'] = $settings_mapper->getValueByCode('nav_mobile_font_lineheight');
		$rules['.header--nav-burger .navbar .navbar-collapse .navbar-nav li a:hover']['color'] = $settings_mapper->getValueByCode('nav_mobile_hover_text_color');
		$rules['.header--nav-burger .navbar .navbar-collapse .navbar-nav li a:hover']['background-color'] = $settings_mapper->getValueByCode('nav_mobile_hover_background_color');
		$rules['.navbar-toggle .icon-bar']['height'] = $settings_mapper->getValueByCode('nav_burger_size');
		$rules['.navbar-toggle .icon-bar']['width'] = $settings_mapper->getValueByCode('nav_burger_size_width');

		//announcement bar rules
		$rules['.announcement-bar']['background-color'] = $settings_mapper->getValueByCode('announcement_bar_background_color');
		$rules['.announcement-bar']['background-image'] = $settings_mapper->getValueByCode('announcement_bar_background_image');
		$rules['.announcement-bar']['min-height'] = $settings_mapper->getValueByCode('announcement_bar_height');
		$rules['.announcement-bar__item']['padding-top'] = $settings_mapper->getValueByCode('announcement_bar_padding');
		$rules['.announcement-bar__item']['padding-bottom'] = $settings_mapper->getValueByCode('announcement_bar_padding');
		$rules['.announcement-bar__item']['padding-left'] = $settings_mapper->getValueByCode('announcement_bar_padding');
		$rules['.announcement-bar__item']['padding-right'] = $settings_mapper->getValueByCode('announcement_bar_padding');
		$rules['.announcement-bar__close']['color'] = $settings_mapper->getValueByCode('announcement_close_color');

		$announcement_background_image_style = $settings_mapper->getValueByCode('announcement_bar_background_image_style');

		switch($announcement_background_image_style) {
			case "cover":
				$rules['.announcement-bar']['background-size'] = 'cover';
				$rules['.announcement-bar']['background-position'] = 'center';
				break;

			case "contain":
				$rules['.announcement-bar']['background-size'] = 'contain';
				$rules['.announcement-bar']['background-position'] = 'center';
				break;
			case "repeat":
				$rules['.announcement-bar']['background-repeat'] = 'repeat';
				$rules['.announcement-bar']['background-position'] = 'center';
				break;
			case "no-repeat":
				$rules['.announcement-bar']['background-repeat'] = 'no-repeat';
				$rules['.announcement-bar']['background-position'] = 'center';
				break;
			default:
		}

		//promotional pop-up/modal
		$rules['#modal-cms.fade']['background'] = $settings_mapper->getValueByCode('promo_popup_shade_color');
		$rules['#modal-cms .close']['color'] = $settings_mapper->getValueByCode('promo_popup_close_color');

		//blog listing title
		$rules['.blog-header h2']['custom_font'] = $settings_mapper->getValueByCode('blog_title_font');
		$rules['.blog-header h2']['font-size'] = $settings_mapper->getValueByCode('blog_title_text_size');
		$rules['.blog-header h2']['line-height'] = $settings_mapper->getValueByCode('blog_title_text_line_height');
		$rules['.blog-header h2, .blog-header h2 a, .blog-header h2 a:visited, .blog-header h2 a:hover']['color'] = $settings_mapper->getValueByCode('blog_title_text_color');
		$rules['.blog-header h2']['margin-top'] = $settings_mapper->getValueByCode('blog_title_vertical_padding');
		$rules['.blog-header h2']['margin-bottom'] = $settings_mapper->getValueByCode('blog_title_vertical_padding');
		$rules['.blog-header h2']['font-weight'] = $settings_mapper->getValueByCode('blog_title_font_weight');

		//blog article title
		$rules['.blog-detail-page .blog-header h1']['custom_font'] = $settings_mapper->getValueByCode('blog_article_title_font');
		$rules['.blog-detail-page .blog-header h1']['font-size'] = $settings_mapper->getValueByCode('blog_article_title_text_size');
		$rules['.blog-detail-page .blog-header h1']['line-height'] = $settings_mapper->getValueByCode('blog_article_title_line_height');
		$rules['.blog-detail-page .blog-header h1, .blog-detail-page .blog-header h1 a']['color'] = $settings_mapper->getValueByCode('blog_article_title_text_color');
		$rules['.blog-detail-page .blog-header h1']['margin-top'] = $settings_mapper->getValueByCode('blog_article_title_vertical_padding');
		$rules['.blog-detail-page .blog-header h1']['margin-bottom'] = $settings_mapper->getValueByCode('blog_article_title_vertical_padding');
		$rules['.blog-detail-page .blog-header h1']['font-weight'] = $settings_mapper->getValueByCode('blog_article_title_text_weight');

		//blog body text
		$rules['.blog-content__copy .text']['custom_font'] = $settings_mapper->getValueByCode('blog_body_text_font');
		$rules['.blog-content__copy .text']['font-size'] = $settings_mapper->getValueByCode('blog_body_text_size');
		$rules['.blog-content__copy .text']['font-weight'] = $settings_mapper->getValueByCode('blog_body_text_weight');
		$rules['.blog-content__copy .text']['line-height'] = $settings_mapper->getValueByCode('blog_body_text_line_height');
		$rules['.blog-content__copy .text, .blog-content__copy .text a:not(.btn)']['color'] = $settings_mapper->getValueByCode('blog_body_text_color');
		$rules['.blog-content__copy .text a:not(.btn)']['color'] = $settings_mapper->getValueByCode('blog_body_link_color');
		$rules['.blog-content__copy .text a:hover:not(.btn)']['color'] = $settings_mapper->getValueByCode('blog_body_link_hover_color');

		//blog button styling
		$rules['.blog-page .btn-blog-continue']['background'] = $settings_mapper->getValueByCode('blog_button_color');
		$rules['.blog-page .btn-blog-continue']['color'] = $settings_mapper->getValueByCode('blog_button_text_color');
		$rules['.blog-page .btn-blog-continue']['font-size'] = $settings_mapper->getValueByCode('blog_button_text_size');
		$rules['.blog-page .btn-blog-continue']['font-family'] = $settings_mapper->getValueByCode('blog_button_font');
		$rules['.blog-page .btn-blog-continue']['border-color'] = $settings_mapper->getValueByCode('blog_button_border_color');
		$rules['.blog-page .btn-blog-continue']['border-size'] = $settings_mapper->getValueByCode('blog_button_border_width');
		//blog sidebar
		$rules['.blog-page .sidebar__item ul']['padding-left'] = $settings_mapper->getValueByCode('blog_sidebar_ul_left_padding');
		$rules['.blog-page .sidebar__item ul li']['margin-bottom'] = $settings_mapper->getValueByCode('blog_sidebar_li_bottom_margin');
		$rules['.blog-page .sidebar__item ul li']['list-style-type'] = $settings_mapper->getValueByCode('blog_sidebar_li_style');
		$rules['.blog-page .sidebar__item a']['custom_font'] = $settings_mapper->getValueByCode('blog_sidebar_font');
		$rules['.blog-page .sidebar__item a']['font-weight'] = $settings_mapper->getValueByCode('blog_sidebar_font_weight');
		$rules['.blog-page .sidebar__item a']['font-size'] = $settings_mapper->getValueByCode('blog_sidebar_font_size');
		$rules['.blog-page .sidebar__item a']['line-height'] = $settings_mapper->getValueByCode('blog_sidebar_font_lineheight');
		$rules['.blog-page .sidebar__item a']['color'] = $settings_mapper->getValueByCode('blog_sidebar_text_color');
		$rules['.blog-page .sidebar__item a']['text-decoration'] = $settings_mapper->getValueByCode('blog_sidebar_decoration');
		$rules['.blog-page .sidebar__item a:hover']['color'] = $settings_mapper->getValueByCode('blog_sidebar_hover_color');
		$rules['.blog-page .sidebar__item a:hover']['text-decoration'] = $settings_mapper->getValueByCode('blog_sidebar_hover_decoration');
		//blog paging
		$rules['.blog-detail-page .btn-blog-pager']['background-color'] = $settings_mapper->getValueByCode('blog_pagination_button_color');
		$rules['.blog-detail-page .btn-blog-pager:hover']['background-color'] = $settings_mapper->getValueByCode('blog_pagination_button_color_hover');
		$rules['.blog-detail-page .btn-blog-pager']['color'] = $settings_mapper->getValueByCode('blog_pagination_button_text_color');
		$rules['.blog-detail-page .btn-blog-pager:hover']['color'] = $settings_mapper->getValueByCode('blog_pagination_button_text_color_hover');
		$rules['.blog-detail-page .btn-blog-pager']['custom_font'] = $settings_mapper->getValueByCode('blog_pagination_button_font');
		$rules['.blog-detail-page .btn-blog-pager']['font-size'] = $settings_mapper->getValueByCode('blog_pagination_button_font_size');
		$rules['.blog-detail-page .btn-blog-pager']['border-color'] = $settings_mapper->getValueByCode('blog_pagination_button_border_color');
		$rules['.blog-detail-page .btn-blog-pager:hover']['border-color'] = $settings_mapper->getValueByCode('blog_pagination_button_border_color_hover');
		$rules['.blog-detail-page .btn-blog-pager']['border-width'] = $settings_mapper->getValueByCode('blog_pagination_button_border_width');

		//blog listing grid spacing
		$rules['.blog-page .blog-grid--col-spacing']['grid-column-gap'] = $settings_mapper->getValueByCode('blog_post_col_spacing');
		$rules['.blog-page .blog-grid--row-spacing']['grid-row-gap'] = $settings_mapper->getValueByCode('blog_post_row_spacing');

		//blog related feed on blog article page
		$rules['.blog-related-feed .blog-header h2 a, .blog-related-feed .blog-feed-module__title']['font-size'] = $settings_mapper->getValueByCode('blog_related_title_font_size');
		$rules['.blog-related-feed .blog-header h2 a, .blog-related-feed .blog-feed-module__title']['line-height'] = $settings_mapper->getValueByCode('blog_related_title_line_height');
		$rules['.blog-related-feed .blog-header h2 a, .blog-related-feed .blog-feed-module__title']['font-weight'] = $settings_mapper->getValueByCode('blog_related_title_font_weight');
		$rules['.blog-related-feed .blog-header h2 a, .blog-related-feed .blog-feed-module__title']['color'] = $settings_mapper->getValueByCode('blog_related_title_color');
		$rules['.blog-related-feed .blog-header h2 a, .blog-related-feed .blog-feed-module__title']['padding'] = $settings_mapper->getValueByCode('blog_related_title_padding');
		$rules['.blog-related-feed .blog-header h2 a, .blog-related-feed .blog-feed-module__title']['text-decoration'] = ($settings_mapper->getValueByCode('blog_related_title_underline_status') == 'Enabled')?'underline':'none';
		$rules['.blog-related-feed a.readmore, .blog-related-feed a.btn']['font-size'] = $settings_mapper->getValueByCode('blog_related_read_more_font_size');
		$rules['.blog-related-feed a.readmore, .blog-related-feed a.btn']['line-height'] = $settings_mapper->getValueByCode('blog_related_read_more_line_height');
		$rules['.blog-related-feed a.readmore, .blog-related-feed a.btn']['font-weight'] = $settings_mapper->getValueByCode('blog_related_read_more_font_weight');
		$rules['.blog-related-feed a.readmore, .blog-related-feed a.btn']['color'] = $settings_mapper->getValueByCode('blog_related_read_more_color');
		$rules['.blog-related-feed a.readmore, .blog-related-feed a.btn']['text-decoration'] = ($settings_mapper->getValueByCode('blog_related_read_more_underline_status') == 'Enabled')?'underline':'';
		$rules['.blog-related-feed a.readmore:hover, .blog-related-feed a.btn:hover']['text-decoration'] = ($settings_mapper->getValueByCode('blog_related_read_more_underline_hover_status') == 'Enabled')?'underline':'';
		$rules['.blog-related-feed a.btn']['background-color'] = $settings_mapper->getValueByCode('blog_related_read_more_button_color');
		$rules['.blog-related-feed a.btn']['border-color'] = $settings_mapper->getValueByCode('blog_related_read_more_border_color');
		$rules['.blog-related-feed a.btn']['border-size'] = $settings_mapper->getValueByCode('blog_related_read_more_border_width');
		$rules['.blog-related-feed a.btn']['margin-top'] = $settings_mapper->getValueByCode('blog_related_read_more_top_margin');
		$rules['.blog-related-feed__title']['font-size'] = $settings_mapper->getValueByCode('blog_related_feed_title_font_size');
		$rules['.blog-related-feed__title']['line-height'] = $settings_mapper->getValueByCode('blog_related_feed_title_line_height');
		$rules['.blog-related-feed__title']['font-weight'] = $settings_mapper->getValueByCode('blog_related_feed_title_font_weight');
		$rules['.blog-related-feed__title']['color'] = $settings_mapper->getValueByCode('blog_related_feed_title_color');
		$rules['.blog-related-feed__title']['padding-top'] = $settings_mapper->getValueByCode('blog_related_feed_title_padding');
		$rules['.blog-related-feed__title']['padding-bottom'] = $settings_mapper->getValueByCode('blog_related_feed_title_padding');
		$rules['.blog-related-feed__title']['custom_font'] = $settings_mapper->getValueByCode('blog_article_title_font');


		//ecommerce primary button styling
		$rules['.btn.btn-ecommerce-primary, .customer .btn, .customer__cart .table--cart .promo-code .btn']['background'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_color').'!important';
		$rules['.btn.btn-ecommerce-primary, .btn.btn-ecommerce-primary:visited, .customer .btn, .customer .btn:visited, .customer__cart .table--cart .promo-code .btn']['color'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_text_color').'!important';
		$rules['.btn.btn-ecommerce-primary, .customer .btn, .customer__cart .table--cart .promo-code .btn']['font-size'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_text_size');
		$rules['.btn.btn-ecommerce-primary, .customer .btn, .customer__cart .table--cart .promo-code .btn']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_text_weight');
		$rules['.btn.btn-ecommerce-primary, .customer .btn, .customer__cart .table--cart .promo-code .btn']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_primary_font');
		$rules['.btn.btn-ecommerce-primary, .customer .btn, .customer__cart .table--cart .promo-code .btn']['border-color'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_border_color');
		$rules['.btn.btn-ecommerce-primary, .customer .btn, .customer__cart .table--cart .promo-code .btn']['border-width'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_border_size');
		$rules['.btn.btn-ecommerce-primary, .customer .btn']['max-width'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_width');
		$rules['.btn.btn-ecommerce-primary']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_padding_tb');
		$rules['.btn.btn-ecommerce-primary']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_padding_tb');
		$rules['.btn.btn-ecommerce-primary']['padding-left'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_padding_lr');
		$rules['.btn.btn-ecommerce-primary']['padding-right'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_padding_lr');
		$rules['.btn.btn-ecommerce-primary:hover, .btn.btn-ecommerce-primary:focus, .customer .btn:hover, .customer .btn:focus, .customer__cart .table--cart .promo-code .btn:hover, .customer__cart .table--cart .promo-code .btn:focus']['background'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_hover_color').'!important';
		$rules['.btn.btn-ecommerce-primary:hover, .btn.btn-ecommerce-primary:focus, .customer .btn:hover, .customer .btn:focus, .customer__cart .table--cart .promo-code .btn:hover, .customer__cart .table--cart .promo-code .btn:focus']['color'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_hover_text_color').'!important';
		$rules['.btn.btn-ecommerce-primary:hover, .btn.btn-ecommerce-primary:focus, .customer .btn:hover, .customer .btn:focus, .customer__cart .table--cart .promo-code .btn:hover, .customer__cart .table--cart .promo-code .btn:focus']['border-color'] = $settings_mapper->getValueByCode('ecommerce_primary_btn_border_hover_color');

		//ecommerce secondary button styling
		$rules['.btn.btn-ecommerce-secondary']['background'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_color').'!important';
		$rules['.btn.btn-ecommerce-secondary, .btn.btn-ecommerce-secondary:visited']['color'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_text_color').'!important';
		$rules['.btn.btn-ecommerce-secondary']['font-size'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_text_size');
		$rules['.btn.btn-ecommerce-secondary']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_text_weight');
		$rules['.btn.btn-ecommerce-secondary']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_secondary_font');
		$rules['.btn.btn-ecommerce-secondary']['border-color'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_border_color');
		$rules['.btn.btn-ecommerce-secondary']['border-width'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_border_size');
		$rules['.btn.btn-ecommerce-secondary']['max-width'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_width');
		$rules['.btn.btn-ecommerce-secondary']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_padding_tb');
		$rules['.btn.btn-ecommerce-secondary']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_padding_tb');
		$rules['.btn.btn-ecommerce-secondary']['padding-left'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_padding_lr');
		$rules['.btn.btn-ecommerce-secondary']['padding-right'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_padding_lr');
		$rules['.btn.btn-ecommerce-secondary:hover, .btn.btn-ecommerce-secondary:focus']['background'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_hover_color').'!important';
		$rules['.btn.btn-ecommerce-secondary:hover, .btn.btn-ecommerce-secondary:focus']['color'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_hover_text_color').'!important';
		$rules['.btn.btn-ecommerce-secondary:hover, .btn.btn-ecommerce-secondary:focus']['border-color'] = $settings_mapper->getValueByCode('ecommerce_secondary_btn_border_hover_color');

		//product in stock label
		$rules['.products .in_stock_message']['color'] = $settings_mapper->getValueByCode('inventory_in_stock_color');
		$rules['.products .in_stock_message']['font-size'] = $settings_mapper->getValueByCode('inventory_in_stock_font_size');
		$rules['.products .in_stock_message']['font-weight'] = $settings_mapper->getValueByCode('inventory_in_stock_font_weight');
		$rules['.products .in_stock_message']['font-style'] = $settings_mapper->getValueByCode('inventory_in_stock_font_style');
		$rules['.products .in_stock_message']['text-decoration'] = $settings_mapper->getValueByCode('inventory_in_stock_text_decoration');

		//product out of stock label
		$rules['.products .out_of_stock_message']['color'] = $settings_mapper->getValueByCode('inventory_out_of_stock_color');
		$rules['.products .out_of_stock_message']['font-size'] = $settings_mapper->getValueByCode('inventory_out_of_stock_font_size');
		$rules['.products .out_of_stock_message']['font-weight'] = $settings_mapper->getValueByCode('inventory_out_of_stock_font_weight');
		$rules['.products .out_of_stock_message']['font-style'] = $settings_mapper->getValueByCode('inventory_out_of_stock_font_style');
		$rules['.products .out_of_stock_message']['text-decoration'] = $settings_mapper->getValueByCode('inventory_out_of_stock_text_decoration');

		//product breadcrumb
		$rules['.product-page .breadcrumb li, .product-page .breadcrumb li a, .category-page .breadcrumb li, .category-page .breadcrumb li a, .customer .breadcrumb li, .customer .breadcrumb li a']['color'] = $settings_mapper->getValueByCode('ecommerce_product_breadcrumb_text_color');

		$rules['.product-page .breadcrumb li.active, .product-page .breadcrumb li a:hover, .category-page .breadcrumb li.active, .category-page .breadcrumb li a:hover, .customer .breadcrumb li.active, .customer .breadcrumb li a:hover']['color'] = $settings_mapper->getValueByCode('ecommerce_product_breadcrumb_text_active_color');

		$rules['.product-page .breadcrumb li, .product-page .breadcrumb li a, .category-page .breadcrumb li, .category-page .breadcrumb li a, .customer .breadcrumb li, .customer .breadcrumb li a']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_breadcrumb_text_size');

		$rules['.product-page .breadcrumb, .category-page .breadcrumb, .customer .breadcrumb']['background-color'] = $settings_mapper->getValueByCode('ecommerce_product_breadcrumb_background_color');

		$rules['.product-page .breadcrumb, .category-page .breadcrumb, .customer .breadcrumb']['text-align'] = $settings_mapper->getValueByCode('ecommerce_product_breadcrumb_alignment');

		$rules['.product-page .breadcrumb, .category-page .breadcrumb, .customer .breadcrumb']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_product_breadcrumb_padding_tb');

		$rules['.product-page .breadcrumb, .category-page .breadcrumb, .customer .breadcrumb']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_product_breadcrumb_padding_tb');

		$rules['.product-page .breadcrumb, .category-page .breadcrumb, .customer .breadcrumb']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_breadcrumb_font_weight');

		//product page description
		$rules['.product-page .header']['margin-bottom'] = $settings_mapper->getValueByCode('ecommerce_product_page_padding_top');
		$rules['.product-page .products__description']['color'] = $settings_mapper->getValueByCode('ecommerce_product_description_color');
		$rules['.product-page .products__description']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_description_size');
		$rules['.product-page .products__description']['line-height'] = $settings_mapper->getValueByCode('ecommerce_product_description_line_height');
		$rules['.product-page .products__description']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_product_description_padding_tb');
		$rules['.product-page .products__description']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_product_description_padding_tb');
		$rules['.product-page .products__description']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_description_font');
		$rules['.product-page .products__description']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_description_font_weight');

		//product page price
		$rules['.product-page .products__status']['color'] = $settings_mapper->getValueByCode('ecommerce_product_price_color');
		$rules['.product-page .products__status']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_price_size');
		$rules['.product-page .products__status']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_price_weight');
		$rules['.product-page .products__status']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_product_price_padding_tb');
		$rules['.product-page .products__status']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_product_price_padding_tb');
		$rules['.product-page .products__status']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_price_font');

		//product recent title
		$rules['.product-page .recent-title']['color'] = $settings_mapper->getValueByCode('ecommerce_product_recent_title_color');
		$rules['.product-page .recent-title']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_recent_size');
		$rules['.product-page .recent-title']['line-height'] = $settings_mapper->getValueByCode('ecommerce_product_recent_line_height');
		$rules['.product-page .recent-title']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_product_recent_padding_tb');
		$rules['.product-page .recent-title']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_product_recent_padding_tb');
		$rules['.product-page .recent-title']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_recent_font');

		//product related title
		$rules['.product-page .related-title']['color'] = $settings_mapper->getValueByCode('ecommerce_product_related_title_color');
		$rules['.product-page .related-title']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_related_size');
		$rules['.product-page .related-title']['line-height'] = $settings_mapper->getValueByCode('ecommerce_product_related_line_height');
		$rules['.product-page .related-title']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_product_related_padding_tb');
		$rules['.product-page .related-title']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_product_related_padding_tb');
		$rules['.product-page .related-title']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_related_font');

		//product page button styling
		$rules['.product-page .btn.btn-ecommerce-cta']['background'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_color').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['color'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_text_color').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_text_size').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_text_weight').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['border-color'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_border_color').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['border-width'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_border_size').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['max-width'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_width').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_padding_tb').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_padding_tb').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['padding-left'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_padding_lr').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['padding-right'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_padding_lr').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta:hover, .product-page .btn.btn-ecommerce-cta:focus']['background'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_hover_color').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta:hover, .product-page .btn.btn-ecommerce-cta:focus']['color'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_hover_text_color').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta:hover, .product-page .btn.btn-ecommerce-cta:focus']['border-color'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_border_hover_color').'!important';
		$rules['.product-page .btn.btn-ecommerce-cta']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_cta_btn_font');

		//product page title
		$rules['.product-page .products--detail h1']['color'] = $settings_mapper->getValueByCode('ecommerce_product_title_color');
		$rules['.product-page .products--detail h1']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_title_size');
		$rules['.product-page .products--detail h1']['line-height'] = $settings_mapper->getValueByCode('ecommerce_product_title_line_height');
		$rules['.product-page .products--detail h1']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_title_font');
		$rules['.product-page .products--detail h1']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_product_title_padding');
		$rules['.product-page .products--detail h1']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_product_title_padding');
		$rules['.product-page .products--detail h1']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_title_font_weight');

		//category page title
		$rules['.category-page .products__title']['color'] = $settings_mapper->getValueByCode('ecommerce_category_product_color');
		$rules['.category-page .products__title']['font-size'] = $settings_mapper->getValueByCode('ecommerce_category_product_size');
		$rules['.category-page .products__title']['line-height'] = $settings_mapper->getValueByCode('ecommerce_category_product_line_height');
		$rules['.category-page .products__title']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_category_product_font');
		$rules['.category-page .products__title']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_category_product_font_weight');

		//category page description
		$rules['.category-page .products__description']['color'] = $settings_mapper->getValueByCode('ecommerce_category_short_description_color');
		$rules['.category-page .products__description']['font-size'] = $settings_mapper->getValueByCode('ecommerce_category_short_description_size');
		$rules['.category-page .products__description']['line-height'] = $settings_mapper->getValueByCode('ecommerce_category_short_description_line_height');
		$rules['.category-page .products__description']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_category_short_description_font');
		$rules['.category-page .products__description']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_category_short_description_font_weight');

		//category page price
		$rules['.category-page .price span']['color'] = $settings_mapper->getValueByCode('ecommerce_category_product_price_color');
		$rules['.category-page .price span']['font-size'] = $settings_mapper->getValueByCode('ecommerce_category_product_price_size');
		$rules['.category-page .price span']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_category_product_price_weight');
		$rules['.category-page .price span']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_category_product_price_font');

		//category page button styling
		$rules['.category-page .btn.btn-ecommerce-category-cta']['background'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_color').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['color'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_text_color').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['font-size'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_text_size').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_text_weight').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['border-color'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_border_color').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['border-width'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_border_size').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['max-width'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_width').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['padding-top'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_padding_tb').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['padding-bottom'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_padding_tb').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['padding-left'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_padding_lr').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['padding-right'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_padding_lr').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta:hover, .category-page .btn.btn-ecommerce-category-cta:focus']['background'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_hover_color').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta:hover, .category-page .btn.btn-ecommerce-category-cta:focus']['color'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_hover_text_color').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta:hover, .category-page .btn.btn-ecommerce-category-cta:focus']['border-color'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_border_hover_color').'!important';
		$rules['.category-page .btn.btn-ecommerce-category-cta']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_category_product_cta_btn_font');

		//cart page
		$rules['.cart-wrapper .table--products th, .checkout-wrapper .table--confirmation th, .customer__cart .table--cart th']['background-color'] = $settings_mapper->getValueByCode('ecommerce_product_cart_background_color').'!important';
		$rules['.cart-wrapper .table--products th+th, .checkout-wrapper .table--confirmation th+th, .customer__cart .table--cart th+th']['border-left-color'] = $settings_mapper->getValueByCode('ecommerce_product_cart_heading_border_color').'!important';
		$rules['.cart-wrapper .products hr']['border-top-color'] = $settings_mapper->getValueByCode('ecommerce_product_cart_border_color').'!important';
		$rules['.cart-wrapper .table--products th, .checkout-wrapper .table--confirmation th, .customer__cart .table--cart th']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_cart_heading_size');
		$rules['.cart-wrapper .table--products th, .checkout-wrapper .table--confirmation th, .customer__cart .table--cart th']['color'] = $settings_mapper->getValueByCode('ecommerce_product_cart_heading_color');
		$rules['.cart-wrapper .table--products th, .checkout-wrapper .table--confirmation th, .customer__cart .table--cart th']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_heading_font');
		$rules['.cart-wrapper .table--products th, .checkout-wrapper .table--confirmation th, .customer__cart .table--cart th']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_heading_font_weight');

		$rules['.cart-wrapper .table--products td, .cart-wrapper .products__total, .cart-wrapper .products__note--shipping > strong, .checkout-wrapper .table--confirmation td, .customer__cart .table--cart td']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_cart_cell_size').'!important';
		$rules['.cart-wrapper .table--products td, .cart-wrapper .table--products td a, .cart-wrapper .products__total, .cart-wrapper .products__note--shipping > strong, .cart-wrapper .table--products .products__details .products__title, .checkout-wrapper .table--confirmation td, .checkout-wrapper .table--confirmation td a, .customer__cart .table--cart td, .customer__cart .table--cart td a']['color'] = $settings_mapper->getValueByCode('ecommerce_product_cart_cell_color').'!important';
		$rules['.cart-wrapper .table--products td, .cart-wrapper .products__total, .cart-wrapper .products__note--shipping > strong, .checkout-wrapper .table--confirmation td, .customer__cart .table--cart td']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_cart_cell_font');
		$rules['.cart-wrapper .table--products td, .cart-wrapper .products__total, .cart-wrapper .products__note--shipping > strong, .checkout-wrapper .table--confirmation td, .customer__cart .table--cart td, .cart-wrapper .table--products td strong, .cart-wrapper .products__total strong']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_cart_cell_font_weight');

		$rules['.cart-page .page-header h1']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_cart_main_title_size');
		$rules['.cart-page .page-header h1']['line-height'] = $settings_mapper->getValueByCode('ecommerce_product_cart_main_title_line_height');
		$rules['.cart-page .page-header h1']['color'] = $settings_mapper->getValueByCode('ecommerce_product_cart_main_title_color');
		$rules['.cart-page .page-header h1']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_cart_main_title_font');
		$rules['.cart-page .page-header h1']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_cart_main_title_font_weight');

		//modal cart
		$rules['.modal--cart .modal-body']['background-color'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_background_color').'!important';
		$rules['.modal--cart .products h3']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_title_size');
		$rules['.modal--cart .products h3, .modal--cart .close']['color'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_title_color');
		$rules['.modal--cart .products h3']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_title_font');
		$rules['.modal--cart .modal-content']['border-color'] = 'transparent';
		$rules['.modal--cart .products h3']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_title_font_weight');

		$rules['.modal--cart .table--products th']['background-color'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_heading_background_color').'!important';
		$rules['.modal--cart .table--products th+th']['border-left-color'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_heading_border_color').'!important';
		$rules['.modal--cart .products hr']['border-top-color'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_border_color').'!important';
		$rules['.modal--cart .table--products th']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_heading_size');
		$rules['.modal--cart .table--products th']['color'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_heading_color');
		$rules['.modal--cart .table--products th']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_heading_font');
		$rules['.modal--cart .table--products th']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_heading_font_weight');

		$rules['.modal--cart .table--products td, .modal--cart .table--products .products__title']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_cell_size').'!important';
		$rules['.modal--cart .table--products td, .modal--cart .table--products td a, .modal--cart .table--products .products__title']['color'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_cell_color').'!important';
		$rules['.modal--cart .table--products td, .modal--cart .table--products .products__title']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_cell_font');
		$rules['.modal--cart .table--products td, .modal--cart .table--products .products__title, .modal--cart .table--products td strong']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_modal_cart_cell_font_weight');

		//checkout page
		$rules['.information-page .main:before, .payment-page .main:before, .shipping-page .main:before']['background-color'] = $settings_mapper->getValueByCode('ecommerce_checkout_left_column_background_color').'!important';
		$rules['.checkout-page .form--checkout .bg-gray-lighter, .checkout-page .promo-field .btn, .information-page .main, .payment-page .main, .shipping-page .main, .customer__cart']['background-color'] = $settings_mapper->getValueByCode('ecommerce_checkout_background_color').'!important';
		$rules['.checkout-page .page-header']['border-bottom-color'] = $settings_mapper->getValueByCode('ecommerce_checkout_border_color').'!important';
		$rules['.checkout-page .form--checkout .form-control, .checkout-page .promo-field .btn']['border-color'] = $settings_mapper->getValueByCode('ecommerce_checkout_border_color').'!important';
		$rules['.checkout-page .products__summary .table td']['border-top-color'] = $settings_mapper->getValueByCode('ecommerce_checkout_border_color').'!important';
		$rules['.checkout-page .form__heading, .customer h2, .customer .h1']['font-size'] = $settings_mapper->getValueByCode('ecommerce_checkout_heading_size');
		$rules['.checkout-page .form__heading, .customer h2, .customer .h1, .confirmation-page .order-number, .confirmation-page .order-text']['color'] = $settings_mapper->getValueByCode('ecommerce_checkout_heading_color');
		$rules['.checkout-page .form__heading, .customer h2, .customer .h1']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_checkout_heading_font');
		$rules['.checkout-page .form__heading, .customer h2, .customer .h1']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_checkout_heading_font_weight');

		$rules['.checkout-page .form--checkout label:not(.error), .checkout-page .form--checkout .products__summary .table th, .checkout-page .form--checkout .products__summary .table td, .checkout-page .form--checkout .form__note, .customer h3, .checkout-page .form--checkout #shipping-rates, .checkout-page .page-header p, .checkout-page #collapse-billing p']['font-size'] = $settings_mapper->getValueByCode('ecommerce_checkout_cell_size').'!important';
		$rules['.checkout-page .form--checkout label:not(.error), .checkout-page .form--checkout .products__summary .table th, .checkout-page .form--checkout .products__summary .table td, .checkout-page .form--checkout .form__note, .checkout-page .account-login a, .checkout-page .promo-field .btn, .customer h3, .checkout-page .form--checkout #shipping-rates, .checkout-page .page-header p, .checkout-page #collapse-billing p']['color'] = $settings_mapper->getValueByCode('ecommerce_checkout_cell_color').'!important';
		$rules['.checkout-page .form--checkout label:not(.error), .checkout-page .form--checkout .products__summary .table th, .checkout-page .form--checkout .products__summary .table td, .checkout-page .form--checkout .form__note, .customer h3, .checkout-page .form--checkout #shipping-rates, .checkout-page .page-header p, .checkout-page #collapse-billing p']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_checkout_cell_font');
		$rules['.checkout-page .form--checkout label:not(.error), .checkout-page .form--checkout .products__summary .table th, .checkout-page .form--checkout .products__summary .table td, .checkout-page .form--checkout .form__note, .customer h3, .checkout-page .form--checkout #shipping-rates, .checkout-page .form--checkout label:not(.error) strong, .checkout-page .page-header p, .checkout-page #collapse-billing p']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_checkout_cell_font_weight');

		$rules['.checkout-page .page-header h1']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_main_title_size');
		$rules['.checkout-page .page-header h1']['line-height'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_main_title_line_height');
		$rules['.checkout-page .page-header h1']['color'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_main_title_color');
		$rules['.checkout-page .page-header h1']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_main_title_font');
		$rules['.checkout-page .page-header h1']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_main_title_font_weight');

		$rules['.checkout-page label.error, .checkout-page ul.errors']['font-size'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_alert_size');
		$rules['.checkout-page label.error, .checkout-page ul.errors li']['margin-top'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_alert_margin');
		$rules['.checkout-page label.error, .checkout-page .form-control.error, .checkout-page ul.errors']['color'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_alert_color');
		$rules['.checkout-page .form-control.error']['border-color'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_alert_color');
		$rules['.checkout-page label.error, .checkout-page ul.errors']['custom_font'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_alert_font');
		$rules['.checkout-page label.error, .checkout-page ul.errors']['font-weight'] = $settings_mapper->getValueByCode('ecommerce_product_checkout_alert_weight');

		//video library
		$rules['.video_library-page .caption__title']['custom_font'] = $settings_mapper->getValueByCode('video_caption_font');
		$rules['.video_library-page .caption__title']['font-weight'] = $settings_mapper->getValueByCode('video_caption_font_weight');
		$rules['.video_library-page .caption__title']['font-size'] = $settings_mapper->getValueByCode('video_caption_font_size');
		$rules['.video_library-page .caption__title']['line-height'] = $settings_mapper->getValueByCode('video_caption_font_lineheight');
		$rules['.video_library-page .caption__title']['color'] = $settings_mapper->getValueByCode('video_caption_text_color');
		$rules['.video_library-page a .caption__title, .video_library-page a.caption__link']['text-decoration'] = $settings_mapper->getValueByCode('video_caption_decoration');
		$rules['.video_library-page a .caption__title:hover, .video_library-page a.caption__link:hover']['color'] = $settings_mapper->getValueByCode('video_caption_hover_color');
		$rules['.video_library-page a .caption__title:hover, .video_library-page a.caption__link:hover']['text-decoration'] = $settings_mapper->getValueByCode('video_caption_hover_decoration');

		//instalinks page
		$rules['body.instalink-page, body.instalink-page .header--instalink']['background'] = $settings_mapper->getValueByCode('instalinks_page_background_color');
		$rules['body.instalink-page .header--instalink:after']['background'] = $settings_mapper->getValueByCode('instalinks_header_color');
		$rules['body.instalink-page .button-bar__item a']['background'] = $settings_mapper->getValueByCode('instalinks_link_bar_button_color');
		$rules['body.instalink-page .button-bar__item a']['border-color'] = $settings_mapper->getValueByCode('instalinks_link_bar_border_color');
		$rules['body.instalink-page .button-bar__item a']['border-width'] = $settings_mapper->getValueByCode('instalinks_link_bar_border_size');
		$rules['body.instalink-page .grid__item .icon']['color'] = $settings_mapper->getValueByCode('instalinks_tile_icon_color');
		$rules['body.instalink-page .grid']['grid-column-gap'] = $settings_mapper->getValueByCode('instalinks_tile_column_gap');
		$rules['body.instalink-page .grid']['grid-row-gap'] = $settings_mapper->getValueByCode('instalinks_tile_row_gap');
		$rules['body.instalink-page .grid__item .icon']['font-size'] = $settings_mapper->getValueByCode('instalinks_tile_icon_size');

		//$icon_padding = $settings_mapper->getValueByCode('instalinks_tile_icon_size');
		//if (isset($icon_padding) && $icon_padding) {
			//$rules['body.instalink-page .grid__item .caption']['padding-right'] = $settings_mapper->getValueByCode('instalinks_tile_icon_size') + 7;
		//} else {
			//$rules['body.instalink-page .grid__item .caption:not()']['padding-right'] = 40;
		//}

		//row spacing & column options
		$rules['.row-height']['min-height'] = $settings_mapper->getValueByCode('min_height');

		//lvp detail page
		//rental
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['background'] = $settings_mapper->getValueByCode('lvp_rental_button_color').'!important';
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental:hover']['background'] = $settings_mapper->getValueByCode('lvp_rental_button_hover_color').'!important';
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['border-width'] = $settings_mapper->getValueByCode('lvp_rental_button_border_width');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['border-color'] = $settings_mapper->getValueByCode('lvp_rental_button_border_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental:hover']['border-color'] = $settings_mapper->getValueByCode('lvp_rental_button_hover_border_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['custom_font'] = $settings_mapper->getValueByCode('lvp_rental_button_font');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['font-size'] = $settings_mapper->getValueByCode('lvp_rental_button_text_size');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['font-weight'] = $settings_mapper->getValueByCode('lvp_rental_button_text_weight');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental, .lvp__sidebar .lvp__btn-group .btn--lvp-rental:visited, .lvp__sidebar .lvp__btn-group .btn--lvp-rental:link']['color'] = $settings_mapper->getValueByCode('lvp_rental_button_text_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental:hover']['color'] = $settings_mapper->getValueByCode('lvp_rental_button_hover_text_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['max-width'] = $settings_mapper->getValueByCode('lvp_rental_button_width');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['padding-top'] = $settings_mapper->getValueByCode('lvp_rental_button_tb_padding');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_rental_button_tb_padding');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['padding-left'] = $settings_mapper->getValueByCode('lvp_rental_button_lr_padding');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-rental']['padding-right'] = $settings_mapper->getValueByCode('lvp_rental_button_lr_padding');

		//purchase
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['background'] = $settings_mapper->getValueByCode('lvp_purchase_button_color').'!important';
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase:hover']['background'] = $settings_mapper->getValueByCode('lvp_purchase_button_hover_color').'!important';
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['border-width'] = $settings_mapper->getValueByCode('lvp_purchase_button_border_width');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['border-color'] = $settings_mapper->getValueByCode('lvp_purchase_button_border_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase:hover']['border-color'] = $settings_mapper->getValueByCode('lvp_purchase_button_hover_border_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['custom_font'] = $settings_mapper->getValueByCode('lvp_purchase_button_font');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['font-size'] = $settings_mapper->getValueByCode('lvp_purchase_button_text_size');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['font-weight'] = $settings_mapper->getValueByCode('lvp_purchase_button_text_weight');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase, .lvp__sidebar .lvp__btn-group .btn--lvp-purchase:visited, .lvp__sidebar .lvp__btn-group .btn--lvp-purchase:link']['color'] = $settings_mapper->getValueByCode('lvp_purchase_button_text_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase:hover']['color'] = $settings_mapper->getValueByCode('lvp_purchase_button_hover_text_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['max-width'] = $settings_mapper->getValueByCode('lvp_purchase_button_width');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['padding-top'] = $settings_mapper->getValueByCode('lvp_purchase_button_tb_padding');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_purchase_button_tb_padding');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['padding-left'] = $settings_mapper->getValueByCode('lvp_purchase_button_lr_padding');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-purchase']['padding-right'] = $settings_mapper->getValueByCode('lvp_purchase_button_lr_padding');

		//trailer
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['background'] = $settings_mapper->getValueByCode('lvp_trailer_button_color').'!important';
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer:hover']['background'] = $settings_mapper->getValueByCode('lvp_trailer_button_hover_color').'!important';
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['border-width'] = $settings_mapper->getValueByCode('lvp_trailer_button_border_width');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['border-color'] = $settings_mapper->getValueByCode('lvp_trailer_button_border_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer:hover']['border-color'] = $settings_mapper->getValueByCode('lvp_trailer_button_hover_border_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['custom_font'] = $settings_mapper->getValueByCode('lvp_trailer_button_font');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['font-size'] = $settings_mapper->getValueByCode('lvp_trailer_button_text_size');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['font-weight'] = $settings_mapper->getValueByCode('lvp_trailer_button_text_weight');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer, .lvp__sidebar .lvp__btn-group .btn--lvp-trailer:visited, .lvp__sidebar .lvp__btn-group .btn--lvp-trailer:link']['color'] = $settings_mapper->getValueByCode('lvp_trailer_button_text_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer:hover']['color'] = $settings_mapper->getValueByCode('lvp_trailer_button_hover_text_color');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['max-width'] = $settings_mapper->getValueByCode('lvp_trailer_button_width');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['padding-top'] = $settings_mapper->getValueByCode('lvp_trailer_button_tb_padding');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_trailer_button_tb_padding');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['padding-left'] = $settings_mapper->getValueByCode('lvp_trailer_button_lr_padding');
		$rules['.lvp__sidebar .lvp__btn-group .btn--lvp-trailer']['padding-right'] = $settings_mapper->getValueByCode('lvp_trailer_button_lr_padding');

		//body text
		$rules['.lvp__tabs .tab-content']['custom_font'] = $settings_mapper->getValueByCode('lvp_body_text_font');
		$rules['.lvp__tabs .tab-content']['color'] = $settings_mapper->getValueByCode('lvp_body_text_color');
		$rules['.lvp__tabs .tab-content']['font-size'] = $settings_mapper->getValueByCode('lvp_body_text_size');
		$rules['.lvp__tabs .tab-content']['font-weight'] = $settings_mapper->getValueByCode('lvp_body_text_weight');
		$rules['.lvp__tabs .tab-content']['line-height'] = $settings_mapper->getValueByCode('lvp_body_text_line_height');
		$rules['.lvp__tabs .tab-content a, .lvp__tabs .tab-content a:focus, .lvp__tabs .tab-content a:visited, .lvp__tabs .tab-content a:link']['color'] = $settings_mapper->getValueByCode('lvp_body_link_color');
		$rules['.lvp__tabs .tab-content a:hover']['color'] = $settings_mapper->getValueByCode('lvp_body_link_hover_color');

		//main title
		$rules['.lvp__title']['custom_font'] = $settings_mapper->getValueByCode('lvp_main_title_font');
		$rules['.lvp__title']['color'] = $settings_mapper->getValueByCode('lvp_main_title_text_color');
		$rules['.lvp__title']['font-size'] = $settings_mapper->getValueByCode('lvp_main_title_text_size');
		$rules['.lvp__title']['font-weight'] = $settings_mapper->getValueByCode('lvp_main_title_font_weight');
		$rules['.lvp__title']['line-height'] = $settings_mapper->getValueByCode('lvp_main_title_text_line_height');
		$rules['.lvp__title']['padding-top'] = $settings_mapper->getValueByCode('vp_main_title_vertical_padding');
		$rules['.lvp__title']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_main_title_vertical_padding');


		//sub title
		$rules['.lvp__sub-title']['custom_font'] = $settings_mapper->getValueByCode('lvp_sub_title_font');
		$rules['.lvp__sub-title']['color'] = $settings_mapper->getValueByCode('lvp_sub_title_text_color');
		$rules['.lvp__sub-title']['font-size'] = $settings_mapper->getValueByCode('lvp_sub_title_text_size');
		$rules['.lvp__sub-title']['font-weight'] = $settings_mapper->getValueByCode('lvp_sub_title_font_weight');
		$rules['.lvp__sub-title']['line-height'] = $settings_mapper->getValueByCode('lvp_sub_title_text_line_height');
		$rules['.lvp__sub-title']['padding-top'] = $settings_mapper->getValueByCode('lvp_sub_title_vertical_padding');
		$rules['.lvp__sub-title']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_sub_title_vertical_padding');

		//background
		$rules['.lvp-detail-page']['background-color'] = $settings_mapper->getValueByCode('lvp_background_color');
		$rules['.lvp-detail-page']['background-image'] = $settings_mapper->getValueByCode('lvp_background_image');

		$lvp_background_image_style = $settings_mapper->getValueByCode('lvp_background_image_size');

		switch($lvp_background_image_style) {
			case "cover":
				$rules['.lvp-detail-page']['background-size'] = 'cover';
				$rules['.lvp-detail-page']['background-position'] = 'center';
				$rules['.lvp-detail-page']['background-repeat'] = 'no-repeat';
				break;
			case "contain":
				$rules['.lvp-detail-page']['background-size'] = 'contain';
				$rules['.lvp-detail-page']['background-position'] = 'center';
				$rules['.lvp-detail-page']['background-repeat'] = 'no-repeat';
				break;
			case "repeat":
				$rules['.lvp-detail-page']['background-repeat'] = 'repeat';
				$rules['.lvp-detail-page']['background-position'] = 'center';
				break;
			case "auto":
				$rules['.lvp-detail-page']['background-size'] = 'auto';
				$rules['.lvp-detail-page']['background-repeat'] = 'no-repeat';
				$rules['.lvp-detail-page']['background-position'] = 'center';
				break;
			default:
		}

		//tabs
		$rules['.lvp__tabs .nav-tabs>li.active>a, .lvp__tabs .nav-tabs>li.active>a:hover']['background'] = $settings_mapper->getValueByCode('lvp_tab_active_color').'!important';
		$rules['.lvp__tabs .nav-tabs>li.active>a']['border-color'] = 'transparent';
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane .panel-default>.panel-heading']['background'] = $settings_mapper->getValueByCode('lvp_tab_color').'!important';
		$rules['.lvp__tabs .nav-tabs>li>a:hover']['background'] = $settings_mapper->getValueByCode('lvp_tab_hover_color').'!important';
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .nav-tabs, .lvp__tabs .tab-pane .panel-default>.panel-heading, .lvp__tabs .tab-pane .panel-default']['border-color'] = $settings_mapper->getValueByCode('lvp_tab_border_color');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .nav-tabs, .lvp__tabs .tab-pane .panel-default>.panel-heading, .lvp__tabs .tab-pane .panel-default']['border-width'] = $settings_mapper->getValueByCode('lvp_tab_border_width');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane .panel-default>.panel-heading .panel-title']['custom_font'] = $settings_mapper->getValueByCode('lvp_tab_font');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane .panel-default>.panel-heading .panel-title']['font-size'] = $settings_mapper->getValueByCode('lvp_tab_text_size');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane .panel-default>.panel-heading .panel-title']['font-weight'] = $settings_mapper->getValueByCode('lvp_tab_text_weight');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .nav-tabs>li>a:visited, .lvp__tabs .tab-pane .panel-default>.panel-heading']['color'] = $settings_mapper->getValueByCode('lvp_tab_text_color');
		$rules['.lvp__tabs .nav-tabs>li.active>a, .lvp__tabs .nav-tabs>li.active>a:visited']['color'] = $settings_mapper->getValueByCode('lvp_tab_active_text_color');
		$rules['.lvp__tabs .nav-tabs>li>a:hover']['color'] = $settings_mapper->getValueByCode('lvp_tab_hover_text_color');
		$rules['.lvp__tabs']['max-width'] = $settings_mapper->getValueByCode('lvp_tab_width');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane .panel-default>.panel-heading']['padding-top'] = $settings_mapper->getValueByCode('lvp_tab_tb_padding');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane .panel-default>.panel-heading']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_tab_tb_padding');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane .panel-default>.panel-heading']['padding-left'] = $settings_mapper->getValueByCode('lvp_tab_lr_padding');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane .panel-default>.panel-heading']['padding-right'] = $settings_mapper->getValueByCode('lvp_tab_lr_padding');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane:first-child .panel']['border-top-left-radius'] = $settings_mapper->getValueByCode('lvp_tab_border_radius');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane:first-child .panel']['border-top-right-radius'] = $settings_mapper->getValueByCode('lvp_tab_border_radius');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane:last-child .panel']['border-bottom-left-radius'] = $settings_mapper->getValueByCode('lvp_tab_border_radius');
		$rules['.lvp__tabs .nav-tabs>li>a, .lvp__tabs .tab-pane:last-child .panel']['border-bottom-right-radius'] = $settings_mapper->getValueByCode('lvp_tab_border_radius');
		$rules['.lvp__tabs .nav-tabs>li+li']['margin-left'] = $settings_mapper->getValueByCode('lvp_tab_left_margin');
		$rules['.lvp__tabs .nav-tabs>li']['margin-right'] = 0;

		$lvp_tab_alignment = $settings_mapper->getValueByCode('lvp_tab_alignment');
		switch($lvp_tab_alignment) {
			case "left":
				$rules['.lvp__tabs .nav-tabs']['display'] = 'flex';
				$rules['.lvp__tabs .nav-tabs']['justify-content'] = 'flex-start';
				break;
			case "center":
				$rules['.lvp__tabs .nav-tabs']['display'] = 'flex';
				$rules['.lvp__tabs .nav-tabs']['justify-content'] = 'center';
				break;
			case "right":
				$rules['.lvp__tabs .nav-tabs']['display'] = 'flex';
				$rules['.lvp__tabs .nav-tabs']['justify-content'] = 'flex-end';
				break;
			default:
		}

		$lvp_border_width = $settings_mapper->getValueByCode('lvp_tab_border_width');
		if ($lvp_border_width == 0) {
			$rules['.lvp__tabs .nav-tabs>li']['margin-bottom'] = 0;
		} else {
			$rules['.lvp__tabs .nav-tabs>li']['margin-bottom'] = '-'.$settings_mapper->getValueByCode('lvp_tab_border_width');
		}

		//gallery
		$rules['.lvp__gallery-title']['custom_font'] = $settings_mapper->getValueByCode('lvp_gallery_title_font');
		$rules['.lvp__gallery-title']['color'] = $settings_mapper->getValueByCode('lvp_gallery_title_color');
		$rules['.lvp__gallery-title']['font-size'] = $settings_mapper->getValueByCode('lvp_gallery_title_size');
		$rules['.lvp__gallery-title']['font-weight'] = $settings_mapper->getValueByCode('lvp_gallery_title_weight');
		$rules['.lvp__gallery-title']['line-height'] = $settings_mapper->getValueByCode('lvp_gallery_title_line_height');
		$rules['.lvp__gallery-title']['margin-bottom'] = $settings_mapper->getValueByCode('lvp_gallery_title_margin_bottom');
		$rules['.lvp--grid']['grid-column-gap'] = $settings_mapper->getValueByCode('lvp_gallery_column_spacing');
		$rules['.lvp--grid']['grid-row-gap'] = $settings_mapper->getValueByCode('lvp_gallery_row_spacing');

		//tray
		$rules['.lvp--tray .slick-next:before, .lvp--tray .slick-prev:before']['font-size'] = $settings_mapper->getValueByCode('lvp_tray_arrow_size');
		$rules['.lvp--tray .slick-arrow']['padding'] = $settings_mapper->getValueByCode('lvp_tray_arrow_padding') . 'px';
		$rules['.lvp--tray .slick-next:before, .lvp--tray .slick-prev:before']['color'] = $settings_mapper->getValueByCode('lvp_tray_arrow_color');
		$rules['.lvp--tray .slick-next:hover:before, .lvp--tray .slick-prev:hover:before']['color'] = $settings_mapper->getValueByCode('lvp_tray_arrow_hover_color');
		$rules['.lvp--tray .slick-next, .lvp--tray .slick-prev']['background'] = $settings_mapper->getValueByCode('lvp_tray_arrow_background_color');
		$rules['.lvp--tray .slick-next:hover, .lvp--tray .slick-prev:hover']['background'] = $settings_mapper->getValueByCode('lvp_tray_arrow_background_hover_color');
		$rules['.lvp--tray .carousel__item']['margin-left'] = $settings_mapper->getValueByCode('lvp_gallery_column_spacing');
		$rules['.lvp--tray .carousel__item']['margin-right'] = $settings_mapper->getValueByCode('lvp_gallery_column_spacing');


		//adjustments for tray
		$lvp_tray_padding = $settings_mapper->getValueByCode('lvp_tray_arrow_padding');
		$lvp_tray_width = $settings_mapper->getValueByCode('lvp_gallery_max_width');

		if ($lvp_tray_padding > 0 && $lvp_tray_width <= 0 ) {
			$rules['.lvp--tray .slick-list']['max-width'] = '95%';
			$rules['.lvp--tray .slick-list']['margin'] = '0 auto';
			$rules['.lvp--tray']['padding'] = '0 ' . $settings_mapper->getValueByCode('lvp_tray_arrow_padding') * 3.2 .'px';
		}

		if ($lvp_tray_width > 0) {
			$rules['.lvp--tray .slick-list']['max-width'] = $settings_mapper->getValueByCode('lvp_gallery_max_width');
			$rules['.lvp--tray']['max-width'] = $settings_mapper->getValueByCode('lvp_gallery_max_width') + 115 ;
			$rules['.lvp--tray .slick-list, .lvp--tray']['margin'] = '0 auto';
		}


		//related tray
		$rules['.lvp--related-tray .slick-next:before, .lvp--related-tray .slick-prev:before']['font-size'] = $settings_mapper->getValueByCode('lvp_related_arrow_size');
		$rules['.lvp--related-tray .slick-arrow']['padding'] = $settings_mapper->getValueByCode('lvp_related_arrow_padding');
		$rules['.lvp--related-tray .slick-next:before, .lvp--related-tray .slick-prev:before']['color'] = $settings_mapper->getValueByCode('lvp_related_arrow_color');
		$rules['.lvp--related-tray .slick-next:hover:before, .lvp--related-tray .slick-prev:hover:before']['color'] = $settings_mapper->getValueByCode('lvp_related_arrow_hover_color');
		$rules['.lvp--related-tray .slick-next, .lvp--related-tray .slick-prev']['background'] = $settings_mapper->getValueByCode('lvp_related_arrow_background_color');
		$rules['.lvp--related-tray .slick-next:hover, .lvp--related-tray .slick-prev:hover']['background'] = $settings_mapper->getValueByCode('lvp_related_arrow_background_hover_color');
		$rules['.lvp--related-tray .carousel__item']['margin-left'] = $settings_mapper->getValueByCode('lvp_related_column_spacing');
		$rules['.lvp--related-tray .carousel__item']['margin-right'] = $settings_mapper->getValueByCode('lvp_related_column_spacing');
		$rules['.lvp--related-tray']['margin-top'] = $settings_mapper->getValueByCode('lvp_related_margin_top');

		//related tray title
		$rules['.lvp__related-title']['custom_font'] = $settings_mapper->getValueByCode('lvp_related_title_font');
		$rules['.lvp__related-title']['color'] = $settings_mapper->getValueByCode('lvp_related_title_color');
		$rules['.lvp__related-title']['font-size'] = $settings_mapper->getValueByCode('lvp_related_title_size');
		$rules['.lvp__related-title']['font-weight'] = $settings_mapper->getValueByCode('lvp_related_title_weight');
		$rules['.lvp__related-title']['line-height'] = $settings_mapper->getValueByCode('lvp_related_title_line_height');
		$rules['.lvp__related-title']['margin-bottom'] = $settings_mapper->getValueByCode('lvp_related_title_margin_bottom');
		$rules['.lvp__related-title']['margin-top'] = $settings_mapper->getValueByCode('lvp_related_title_margin_top');

		//related tray caption
		$rules['.lvp--related-tray .carousel__caption h3']['custom_font'] = $settings_mapper->getValueByCode('lvp_related_post_title_font');
		$rules['.lvp--related-tray .carousel__caption h3, .lvp--related-tray .carousel__caption h3>a,.lvp--related-tray .carousel__caption h3>a:hover, .lvp--related-tray .carousel__caption h3>a:visited']['color'] = $settings_mapper->getValueByCode('lvp_related_post_title_color');
		$rules['.lvp--related-tray .carousel__caption h3']['font-size'] = $settings_mapper->getValueByCode('lvp_related_post_title_font_size');
		$rules['.lvp--related-tray .carousel__caption h3']['font-weight'] = $settings_mapper->getValueByCode('lvp_related_post_title_weight');
		$rules['.lvp--related-tray .carousel__caption h3']['line-height'] = $settings_mapper->getValueByCode('lvp_related_post_title_line_height');
		$rules['.lvp--related-tray .carousel__caption h3']['margin-top'] = $settings_mapper->getValueByCode('lvp_related_post_title_margin_top') . 'px !important';

		//adjustments for related
		$lvp_related_padding = $settings_mapper->getValueByCode('lvp_related_arrow_padding');
		$lvp_related_width = $settings_mapper->getValueByCode('lvp_related_max_width');

		if ($lvp_related_padding > 0 && $lvp_related_width <= 0 ) {
			$rules['.lvp--related-tray .slick-list']['max-width'] = '95%';
			$rules['.lvp--related-tray .slick-list']['margin'] = '0 auto';
			$rules['.lvp--related-tray']['padding'] = '0 ' . $settings_mapper->getValueByCode('lvp_related_arrow_padding') * 3.2 .'px';
		}

		if ($lvp_related_width > 0) {
			$rules['.lvp--related-tray .slick-list']['max-width'] = $settings_mapper->getValueByCode('lvp_related_max_width');
			$rules['.lvp--related-tray']['max-width'] = $settings_mapper->getValueByCode('lvp_related_max_width') + 115 ;
			$rules['.lvp--related-tray .slick-list, .lvp--related-tray']['margin'] = '0 auto';
		}

		//end lvp detail page

		//lvp category page
		//main title
		$rules['.lvp-category-page .lvp__title']['custom_font'] = $settings_mapper->getValueByCode('lvp_cat_page_title_font');
		$rules['.lvp-category-page .lvp__title']['color'] = $settings_mapper->getValueByCode('lvp_cat_page_title_color');
		$rules['.lvp-category-page .lvp__title']['font-size'] = $settings_mapper->getValueByCode('lvp_cat_page_title_size');
		$rules['.lvp-category-page .lvp__title']['font-weight'] = $settings_mapper->getValueByCode('lvp_cat_page_title_font_weight');
		$rules['.lvp-category-page .lvp__title']['line-height'] = $settings_mapper->getValueByCode('lvp_cat_page_title_line_height');
		$rules['.lvp-category-page .lvp__title']['padding-top'] = $settings_mapper->getValueByCode('lvp_cat_page_title_vertical_padding');
		$rules['.lvp-category-page .lvp__title']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_cat_page_title_vertical_padding');

		//entry title
		$rules['.lvp-category-page .lvp--grid__entry-title']['custom_font'] = $settings_mapper->getValueByCode('lvp_cat_entry_title_font');
		$rules['.lvp-category-page .lvp--grid__entry-title']['color'] = $settings_mapper->getValueByCode('lvp_cat_entry_title_color');
		$rules['.lvp-category-page .lvp--grid__entry-title']['font-size'] = $settings_mapper->getValueByCode('lvp_cat_entry_title_size');
		$rules['.lvp-category-page .lvp--grid__entry-title']['font-weight'] = $settings_mapper->getValueByCode('lvp_cat_entry_title_font_weight');
		$rules['.lvp-category-page .lvp--grid__entry-title']['line-height'] = $settings_mapper->getValueByCode('lvp_cat_entry_title_line_height');
		$rules['.lvp-category-page .lvp--grid__entry-title']['padding-top'] = $settings_mapper->getValueByCode('lvp_cat_entry_title_vertical_padding');
		$rules['.lvp-category-page .lvp--grid__entry-title']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_cat_entry_title_vertical_padding');

		//description
		$rules['.lvp-category-page .lvp--grid__description p']['custom_font'] = $settings_mapper->getValueByCode('lvp_cat_description_font');
		$rules['.lvp-category-page .lvp--grid__description p']['color'] = $settings_mapper->getValueByCode('lvp_cat_description_color');
		$rules['.lvp-category-page .lvp--grid__description p']['font-size'] = $settings_mapper->getValueByCode('lvp_cat_description_size');
		$rules['.lvp-category-page .lvp--grid__description p']['font-weight'] = $settings_mapper->getValueByCode('lvp_cat_description_font_weight');
		$rules['.lvp-category-page .lvp--grid__description p']['line-height'] = $settings_mapper->getValueByCode('lvp_cat_description_line_height');
		$rules['.lvp-category-page .lvp--grid__description p']['padding-top'] = $settings_mapper->getValueByCode('lvp_cat_description_vertical_padding');
		$rules['.lvp-category-page .lvp--grid__description p']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_cat_description_vertical_padding');

		//cta
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['background'] = $settings_mapper->getValueByCode('lvp_cat_button_color').'!important';
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail:hover']['background'] = $settings_mapper->getValueByCode('lvp_cat_button_hover_color').'!important';
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['border-width'] = $settings_mapper->getValueByCode('lvp_cat_button_border_width');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['border-color'] = $settings_mapper->getValueByCode('lvp_cat_button_border_color');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail:hover']['border-color'] = $settings_mapper->getValueByCode('lvp_cat_button_hover_border_color');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['custom_font'] = $settings_mapper->getValueByCode('lvp_cat_button_font');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['font-size'] = $settings_mapper->getValueByCode('lvp_cat_button_text_size');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['font-weight'] = $settings_mapper->getValueByCode('lvp_cat_button_text_weight');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail, .lvp-category-page .lvp--grid__description .btn--lvp-detail:visited']['color'] = $settings_mapper->getValueByCode('lvp_cat_button_text_color');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail:hover']['color'] = $settings_mapper->getValueByCode('lvp_cat_button_hover_text_color');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['max-width'] = $settings_mapper->getValueByCode('lvp_cat_button_width');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['padding-top'] = $settings_mapper->getValueByCode('lvp_cat_button_tb_padding');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_cat_button_tb_padding');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['padding-left'] = $settings_mapper->getValueByCode('lvp_cat_button_lr_padding');
		$rules['.lvp-category-page .lvp--grid__description .btn--lvp-detail']['padding-right'] = $settings_mapper->getValueByCode('lvp_cat_button_lr_padding');

		//overlay
		$rules['.lvp-category-page .lvp__caption--overlay']['background-color'] = $settings_mapper->getValueByCode('lvp_cat_overlay_background_color');
		$rules['.lvp-category-page .lvp__caption--overlay']['padding-right'] = $settings_mapper->getValueByCode('lvp_cat_overlay_horizontal_padding');
		$rules['.lvp-category-page .lvp__caption--overlay']['padding-left'] = $settings_mapper->getValueByCode('lvp_cat_overlay_horizontal_padding');
		$rules['.lvp-category-page .lvp__caption--overlay']['padding-top'] = $settings_mapper->getValueByCode('lvp_cat_overlay_vertical_padding');
		$rules['.lvp-category-page .lvp__caption--overlay']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_cat_overlay_vertical_padding');

		//background
		$rules['.lvp-category-page']['background-color'] = $settings_mapper->getValueByCode('lvp_cat_background_color');
		$rules['.lvp-category-page']['background-image'] = $settings_mapper->getValueByCode('lvp_cat_background_image');

		$lvp_cat_background_image_style = $settings_mapper->getValueByCode('lvp_cat_background_image_size');

		switch($lvp_cat_background_image_style) {
			case "cover":
				$rules['.lvp-category-page']['background-size'] = 'cover';
				$rules['.lvp-category-page']['background-position'] = 'center';
				$rules['.lvp-category-page']['background-repeat'] = 'no-repeat';
				break;
			case "contain":
				$rules['.lvp-category-page']['background-size'] = 'contain';
				$rules['.lvp-category-page']['background-position'] = 'center';
				$rules['.lvp-category-page']['background-repeat'] = 'no-repeat';
				break;
			case "repeat":
				$rules['.lvp-category-page']['background-repeat'] = 'repeat';
				$rules['.lvp-category-page']['background-position'] = 'center';
				break;
			case "auto":
				$rules['.lvp-category-page']['background-size'] = 'auto';
				$rules['.lvp-category-page']['background-repeat'] = 'no-repeat';
				$rules['.lvp-category-page']['background-position'] = 'center';
				break;
			default:
		}

		//breadcrumb
		$rules['.lvp-category-page .breadcrumb li, .lvp-category-page .breadcrumb li a']['color'] = $settings_mapper->getValueByCode('lvp_cat_breadcrumb_color');
		$rules['.lvp-category-page .breadcrumb li.active, .lvp-category-page .breadcrumb li a:hover']['color'] = $settings_mapper->getValueByCode('lvp_cat_breadcrumb_active_color');
		$rules['.lvp-category-page .breadcrumb li, .lvp-category-page .breadcrumb li a']['font-size'] = $settings_mapper->getValueByCode('lvp_cat_breadcrumb_size');
		$rules['.lvp-category-page .breadcrumb']['background-color'] = $settings_mapper->getValueByCode('lvp_cat_breadcrumb_background_color');
		$rules['.lvp-category-page .breadcrumb']['text-align'] = $settings_mapper->getValueByCode('lvp_cat_breadcrumb_alignment');
		$rules['.lvp-category-page .breadcrumb']['padding-top'] = $settings_mapper->getValueByCode('lvp_cat_breadcrumb_padding_tb');
		$rules['.lvp-category-page .breadcrumb']['padding-bottom'] = $settings_mapper->getValueByCode('lvp_cat_breadcrumb_padding_tb');
		$rules['.lvp-category-page .breadcrumb']['font-weight'] = $settings_mapper->getValueByCode('lvp_cat_breadcrumb_font_weight');

		//paging
		$rules['.lvp-category-page .pagination>li>a, .lvp-category-page .pagination>li>span']['color'] = $settings_mapper->getValueByCode('lvp_cat_paging_color');
		$rules['.lvp-category-page .pagination>li.active>a, .lvp-category-page .pagination>li.active>a:hover, .lvp-category-page .pagination>li>a:hover, .lvp-category-page .pagination>li>span:hover']['color'] = $settings_mapper->getValueByCode('lvp_cat_paging_active_color');
		$rules['.lvp-category-page .pagination>li>a, .lvp-category-page .pagination>li>span']['font-size'] = $settings_mapper->getValueByCode('lvp_cat_paging_size');
		$rules['.lvp-category-page .pagination>li>a, .lvp-category-page .pagination>li>span']['background-color'] = $settings_mapper->getValueByCode('lvp_cat_paging_background_color');
		$rules['.lvp-category-page .pagination>li.active>a, .lvp-category-page .pagination>li.active>a:hover, .lvp-category-page .pagination>li>a:hover, .lvp-category-page .pagination>li>span:hover']['background-color'] = $settings_mapper->getValueByCode('lvp_cat_paging_active_background_color');
		$rules['.lvp-category-page .pagination>li>a, .lvp-category-page .pagination>li>span']['font-weight'] = $settings_mapper->getValueByCode('lvp_cat_paging_font_weight');
		$rules['.lvp-category-page .pagination>li.active>a, .lvp-category-page .pagination>li.active>a:hover, .lvp-category-page .pagination>li>a:hover, .lvp-category-page .pagination>li>span:hover, .lvp-category-page .pagination>li.active>a, .lvp-category-page .pagination>li.active>a:hover, .lvp-category-page .pagination>li>a, .lvp-category-page .pagination>li>span']['border-color'] = $settings_mapper->getValueByCode('lvp_cat_paging_border_color');

		//grid spacing
		$rules['.lvp-category-page .lvp--grid']['grid-column-gap'] = $settings_mapper->getValueByCode('lvp_cat_grid_column_spacing');
		$rules['.lvp-category-page .lvp--grid']['grid-row-gap'] = $settings_mapper->getValueByCode('lvp_cat_grid_row_spacing');


		//end lvp category page

		//login and registration styles

		//background settings
		$rules['.auth-page']['background-color'] = $settings_mapper->getValueByCode('login_background_color');
		$rules['.auth-page']['background-image'] = $settings_mapper->getValueByCode('login_background_image');

		$auth_background_image_style = $settings_mapper->getValueByCode('login_background_image_image_size');

		switch($auth_background_image_style) {
			case "cover":
				$rules['.auth-page']['background-size'] = 'cover';
				$rules['.auth-page']['background-position'] = 'center';
				$rules['.auth-page']['background-repeat'] = 'no-repeat';
				break;
			case "contain":
				$rules['.auth-page']['background-size'] = 'contain';
				$rules['.auth-page']['background-position'] = 'center';
				$rules['.auth-page']['background-repeat'] = 'no-repeat';
				break;
			case "repeat":
				$rules['.auth-page']['background-repeat'] = 'repeat';
				$rules['.auth-page']['background-position'] = 'center';
				break;
			case "auto":
				$rules['.auth-page']['background-size'] = 'auto';
				$rules['.auth-page']['background-repeat'] = 'no-repeat';
				$rules['.auth-page']['background-position'] = 'center';
				break;
			default:
		}

		$rules['.auth-page .login-form form, .modal--alert .modal--alert__content']['background-color'] = $settings_mapper->getValueByCode('login_form_background_color');
		$rules['.modal--alert .modal--alert__header']['background-color'] = $settings_mapper->getValueByCode('login_modal_background_color');

		//form title
		$rules['.auth-page .login-form h1']['custom_font'] = $settings_mapper->getValueByCode('login_heading_font');
		$rules['.auth-page .login-form h1']['color'] = $settings_mapper->getValueByCode('login_heading_text_color');
		$rules['.auth-page .login-form h1']['font-size'] = $settings_mapper->getValueByCode('login_heading_text_size');
		$rules['.auth-page .login-form h1']['font-weight'] = $settings_mapper->getValueByCode('login_heading_font_weight');
		$rules['.auth-page .login-form h1']['line-height'] = $settings_mapper->getValueByCode('login_heading_text_line_height');
		$rules['.auth-page .login-form h1']['margin-bottom'] = $settings_mapper->getValueByCode('login_heading_bottom_margin') .'px !important';

		//form text
		$rules['.auth-page .login-form form, .modal--alert .modal--alert__body p']['custom_font'] = $settings_mapper->getValueByCode('login_form_font');
		$rules['.auth-page .login-form form, .modal--alert .modal--alert__body p']['color'] = $settings_mapper->getValueByCode('login_form_text_color');
		$rules['.auth-page .login-form form, .modal--alert .modal--alert__body p']['font-size'] = $settings_mapper->getValueByCode('login_form_text_size');
		$rules['.auth-page .login-form form, .modal--alert .modal--alert__body p']['font-weight'] = $settings_mapper->getValueByCode('login_form_font_weight');
		$rules['.auth-page .login-form form, .modal--alert .modal--alert__body p']['line-height'] = $settings_mapper->getValueByCode('login_form_text_line_height');

		//modal close
		$rules['.modal--alert .modal--alert__header button']['color'] = $settings_mapper->getValueByCode('modal_close_text_color');
		$rules['.modal--alert .modal--alert__header button']['font-size'] = $settings_mapper->getValueByCode('modal_close_text_size');
		$rules['.modal--alert .modal--alert__header button']['font-weight'] = $settings_mapper->getValueByCode('modal_close_font_weight');
		$rules['.modal--alert .modal--alert__header button']['line-height'] = $settings_mapper->getValueByCode('modal_close_text_line_height');

		//primary button styling
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['background'] = $settings_mapper->getValueByCode('account_primary_btn_color').'!important';
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary, .auth-page .btn.btn--cms-account-primary:visited, .modal--alert .modal--alert__body .btn--cms-account-primary:visited']['color'] = $settings_mapper->getValueByCode('account_primary_btn_text_color').'!important';
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['font-size'] = $settings_mapper->getValueByCode('account_primary_btn_text_size');
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['font-weight'] = $settings_mapper->getValueByCode('account_primary_btn_text_weight');
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['custom_font'] = $settings_mapper->getValueByCode('account_primary_font');
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['border-color'] = $settings_mapper->getValueByCode('account_primary_btn_border_color');
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['border-width'] = $settings_mapper->getValueByCode('account_primary_btn_border_size');
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['max-width'] = $settings_mapper->getValueByCode('account_primary_btn_width');
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['padding-top'] = $settings_mapper->getValueByCode('account_primary_btn_padding_tb');
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['padding-bottom'] = $settings_mapper->getValueByCode('account_primary_btn_padding_tb');
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['padding-left'] = $settings_mapper->getValueByCode('account_primary_btn_padding_lr');
		$rules['.auth-page .btn.btn--cms-account-primary, .modal--alert .modal--alert__body .btn--cms-account-primary']['padding-right'] = $settings_mapper->getValueByCode('account_primary_btn_padding_lr');
		$rules['.auth-page .btn.btn--cms-account-primary:hover, .auth-page .btn.btn--cms-account-primary:focus, .modal--alert .modal--alert__body .btn--cms-account-primary:hover, .modal--alert .modal--alert__body .btn--cms-account-primary:focus']['background'] = $settings_mapper->getValueByCode('account_primary_btn_hover_color').'!important';
		$rules['.auth-page .btn.btn--cms-account-primary:hover, .auth-page .btn.btn--cms-account-primary:focus, .modal--alert .modal--alert__body .btn--cms-account-primary:hover, .modal--alert .modal--alert__body .btn--cms-account-primary:focus']['color'] = $settings_mapper->getValueByCode('account_primary_btn_hover_text_color').'!important';
		$rules['.auth-page .btn.btn--cms-account-primary:hover, .auth-page .btn.btn--cms-account-primary:focus, .modal--alert .modal--alert__body .btn--cms-account-primary:hover, .modal--alert .modal--alert__body .btn--cms-account-primary:focus']['border-color'] = $settings_mapper->getValueByCode('account_primary_btn_border_hover_color');

		//secondary button styling
		$rules['.auth-page .btn--cms-account-secondary, .auth-page .btn.btn--cms-account-secondary:visited']['color'] = $settings_mapper->getValueByCode('account_secondary_btn_text_color').'!important';
		$rules['.auth-page .btn--cms-account-secondary']['font-size'] = $settings_mapper->getValueByCode('account_secondary_btn_text_size');
		$rules['.auth-page .btn--cms-account-secondary']['font-weight'] = $settings_mapper->getValueByCode('account_secondary_btn_text_weight');
		$rules['.auth-page .btn--cms-account-secondary']['custom_font'] = $settings_mapper->getValueByCode('account_secondary_font');
		$rules['.auth-page .btn--cms-account-secondary:hover, .auth-page .btn--cms-account-secondary:focus']['color'] = $settings_mapper->getValueByCode('account_secondary_btn_text_hover_color').'!important';

		//modal register button styling
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['background'] = $settings_mapper->getValueByCode('modal_register_btn_color').'!important';
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register, .modal--alert .modal--alert__body .btn.btn--cms-modal-register:visited']['color'] = $settings_mapper->getValueByCode('modal_register_btn_text_color').'!important';
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['font-size'] = $settings_mapper->getValueByCode('modal_register_btn_text_size');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['font-weight'] = $settings_mapper->getValueByCode('modal_register_btn_text_weight');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['custom_font'] = $settings_mapper->getValueByCode('modal_register_font');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['border-color'] = $settings_mapper->getValueByCode('modal_register_btn_border_color');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['border-width'] = $settings_mapper->getValueByCode('modal_register_btn_border_size');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['max-width'] = $settings_mapper->getValueByCode('modal_register_btn_width');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['padding-top'] = $settings_mapper->getValueByCode('modal_register_btn_padding_tb');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['padding-bottom'] = $settings_mapper->getValueByCode('modal_register_btn_padding_tb');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['padding-left'] = $settings_mapper->getValueByCode('modal_register_btn_padding_lr');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register']['padding-right'] = $settings_mapper->getValueByCode('modal_register_btn_padding_lr');
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register:hover, .modal--alert .modal--alert__body .btn--cms-modal-register:focus']['background'] = $settings_mapper->getValueByCode('modal_register_btn_hover_color').'!important';
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register:hover, .modal--alert .modal--alert__body .btn--cms-modal-register:focus']['color'] = $settings_mapper->getValueByCode('modal_register_btn_hover_text_color').'!important';
		$rules['.modal--alert .modal--alert__body .btn--cms-modal-register:hover, .modal--alert .modal--alert__body .btn--cms-modal-register:focus']['border-color'] = $settings_mapper->getValueByCode('modal_register_btn_border_hover_color');
		//end login and registration styles

		//account page styles
		//background settings
		$rules['.account-page']['background-color'] = $settings_mapper->getValueByCode('account_background_color');
		$rules['.account-page']['background-image'] = $settings_mapper->getValueByCode('account_background_image');

		$account_background_image_style = $settings_mapper->getValueByCode('account_background_image_image_size');

		switch($account_background_image_style) {
			case "cover":
				$rules['.account-page']['background-size'] = 'cover';
				$rules['.account-page']['background-position'] = 'center';
				$rules['.account-page']['background-repeat'] = 'no-repeat';
				break;
			case "contain":
				$rules['.account-page']['background-size'] = 'contain';
				$rules['.account-page']['background-position'] = 'center';
				$rules['.account-page']['background-repeat'] = 'no-repeat';
				break;
			case "repeat":
				$rules['.account-page']['background-repeat'] = 'repeat';
				$rules['.account-page']['background-position'] = 'center';
				break;
			case "auto":
				$rules['.account-page']['background-size'] = 'auto';
				$rules['.account-page']['background-repeat'] = 'no-repeat';
				$rules['.account-page']['background-position'] = 'center';
				break;
			default:
		}



		//page header
		$rules['.account-page .page-header--account']['background-color'] = $settings_mapper->getValueByCode('account_header_background_color');
		$rules['.account-page .page-header--account h1']['custom_font'] = $settings_mapper->getValueByCode('account_header_font');
		$rules['.account-page .page-header--account h1']['color'] = $settings_mapper->getValueByCode('account_header_color'). '!important';
		$rules['.account-page .page-header--account h1']['font-size'] = $settings_mapper->getValueByCode('account_header_size');
		$rules['.account-page .page-header--account h1']['font-weight'] = $settings_mapper->getValueByCode('account_header_font_weight');
		$rules['.account-page .page-header--account h1']['line-height'] = $settings_mapper->getValueByCode('account_header_line_height');
		$rules['.account-page .page-header--account']['margin-bottom'] = $settings_mapper->getValueByCode('account_header_margin') .'px !important';
		$rules['.account-page .page-header--account']['padding-bottom'] = $settings_mapper->getValueByCode('account_header_padding') .'px !important';
		$rules['.account-page .page-header--account']['padding-top'] = $settings_mapper->getValueByCode('account_header_padding') .'px !important';

		$rules['.account-page .page-header--account small']['custom_font'] = $settings_mapper->getValueByCode('account_holder_font');
		$rules['.account-page .page-header--account small']['color'] = $settings_mapper->getValueByCode('account_holder_color') .'!important';
		$rules['.account-page .page-header--account small']['font-size'] = $settings_mapper->getValueByCode('account_holder_size');
		$rules['.account-page .page-header--account small']['font-weight'] = $settings_mapper->getValueByCode('account_holder_font_weight');

		//tabs
		$rules['.account-page .nav-tabs>li.active>a, .account-page .nav-tabs>li.active>a:hover']['background'] = $settings_mapper->getValueByCode('account_tab_active_color').'!important';
		$rules['.account-page .nav-tabs>li.active>a']['border-color'] = 'transparent';
		$rules['.account-page .nav-tabs>li>a']['background'] = $settings_mapper->getValueByCode('account_tab_color').'!important';
		$rules['.account-page .nav-tabs>li>a:hover']['background'] = $settings_mapper->getValueByCode('account_tab_hover_color').'!important';
		$rules['.account-page .nav-tabs>li>a, .account-page .nav-tabs']['border-color'] = $settings_mapper->getValueByCode('account_tab_border_color');
		$rules['.account-page .nav-tabs>li>a, .account-page .nav-tabs']['border-width'] = $settings_mapper->getValueByCode('account_tab_border_width');
		$rules['.account-page .nav-tabs>li>a']['custom_font'] = $settings_mapper->getValueByCode('account_tab_font');
		$rules['.account-page .nav-tabs>li>a']['font-size'] = $settings_mapper->getValueByCode('account_tab_text_size');
		$rules['.account-page .nav-tabs>li>a']['font-weight'] = $settings_mapper->getValueByCode('account_tab_text_weight');
		$rules['.account-page .nav-tabs>li>a, .account-page .nav-tabs>li>a:visited']['color'] = $settings_mapper->getValueByCode('account_tab_text_color');
		$rules['.account-page .nav-tabs>li.active>a, .account-page .nav-tabs>li.active>a:visited']['color'] = $settings_mapper->getValueByCode('account_tab_active_text_color');
		$rules['.account-page .nav-tabs>li>a:hover']['color'] = $settings_mapper->getValueByCode('account_tab_hover_text_color');
		$rules['.account-page']['max-width'] = $settings_mapper->getValueByCode('account_tab_width');
		$rules['.account-page .nav-tabs>li>a']['padding-top'] = $settings_mapper->getValueByCode('account_tab_tb_padding');
		$rules['.account-page .nav-tabs>li>a']['padding-bottom'] = $settings_mapper->getValueByCode('account_tab_tb_padding');
		$rules['.account-page .nav-tabs>li>a']['padding-left'] = $settings_mapper->getValueByCode('account_tab_lr_padding');
		$rules['.account-page .nav-tabs>li>a']['padding-right'] = $settings_mapper->getValueByCode('account_tab_lr_padding');
		$rules['.account-page .nav-tabs>li>a']['border-top-left-radius'] = $settings_mapper->getValueByCode('account_tab_border_radius');
		$rules['.account-page .nav-tabs>li>a']['border-top-right-radius'] = $settings_mapper->getValueByCode('account_tab_border_radius');
		$rules['.account-page .nav-tabs>li+li']['margin-left'] = $settings_mapper->getValueByCode('account_tab_left_margin');
		$rules['.account-page .nav-tabs>li']['margin-right'] = 0;
		$rules['.account-page .nav-tabs']['background'] = $settings_mapper->getValueByCode('account_tab_bg_color');

		$account_tab_alignment = $settings_mapper->getValueByCode('account_tab_alignment');
		switch($account_tab_alignment) {
			case "left":
				$rules['.account-page .nav-tabs']['display'] = 'flex';
				$rules['.account-page .nav-tabs']['justify-content'] = 'flex-start';
				break;
			case "center":
				$rules['.account-page .nav-tabs']['display'] = 'flex';
				$rules['.account-page .nav-tabs']['justify-content'] = 'center';
				break;
			case "right":
				$rules['.account-page .nav-tabs']['display'] = 'flex';
				$rules['.account-page .nav-tabs']['justify-content'] = 'flex-end';
				break;
			default:
		}

		$account_tab_border_width = $settings_mapper->getValueByCode('account_tab_border_width');
		if ($account_tab_border_width == 0) {
			$rules['.account-page .nav-tabs>li']['margin-bottom'] = 0;
		} else {
			$rules['.account-page .nav-tabs>li']['margin-bottom'] = '-'.$settings_mapper->getValueByCode('account_tab_border_width');
		}

		//account settings
		$rules['.account-page .account__header h2, .account-page .table>thead>tr>th, .table>tbody>tr>td']['custom_font'] = $settings_mapper->getValueByCode('account_heading_font');
		$rules['.account-page .account__header h2, .account-page .table>thead>tr>th, .table>tbody>tr>td']['color'] = $settings_mapper->getValueByCode('account_heading_color'). '!important';
		$rules['.account-page .account__header h2, .account-page .table>thead>tr>th, .table>tbody>tr>td']['font-size'] = $settings_mapper->getValueByCode('account_heading_size');
		$rules['.account-page .account__header h2']['font-weight'] = $settings_mapper->getValueByCode('account_heading_font_weight');
		$rules['.account-page .account__header, .account-page .table>thead>tr>th, .table>tbody>tr>td']['line-height'] = $settings_mapper->getValueByCode('account_heading_line_height');
		$rules['.account-page .account__header']['margin-bottom'] = $settings_mapper->getValueByCode('account_heading_margin') .'px !important';
		$rules['.account-page .account__header']['padding-bottom'] = $settings_mapper->getValueByCode('account_heading_padding') .'px !important';
		$rules['.account-page .account__header']['padding-top'] = $settings_mapper->getValueByCode('account_heading_padding') .'px !important';

		$rules['.account-page .account__header small']['custom_font'] = $settings_mapper->getValueByCode('account_heading_sub_font');
		$rules['.account-page .account__header small']['color'] = $settings_mapper->getValueByCode('account_heading_sub_color'). '!important';
		$rules['.account-page .account__header small']['font-size'] = $settings_mapper->getValueByCode('account_heading_sub_size');
		$rules['.account-page .account__header small']['font-weight'] = $settings_mapper->getValueByCode('account_heading_sub_font_weight');

		$rules['.account-page .account__header > a, .account-page .table>thead>tr>th a, .table>tbody>tr>td a']['custom_font'] = $settings_mapper->getValueByCode('account_heading_link_font');
		$rules['.account-page .account__header > a, .account-page .table>thead>tr>th a, .table>tbody>tr>td a']['color'] = $settings_mapper->getValueByCode('account_heading_link_color'). '!important';
		$rules['.account-page .account__header > a, .account-page .table>thead>tr>th a, .table>tbody>tr>td a']['font-size'] = $settings_mapper->getValueByCode('account_heading_link_size');
		$rules['.account-page .account__header > a']['font-weight'] = $settings_mapper->getValueByCode('account_heading_link_font_weight');

		$rules['.account-page .account__card > a']['custom_font'] = $settings_mapper->getValueByCode('account_card_font');
		$rules['.account-page .account__card > a']['color'] = $settings_mapper->getValueByCode('account_card_color'). '!important';
		$rules['.account-page .account__card:hover > a']['color'] = $settings_mapper->getValueByCode('account_card_hover_color'). '!important';
		$rules['.account-page .account__card > a']['font-size'] = $settings_mapper->getValueByCode('account_card_size');
		$rules['.account-page .account__card > a']['font-weight'] = $settings_mapper->getValueByCode('account_card_font_weight');

		$rules['.account-page .account__card.account__add-card > a > i']['color'] = $settings_mapper->getValueByCode('account_card_icon_color'). '!important';
		$rules['.account-page .account__card.account__add-card:hover > a > i']['color'] = $settings_mapper->getValueByCode('account_card_icon_color'). '!important';
		$rules['.account-page .account__card.account__add-card > a > i']['font-size'] = $settings_mapper->getValueByCode('account_card_icon_size');
		$rules['.account-page .account__card.account__add-card > a > i']['font-weight'] = $settings_mapper->getValueByCode('account_card_icon_font_weight');

		$rules['.account-page .account__card, .account-page .table>thead>tr>th, .account-page .table>tbody>tr>td']['border-color'] = $settings_mapper->getValueByCode('account_card_border_color'). '!important';
		$rules['.account-page .account__card:hover']['border-color'] = $settings_mapper->getValueByCode('account_card_border_hover_color'). '!important';



		//end account page styles

		//temp hack for tablet
		$tablet_rules = $mobile_rules;

		$this->createGlobalCssRules($rules, $mobile_rules, $tablet_rules);

		$mapper = new Application_Model_Mapper_Page();
		if ($page_id) {
			$pages[] = $mapper->find($page_id);
			$pages[] = $mapper->doesExists(['code'=>'footer']);
			$pages[] = $mapper->doesExists(['code'=>'mobile_footer']);
			//get blocks and modals
			foreach ($mapper->fetchBlocks() as $block) {
				$pages[] = $block;
			}
			foreach ($mapper->fetchModals() as $block) {
				$pages[] = $block;
			}
		}else {
			$pages = $mapper->fetchAll();

		}

		foreach ($pages as $page) {
			$page_id = $page->id;

			$rules = (array)json_decode($page->json_styles);
			$settings = json_decode($page->json_settings);
			$special_rules = array();
			$raw_css = '';

			//promotional pop-up/modal
			$special_rules['#modal-cms.fade']['background'] = (isset($settings->promo_popup_shade_color))?$settings->promo_popup_shade_color:'';
			$special_rules['#modal-cms .close']['color'] = (isset($settings->promo_popup_close_color))?$settings->promo_popup_close_color:'';

			/*
			//special animation settings convert to consolidated rules
			if ((isset($settings->kb_animation) && $settings->kb_animation == 'kb_animate')) {
				if (!$rules->animation_iteration_count) $rules->animation_iteration_count = 'infinite';
				$special_rules['.background-scale'] = array(
					'animation_iteration_count' => $rules->animation_iteration_count,
					'animation_duration' => $rules->animation_duration,
					'animation_timing_function' => $rules->animation_timing_function,
				);
			}else {//to eliminate conflict with existing animations
				unset($rules->animation_iteration_count);
			}
			*/
			//to eliminate conflict with existing animations
			unset($rules->animation_iteration_count);

			//special background rules need to be added for certain stretch behaviors
			if (isset($rules->background_size) && $rules->background_size == 'full_width' && isset($rules->background_image) && $rules->background_image) {
				$rules->background_repeat = "no-repeat";
				$rules->background_position = "top center";
				$rules->background_size = "contain";
			}

			$this->createCssRules("page-".$page->id, $rules, $special_rules, (isset($settings->custom_css))?$settings->custom_css:null, $raw_css);

			//get custom CSS from sections in page
			$mapper = new Application_Model_Mapper_PageSection();
			$sections = $mapper->getPageSections($page_id);

			if ($sections) {
				foreach ($sections as $section) {
					$rules = json_decode($section->json_styles);
					$settings = json_decode($section->json_settings);
					$special_rules = array();
					$raw_css = '';

					//special animation settings convert to consolidated rules
					if ((isset($settings->kb_animation) && $settings->kb_animation == 'kb_animate')) {
						if (!$rules->animation_iteration_count) $rules->animation_iteration_count = 'infinite';
						$special_rules['.background-scale'] = array(
							'animation_iteration_count' => $rules->animation_iteration_count,
							'animation_duration' => $rules->animation_duration,
							'animation_timing_function' => $rules->animation_timing_function,
						);
					}else {//to eliminate conflict with existing animations
						unset($rules->animation_iteration_count);
					}

					//special background rules need to be added for certain stretch behaviors
					if (isset($rules->background_size) && $rules->background_size == 'full_width' && isset($rules->background_image) && $rules->background_image) {
						$rules->background_repeat = "no-repeat";
						$rules->background_position = "top center";
						$rules->background_size = "contain";
					}

					$this->createCssRules("section-block-" . $section->id, $rules, $special_rules, (isset($settings->custom_css))?$settings->custom_css:null, $raw_css);

					$mapper = new Admin_Model_Mapper_PageSectionRow();
					$rows = $mapper->getPageSectionRows($section->id);

					if ($rows) {
						foreach ($rows as $row) {
							//get custom CSS from rows in page
							$rules = json_decode($row->json_styles);
							$settings = json_decode($row->json_settings);
							$special_rules = array();
							$raw_css = '';

							/*
							//special animation settings convert to consolidated rules
							if ((isset($settings->kb_animation) && $settings->kb_animation == 'kb_animate')) {
								if (!$rules->animation_iteration_count) $rules->animation_iteration_count = 'infinite';
								$special_rules['.background-scale'] = array(
									'animation_iteration_count' => $rules->animation_iteration_count,
									'animation_duration' => $rules->animation_duration,
									'animation_timing_function' => $rules->animation_timing_function,
								);
							}else {//to eliminate conflict with existing animations
								unset($rules->animation_iteration_count);
							}
							*/
							//to eliminate conflict with existing animations
							unset($rules->animation_iteration_count);

							//special background rules need to be added for certain stretch behaviors
							if (isset($rules->background_size) && $rules->background_size == 'full_width' && isset($rules->background_image) && $rules->background_image) {
								$rules->background_repeat = "no-repeat";
								$rules->background_position = "top center";
								$rules->background_size = "contain";
							}

							$this->createCssRules("row-block-" . $row->id, $rules, $special_rules, (isset($settings->custom_css))?$settings->custom_css:null, $raw_css);

							$mapper = new Admin_Model_Mapper_PageSectionRowColumn();
							$columns = $mapper->getPageSectionRowColumns($row->id);

							//get custom CSS from modules in page
							if ($columns) {
								$mapper = new Admin_Model_Mapper_PageSectionRowColumnModule();
								foreach ($columns as $column) {
									$modules = $mapper->getPageSectionRowColumnModules($column->id);

									if ($modules) {
										foreach ($modules as $module) {
											$rules = json_decode($module->json_styles);
											$settings = json_decode($module->json_settings);
											$raw_css = '';

											//some modules may have special rules that are applied only to sub elements
											//TODO we may be able to get rid of this by using a SCSS/LESS parser and handling directly in createCssRules function
											$special_rules = array();
											if ($module->type == 'image_gallery') {
												if (isset($settings->image_margin) && !$this->isempty($settings->image_margin)) {
													$special_rules['.gallery__item'] = array(
														'padding-top' => $settings->image_margin / 2,
														'padding-right' => ($settings->image_margin / 2),
														'padding-bottom' => $settings->image_margin / 2,
														'padding-left' => ($settings->image_margin / 2),
														'margin-bottom' => 0
													);
													$special_rules['.gallery__caption'] = array(
														'padding-top' => $settings->image_margin / 2,
														'padding-right' => ($settings->image_margin / 2),
														'padding-bottom' => $settings->image_margin / 2,
														'padding-left' => ($settings->image_margin / 2),
														'margin-bottom' => 0
													);
													$special_rules['.row.gallery-row'] = array(
														'margin-left' => -$settings->image_margin / 2,
														'margin-right' => -$settings->image_margin / 2,
													);
													$special_rules['.gallery-grid'] = array(
														'column-gap' => $settings->image_margin
													);
												}


											}

											/*
											//special animation settings convert to consolidated rules
											if ((isset($settings->kb_animation) && $settings->kb_animation == 'kb_animate')) {
												if (!$rules->animation_iteration_count) $rules->animation_iteration_count = 'infinite';
												$special_rules['.background-scale'] = array(
													'animation_iteration_count' => $rules->animation_iteration_count,
													'animation_duration' => $rules->animation_duration,
													'animation_timing_function' => $rules->animation_timing_function,
												);
											}else {//to eliminate conflict with existing animations
												unset($rules->animation_iteration_count);
											}
											*/
											//to eliminate conflict with existing animations
											unset($rules->animation_iteration_count);

											//special background rules need to be added for certain stretch behaviors
											if (isset($rules->background_size) && $rules->background_size == 'full_width' && isset($rules->background_image) && $rules->background_image) {
												$rules->background_repeat = "no-repeat";
												$rules->background_position = "top center";
												$rules->background_size = "contain";
											}

											$this->createCssRules("module-" . $module->id, $rules, $special_rules, (isset($settings->custom_css))?$settings->custom_css:null, $raw_css);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private function createGlobalCssRules($rules = array(), $mobile_rules = array(), $tablet_rules = array())
	{
		//get list of fonts
		$mapper = new Application_Model_Mapper_Font();
		$fonts = $mapper->getActiveFonts();

		$css = '';

		//imports need to happen outside
		foreach ($rules as $parent => $rule_set) {
			foreach ($rule_set as $rule => $value) {
				switch ($rule) {
					case "custom_font":
						if ($value == 'default') break;

						if (!$this->isempty($value) && !in_array($value,$this->font_families)) {
							if (stripos($value,"http://") !== false || stripos($value,"https://") !== false) {
								//old font method
								$css .= '@import url(\'' . $value . '\');' . "\n";
							}else {
								$font = $fonts[$value];
								if (stripos($font['url'],"http://") !== false || stripos($font['url'],"https://") !== false) {
									//old font method
									$css .= '@import url(\'' . $font['url'] . '\');' . "\n";
								}else {
									if ($font['url']) {
										//$css .= '@import url(\'' . $font['url'] . '\');' . "\n";
										$css .= '@font-face {' . "\n" .
											'font-family: \'' . $font['title'] . '\';' . "\n" .
											'src: url(\'' . $font['url'] . '\');' . "\n" .
											'font-weight: normal;' . "\n" .
											'font-style: normal;' . "\n" .
											'font-display: swap;' . "\n".
											'}' . "\n";
									}
								}
							}

							$this->font_families[] = $value;
						}
						break;
				}
			}
		}

		foreach ($rules as $parent => $rule_set) {
			if ($rule_set) {
				$css .= $parent . ' {' . "\n";

				$css .= $this->convertToCSS($rule_set);

				$css .= '}' . "\n\n";
			}
		}

		//TODO - is there a better way to handle this?
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		//desktop nav logo max width
		$setting = preg_replace("/[^0-9]/", "",$settings_mapper->getValueByCode('header_logo_desktop_width'));
		$max_width = ($setting)?$setting:'200';
		$setting = preg_replace("/[^0-9]/", "",$settings_mapper->getValueByCode('sticky_header_logo_desktop_width'));
		$max_width_sticky = ($setting)?$setting:$max_width;
		$css .= '@media (min-width: 992px) {
				.header .navbar-brand img {
					max-width: '.$max_width.'px!important;
				}
				.header.header-fixed-shrink .navbar-brand img {
					max-width: '.$max_width_sticky.'px!important;
				}
			}
		';
		//tablet nav logo max width
		$setting = preg_replace("/[^0-9]/", "",$settings_mapper->getValueByCode('header_logo_tablet_width'));
		$max_width = ($setting)?$setting:'200';
		$setting = preg_replace("/[^0-9]/", "",$settings_mapper->getValueByCode('sticky_header_logo_tablet_width'));
		$max_width_sticky = ($setting)?$setting:$max_width;
		$css .= '@media (max-width: 991px) {
				.header .navbar-brand img {
					max-width: '.$max_width.'px!important;
				}
				.header.header-fixed-shrink .navbar-brand img {
					max-width: '.$max_width_sticky.'px!important;
				}
			}
		';

		//mobile nav logo max width
		$setting = preg_replace("/[^0-9]/", "",$settings_mapper->getValueByCode('site_logo_mobile_width'));
		$max_width = ($setting)?$setting:'200';
		$css .= '@media (max-width: 767px) {
				.header .navbar-brand img, .header.header-fixed-shrink .navbar-brand img {
					max-width: '.$max_width.'px!important;
				}
			}
		';

		//navigation overrides
		$navigation_settings_mapper = new Admin_Model_Mapper_NavigationItem();
		$mapper = new Admin_Model_Mapper_NavigationItem();
		$main_nav = $mapper->fetchAll();
		foreach ($main_nav as $item) {
			$nav_item = $navigation_settings_mapper->find($item->id);
			$nav_styles = json_decode($nav_item->json_styles);

			$rule_set = array();
			if (isset($nav_styles->link_text_color) && $nav_styles->link_text_color != '') { $rule_set['color'] = $nav_styles->link_text_color; }
			if (isset($nav_styles->link_font_weight) && $nav_styles->link_font_weight != '') { $rule_set['font-weight'] = $nav_styles->link_font_weight; }
			if (isset($nav_styles->link_line_spacing) && $nav_styles->link_line_spacing != '') { $rule_set['line-height'] = $nav_styles->link_line_spacing; }
			if (isset($nav_styles->padding_top) && $nav_styles->padding_top != '') { $rule_set['padding-top'] = $nav_styles->padding_top; }
			if (isset($nav_styles->padding_right) && $nav_styles->padding_right != '') { $rule_set['padding-right'] = $nav_styles->padding_right; }
			if (isset($nav_styles->padding_bottom) && $nav_styles->padding_bottom != '') { $rule_set['padding-bottom'] = $nav_styles->padding_bottom; }
			if (isset($nav_styles->padding_left) && $nav_styles->padding_left != '') { $rule_set['padding-left'] = $nav_styles->padding_left; }
			if (isset($nav_styles->margin_top) && $nav_styles->margin_top != '') { $rule_set['margin-top'] = $nav_styles->margin_top; }
			if (isset($nav_styles->margin_right) && $nav_styles->margin_right != '') { $rule_set['margin-right'] = $nav_styles->margin_right; }
			if (isset($nav_styles->margin_bottom) && $nav_styles->margin_bottom != '') { $rule_set['margin-bottom'] = $nav_styles->margin_bottom; }
			if (isset($nav_styles->margin_left) && $nav_styles->margin_left != '') { $rule_set['margin-left'] = $nav_styles->margin_left; }

			if ($rule_set) {
				$css .= ".header .navbar-nav>li>a.nav-style-id-{$item->id}, .header .dropdown-menu li a.nav-style-id-{$item->id}" . ' {' . "\n";
				$css .= $this->convertToCSS($rule_set);
				$css .= '}' . "\n\n";
			}

			if (!$item->main_nav_parent_id) {
				$rule_set = array();
				if (isset($nav_styles->link_text_color) && $nav_styles->link_text_color != '') { $rule_set['border-bottom-color'] = $nav_styles->link_text_color; }

				if ($rule_set) {
					$css .= ".header .navbar-nav>li>a.nav-style-id-{$item->id}:hover, .header .navbar-nav>li>a.nav-style-id-{$item->id}:active,
				 .header .dropdown-menu li a.nav-style-id-{$item->id}:hover, .header .dropdown-menu li a.nav-style-id-{$item->id}:active" . ' {' . "\n";
					$css .= $this->convertToCSS($rule_set);
					$css .= '}' . "\n\n";
				}
			}
		}

		//mobile rules
		$css .= '@media (max-width: 767px) {';
		foreach ($mobile_rules as $parent => $rule_set) {
			if ($rule_set) {
				$css .= $parent . ' {' . "\n";

				$css .= $this->convertToCSS($rule_set);

				$css .= '}' . "\n\n";
			}
		}
		$css .=	'}';
		//tablet rules
		$css .= '@media (max-width: 991px) {';
		foreach ($tablet_rules as $parent => $rule_set) {
			if ($rule_set) {
				$css .= $parent . ' {' . "\n";

				$css .= $this->convertToCSS($rule_set);

				$css .= '}' . "\n\n";
			}
		}
		$css .=	'}';

		//global site container setting rules
		$container_width = $settings_mapper->getValueByCode('global_container_control');
		if ($container_width) {
			$css .= '@media (min-width: 1440px) {';

			switch ($container_width) {
				case "container-fluid":
					$css .= ".header .container, .main > .container {
						width: 100%;
					}";
					break;
				case "container-fluid-padded":
					$css .= ".header .container, .main > .container {
						width: 100%;
						padding-left: 72px;
						padding-right: 72px;
					}";
					break;
				default:
					$css .= ".container {
						max-width: $container_width !important;
						width: 100%;
					}";
					break;
			}

			$css .=	'}';
		}

		$this->css .= $css;
	}

	private function createCssRules($css_id, $rules = array(), $special_rules = array(), $custom_css, $raw_css)
	{
		$css = '';

		if ($rules && array_filter((array)$rules)) {
			$css .= '#'.$css_id.' {'."\n";

			$css .= $this->convertToCSS($rules);

			$css .= '}'."\n\n";
		}

		//custom CSS rules
		if ($custom_css && array_filter((array)$custom_css)) {
			$custom_rules = explode("}", $custom_css);

			foreach ($custom_rules as $custom_rule) {
				if (trim($custom_rule)) {
					$css .= "\n" . '#' . $css_id . ' ' . $custom_rule . '}' . "\n";
				}
			}
		}

		//special rules needed by some modules
		if ($special_rules) {
			foreach ($special_rules as $parent => $rules) {
				$css .= '#'.$css_id.' '.$parent.' {'."\n";

				$css .= $this->convertToCSS($rules);

				$css .= '}'."\n\n";
			}
		}

		//complex box-shadow for section/row/module
		if (isset($rules->box_shadow_color) && $rules->box_shadow_color) {
			$offsets = array(); $hover = false;
			if (isset($rules->box_shadow_offset_lr) && $rules->box_shadow_offset_lr) {
				if ($rules->box_shadow_offset_lr == preg_replace("/[^0-9.-]/", "", $rules->box_shadow_offset_lr)) $rules->box_shadow_offset_lr .= "px";
				$offsets[] = $rules->box_shadow_offset_lr;
			}else {
				$offsets[] = "0px";
			}
			if (isset($rules->box_shadow_offset_tb) && $rules->box_shadow_offset_tb) {
				if ($rules->box_shadow_offset_tb == preg_replace("/[^0-9.-]/", "", $rules->box_shadow_offset_tb)) $rules->box_shadow_offset_tb .= "px";
				$offsets[] = $rules->box_shadow_offset_tb;
			}else {
				$offsets[] = "0px";
			}

			if (!$rules->box_shadow_size) $rules->box_shadow_size = 0;
			if ($rules->box_shadow_size == preg_replace("/[^0-9.]/", "", $rules->box_shadow_size)) $rules->box_shadow_size .= "px";
			if ($rules->box_shadow_blur == preg_replace("/[^0-9.]/", "", $rules->box_shadow_blur)) $rules->box_shadow_blur .= "px";

			if (isset($rules->box_shadow_on_hover) && $rules->box_shadow_on_hover == 'on') {
				$hover = true;

				$css .= '#'.$css_id.':hover {'."\n";

				$css .= "box-shadow: ".implode(" ", $offsets) . " ".$rules->box_shadow_blur . " ".$rules->box_shadow_size . " " . $rules->box_shadow_color . ';' . "\n";

				$css .= '}'."\n\n";
			}

			$css .= '#'.$css_id.' {'."\n";

			if ($hover) {
				$css .= "box-shadow: none;" . "\n";
			}else {
				$css .= "box-shadow: ".implode(" ", $offsets) . " ".$rules->box_shadow_blur . " ".$rules->box_shadow_size . " " . $rules->box_shadow_color . ';' . "\n";
			}

			if (isset($rules->box_shadow_on_hover) && $rules->box_shadow_on_hover == 'on') {
				$css .= "transition-duration: " . $rules->box_shadow_transition_duration . 's;' . "\n";
			}

			$css .= '}'."\n\n";
		}

		//append raw css at the end (this is the most complex CSS generated and too many custom rules so just adding it this way
		if ($raw_css) {
			$css .= "\n".$raw_css;
		}

		$this->css .= $css;
	}

	private function convertToCSS($rules)
	{
		//get list of fonts
		$mapper = new Application_Model_Mapper_Font();
		$fonts = $mapper->getActiveFonts();

		$css = '';

		foreach ($rules as $rule => $value) {
			if ($this->isempty($value)) continue;

			$rule = str_replace("_","-",$rule);
			switch ($rule) {
				//box shadow rules are complex so ignoring them there here and dealing with them separately
				case "box-shadow-color":
				case "box-shadow-size":
				case "box-shadow-offset-lr":
				case "box-shadow-offset-tb":
				case "box-shadow-on-hover":
				case "box-shadow-transition-duration":
				case "box-shadow-blur":
				case "box_shadow_style":
				case "keyframe-up-down-pixels":
				case "keyframe-left-right-pixels":
					break;

				case "background-image":
					if (!$this->isempty($value)) {
						$css .= 'background-image: url(\'' . $value . '\');' . "\n";
					}
					break;
				case "padding":
					if (!$this->isempty($value)) {
						$padding = explode(" ", $value);

						if (count($padding) == 1) {
							if (isset($padding[0]) && $padding[0] != 'auto') {
								$css .= 'padding: ' . $padding[0] . ';' . "\n";
							}
						}elseif (count($padding) == 2) {
							$css .= 'padding: ' . $padding[0] . ' ' . $padding[1] . ';' . "\n";
						}elseif (count($padding) == 4) {
							if (isset($padding[0]) && $padding[0] != 'auto') {
								$css .= 'padding-top: ' . $padding[0] . ';' . "\n";
							}
							if (isset($padding[1]) && $padding[1] != 'auto') {
								$css .= 'padding-right: ' . $padding[1] . ';' . "\n";
							}
							if (isset($padding[2]) && $padding[2] != 'auto') {
								$css .= 'padding-bottom: ' . $padding[2] . ';' . "\n";
							}
							if (isset($padding[3]) && $padding[3] != 'auto') {
								$css .= 'padding-left: ' . $padding[3] . ';' . "\n";
							}
						}
					}
					break;
				case "margin":
					if (!$this->isempty($value)) {
						$margin = explode(" ", $value);

						if (count($margin) == 1) {
							if (isset($margin[0]) && $margin[0] != 'auto') {
								$css .= 'margin: ' . $margin[0] . ';' . "\n";
							}
						}elseif (count($margin) == 2) {
							$css .= 'margin: ' . $margin[0] . ' ' . $margin[1] . ';' . "\n";
						}elseif (count($margin == 4)) {
							if (isset($margin[0]) && $margin[0] != 'auto') {
								$css .= 'margin-top: ' . $margin[0] . ';' . "\n";
							}
							if (isset($margin[1]) && $margin[1] != 'auto') {
								$css .= 'margin-right: ' . $margin[1] . ';' . "\n";
							}
							if (isset($margin[2]) && $margin[2] != 'auto') {
								$css .= 'margin-bottom: ' . $margin[2] . ';' . "\n";
							}
							if (isset($margin[3]) && $margin[3] != 'auto') {
								$css .= 'margin-left: ' . $margin[3] . ';' . "\n";
							}
						}
					}
					break;
				case "padding-top":
				case "padding-right":
				case "padding-bottom":
				case "padding-left":
				case "margin-top":
				case "margin-right":
				case "margin-bottom":
				case "margin-left":
				case "font-size":
				case "line-height":
				case "min-height":
				case "max-height":
				case "width":
				case "height":
				case "grid-column-gap":
				case "grid-row-gap":
				case "column-gap":
				case "row-gap":
				if (!$this->isempty($value)) {
						if ($value == preg_replace("/[^0-9,.-]/", "", $value)) $value .= "px";
						$css .= $rule . ': ' . $value . ';' . "\n";
					}
					break;
				case "max-width":
					if (!$this->isempty($value)) {
						if ($value == preg_replace("/[^0-9,.-]/", "", $value)) $value .= "px !important";
						$css .= $rule . ': ' . $value . ';' . "\n";
					}
					break;
				case "custom-font":
					if (!$this->isempty($value)) {
						if ($value == 'default') break;

						if (stripos($value,"http://") !== false || stripos($value,"https://") !== false) {
							//old font method
							$qs = parse_url($value, PHP_URL_QUERY);
							$qs_array = explode("&", $qs);
							$nvp = array();
							foreach ($qs_array as $pair) {
								$nv = explode("=", $pair);
								if (array_key_exists(0, $nv) && array_key_exists(1, $nv)) {
									$nvp[$nv[0]] = $nv[1];
								}
							}
							$font_name = urldecode($nvp['family']);
						}else {
							$font = $fonts[$value];
							$font_name = $font['title'];
						}

						$css .= 'font-family: \''. $font_name .'\';' . "\n";
					}
					break;
				case "video-url":
				case "video-poster":
					//this is only used internally to apply a video as a background
					break;
				case "animation-duration-seconds":
					//special animate.css rules
					if (!$this->isempty($value)) {
						$css .= 'animation-duration: ' . $value . 's;' . "\n";
						$css .= '-webkit-animation-duration: ' . $value . 's;' . "\n";
						$css .= '-moz-animation-duration: ' . $value . 's;' . "\n";
						$css .= '-o-animation-duration: ' . $value . 's;' . "\n";
					}
					break;
				case "animation-delay-seconds":
					//special animate.css rules
					if (!$this->isempty($value)) {
						$css .= 'animation-delay: ' . $value . 's;' . "\n";
						$css .= '-webkit-animation-delay: ' . $value . 's;' . "\n";
						$css .= '-moz-animation-delay: ' . $value . 's;' . "\n";
						$css .= '-o-animation-delay: ' . $value . 's;' . "\n";
					}
					break;
				case "border-radius":
				case "border-width":
					if (!$this->isempty($value)) {
						$radius = explode(" ", $value);

						if (count($radius) >= 1) {
							foreach ($radius as $k => $rad) {
								if ($rad == preg_replace("/[^0-9,.-]/", "", $rad)) $rad .= "px";
								$radius[$k] = $rad;
							}
							$value = implode(" ", $radius);
						}else {
							if ($value == preg_replace("/[^0-9,.-]/", "", $value)) $value .= "px";
						}

						$css .= $rule . ': ' . $value . ';' . "\n";
					}

					break;
				case "animation-delay":
				case "animation-duration":
					if (!$this->isempty($value)) {
						if ($value == preg_replace("/[^0-9,.-]/", "", $value)) $value .= "s";
						$css .= $rule . ': ' . $value . ';' . "\n";
					}
					break;
				case "animation-iteration-count":
					if ($this->isempty($value)) {
						$value = "infinite";
					}
					$css .= $rule . ': ' . $value . ';' . "\n";
					break;
				case "from":
				case "to":
					if (!$this->isempty($value)) {
						$css .= $rule . $value . "\n";
					}
					break;
				default:
					if (!$this->isempty($value)) {
						$css .= $rule . ': ' . $value . ';' . "\n";
					}
					break;
			}
		}

		return $css;
	}

	public function cssStamp($page_id)
	{
		$stamp = @filemtime($this->cache_dir.'/zend_cache---css_site');
		if (!$stamp) $stamp = time();

		return $stamp;
	}

	public function buildHeader($override_template = null)
	{
		$cache_id = 'navigation_horizontal_'.$override_template;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		if (!$override_template) {
			$header_type = $settings_mapper->getValueByCode('header_type');
			if (!$header_type) $header_type = "default";
		}else {
			$header_type = $override_template;
		}

		if(!($cache->test($cache_id)) || $this->_wireframe_mode) {
			$mapper = new Application_Model_Mapper_NavigationItem();
			$this->_view->main_nav = $mapper->mainNavArray();

			ksort($this->_view->main_nav);

			//home page setting
			$this->_view->home_code = $settings_mapper->getValueByCode('home_page');

			//aggregate enabled social media icons
			$social = [];
			if ($settings_mapper->getValueByCode('facebook_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('facebook_url')) {
					$social['facebook'] = $url;
				}
			}
			if ($settings_mapper->getValueByCode('twitter_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('twitter_url')) {
					$social['twitter'] = $url;
				}
			}
			if ($settings_mapper->getValueByCode('instagram_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('instagram_url')) {
					$social['instagram'] = $url;
				}
			}
			if ($settings_mapper->getValueByCode('youtube_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('youtube_url')) {
					$social['youtube'] = $url;
				}
			}
			if ($settings_mapper->getValueByCode('pinterest_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('pinterest_url')) {
					$social['pinterest'] = $url;
				}
			}
			if ($settings_mapper->getValueByCode('google_plus_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('google_plus_url')) {
					$social['google-plus'] = $url;
				}
			}
			if ($settings_mapper->getValueByCode('linkedin_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('linkedin_url')) {
					$social['linkedin'] = $url;
				}
			}
			if ($settings_mapper->getValueByCode('tumblr_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('tumblr_url')) {
					$social['tumblr'] = $url;
				}
			}
			if ($settings_mapper->getValueByCode('reddit_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('reddit_url')) {
					$social['reddit'] = $url;
				}
			}
			if ($settings_mapper->getValueByCode('email_icon_display') == 'yes') {
				if ($url = $settings_mapper->getValueByCode('email_url')) {
					$social['envelope'] = "mailto:".$url;
				}
			}

			$this->_view->social = $social;
			$this->_view->wireframe = $this->_wireframe_mode; //setting to change header links to wireframe mode

			$html = $this->_view->render("/html-build/navigation/{$header_type}.phtml");

			if (!$this->_wireframe_mode) {
				$cache->save($html, $cache_id, array('navigation'));
			}
		}else {
			$html = $cache->load($cache_id);
		}

		preg_match_all('/<style.*?>(.+?)<\/style>/si', $html, $matches);
		if (isset($matches[1])) {
			foreach ($matches[1] as $style_block) {
				$this->module_style_block .= $style_block;
			}
		}

		return preg_replace("/<style.*?>.+?<\/style>/si","",$html);
	}

	public function buildFooter()
	{
		//no reason to cache modules since doing full page cache for now
		/*
		$cache_id = 'footer';
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);

		if(!($cache->test($cache_id)) || $this->_wireframe_mode) {
		*/
			$html = '';

			//desktop footer
			$mapper = new Application_Model_Mapper_Page();
			$page = $mapper->doesExists(['code'=>'footer']);

			$mapper = new Application_Model_Mapper_PageSection();
			$sections = $mapper->getPageSections($page->id);

			$html .= '<footer class="hidden-xs">';
			$second_div = false;

			if ($sections) {
				foreach ($sections as $section) {
					$columns = $mapper->countSectionColumns($section->id);
					$fullwidth = false;
					switch ($section->type) {
						case 'standard':
							$html .= '<div id="section-block-'.$section->id.'" class="container '.$section->class.'">';
							break;
						case 'fullwidth':
							$fullwidth = true;
							$html .= '<div id="section-block-'.$section->id.'" class="section-block '.$section->class.'">';

							/*
							$second_div = true;
							if ($columns > 1) {
								$html .= '<div id="section-block-'.$section->id.'" class="container-fluid '.$section->class.'">';
							}else {
								//$fullwidth = true;
								$html .= '<div id="section-block-'.$section->id.'" class="fullwidth '.$section->class.'">';
							}
							$html .= '<div class="container">';
							*/
							break;
						case 'custom':
							$html .= '<div id="section-block-'.$section->id.'" class="custom_section '.$section->class.'">';
							break;
						default:
							break;
					}
					$html .= $this->buildSection($section->id, $fullwidth, false, false, $page->id);

					$html .= '</div>';
					if ($second_div) {
						$html .= '</div>';
					}
				}
			}
			$html .= '</footer>';

			//Mobile footer
			$mapper = new Application_Model_Mapper_Page();
			$page = $mapper->doesExists(['code'=>'mobile_footer']);

			$mapper = new Application_Model_Mapper_PageSection();
			$sections = $mapper->getPageSections($page->id);

			$html .= '<footer class="visible-xs">';
			$second_div = false;

			if ($sections) {
				foreach ($sections as $section) {
					$columns = $mapper->countSectionColumns($section->id);
					$fullwidth = false;
					switch ($section->type) {
						case 'standard':
							$html .= '<div id="section-block-'.$section->id.'" class="container '.$section->class.'">';
							break;
						case 'fullwidth':
							$fullwidth = true;
							$html .= '<div id="section-block-'.$section->id.'" class="section-block '.$section->class.'">';

							/*
							$second_div = true;
							if ($columns > 1) {
								$html .= '<div id="section-block-'.$section->id.'" class="container-fluid '.$section->class.'">';
							}else {
								//$fullwidth = true;
								$html .= '<div id="section-block-'.$section->id.'" class="fullwidth '.$section->class.'">';
							}
							$html .= '<div class="container">';
							*/
							break;
						case 'custom':
							$html .= '<div id="section-block-'.$section->id.'" class="custom_section '.$section->class.'">';
							break;
						default:
							break;
					}
					$html .= $this->buildSection($section->id, $fullwidth, false, false, $page->id);

					$html .= '</div>';
					if ($second_div) {
						$html .= '</div>';
					}
				}
			}
			$html .= '</footer>';
/*
			if (!$this->_wireframe_mode) {
				$cache->save($html, $cache_id, array('footer', 'page_' . $page->id));
			}
		}else {
			$html = $cache->load($cache_id);
		}
*/

		preg_match_all('/<style.*?>(.+?)<\/style>/si', $html, $matches);
		if (isset($matches[1])) {
			foreach ($matches[1] as $style_block) {
				$this->module_style_block .= $style_block;
			}
		}

		return preg_replace("/<style.*?>.+?<\/style>/si","",$html);
	}

	public function buildCustomHeader($id)
	{
		$cache_id = 'custom_header_'.$id;
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);

		if(!($cache->test($cache_id)) || $this->_wireframe_mode) {
			$mapper = new Application_Model_Mapper_Page();
			$page = $mapper->doesExists(['id'=>$id]);

			$mapper = new Application_Model_Mapper_PageSection();
			$sections = $mapper->getPageSections($page->id);

			$html = '<header>';
			$second_div = false;

			if ($sections) {
				foreach ($sections as $section) {
					$columns = $mapper->countSectionColumns($section->id);
					$fullwidth = false;
					switch ($section->type) {
						case 'standard':
							$html .= '<div id="section-block-'.$section->id.'" class="container '.$section->class.'">';
							break;
						case 'fullwidth':
							$second_div = true;
							if ($columns > 1) {
								$html .= '<div id="section-block-'.$section->id.'" class="container-fluid '.$section->class.'">';
							}else {
								//$fullwidth = true;
								$html .= '<div id="section-block-'.$section->id.'" class="fullwidth '.$section->class.'">';
							}
							$html .= '<div class="container">';
							break;
						case 'custom':
							$html .= '<div id="section-block-'.$section->id.'" class="custom_section '.$section->class.'">';
							break;
						default:
							break;
					}
					$html .= $this->buildSection($section->id, $fullwidth, false, false, $page->id);

					$html .= '</div>';
					if ($second_div) {
						$html .= '</div>';
					}
				}
			}
			$html .= '</header>';

			if (!$this->_wireframe_mode) {
				$cache->save($html, $cache_id, array('header', 'custom_header', 'page_' . $page->id));
			}
		}else {
			$html = $cache->load($cache_id);
		}

		preg_match_all('/<style.*?>(.+?)<\/style>/si', $html, $matches);
		if (isset($matches[1])) {
			foreach ($matches[1] as $style_block) {
				$this->module_style_block .= $style_block;
			}
		}

		return preg_replace("/<style.*?>.+?<\/style>/si","",$html);
	}

	public function getModuleStyleBlock()
	{
		return $this->module_style_block;
	}
}
