<?php
class Application_Model_ProductImage extends Application_Model_Abstract
{
	public $product_id;
	public $image_src;
	public $alt_text;
	public $sort_order;
	public $primary;
	public $secondary;
}
