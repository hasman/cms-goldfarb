<?php
class Application_Model_OrderProfile extends Application_Model_Abstract
{
	public $order_id;
    public $promo_code;
    public $affiliate_code;
	public $user_agent;
    public $remote_addr;
    public $site;
    public $custom_item_1;
	public $custom_item_1_value;
	public $custom_item_2;
	public $custom_item_2_value;
	public $custom_item_3;
	public $custom_item_3_value;
	public $custom_item_4;
	public $custom_item_4_value;
	public $custom_item_5;
	public $custom_item_5_value;
	public $custom_item_6;
	public $custom_item_6_value;
}
