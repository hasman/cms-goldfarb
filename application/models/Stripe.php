<?php
//use Stripe\Stripe as Stripe;

class Application_Model_Stripe
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
		$this->cfg = (isset($options['stripe']))?$options['stripe']:array();

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		if ($value = $settings_mapper->getValueByCode('ecommerce_stripe_secret_key')) {
			$this->cfg['secret_key'] = $value;
		}
		if ($value = $settings_mapper->getValueByCode('ecommerce_stripe_publishable_key')) {
			$this->cfg['public_key'] = $value;
		}

		\Stripe\Stripe::setApiKey($this->cfg['secret_key']);
	}

	/*
	 * token: the Stripe token (token of the payment card)
	 */
	public function charge($token, $amount, $email_address, $description = null)
	{
		$customer_id = $this->findCustomerId($email_address);
		if (!$customer_id) {
			$customer = \Stripe\Customer::create([
				'email' => $email_address,
				'source' => $token,
			]);

			$customer_id = $customer->id;
		}

		$charge = \Stripe\Charge::create([
			'customer' => $customer_id,
			'amount'   => $amount * 100,
			'currency' => 'usd',
			'description' => $description
		]);

		//Cny_Debug::prettyPrint($charge,true);

		return $charge->id;
	}

	public function refund($transactionId, $amount = null, $description = null)
	{
		$refund = \Stripe\Refund::create([
			"charge" => $transactionId,
			"amount" => ($amount)?($amount * 100):null,
			'description' => $description
		]);

		//print_r($refund);

		if (isset($refund) && $refund->status == 'succeeded') {
			return array('response' => $refund, 'error' => null);
		}else {
			return "";
		}

	}

	public function createSubscription($token, $email_address, $plan_id, $metadata = array(), $additional_params = array())
	{
		try {
			$customer_id = $this->findCustomerId($email_address);
			if (!$customer_id) {
				$customer = \Stripe\Customer::create([
					'email' => $email_address,
					'source' => $token,
				]);

				$customer_id = $customer->id;
			}

			if (isset($customer->id)) {
				$charge = \Stripe\Subscription::create([
					"customer" => $customer_id,
					"items" => [
						[
							"plan" => $plan_id,
						],
					],
					"metadata" => $metadata,
					$additional_params
				]);

				//print_r($charge);

				return $charge->id;
			}
		}catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function updateSubscription($subscription_id, $additional_params = array())
	{
		try {
			$charge = \Stripe\Subscription::update($subscription_id, [
				$additional_params
			]);

			//print_r($charge);

			return $charge->id;
		}catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function createCardToken($cc_number, $exp_month, $exp_year, $cvv2 = null)
	{
		$token = \Stripe\Token::create([
			'card' => [
				'number' => $cc_number,
				'exp_month' => $exp_month,
				'exp_year' => $exp_year,
				'cvc' => $cvv2,
			],
		]);

		return $token->id;
	}

	public function findCustomerId($email)
	{
		$customer = \Stripe\Customer::all([
			'email' => $email
		]);

		if (isset($customer->data[0]->id)) {
			return $customer->data[0]->id;
		}

		return null;
	}

	public function findSubscription($subscription_id)
	{
		$subscription = \Stripe\Subscription::retrieve($subscription_id);

		return $subscription;
	}

	public function getTransaction($transaction_id)
	{
		$charge = \Stripe\Charge::retrieve($transaction_id);

		//Cny_Debug::prettyPrint($charge,true);

		return $charge;
	}

	public function getCustomer($customer_id)
	{
		$customer = \Stripe\Customer::retrieve($customer_id);

		//Cny_Debug::prettyPrint($customer,true);

		return $customer;
	}
}
