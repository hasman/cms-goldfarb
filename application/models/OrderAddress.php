<?php
class Application_Model_OrderAddress extends Application_Model_Abstract
{
	public $order_id;
	public $type;
	public $first_name;
	public $last_name;
	public $company;
	public $address;
	public $address2;
	public $city;
	public $county = '';
	public $region;
	public $postcode;
	public $country;
	public $phone;
}
