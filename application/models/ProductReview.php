<?php
class Application_Model_ProductReview extends Application_Model_Abstract
{
	public $product_id;
	public $product_variant_id;
	public $rating;
	public $name;
	public $email;
	public $review;
	public $status;
	public $is_spam;
	public $remote_addr;
	public $user_agent;
	public $referrer;
}
