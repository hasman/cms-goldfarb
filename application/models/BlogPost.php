<?php
class Application_Model_BlogPost extends Application_Model_Abstract
{
	public $code;
	public $title;
	public $meta_keywords;
	public $meta_description;
	public $summary;
	public $content;
	public $thumbnail;
	public $thumbnail_alt_tag;
	public $image;
	public $image_alt_tag;
	public $posted_date;
	public $posted_date_format;
	public $byline;
	public $status;
	public $external_url;
}
