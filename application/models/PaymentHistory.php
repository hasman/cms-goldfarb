<?php
class Application_Model_PaymentHistory extends Application_Model_Abstract
{
	public $order_id;
	public $type;
	public $cc_type;
	public $amount;
	public $amount_remaining;
	public $transaction_id;
	public $response;
	public $status;
	public $voided = 'No';
	public $note;
}
