<?php
class Application_Model_Customer extends Application_Model_Abstract
{
	public $password;
	public $first_name;
	public $last_name;
	public $email;
	public $status;
	public $last_login;
	public $reset_code;
	public $reset_expire;
	public $failed_attempts;
}
