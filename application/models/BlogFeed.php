<?php
class Cny_Model_BlogFeed
{
	public function __construct($url = null)
	{
		$this->blog_url = $url;
	}

	public function fetchFeed($limit = null, $tag = null, $category = null)
	{
		$cache_dir = APPLICATION_PATH.'/../tmp/cache';
		$frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 60 );
		$backendOptions  = array( 'cache_dir' => $cache_dir );


		if ($limit && !is_int($limit)) {
			$tag = $limit;
			unset($limit);
		}

		if (!$this->blog_url) {
			$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
			$blog_url = ($settings_mapper->getValueByCode('blog_url'))?$settings_mapper->getValueByCode('blog_url'):'blog';

			$cache_id = 'blog_feed_' . preg_replace('/[^\da-z]/i', '', $tag);
			$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

			if (!($cache->test($cache_id))) {
				$mapper = new Application_Model_Mapper_BlogPost();
				$channel = $mapper->fetchArticles($limit, $tag);

				$cache->save($channel, $cache_id, array('blog_feed', 'tag_' . preg_replace('/[^\da-z]/i', '', $tag)));
			} else {
				$channel = $cache->load($cache_id);
			}

			$results = array();
			foreach ($channel as $item) {
				if ($item->external_url) {
					$post_url = $item->external_url;
				}else {
					$post_url = '/' . $blog_url . '/' . $item->code;
				}

				$results[] = array(
					'title' => $item->title,
					'description' => $item->summary,
					'description_text' => strip_tags($item->summary),
					'date' => $item->posted_date,
					'link' => $post_url,
					'link_external' => ($item->external_url)?"Yes":"No",
					'thumbnail' => $item->thumbnail,
					'byline' => $item->byline
				);
			}
		}elseif (stripos($this->blog_url, "news.google.com") !== false) {
			$url = $this->blog_url;

			$cache_id = 'google_news_feed_';
			$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

			if(!($cache->test($cache_id))) {
				$channel = new Zend_Feed_Rss($url);

				$cache->save($channel, $cache_id, array('google_news_feed'));
			}else {
				$channel = $cache->load($cache_id);
			}

			$results = array();
			foreach ($channel as $item) {
				$results[] = array(
					'title' => $item->title(),
					'description' => $item->description(),
					'description_text' => strip_tags($item->description()),
					'date' => $item->pubDate(),
					'link' => $item->link(),
					'thumbnail' => ''
					//'thumbnail' => ($item->{'media:thumbnail'}->getDOM()->getAttribute('url'))?$item->{'media:thumbnail'}->getDOM()->getAttribute('url'):$item->thumbnail()
				);
			}
		}else {
			$url = $this->blog_url;
			$qs = false;
			if ($tag) {
				if (!$qs) {
					$url .= "?";
					$qs = true;
				}else {
					$url .= "&";
				}
				$url .= 'tag='.str_replace(" ","-",$tag);
			}
			if ($category) {
				if (!$qs) {
					$url .= "?";
					$qs = true;
				}else {
					$url .= "&";
				}
				$url .= 'cat='.str_replace(" ","-",$category);
			}

			$cache_id = 'wp_feed_'.preg_replace('/[^\da-z]/i', '', $tag).'_'.preg_replace('/[^\da-z]/i', '', $category);
			$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

			if(!($cache->test($cache_id))) {
				$channel = new Zend_Feed_Rss($url);

				$cache->save($channel, $cache_id, array('wordpress_feed','tag_'.preg_replace('/[^\da-z]/i', '', $tag)));
			}else {
				$channel = $cache->load($cache_id);
			}

			$results = array();
			foreach ($channel as $item) {
				$results[] = array(
					'title' => $item->title(),
					'description' => $item->description(),
					'description_text' => strip_tags($item->description()),
					'date' => $item->pubDate(),
					'link' => $item->link(),
					'thumbnail' => ($item->{'media:thumbnail'}->getDOM()->getAttribute('url'))?$item->{'media:thumbnail'}->getDOM()->getAttribute('url'):$item->thumbnail()
				);
			}
		}

		return $results;
	}

}
