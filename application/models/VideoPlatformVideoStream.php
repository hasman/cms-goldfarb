<?php
class Application_Model_VideoPlatformVideoStream extends Application_Model_Abstract
{
	public $video_platform_video_id;
	public $filename;
	public $format_name;
	public $resolution;
	public $transcode_status;
	public $public_url;
	public $aws_job_id;
}
