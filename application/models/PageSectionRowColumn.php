<?php
class Application_Model_PageSectionRowColumn extends Application_Model_Abstract
{
	public $id;
	public $page_section_row_id;
	public $col_class;
	public $class;
	public $sort_order = 9999;
	public $created;
	public $modified;
}
