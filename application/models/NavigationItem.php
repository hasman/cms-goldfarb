<?php
class Application_Model_NavigationItem extends Application_Model_Abstract {
	public $id;
	public $title;
	public $url;
	public $target;
	public $main_nav_status = 'Disabled';
	public $main_nav_parent_id;
	public $main_nav_sort_order;
	public $page_id;
	public $block_id;
	public $mobile_url;
	public $panel_open_type;
	public $category_id;
	public $json_styles;
	public $json_settings;
	public $created;
	public $modified;
	public $status = 'Enabled';
}
