<?php
class Application_Model_Product extends Application_Model_Abstract
{
	public $code;
	public $title;
	public $title_sort;
	public $description;
	public $summary;
	public $sku;
	public $barcode;
	public $inventory;
	public $price;
	public $msrp;
	public $weight;
	public $length;
	public $width;
	public $height;
	public $meta_title;
	public $meta_description;
	public $meta_keywords;
	public $status;
	public $warehouse;
	public $available_date;
}
