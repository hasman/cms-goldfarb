<?php

class Application_Model_Emails
{
	var $_paypal;

	static $_defaultView;
	protected $_view;

	protected function getDefaultView()
	{
		if (self::$_defaultView === null) {
			self::$_defaultView = new Zend_View();
			self::$_defaultView->setScriptPath(APPLICATION_PATH.'/views/scripts');
			self::$_defaultView->setHelperPath(APPLICATION_PATH.'/views/helpers');
		}
		return self::$_defaultView;
	}

	public function __construct()
	{
		$this->_view = self::getDefaultView();
		$this->_view->template_data = array();

		$this->_db = Zend_Db_Table::getDefaultAdapter();

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_full_domain = (isset($options['site']['full_secure']))?$options['site']['full_secure']:null;

		if (isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"]) {
			$secure_https = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == 'on') ? "s" : "";
			$this->_full_domain = "http" . $secure_https . "://" . $_SERVER["SERVER_NAME"];
		}

		$this->_view->domain = $this->_view->template_data['domain'] = $this->_full_domain;
		$this->_view->asset_url = $this->_view->template_data['asset_url'] = $this->_full_domain . "/assets/images/emails";

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->_default_from = $settings_mapper->getValueByCode('email_from_address');
		$this->_default_from_name = $settings_mapper->getValueByCode('email_from_name');
		$this->_default_reply_to = $settings_mapper->getValueByCode('email_reply_address');
		$this->_default_reply_to_name = $settings_mapper->getValueByCode('email_reply_name');


		//Email Title
		$this->_view->template_data['settings']['title_font'] = ($settings_mapper->getValueByCode('email_title_font'))?$settings_mapper->getValueByCode('email_title_font'):"Helvetica";
		$this->_view->template_data['settings']['title_size'] = ($settings_mapper->getValueByCode('email_title_size'))?$settings_mapper->getValueByCode('email_title_size'):"34px";
		$this->_view->template_data['settings']['title_weight'] = ($settings_mapper->getValueByCode('email_title_weight'))?$settings_mapper->getValueByCode('email_title_weight'):"400";
		$this->_view->template_data['settings']['title_color'] = ($settings_mapper->getValueByCode('email_title_color'))?$settings_mapper->getValueByCode('email_title_color'):"#0a0a0a";
		$this->_view->template_data['settings']['title_line_height'] = ($settings_mapper->getValueByCode('email_title_line_height'))?$settings_mapper->getValueByCode('email_title_line_height'):"1.3";

		//Email Message
		$this->_view->template_data['settings']['message_text'] = ($settings_mapper->getValueByCode('email_message_text'))?$settings_mapper->getValueByCode('email_message_text'):"Thanks for shopping with us!";
		$this->_view->template_data['settings']['message_font'] = ($settings_mapper->getValueByCode('email_message_font'))?$settings_mapper->getValueByCode('email_message_font'):"Helvetica";
		$this->_view->template_data['settings']['message_size'] = ($settings_mapper->getValueByCode('email_message_size'))?$settings_mapper->getValueByCode('email_message_size'):"34px";
		$this->_view->template_data['settings']['message_weight'] = ($settings_mapper->getValueByCode('email_message_weight'))?$settings_mapper->getValueByCode('email_message_weight'):"400";
		$this->_view->template_data['settings']['message_color'] = ($settings_mapper->getValueByCode('email_message_color'))?$settings_mapper->getValueByCode('email_message_color'):"#0a0a0a";
		$this->_view->template_data['settings']['message_line_height'] = ($settings_mapper->getValueByCode('email_message_line_height'))?$settings_mapper->getValueByCode('email_message_line_height'):"1.3";

		//Email Labels
		$this->_view->template_data['settings']['label_font'] = ($settings_mapper->getValueByCode('email_label_font'))?$settings_mapper->getValueByCode('email_label_font'):"Helvetica";
		$this->_view->template_data['settings']['label_size'] = ($settings_mapper->getValueByCode('email_label_size'))?$settings_mapper->getValueByCode('email_label_size'):"34px";
		$this->_view->template_data['settings']['label_weight'] = ($settings_mapper->getValueByCode('email_label_weight'))?$settings_mapper->getValueByCode('email_label_weight'):"400";
		$this->_view->template_data['settings']['label_color'] = ($settings_mapper->getValueByCode('email_label_color'))?$settings_mapper->getValueByCode('email_label_color'):"#0a0a0a";
		$this->_view->template_data['settings']['label_line_height'] = ($settings_mapper->getValueByCode('email_label_line_height'))?$settings_mapper->getValueByCode('email_label_line_height'):"1.3";


		//Email Information
		$this->_view->template_data['settings']['font_family'] = ($settings_mapper->getValueByCode('email_font'))?$settings_mapper->getValueByCode('email_font'):"Helvetica";
		$this->_view->template_data['settings']['font_size'] = ($settings_mapper->getValueByCode('email_font_size'))?$settings_mapper->getValueByCode('email_font_size'):"16px";
		$this->_view->template_data['settings']['font_weight'] = ($settings_mapper->getValueByCode('email_weight'))?$settings_mapper->getValueByCode('email_weight'):"400";
		$this->_view->template_data['settings']['text_color'] = ($settings_mapper->getValueByCode('email_text_color'))?$settings_mapper->getValueByCode('email_text_color'):"#0a0a0a";
		$this->_view->template_data['settings']['line_height'] = ($settings_mapper->getValueByCode('email_line_height'))?$settings_mapper->getValueByCode('email_line_height'):"1.3";
		$this->_view->template_data['settings']['link_color'] = ($settings_mapper->getValueByCode('email_link_color'))?$settings_mapper->getValueByCode('email_link_color'):"#2795ca";


		//Email Global
		$this->_view->template_data['settings']['background_color'] = ($settings_mapper->getValueByCode('email_background_color'))?$settings_mapper->getValueByCode('email_background_color'):"#f7f7f7";
		$this->_view->template_data['settings']['body_color'] = ($settings_mapper->getValueByCode('email_body_color'))?$settings_mapper->getValueByCode('email_body_color'):"#fefefe";
		$this->_view->template_data['settings']['table_color'] = ($settings_mapper->getValueByCode('email_table_color'))?$settings_mapper->getValueByCode('email_table_color'):"#ebebeb";
		$this->_view->template_data['settings']['table_border_color'] = ($settings_mapper->getValueByCode('email_table_border_color'))?$settings_mapper->getValueByCode('email_table_border_color'):"#444";

		$this->_view->site_name = $this->_view->template_data['site_name'] = $this->_site_name = ($site_name = $settings_mapper->getValueByCode('email_site_name'))?$site_name:$settings_mapper->getValueByCode('site_name');
		$this->_view->logo = $this->_view->template_data['logo'] = $settings_mapper->getValueByCode('email_site_logo');
		$this->_view->template_data['logo_width'] = preg_replace("/[^0-9]/","",$settings_mapper->getValueByCode('email_site_logo_width'));
		$this->_view->template_data['logo_height'] = preg_replace("/[^0-9]/","",$settings_mapper->getValueByCode('email_site_logo_height'));

		//aggregate enabled social media icons
		$social = [];
		$socials = array(
			'facebook' => 'Facebook',
			'twitter' => 'Twitter',
			'instagram' => 'Instagram',
			'youtube' => 'YouTube',
			'pinterest' => 'Pinterest',
			'linkedin' => 'LinkedIn',
			//'tumblr' => 'Tumblr',
			//'reddit' => 'Reddit',
			'email' => 'Email'
		);
		foreach ($socials as $tag => $name) {
			if ($settings_mapper->getValueByCode("email_social_{$tag}_icon_display") == 'yes') {
				if ($url = $settings_mapper->getValueByCode("{$tag}_url")) {
					$social[$tag] = $url;
				}
			}
		}
		$this->_view->social = $this->_view->template_data['social'] = $social;

		//address for footer
		$this->_view->template_data['footer_address'] = array(
			'address1' => $settings_mapper->getValueByCode('email_street_address_1'),
			'address2' => $settings_mapper->getValueByCode('email_street_address_2'),
			'city' => $settings_mapper->getValueByCode('email_city'),
			'region' => $settings_mapper->getValueByCode('email_region'),
			'postcode' => $settings_mapper->getValueByCode('email_postcode'),
		);

		$this->_view->template_data['support_phone'] = $settings_mapper->getValueByCode('email_support_phone_number');
		$this->_view->template_data['support_email'] = $settings_mapper->getValueByCode('email_support_email');
	}

	public function orderConfirmation($order_id)
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$mapper = new Application_Model_Mapper_Order();
		$this->_view->order = $order = $mapper->find($order_id);

		if (!$order) {
			//echo "invalid order id <br/>\n";
			return false;
		}

		//data
		$mapper = new Application_Model_Mapper_OrderAddress();
		$this->_view->billing_address = $mapper->findByOrderId($order_id, 'billing');
		$this->_view->shipping_address = $mapper->findByOrderId($order_id, 'shipping');

		$mapper = new Application_Model_Mapper_OrderItem();
		$this->_view->products = $mapper->findByOrderId($order_id);

		$mapper = new Application_Model_Mapper_OrderTotal();
		$this->_view->totals = $mapper->findByOrderId($order_id);

		$mapper = new Application_Model_Mapper_OrderPayment();
		$this->_view->payment = $mapper->findByOrderId($order_id);

		$mapper = new Application_Model_Mapper_OrderShipping();
		$this->_view->shipping = $mapper->findByOrderId($order_id);

		$mapper = new Application_Model_Mapper_OrderProfile();
		$this->_view->order_profile = $mapper->findByOrderId($order_id);

		//email
		$mail = new Zend_Mail();

		$mail_body = $this->_view->render("/emails/order_confirmation.phtml");
		$mail->setFrom($this->_default_from, $this->_default_from_name);
		$mail->setReplyTo($this->_default_reply_to, $this->_default_reply_to_name);
		$mail->setSubject("Order Confirmation #".$order_id);

		if ($settings_mapper->getValueByCode('ecommerce_bcc_email_status') == 'Enabled') {
			if ($bcc_address = $settings_mapper->getValueByCode('ecommerce_bcc_email_address')) {
				$mail->addBcc($bcc_address);
			}
		}

		$mail->addTo($order->email);

		$mail->setBodyHtml($mail_body);
		if ($order->email) {
			$sent = $mail->send();
		}

		if ($sent) {
			//echo "email sent <br/>\n";
			return true;
		}else {
			//echo "failed to send email <br/>\n";
			return false;
		}
	}

	public function paymentConfirmation($module_content, $email_addresses, $panels)
	{
		$this->_view->intro = $module_content->email_confirmation_text;
		$this->_view->bottom_text = (isset($module_content->email_confirmation_bottom_text) && $module_content->email_confirmation_bottom_text)?$module_content->email_confirmation_bottom_text:'';
		$this->_view->panels = $panels;

		//email
		$mail = new Zend_Mail();

		$mail_body = $this->_view->render("/emails/payment_confirmation.phtml");

		$mail->setFrom(isset($module_content->from_email_address) ? $module_content->from_email_address : $this->_default_from, ($module_content->from_email_address)?null:$this->_default_from_name );
		$mail->setReplyTo(isset($module_content->from_email_address) ? $module_content->from_email_address : $this->_default_reply_to, $this->_default_reply_to_name);

		foreach ($email_addresses as $email_address) {
			$mail->addTo($email_address);
		}

		if (isset($module_content->to_email_address) && $module_content->to_email_address) {
			$mail->addBcc($module_content->to_email_address);
		}

		$mail->setSubject($this->_site_name.' Payment Confirmation');

		$mail->setBodyHtml($mail_body);
		$sent = $mail->send();

		if ($sent) {
			//echo "email sent <br/>\n";
			return true;
		}else {
			//echo "failed to send email <br/>\n";
			return false;
		}
	}

	public function generic($content, $email_addresses, $subject, $from = null, $bcc_addresses = null, $replyto = null)
	{
		//email
		$mail = new Zend_Mail();

		$this->_view->content = $content;
		$mail_body = $this->_view->render("/emails/generic.phtml");

		$replyto_name = '';
		if (!$replyto) {
			$replyto = ($from)?$from:$this->_default_reply_to;
			$replyto_name = $this->_default_reply_to_name;
		}

		$mail->setFrom(($from)?$from:$this->_default_from, ($from)?null:$this->_default_from_name);
		$mail->setReplyTo($replyto, $replyto_name);

		if (is_array($email_addresses)) {
			foreach ($email_addresses as $email_address) {
				$mail->addTo($email_address);
			}
		}else {
			$mail->addTo($email_addresses);
		}

		if ($bcc_addresses) {
			if (is_array($bcc_addresses)) {
				foreach ($bcc_addresses as $email_address) {
					$mail->addBcc($email_address);
				}
			} else {
				$mail->addBcc($bcc_addresses);
			}
		}

		$mail->setSubject($subject);

		$mail->setBodyHtml($mail_body);
		$sent = $mail->send();

		if ($sent) {
			//echo "email sent <br/>\n";
			return true;
		}else {
			//echo "failed to send email <br/>\n";
			return false;
		}
	}

	public function orderVideoConfirmation($order_id)
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$mapper = new Application_Model_Mapper_VideoPlatformOrder();
		$this->_view->order = $order = $mapper->find($order_id);

		if (!$order) {
			//echo "invalid order id <br/>\n";
			return false;
		}

		//data
		$mapper = new Application_Model_Mapper_Customer();
		$customer = $mapper->find($order->customer_id);

		$mapper = new Application_Model_Mapper_VideoPlatformOrderAddress();
		$this->_view->billing_address = $mapper->findByOrderId($order_id, 'billing');

		$mapper = new Application_Model_Mapper_VideoPlatformOrderItem();
		$this->_view->products = $mapper->findByOrderId($order_id);

		$mapper = new Application_Model_Mapper_VideoPlatformOrderPayment();
		$this->_view->payment = $mapper->findByOrderId($order_id);

		//email
		$mail = new Zend_Mail();

		$mail_body = $this->_view->render("/emails/order_video_confirmation.phtml");
		$mail->setFrom($this->_default_from, $this->_default_from_name);
		$mail->setReplyTo($this->_default_reply_to, $this->_default_reply_to_name);
		$mail->setSubject("Video Order Confirmation #".$order_id);

		if ($settings_mapper->getValueByCode('ecommerce_bcc_email_status') == 'Enabled') {
			if ($bcc_address = $settings_mapper->getValueByCode('ecommerce_bcc_email_address')) {
				$mail->addBcc($bcc_address);
			}
		}

		$mail->addTo($customer->email);

		$mail->setBodyHtml($mail_body);
		if ($customer->email) {
			$sent = $mail->send();
		}

		if ($sent) {
			//echo "email sent <br/>\n";
			return true;
		}else {
			//echo "failed to send email <br/>\n";
			return false;
		}
	}

	public function shippedNotification($order_id, $products = array())
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$mapper = new Application_Model_Mapper_Order();
		$this->_view->order = $order = $mapper->find($order_id);

		if (!$order) {
			//echo "invalid order id <br/>\n";
			return false;
		}

		//data
		$mapper = new Application_Model_Mapper_OrderAddress();
		$this->_view->billing_address = $mapper->findByOrderId($order_id, 'billing');
		$this->_view->shipping_address = $mapper->findByOrderId($order_id, 'shipping');

		$this->_view->products = $products;

		$mapper = new Application_Model_Mapper_OrderTotal();
		$this->_view->totals = $mapper->findByOrderId($order_id);

		$mapper = new Application_Model_Mapper_OrderPayment();
		$this->_view->payment = $mapper->findByOrderId($order_id);

		$mapper = new Application_Model_Mapper_OrderShipping();
		$this->_view->shipping = $mapper->findByOrderId($order_id);

		$mapper = new Application_Model_Mapper_OrderProfile();
		$this->_view->order_profile = $mapper->findByOrderId($order_id);

		//get a list of the unique tracking numbers
		$this->_view->tracking_numbers = array();
		foreach ($products as $product) {
			$this->_view->tracking_numbers[$product->tracking_code] = $product->tracking_code;
		}

		//email
		$mail = new Zend_Mail();

		$mail_body = $this->_view->render("/emails/shipped_notification.phtml");
		$mail->setFrom($this->_default_from, $this->_default_from_name);
		$mail->setReplyTo($this->_default_reply_to, $this->_default_reply_to_name);
		$mail->setSubject("Shipment Notification - Order #".$order_id);

		if ($settings_mapper->getValueByCode('ecommerce_bcc_email_status') == 'Enabled') {
			if ($bcc_address = $settings_mapper->getValueByCode('ecommerce_bcc_email_address')) {
				$mail->addBcc($bcc_address);
			}
		}

		$mail->addTo($order->email);

		$mail->setBodyHtml($mail_body);
		if ($order->email) {
			$sent = $mail->send();
		}

		if ($sent) {
			//echo "email sent <br/>\n";
			return true;
		}else {
			//echo "failed to send email <br/>\n";
			return false;
		}
	}
}
