<?php
class Application_Model_Mapper_VideoPlatformImageTag extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformImageTag');
		$this->setEntity('Application_Model_VideoPlatformImageTag');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByCode($code)
	{
		$select = $this->getDbTable()->select()->where("code = ?", $code);
		$result = $this->getDbTable()->fetchRow($select);
		return $result;
	}

	public function fetchAllActive()
	{
		$select = $this->getDbTable()->select()->where("status= ?", 'Enabled');
		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

}
