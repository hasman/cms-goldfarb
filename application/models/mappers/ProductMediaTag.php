<?php
class Application_Model_Mapper_ProductMediaTag extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductMediaTag');
		$this->setEntity('Application_Model_ProductMediaTag');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAllActive()
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('pit'=>$this->_table_prefix.'product_media_tag'),'*')
			->joinLeft(array('it'=>$this->_table_prefix.'media_tags'),'pit.media_tag_id = it.id',array('title'=>'title','code'=>'code'))
			->joinLeft(array('pi'=>$this->_table_prefix.'product_media'),'pi.id = pit.product_media_id','')
			->where("pit.status = ?","Enabled")
			->group('pi.id')
		;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchByProductMediaId($media_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('pit'=>$this->_table_prefix.'product_media_tag'),'*')
			->joinLeft(array('it'=>$this->_table_prefix.'media_tags'),'pit.media_tag_id = it.id',array('title'=>'title','code'=>'code'))
			->where("it.status = ?","Enabled")
			->where("pit.product_media_id = ?",$media_id);

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchProductMediaByTagId($tag_id, $product_id = null)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('pi'=>$this->_table_prefix.'product_media'),'*')
			->joinLeft(array('pit'=>$this->_table_prefix.'product_media_tag'),'pit.product_media_id = pi.id','')
			->where("pit.media_tag_id = ?",$tag_id)
			->order("pi.sort_order ASC");

		if ($product_id) {
			$select->where("pi.product_id = ?", $product_id);
		}

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}
}
