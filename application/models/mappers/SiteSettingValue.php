<?php
class Application_Model_Mapper_SiteSettingValue extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_SiteSettingValue');
		$this->setEntity('Application_Model_SiteSettingValue');

		$options = Zend_Registry::get('configuration')->toArray();

		$this->cache_dir = $cache_dir = $options['dir']['cache'];
		$this->frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function getValueByCode($code, $default = null)
	{
		$cache_id = 'site_settings';
		$cache = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);

		if(!($cache->test($cache_id))) {
			$select = $this->getDbTable()->select();
			$results = $this->fetchAll($select);

			$array = array();
			foreach ($results as $row) {
				$array[$row->code] = $row->content;
			}

			$cache->save($array, $cache_id);
		}else {
			$array = $cache->load($cache_id);
		}

		if (isset($array[$code])) {
			return $array[$code];
		}else {
			return $default;
		}

		/*
		$select = $this->getDbTable()->select()
			->where('code = ?', $code);
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else {
			return current($result)->content;
		}
		*/
	}
}
