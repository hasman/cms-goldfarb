<?php
class Application_Model_Mapper_OrderTotal extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_OrderTotal');
		$this->setEntity('Application_Model_OrderTotal');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByOrderId($order_id)
	{
		$select = $this->getDbTable()->select()->where("order_id = ?", $order_id)->order('sort_order ASC');
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}
	public function findByOrderIdAndCode($order_id, $code)
	{
		$select = $this->getDbTable()->select()->where("order_id = ?", $order_id)->where("code = ?", $code);

		$result = $this->getDbTable()->fetchRow($select);
		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			return new $entity($result->toArray());
		} else {
			return false;
		}
	}
}
