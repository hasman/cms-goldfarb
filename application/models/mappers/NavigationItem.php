<?php
class Application_Model_Mapper_NavigationItem extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_NavigationItem');
		$this->setEntity('Application_Model_NavigationItem');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function mainNavArray()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('main_nav_status = ?','Enabled')
			//->where('main_nav_parent_page_id = ?',0)
			->order('main_nav_sort_order')
		;
		$results = $this->fetchAll($select);

		$rows = [];
		foreach ($results as $result) {
			$rows[] = $result->toArray();
		}

		return $this->buildTree($rows);
	}

	public function buildTree(array $elements, $parentId = 0) {
		$branch = array();

		foreach ($elements as $element) {
			if ($element['main_nav_parent_id'] == $parentId) {
				$children = $this->buildTree($elements, $element['id']);
				if ($children) {
					$element['children'] = $children;
				}
				$branch[] = $element;
			}
		}

		return $branch;
	}

}
