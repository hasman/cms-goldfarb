<?php
class Application_Model_Mapper_CustomerAddress extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_CustomerAddress');
		$this->setEntity('Application_Model_CustomerAddress');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByCustomerId($customer_id, $type = null, $default = null, $return_null = false)
	{
		$select = $this->getDbTable()->select()->where("customer_id = ?", $customer_id);

		if ($default) {
			$select->where("default_address = ?",$default);
		}

		if ($type) {
			$select->where("address_type = ?",$type);

			$result = $this->getDbTable()->fetchAll($select);

			if ($default == 'Yes') {
				$row = $result->current();
				if ($row)
					return new $this->_entity($row->toArray());
				else {
					if ($return_null) return null;

					$entity = $this->getEntity();
					return new $entity;
				}
			}else {
				if ($result) {
					$collection = array();
					$entity = $this->getEntity();
					foreach ($result as $row) {
						$collection[] = new $entity($row->toArray());
					}
					return $collection;
				} else {
					return false;
				}
			}
		}else {
			$result = $this->getDbTable()->fetchAll($select);

			if ($result) {
				$collection = array();
				$entity = $this->getEntity();
				foreach ($result as $row) {
					$collection[] = new $entity($row->toArray());
				}
				return $collection;
			} else {
				return false;
			}
		}
	}

	public function makeDefault($customer_id, $address_id, $address_type)
	{
		$data['modified'] = date('Y-m-d H:i:s');
		$data['default_address'] = 'No';

		$where = array();
		$where[] = $this->getDbTable()->getAdapter()->quoteInto('address_type = ?', $address_type);
		$where[] = $this->getDbTable()->getAdapter()->quoteInto('customer_id = ?', $customer_id);

		$this->getDbTable()->update($data, implode(" AND ",$where));

		//set new default
		$data['default_address'] = 'Yes';

		$where = array();
		$where[] = $this->getDbTable()->getAdapter()->quoteInto('id = ?', $address_id);
		$where[] = $this->getDbTable()->getAdapter()->quoteInto('address_type = ?', $address_type);
		$where[] = $this->getDbTable()->getAdapter()->quoteInto('customer_id = ?', $customer_id);
		$this->getDbTable()->update($data, implode(" AND ",$where));
	}

}
