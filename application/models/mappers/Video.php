<?php
class Application_Model_Mapper_Video extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_Video');
		$this->setEntity('Application_Model_Video');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchActive($sort = 'title ASC')
	{
		if (!$sort) $sort = 'title ASC';

		$select = $this->getDbTable()->select()->where('status = ?', 'Enabled')->order($sort);
		return $this->fetchAll($select);
	}

    public function findEnabled($id)
    {
        $select = $this->getDbTable()->select()->where('status = ?', 'Enabled')->where('id = ?', $id);
        $result = $this->fetchAll($select);

        if ($result) {
        	return current($result);
        }else {
        	return false;
        }
    }

	public function findEnabledByCode($code)
	{
		$select = $this->getDbTable()->select()->where('status = ?', 'Enabled')->where('code = ?', $code);
		$result = $this->fetchAll($select);

		if ($result) {
			return current($result);
		}else {
			return false;
		}
	}
}
