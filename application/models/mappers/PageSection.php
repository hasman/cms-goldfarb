<?php
class Application_Model_Mapper_PageSection extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_PageSection');
		$this->setEntity('Application_Model_PageSection');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function getPageSections($page_id)
	{
		$select = $this->getDbTable()->select()
			->where('page_id = ?', $page_id)
			->order('sort_order');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}

	public function countSectionColumns($section_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('psr'=>$this->_table_prefix.'page_section_rows'),null)
			->joinLeft(array('psrc'=>$this->_table_prefix.'page_section_row_columns'),'psrc.page_section_row_id = psr.id', 'id')
			->where('psr.page_section_id = ?', $section_id)
			;
		$result = $this->fetchAll($select);

		return count($result);
	}
}
