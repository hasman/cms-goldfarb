<?php
class Application_Model_Mapper_Promo extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_Promo');
		$this->setEntity('Application_Model_Promo');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByCode($code, $amount){
		//first check new coupon type single use code
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('psc'=>$this->_table_prefix.'promo_single_codes'),'*')
			->where("psc.code = ?",$code)
			->where("psc.used_timestamp IS NULL")
			->where('psc.expiration >= ?',new Zend_Db_Expr("NOW()"))
			->limit(1)
		;
		$result =  $this->getDbTable()->fetchRow($select);

		if ($result) {
			return (object)array(
				'id' => 0, //$result->promo_id
				'threshold_id' => 0,
				'title' => 'Coupon',
				'sitewide' => 'Yes',
				'type' => 'fixed_order',
				'amount' => $result->amount,
				'code' => $code,
				'threshold' => 0,
				'shipping_restriction' => null,
				'shipping_amount' => null,
				'shipping_qty_threshold' => null,
				'shipping_type' => null
			);
		}

		//regular promotions
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('p'=>$this->_table_prefix.'promos'),'*')
			->joinLeft(array('psc'=>$this->_table_prefix.'promo_single_codes'),'psc.promo_id = p.id AND psc.used_timestamp IS NULL','')
			->joinLeft(array('pt'=>$this->_table_prefix.'promo_thresholds'),'pt.promo_id = p.id',array('threshold_id'=>'id','threshold','amount','type','shipping_amount','shipping_type','shipping_restriction','shipping_qty_threshold','shipping_amount_over_threshold','sitewide','banner'))
			->where("(p.code = ? OR psc.code = ?)",$code)
			->where("(pt.threshold <= ?)",$amount)
			->where('p.status = ?','Enabled')
			->where("p.trigger = 'code' OR p.trigger = 'single_use_code' ")
			->where('p.utc_start_datetime <= ?',new Zend_Db_Expr("NOW()"))->where('p.utc_end_datetime >= ?',new Zend_Db_Expr("NOW()"))
			->order('pt.threshold DESC')
			->limit(1)
		;
		$result =  $this->getDbTable()->fetchRow($select);

		if ($result) {
			return $result;
		}

		return null;
	}

	public function validateCode($code, $amount){
		//first check new coupon type single use code
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('psc'=>$this->_table_prefix.'promo_single_codes'),'*')
			->where("psc.code = ?",$code)
			->where("psc.used_timestamp IS NULL")
			->where('psc.expiration >= ?',new Zend_Db_Expr("NOW()"))
			->limit(1)
		;
		$result =  $this->getDbTable()->fetchRow($select);

		if ($result) {
			if (strtotime($result->expiration) < time()) {
				return "Coupon Expired";
			}else if ($result->used_timestamp) {
				return "Coupon already redeemed";
			}else {
				return null;
			}
		}

		//regular promotions
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('p'=>$this->_table_prefix.'promos'),'*')
			->joinLeft(array('psc'=>$this->_table_prefix.'promo_single_codes'),'psc.promo_id = p.id',array('used_timestamp'))
			->joinLeft(array('pt'=>$this->_table_prefix.'promo_thresholds'),'pt.promo_id = p.id',array('threshold'))
			->where("(p.code = ? OR psc.code = ?)",$code)
			//->where("(p.threshold <= ?)",$amount)
			->where('p.status = ?','Enabled')
			->where("p.trigger = 'code' OR p.trigger = 'single_use_code' ")
			//->where('start_date <= ?',new Zend_Db_Expr("DATE(NOW())"))->where('end_date >= ?',new Zend_Db_Expr("DATE(NOW())"))
		;
		$result =  $this->getDbTable()->fetchRow($select);

		if ($result) {
			if (strtotime($result->utc_start_datetime) > time()) {
				return "Coupon not in system";
			}else if (strtotime($result->utc_end_datetime) < time()) {
				return "Coupon Expired";
			}else if ($result->used_timestamp) {
				return "Coupon already redeemed";
			}else if ($result->threshold > $amount) {
				$more = $result->threshold - $amount;
				return "Spend $".number_format($more,2)." to activate coupon";
			}else {
				return null;
			}
		}

		return "Coupon not in system";
	}

	public function fetchAutomaticPromo($amount, $ignore_cart = false){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('p'=>$this->_table_prefix.'promos'),'*')
			->joinLeft(array('pt'=>$this->_table_prefix.'promo_thresholds'),'pt.promo_id = p.id',array('threshold_id'=>'id','threshold','amount','type','shipping_amount','shipping_type','shipping_restriction','shipping_qty_threshold','shipping_amount_over_threshold','sitewide','banner'))
			->where("(pt.threshold <= ?)",$amount)
			->where('p.status = ?','Enabled')
			->where('p.trigger = ?','auto')
			->where('p.utc_start_datetime <= ?',new Zend_Db_Expr("NOW()"))->where('p.utc_end_datetime >= ?',new Zend_Db_Expr("NOW()"))
			->order('pt.threshold DESC')
		;
		$results =  $this->getDbTable()->fetchAll($select);

		if (count($results) > 0) {
			$this->_cart = new Zend_Session_Namespace('cart');

			if (count($results) > 1) {
				$prod = array();
				$product_mapper = new Application_Model_Mapper_Product();
				$variant_mapper = new Application_Model_Mapper_ProductVariant();

				foreach ($this->_cart->items as $item_id => $item) {
					$prod['product'] = $product_mapper->find($item['product_id']);

					if ($prod['product']->status == 'Disabled') {
						break;
					}

					$prod['qty'] = $item['qty'];
					$prod['price'] = $prod['product']->price;
					$prod['msrp'] = $prod['product']->msrp;

					if (isset($item['variant_id']) && $item['variant_id']) {
						$prod['variant'] = $variant_mapper->find($item['variant_id']);

						if ($prod['variant']->status == 'Disabled') {
							break;
						}

						$prod['price'] = $prod['variant']->price;
						$prod['msrp'] = $prod['variant']->msrp;
					}

					$prods[] = $prod;
				}

				$best = null;
				$best_discount = 0;
				foreach ($results as $promo) {
					$discount = 0;

					if ($promo->sitewide == 'Yes') {
						foreach ($prod as $item_id => $info) {
							if ($promo->type == "fixed") {
								$discount += $promo->amount;
							} elseif ($promo->type == "percent") {
								$discount += round($info['price'] * ($promo->amount / 100), 2);
							} elseif ($promo->type == "percent_msrp") {
								$new_price = round($info['msrp'] - ($info['msrp'] * ($promo->amount / 100)), 2);
								if ($new_price < $info['price']) {
									$discount += $info['price'] - $new_price;
								}
							} elseif ($promo->type == "price") {
								$discount += ($info['price'] - $promo->amount);
							}
						}
					} else {
						$mapper = new Application_Model_Mapper_PromoThresholdProduct();
						$promo_products = $mapper->fetchProductArrayByPromoThresholdId($promo->threshold_id, 'include');

						foreach ($prod as $item_id => $info) {
							if (in_array($item_id, $promo_products)) {
								if ($promo->type == "fixed") {
									$discount += $promo->amount;
								} elseif ($promo->type == "percent") {
									$discount += round($info['price'] * ($promo->amount / 100), 2);
								} elseif ($promo->type == "percent_msrp") {
									$new_price = round($info['msrp'] - ($info['msrp'] * ($promo->amount / 100)), 2);
									if ($new_price < $info['price']) {
										$discount += $info['price'] - $new_price;
									}
								} elseif ($promo->type == "price") {
									$discount += ($info['price'] - $promo->amount);
								}
							}
						}
					}

					//try estimate shipping savings to help determine best promo
					if ($promo->shipping_type) {
						//since we don't know the location lets just assume average shipping price
						$shipping = 7.99;

						if ($promo->shipping_type == 'fixed') {
							$discount += $promo->shipping_amount;
						}elseif ($promo->shipping_type == 'price') {
							$discount += ($shipping - $promo->shipping_amount);
						}
					}

					if ($discount > $best_discount) {
						$best_discount = $discount;
						$best = $promo;
					}
				}

				return $best;
			} else {
				$result = $results[0];

				if ($result->sitewide == 'No') {
					$mapper = new Application_Model_Mapper_PromoThresholdProduct();
					$promo_products = $mapper->fetchProductArrayByPromoThresholdId($result->threshold_id);

					$cnt = 0;

					$product_mapper = new Application_Model_Mapper_Product();
					$variant_mapper = new Application_Model_Mapper_ProductVariant();
					foreach ($this->_cart->items as $item_id => $item) {
						$prod['product'] = $product_mapper->find($item['product_id']);

						if ($prod['product']->status == 'Disabled') {
							break;
						}

						if (in_array($item['product_id'], $promo_products['products'])) {
							$cnt += $item['qty'];
						}

						if (isset($item['variant_id']) && $item['variant_id']) {
							$prod['variant'] = $variant_mapper->find($item['variant_id']);

							if ($prod['variant']->status == 'Disabled') {
								break;
							}

							if (in_array($item['variant_id'], $promo_products['variants'])) {
								$cnt += $item['qty'];
							}
						}
					}

					if ($cnt > 0 || $ignore_cart) {
						return $result;
					} else {
						return null;
					}
				}

				return $result;
			}
		}

		return null;
	}
}
