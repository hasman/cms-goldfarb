<?php
class Application_Model_Mapper_VideoPlatformVideoAnalytic extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoAnalytic');
		$this->setEntity('Application_Model_VideoPlatformVideoAnalytic');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function updateAnalytic($customer_id = null, $hash, $played, $seconds_plus)
	{
		if (!$customer_id) {
			$this->getDbTable()->update(array(
				'played' => $played,
				'seconds_played' => new Zend_Db_Expr("seconds_played + $seconds_plus")
			), " hash = '$hash' ");
		}else {
			$this->getDbTable()->update(array(
				'played' => $played,
				'seconds_played' => new Zend_Db_Expr("seconds_played + $seconds_plus")
			), "customer_id = '$customer_id' AND hash = '$hash' ");
		}

		return true;
	}

	public function fetchByCustomerId($customer_id, $limit = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->where("customer_id = ?", $customer_id)
			->order('created DESC')
			->group('video_platform_video_id')
		;

		if ($limit) {
			$select->limit($limit);
		}

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			return $result;
		}

		return null;
	}
}
