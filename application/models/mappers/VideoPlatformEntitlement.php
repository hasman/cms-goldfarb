<?php
class Application_Model_Mapper_VideoPlatformEntitlement extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformEntitlement');
		$this->setEntity('Application_Model_VideoPlatformEntitlement');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function checkEntitlement($customer_id, $video_id)
	{
		$select = $this->getDbTable()->select()
			->where("customer_id = ?", $customer_id)
			->where("video_platform_video_id = ?", $video_id)
			->where("expiration IS NULL OR expiration >= NOW()")
		;

		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function fetchEntitlement($customer_id, $video_id)
	{
		$select = $this->getDbTable()->select()
			->where("customer_id = ?", $customer_id)
			->where("video_platform_video_id = ?", $video_id)
			->order('expiration DESC')
		;

		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function fetchByCustomerId($customer_id, $type = null)
	{
		//$select = $this->getDbTable()->select()
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('poi'=>$this->_table_prefix.'video_platform_order_items'), array('title', 'price','purchase_type','total','discount','tax'))
			->joinLeft(array('pv'=>$this->_table_prefix.'video_platform_videos'),'poi.video_platform_video_id = pv.id', array('code','filename','transcode_status','thumbnail_src','poster_src','notify_email'))
			->joinLeft(array('po'=>$this->_table_prefix.'video_platform_orders'),'poi.video_platform_order_id = po.id', array('id','customer_id','bill_method','created'))
			->joinLeft(array('cu'=>$this->_table_prefix.'customers'),'po.customer_id = cu.id', array('first_name','last_name','email','status'))
			->where("cu.id = ?", $customer_id)
		;


		if ($type) {
			$select->where("poi.purchase_type = ?", $type);
		}

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}

		/*if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}*/
	}

}
