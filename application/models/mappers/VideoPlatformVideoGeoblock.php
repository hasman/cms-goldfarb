<?php
class Application_Model_Mapper_VideoPlatformVideoGeoblock extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoGeoblock');
		$this->setEntity('Application_Model_VideoPlatformVideoGeoblock');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()
			->where("video_platform_video_id = ?", $video_id)
			->order("sort_order")
		;

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function checkVideoGeoBlock($video_id)
	{
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$blocks = $this->fetchByVideoId($video_id);

		if (!$blocks) return false;

		//use MaxMind Web services
		if ($settings_mapper->getValueByCode('maxmind_account_id') && $settings_mapper->getValueByCode('maxmind_license_key')) {
			$maxmind = new Application_Model_MaxMind();

			$json = $maxmind->getInfoFromIP();
			if ($json) {
				$data = json_decode($json);
			}

			if ($settings_mapper->getValueByCode('maxmind_block_anonymizers') == 'Enabled' &&
				((isset($data->traits->is_anonymous) && $data->traits->is_anonymous) || (isset($data->traits->is_anonymous_vpn) && $data->traits->is_anonymous_vpn))
			) {
				return true;
			}

			$normalized_data = [
				'country' => (isset($data->country->iso_code) && $data->country->iso_code)?$data->country->iso_code:'',
				'region' => (isset($data->subdivisions[0]->iso_code) && $data->subdivisions[0]->iso_code)?$data->subdivisions[0]->iso_code:'',
				'city' => (isset($data->city->names->en) && $data->city->names->en)?$data->city->names->en:'',
				'postcode' => (isset($data->postal->code) && $data->postal->code)?$data->postal->code:'',
			];
		}

		$allow_all = false;
		foreach ($blocks as $block) {
			if ($block->status == 'Disallow' && $block->geo_code == $normalized_data[$block->granularity]) {
				return true;
			}elseif ($block->status == 'Allow' && $block->geo_code == $normalized_data[$block->granularity]) {
				return false;
			}elseif ($block->status == 'Allow' && $block->geo_code == 'all') {
				$allow_all = true;
			}
		}

		if (!$allow_all) {
			return true;
		}

		//block by default if rules but no match for some reason
		return true;
	}
}
