<?php
class Application_Model_Mapper_Template extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_Template');
		$this->setEntity('Application_Model_Template');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByCode($code)
	{
		$select = $this->getDbTable()->select()->where("code = ?", $code);
		$result = $this->getDbTable()->fetchRow($select);
		return $result;
	}
}
