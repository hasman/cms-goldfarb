<?php
class Application_Model_Mapper_TemplateBlock extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_TemplateBlock');
		$this->setEntity('Application_Model_TemplateBlock');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByTemplateId($template_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('tb'=>$this->_table_prefix.'template_blocks'),'*')
			->joinLeft(array('p'=>$this->_table_prefix.'pages'), 'tb.block_id = p.id', array('block_title'=>'title'))
			->where("tb.template_id = ?", $template_id)
		;
		$results = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($results as $row) {
			$row = $row->toArray();
			$row['settings'] = json_decode($row['json_settings']);
			$array[$row['position_code']] = $row;
		}

		return $array;
	}
}
