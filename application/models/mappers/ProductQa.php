<?php
class Application_Model_Mapper_ProductQa extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductQa');
		$this->setEntity('Application_Model_ProductQa');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_id = ?', $product_id)
			->where('status = ?', 'Approved')
			->order('created DESC');
		$result = $this->fetchAll($select);

		return $result;
	}

	public function fetchByProductVariantId($product_variant_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_variant_id = ?', $product_variant_id)
			->where('status = ?', 'Approved')
			->order('created DESC');
		$result = $this->fetchAll($select);

		return $result;
	}

}
