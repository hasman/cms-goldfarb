<?php
class Application_Model_Mapper_ProductProfile extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductProfile');
		$this->setEntity('Application_Model_ProductProfile');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_cache_dir = $cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->_backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function save($entity)
	{
		$output = parent::save($entity);

		//reset customer cache of profile data
		$cache_id = 'product_profile_' . $entity->product_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);
		$cache->remove($cache_id);

		return $output;
	}

	public function getProfileByProductId($product_id)
	{
		$cache_id = 'product_profile_' . $product_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);

		if (!($cache->test($cache_id))) {
			$select = $this->getDbTable()->select()->setIntegrityCheck(false)
				->from(array('pp'=>$this->_table_prefix.'product_profiles'), array('id','value','product_attribute_id'))
				->joinLeft(array('pa'=>$this->_table_prefix.'product_attributes'),'pp.product_attribute_id = pa.id', array('code','title','field_type','options'))
				->where('pp.product_id = ?',$product_id)
			;
			$results = $this->getDbTable()->fetchAll($select);

			$data = array();
			foreach ($results as $row) {
				$data[$row->code] = array('title'=> $row->title, 'value' => $row->value, 'product_profile_id' => $row->id, 'field_type' => $row->field_type, 'options' => $row->options, 'product_attribute_id' => $row->product_attribute_id);
			}

			$cache->save($data, $cache_id, array('product_profile', 'product_' . $product_id));
		} else {
			$data = $cache->load($cache_id);
		}

		return $data;
	}

}
