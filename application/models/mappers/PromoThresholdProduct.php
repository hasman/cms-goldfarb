<?php
class Application_Model_Mapper_PromoThresholdProduct extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_PromoThresholdProduct');
		$this->setEntity('Application_Model_PromoThresholdProduct');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByPromoThresholdId($id, $type = 'include'){
		$select = $this->getDbTable()->select()->where("promo_threshold_id=?",$id)->where('type =?',$type);

		return  $this->getDbTable()->fetchAll($select);
	}

	public function fetchProductArrayByPromoThresholdId($id, $type = 'include'){
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('ptp'=>$this->_table_prefix.'promo_threshold_products'),'*')
			->where("ptp.promo_threshold_id=?",$id)
			->where('ptp.type =?',$type);

		//TODO - figure out BOGO so you can re-enable it as an option
		if ($type == 'bogo') {
			$select
				->joinLeft(array('p'=>$this->_table_prefix.'products'),'ptp.product_id = p.id','')
				->where('p.inventory IS NULL OR p.inventory > 1');
		}

		$results =  $this->getDbTable()->fetchAll($select);

		$array = array('products'=>array(),'variants'=>array());
		foreach ($results as $row) {
			//$array[] = array('product_id'=>$row->product_id, 'variant_id'=>$row->variant_id);
			if ($row->product_id) {
				$array['products'][] = $row->product_id;
			}
			if ($row->variant_id) {
				$array['variants'][] = $row->variant_id;
			}
		}

		return $array;
	}

}
