<?php
class Application_Model_Mapper_Product extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_Product');
		$this->setEntity('Application_Model_Product');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchActive($sort_type = null, $limit = null)
	{
		$select = $this->getDbTable()->select()
			->where('status <> ?', 'Disabled')
		;

		switch ($sort_type) {
			case "a-z":
				$select->order('title_sort ASC');
				break;
			case "z-a":
				$select->order('title_sort DESC');
				break;
			case "newest":
				$select->order('created ASC');
				break;
			case "oldest":
				$select->order('created DESC');
				break;
			case "random":
				$select->order(new Zend_Db_Expr("RAND()"));
				break;
			default:
				$select->order('title_sort', 'title');
				break;
		}

		if ($limit) {
			$select->limit($limit);
		}

		return $this->fetchAll($select);
	}

    public function findEnabled($id)
    {
        $select = $this->getDbTable()->select()->where('status <> ?', 'Disabled')->where('id = ?', $id);
        $result = $this->fetchAll($select);

        if ($result) {
        	return current($result);
        }else {
        	return false;
        }
    }

	public function findEnabledByCode($code)
	{
		$select = $this->getDbTable()->select()->where('status <> ?', 'Disabled')->where('code = ?', $code);
		$result = $this->fetchAll($select);

		if ($result) {
			return current($result);
		}else {
			return false;
		}
	}

	public function findByCode($code)
	{
		$select = $this->getDbTable()->select()
			->where("code = ?", $code)
			->where("status = ?", "Enabled")
		;

		$result = $this->getDbTable()->fetchRow($select);

		return $result;
	}
}
