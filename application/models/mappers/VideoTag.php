<?php
class Application_Model_Mapper_VideoTag extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoTag');
		$this->setEntity('Application_Model_VideoTag');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAllActive()
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('vt'=>$this->_table_prefix.'video_tag'),'*')
			->joinLeft(array('t'=>$this->_table_prefix.'video_tags'),'vt.video_tag_id = t.id',array('title'=>'title','code'=>'code'))
			->joinLeft(array('v'=>$this->_table_prefix.'videos'),'v.id = vt.video_id','')
			->where("t.status = ?","Enabled")
			->where("v.status = ?","Enabled")
			->group('t.id')
		;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('vt'=>$this->_table_prefix.'video_tag'),'*')
			->joinLeft(array('t'=>$this->_table_prefix.'video_tags'),'vt.video_tag_id = t.id',array('title'=>'title','code'=>'code'))
			->where("t.status = ?","Enabled")
			->where("vt.video_id = ?",$video_id);

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchVideosByTagId($tag_id, $sort = 'title ASC')
	{
		if (!$sort) $sort = 'title ASC';

		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('v'=>$this->_table_prefix.'videos'),'*')
			->joinLeft(array('vt'=>$this->_table_prefix.'video_tag'),'vt.video_id = v.id','')
			->where("v.status = ?","Enabled")
			->where("vt.video_tag_id = ?",$tag_id)
			->order('v.'.$sort)
			;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

}
