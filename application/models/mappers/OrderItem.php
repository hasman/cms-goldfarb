<?php
class Application_Model_Mapper_OrderItem extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_OrderItem');
		$this->setEntity('Application_Model_OrderItem');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function save($entity, $update_inv = true)
	{
        if ($entity) {
	        $settings_mapper = new Application_Model_Mapper_SiteSettingValue();
	        if ($settings_mapper->getValueByCode('inventory_tracking_status') == 'Enabled') {
		        //check inventory and update stock
		        if (isset($entity->variant_id) && $entity->variant_id) {
			        $mapper = new Application_Model_Mapper_ProductVariant();
			        $item = $mapper->find($entity->variant_id, false);
		        } else {
			        $mapper = new Application_Model_Mapper_Product();
			        $item = $mapper->find($entity->product_id, false);
		        }

		        if (!is_null($item->inventory) && $update_inv) {
			        $new_inventory = $item->inventory - $entity->qty;
			        $item->inventory = $new_inventory;

			        $new_status = $settings_mapper->getValueByCode('inventory_out_of_stock_change_status');
			        if ($new_status && $new_inventory <= 0) {
			        	$item->status = $new_status;
			        }

			        /*
					if ($new_inventory <= 0) {
						//send email
						$mail = new Zend_Mail();
						$mail->setBodyHtml("
						The following product is now out of stock.<br/><br/>
						Product: {$item->title} <br/>
						SKU: {$item->sku} <br/>
						Inventory: $new_inventory
						");
						$mail->setFrom("inventory@.com");
						if (APPLICATION_ENV == 'production') {

						}else {
							$mail->addTo('craig@cyber-ny.com');
						}
						$mail->setSubject("Product Out of Stock: {$item->title}");
						$mail->send();
					}
					*/

			        $mapper->save($item);
		        }
	        }
        }
		return parent::save($entity);
	}

	public function findByOrderId($order_id, $alphabetize = false)
	{
		$select = $this->getDbTable()->select()->from($this->_table_prefix."order_items")->where("order_id = ?", $order_id);

		if ($alphabetize) {
			$select->order(array('title ASC', 'order_items.type ASC'));
		}

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function addTracking($order_id, $sku, $tracking_code, $shipped_date)
	{
		$select = $this->getDbTable()->select()->from($this->_table_prefix."order_items")
			->where("order_id = ?", $order_id)
			->where('warehouse_sku = ? OR sku = ?', $sku)
			->where('status = ?','Processing');
		$row = $this->getDbTable()->fetchRow($select);

		if ($row) {
			$result = $row->toArray();
			$result['tracking_code'] = $tracking_code;
			$result['shipped_date'] = $shipped_date;
			$result['status'] = 'Shipped';

			parent::save(new Application_Model_OrderItem($result));

			return true;
		}else {
			return false;
		}
	}
}
