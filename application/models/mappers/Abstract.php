<?php
abstract class Application_Model_Mapper_Abstract
{
	private $_db;
	private $_db_table;
	private $_identity = 'id';
	protected $_entity;

	public function setDb($ini_db_name = null)
	{
		if ($ini_db_name) {
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
			$resource = $bootstrap->getPluginResource('multidb');

			$this->_db = $resource->getDb($ini_db_name);
		}
	}

	public function getDb()
	{
		return $this->_db;
	}

	public function setDbTable($db_table)
	{
		if (is_string($db_table)) {
			if (isset($this->_db)) {
				$tmp_db_table = new $db_table(array('db' => $this->_db));
			}else {
				$tmp_db_table = new $db_table();
			}
		} else {
			$tmp_db_table = $db_table;
		}
		if (!$tmp_db_table instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_db_table = $tmp_db_table;
		return $this;
	}


	/**
	 * @return Zend_Db_Table_Abstract
	 * @throws Exception
	 */
	public function getDbTable()
	{
		if (!isset($this->_db_table)) {
			throw new Exception('Invalid table data gateway provided');
		}
		return $this->_db_table;
	}

	public function setIdentity($value)
	{
		$this->_identity = $value;
		return $this;
	}
	public function getIdentity()
	{
		return $this->_identity;
	}

	public function setEntity($value)
	{
		$this->_entity = $value;
		return $this;
	}
	public function getEntity()
	{
		if (!isset($this->_entity)) {
			throw new Exception('Entity not provided');
		}
		return $this->_entity;
	}

	public function find($id)
	{
		//help prevent SQL Injection attacks
		//TODO - ZF doesn't seem to escape the value?!? There can be compound primary keys so $id could be an array. For now doing a relatively simple check and validation
		$describe = $this->getDbTable()->info();
		if (count($describe['primary']) == 1 && $describe['metadata'][$this->_identity]['DATA_TYPE'] == 'int') {
			if (!is_numeric($id)) {
				return false;
			}
		}

		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return false;
		}
		$row = $result->current();
		return new $this->_entity($row->toArray());
	}

	public function fetchAll($select = null)
	{
		$resultSet = $this->getDbTable()->fetchAll($select);
		$entries   = array();
		foreach ($resultSet as $row) {
			$entries[] = new $this->_entity($row->toArray());
		}
		return $entries;
	}

	public function blank($options)
	{
		return new $this->_entity($options);
	}

	public function save($entity)
	{
		$output = false;
		$entity_type = $this->getEntity();
		if (!$entity instanceof $entity_type) {
			throw new Exception('Invalid entity provided');
		}
		$identity = $this->getIdentity();
		$entity_id = $entity->$identity;
		$c = $this->find($entity_id);
		if (empty($c)) {
			unset($entity->$identity);
			$entity->created = new Zend_Db_Expr("NOW()");
			$entity->modified = null;
			$data = $entity->toArray();

			foreach ($data as $k => $v) {
				if ($v === '') $data[$k] = null;
			}

			$result = $this->getDbTable()->insert($data);
			if ($result >= 1) {
				$output = $result;
			}
		} else {
			$entity->modified = new Zend_Db_Expr("NOW()");
			$data = $entity->toArray();
			unset($data['created']);

			foreach ($data as $k => $v) {
				if ($v === '') $data[$k] = null;
			}

			$where = $this->getDbTable()->getAdapter()->quoteInto($identity.' = ?', $entity_id);
			$result = $this->getDbTable()->update($data, $where);
			if ($result >= 1) {
				$output = $entity_id;
			}
		}
		return $output;
	}

	public function delete($entity)
	{
		$entity_type = $this->getEntity();
		if (!$entity instanceof $entity_type) {
			throw new Exception('Invalid entity provided');
		}
		$identity = $this->getIdentity();
		$entity_id = $entity->$identity;

		if ($entity_id) {
			return $this->getDbTable()->delete(array('id = ?' => $entity_id));
		}else {
			return false;
		}
	}

	public function setSortOrder($id, $sort = null, $array = array())
	{
		$data = array(
			'sort_order' => ($sort)?$sort:99999,
		);
		if ($array) {
			$data = $data + $array;
		}

		if ($id) {
			$data['modified'] = new Zend_Db_Expr("NOW()");
			$this->getDbTable()->update($data, array('id = ?' => $id));
		}

		return $id;
	}

	public function doesExists($record, $id_ignore = null)
	{
		$select = $this->getDbTable()->select();
		foreach ($record as $key => $value) {
			if (!is_null($value)) {
				$select->where($key ." = ?", $value);
			}
		}
		if ($id_ignore) {
			$identity = $this->getIdentity();
			$select->where($identity ." <> ?", $id_ignore);
		}

		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			$entity = $this->getEntity();
			$object = new $entity($result->toArray());
			return $object;
		} else {
			return false;
		}
	}
}
