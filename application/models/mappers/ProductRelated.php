<?php
class Application_Model_Mapper_ProductRelated extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductRelated');
		$this->setEntity('Application_Model_ProductRelated');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('p'=>$this->_table_prefix.'products'),'*')
			->joinLeft(array('pr'=>$this->_table_prefix.'product_related'),'pr.product_related_id = p.id',array('product_related_id','sort_order'))
			->where("p.status = ?","Enabled")
			->where("pr.product_id = ?",$product_id);

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

}
