<?php
class Application_Model_Mapper_ProductVariant extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductVariant');
		$this->setEntity('Application_Model_ProductVariant');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function getVariantsByProductId($product_id, $product_variant_type_id = null)
	{
		$select = $this->getDbTable()->select()
			->where("parent_product_id = ?", $product_id)
			->where("status = ?","Enabled")
			->order('sort_order');

		if ($product_variant_type_id) {
			$select->where('product_variant_type_id = ?', $product_variant_type_id);
		}

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}

	}

	public function findBySku($sku, $product_id = null)
	{
		$select = $this->getDbTable()->select()->where("sku = ?", $sku);

		if ($product_id) {
			$select->where('parent_product_id = ?', $product_id);
		}

		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}

	}
}
