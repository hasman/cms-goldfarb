<?php
class Application_Model_Mapper_ProductImage extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductImage');
		$this->setEntity('Application_Model_ProductImage');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function setPrimaryByProductId($product_id, $image_id)
	{
		if ($product_id && $image_id) {
			$this->getDbTable()->update(array('primary' => 'No'), "product_id = '$product_id'");

			$this->getDbTable()->update(array('primary' => 'Yes'), "product_id = '$product_id' AND id = '$image_id' ");
		}
	}

	public function setSecondaryByProductId($product_id, $image_id)
	{
		if ($product_id && $image_id) {
			$this->getDbTable()->update(array('secondary' => 'No'), "product_id = '$product_id'");

			$this->getDbTable()->update(array('secondary' => 'Yes'), "product_id = '$product_id' AND id = '$image_id' ");
		}
	}

	public function fetchOrderedByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_id = ?', $product_id)
			->order('sort_order');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}

	public function fetchPrimaryByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_id = ?', $product_id)
			->where('`primary` = ?', 'Yes');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result[0];
	}

	public function fetchSecondaryByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_id = ?', $product_id)
			->where('`secondary` = ?', 'Yes');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result[0];
	}

	public function fetchNonPrimaryByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_id = ?', $product_id)
			->where('`primary` = ?', 'No')
			->where('`secondary` = ?', 'No')
			->order('sort_order');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
