<?php
class Application_Model_Mapper_Customer extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_Customer');
		$this->setEntity('Application_Model_Customer');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_cache_dir = $cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->_backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function save($entity)
	{
		$output = parent::save($entity);

		//reset customer cache of profile data
		$cache_id = 'customer_profile_' . $entity->id;
		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);
		$cache->remove($cache_id);

		return $output;
	}

	public function setLastLogin($id)
	{
		$this->getDbTable()->update(array('last_login'=>new Zend_Db_Expr("NOW()")), array('id = ?' => $id));
	}

	public function setResetCode($id)
	{
		$code = md5(uniqid(rand()));

		$this->getDbTable()->update(array("reset_expire"=>new Zend_Db_Expr("DATE_ADD(NOW(),INTERVAL 1 HOUR)"),"reset_code"=>$code), array('id = ?' => $id));

		return $code;
	}

	public function validateCode($code)
	{
		$select = $this->getDbTable()->select()
			->where('reset_code = ?',$code)
			->where('reset_expire > NOW()')
			->where('status = ?', 'Enabled')
			->limit(1)
		;
		$result = $this->getDbTable()->fetchAll($select);

		if ($result->count() > 0) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function clearResetCode($id)
	{
		$this->getDbTable()->update(array("failed_attempts"=>new Zend_Db_Expr("NULL"),"reset_code"=>new Zend_Db_Expr("NULL"),"reset_expire"=>new Zend_Db_Expr("NULL")), array('id = ?' => $id));
	}
}
