<?php
class Application_Model_Mapper_VideoPlatformVideoImageTag extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoImageTag');
		$this->setEntity('Application_Model_VideoPlatformVideoImageTag');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAllActive()
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)
			->from(array('vt'=>$this->_table_prefix.'video_platform_video_image_tag'),'*')
			->joinLeft(array('t'=>$this->_table_prefix.'video_platform_image_tags'),'vt.video_platform_image_tag_id = t.id',array('title'=>'title','code'=>'code'))
			->joinLeft(array('v'=>$this->_table_prefix.'video_platform_videos'),'v.id = vt.video_platform_video_id','')
			->where("t.status = ?","Enabled")
			->where("v.status = ?","Enabled")
			->group('t.id')
		;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()
			->from(array('vt'=>$this->_table_prefix.'video_platform_video_image_tag'),'*')
			->joinLeft(array('t'=>$this->_table_prefix.'video_platform_image_tags'),'vt.video_platform_image_tag_id = t.id',array('title'=>'title','code'=>'code'))
			->where("t.status = ?","Enabled")
			->where("vt.video_id = ?",$video_id);

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchVideosByTagId($tag_id, $sort = 'title ASC')
	{
		if (!$sort) $sort = 'title ASC';

		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('v'=>$this->_table_prefix.'video_platform_videos'),'*')
			->joinLeft(array('vt'=>$this->_table_prefix.'video_platform_video_image_tag'),'vt.video_platform_video_id = v.id','')
			->where("v.status = ?","Enabled")
			->where("vt.video_platform_image_tag_id = ?",$tag_id)
			->order('v.'.$sort)
			;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

}
