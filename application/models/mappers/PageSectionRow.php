<?php
class Application_Model_Mapper_PageSectionRow extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_PageSectionRow');
		$this->setEntity('Application_Model_PageSectionRow');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function getPageSectionRows($section_id)
	{
		$select = $this->getDbTable()->select()
			->where('page_section_id = ?', $section_id)
			->order('sort_order');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}

	public function rowContainsFullwidthModule($row_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('psr'=>$this->_table_prefix.'page_section_rows'),null)
			->from(array('psrc'=>$this->_table_prefix.'page_section_row_columns'),'id')
			->where('psr.id = ?', $row_id)
			->where('psrc.page_section_row_id = psr.id')
			->where('psrc.col_class = ?','0')
		;
		$result = $this->fetchAll($select);

		if (count($result) == 0) return false;
		else return true;
	}

	public function getRowModuleCount($row_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('psr'=>$this->_table_prefix.'page_section_rows'),null)
			->from(array('psrc'=>$this->_table_prefix.'page_section_row_columns'),null)
			->from(array('psrcm'=>$this->_table_prefix.'page_section_row_column_modules'),'id')
			->where('psr.id = ?', $row_id)
			->where('psrc.page_section_row_id = psr.id')
			->where('psrcm.page_section_row_column_id = psrc.id')
		;
		$result = $this->fetchAll($select);

		return count($result);
	}
}
