<?php
class Application_Model_Mapper_VideoPlatformVideoTrailerStream extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoTrailerStream');
		$this->setEntity('Application_Model_VideoPlatformVideoTrailerStream');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByTrailerId($trailer_id)
	{
		$select = $this->getDbTable()->select()
			->where("video_platform_trailer_id = ?", $trailer_id)
			->order('resolution ASC')
		;

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function findValidated($stream_id, $trailer_id)
	{
		$select = $this->getDbTable()->select()
			->where('id = ?', $stream_id)
			->where("video_platform_trailer_id = ?", $trailer_id)
		;

		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			$entity = $this->getEntity();

			return new $entity($result->toArray());
		} else {
			return false;
		}
	}
}
