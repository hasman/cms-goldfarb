<?php
class Application_Model_Mapper_FaqSection extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_FaqSection');
		$this->setEntity('Application_Model_FaqSection');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchOrdered()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?', 'Enabled')
			->order('sort_order ASC');

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function fetchPairs()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?', 'Enabled')
			->order('title ASC');
		$result = $this->getDbTable()->fetchAll($select);

		$array = array();
		foreach ($result as $row) {
			$array[$row->id] = $row->title;
		}

		return $array;
	}
}
