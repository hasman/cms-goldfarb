<?php
class Application_Model_Mapper_VideoPlatformVideoCategory extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoCategory');
		$this->setEntity('Application_Model_VideoPlatformVideoCategory');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchCategoriesByVideoId($video_platform_video_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pc'=>$this->_table_prefix."video_platform_video_categories"),array('video_platform_category_id'))
			->joinLeft(array('c'=>$this->_table_prefix.'categories'),'pc.category_id = c.id', '*')
			->where("pc.video_platform_video_id = ?", $video_platform_video_id)
			->order('pc.sort_order ASC');

		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}

	public function fetchVideosByCategoryId($category_id, $sort_type = 'a-z', $limit = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pc'=>$this->_table_prefix."video_platform_video_categories"),null)
			->joinLeft(array('p'=>$this->_table_prefix.'video_platform_videos'),'pc.video_platform_video_id = p.id', '*')
			->where("pc.video_platform_category_id = ?", $category_id)
			->where('p.status =?', 'Enabled')
		;

		switch ($sort_type) {
			case "a-z":
				$select->order('p.title ASC');
				break;
			case "z-a":
				$select->order('p.title DESC');
				break;
			case "low-high":
				$select->order('p.price ASC');
				break;
			case "high-low":
				$select->order('p.price DESC');
				break;
			case "newest":
				$select->order('p.created ASC');
				break;
			case "oldest":
				$select->order('p.created DESC');
				break;
			case "random":
				$select->order(new Zend_Db_Expr("RAND()"));
				break;
		}

		if ($limit) {
			$select->limit($limit);
		}

		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}
}
