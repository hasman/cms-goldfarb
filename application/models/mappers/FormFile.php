<?php
class Application_Model_Mapper_FormFile extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_FormFile');
		$this->setEntity('Application_Model_FormFile');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByHash($hash)
	{
		$select = $this->getDbTable()->select()->where("hash = ?", $hash);
		$result = $this->getDbTable()->fetchRow($select);
		return $result;
	}
}
