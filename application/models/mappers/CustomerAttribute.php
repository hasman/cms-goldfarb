<?php
class Application_Model_Mapper_CustomerAttribute extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_CustomerAttribute');
		$this->setEntity('Application_Model_CustomerAttribute');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByCode($attribute_code)
	{
		$select = $this->getDbTable()->select()
			->where('code = ?',$attribute_code)
		;
		$row = $this->getDbTable()->fetchRow($select);

		if (!$row) {
			return false;
		}

		return new $this->_entity($row->toArray());
	}
}
