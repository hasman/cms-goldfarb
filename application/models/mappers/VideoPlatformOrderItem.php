<?php
class Application_Model_Mapper_VideoPlatformOrderItem extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformOrderItem');
		$this->setEntity('Application_Model_VideoPlatformOrderItem');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByOrderId($order_id)
	{
		$select = $this->getDbTable()->select()
			->where("video_platform_order_id = ?", $order_id)
		;

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

}
