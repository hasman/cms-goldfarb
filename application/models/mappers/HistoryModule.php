<?php
class Application_Model_Mapper_HistoryModule extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_HistoryModule');
		$this->setEntity('Application_Model_HistoryModule');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function getByModule($module_id)
	{
		$select = $this->getDbTable()->select()
			->where('module_id = ?', $module_id)
			->order('created');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
