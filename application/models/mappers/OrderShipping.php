<?php
class Application_Model_Mapper_OrderShipping extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_OrderShipping');
		$this->setEntity('Application_Model_OrderShipping');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByOrderId($order_id)
	{
		$select = $this->getDbTable()->select()->where("order_id = ?", $order_id);
		$result = $this->getDbTable()->fetchAll($select);

		if ($result->count() > 0) {
			$row = $result->current();
			return new $this->_entity($row->toArray());

			/*
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
			*/
		} else {
			return false;
		}
	}

    public function findByOrderIdAndCode($order_id, $code)
    {
        $select = $this->getDbTable()->select()->where("order_id = ?", $order_id)->where("code = ?", $code);

        $result = $this->getDbTable()->fetchRow($select);
        if ($result) {
            $entity = $this->getEntity();
            return new $entity($result->toArray());
        } else {
            return false;
        }
    }

}
