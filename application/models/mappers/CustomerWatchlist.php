<?php
class Application_Model_Mapper_CustomerWatchlist extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_CustomerWatchlist');
		$this->setEntity('Application_Model_CustomerWatchlist');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_cache_dir = $cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->_backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByCustomerId($id)
	{
		if (!$id) return null;

		$select = $this->getDbTable()->select()->where("customer_id = ?", $id);

		/** @var $result Zend_Db_Table_Rowset */
		$result = $this->getDbTable()->fetchAll($select);
		if ($result->count()) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
			return false;
		}
	}

	public function addVideo($customer_id, $video_id)
	{
		if (!$customer_id) return null;

		$current = $this->findByCustomerId($customer_id);

		if ($current) {
			$list = json_decode($current->list);

			if (!in_array($video_id,$list)) {
				$list[] = $video_id;
			}

			$current->list = json_encode($list);
		}else {
			$current = new Application_Model_CustomerWatchlist();
			$current->customer_id = $customer_id;

			$list = array();
			$list[] = $video_id;

			$current->list = json_encode($list);
		}

		$this->save($current);
	}

	public function removeVideo($customer_id, $video_id)
	{
		if (!$customer_id) return null;

		$current = $this->findByCustomerId($customer_id);

		if ($current) {
			$list = json_decode($current->list);

			foreach ($list as $k => $id) {
				if ($id == $video_id) {
					unset($list[$k]);
				}
			}

			$current->list = json_encode(array_values($list));
		}

		$this->save($current);
	}

}
