<?php
class Application_Model_Mapper_BlogPostTag extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_BlogPostTag');
		$this->setEntity('Application_Model_BlogPostTag');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAllActive()
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('bpt'=>$this->_table_prefix.'blog_post_tag'),'*')
			->joinLeft(array('bt'=>$this->_table_prefix.'blog_tags'),'bpt.blog_tag_id = bt.id',array('title'=>'title','code'=>'code'))
			->joinLeft(array('bp'=>$this->_table_prefix.'blog_posts'),'bp.id = bpt.blog_post_id','')
			->where("bt.status = ?","Enabled")
			->where("bp.status = ?","Enabled")
			->group('bt.id')
		;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchByBlogId($blog_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('bpt'=>$this->_table_prefix.'blog_post_tag'),'*')
			->joinLeft(array('bt'=>$this->_table_prefix.'blog_tags'),'bpt.blog_tag_id = bt.id',array('title'=>'title','code'=>'code'))
			->where("bt.status = ?","Enabled")
			->where("bpt.blog_post_id = ?",$blog_id);

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchBlogsByTagId($tag_id, $limit = null, $ignore_id = null)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('bp'=>$this->_table_prefix.'blog_posts'),'*')
			->joinLeft(array('bpt'=>$this->_table_prefix.'blog_post_tag'),'bpt.blog_post_id = bp.id','')
			->where("bp.status = ?","Enabled")
			->where("bpt.blog_tag_id = ?",$tag_id)
			->order("bp.posted_date DESC");

		if ($ignore_id) {
			$select->where('bp.id <> ?', $ignore_id);
		}

		if ($limit) {
			$select->limit($limit);
		}

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

}
