<?php
class Application_Model_Mapper_ProductVideo extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductVideo');
		$this->setEntity('Application_Model_ProductVideo');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchOrderedByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_id = ?', $product_id)
			->order('sort_order');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
