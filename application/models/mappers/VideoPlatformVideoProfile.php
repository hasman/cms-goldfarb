<?php
class Application_Model_Mapper_VideoPlatformVideoProfile extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoProfile');
		$this->setEntity('Application_Model_VideoPlatformVideoProfile');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_cache_dir = $cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->_backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function save($entity)
	{
		$output = parent::save($entity);

		//reset cache of profile data
		$this->flushCacheByVideoId($entity->video_platform_video_id);

		return $output;
	}

	public function getProfileByVideoId($video_platform_video_id)
	{
		$cache_id = 'video_platform_video_profile_' . $video_platform_video_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);

		if (!($cache->test($cache_id))) {
			$select = $this->getDbTable()->select()->setIntegrityCheck(false)
				->from(array('pp'=>$this->_table_prefix.'video_platform_video_profiles'), array('id','value','video_platform_video_attribute_id','visibility'))
				->joinLeft(array('pa'=>$this->_table_prefix.'video_platform_video_attributes'),'pp.video_platform_video_attribute_id = pa.id', array('code','title','field_type','options'))
				->where('pp.video_platform_video_id = ?',$video_platform_video_id)
				->order('pp.sort_order')
			;
			$results = $this->getDbTable()->fetchAll($select);

			$data = array();
			foreach ($results as $row) {
				$data[$row->code] = array('title'=> $row->title, 'value' => $row->value, 'video_platform_video_profile_id' => $row->id, 'field_type' => $row->field_type, 'options' => $row->options, 'video_platform_video_attribute_id' => $row->video_platform_video_attribute_id, 'visibility' => $row->visibility);
			}

			$cache->save($data, $cache_id, array('video_platform_video_profile', 'video_platform_video_' . $video_platform_video_id));
		} else {
			$data = $cache->load($cache_id);
		}

		return $data;
	}

	public function flushCacheByVideoId($video_id)
	{
		$cache_id = 'video_platform_video_profile_' . $video_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);
		$cache->remove($cache_id);

		return;
	}

}
