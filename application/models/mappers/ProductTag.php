<?php
class Application_Model_Mapper_ProductTag extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductTag');
		$this->setEntity('Application_Model_ProductTag');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAllActive()
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('pt'=>$this->_table_prefix.'product_tag'),'*')
			->joinLeft(array('t'=>$this->_table_prefix.'product_tags'),'pt.product_tag_id = t.id',array('title'=>'title','code'=>'code'))
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'p.id = pt.product_id','')
			->where("t.status = ?","Enabled")
			->where("p.status = ?","Enabled")
			->group('t.id')
		;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('pt'=>$this->_table_prefix.'product_tag'),'*')
			->joinLeft(array('t'=>$this->_table_prefix.'product_tags'),'pt.product_tag_id = t.id',array('title'=>'title','code'=>'code'))
			->where("t.status = ?","Enabled")
			->where("pt.product_id = ?",$product_id);

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchProductsByTagId($tag_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('p'=>$this->_table_prefix.'products'),'*')
			->joinLeft(array('pt'=>$this->_table_prefix.'product_tag'),'pt.product_id = p.id','')
			->where("p.status = ?","Enabled")
			->where("pt.product_tag_id = ?",$tag_id)
			;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

}
