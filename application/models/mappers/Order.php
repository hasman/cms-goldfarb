<?php
class Application_Model_Mapper_Order extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_Order');
		$this->setEntity('Application_Model_Order');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByCustomerId($customer_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)->from(array('o'=>$this->_table_prefix.'orders'),'*')
			->joinLeft(array('oa'=>$this->_table_prefix.'order_addresses'),'o.id = oa.order_id AND oa.type="billing"',array('first_name','last_name','company'))
			->joinLeft(array('opay'=>$this->_table_prefix.'order_payments'),'o.id = opay.order_id',array('type','total_amount'))
			->where('o.customer_id = ?', $customer_id)
			->order('o.created DESC')
		;

		return $select;
	}

	public function setShipStatus($order_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('oi'=>$this->_table_prefix.'order_items'), '')
			->where('oi.order_id = ?', $order_id)
			->columns(array('shipped'=>new Zend_Db_Expr("(SELECT COUNT(*) FROM {$this->_table_prefix}order_items AS ois WHERE ois.order_id = oi.order_id AND ois.status = 'Shipped')")))
			->columns(array('total'=>new Zend_Db_Expr("(SELECT COUNT(*) FROM {$this->_table_prefix}order_items AS oi2 WHERE oi2.order_id = oi.order_id)")))
		;
		$result = $this->getDbTable()->fetchRow($select);

		$status = "Partial";
		if (isset($result) && $result->shipped == $result->total) {
			$status = "Shipped";
		}
		if (isset($result) && $result->shipped == 0) {
			$status = "Order Taken";
		}

		if (isset($result)) {
			$this->getDbTable()->update(array("status"=>$status), "id = '{$order_id}'");
		}
	}
}
