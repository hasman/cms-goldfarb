<?php
class Application_Model_Mapper_ProductCategory extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductCategory');
		$this->setEntity('Application_Model_ProductCategory');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchCategoriesByProductId($product_id, $include_primary = true)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pc'=>$this->_table_prefix."product_categories"),array('primary_category','category_id'))
			->joinLeft(array('c'=>$this->_table_prefix.'categories'),'pc.category_id = c.id', '*')
			->where("pc.product_id = ?", $product_id)
			->order('pc.primary_category DESC');

		if (!$include_primary) {
			$select->where('pc.primary_category = ?', 'No');
		}

		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}

	public function fetchProductPrimaryCategory($product_id)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pc'=>$this->_table_prefix."product_categories"),'*')
			->joinLeft(array('c'=>$this->_table_prefix.'categories'),'pc.category_id = c.id', array('code','name'))
			->where("pc.product_id = ?", $product_id)
			->where('pc.primary_category = ?', 'Yes');
		$result = $this->getDbTable()->fetchRow($select);
		return $result;
	}

	public function fetchProductsByCategoryId($category_id, $sort_type = 'a-z', $limit = null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pc'=>$this->_table_prefix."product_categories"),null)
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'pc.product_id = p.id', '*')
			->where("pc.category_id = ?", $category_id)
			->where('p.status <> ?', 'Disabled')
		;

		switch ($sort_type) {
			case "a-z":
				$select->order('p.title_sort ASC');
				break;
			case "z-a":
				$select->order('p.title_sort DESC');
				break;
			case "low-high":
				$select->order('p.price ASC');
				break;
			case "high-low":
				$select->order('p.price DESC');
				break;
			case "newest":
				$select->order('p.created ASC');
				break;
			case "oldest":
				$select->order('p.created DESC');
				break;
			case "random":
				$select->order(new Zend_Db_Expr("RAND()"));
				break;
		}

		if ($limit) {
			$select->limit($limit);
		}

		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}
}
