<?php
class Application_Model_Mapper_VideoPlatformVideoExtrasStream extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoExtrasStream');
		$this->setEntity('Application_Model_VideoPlatformVideoExtrasStream');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByExtrasId($extra_id)
	{
		$select = $this->getDbTable()->select()
			->where("video_platform_extras_id = ?", $extra_id)
			->order('resolution ASC')
		;

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

	public function findValidated($stream_id, $extra_id)
	{
		$select = $this->getDbTable()->select()
			->where('id = ?', $stream_id)
			->where("video_platform_extras_id = ?", $extra_id)
		;

		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			$entity = $this->getEntity();

			return new $entity($result->toArray());
		} else {
			return false;
		}
	}
}
