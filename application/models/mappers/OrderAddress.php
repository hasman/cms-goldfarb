<?php
class Application_Model_Mapper_OrderAddress extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_OrderAddress');
		$this->setEntity('Application_Model_OrderAddress');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByOrderId($order_id, $type)
	{
		$select = $this->getDbTable()->select()->where("order_id = ?", $order_id)->where("type = ?",$type);
		$result = $this->getDbTable()->fetchAll($select);

		if ($result->count() > 0) {
			$row = $result->current();
			return new $this->_entity($row->toArray());
		} else {
            return false;
		}
	}
}
