<?php
class Application_Model_Mapper_VideoPlatformVideo extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideo');
		$this->setEntity('Application_Model_VideoPlatformVideo');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByCode($code)
	{
		$select = $this->getDbTable()->select()
			->where("code = ?", $code)
			->where("status = ?", "Enabled")
		;

		$result = $this->getDbTable()->fetchRow($select);

		return $result;
	}

	public function findByVideoHash($hash)
	{
		$select = $this->getDbTable()->select()
			->where("video_hash = ?", $hash)
			->where("status = ?", "Enabled")
		;

		$result = $this->getDbTable()->fetchRow($select);

		return $result;
	}

	public function fetchActive($sort_type = null, $limit = null)
	{
		$select = $this->getDbTable()->select()
			->where('status <> ?', 'Disabled')
		;

		switch ($sort_type) {
			case "a-z":
				$select->order('title ASC');
				break;
			case "z-a":
				$select->order('title DESC');
				break;
			case "newest":
				$select->order('created ASC');
				break;
			case "oldest":
				$select->order('created DESC');
				break;
			case "random":
				$select->order(new Zend_Db_Expr("RAND()"));
				break;
			default:
				$select->order('title', 'title');
				break;
		}

		if ($limit) {
			$select->limit($limit);
		}

		return $this->fetchAll($select);
	}
}
