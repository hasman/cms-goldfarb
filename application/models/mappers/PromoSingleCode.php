<?php
class Application_Model_Mapper_PromoSingleCode extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_PromoSingleCode');
		$this->setEntity('Application_Model_PromoSingleCode');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function markUsed($code) {
		$data['used_timestamp'] = new Zend_Db_Expr("NOW()");

		$where = $this->getDbTable()->getAdapter()->quoteInto('code = ?', $code);
		$this->getDbTable()->update($data, $where);
	}

}
