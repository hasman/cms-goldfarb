<?php
class Application_Model_Mapper_Font extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_Font');
		$this->setEntity('Application_Model_Font');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function getActiveFonts()
	{
		$fonts = Zend_Registry::get('cms_fonts');
		if (!is_array($fonts)) {
			$fonts = $fonts->toArray();
		}

		$db_fonts = $this->fetchAll();
		foreach ($db_fonts as $row) {
			if ($row->status == "Disabled") {
				unset($fonts[$row->code]);
			}else {
				if (!array_key_exists($row->code,$fonts)) {
					$fonts[$row->code] = array(
						'title' => $row->title,
						'type' => $row->type,
						'url' => $row->url
					);
				}
			}
		}

		return $fonts;
	}

	public function getWeights($font_family_name)
	{
		$fonts = $this->getActiveFonts();

		$url = $fonts[$font_family_name]['url'];

		$tmp = explode(":", $url);
		$weight_string = end($tmp);

		if (!$weight_string) {
			return null;
		}

		$weights = explode(",", $weight_string);

		if (!$weights || count($weights) == 0) {
			return null;
		}

		return $weights;
	}
}
