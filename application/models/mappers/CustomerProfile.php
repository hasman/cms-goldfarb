<?php
class Application_Model_Mapper_CustomerProfile extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_CustomerProfile');
		$this->setEntity('Application_Model_CustomerProfile');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_cache_dir = $cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->_backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function save($entity)
	{
		$output = parent::save($entity);

		//reset customer cache of profile data
		$cache_id = 'customer_profile_' . $entity->customer_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);
		$cache->remove($cache_id);

		return $output;
	}

	public function getProfileByCustomerId($customer_id)
	{
		$cache_id = 'customer_profile_' . $customer_id;
		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);

		if (!($cache->test($cache_id))) {
			$select = $this->getDbTable()->select()->setIntegrityCheck(false)
				->from(array('cp'=>$this->_table_prefix.'customer_profiles'), array('id','value'))
				->joinLeft(array('ca'=>$this->_table_prefix.'customer_attributes'),'cp.customer_attribute_id = ca.id', array('code','title','field_type','options'))
				->where('cp.customer_id = ?',$customer_id)
			;
			$results = $this->getDbTable()->fetchAll($select);

			$data = array();
			foreach ($results as $row) {
				$data[$row->code] = array('title'=> $row->title, 'value' => $row->value, 'customer_profile_id' => $row->id, 'field_type' => $row->field_type, 'options' => $row->options);
			}

			$cache->save($data, $cache_id, array('customer_profile', 'customer_' . $customer_id));
		} else {
			$data = $cache->load($cache_id);
		}

		return $data;
	}

	public function getCustomerAttribute($customer_id, $attribute_code)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('cp'=>$this->_table_prefix.'customer_profiles'), array('*'))
			->joinLeft(array('ca'=>$this->_table_prefix.'customer_attributes'),'cp.customer_attribute_id = ca.id', null)
			->where('cp.customer_id = ?',$customer_id)
			->where('ca.code = ?',$attribute_code)
		;
		$row = $this->getDbTable()->fetchRow($select);

		if (!$row) {
			return false;
		}

		return new $this->_entity($row->toArray());
	}

	public function updateCustomerAttribute($customer_id, $attribute_code, $value)
	{
		$entity = $this->getCustomerAttribute($customer_id, $attribute_code);

		if ($entity) {
			$entity->value = $value;
		}else {
			$mapper = new Application_Model_Mapper_CustomerAttribute();
			$attribute = $mapper->findByCode($attribute_code);

			$entity = new Application_Model_CustomerProfile(array(
				'customer_id' => $customer_id,
				'customer_attribute_id' => $attribute->id,
				'value' => $value
			));
		}

		return $this->save($entity);
	}
}
