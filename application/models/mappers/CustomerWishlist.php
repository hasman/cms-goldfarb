<?php
class Application_Model_Mapper_CustomerWishlist extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_CustomerWishlist');
		$this->setEntity('Application_Model_CustomerWishlist');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_cache_dir = $cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array( 'automatic_serialization' => true, 'lifetime' => 0 );
		$this->_backendOptions  = array( 'cache_dir' => $cache_dir );

		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function addToWishlist($customer_id, $product_id, $variant_id = null, $price = null, $additional_data = array())
	{
		if (is_null($price)) {
			if ($variant_id) {
				$mapper = new Application_Model_Mapper_ProductVariant();
				$item = $mapper->find($variant_id);
			}else {
				$mapper = new Application_Model_Mapper_Product();
				$item = $mapper->find($variant_id);
			}

			$price = $item->price;
		}

		$data = array(
			'customer_id' => $customer_id,
			'product_id' => $product_id,
			'variant_id' => $variant_id,
			'wishlist_price' => $price,
			'additional_data' => json_encode($additional_data)
		);

		$select = $this->getDbTable()->select()
			->where("customer_id = ?", $customer_id)
			->where("product_id = ?", $product_id)
		;
		if ($variant_id) {
			$select->where("variant_id = ?", $variant_id);
		}else {
			$select->where("variant_id IS NULL");
		}
		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			$data['id'] = $result->id;
			if (!$additional_data) {
				$data['additional_data'] = $result->additional_data;
			}
		}

		return parent::save(new Application_Model_CustomerWishlist($data));
	}

	public function removeFromWishlist($customer_id, $product_id, $variant_id = null)
	{
		$select = $this->getDbTable()->select()
			->where("customer_id = ?", $customer_id)
			->where("product_id = ?", $product_id)
		;
		if ($variant_id) {
			$select->where("variant_id = ?", $variant_id);
		}else {
			$select->where("variant_id IS NULL");
		}
		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			return parent::delete(new Application_Model_CustomerWishlist($result->toArray()));
		}

		return false;
	}

	public function inWishlist($customer_id, $product_id, $variant_id = null)
	{
		$select = $this->getDbTable()->select()
			->where("customer_id = ?", $customer_id)
			->where("product_id = ?", $product_id)
		;
		if ($variant_id) {
			$select->where("variant_id = ?", $variant_id);
		}else {
			$select->where("variant_id IS NULL");
		}
		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			return true;
		}

		return false;
	}

	public function fetchByCustomerId($customer_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('cw'=>$this->_table_prefix.'customer_wishlists'),'*')
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'cw.product_id = p.id',array('title'=>'title','code','code','price'=>'price'))
			->joinLeft(array('pv'=>$this->_table_prefix.'product_variants'),'cw.variant_id = pv.id',array('variant_title'=>'title','variant_code'=>'code','variant_price'=>'price'))
			->where("cw.customer_id = ?",$customer_id)
			->order('created ASC');

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchOnSaleByCustomerId($customer_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('cw'=>$this->_table_prefix.'customer_wishlists'),'*')
			->joinLeft(array('p'=>$this->_table_prefix.'products'),'cw.product_id = p.id',array('title'=>'title','code','code','price'=>'price'))
			->joinLeft(array('pv'=>$this->_table_prefix.'product_variants'),'cw.variant_id = pv.id',array('variant_title'=>'title','variant_code'=>'code','variant_price'=>'price'))
			->where("cw.customer_id = ?",$customer_id)
			->where(new Zend_Db_Expr("cw.price - IF (cw.variant_id NOT NULL AND cw.variant_id > 0, pv.price, p.price) > 0"))
			->order('created ASC');

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}
}
