<?php
class Application_Model_Mapper_VideoPlatformVideoMonetization extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoMonetization');
		$this->setEntity('Application_Model_VideoPlatformVideoMonetization');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()
			->where("video_platform_video_id = ?", $video_id)
		;

		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}
}
