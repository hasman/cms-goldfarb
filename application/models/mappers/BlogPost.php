<?php
class Application_Model_Mapper_BlogPost extends Application_Model_Mapper_Abstract
{
	protected $events;

	public function events(Zend_EventManager_EventCollection $events = null)
	{
		if (null !== $events) {
			$this->events = $events;
		} elseif (null === $this->events) {
			$this->events = new Zend_EventManager_EventManager(__CLASS__);
		}
		return $this->events;
	}

	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_BlogPost');
		$this->setEntity('Application_Model_BlogPost');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByCode($code)
	{
		$select = $this->getDbTable()->select()->where("code = ?", $code);
		$result = $this->getDbTable()->fetchRow($select);
		return $result;
	}

	public function fetchAllActiveArticles()
	{
		$select = $this->getDbTable()->select()->where("status= ?", 'enabled')->order('posted_date DESC')
			;
		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchLatestArticles($limit = 10, $ignore_id = null)
	{
		$select = $this->getDbTable()->select();
		$select
			->from($this->_table_prefix.'blog_posts')
			->where("status= ?", 'enabled')
			->order('posted_date DESC')
			->columns(array("article_id"=>"id"))
			->limit($limit)
			;

		if ($ignore_id) {
			$select->where("id <> ?", $ignore_id);
		}

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchArticles($limit = 10, $tag = null)
	{
		$select = $this->getDbTable()->select()
			->from($this->_table_prefix.'blog_posts')
			->where("blog_posts.status= ?", 'enabled')
			->order('blog_posts.posted_date DESC')
			->limit($limit)
		;

		if ($tag) {
			$select->joinLeft(array('bpt'=>"{$this->_table_prefix}blog_post_tag"),'bpt.blog_post_id = blog_posts.id','')
				->joinLeft(array('bt'=>$this->_table_prefix.'blog_tags'),'bt.id = bpt.blog_tag_id','')
				->distinct()
			;

			$blog_tag_mapper = new Application_Model_Mapper_BlogTag();
			if (stripos($tag, ",") !== false) {
				$tags = explode(",", $tag);

				$tag_list = '';
				foreach ($tags as $tag) {
					$tag = trim($tag);
					if (!$blog_tag_mapper->findByCode($tag)) {
						$result = $blog_tag_mapper->findByTitle($tag);
						$tag_list .= "'{$result['code']}',";
					}else {
						$tag_list .= "'{$tag}',";
					}
				}

				$select->where("bt.code IN (".substr($tag_list,0,-1).")");
			}else {
				if (!$blog_tag_mapper->findByCode($tag)) {
					$result = $blog_tag_mapper->findByTitle($tag);
					$tag = $result['code'];
				}

				if ($tag) {
					$select->where('bt.code = ?', $tag);
				}
			}
		}

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function save($entity)
	{
		$params = compact('entity');
		$this->events()->trigger(__FUNCTION__.'.pre', $this, $params);

		$output = parent::save($entity);

		$params = compact('entity', 'output');
		$this->events()->trigger(__FUNCTION__.'.post', $this, $params);

		return $output;
	}

	public function findNextPrev($current_id, $dir = 'next') {
		$current = $this->find($current_id);

		$select = $this->getDbTable()->select();
		$select
			->from($this->_table_prefix.'blog_posts')
			->where("status= ?", 'enabled')
			->where('id <> ?', $current_id)
		;

		if ($dir == 'next') {
			$select->where('posted_date >= ?', $current->posted_date)
				->order('posted_date ASC');
		}elseif ($dir == 'prev') {
			$select->where('posted_date <= ?', $current->posted_date)
				->order('posted_date DESC');
		}

		$select->limit(1);

		$row = $this->getDbTable()->fetchRow($select);

		if (!$row) return null;

		return $row;
	}

	public function fetchRelatedArticlesBytags($article_id, $limit = null)
	{
		//get tags for current article
		$mapper = new Application_Model_Mapper_BlogPostTag();
		$tags = $mapper->fetchByBlogId($article_id);

		$tag_list = '';
		foreach ($tags as $tag) {
			$tag_list .= $tag['blog_tag_id'].",";
		}
		if ($tag_list) {
			$tag_list = substr($tag_list,0, -1);

			$select = $this->getDbTable()->select()
				->from(array('bp'=>$this->_table_prefix.'blog_posts'),'*')
				->where("bp.status= ?", 'enabled')
				->order('bp.posted_date DESC')
				->joinLeft(array('bpt'=>"{$this->_table_prefix}blog_post_tag"),'bpt.blog_post_id = bp.id','')
				//->joinLeft(array('bt'=>$this->_table_prefix.'blog_tags'),'bt.id = bpt.blog_tag_id','')
				->where('bp.id <> ?', $article_id)
				->where("bpt.blog_tag_id IN ($tag_list)")
				->distinct()
			;

			if ($limit) {
				$select->limit($limit);
			}

			$result = $this->getDbTable()->fetchAll($select);
			return $result;
		}

		return null;
	}
}
