<?php
class Application_Model_Mapper_VideoPlatformVideoTrack extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoTrack');
		$this->setEntity('Application_Model_VideoPlatformVideoTrack');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByVideoId($video_id, $kind = null)
	{
		$select = $this->getDbTable()->select()
			->where("video_platform_video_id = ?", $video_id)
		;

		if ($kind) {
			$select->where('kind = ?', $kind);
		}

		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			$collection = array();
			$entity = $this->getEntity();
			foreach ($result as $row) {
				$collection[] = new $entity($row->toArray());
			}
			return $collection;
		} else {
			return false;
		}
	}

}
