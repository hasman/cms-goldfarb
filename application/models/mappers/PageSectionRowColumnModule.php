<?php
class Application_Model_Mapper_PageSectionRowColumnModule extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_PageSectionRowColumnModule');
		$this->setEntity('Application_Model_PageSectionRowColumnModule');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function save($entity, $save_history = true)
	{
		if ($entity->id && $save_history) {
			$mapper = new Application_Model_Mapper_HistoryModule();
			$history = parent::find($entity->id)->toArray();

			//remove fields we don't want/need to save history
			unset($history['id']);
			unset($history['created']);
			unset($history['modified']);
			//issue with padding field for some reason
			if (json_encode($history['padding']) == '{}') {
				$history['padding'] = '';
			}

			$mapper->save(new Application_Model_HistoryModule(
				array(
					'module_id' => $entity->id,
					'table_field_json' => json_encode($history),
					'json_content' => $history['json_content']
				)
			));
		}

		return parent::save($entity);
	}

	public function getPageSectionRowColumnModule($column_id)
	{
		$select = $this->getDbTable()->select()
			->where('page_section_row_column_id = ?', $column_id);
		$result = $this->getDbTable()->fetchRow($select);

		if (count($result) == 0) return null;
		else return (object)$result;
	}

	public function getPageSectionRowColumnModules($column_id)
	{
		$select = $this->getDbTable()->select()
			->where('page_section_row_column_id = ?', $column_id)
			->order('sort_order');
		$result = $this->getDbTable()->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
