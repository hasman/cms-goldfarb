<?php
class Application_Model_Mapper_ProductReviewFile extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductReviewFile');
		$this->setEntity('Application_Model_ProductReviewFile');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function findByHash($hash)
	{
		$select = $this->getDbTable()->select()->where("hash = ?", $hash);
		$result = $this->getDbTable()->fetchRow($select);
		return $result;
	}

	public function fetchByReviewId($id)
	{
		$select = $this->getDbTable()->select()->where("product_review_id = ?", $id);
		$results = $this->getDbTable()->fetchAll($select);

		return $results;
	}

	public function findByProductId($product_id, $id=null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('prf'=>$this->_table_prefix."product_review_files"),'*')
			->joinLeft(array('pr'=>$this->_table_prefix.'product_reviews'),'prf.product_review_id = pr.id', '')
			->where("pr.product_id = ?", $product_id);

		if($id) {
			$select->where("pr.id = ?", $id);
		}

		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}

	public function findByProductVariantId($product_variant_id, $id=null)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('prf'=>$this->_table_prefix."product_review_files"),'*')
			->joinLeft(array('pr'=>$this->_table_prefix.'product_reviews'),'prf.product_review_id = pr.id', '')
			->where("pr.product_variant_id = ?", $product_variant_id);

		if($id) {
			$select->where("pr.id = ?", $id);
		}

		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}
}
