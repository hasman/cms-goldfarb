<?php
class Application_Model_Mapper_ProductImageTag extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductImageTag');
		$this->setEntity('Application_Model_ProductImageTag');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAllActive()
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('pit'=>$this->_table_prefix.'product_image_tag'),'*')
			->joinLeft(array('it'=>$this->_table_prefix.'image_tags'),'pit.image_tag_id = it.id',array('title'=>'title','code'=>'code'))
			->joinLeft(array('pi'=>$this->_table_prefix.'product_image'),'pi.id = pit.product_image_id','')
			->where("pit.status = ?","Enabled")
			->group('pi.id')
		;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchByProductImageId($image_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('pit'=>$this->_table_prefix.'product_image_tag'),'*')
			->joinLeft(array('it'=>$this->_table_prefix.'image_tags'),'pit.image_tag_id = it.id',array('title'=>'title','code'=>'code'))
			->where("it.status = ?","Enabled")
			->where("pit.product_image_id = ?",$image_id);

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchProductImagesByTagId($tag_id, $product_id = null)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('pi'=>$this->_table_prefix.'product_images'),'*')
			->joinLeft(array('pit'=>$this->_table_prefix.'product_image_tag'),'pit.product_image_id = pi.id','')
			->where("pit.image_tag_id = ?",$tag_id)
			->order("pi.sort_order ASC");

		if ($product_id) {
			$select->where("pi.product_id = ?", $product_id);
		}

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}
}
