<?php
class Application_Model_Mapper_ProductVariantType extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductVariantType');
		$this->setEntity('Application_Model_ProductVariantType');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchIdPairs()
	{
		$select = $this->getDbTable()->select()
		;
		$results = $this->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->id] = $row->title;
		}

		return $pairs;
	}

	public function fetchCodePairs()
	{
		$select = $this->getDbTable()->select()
		;
		$results = $this->fetchAll($select);

		$pairs = array();
		foreach ($results as $row) {
			$pairs[$row->code] = $row->title;
		}

		return $pairs;
	}

	public function fetchByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('pvt'=>$this->_table_prefix.'product_variant_types'),'*')
			->joinLeft(array('pv'=>$this->_table_prefix.'product_variants'),'pv.product_variant_type_id = pvt.id','')
			->where("pv.status = ?","Enabled")
			->where("pv.parent_product_id = ?",$product_id)
			->group('pvt.id');

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}
}
