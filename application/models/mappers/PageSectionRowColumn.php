<?php
class Application_Model_Mapper_PageSectionRowColumn extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_PageSectionRowColumn');
		$this->setEntity('Application_Model_PageSectionRowColumn');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function getPageSectionRowColumns($row_id)
	{
		$select = $this->getDbTable()->select()
			->where('page_section_row_id = ?', $row_id)
			->order('sort_order');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
