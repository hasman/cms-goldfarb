<?php
class Application_Model_Mapper_Page extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_Page');
		$this->setEntity('Application_Model_Page');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchPages()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('block_type = ?', 'page')
			->order('title')
		;
		return $this->fetchAll($select);
	}

	public function fetchHeaders()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('block_type = ?', 'header')
			->order('title')
		;
		return $this->fetchAll($select);
	}

	public function fetchFooters()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('block_type = ?', 'footer')
			->order('title')
		;
		return $this->fetchAll($select);
	}

	public function fetchBlocks()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('block_type = ?', 'block')
			->order('title')
		;
		return $this->fetchAll($select);
	}

	public function fetchModals()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('block_type = ?', 'modal')
			->order('title')
		;
		return $this->fetchAll($select);
	}

	public function fetchNavPanels()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->where('block_type = ?', 'navigation')
			->order('title')
		;
		return $this->fetchAll($select);
	}
}
