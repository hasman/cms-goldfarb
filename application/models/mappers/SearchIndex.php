<?php
class Application_Model_Mapper_SearchIndex extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_SearchIndex');
		$this->setEntity('Application_Model_SearchIndex');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function doesUrlExists($url)
	{
		$select = $this->getDbTable()->select();
		$select->where("url = ?", $url);
		$result = $this->getDbTable()->fetchRow($select);

		if ($result) {
			$entity = $this->getEntity();
			$object = new $entity($result->toArray());
			return $object;
		}
		return false;
	}

	public function deleteByUrl($url)
	{
		$output = false;

		if ($entity = $this->doesUrlExists($url)) {
			$output = $this->delete($entity);
		}

		return $output;
	}

	public function query($q)
	{
		$ft_match_query = new Zend_Db_Expr(
			$this->getDbTable()->getAdapter()->quoteInto("MATCH(title,keywords,description,content) AGAINST(? WITH QUERY EXPANSION)", $q)
		);
		$ft_match_sort = new Zend_Db_Expr(
			$this->getDbTable()->getAdapter()->quoteInto("MATCH(title,keywords,description,content) AGAINST(? WITH QUERY EXPANSION) DESC", $q)
		);
		$select = $this->getDbTable()->select();
		$select->from('search_index', array('*', '_score' => $ft_match_query));
		$select->where($ft_match_query);
		$select->order($ft_match_sort);
		$result = $this->getDbTable()->fetchAll($select);

		if ($result) {
			return $result;
		}
		return false;
	}
}
