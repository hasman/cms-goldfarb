<?php
class Application_Model_Mapper_VideoCategory extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoCategory');
		$this->setEntity('Application_Model_VideoCategory');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchVideosByCategoryId($category_id, $sort = 'sort_order ASC')
	{
		if (!$sort) $sort = 'sort_order ASC';

		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('v'=>$this->_table_prefix."videos"),'*')
			->where("v.video_category_id = ?", $category_id)
			->where('v.status =?', 'Enabled')
			->order('v.'.$sort);
		$results = $this->getDbTable()->fetchAll($select);
		return $results;
	}

	public function fetchCategories()
	{
		$select = $this->getDbTable()->select()
			->where('status = ?','Enabled')
			->order('name')
		;
		return $this->fetchAll($select);
	}
}
