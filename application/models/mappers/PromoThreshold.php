<?php
class Application_Model_Mapper_PromoThreshold extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_PromoThreshold');
		$this->setEntity('Application_Model_PromoThreshold');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchByPromoId($id){
		$select = $this->getDbTable()->select()->where("promo_id=?",$id);

		return  $this->getDbTable()->fetchAll($select);
	}

}
