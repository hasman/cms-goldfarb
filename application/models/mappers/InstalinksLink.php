<?php
class Application_Model_Mapper_InstalinksLink extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_InstalinksLink');
		$this->setEntity('Application_Model_InstalinksLink');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchActive()
	{
		$select = $this->getDbTable()->select()->where('status = ?', 'Enabled')->order('sort_order');
		return $this->fetchAll($select);
	}
}
