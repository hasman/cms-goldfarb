<?php
class Application_Model_Mapper_VideoPlatformVideoAttribute extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoAttribute');
		$this->setEntity('Application_Model_VideoPlatformVideoAttribute');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}


}
