<?php
class Application_Model_Mapper_VideoPlatformVideoTag extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_VideoPlatformVideoTag');
		$this->setEntity('Application_Model_VideoPlatformVideoTag');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAllActive()
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('vt'=>$this->_table_prefix.'video_platform_video_tags'),'*')
			->joinLeft(array('t'=>$this->_table_prefix.'video_platform_tags'),'vt.video_platform_tag_id = t.id',array('title'=>'title','code'=>'code'))
			->joinLeft(array('v'=>$this->_table_prefix.'video_platform_videos'),'v.id = vt.video_platform_video_id','')
			->where("t.status = ?","Enabled")
			->where("v.status = ?","Enabled")
			->group('t.id')
		;

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchByVideoId($video_id)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('vt'=>$this->_table_prefix.'video_platform_video_tags'),'*')
			->joinLeft(array('t'=>$this->_table_prefix.'video_platform_tags'),'vt.video_platform_tag_id = t.id',array('title'=>'title','code'=>'code'))
			->where("t.status = ?","Enabled")
			->where("vt.video_id = ?",$video_id);

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}

	public function fetchVideosByTagId($tag_id, $sort_type = 'a-z', $limit = null)
	{
		$select = $this->getDbTable()->select()
			->setIntegrityCheck(false)->from(array('v'=>$this->_table_prefix.'video_platform_videos'),'*')
			->joinLeft(array('vt'=>$this->_table_prefix.'video_platform_video_tags'),'vt.video_platform_video_id = v.id','')
			->where("v.status = ?","Enabled")
			->where("vt.video_platform_tag_id = ?",$tag_id)
		;

		switch ($sort_type) {
			case "a-z":
				$select->order('v.title ASC');
				break;
			case "z-a":
				$select->order('v.title DESC');
				break;
			case "low-high":
				$select->order('v.price ASC');
				break;
			case "high-low":
				$select->order('v.price DESC');
				break;
			case "newest":
				$select->order('v.created ASC');
				break;
			case "oldest":
				$select->order('v.created DESC');
				break;
			case "random":
				$select->order(new Zend_Db_Expr("RAND()"));
				break;
		}

		if ($limit) {
			$select->limit($limit);
		}

		$result = $this->getDbTable()->fetchAll($select);
		return $result;
	}
}
