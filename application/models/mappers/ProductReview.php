<?php
class Application_Model_Mapper_ProductReview extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductReview');
		$this->setEntity('Application_Model_ProductReview');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function fetchAggregateByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->from("{$this->_table_prefix}product_reviews")
			->where("product_id = ?", $product_id)
			->columns(array('average' => new Zend_Db_Expr("ROUND(AVG(rating),1)"), 'total' => new Zend_Db_Expr("COUNT(*)")))
			->where('status = ?', 'Approved');

		$result = $this->getDbTable()->fetchRow($select);

		return $result;
	}

	public function fetchReviewsByProductId($product_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_id = ?', $product_id)
			->where('status = ?', 'Approved')
			->order('created DESC');
		$result = $this->fetchAll($select);

		return $result;
	}

	public function fetchAggregateByProductVariantId($product_variant_id)
	{
		$select = $this->getDbTable()->select()
			->from("{$this->_table_prefix}product_reviews")
			->where("product_variant_id = ?", $product_variant_id)
			->columns(array('average' => new Zend_Db_Expr("ROUND(AVG(rating),1)"), 'total' => new Zend_Db_Expr("COUNT(*)")))
			->where('status = ?', 'Approved');

		$result = $this->getDbTable()->fetchRow($select);

		return $result;
	}

	public function fetchReviewsByProductVariantId($product_variant_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_variant_id = ?', $product_variant_id)
			->where('status = ?', 'Approved')
			->order('created DESC');
		$result = $this->fetchAll($select);

		return $result;
	}

	public function fetchReviewsFeed($category_id = null, $limit, $sort)
	{
		$select = $this->getDbTable()->select()->setIntegrityCheck(false)
			->from(array('pr'=>"{$this->_table_prefix}product_reviews"))
			->where('pr.status = ?', 'Approved')
			->limit($limit)
			;

		if ($category_id) {
			$select
				->joinLeft(array('pv'=>"{$this->_table_prefix}product_variants"),'pr.product_variant_id = pv.id', '')
				->joinLeft(array('pc'=>"{$this->_table_prefix}product_categories"),'(IF (pr.product_variant_id, pc.product_id = pv.parent_product_id, pc.product_id = pr.product_id) )', '')
				->joinLeft(array('c'=>"{$this->_table_prefix}categories"),'pc.category_id = c.id')
				->where("(c.id = '{$category_id}' OR c.parent_id = '{$category_id}' )")
			;
		}

		if ($sort == 'rating') {
			$select->order("pr.rating DESC");
		}elseif ($sort == 'newest') {
			$select->order("pr.created DESC");
		}

		$result = $this->fetchAll($select);

		return $result;
	}


}
