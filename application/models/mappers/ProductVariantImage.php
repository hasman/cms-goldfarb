<?php
class Application_Model_Mapper_ProductVariantImage extends Application_Model_Mapper_Abstract
{
	public function __construct()
	{
		$this->setDbTable('Application_Model_DbTable_ProductVariantImage');
		$this->setEntity('Application_Model_ProductVariantImage');

		$options = Zend_Registry::get('configuration')->toArray();
		$this->_table_prefix = isset($options['resources']['multidb']['db']['table_prefix'])?$options['resources']['multidb']['db']['table_prefix']:'';
	}

	public function setPrimaryByProductVariantId($product_variant_id, $image_id)
	{
		if ($product_variant_id && $image_id) {
			$this->getDbTable()->update(array('primary' => 'No'), "product_variant_id = '$product_variant_id'");

			$this->getDbTable()->update(array('primary' => 'Yes'), "product_variant_id = '$product_variant_id' AND id = '$image_id' ");
		}
	}

	public function setSecondaryByProductVariantId($product_variant_id, $image_id)
	{
		if ($product_variant_id && $image_id) {
			$this->getDbTable()->update(array('secondary' => 'No'), "product_variant_id = '$product_variant_id'");

			$this->getDbTable()->update(array('secondary' => 'Yes'), "product_variant_id = '$product_variant_id' AND id = '$image_id' ");
		}
	}

	public function fetchOrderedByProductVariantId($product_variant_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_variant_id = ?', $product_variant_id)
			->order('sort_order');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}

	public function fetchPrimaryByProductVariantId($product_variant_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_variant_id = ?', $product_variant_id)
			->where('`primary` = ?', 'Yes');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result[0];
	}

	public function fetchSecondaryByProductVariantId($product_variant_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_variant_id = ?', $product_variant_id)
			->where('`secondary` = ?', 'Yes');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result[0];
	}

	public function fetchNonPrimaryByProductVariantId($product_variant_id)
	{
		$select = $this->getDbTable()->select()
			->where('product_variant_id = ?', $product_variant_id)
			->where('`primary` = ?', 'No')
			->where('`secondary` = ?', 'No')
			->order('sort_order');
		$result = $this->fetchAll($select);

		if (count($result) == 0) return null;
		else return $result;
	}
}
