<?php
class Application_Model_Zype
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Registry::get('configuration')->toArray();
		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->_cache_dir = $options['dir']['cache'];
		$this->_frontendOptions = array('automatic_serialization' => true, 'lifetime' => $settings_mapper->getValueByCode('zype_cache_lifetime', 300));
		$this->_backendOptions = array('cache_dir' => $this->_cache_dir);

		$this->cfg['api_url'] = "https://api.zype.com/";
		$this->cfg['player_url'] = "https://player.zype.com/";
		$this->cfg['api_keys']['admin'] = $settings_mapper->getValueByCode('zype_api_key_admin');
		$this->cfg['api_keys']['embed'] = $settings_mapper->getValueByCode('zype_api_key_embed');
		$this->cfg['api_keys']['player'] = $settings_mapper->getValueByCode('zype_api_key_player');
		$this->cfg['api_keys']['read'] = $settings_mapper->getValueByCode('zype_api_key_read_only');
		$this->cfg['api_keys']['app_key'] = $settings_mapper->getValueByCode('zype_app_key');
		$this->cfg['client_id'] = $settings_mapper->getValueByCode('zype_client_id');
		$this->cfg['secret_key'] = $settings_mapper->getValueByCode('zype_secret_key');
	}

	private function execute($endpoint = '', $method = 'GET', $key_type = 'read', $data = array(), $query_params = array(), $cache_results = true)
	{
		if ($method != 'GET') {
			$cache_results = false;
		}

		$qs_array = [];
		if ($key_type) $qs_array['api_key'] = $this->cfg['api_keys'][$key_type];
		if ($query_params) {
			foreach ($query_params as $k => $param) {
				$qs_array[$k] = $param;
			}
		}

		$queryString = http_build_query($qs_array);

		/*
		$queryString = '';
		if ($key_type) $queryString .= "&api_key=" .$this->cfg['api_keys'][$key_type];
		foreach ($query_params as $k => $param) {
			if (is_array($param)) {
				foreach ($param as $v) {
					$queryString .= "&".urlencode($k)."=".urlencode($v);
				}
			}else {
				$queryString .= "&".urlencode($k)."=".urlencode($param);
			}
		}
		*/

		$url = $this->cfg['api_url'] . $endpoint . "?$queryString";

		$data = json_encode($data);

		$cache_id = 'zype_'.md5($url.'-'.$data.'-'.$queryString);
		$cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);

		if(!$cache_results || !($cache->test($cache_id))) {
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data),
				'Expect: '
			));

			$result = curl_exec($ch);
			curl_close($ch);

			$cache->save($result, $cache_id, array('zype'));
		}else{
			$result = $cache->load($cache_id);
		}

		$json = json_decode($result);

		return $json;
	}

	public function refreshAccount()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('customer'));
		if( !$auth->hasIdentity() ){
			//return null;
		}else {
			$customer = $auth->getIdentity();
		}

		if (!isset($customer->zype->refresh_token) || !$customer->zype->refresh_token) {
			//manually destroy session if handler is memcache due to PHP7 memcache bug @see https://github.com/php-memcached-dev/php-memcached/issues/199
			if(session_status() !== PHP_SESSION_DISABLED && ini_get('session.save_handler') === 'memcache') {
				//surpress warnings just in case
				@session_destroy();
			}

			Zend_Session :: forgetMe();

			$auth->clearIdentity();
			//$this->redirect('/login');
			header("Location: /login");exit;
		}else {
			$token = $this->refreshConsumerAccessToken(array('email' => $customer->email, 'refresh_token' => $customer->zype->refresh_token));

			$customer->zype->access_token = $token->access_token;
			$customer->zype->refresh_token = $token->refresh_token;
			$customer->zype->expiration = $token->created_at + $token->expires_in;

			return $customer;
		}
	}

	public function setCookie()
	{
		$customer = new Zend_Session_Namespace('customer');

		if (!isset($customer->id) && $_COOKIE['kodz_id'] && $_COOKIE['kodz_token']) {

		}
	}

	public function listVideos($data = array())
	{
		return $this->execute('videos', 'GET', 'read', null, $data);

		/*
		 * example response format
		 *
		 *

		[pagination] => stdClass Object
        (
            [current] => 1
            [previous] =>
            [next] => 2
            [per_page] => 10
            [pages] => 13
        )

		[response] => Array
        (
            [0] => stdClass Object
                (
                    [_id] => 5c34f768c37e10561e617302
                    [active] => 1
                    [categories] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [_id] => 5c34f768c37e10561e617309
                                    [category_id] => 5c34f75f34ed121274216c8a
                                    [title] => YouTube Channel
                                    [value] => Array
                                        (
                                            [0] => kinolorber
                                        )
                                )
                        )
                    [country] =>
                    [created_at] => 2019-01-08T14:18:04.033-05:00
                    [custom_attributes] => stdClass Object
                        (
                        )
                    [description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                    [disable_at] =>
                    [discovery_url] =>
                    [duration] => 2940
                    [enable_at] =>
                    [episode] =>
                    [featured] =>
                    [foreign_id] =>
                    [free_audio_stream] =>
                    [friendly_title] => northern-wedding
                    [keywords] => Array
                        (
                            [0] => Deutschland
                            [1] => yt:cc=onFilm
                        )
                    [marketplace_ids] => stdClass Object
                        (
                        )
                    [mature_content] =>
                    [on_air] =>
                    [ott_description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                    [pass_required] =>
                    [plan_ids] => Array
                        (
                        )
                    [preview_ids] => Array
                        (
                        )
                    [program_guide_id] =>
                    [published_at] => 2015-10-13T22:37:49.000-04:00
                    [purchase_price] =>
                    [purchase_required] =>
                    [rating] => 0
                    [related_playlist_ids] => Array
                        (
                        )
                    [rental_duration] =>
                    [rental_price] =>
                    [rental_required] =>
                    [request_count] => 0
                    [season] =>
                    [site_id] => 5c1c37a4649f0f131a003367
                    [source_id] =>
                    [status] => created
                    [subscription_ads_enabled] => 1
                    [subscription_required] =>
                    [title] => Northern Wedding
                    [updated_at] => 2019-01-26T01:20:21.360-05:00
                    [zobject_ids] => Array
                        (
                        )
                    [crunchyroll_id] =>
                    [hulu_id] =>
                    [mrss_id] =>
                    [kaltura_id] =>
                    [thumbnails] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [aspect_ratio] =>
                                    [height] => 240
                                    [name] =>
                                    [url] => https://image.zype.com/5c1c37a4649f0f131a003367/5c34f768c37e10561e617302/custom_thumbnail/240.jpg?1546975080
                                    [width] => 426
                                )
							...
                        )
                    [transcoded] =>
                    [vimeo_id] =>
                    [youtube_id] => EuCDLXEmql8
                    [short_description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                    [content_rules] => Array
                        (
                        )
                    [is_zype_live] =>
                )
			...
		 *
		 */
	}

	public function getVideo($video_id)
	{
		$data = array(
			'id' => $video_id,
			'zobject_details' => true
		);

		$response = $this->execute('videos', 'GET', 'read', [], $data);

		if (isset($response->response) && array_key_exists(0, $response->response)) {
			return $response->response[0];
		}

		return false;

		/*
		 * example response format
		 *
		 *

		[response] => Array
        (
            [0] => stdClass Object
                (
                    [_id] => 5c34f768c37e10561e617302
                    [active] => 1
                    [categories] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [_id] => 5c34f768c37e10561e617309
                                    [category_id] => 5c34f75f34ed121274216c8a
                                    [title] => YouTube Channel
                                    [value] => Array
                                        (
                                            [0] => kinolorber
                                        )
                                )
                        )
                    [country] =>
                    [created_at] => 2019-01-08T14:18:04.033-05:00
                    [custom_attributes] => stdClass Object
                        (
                        )
                    [description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                    [disable_at] =>
                    [discovery_url] =>
                    [duration] => 2940
                    [enable_at] =>
                    [episode] =>
                    [featured] =>
                    [foreign_id] =>
                    [free_audio_stream] =>
                    [friendly_title] => northern-wedding
                    [keywords] => Array
                        (
                            [0] => Deutschland
                            [1] => yt:cc=onFilm
                        )
                    [marketplace_ids] => stdClass Object
                        (
                        )
                    [mature_content] =>
                    [on_air] =>
                    [ott_description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                    [pass_required] =>
                    [plan_ids] => Array
                        (
                        )
                    [preview_ids] => Array
                        (
                        )
                    [program_guide_id] =>
                    [published_at] => 2015-10-13T22:37:49.000-04:00
                    [purchase_price] =>
                    [purchase_required] =>
                    [rating] => 0
                    [related_playlist_ids] => Array
                        (
                        )
                    [rental_duration] =>
                    [rental_price] =>
                    [rental_required] =>
                    [request_count] => 0
                    [season] =>
                    [site_id] => 5c1c37a4649f0f131a003367
                    [source_id] =>
                    [status] => created
                    [subscription_ads_enabled] => 1
                    [subscription_required] =>
                    [title] => Northern Wedding
                    [updated_at] => 2019-01-26T01:20:21.360-05:00
                    [zobject_ids] => Array
                        (
                        )
                    [crunchyroll_id] =>
                    [hulu_id] =>
                    [mrss_id] =>
                    [kaltura_id] =>
                    [thumbnails] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [aspect_ratio] =>
                                    [height] => 240
                                    [name] =>
                                    [url] => https://image.zype.com/5c1c37a4649f0f131a003367/5c34f768c37e10561e617302/custom_thumbnail/240.jpg?1546975080
                                    [width] => 426
                                )
							...
                        )
                    [transcoded] =>
                    [vimeo_id] =>
                    [youtube_id] => EuCDLXEmql8
                    [short_description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                    [content_rules] => Array
                        (
                        )
                    [is_zype_live] =>
                )
			...
		 *
		 */
	}

	public function updateVideo($video_id, $data = array())
	{
		return $this->execute('videos/'.$video_id, 'PUT', 'admin', $data);


		/*
		 * example response format
		 *
		 *

		[response] => stdClass Object
            (
                [_id] => 5c34f768c37e10561e617302
                [active] => 1
                [categories] => Array
                    (
                        [0] => stdClass Object
                            (
                                [_id] => 5c34f768c37e10561e617309
                                [category_id] => 5c34f75f34ed121274216c8a
                                [title] => YouTube Channel
                                [value] => Array
                                    (
                                        [0] => kinolorber
                                    )
                            )
                    )
                [country] =>
                [created_at] => 2019-01-08T14:18:04.033-05:00
                [custom_attributes] => stdClass Object
                    (
                    )
                [description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                [disable_at] =>
                [discovery_url] =>
                [duration] => 2940
                [enable_at] =>
                [episode] =>
                [featured] =>
                [foreign_id] =>
                [free_audio_stream] =>
                [friendly_title] => northern-wedding
                [keywords] => Array
                    (
                        [0] => Deutschland
                        [1] => yt:cc=onFilm
                    )
                [marketplace_ids] => stdClass Object
                    (
                    )
                [mature_content] =>
                [on_air] =>
                [ott_description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                [pass_required] =>
                [plan_ids] => Array
                    (
                    )
                [preview_ids] => Array
                    (
                    )
                [program_guide_id] =>
                [published_at] => 2015-10-13T22:37:49.000-04:00
                [purchase_price] =>
                [purchase_required] =>
                [rating] => 0
                [related_playlist_ids] => Array
                    (
                    )
                [rental_duration] =>
                [rental_price] =>
                [rental_required] =>
                [request_count] => 0
                [season] =>
                [site_id] => 5c1c37a4649f0f131a003367
                [source_id] =>
                [status] => created
                [subscription_ads_enabled] => 1
                [subscription_required] =>
                [title] => Northern Wedding
                [updated_at] => 2019-01-26T01:20:21.360-05:00
                [zobject_ids] => Array
                    (
                    )
                [crunchyroll_id] =>
                [hulu_id] =>
                [mrss_id] =>
                [kaltura_id] =>
                [thumbnails] => Array
                    (
                        [0] => stdClass Object
                            (
                                [aspect_ratio] =>
                                [height] => 240
                                [name] =>
                                [url] => https://image.zype.com/5c1c37a4649f0f131a003367/5c34f768c37e10561e617302/custom_thumbnail/240.jpg?1546975080
                                [width] => 426
                            )
						...
                    )
                [transcoded] =>
                [vimeo_id] =>
                [youtube_id] => EuCDLXEmql8
                [short_description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                [content_rules] => Array
                    (
                    )
                [is_zype_live] =>
            )
		 *
		 */
	}

	public function listPlaylists($data = array())
	{
		if (!isset($data['per_page'])) $data['per_page'] = 100;

		return $this->execute('playlists', 'GET', 'read', array(), $data);

		/*
		 *  example response format
		 *
		 *

		[pagination] => stdClass Object
        (
            [current] => 1
            [previous] =>
            [next] => 2
            [per_page] => 10
            [pages] => 13
        )

		[response] => Array
        (
            [0] => stdClass Object
                (
                    [_id] => 5c4f528572289b10f600156e
                    [_keywords] => Array
                        (
                            [0] => art
                            [1] => desc
                        )

                    [active] => 1
                    [auto_remove_video_entitlements] =>
                    [auto_update_video_entitlements] =>
                    [categories] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [_id] => 5c4f528572289b10f600156f
                                    [category_id] => 5c4882a0af5909149201363f
                                    [title] => Classics
                                    [value] => Array
                                        (
                                        )

                                )
							...
                        )
                    [created_at] => 2019-01-28T14:05:41.041-05:00
                    [deleted_at] =>
                    [description] =>
                    [discovery_url] =>
                    [friendly_title] => currently-available
                    [match_type] => any
                    [parent_id] =>
                    [playlist_item_count] => 21
                    [playlist_type] => manual
                    [priority] => 3
                    [purchase_price] =>
                    [purchase_required] =>
                    [related_video_ids] => Array
                        (
                        )
                    [rental_duration] =>
                    [rental_price] =>
                    [rental_required] =>
                    [site_id] => 5c1c37a4649f0f131a003367
                    [thumbnail_layout] => landscape
                    [title] => Desc/Art
                    [updated_at] => 2019-01-29T13:23:37.863-05:00
                    [images] => Array
                        (
                        )
                    [thumbnails] => Array
                        (
                        )
                    [content_rules] => Array
                        (
                        )
                )
			....

		 */
	}

	public function getPlaylist($playlist_id)
	{
		$data = array(
			'id' => $playlist_id
		);

		$response = $this->execute('playlists', 'GET', 'read', $data);

		if (isset($response->response)) {
			return $response->response[0];
		}

		return false;

		/*
		 *  example response format
		 *
		 *

		[response] => Array
        (
            [0] => stdClass Object
                (
                    [_id] => 5c4f528572289b10f600156e
                    [_keywords] => Array
                        (
                            [0] => art
                            [1] => desc
                        )

                    [active] => 1
                    [auto_remove_video_entitlements] =>
                    [auto_update_video_entitlements] =>
                    [categories] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [_id] => 5c4f528572289b10f600156f
                                    [category_id] => 5c4882a0af5909149201363f
                                    [title] => Classics
                                    [value] => Array
                                        (
                                        )

                                )
							...
                        )
                    [created_at] => 2019-01-28T14:05:41.041-05:00
                    [deleted_at] =>
                    [description] =>
                    [discovery_url] =>
                    [friendly_title] => currently-available
                    [match_type] => any
                    [parent_id] =>
                    [playlist_item_count] => 21
                    [playlist_type] => manual
                    [priority] => 3
                    [purchase_price] =>
                    [purchase_required] =>
                    [related_video_ids] => Array
                        (
                        )
                    [rental_duration] =>
                    [rental_price] =>
                    [rental_required] =>
                    [site_id] => 5c1c37a4649f0f131a003367
                    [thumbnail_layout] => landscape
                    [title] => Desc/Art
                    [updated_at] => 2019-01-29T13:23:37.863-05:00
                    [images] => Array
                        (
                        )
                    [thumbnails] => Array
                        (
                        )
                    [content_rules] => Array
                        (
                        )
                )
			....

		 */
	}

	public function updatePlaylist($playlist_id, $data = array())
	{
		return $this->execute('playlists/'.$playlist_id, 'PUT', 'admin', $data);


		/*
		 * example response format
		 *
		 *

		[response] => stdClass Object
            (
                [_id] => 5c4f528572289b10f600156e
                [_keywords] => Array
                    (
                        [0] => art
                        [1] => desc
                    )

                [active] => 1
                [auto_remove_video_entitlements] =>
                [auto_update_video_entitlements] =>
                [categories] => Array
                    (
                        [0] => stdClass Object
                            (
                                [_id] => 5c4f528572289b10f600156f
                                [category_id] => 5c4882a0af5909149201363f
                                [title] => Classics
                                [value] => Array
                                    (
                                    )

                            )
						...
                    )
                [created_at] => 2019-01-28T14:05:41.041-05:00
                [deleted_at] =>
                [description] =>
                [discovery_url] =>
                [friendly_title] => currently-available
                [match_type] => any
                [parent_id] =>
                [playlist_item_count] => 21
                [playlist_type] => manual
                [priority] => 3
                [purchase_price] =>
                [purchase_required] =>
                [related_video_ids] => Array
                    (
                    )
                [rental_duration] =>
                [rental_price] =>
                [rental_required] =>
                [site_id] => 5c1c37a4649f0f131a003367
                [thumbnail_layout] => landscape
                [title] => Desc/Art
                [updated_at] => 2019-01-29T13:23:37.863-05:00
                [images] => Array
                    (
                    )
                [thumbnails] => Array
                    (
                    )
                [content_rules] => Array
                    (
                    )
            )
		 *
		 */
	}

	public function listPlaylistVideos($playlist_id, $data = array())
	{
		return $this->execute("playlists/$playlist_id/videos", 'GET', 'read', null, $data);

		/*
		 * example response format
		 *
		 *

		[pagination] => stdClass Object
        (
            [current] => 1
            [previous] =>
            [next] => 2
            [per_page] => 10
            [pages] => 13
        )

		[response] => Array
        (
            [0] => stdClass Object
                (
                    [_id] => 5c34f768c37e10561e617302
                    [active] => 1
                    [categories] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [_id] => 5c34f768c37e10561e617309
                                    [category_id] => 5c34f75f34ed121274216c8a
                                    [title] => YouTube Channel
                                    [value] => Array
                                        (
                                            [0] => kinolorber
                                        )
                                )
                        )
                    [country] =>
                    [created_at] => 2019-01-08T14:18:04.033-05:00
                    [custom_attributes] => stdClass Object
                        (
                        )
                    [description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                    [disable_at] =>
                    [discovery_url] =>
                    [duration] => 2940
                    [enable_at] =>
                    [episode] =>
                    [featured] =>
                    [foreign_id] =>
                    [free_audio_stream] =>
                    [friendly_title] => northern-wedding
                    [keywords] => Array
                        (
                            [0] => Deutschland
                            [1] => yt:cc=onFilm
                        )
                    [marketplace_ids] => stdClass Object
                        (
                        )
                    [mature_content] =>
                    [on_air] =>
                    [ott_description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                    [pass_required] =>
                    [plan_ids] => Array
                        (
                        )
                    [preview_ids] => Array
                        (
                        )
                    [program_guide_id] =>
                    [published_at] => 2015-10-13T22:37:49.000-04:00
                    [purchase_price] =>
                    [purchase_required] =>
                    [rating] => 0
                    [related_playlist_ids] => Array
                        (
                        )
                    [rental_duration] =>
                    [rental_price] =>
                    [rental_required] =>
                    [request_count] => 0
                    [season] =>
                    [site_id] => 5c1c37a4649f0f131a003367
                    [source_id] =>
                    [status] => created
                    [subscription_ads_enabled] => 1
                    [subscription_required] =>
                    [title] => Northern Wedding
                    [updated_at] => 2019-01-26T01:20:21.360-05:00
                    [zobject_ids] => Array
                        (
                        )
                    [crunchyroll_id] =>
                    [hulu_id] =>
                    [mrss_id] =>
                    [kaltura_id] =>
                    [thumbnails] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [aspect_ratio] =>
                                    [height] => 240
                                    [name] =>
                                    [url] => https://image.zype.com/5c1c37a4649f0f131a003367/5c34f768c37e10561e617302/custom_thumbnail/240.jpg?1546975080
                                    [width] => 426
                                )
							...
                        )
                    [transcoded] =>
                    [vimeo_id] =>
                    [youtube_id] => EuCDLXEmql8
                    [short_description] => When the bug is found in Linda's desk, all NATO is in high alert. Moritz must cover his tracks by any
                    [content_rules] => Array
                        (
                        )
                    [is_zype_live] =>
                )
			...
		 *
		 */
	}

	public function getPlaylistChildrenPlaylists($playlist_id)
	{
		/*
		 * There is no way to get just the children playlists of a specific list so have to get the entire
		 * relationships list and parse to get the children for the specific playlist
		 */

		$full_list = $this->execute("playlists/relationships", 'GET', 'admin', array());

		return $this->getPlaylistChildren($full_list, $playlist_id, array());
	}

	private function getPlaylistChildren($item, $playlist_id, $array)
	{
		if (!$item || !is_array($item->playlists)) return false;

		foreach ($item->playlists as $list) {
			if ($list->id == $playlist_id) {
				if ($list->playlists) {
					foreach ($list->playlists as $child) {
						//the children aren't in order so have to order them ourselves
						$array[$child->priority] = $child;
					}
				}

				return $array;
			}else {
				$array = $this->getPlaylistChildren($list, $playlist_id, $array);

				if ($array) return $array;
			}
		}

		return $array;
	}

	public function listCategories($data = array())
	{
		return $this->execute('categories', 'GET', 'read', $data);

		/*
		 *  example response format
		 *
		 *

		[pagination] => stdClass Object
        (
            [current] => 1
            [previous] =>
            [next] => 2
            [per_page] => 10
            [pages] => 13
        )

		[response] => Array
        (
            [0] => stdClass Object
                (

		*/
	}

	public function getCategory($category_id)
	{
		$data = array(
			'id' => $category_id
		);

		$response = $this->execute('categories', 'GET', 'read', $data);

		if (isset($response->response)) {
			return $response->response[0];
		}

		return false;

		/*
		 *  example response format
		 *
		 *



		 */
	}

	public function createTransaction($data = array())
	{
		$response = $this->execute('transactions', 'POST', 'admin', $data);

		if (isset($response->response)) {
			return $response->response;
		}

		return false;

		/*
		 *  example response format
		 *
		 *



		 */
	}

	public function createConsumer($user_data = array())
	{
		$data = array(
			'consumer' => $user_data
		);

		$response = $this->execute('consumers', 'POST', 'admin', $data);

		if (isset($response->response)) {
			return $response->response;
		}

		return false;

		/*
		 *  example response format
		 *
		 *



		 */
	}

	public function updateConsumer($consumer_id, $user_data = array())
	{
		$data = array(
			'consumer' => $user_data
		);

		$response = $this->execute('consumers/'.$consumer_id, 'PUT', 'admin', $data);

		if (isset($response->message)) {
			return $response->message;
		}else {
			return "Account updated";
		}
	}

	public function getConsumerByEmail($email)
	{
		$response = $this->execute('consumers', 'GET', 'admin', array(), array('email'=>$email));

		if (isset($response->response)) {
			return $response->response[0];
		}

		return false;

		/*
		 *  example response format
		 *
		 *


		*/
	}

	public function getConsumerAccessToken($user_data = array())
	{
		$data = array(
			'client_id' => $this->cfg['client_id'],
			'client_secret' => $this->cfg['secret_key'],
			'username' => $user_data['email'],
			'password' => $user_data['password'],
			'grant_type' => 'password'
		);

		return $this->execute('oauth/token', 'POST', 'admin', $data);

		/*
		 *  example response format
		 *
		 *

		[response] =>  stdClass Object
            (
				[access_token] => ''
				[token_type] => 'bearer'
				[expires_in] => ''
				[refresh_token] => ''
				[scope] => ''
				[grant_type] => ''
				[created_at] => ''
			}

		*/
	}

	public function refreshConsumerAccessToken($user_data = array())
	{
		$data = array(
			'client_id' => $this->cfg['client_id'],
			'client_secret' => $this->cfg['secret_key'],
			'username' => $user_data['email'],
			'refresh_token' => $user_data['refresh_token'],
			'grant_type' => 'refresh_token'
		);

		return $this->execute('oauth/token', 'POST', 'admin', $data);

		/*
		 *  example response format
		 *
		 *

		[response] =>  stdClass Object
            (
				[access_token] => ''
				[token_type] => 'bearer'
				[expires_in] => ''
				[refresh_token] => ''
				[scope] => ''
				[grant_type] => ''
				[created_at] => ''
			}

		*/
	}

	public function initiateForgotPassword($email)
	{
		return $this->execute('consumers/forgot_password', 'PUT', 'admin', array('email'=>$email));
	}

	public function createVideoEntitlement($customer_id, $data)
	{
		return $this->execute("consumers/$customer_id/videos", 'POST', 'admin', $data);

		/*
		 *  example response format
		 *
		 *


		*/
	}

	public function createPlaylistEntitlement($customer_id, $data)
	{
		return $this->execute("consumers/$customer_id/playlists", 'POST', 'admin', $data);

		/*
		 *  example response format
		 *
		 *


		*/
	}

	public function getVideoEntitlements($customer_id, $data = array())
	{
		return $this->execute("consumers/$customer_id/videos", 'GET', 'admin', $data, null, false);

		/*
		 *  example response format
		 *
		 *

		[pagination] => stdClass Object
        (
            [current] => 1
            [previous] =>
            [next] => 2
            [per_page] => 10
            [pages] => 13
        )

		[response] => Array
        (
            [0] => stdClass Object
                (
					[_id] => ''
					[consumer_id] => ''
					[created_at] => ''
					[expires_at] => ''
					[transaction_id] => ''
					[transaction_type] => '' // ( purchase/rental/pass )
					[updated_at] => ''
					[video_id] => ''
					[video_title] => ''
				)
			...
		)

		*/
	}

	public function getVideoEntitlement($customer_id, $entitlement_id)
	{
		return $this->execute("consumers/$customer_id/videos/id/$entitlement_id", 'GET', 'admin', null, null, false);

		/*
		 *  example response format
		 *
		 *

		[response] => stdClass Object
            (
				[_id] => ''
				[consumer_id] => ''
				[created_at] => ''
				[expires_at] => ''
				[transaction_id] => ''
				[transaction_type] => '' // ( purchase/rental/pass )
				[updated_at] => ''
				[video_id] => ''
				[video_title] => ''
			)


		*/
	}

	public function checkVideoEntitlement($video_id, $access_token)
	{
		return $this->execute("videos/$video_id/entitled", 'GET', 'read', array(), array('access_token'=>$access_token), false);

		/*
		 *  example response format
		 *
		 *


		*/
	}

	public function getPlaylistEntitlements($customer_id, $data = array())
	{
		return $this->execute("consumers/$customer_id/playlists", 'GET', 'admin', $data, null, false);

		/*
		 *  example response format
		 *
		 *

		[pagination] => stdClass Object
        (
            [current] => 1
            [previous] =>
            [next] => 2
            [per_page] => 10
            [pages] => 13
        )

		[response] => Array
        (
            [0] => stdClass Object
                (
					[_id] => ''
					[consumer_id] => ''
					[created_at] => ''
					[expires_at] => ''
					[transaction_id] => ''
					[transaction_type] => '' // ( purchase/rental/pass )
					[updated_at] => ''
					[playlist_id] => ''
					[playlist_title] => ''
				)
			...
		)

		*/
	}

	public function getVideoPlayer($video_id, $user_agent, $query_params = array())
	{
		$qs_array = [];
		$qs_array['api_key'] = $this->cfg['api_keys']['player'];
		foreach ($query_params as $k => $param) {
			$qs_array[$k] = $param;
		}

		$queryString = http_build_query($qs_array);
		$url = $this->cfg['player_url'] . '/embed/'. $video_id . ".js" . "?$queryString";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper('GET'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}

	public function getVideoPlayerData($video_id, $user_agent, $query_params = array())
	{
		$qs_array = [];
		$qs_array['api_key'] = $this->cfg['api_keys']['player'];
		foreach ($query_params as $k => $param) {
			$qs_array[$k] = $param;
		}

		$queryString = http_build_query($qs_array);
		$url = $this->cfg['player_url'] . '/embed/'. $video_id . ".json" . "?$queryString";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper('GET'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}

	public function getCategoryValues($category_id)
	{
		$result = $this->execute("categories/$category_id", 'GET', 'read', array());

		return $result->response->values;
	}

	public function redeemCode($redemption_code, $access_token)
	{
		return $this->execute("redemption_codes/redeem", 'POST', 'admin', array('code'=>$redemption_code,'access_token'=>$access_token));

		/*
		 * example success format
		 *
		 *

		[response] => stdClass Object (
			[_id] => 5ca39a46c7e83b6ce939943c
			[code] => TOVGMSZZCMC7OVZN
			[content_discovery_url] =>
			[content_title] => Like Me
			[content_type] => video
			[created_at] => 2019-04-02T13:22:14.477-04:00
			[expiration_date] => 2019-04-30
			[pass_plan_id] =>
			[plan_id] =>
			[playlist_id] =>
			[redeemed_at] => 2019-04-02T13:59:20.495-04:00
			[site_id] => 5c1c37a4649f0f131a003367
			[transaction_id] => 5ca3a2f8eabece145e9f3356
			[updated_at] => 2019-04-02T13:59:20.495-04:00
			[video_id] => 5c7d59d679922f125600a3b2
			[nice_code] => TOVG-MSZZ-CMC7-OVZN
			[available] =>
			[redeemed] => 1
			[expired] => )
		)

		 *
		 * example failure format
		 *

		[message] => Redemption code not available.

		 */
	}

	public function linkDevice($consumer_id, $pin)
	{
		return $this->execute("pin/link", 'PUT', 'admin', array(), array('consumer_id'=>$consumer_id,'pin'=>$pin));

		/*
		 * example success format
		 *
		 *

		[response] => stdClass Object (
			[_id] => 5ca39a46c7e83b6ce939943c
			[consumer_id] =>
			[created_at] => 2019-04-02T13:22:14.477-04:00
			[deleted_at] =>
			[linked_device_id] =>
			[pin] =>
			[pin_expiration] => 2019-04-02T13:22:14.477-04:00
			[updated_at] =>
			[linked] =>
			[subscription_count] =>
		  )
		)

		 *
		 * example failure format
		 *

		[message] => Device Pin not found.

		 */
	}

	public function getVideoContentRules($video_id)
	{
		return $this->execute("videos/$video_id/content_rules", 'GET', 'read', array(), array());

		/*
		 * example success format
		 *
		 *

	    [response] => Array
        (
            [0] => stdClass Object
                (
                    [_id] => 5cd2f69cc7e83b7fc47b3d3e
                    [created_at] => 2019-05-08T11:32:45.002-04:00
                    [geographies] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [_id] => 5cd2f69cc7e83b7fc47b3d3f
                                    [countries] => Array
                                        (
                                            [0] => US
                                        )
                                )
                        )
                    [policy] => deny
                    [priority] => 1
                )
        )


		 */
	}

	public function testVideoContentRules($video_id, $country_code = null)
	{
		$result = $this->execute("videos/$video_id/content_rules", 'GET', 'read', array(), array());

		$restricted = false;
		foreach ($result as $response) {
			foreach ($response as $row) {
				if (isset($row->geographies) && isset($row->policy) && $row->policy == 'deny') {
					foreach ($row->geographies as $geography) {
						foreach ($geography->countries as $country) {
							if ($country == "*" || $country == $country_code) {
								$restricted = true;
							}
						}
					}
				}elseif (isset($row->geographies) && isset($row->policy) && $row->policy == 'allow') {
					foreach ($row->geographies as $geography) {
						foreach ($geography->countries as $country) {
							if ($country == "*" || $country == $country_code) {
								return false;
							}
						}
					}
				}
			}
		}

		return $restricted;
	}

	public function getPlaylistContentRules($playlist_id)
	{
		return $this->execute("playlists/$playlist_id/content_rules", 'GET', 'read', array(), array());

		/*
		 * example success format
		 *
		 *

	    [response] => Array
        (
            [0] => stdClass Object
                (
                    [_id] => 5cd2f69cc7e83b7fc47b3d3e
                    [created_at] => 2019-05-08T11:32:45.002-04:00
                    [geographies] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [_id] => 5cd2f69cc7e83b7fc47b3d3f
                                    [countries] => Array
                                        (
                                            [0] => US
                                        )
                                )
                        )
                    [policy] => deny
                    [priority] => 1
                )
        )


		 */
	}

	public function testPlaylistContentRules($playlist_id, $country_code = null)
	{
		$result = $this->execute("playlists/$playlist_id/content_rules", 'GET', 'read', array(), array());

		$restricted = false;
		foreach ($result as $response) {
			foreach ($response as $row) {
				if (isset($row->geographies) && isset($row->policy) && $row->policy == 'deny') {
					foreach ($row->geographies as $geography) {
						foreach ($geography->countries as $country) {
							if ($country == "*" || $country == $country_code) {
								$restricted = true;
							}
						}
					}
				}elseif (isset($row->geographies) && isset($row->policy) && $row->policy == 'allow') {
					foreach ($row->geographies as $geography) {
						foreach ($geography->countries as $country) {
							if ($country == "*" || $country == $country_code) {
								return false;
							}
						}
					}
				}
			}
		}

		return $restricted;
	}

	public function geoIP($ip_address)
	{
		return $this->execute("geoip", 'GET', 'read', array(), array('ip_address'=>$ip_address), false);

		/*
		 * example success format
		 *
		 *

		[response] => stdClass Object (
			[request] => 192.0.0.0
			[ip] => 192.0.0.0
			[country_code] => 225
			[country_code2] => US
			[country_code3] => USA
			[country_name] => United States
			[continent_code] => NA
		  )
		)
		 */
	}

	public function listAdTimings($video_id)
	{
		return $this->execute("videos/$video_id/ad_timings", 'GET', 'read', array(), array());

		/*
		 *
		 *  example success format

		[response] => Array
        (
            [0] => stdClass Object
                (
                    [data_source_id] => 5c782ac55fcac114ca009fd8
                    [ad_timings] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [_id] => 5c782ac55fcac114ca009fd9
                                    [mode] => off
                                    [time] => 0
                                    [type] => in
                                )
                        )
                )
        )
		*/
	}

	public function listSubscriptionPlans($data = array())
	{
		return $this->execute("plans", 'GET', 'read', array(), $data);

		/*
		 * example success format
		 *
		 *

		[response] => stdClass Object (
			Array
               (
                [0] => stdClass Object (
					[_id] => "5d66cb2dbde1425d12685472"
					[_keywords] => array(
						"kino",
						"members"
					)
					[active] => true
					[amazon_id] => ""
					[amount] => "9.99
					[braintree_id] => null
					[created_at] => "2019-08-28T14:42:53.801-04:00"
					[currency] => "USD"
					[deleted_at] => null
					[description] => "Members for Kino Now"
					[discovery_url] => ""
					[entitlement_type] => "global"
					[interval] => "month
					[interval_count] => 1
					[marketplace_ids] => []
					[name] => "Kino Members"
					[playlist_ids] => array()
					[position] => 0
					[site_id] => "5c1c37a4649f0f131a003367
					[stripe_id] => ""
					[third_party_id] => ""
					[trial_period_days] => 7
					[updated_at] => "2019-08-28T14:43:22.555-04:00"
				)
			)
		  )
		)
		 */

	}

	public function getSubscriptionPlan($zype_plan_id)
	{
		return $this->execute("plans/$zype_plan_id", 'GET', 'read', array(), array());

		/*
		 * example success format
		 *
		 *

		[response] => stdClass Object (
			[_id] => "5d66cb2dbde1425d12685472"
			[_keywords] => array(
				"kino",
				"members"
			)
			[active] => true
			[amazon_id] => ""
			[amount] => "9.99
			[braintree_id] => null
			[created_at] => "2019-08-28T14:42:53.801-04:00"
			[currency] => "USD"
			[deleted_at] => null
			[description] => "Members for Kino Now"
			[discovery_url] => ""
			[entitlement_type] => "global"
			[interval] => "month
			[interval_count] => 1
			[marketplace_ids] => []
			[name] => "Kino Members"
			[playlist_ids] => array()
			[position] => 0
			[site_id] => "5c1c37a4649f0f131a003367
			[stripe_id] => ""
			[third_party_id] => ""
			[trial_period_days] => 7
			[updated_at] => "2019-08-28T14:43:22.555-04:00"
		  )
		)
		 */
	}

	public function createSubscription($data)
	{
		return $this->execute("subscriptions", 'POST', 'admin', $data);

		/*
		 *  example response format
		 *
		 *


		*/
	}

	public function createVideoContentRule($video_id, $data)
	{
		return $this->execute("videos/$video_id/content_rules", 'POST', 'admin', $data);

		/*
		 *  example response format
		 *
		 *


		*/
	}

	public function deleteVideoContentRule($video_id, $rule_id)
	{
		return $this->execute("videos/$video_id/content_rules/$rule_id", 'DELETE', 'admin');

		/*
		 *  example response format
		 *
		 *


		*/
	}

	public function getVideoSubtitles($video_id)
	{
		$response = $this->execute("videos/$video_id/subtitles", 'GET', 'read');

		if (isset($response->response)) {
			return $response->response;
		}

		return false;

		/*
		 * example success format
		 *
		 *

		[response] => Array
        (
            [0] => stdClass Object
                (
					[_id] => ''
					[active] => true
					[attachment_storage] => stdClass Object (
							[file] => 'gcs'
						)
					[extension_type] => 'vtt'
					[file_content_type] => 'application/octet-stream'
					[file_file_name] => ''
					[file_file_size] => ''
					[file_fingerprint] => ''
					[file_updated_at] => ''
					[language] => 'en' //language ISO code
					[storage] => 'gcs'
				)
			...
		)
		 */
	}
}
