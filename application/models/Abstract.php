<?php
class Application_Model_Abstract
{
	public $id;
	public $created;
	public $modified;

	public function __construct($options = null)
	{
		if (!is_null($options)) {
			$this->_setOptions($options);
		}
	}

	public function _setOptions($options)
	{
		if (is_array($options)) {
			$methods = get_class_methods($this);
			$tmp_vars = get_object_vars($this);
			$variables = array_keys($tmp_vars);
			foreach ($options as $key => $value) {
				$method_name = 'set'.$this->_camelize($key);
				if (in_array($method_name, $methods)) {
					$this->$method_name($value);
				} elseif (in_array($key, $variables)) {
					$this->$key = $value;
				}
			}
		}
	}

    public function __call($method, $args)
    {
        if (strpos($method, 'get') === 0) {
            $variable_name = $this->_decamelize(str_replace('get', '', $method));
            if (isset($this->$variable_name)) {
                return $this->$variable_name;
            } else {
                return null;
            }
        }
    }

	public function toArray()
	{
		return get_object_vars($this);
	}

	function _decamelize($word) {
		return preg_replace_callback(
			'/(^|[a-z])([A-Z])/',
			function ($matches) {
				return strtolower(strlen($matches[1]) ? "{$matches[1]}_$matches[2]" : $matches[2]);
			},
			$word
		);
	}

	function _camelize($word) {
		return preg_replace_callback('/(^|_)([a-z])/',
			function ($matches) {
				return strtoupper($matches[2]);
			},
			$word);
	}
}
