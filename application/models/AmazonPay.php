<?php

class Application_Model_AmazonPay
{
	private $cfg;

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();
		$this->_client = new AmazonPay\Client(array(
			'client_id' => $settings_mapper->getValueByCode('ecommerce_amazonpay_client_id'),
			'merchant_id' => $settings_mapper->getValueByCode('ecommerce_amazonpay_merchant_id'),
			'access_key' => $settings_mapper->getValueByCode('ecommerce_amazonpay_access_key'),
			'secret_key' => $settings_mapper->getValueByCode('ecommerce_amazonpay_secret_key'),
			'region' => $settings_mapper->getValueByCode('ecommerce_amazonpay_region'),
		));
		if (APPLICATION_ENV != 'production') {
			$this->_client->setSandbox(true);
		}

		$this->_store_name = $settings_mapper->getValueByCode('site_name');
	}

	public function getAddress($amazonOrderReferenceId, $access_token)
	{
		return $response = $this->_client->GetOrderReferenceDetails(array(
			'amazon_order_reference_id' => $amazonOrderReferenceId,
			'address_consent_token' => $access_token
		));
	}

	public function authorize($products, $totals = array(), $amazon_order_reference_id)
	{
		$error = '';

		$params = array();
		$params['amazon_order_reference_id'] = $amazon_order_reference_id;
		$params['authorization_amount'] = $params['amount'] = round($totals['total'], 2);
		$params['currency_code'] = 'USD';
		$params['authorization_reference_id'] = $params['amazon_order_reference_id'];//this should be a unique string/id but we don't create one yet so using Amazon's for now
		$params['seller_authorization_note'] = 'Authorizing payment';
		$params['transaction_timeout'] = 0;
		$params['store_name'] = $this->_store_name;

		$response = $this->_client->SetOrderReferenceDetails($params);
		$responseObject = json_decode($response->toJson());

		if ($this->_client->success) {
			$response = $this->_client->confirmOrderReference($params);
			$responseObject = json_decode($response->toJson());
		}

		if ($this->_client->success) {
			$response = $this->_client->authorize($params);
			$responseObject = json_decode($response->toJson());

			//print_r($responseObject);exit;

			if ($this->_client->success && $responseObject->AuthorizeResult->AuthorizationDetails->AuthorizationStatus->State != "Declined") {
				return array('response' => $responseObject, 'profile_response' => null, 'error_message' => null);
			}else {
				$error = "Declined by Amazon Pay";

				return array('response' => null, 'profile_response' => null, 'error_message' => $error);
			}
		}
	}

	public function capture($authorization_id, $amount, $reference_id)
	{
		//capture funds
		$response = $this->_client->capture(array(
			'amazon_authorization_id' => $authorization_id,
			'capture_amount' => $amount,
			'currency_code' => 'USD',
			'capture_reference_id' => $reference_id
		));
		$responseObject = json_decode($response->toJson());

		//print_r($responseObject);exit;

		if($this->_client->success && !isset($responseObject->Error)) {
			return array('response' => $responseObject, 'profile_response' => null, 'error_message' => null);
		}else {
			return array('response' => null, 'profile_response' => null, 'error_message' => $responseObject->Error);
		}
	}

	public function refundTransaction($transactionId, $amount, $order_id)
	{
		$response = $this->_client->Refund(array(
			'amazon_capture_id' => $transactionId,
			'refund_reference_id' => $order_id."-".time(),
			'refund_amount' => $amount,
			'currency_code' => 'USD',
		));
		$responseObject = json_decode($response->toJson());

		if($this->_client->success && !isset($responseObject->Error)) {
			return array('response' => $responseObject, 'error_message' => null);
		}else {
			return array('response' => null, 'error_message' => $responseObject->Error);
		}
	}
}
