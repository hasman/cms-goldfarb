<?php
use FedEx\RateService, FedEx\RateService\ComplexType, FedEx\RateService\SimpleType;
use FedEx\RateService\Request;

class Application_Model_FedExRate
{
	var $webAuthenticationDetail;
	var $clientDetail;

	public function __construct()
	{
		//require_once 'fedex_constants.php';
		ini_set("soap.wsdl_cache_enabled", "0");

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->credentials = array(
			'account_number' => $settings_mapper->getValueByCode('fedex_account_number'),
			'meter_number' => $settings_mapper->getValueByCode('fedex_meter_number'),
			'hub_id' => $settings_mapper->getValueByCode('fedex_hub_id'),
			'key' => $settings_mapper->getValueByCode('fedex_key'),
			'password' => $settings_mapper->getValueByCode('fedex_password'),
			'shipper_address' => $settings_mapper->getValueByCode('fedex_shipper_address'),
			'shipper_city' => $settings_mapper->getValueByCode('fedex_shipper_city'),
			'shipper_region' => $settings_mapper->getValueByCode('fedex_shipper_region'),
			'shipper_postcode' => $settings_mapper->getValueByCode('fedex_shipper_postcode'),
			'shipper_country' => $settings_mapper->getValueByCode('fedex_shipper_country'),
			'package_length' => $settings_mapper->getValueByCode('fedex_package_length',5),
			'package_width' => $settings_mapper->getValueByCode('fedex_package_width',5),
			'package_height' => $settings_mapper->getValueByCode('fedex_package_height',1),
		);
	}


	public function getAvailableRates($address, $weight = 5)
	{
		$rateRequest = new ComplexType\RateRequest();

		//authentication & client details
		$rateRequest->WebAuthenticationDetail->UserCredential->Key = $this->credentials['key'];
		$rateRequest->WebAuthenticationDetail->UserCredential->Password = $this->credentials['password'];
		$rateRequest->ClientDetail->AccountNumber = $this->credentials['account_number'];
		$rateRequest->ClientDetail->MeterNumber = $this->credentials['meter_number'];
		$rateRequest->TransactionDetail->CustomerTransactionId = 'rate service request';

		//version
		$rateRequest->Version->ServiceId = 'crs';
		$rateRequest->Version->Major = 24;
		$rateRequest->Version->Minor = 0;
		$rateRequest->Version->Intermediate = 0;
		$rateRequest->ReturnTransitAndCommit = true;

		//shipper
		$rateRequest->RequestedShipment->PreferredCurrency = 'USD';
		$rateRequest->RequestedShipment->Shipper->Address->StreetLines = [$this->credentials['shipper_address']];
		$rateRequest->RequestedShipment->Shipper->Address->City = $this->credentials['shipper_city'];
		$rateRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = $this->credentials['shipper_region'];
		$rateRequest->RequestedShipment->Shipper->Address->PostalCode = $this->credentials['shipper_postcode'];
		$rateRequest->RequestedShipment->Shipper->Address->CountryCode = $this->credentials['shipper_country'];

		//recipient
		$rateRequest->RequestedShipment->Recipient->Address->StreetLines = isset($address['address1'])?array($address['address1']):array();
		$rateRequest->RequestedShipment->Recipient->Address->City = $address['city'];

		if ($address['country'] == 'US' || $address['country'] == 'CA') {//TODO - we don't have normalized region codes internationally so just remove. FedEx requires 2 character code and throws an error otherwise. Doesn't seem to effect getting the rate
			$rateRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = $address['region'];
		}

		$rateRequest->RequestedShipment->Recipient->Address->PostalCode = $address['postcode'];
		$rateRequest->RequestedShipment->Recipient->Address->CountryCode = $address['country'];

		//shipping charges payment
		$rateRequest->RequestedShipment->ShippingChargesPayment->PaymentType = SimpleType\PaymentType::_SENDER;

		//rate request types
		$rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_PREFERRED, SimpleType\RateRequestType::_LIST];
		$rateRequest->RequestedShipment->PackageCount = 1;

		//create package line items
		$rateRequest->RequestedShipment->RequestedPackageLineItems = [new ComplexType\RequestedPackageLineItem()];

		//package
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Value = $weight;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Units = SimpleType\WeightUnits::_LB;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Length = $this->credentials['package_length'];
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Width = $this->credentials['package_width'];
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Height = $this->credentials['package_height'];
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Units = SimpleType\LinearUnits::_IN;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->GroupPackageCount = 1;

		$rateServiceRequest = new Request();

		if (APPLICATION_ENV == 'production') {
			$rateServiceRequest->getSoapClient()->__setLocation(Request::PRODUCTION_URL);
		}
		try {
			$rates = $rateServiceRequest->getGetRatesReply($rateRequest); // send true as the 2nd argument to return the SoapClient's stdClass response.
		}
		catch (Exception $e)
		{
			//echo 'Exception caught: ' . $e->getMessage() . "\n";
		}
//Cny_Debug::prettyPrint($rates);
		if (isset($rates->RateReplyDetails))
			return $rates->RateReplyDetails;
		else
			return array();
	}

}
