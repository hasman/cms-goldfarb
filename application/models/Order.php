<?php
class Application_Model_Order extends Application_Model_Abstract
{
	public $customer_id;
	public $email;
	public $special_instructions;
	public $tax_exemption;
	public $tax_declaration;
	public $tax_rate;
	public $quantity;
	public $bill_method;
	public $cart;
	public $shopper_ip;
	public $status;
	public $approved_at;
	public $counter = 1;
	public $shipping_charged = 'No';
	public $confirmation_sent = 'No';
	public $decline_count;
	public $decline_date;
}
