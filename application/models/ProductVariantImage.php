<?php
class Application_Model_ProductVariantImage extends Application_Model_Abstract
{
	public $product_variant_id;
	public $image_src;
	public $alt_text;
	public $sort_order;
	public $primary;
	public $secondary;
}
