<?php

class Application_Model_Payrix
{
	private $_cfg = array();

	public function __construct()
	{
		$options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();

		$settings_mapper = new Application_Model_Mapper_SiteSettingValue();

		$this->_cfg['api_key'] = $options['payrix']['private_api_key'];
		$this->_cfg['merchant_id'] = $settings_mapper->getValueByCode('ecommerce_payrix_merchant_id');
		$this->_store_name = $settings_mapper->getValueByCode('site_name');

		$this->_payrix_config = new \SplashPayments\Utilities\Config();
		$this->_payrix_config->setApiKey($this->_cfg['api_key']);
		if (APPLICATION_ENV != 'production') {
			$this->_payrix_config->setTestMode(true);
		}

	}

	/*
	 * Transaction Types:
	 *	'1'	Credit Card Only: Sale Transaction. This is the most common type of Transaction, it processes a sale and charges the customer
	 *	'2'	Credit Card Only: Auth Transaction. Authorizes and holds the requested total on the credit card
	 *	'3'	Credit Card Only: Capture Transaction. Finalizes a prior Auth Transaction and charges the customer
	 *	'4'	Credit Card Only: Reverse Authorization. Reverses a prior Auth or Sale Transaction and releases the credit hold.
	 *	'5'	Credit Card Only: Refund Transaction. Refunds a prior Capture or Sale Transaction (total may be specified for a partial refund).
	 *	'7'	Echeck Only: Echeck Sale Transaction. Sale Transaction for ECheck payment.
	 *	'8'	Echeck Only: ECheck Refund Transaction. Refund Transaction for prior ECheck Sale Transaction.
	 *	'9'	Echeck Only: Echeck PreSale Transaction. Notification of imminent Echeck Sale Transaction.
	 *	'10' Echeck Only: Echeck PreRefund Transaction. Notification of imminent Echeck Refund Transaction.
	 *	'11' Echeck Only: Echeck Redeposit Transaction. Attempt to redeposit a prior failed eCheck Sale Transaction.
	 *	'12' Echeck Only: Echeck Account Verification Transaction. Attempt to verify eCheck payment details
	 *	'14'
	 */
	public function createTransaction($payment, $totals = array(), $billing, $shipping, $products, $comments = null, $intent = 'sale')
	{
		$name = explode(" ", $payment['name']);
		//convert iso 2 letter to 3 letter
		$iso2to3 = ["TW" => "TWN","AF" => "AFG","AL" => "ALB","DZ" => "DZA","AS" => "ASM","AD" => "AND","AO" => "AGO","AI" => "AIA","AQ" => "ATA","AG" => "ATG","AR" => "ARG","AM" => "ARM","AW" => "ABW","AU" => "AUS","AT" => "AUT","AZ" => "AZE","BS" => "BHS","BH" => "BHR","BD" => "BGD","BB" => "BRB","BY" => "BLR","BE" => "BEL","BZ" => "BLZ","BJ" => "BEN","BM" => "BMU","BT" => "BTN","BO" => "BOL","BA" => "BIH","BW" => "BWA","BV" => "BVT","BR" => "BRA","IO" => "IOT","VG" => "VGB","BN" => "BRN","BG" => "BGR","BF" => "BFA","BI" => "BDI","CV" => "CPV","KH" => "KHM","CM" => "CMR","CA" => "CAN","KY" => "CYM","CF" => "CAF","TD" => "TCD","CL" => "CHL","CN" => "CHN","CX" => "CXR","CC" => "CCK","CO" => "COL","KM" => "COM","CG" => "COG","CK" => "COK","CR" => "CRI","HR" => "HRV","CU" => "CUB","CW" => "CUW","CY" => "CYP","CZ" => "CZE","CI" => "CIV","KP" => "PRK","CD" => "COD","DK" => "DNK","DJ" => "DJI","DM" => "DMA","DO" => "DOM","EC" => "ECU","EG" => "EGY","SV" => "SLV","GQ" => "GNQ","ER" => "ERI","EE" => "EST","ET" => "ETH","FK" => "FLK","FO" => "FRO","FJ" => "FJI","FI" => "FIN","FR" => "FRA","GF" => "GUF","PF" => "PYF","TF" => "ATF","GA" => "GAB","GM" => "GMB","GE" => "GEO","DE" => "DEU","GH" => "GHA","GI" => "GIB","GR" => "GRC","GL" => "GRL","GD" => "GRD","GP" => "GLP","GU" => "GUM","GT" => "GTM","GG" => "GGY","GN" => "GIN","GW" => "GNB","GY" => "GUY","HT" => "HTI","HM" => "HMD","VA" => "VAT","HN" => "HND","HU" => "HUN","IS" => "ISL","IN" => "IND","ID" => "IDN","IR" => "IRN","IQ" => "IRQ","IE" => "IRL","IM" => "IMN","IL" => "ISR","IT" => "ITA","JM" => "JAM","JP" => "JPN","JE" => "JEY","JO" => "JOR","KZ" => "KAZ","KE" => "KEN","KI" => "KIR","KW" => "KWT","KG" => "KGZ","LA" => "LAO","LV" => "LVA","LB" => "LBN","LS" => "LSO","LR" => "LBR","LY" => "LBY","LI" => "LIE","LT" => "LTU","LU" => "LUX","MG" => "MDG","MW" => "MWI","MY" => "MYS","MV" => "MDV","ML" => "MLI","MT" => "MLT","MH" => "MHL","MQ" => "MTQ","MR" => "MRT","MU" => "MUS","YT" => "MYT","MX" => "MEX","FM" => "FSM","MC" => "MCO","MN" => "MNG","ME" => "MNE","MS" => "MSR","MA" => "MAR","MZ" => "MOZ","MM" => "MMR","NA" => "NAM","NR" => "NRU","NP" => "NPL","NL" => "NLD","NC" => "NCL","NZ" => "NZL","NI" => "NIC","NE" => "NER","NG" => "NGA","NU" => "NIU","NF" => "NFK","MP" => "MNP","NO" => "NOR","OM" => "OMN","PK" => "PAK","PW" => "PLW","PA" => "PAN","PG" => "PNG","PY" => "PRY","PE" => "PER","PH" => "PHL","PN" => "PCN","PL" => "POL","PT" => "PRT","PR" => "PRI","QA" => "QAT","KR" => "KOR","MD" => "MDA","RO" => "ROU","RU" => "RUS","RW" => "RWA","RE" => "REU","BL" => "BLM","SH" => "SHN","KN" => "KNA","LC" => "LCA","MF" => "MAF","PM" => "SPM","VC" => "VCT","WS" => "WSM","SM" => "SMR","ST" => "STP","SA" => "SAU","SN" => "SEN","RS" => "SRB","SC" => "SYC","SL" => "SLE","SG" => "SGP","SX" => "SXM","SK" => "SVK","SI" => "SVN","SB" => "SLB","SO" => "SOM","ZA" => "ZAF","GS" => "SGS","SS" => "SSD","ES" => "ESP","LK" => "LKA","PS" => "PSE","SD" => "SDN","SR" => "SUR","SJ" => "SJM","SZ" => "SWZ","SE" => "SWE","CH" => "CHE","SY" => "SYR","TJ" => "TJK","TH" => "THA","MK" => "MKD","TL" => "TLS","TG" => "TGO","TK" => "TKL","TO" => "TON","TT" => "TTO","TN" => "TUN","TR" => "TUR","TM" => "TKM","TC" => "TCA","TV" => "TUV","UG" => "UGA","UA" => "UKR","AE" => "ARE","GB" => "GBR","TZ" => "TZA","UM" => "UMI","VI" => "VIR","US" => "USA","UY" => "URY","UZ" => "UZB","VU" => "VUT","VE" => "VEN","VN" => "VNM","WF" => "WLF","EH" => "ESH","YE" => "YEM","ZM" => "ZMB","ZW" => "ZWE","AX" => "ALA"];
		$billing['country'] = $iso2to3[$billing['country']];

		//convert Canada 2 letter Provence to full names
		$can2tofull = ["AB" => "Alberta","BC" => "British Columbia","MB" => "Manitoba","NB" => "New Brunswick","NL" => "Newfoundland and Labrador","NT" => "Northwest Territories","NS" => "Nova Scotia","NU" => "Nunavut","ON" => "Ontario","PE" => "Prince Edward Island","QC" => "Quebec","SK" => "Saskatchewan","YT" => "Yukon Territory"];
		if($billing['country'] == 'CA'){
		$billing['region'] = $can2tofull[$billing['region']];
		}

		$object = new \SplashPayments\txns(
			array (
				'merchant' => $this->_cfg['merchant_id'],
				'type' => 1,
				'origin' => 2, //e-commerce
				//'signature' => 0,
				'clientIp' => $_SERVER['REMOTE_ADDR'],
				'email' => isset($billing['email'])?$billing['email']:null,
				'first' => isset($name[0])?$name[0]:null,
				'last' => isset($name[1])?$name[1]:null,
				'company' => isset($billing['company'])?$billing['company']:null,
				'address1' => isset($billing['address'])?$billing['address']:null,
				'address2' => isset($billing['address2'])?$billing['address2']:null,
				'city' => isset($billing['city'])?$billing['city']:null,
				'state' => isset($billing['region'])?$billing['region']:null,
				'zip' => isset($billing['postcode'])?$billing['postcode']:null,
				'country' => isset($billing['country'])?$billing['country']:null, //can't pass country because everyone else uses 2 character ISO and Payrix using 3 character ISO value :(
				'payment' => array(
					//'method' => 2, //TODO - hopefully this truly is optional otherwise need to do CC type analysis
					'number' =>$payment['cc_num'],
					'cvv' => $payment['ccv']
				),
				'expiration' => str_pad($payment['exp_month'],2,"0",STR_PAD_LEFT).substr($payment['exp_year'],-2),
				'total' => (int)$totals['total']*100,
				'tax' => (int)(isset($totals['tax']))?$totals['tax']*100:0,
				'shipping' => (int)(isset($totals['shipping']))?$totals['shipping']*100:0,
				'discount' => (int)(isset($totals['discount']))?$totals['discount']*100:0,
				'description' => $comments
			)
		);

		try {
			if ($object->create()) {
				$tresponse = $object->getResponse();
			}
		}
		catch(\SplashPayments\Exceptions\Base $e) {
			//echo "ERROR <br/><br/>\r\n\r\n";
			//Cny_Debug::prettyPrint($object,true);
			// Handle exceptions
			//$error = $object->getErrors()[0]['msg'];
			$error = '';
			foreach ($object->getErrors() as $err) {
				$error .= $err['msg']."<br/>";
			}
		}

		$profile_response='';
		return array('response' => $tresponse[0], 'profile_response' => $profile_response, 'error_message' => $error);
	}

	public function createRecurringBilling($payment, $billing, $plan_id, $start_date)
	{
		$name = explode(" ", $payment['name']);

		$object = new \SplashPayments\customers(
			array (
				'merchant' => $this->_cfg['merchant_id'],
				'first' => isset($name[0])?$name[0]:null,
				'last' => isset($name[1])?$name[1]:null,
				'email' => isset($billing['email'])?$billing['email']:null,
				'inactive' => 0,
				'frozen' => 0
			)
		);

		try {
			if ($object->create()) {
				$customer = $object->getResponse();

				//create token of payment details
				$object = new \SplashPayments\tokens(
					array(
						'customer' => current($customer)->id,
						'payment' => array(
							//'method' => 2, //TODO - hopefully this truly is optional otherwise need to do CC type analysis
							'number' => $payment['cc_num'],
							'cvv' => $payment['ccv']
						),
						'expiration' => str_pad($payment['exp_month'], 2, "0", STR_PAD_LEFT) . substr($payment['exp_year'], -2),
						'inactive' => 0,
						'frozen' => 0
					)
				);

				try {
					if ($object->create()) {
						$token = $object->getResponse();

						//create subscription
						$object = new \SplashPayments\subscriptions(
							array(
								'plan' => $plan_id,
								'start' => $start_date,
								'origin' => 2,
								'inactive' => 0,
								'frozen' => 0
							)
						);

						try {
							if ($object->create()) {
								$subscription = $object->getResponse();

								//connect subscription to payment
								$object = new \SplashPayments\subscriptionTokens(
									array(
										'subscription' => current($subscription)->id,
										'token' => current($token)->token,
										'inactive' => 0,
										'frozen' => 0
									)
								);

								try {
									if ($object->create()) {
										$tresponse = $object->getResponse();
									}
								} catch (\SplashPayments\Exceptions\Base $e) {
									$error = $object->getErrors()[0]['msg'];
								}
							}
						} catch (\SplashPayments\Exceptions\Base $e) {
							//echo "ERROR <br/><br/>\r\n\r\n";
							//print_r($object->getErrors()[0]);exit;
							$error = $object->getErrors()[0]['msg'];
						}
					}
				} catch (\SplashPayments\Exceptions\Base $e) {
					$error = $object->getErrors()[0]['msg'];
				}
			}
		}
		catch (\SplashPayments\Exceptions\Base $e) {
			$error = $object->getErrors()[0]['msg'];
		}

		return array('response' => ($tresponse)?$tresponse[0]:null, 'error_message' => $error);
	}
}
